package com.tungphongdo.springboot_unitest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootUnitestApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootUnitestApplication.class, args);
    }

}
