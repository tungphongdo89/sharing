package com.tungphongdo;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PersonTest {

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void getMySalary() {
        double expectedResult = 200; //200$
        Person person = new Person();
        double actualResult = person.getMySalary();
        assertEquals(expectedResult, actualResult);
    }

    @Test
    void getMySalary2() {
        double expectedResult = 300;
        Person person = new Person();
        double actualResult = person.getMySalary();
        assertEquals(expectedResult, actualResult);
    }
}