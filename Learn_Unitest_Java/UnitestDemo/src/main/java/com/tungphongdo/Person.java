package com.tungphongdo;

public class Person {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getMySalary(){
        int numberOfWorkingDay = 20;
        int salaryPerDay = 10;
        double payment = numberOfWorkingDay * salaryPerDay;
        return payment;
    }
}
