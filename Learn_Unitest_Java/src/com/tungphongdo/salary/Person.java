package com.tungphongdo.salary;

public class Person {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getMySalary(){
        int numberOfWorkingDay = 20;
        int salaryPerDay = 10;
        int payment  = numberOfWorkingDay * salaryPerDay;
        return payment;
    }
}
