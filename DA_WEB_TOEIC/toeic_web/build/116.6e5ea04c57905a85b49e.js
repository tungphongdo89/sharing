(window.webpackJsonp=window.webpackJsonp||[]).push([[116],{1142:function(e,a,t){},3044:function(e,a,t){"use strict";t.r(a);var l=t(20),n=t.n(l),c=t(21),r=t.n(c),s=t(22),i=t.n(s),m=t(23),o=t.n(m),d=t(14),u=t.n(d),E=t(0),f=t.n(E),p=t(2024),v=t(2025),h=t(668),N=t(1894),b=t(2026),x=t(659),y=t(1590),g=t(220),w=t(1630),k=t(711),C=t(1855),z=t(714),R=t(1898),B=t(41),P=t(835),S=t(111),F=t.n(S);t(1142);function L(e){var a=function(){if("undefined"==typeof Reflect||!Reflect.construct)return!1;if(Reflect.construct.sham)return!1;if("function"==typeof Proxy)return!0;try{return Boolean.prototype.valueOf.call(Reflect.construct(Boolean,[],(function(){}))),!0}catch(e){return!1}}();return function(){var t,l=u()(e);if(a){var n=u()(this).constructor;t=Reflect.construct(l,arguments,n)}else t=l.apply(this,arguments);return o()(this,t)}}var A=function(e){i()(t,e);var a=L(t);function t(){return n()(this,t),a.apply(this,arguments)}return r()(t,[{key:"render",value:function(){return f.a.createElement(f.a.Fragment,null,f.a.createElement(p.a,null,f.a.createElement(v.a,{sm:"12"},f.a.createElement(h.a,null,f.a.createElement(N.a,null,f.a.createElement(b.a,null,"Account")),f.a.createElement(x.a,null,f.a.createElement(p.a,{className:"mx-0",col:"12"},f.a.createElement(v.a,{className:"pl-0",sm:"12"},f.a.createElement(y.a,{className:"d-sm-flex d-block"},f.a.createElement(y.a,{className:"mt-md-1 mt-0",left:!0},f.a.createElement(y.a,{className:"rounded mr-2",object:!0,src:F.a,alt:"Generic placeholder image",height:"112",width:"112"})),f.a.createElement(y.a,{body:!0},f.a.createElement(p.a,null,f.a.createElement(v.a,{sm:"9",md:"6",lg:"5"},f.a.createElement("div",{className:"users-page-view-table"},f.a.createElement("div",{className:"d-flex user-info"},f.a.createElement("div",{className:"user-info-title font-weight-bold"},"Username"),f.a.createElement("div",null,"crystal")),f.a.createElement("div",{className:"d-flex user-info"},f.a.createElement("div",{className:"user-info-title font-weight-bold"},"Name"),f.a.createElement("div",null,"Crystal Hamilton")),f.a.createElement("div",{className:"d-flex user-info"},f.a.createElement("div",{className:"user-info-title font-weight-bold"},"Email"),f.a.createElement("div",{className:"text-truncate"},f.a.createElement("span",null,"crystalhamilton@gmail.com"))))),f.a.createElement(v.a,{md:"12",lg:"5"},f.a.createElement("div",{className:"users-page-view-table"},f.a.createElement("div",{className:"d-flex user-info"},f.a.createElement("div",{className:"user-info-title font-weight-bold"},"Status"),f.a.createElement("div",null,"active")),f.a.createElement("div",{className:"d-flex user-info"},f.a.createElement("div",{className:"user-info-title font-weight-bold"},"Role"),f.a.createElement("div",null,"admin")),f.a.createElement("div",{className:"d-flex user-info"},f.a.createElement("div",{className:"user-info-title font-weight-bold"},"Company"),f.a.createElement("div",null,f.a.createElement("span",null,"North Star Aviation Pvt Ltd"))))))))),f.a.createElement(v.a,{className:"mt-1 pl-0",sm:"12"},f.a.createElement(g.a.Ripple,{className:"mr-1",color:"primary",outline:!0},f.a.createElement(B.Link,{to:"/app/user/edit"},f.a.createElement(k.a,{size:15}),f.a.createElement("span",{className:"align-middle ml-50"},"Edit"))),f.a.createElement(g.a.Ripple,{color:"danger",outline:!0},f.a.createElement(C.a,{size:15}),f.a.createElement("span",{className:"align-middle ml-50"},"Delete"))))))),f.a.createElement(v.a,{sm:"12",md:"6"},f.a.createElement(h.a,null,f.a.createElement(N.a,null,f.a.createElement(b.a,null,"Information")),f.a.createElement(x.a,null,f.a.createElement("div",{className:"users-page-view-table"},f.a.createElement("div",{className:"d-flex user-info"},f.a.createElement("div",{className:"user-info-title font-weight-bold"},"Birth Date"),f.a.createElement("div",null," 28 January 1998")),f.a.createElement("div",{className:"d-flex user-info"},f.a.createElement("div",{className:"user-info-title font-weight-bold"},"Mobile"),f.a.createElement("div",null,"+65958951757")),f.a.createElement("div",{className:"d-flex user-info"},f.a.createElement("div",{className:"user-info-title font-weight-bold"},"Website"),f.a.createElement("div",{className:"text-truncate"},f.a.createElement("span",null,"https://rowboat.com/insititious/Crystal"))),f.a.createElement("div",{className:"d-flex user-info"},f.a.createElement("div",{className:"user-info-title font-weight-bold"},"Languages"),f.a.createElement("div",{className:"text-truncate"},f.a.createElement("span",null,"English, French"))),f.a.createElement("div",{className:"d-flex user-info"},f.a.createElement("div",{className:"user-info-title font-weight-bold"},"Gender"),f.a.createElement("div",{className:"text-truncate"},f.a.createElement("span",null,"Female"))),f.a.createElement("div",{className:"d-flex user-info"},f.a.createElement("div",{className:"user-info-title font-weight-bold"},"Contact"),f.a.createElement("div",{className:"text-truncate"},f.a.createElement("span",null,"email, message, phone"))))))),f.a.createElement(v.a,{sm:"12",md:"6"},f.a.createElement(h.a,null,f.a.createElement(N.a,null,f.a.createElement(b.a,null,"Social Links")),f.a.createElement(x.a,null,f.a.createElement("div",{className:"users-page-view-table"},f.a.createElement("div",{className:"d-flex user-info"},f.a.createElement("div",{className:"user-info-title font-weight-bold"},"Twitter"),f.a.createElement("div",{className:"text-truncate"},f.a.createElement("span",null,"https://twitter.com/crystal"))),f.a.createElement("div",{className:"d-flex user-info"},f.a.createElement("div",{className:"user-info-title font-weight-bold"},"Facebook"),f.a.createElement("div",{className:"text-truncate"},f.a.createElement("span",null,"https://www.facebook.com/crystal"))),f.a.createElement("div",{className:"d-flex user-info"},f.a.createElement("div",{className:"user-info-title font-weight-bold"},"Instagram"),f.a.createElement("div",{className:"text-truncate"},f.a.createElement("span",null,"https://www.instagram.com/crystal"))),f.a.createElement("div",{className:"d-flex user-info"},f.a.createElement("div",{className:"user-info-title font-weight-bold"},"Github"),f.a.createElement("div",{className:"text-truncate"},f.a.createElement("span",null,"https://github.com/crystal"))),f.a.createElement("div",{className:"d-flex user-info"},f.a.createElement("div",{className:"user-info-title font-weight-bold"},"CodePen"),f.a.createElement("div",{className:"text-truncate"},f.a.createElement("span",null,"https://codepen.io/crystal"))),f.a.createElement("div",{className:"d-flex user-info"},f.a.createElement("div",{className:"user-info-title font-weight-bold"},"Slack"),f.a.createElement("div",{className:"text-truncate"},f.a.createElement("span",null,"@crystal"))))))),f.a.createElement(v.a,{sm:"12"},f.a.createElement(h.a,null,f.a.createElement(N.a,{className:"border-bottom pb-1 mx-2 px-0"},f.a.createElement(b.a,null,f.a.createElement(z.a,{size:18}),f.a.createElement("span",{className:"align-middle ml-50"},"Permissions"))),f.a.createElement(x.a,null," ",f.a.createElement(w.a,{className:"permissions-table",borderless:!0,responsive:!0},f.a.createElement("thead",null,f.a.createElement("tr",null,f.a.createElement("th",null,"Module"),f.a.createElement("th",null,"Read"),f.a.createElement("th",null,"Write"),f.a.createElement("th",null,"Create"),f.a.createElement("th",null,"Delete"))),f.a.createElement("tbody",null,f.a.createElement("tr",null,f.a.createElement("td",null,"Users"),f.a.createElement("td",null,f.a.createElement(P.a,{disabled:!0,color:"primary",icon:f.a.createElement(R.a,{className:"vx-icon",size:16}),label:"",defaultChecked:!0})),f.a.createElement("td",null,f.a.createElement(P.a,{disabled:!0,color:"primary",icon:f.a.createElement(R.a,{className:"vx-icon",size:16}),label:"",defaultChecked:!1})),f.a.createElement("td",null,f.a.createElement(P.a,{disabled:!0,color:"primary",icon:f.a.createElement(R.a,{className:"vx-icon",size:16}),label:"",defaultChecked:!1})),f.a.createElement("td",null," ",f.a.createElement(P.a,{disabled:!0,color:"primary",icon:f.a.createElement(R.a,{className:"vx-icon",size:16}),label:"",defaultChecked:!0}))),f.a.createElement("tr",null,f.a.createElement("td",null,"Articles"),f.a.createElement("td",null,f.a.createElement(P.a,{disabled:!0,color:"primary",icon:f.a.createElement(R.a,{className:"vx-icon",size:16}),label:"",defaultChecked:!1})),f.a.createElement("td",null,f.a.createElement(P.a,{disabled:!0,color:"primary",icon:f.a.createElement(R.a,{className:"vx-icon",size:16}),label:"",defaultChecked:!0})),f.a.createElement("td",null,f.a.createElement(P.a,{disabled:!0,color:"primary",icon:f.a.createElement(R.a,{className:"vx-icon",size:16}),label:"",defaultChecked:!1})),f.a.createElement("td",null," ",f.a.createElement(P.a,{disabled:!0,color:"primary",icon:f.a.createElement(R.a,{className:"vx-icon",size:16}),label:"",defaultChecked:!0}))),f.a.createElement("tr",null,f.a.createElement("td",null,"Staff"),f.a.createElement("td",null,f.a.createElement(P.a,{disabled:!0,color:"primary",icon:f.a.createElement(R.a,{className:"vx-icon",size:16}),label:"",defaultChecked:!0})),f.a.createElement("td",null,f.a.createElement(P.a,{disabled:!0,color:"primary",icon:f.a.createElement(R.a,{className:"vx-icon",size:16}),label:"",defaultChecked:!0})),f.a.createElement("td",null,f.a.createElement(P.a,{disabled:!0,color:"primary",icon:f.a.createElement(R.a,{className:"vx-icon",size:16}),label:"",defaultChecked:!1})),f.a.createElement("td",null," ",f.a.createElement(P.a,{disabled:!0,color:"primary",icon:f.a.createElement(R.a,{className:"vx-icon",size:16}),label:"",defaultChecked:!1}))))))))))}}]),t}(f.a.Component);a.default=A},835:function(e,a,t){"use strict";var l=t(20),n=t.n(l),c=t(21),r=t.n(c),s=t(22),i=t.n(s),m=t(23),o=t.n(m),d=t(14),u=t.n(d),E=t(0),f=t.n(E);function p(e){var a=function(){if("undefined"==typeof Reflect||!Reflect.construct)return!1;if(Reflect.construct.sham)return!1;if("function"==typeof Proxy)return!0;try{return Boolean.prototype.valueOf.call(Reflect.construct(Boolean,[],(function(){}))),!0}catch(e){return!1}}();return function(){var t,l=u()(e);if(a){var n=u()(this).constructor;t=Reflect.construct(l,arguments,n)}else t=l.apply(this,arguments);return o()(this,t)}}var v=function(e){i()(t,e);var a=p(t);function t(){return n()(this,t),a.apply(this,arguments)}return r()(t,[{key:"render",value:function(){return f.a.createElement("div",{className:"vx-checkbox-con ".concat(this.props.className?this.props.className:""," vx-checkbox-").concat(this.props.color)},f.a.createElement("input",{type:"checkbox",defaultChecked:this.props.defaultChecked,checked:this.props.checked,value:this.props.value,disabled:this.props.disabled,onClick:this.props.onClick?this.props.onClick:null,onChange:this.props.onChange?this.props.onChange:null}),f.a.createElement("span",{className:"vx-checkbox vx-checkbox-".concat(this.props.size?this.props.size:"md")},f.a.createElement("span",{className:"vx-checkbox--check"},this.props.icon)),f.a.createElement("span",null,this.props.label))}}]),t}(f.a.Component);a.a=v}}]);