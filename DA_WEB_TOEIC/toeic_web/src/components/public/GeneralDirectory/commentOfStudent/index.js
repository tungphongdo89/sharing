import React, {Component} from 'react';
import Swiper from "react-id-swiper"
import {Card, CardBody, CardHeader, Progress} from "reactstrap"
import img1 from "../../../../assets/img/slider/banner-26.jpg";
import img2 from "../../../../assets/img/slider/banner-39.jpg";
import img3 from "../../../../assets/img/slider/banner-28.jpg";
import img4 from "../../../../assets/img/slider/banner-29.jpg";
import img5 from "../../../../assets/img/slider/banner-30.jpg";

const dataImg = [
  {
    img: img1,
    title: "anh1"

  },
  {
    img: img2,
    title: "anh2"

  },
  {
    img: img3,
    title: "anh3"

  },
  {
    img: img4,
    title: "anh4"

  },
  {
    img: img5,
    title: "anh5"

  }
]


const params = {
  effect: "coverflow",
  grabCursor: true,
  centeredSlides: true,
  slidesPerView: "auto",
  coverflowEffect: {
    rotate: 50,
    stretch: 0,
    depth: 100,
    modifier: 1,
    slideShadows: true
  },
  pagination: {
    el: ".swiper-pagination"
  }
}


class CommentOfStudent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: dataImg
    }
  }

  // componentWillMount() {
  //   this.setState({
  //     data: dataImg
  //   });
  // }

  renderImg = () => {
    let xhtml = "";
    xhtml = dataImg.map((value, key) => {
      return <Card key={key} style={{height: "40rem", backgroundColor: "bisque"}}>
        <CardHeader className="mx-auto">
          <div className="avatar mr-1 avatar-xl">
            <img src={value.img} alt="avatarImg"/>
          </div>
        </CardHeader>
        <CardBody className="text-center">
          <h4>Jonell Binion</h4>
          <p>Designer</p>
          <div className="d-flex justify-content-between mt-1">
            <small className="float-left font-weight-bold mb-25">
              720 Points
            </small>
            <small className="float-left font-weight-bold mb-25">
              1000
            </small>
          </div>
          <Progress className="box-shadow-6" value="75"/>
          <div className="card-btns d-flex justify-content-between">
            <p className="desc">
              Trước khi học mình cũng hơi lo vì tính mình nhanh chán nên chỉ sợ học được vài bữa lại
              bỏ. Nhưng khi học rồi thì mình thấy thích lắm. Chương trình đặt ra nhiều đích từ gần đến xa, tạo cảm giác
              rõ ràng và giúp người học có động lực để phấn đấu. Mình cũng thích bảng xếp hạng tuần nữa, nhìn vào đó
              mình luôn cố gắng để được lọt vào top.</p>
          </div>
        </CardBody>
      </Card>
    })

    return xhtml;
  }

  render() {
    return (
      <Card>
        {/*<CardHeader>*/}
        {/*<CardTitle>Coverflow Effect</CardTitle>*/}
        {/*</CardHeader>*/}
        <CardBody>
          <Swiper {...params}>
            {
              this.renderImg()
            }
          </Swiper>
        </CardBody>
      </Card>
    );
  }
}

export default CommentOfStudent;
