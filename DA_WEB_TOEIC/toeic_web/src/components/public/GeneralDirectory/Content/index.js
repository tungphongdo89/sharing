import React, {Component} from 'react';
import CarouselPage2 from "../../../../commons/SildePage/CarouselPage2";
// import ListStudentTop from "./listStudentTop";
import {Card, CardActionArea, CardContent, CardMedia, Typography, withStyles} from '@material-ui/core';
import styles from "./styles"
import ResponsivePlayer from "../../../../commons/Media/ResponsivePlayer";
import MultiSlides from "../../../../extensions/swiper/MultiSlides";
import "swiper/css/swiper.css"
import "../../../../assets/scss/plugins/extensions/swiper.scss"
import CommentOfStudent from "../commentOfStudent";
import ButtonNext from "../../../../views/ui-elements/button-next";
import {FormattedMessage} from "react-intl";

const listTopTeacher = [{
  id: 1,
  alt: "Huseyn_Badalov",
  image: "https://s3-ap-southeast-1.amazonaws.com/cnd.migitek.storage/image/imgHome/Huseyn_Badalov.jpg",
  title: "Huseyn_Badalov",
  description: "Master/ Teacher Huseyn Badalov has 10 years worked in English certification's training (Toeic, IELTS)."
}, {
  id: 2,
  alt: "Teacher_Paul",
  image: "https://s3-ap-southeast-1.amazonaws.com/cnd.migitek.storage/image/imgHome/Thay-Paul.jpg",
  title: "Teacher Paul",
  description: "Master/ Teacher Huseyn Badalov has 10 years worked in English certification's training (Toeic, IELTS)."
},
  {
    id: 3,
    alt: "Tony_Malcolm_Sparg",
    image: "https://s3-ap-southeast-1.amazonaws.com/cnd.migitek.storage/image/imgHome/Tony_Malcolm_Sparg.jpg",
    title: "Tony Malcolm Sparg",
    description: "Master/ Teacher Huseyn Badalov has 10 years worked in English certification's training (Toeic, IELTS)."
  }
]

class ContentHomePage extends Component {

  renderTeacher = () => {
    const {classes} = this.props
    let xhtml = "";
    xhtml = listTopTeacher.map((value, key) => {
      return <div key={key} className="col-sm">
        <Card className={classes.root}>
          <CardActionArea>
            <CardMedia
              component="img"
              alt={value.alt}
              height="250px"
              image={value.image}
              title={value.title}
            />
            <CardContent>
              <Typography gutterBottom variant="h5" component="h2">
                {value.title}
              </Typography>
              <Typography variant="body2" color="textSecondary" component="p" style={{maxHeight: "400"}}>
                {value.description}
              </Typography>
            </CardContent>
          </CardActionArea>
        </Card>
      </div>
    })
    return xhtml;
  }

  render() {
    const {classes} = this.props
    return (
      <div>
        {/*<CarouselPage2/>*/}
        <div className="divider" style={{maxWidth: "90%", margin: "auto"}}>
          <div className="divider-text" style={{
            backgroundColor: "transparent",
          }}>
            <h3 style={{
              fontWeight: "600",
              lineHeight: '1.66',
              fontSize: "28px",
              textTransform: "capitalize",
              textAlign: "center",
              margin: "15px"
            }}>
              {/*Sinh viên tiêu biểu*/}
              <FormattedMessage id="representative.students" tagName="data" />
            </h3>
          </div>
        </div>
        <div className="container" style={{maxWidth: "90%"}}>
          {/*<hr style={{textAlign: "center", width: "30%", backgroundColor: "#000"}}/>*/}
          {/*<ListStudentTop/>*/}
          <MultiSlides/>
        </div>
        <div className="divider" style={{maxWidth: "90%", margin: "auto"}}>
          <div className="divider-text" style={{
            backgroundColor: "transparent",
          }}>
            <h3 style={{
              fontWeight: "600",
              lineHeight: '1.66',
              fontSize: "28px",
              textTransform: "capitalize",
              textAlign: "center",
              margin: "15px"
            }}>
              {/*Giáo viên tiêu biểu*/}
              <FormattedMessage id="representative.teachers" tagName="data" />
            </h3>
          </div>
        </div>
        {/*<hr style={{textAlign: "center", width: "30%", top: "10%", backgroundColor: "#000"}}/>*/}
        <div className="container" style={{maxWidth: "80%",}}>
          <div className="row">
            {this.renderTeacher()}
          </div>
        </div>
        <div className="divider" style={{
          maxWidth: "90%", margin: "auto", marginTop: "3rem 0",
        }}>
          <div className="divider-text" style={{
            backgroundColor: "transparent",
          }}>
            <h3 style={{
              fontWeight: "600",
              lineHeight: '1.66',
              fontSize: "28px",
              textTransform: "capitalize",
              textAlign: "center",
              margin: "1rem 0"
            }}>
              {/*Video giới thiệu*/}
              <FormattedMessage id="introduction.video" tagName="data" />
            </h3>
          </div>
        </div>
        {/*<hr style={{textAlign: "center", width: "30%", backgroundColor: "#000", marginTop: "25px"}}/>*/}
        <div className="container">
          <ResponsivePlayer/>
        </div>
        <div className="divider" style={{
          maxWidth: "90%", margin: "auto", marginTop: "3rem 0",
        }}>
          <div className="divider-text" style={{
            backgroundColor: "transparent",
          }}>
            <h3 style={{
              fontWeight: "600",
              lineHeight: '1.66',
              fontSize: "28px",
              textTransform: "capitalize",
              textAlign: "center",
              margin: "1rem 0"
            }}>
              {/*Nhận xét của học viên*/}
              <FormattedMessage id="commentary.student" tagName="data" />
            </h3>
          </div>
        </div>
        <div className="container" style={{maxWidth: "90%",}}>
          <CommentOfStudent/>
        </div>
        <div className="container" style={{maxWidth: "90%", margin: "auto"}}>
          {/*<ButtonNext/>*/}
        </div>
      </div>
    );
  }
}

ContentHomePage.propTypes = {};

export default withStyles(styles)(ContentHomePage);
