const styles = theme => ({
  root: {
    maxWidth: 400,
    margin: "auto",
    height: "100%",
    boxShadow: "-9px 15px 11px 6px #888888"

    // padding:"5%"
  },
  wrapper: {
    display: 'flex',
    height: "100vh",
    flexDirection: 'row'
  },
  wrapperContent: {
    width: "100%",
    padding: 10,
    position: "relative",
  },
  shiftleft: {
    // marginLeft: -240,
    position: "absolute"
  },
  dividerText: {
    // background: "none"
  }
})
export default styles
