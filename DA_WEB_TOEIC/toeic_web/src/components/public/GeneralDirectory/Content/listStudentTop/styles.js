const styles = theme => ({
  root: {
    maxWidth: 345,
  },
  banner: {
    marginTop: "2%",
    padding: "0 5%",
    display: 'flex',
    height: "500px",
    position: 'relative',
    textAlign: "center",
    bannerGrid: {
      height: '100%'
    },
    background: 'transparent'
  },
  content: {
    color: "white",
    backgroundColor: "darkred",
    height: "100%",
    cursor: "pointer",
    padding: "30px",
    transition: "300ms",
    "&:hover,&:active": {
      backgroundColor: "darkred",
      viewButton: {
        backgroundColor: '#efefef',
        color: "#8b0000 "
      }
    },
    title: {
      fontSize: '14pt',
      fontWeight: 500,
    },
    caption: {
      fontSize: '16pt',
      marginTop: "10px",
    },
    viewButton: {
      marginTop: "40px",
      color: "white",
      fontSize: "600",
      border: "3px solid white",
      textTransform: "capitalize",
      transition: "200ms",
    }
  },
  Media: {
    backgroundColor: "white",
    marginTop: "5%",
    height: "250px",
    width: "250px",
    borderRadius: "50%",
    overflow: "hidden",
    position: "relative",
    MediaCaption: {
      textOverflow: "ellipsis",
      position: "absolute",
      bottom: 0,

      padding: "15px",

      backgroundColor: "black",
      color: "white",
      opacity: 0.6,

      width: "100%",
      height: "10%",
      fontSize: "30px",
      fontWeight: 200,

      transition: '300ms',
      cursor: "pointer",
      "&:hover":
        {
          opacity: 0.8,

        }
    },
    transition: "300ms",
    cursor: "pointer",
    "&:hover":
      {
        filter: "brightness(115%)",

      }
  },
})
export default styles
