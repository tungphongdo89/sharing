import React, {Component} from 'react';
import {withStyles} from '@material-ui/core';
import styles from "./styles"
import Breadcrumbs from "../../../../../views/ui-elements/cards/basic/Cards";
import {Col, Row} from "reactstrap";
import BasicCards from "../../../../../views/ui-elements/cards/basic/BasicCards";


class ListStudentTop extends Component {
  constructor(props) {
    super(props);
    this.state = {
      autoPlay: true,
      timer: 15000,
      animation: "fade",
      indicators: true,
      timeout: 500,
      navButtonsAlwaysVisible: false
    }
  }

  render() {
    const {autoPlay, timer, animation, indicators, timeout, navButtonsAlwaysVisible} = this.state;
    const {classes} = this.props
    return (
      <div>
        <React.Fragment>
          <Breadcrumbs
            breadCrumbTitle="Basic Cards"
            breadCrumbParent="Card"
            breadCrumbActive="Basic Cards"
          />
          <Row>
            <Col sm="12">
              <BasicCards/>
            </Col>
          </Row>
        </React.Fragment>
      </div>
    );
  }
}

ListStudentTop.propTypes = {};

export default withStyles(styles)(ListStudentTop);
