import {fade} from '@material-ui/core/styles';

const styles = theme => ({
  grow: {
    flexGrow: 0.5,
  },
  grow2: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    display: 'none',
    [theme.breakpoints.up('sm')]: {
      display: 'block',
    },
    marginLeft: "5%"
  },
  search: {
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: fade(theme.palette.common.white, 0.15),
    '&:hover': {
      backgroundColor: fade(theme.palette.common.white, 0.25),
    },
    marginRight: theme.spacing(2),
    marginLeft: 0,
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      marginLeft: theme.spacing(3),
      width: 'auto',
    },
  },
  searchIcon: {
    padding: theme.spacing(0, 2),
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  inputRoot: {
    color: 'inherit',
  },
  inputInput: {
    padding: theme.spacing(1, 1, 1, 0),
    // vertical padding + font size from searchIcon
    paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('md')]: {
      width: '20ch',
    },
  },
  sectionDesktop: {
    display: 'none',
    [theme.breakpoints.up('md')]: {
      display: 'flex',
    },
  },
  sectionMobile: {
    display: 'flex',
    [theme.breakpoints.up('md')]: {
      display: 'none',
    },
  },
  linkMenu: {
    float: "right",
    display: "inline-flex",
    "&>div>div>button": {
      color: "white"
    }

  },
  NavHeader: {
    display: "flex",
    "&>ul>li": {
      margin: 'auto'
    }
  },
  UlHeader: {
    display: "flex",
    color: "white",
    "&>li": {
      "&>a": {
        color: "white",
      },
      // margin: "auto",
      float: "left",
      paddingRight: "10%",
      // alignContent: "space-between"
    }
  },
  login: {
    '&:hover': {
      color: "red"
    }
  },
  register: {
    '&:hover': {
      color: "red"
    }
  },
  activeClassName: {
    "&:focus": {
      // color: "#fff"
      color: "red"
    },
    // color: "primary",
  },

})
export default styles
