import React, {Component, useContext} from 'react';
import PropTypes from 'prop-types';
import {bindActionCreators, compose} from "redux";
import SideBarAction from "../../../../redux/actions/sidebar/sidebar.action";
import {connect} from "react-redux";
import {withStyles} from "@material-ui/core";
import styles from "./styles"
import logoMigi from "../../../../../src/assets/img/logo/migi.png"
import userImg from "../../../../../src/assets/img/profile/user-uploads/user-02.jpg"
import {Link, NavLink} from "react-router-dom"
import ShowContactActions from "../../../../redux/actions/contact";

import menuStudentData from "../../../../assets/data/header/menu.student"

import {ChevronDown, ChevronRight, LogIn, Power, User, UserPlus} from "react-feather"
import {
  Button,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Navbar,
  UncontrolledButtonDropdown,
  UncontrolledDropdown
} from "reactstrap"
import loginActions from "../../../../redux/actions/auth/loginActions";
import {Auth0Context} from "../../../../authServices/auth0/auth0Service";
import {Nav} from "react-bootstrap";
import {FormattedMessage} from "react-intl";

export const useAuth0 = () => useContext(Auth0Context)

class HeaderHomePage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      mobileMoreAnchorEl: null,
      isMobileMenuOpen: false,
      anchorEl: null,
      anchorElHeader: null,
      isMenuOpen: false,
      value: 0

    }
  }


  componentDidMount() {
    document.getElementById("root").click();
  }

  logoutHandle = () => {
    const {loginActions} = this.props;
    const {logoutWithJWT} = loginActions;
    logoutWithJWT();
  }

  renderMenu = (value, index) => {
    if (value.children && value.children.length > 0) {
      return <UncontrolledDropdown direction={"right"} className="dropdown mr-1 d-inline-block" nav
                                   key={"UCD_" + index + value.name} style={{float: "right"}}>
        <DropdownToggle
          // color="primary"
          // outline
          style={{
            textTransform: "uppercase",
            textColor: "#fff",
            backgroundColor: "none",
            fontWeight: "400",
            borderRadius: "20px"
          }}
          caret
          nav
        >
          {value.name}
          <ChevronRight size={15} className="right"/>
        </DropdownToggle>
        <DropdownMenu className="menu menu-dropdown-container" style={{marginLeft: "20%", top: "5%"}}>
          <Nav>
            {value.children.map((listItem) => this.renderMenu(listItem, index + 1))}
          </Nav>
        </DropdownMenu>
      </UncontrolledDropdown>
    } else {
      return <DropdownItem className="menu menu-item-container" key={"DDNavItem_" + index + value.name}>
        <NavLink tag="a" to={value.path} className="mr-1 font-small-3 font-weight-bold uppercase"
        >
          {value.name}
        </NavLink>
      </DropdownItem>
    }
  }

openContactForm = () =>{
    const {ShowContactActions} = this.props;
    const {showContact} = ShowContactActions;
    showContact();
}

openHomeContent = ()=>{
  const {ShowContactActions} = this.props;
  const {hideContact} = ShowContactActions;
  hideContact();
}

  render() {
    const {classes, name, isAuthenticated} = this.props
    return (
      <Navbar
        className="header-navbar  navbar-expand-lg navbar navbar-with-menu  navbar-light navbar-shadow ">
        <div className="navbar-wrapper">
          <div className="navbar-container content">
            <div className="navbar-collapse" id="navbar-mobile">
              <div className="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                <img src={logoMigi} alt="" width="250" height="50" left="10"/>
              </div>
              <ul className="nav navbar-nav ">
                <li>
                  <div className="dropdown mr-1 mb-1 mt-1 ">
                    <UncontrolledButtonDropdown className="dropdown mr-1 ">
                      <Button.Ripple onClick={this.openHomeContent} outline color="primary" style={{borderRadius: "20px"}}>
                        <NavLink tag="a" to="/" className="font-small-3 font-weight-bold">
                          HOME
                        </NavLink>
                      </Button.Ripple>
                    </UncontrolledButtonDropdown>
                    <UncontrolledButtonDropdown className="dropdown mr-1 d-inline-block">
                      <DropdownToggle
                        color="primary"
                        outline
                        style={{
                          textTransform: "uppercase",
                          textColor: "#fff",
                          fontWeight: "400",
                          borderRadius: "20px"
                        }}
                        caret
                      >
                        Bài thi
                        <ChevronDown size={15}/>
                      </DropdownToggle>
                      <DropdownMenu>
                        {
                          menuStudentData.map((value => this.renderMenu(value, 0)))
                        }
                      </DropdownMenu>
                    </UncontrolledButtonDropdown>
                    <UncontrolledButtonDropdown  className="dropdown mr-1  ">
                      <DropdownToggle
                        color="primary"
                        outline
                        style={{
                          textTransform: "uppercase",
                          textColor: "#fff",
                          backgroundColor: "none",
                          fontWeight: "400",
                          borderRadius: "20px"
                        }}
                        caret
                      >
                        Lộ trình
                        <ChevronDown size={15}/>
                      </DropdownToggle>
                      <DropdownMenu>
                        <DropdownItem tag="a">Option 1</DropdownItem>
                        <DropdownItem tag="a">Option 2</DropdownItem>
                        <DropdownItem tag="a">Option 3</DropdownItem>
                      </DropdownMenu>
                    </UncontrolledButtonDropdown>
                    <UncontrolledButtonDropdown className="dropdown mr-1 ">
                      <Button.Ripple onClick={this.openContactForm} outline color="primary" style={{borderRadius: "20px"}}>
                        <NavLink tag="a" to="/contact" className="font-small-3 font-weight-bold ">
                          LIÊN HỆ
                        </NavLink>
                      </Button.Ripple>
                    </UncontrolledButtonDropdown>
                  </div>
                </li>
              </ul>
              <div style={{flexGrow: "0.2"}}></div>
              {
                isAuthenticated && <ul className="nav navbar-nav float-right">
                  <UncontrolledDropdown
                    tag="li"
                    className="dropdown-user nav-item"
                  >
                    <DropdownToggle
                      tag="a"
                      data-toggle="dropdown"
                      className="nav-link dropdown-user-lk"
                    >
                      <div className="user-nav d-sm-flex d-none">
                        <span className="user-name text-boldin-600">
                          John Doe
                        </span>
                        <span className="user-status">Available</span>
                      </div>
                      <span>
                        <img
                          src={userImg}
                          className="round"
                          height="40"
                          width="40"
                          alt="avatar"
                        />
                      </span>
                    </DropdownToggle>
                    <DropdownMenu right>
                      <DropdownItem tag="a" href="#">
                        <User size={14} className="mr-50"/>
                        <span className="align-bottom">
                          Edit Profile
                        </span>
                      </DropdownItem>
                      <DropdownItem divider/>
                      <DropdownItem tag="a" href="/pages/reset-password">
                        
                        <span className="align-bottom">
                          <FormattedMessage id={"change.pass"}/>
                        </span>
                      </DropdownItem>
                      <DropdownItem divider/>
                      <DropdownItem tag="a" onClick={() => this.logoutHandle()}>
                        <Power size={14} className="mr-50"/>
                        <span>Log Out</span>
                      </DropdownItem>
                    </DropdownMenu>
                  </UncontrolledDropdown>
                </ul>
              }
              {
                !isAuthenticated && <ul className="nav navbar-nav float-right">
                  <Button.Ripple className="mt-1" color="flat-success">
                    <LogIn size={14}/>
                    <b><Link to="/pages/login" className={classes.login}>Đăng nhập </Link></b>
                  </Button.Ripple>

                  <Button.Ripple className="mt-1" color="flat-success">
                    <UserPlus size={14}/>
                    <b><Link to="/pages/register" className={classes.register}> Đăng ký</Link></b>
                  </Button.Ripple>
                </ul>
              }
            </div>
          </div>
        </div>
      </Navbar>
    );
  }
}

HeaderHomePage.propTypes = {
  classes: PropTypes.object,
  open: PropTypes.bool,
  history: PropTypes.object,
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
  isAuthenticated: PropTypes.bool,
  user: PropTypes.object
}

const mapStateToProps = (state, ownProps) => {
  return {
    open: state.sideBarReducer.open,
    isAuthenticated: state.auth.login.isAuthenticated,
    user: state.auth
  }
}
const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    sideBarAction: bindActionCreators(SideBarAction, dispatch),
    loginActions: bindActionCreators(loginActions, dispatch),
    ShowContactActions: bindActionCreators(ShowContactActions,dispatch),
    useAuth0: useAuth0
  }
}
const withConnect = connect(mapStateToProps, mapDispatchToProps)

export default compose(withStyles(styles), withConnect)(HeaderHomePage);
