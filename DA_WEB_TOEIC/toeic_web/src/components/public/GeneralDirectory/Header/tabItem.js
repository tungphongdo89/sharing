import React from "react"
import {Nav, NavItem, NavLink, TabContent, TabPane} from "reactstrap"
import classnames from "classnames"

class TabsItemCentered extends React.Component {

  state = {
    active: "1",
    dropdownOpen: false
  }

  toggle = tab => {
    if (this.state.active !== tab) {
      this.setState({active: tab})
    }
  }

  togggleDropdown = () => {
    this.setState({
      dropdownOpen: !this.state.dropdownOpen
    })
  }

  render() {
    return (
      <Nav tabs className="justify-content-center">
        <NavItem>
          <NavLink
            className={classnames({
              active: this.state.active === "1"
            })}
            onClick={() => {
              this.toggle("1")
            }}
          >
            Home
          </NavLink>
        </NavItem>
        <NavItem>
          <NavLink
            className={classnames({
              active: this.state.active === "2"
            })}
            onClick={() => {
              this.toggle("2")
            }}
          >
            Service
          </NavLink>
        </NavItem>
        <NavItem>
          <NavLink disabled>Disabled</NavLink>
        </NavItem>
        <NavItem>
          <NavLink
            className={classnames({
              active: this.state.active === "3"
            })}
            onClick={() => {
              this.toggle("3")
            }}
          >
            Account
          </NavLink>
        </NavItem>
      </Nav>

    )
  }
}

export default TabsItemCentered
