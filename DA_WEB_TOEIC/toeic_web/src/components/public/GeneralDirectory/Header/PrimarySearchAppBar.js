import React, {Component} from 'react';
import {withStyles} from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import logoMigi from "../../../../../src/assets/img/logo/migi_trans.png"
import {history} from "../../../../history";
import {getSessionCookie} from "../../../../commons/configCookie";

import {
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Spinner,
  UncontrolledButtonDropdown,
  UncontrolledDropdown
} from "reactstrap";
import {ChevronDown, ChevronRight, Power, User} from "react-feather";
import {Link, NavLink} from "react-router-dom";
import styles from "./styles";
import PropTypes from "prop-types";
import {bindActionCreators, compose} from "redux";
import SideBarAction from "../../../../redux/actions/sidebar/sidebar.action";
import loginActions from "../../../../redux/actions/auth/loginActions";
import topicActions from "../../../../redux/actions/topic/topicAction";
import wordAction from "../../../../redux/actions/word/wordAction";
import {connect} from "react-redux";
import {useAuth0} from "./index";
import {
  CODE_PART1_LF,
  CODE_PART2_LF,
  CODE_PART3_LF,
  CODE_PART4_LF,
  LISTENING_FILL_UNIT,
  ROLE_STUDENT
} from "../../../../commons/constants/CONSTANTS";

import translationAction from "../../../../redux/actions/translation-exercise/translationAction";
import practiceListenMultiActions from "../../../../redux/actions/practice/listening";
import readingActions from "../../../../redux/actions/practice/reading";
import testAction from "../../../../redux/actions/do-test-management";
import historyTestAction from "../../../../redux/actions/history-test";
import Multilangue from "../../../../layouts/components/navbar/Multilangue";
import {FormattedMessage} from "react-intl";
import * as Icon from "react-bootstrap-icons"
import {showMessage} from "../../../../commons/utils/convertDataForToast";
import historyPracticesAction from "../../../../redux/actions/history-practices";

class PrimarySearchAppBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      load: false,
      anchorEl: null,
      mobileMoreAnchorEl: null,
      isMenuOpen: false,
      isMobileMenuOpen: false
    }
  }

  componentDidMount() {
    document.getElementById("root").click();
    if (this.props.login.isAuthenticated && this.props.login.userRole === ROLE_STUDENT) {
      this.props.topicActions.fetchListPractices();
      this.props.topicActions.getMenuListTest();
    }
  }

  logoutHandle = () => {
    const {loginActions} = this.props;
    const {logoutWithJWT} = loginActions;
    history.push("/");
    logoutWithJWT()
  }

  onItemClick= ()=> {
    document.getElementById('root').click()
  }
  nextPage = (paramValue, paramId, paramCode) => {
    console.log("value: ", paramValue);
    console.log("id: ", paramId);

    let question = {};
    question.parentId = paramId;

    const {translationAction, practiceListenMultiAction, testAction, wordAction} = this.props;
    const {noTranslationWhenDoingTest} = testAction;
    noTranslationWhenDoingTest(false)
    //wordAction.togglePopup(false);
    const {fetchQaTransAvByLevel, fetchQaTransVaByLevel} = translationAction;
    const {getPartId} = practiceListenMultiAction;
    if (paramCode === CODE_PART1_LF || paramCode === CODE_PART2_LF || paramCode === CODE_PART3_LF || paramCode === CODE_PART4_LF) {
      getPartId(paramId);
    } else getPartId(0);
    if (null !== paramValue) {
      let {disableAnswerWhenSubmit, setLevelTitleForAVVA} = translationAction;
      disableAnswerWhenSubmit(false);
      if (question.parentId === 61 || question.parentId === 104) {
        // setLevelTitleForAVVA("Dễ")
        setLevelTitleForAVVA("easy")
      }
      if (question.parentId === 62 || question.parentId === 105) {
        // setLevelTitleForAVVA("Trung bình")
        setLevelTitleForAVVA("medium")
      }
      if (question.parentId === 63 || question.parentId === 106) {
        // setLevelTitleForAVVA("Khó")
        setLevelTitleForAVVA("difficult")
      }

      if (paramValue === "3") {
        fetchQaTransAvByLevel(question);
      }
      if (paramValue === "4") {
        fetchQaTransVaByLevel(question);
      }
    }
    this.onItemClick();
  }

  getMinitest = () => {
    const {testAction} = this.props;
    const {cleanTest} = testAction;
    cleanTest();
    window.location.replace("/pages/minitest");
    // history.push("/pages/minitest")
  }

  mouseEnterEvent = () => {
    this.setState({
      styleMouse: {width: "100%", display: 'flex', padding: '10px', backgroundColor: 'lightgray'}
    })
  }

  mouseLeaveEvent = () => {
    this.setState({
      styleMouse: {width: "100%", display: 'flex', padding: '10px', backgroundColor: 'white'}
    })
  }

  formatListPractices = (data, parentCode) => {
    const arr = [];
    for (let i = 0; i < data.length; i++) {
      const dataItem = data[i];
      if (dataItem.paramParentCode === parentCode) {
        const children = this.formatListPractices(data, dataItem.paramCode);
        if (children.length > 0) {
          dataItem.children = children;
        }
        arr.push(dataItem);
      }
    }
    return arr;
  }

  cutStringName = (data)=>{
    let str= data;
    if(data !== null){
      if(data.toString().length <= 20){
        str = data;
      } else {
        str = data.slice(0,20) + '...';
      }
    }
    return str;
  }

  renderMenuHeader = (value, index) => {
    if (value.children && value.children.length > 0) {
      return <UncontrolledDropdown direction={"right"} className="dropdown mr-1 d-inline-block"
                                   style={{float: "right"}}>
        <DropdownToggle
          // color="primary"
          // outline
          style={{
            width: "100%",
            textTransform: "uppercase",
            textColor: "#fff",
            backgroundColor: "none",
            fontWeight: "400",
            borderRadius: "20px"
          }}
          caret
          nav
          id="headerTest"
          onClick={() => {
            const {practiceListenMultiAction} = this.props;
            const {getValueTypeCode} = practiceListenMultiAction;
            getValueTypeCode(value.paramValue)
            const {multiListenQuestionAction, readingActions} = this.props;
            multiListenQuestionAction.cleanListQuestionOfListeningWordFill();
            multiListenQuestionAction.cleanResultQuestionOfListeningWordFill();
            multiListenQuestionAction.cleanNumberCorrectAnswerWordFill();
            multiListenQuestionAction.cleanListQuestionAndAnswerListening();
            readingActions.cleanListQuestionOfReadingWordFill();
            readingActions.cleanResultQuestionOfReadingWordFill();
            readingActions.getTypeCodeReading(value.paramValue)
          }}
        >
          <FormattedMessage id={value.paramCode.toLowerCase().replaceAll("_",".")} tagName={value.paramCode.toLowerCase().replaceAll("_",".")} />
          {/*<ChevronRight size={15} className="right" />*/}
          <ChevronRight size={15} className="right"/>
        </DropdownToggle>
        <DropdownMenu className="menu menu-dropdown-container"
                      style={value.paramValue === "3" || value.paramValue === "4" ? { marginLeft: "30px", top: "5%", padding: '0px' }
                        : value.paramCode === LISTENING_FILL_UNIT ? { marginLeft: "30px", top: "5%", width: '120%', padding: '0px' }
                          : { marginLeft: "30px", top: "5%", width: '135%', padding: '0px' }}
        >
          {value.children.map((listItem) => this.renderMenuHeader(listItem, index + 1))}

        </DropdownMenu>
      </UncontrolledDropdown>
    } else {
      // return <DropdownItem style={{width:"100%", padding: '0px !important'}} className="menu menu-item-container" key={"DDNavItem_" + index + value.paramName}>
      //   <NavLink  tag="a" to={value.paramValue && value.paramValue.indexOf("/") !== -1 ?
      //     value.paramValue : value.paramValue === "3" ? "/pages/translation-av" : "/pages/translation-va" }
      //             className="mr-1 font-small-3 font-weight-bold uppercase"
      //             onClick={()=> this.nextPage(value.paramValue, value.paramId)}
      //   >
      //     {value.paramName}
      //   </NavLink>
      // </DropdownItem>

      return (
        <>
            <NavLink key={"DDNavItem_" + index + value.paramName}
              // style={this.state.styleMouse}
              // onMouseEnter={this.mouseEnterEvent} onMouseLeave={this.mouseLeaveEvent}
                     style={{ width: "100%", display: 'flex', padding: '10px', borderBottom: '1px solid lightgray' }}
                     tag="a" to={value.paramValue && value.paramValue.indexOf("/") !== -1 ?
              value.paramValue : value.paramValue === "3" ? "/pages/translation-av" : "/pages/translation-va"}
                     className="mr-1 font-small-3 font-weight-bold uppercase"
                     onClick={() => this.nextPage(value.paramValue, value.paramId, value.paramCode)} id="headerTest"
            >
              <FormattedMessage id={value.paramCode.toLowerCase().replaceAll("_",".")}  />
            </NavLink>
        </>
      )

    }
  }

  render() {
    const {classes, login, onRef, user, listTypeTests} = this.props
    let listTests = []
    if (undefined !== listTypeTests) {
      listTests = listTypeTests;
    }
    const {isAuthenticated, userRole} = login;
    const loadingComponent = (
      <div style={{
        position: 'absolute',
        zIndex: 110,
        top: 0,
        left: 0,
        width: '100%',
        height: '100%',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        background: 'rgba(255,255,255,0.8)',
      }}>
        <Spinner color="primary" className="reload-spinner"/>
      </div>
    );
    return (
      <>
        <style>{`
          #partName{
            background-color: gray !important:
          }
          #btnHomePage:focus{
            // background-color: #2EE59D;
            box-shadow: 0px 15px 20px rgba(9, 132, 255, 0.3) !important;
            // color: #fff;
            transform: translateY(-3px);
          }
          #headerTest:hover{
            background-color: #DAF7A6;
            box-shadow: 0px 15px 20px rgba(9, 132, 255, 0.3) !important;
            // color: #fff;
            transform: translateY(-3px);
            border-radius: 10px
          }
          #headerTest:active{
          color: #FD001B
          }
          #headerTest:target{
          color: #24FF01
          }
        `}</style>

        <div className={classes.grow}
             onClick={() => {
               setTimeout(() => this.props.wordAction.togglePopup(false), 200);

             }}>
          {this.state.load === true ? <>{loadingComponent}</> : ""}
          <AppBar position="static" style={{backgroundColor: "#fff"}}>
            <Toolbar>
              <IconButton
                edge="start"
                className={classes.menuButton}
                color="inherit"
                aria-label="open drawer"
              >
                <div className="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                  <NavLink to="/">
                    <img src={logoMigi} alt="" width="150" height="50" left="10"
                         style={{maxWidth: "100%", margin: "auto"}}/>
                  </NavLink>
                </div>
              </IconButton>
              <div className={classes.grow2}/>
              <div className={classes.sectionDesktop}>
                <UncontrolledButtonDropdown >
                  <DropdownToggle
                    // color="primary"
                    outline
                    style={{
                      textTransform: "uppercase",
                      textColor: "#fff",
                      fontWeight: "400",
                      borderRadius: "20px",
                      border: "none"
                    }}
                    id="btnHomePage"
                    onKeyDown={(e) => {
                      if (e.keyCode === 13) {
                        // e.preventDefault("/")
                        history.push("/")
                      }
                    }}
                    onClick={(e) => {
                      history.push("/")
                    }}
                  >
                    <NavLink
                      exact to="/"
                      activeStyle={{color: "red"}}
                    >
                      <FormattedMessage id="home"  />
                    </NavLink>
                  </DropdownToggle>
                </UncontrolledButtonDropdown>
                {
                  isAuthenticated && userRole === ROLE_STUDENT &&
                  <UncontrolledButtonDropdown className="dropdown mr-1">
                    <DropdownToggle
                      // color="primary"
                      outline
                      style={{
                        textTransform: "uppercase",
                        textColor: "#fff",
                        fontWeight: "400",
                        borderRadius: "20px",
                        border: "none",
                        color: "#7367f0"
                      }}
                      id="btnHomePage"
                    >
                      <FormattedMessage id="do.practice" />
                      <ChevronDown size={15}/>
                    </DropdownToggle>
                    <DropdownMenu>
                      {
                        this.props.listPractice ?
                          this.formatListPractices(this.props.listPractice, null).map((value => this.renderMenuHeader(value, 0)))
                          : null
                      }
                    </DropdownMenu>
                  </UncontrolledButtonDropdown>
                }
                {
                  isAuthenticated && userRole === ROLE_STUDENT &&
                  <UncontrolledButtonDropdown className="dropdown mr-1">
                    <DropdownToggle
                      // color="primary"
                      outline
                      style={{
                        textTransform: "uppercase",
                        textColor: "#fff",
                        backgroundColor: "none",
                        fontWeight: "400",
                        borderRadius: "20px",
                        border: "none",
                        color: "#7367f0"
                      }}
                      id="btnHomePage"
                    >
                      <FormattedMessage id="do.test"  />
                      <ChevronDown size={15}/>
                    </DropdownToggle>
                    <DropdownMenu>

                      {/*------------start-----------------------*/}
                      {undefined !== listTests ?
                        listTests.map((test) =>
                          <UncontrolledDropdown direction={"right"} className="dropdown mr-1 d-inline-block"
                                                key={"UCD_" + "1"}>
                            <DropdownToggle
                              style={{
                                textTransform: "uppercase",
                                textColor: "#fff",
                                backgroundColor: "none",
                                fontWeight: "400",
                                borderRadius: "20px"
                              }}
                              nav
                              id="btnHomePage"
                            >
                              <span>
                                <nav>
                                  <div
                                    className="mr-1 font-small-3 uppercase"
                                    onClick={() => this.getMinitest()}
                                  >
                                    {/*tag="a" to={"/pages/full-test"}*/}
                                    <FormattedMessage id="mini.test" />
                                  </div>
                                </nav>
                              </span>
                            </DropdownToggle>
                          </UncontrolledDropdown>) : null
                      }
                      <UncontrolledDropdown direction={"right"} className="dropdown mr-1 d-inline-block"
                                            key={"UCD_" + "1"} >
                        <DropdownToggle
                          style={{
                            textTransform: "uppercase",
                            textColor: "#fff",
                            backgroundColor: "none",
                            fontWeight: "400",
                            borderRadius: "20px"
                          }}
                          nav
                          onClick={() => {
                            const {testAction, historyTestAction} = this.props;
                            const {cleanTest} = testAction;
                            cleanTest();
                            historyTestAction.cleanHistory();
                          }
                          }
                          id="btnHomePage"
                        >
                          <span>
                            <nav>
                                <NavLink tag="a" to={"/pages/doing-test"}
                                         className="mr-1 font-small-3 uppercase"
                                >
                                  <FormattedMessage id="full.test"  />
                                </NavLink>
                            </nav>
                          </span>
                        </DropdownToggle>
                      </UncontrolledDropdown>
                      {/*------------------end-----------------------*/}

                    </DropdownMenu>
                  </UncontrolledButtonDropdown>
                }

                <UncontrolledButtonDropdown >
                  <DropdownToggle
                    // color="primary"
                    outline
                    style={{
                      textTransform: "uppercase",
                      textColor: "#fff",
                      fontWeight: "400",
                      borderRadius: "20px",
                      border: "none"
                    }}
                    id="btnHomePage"
                    onClick={onRef}
                  >
                    <NavLink
                      to="/pages/login"
                      activeStyle={{color: "red"}}
                    >
                      <FormattedMessage id="introduction"  />
                    </NavLink>

                  </DropdownToggle>
                </UncontrolledButtonDropdown>
                <UncontrolledButtonDropdown >
                  <DropdownToggle
                    // color="primary"
                    outline
                    style={{
                      textTransform: "uppercase",
                      textColor: "#fff",
                      fontWeight: "400",
                      borderRadius: "20px",
                      border: "none"
                    }}
                    id="btnHomePage"
                    onClick={onRef}
                  >
                    {/*<Link to="/contact">*/}
                    <NavLink
                      to="/contact"
                      // className={classes.activeClassName}
                      activeStyle={{color: "red"}}
                    >
                      <FormattedMessage id="contact" />
                    </NavLink>
                    {/*</Link>*/}
                  </DropdownToggle>
                </UncontrolledButtonDropdown>
              </div>

              <div className={classes.grow}/>
              <div className="mr-2">
                <Multilangue checkDisabled={true}/>
              </div>
              {getSessionCookie() !== null && isAuthenticated ?
                <>
                  {
                    <ul className="nav navbar-nav float-right " style={{display: "contents !important"}}>
                      <UncontrolledDropdown
                        tag="li"
                        className="dropdown-user nav-item "
                      >
                        <DropdownToggle
                          tag="a"
                          data-toggle="dropdown"
                          className="nav-link dropdown-user-link d-flex"
                        >
                          <div style={{ marginLeft: "1em", order: "1", color:'black' }}>
                            {
                              user.login.userInfoLogin.userAvatar !== null ?
                                <img
                                  src={user.login.userInfoLogin.userAvatar}
                                  className="round"
                                  height="40"
                                  width="40"
                                  alt="avatar"
                                /> :
                                <Icon.PersonCircle size={40}/>
                            }
                          </div>
                          <div className="user-nav d-sm-flex d-none m-auto">
                        <span className="user-name text-bold-600 success"  style={{
                          marginLeft: "1em ! important",
                          whiteSpace: 'nowrap',
                          maxWidth: '150px',
                          overflow: 'hidden',
                          textOverflow: 'ellipsis'}}
                              title={user.login.userInfoLogin.userShowName}>
                          {user.login.userInfoLogin.userShowName}
                        </span>
                          </div>
                        </DropdownToggle>
                        <DropdownMenu right>
                          <DropdownItem tag="a">
                            <User size={14} className="mr-50" onClick={onRef}/>
                            <span className="align-bottom">
                          <Link to="/pages/profiles" style={{cursor: "pointer"}} onClick={()=>{
                            this.props.historyPracticesAction.changeActiveTab("1")
                            this.props.historyTestAction.updateActiveTabWhenFail("")
                          }
                          }>
                           <FormattedMessage id="management.account" />
                         </Link>
                        </span>
                          </DropdownItem>
                          <DropdownItem divider/>
                          <DropdownItem onClick={onRef}>
                            <User size={14} className="mr-50"/>
                            <span className="align-bottom">
                          <Link to="/pages/reset-password" style={{cursor: "pointer"}}>
                            <FormattedMessage id="change.pass"  />
                          </Link>
                        </span>
                          </DropdownItem>
                          {/*window.location.pathname === '/'*/}
                          {this.props.login.userRole === 'ADMIN' && getSessionCookie() !== null ?
                            <React.Fragment>
                              <DropdownItem divider/>
                              <DropdownItem onClick={onRef}>
                                <User size={14} className="mr-50"/>
                                <span className="align-bottom">
                            <a style={{cursor: "pointer"}}
                               href="/private/admin"
                            >
                              <FormattedMessage id="return.admin"  />
                            </a>
                          </span>
                              </DropdownItem>
                            </React.Fragment>
                            : null
                          }
                          <DropdownItem divider/>
                          <DropdownItem tag="a" onClick={() => this.logoutHandle()}>
                            <Power size={14} className="mr-50"/>
                            <span><FormattedMessage id="logout"  /></span>
                          </DropdownItem>
                        </DropdownMenu>
                      </UncontrolledDropdown>
                    </ul>
                  }
                </>
                :
                <>
                  {
                    <ul className="nav navbar-nav float-right round " style={{display: "contents"}}>
                      <UncontrolledDropdown
                        tag="li"
                        className="dropdown-user nav-item "
                      >
                        <DropdownToggle
                          tag="a"
                          data-toggle="dropdown"
                          className="nav-link dropdown-user-link d-flex  m-0 p-0"
                          onClick={onRef}
                        >
                          <Link to="/pages/login" style={{cursor: "pointer"}}className={classes.login}>
                             <FormattedMessage id="login" />
                          </Link>
                          <b style={{color: "#000"}}>&nbsp;/&nbsp;</b>
                        </DropdownToggle>
                      </UncontrolledDropdown>
                      <UncontrolledDropdown
                        tag="li"
                        className="dropdown-user nav-item "
                      >
                        <DropdownToggle
                          tag="a"
                          data-toggle="dropdown"
                          className="nav-link dropdown-user-link d-flex m-0 p-0"
                          onClick={onRef}
                        >
                          <Link to="/pages/register" style={{cursor: "pointer"}} className={classes.register}>
                             <FormattedMessage id="register" />
                          </Link>
                        </DropdownToggle>
                      </UncontrolledDropdown>
                    </ul>
                  }
                </>
              }
            </Toolbar>
          </AppBar>
        </div>
      </>
    );
  }
}

PrimarySearchAppBar.propTypes = {
  classes: PropTypes.object,
  open: PropTypes.bool,
  history: PropTypes.object,
  children: PropTypes.node,
  // index: PropTypes.any.isRequired,
  // value: PropTypes.any.isRequired,
  // isAuthenticated: PropTypes.bool,
  login: PropTypes.object,
  user: PropTypes.object,
  onRef: PropTypes.func
}

const mapStateToProps = (state, ownProps) => {
  return {
    open: state.sideBarReducer.open,
    login: state.auth.login,
    user: state.auth,
    listPractice: state.topicReducer.listPractices,
    listTypeTests: state.topicReducer.listTypeTests
  }
}
const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    sideBarAction: bindActionCreators(SideBarAction, dispatch),
    loginActions: bindActionCreators(loginActions, dispatch),
    useAuth0: useAuth0,
    topicActions: bindActionCreators(topicActions, dispatch),
    translationAction: bindActionCreators(translationAction, dispatch),
    testAction: bindActionCreators(testAction, dispatch),
    practiceListenMultiAction: bindActionCreators(practiceListenMultiActions, dispatch),
    historyTestAction: bindActionCreators(historyTestAction, dispatch),
    wordAction: bindActionCreators(wordAction, dispatch),
    multiListenQuestionAction: bindActionCreators(practiceListenMultiActions, dispatch),
    readingActions: bindActionCreators(readingActions, dispatch),
    historyPracticesAction: bindActionCreators(historyPracticesAction, dispatch)

  }
}
const withConnect = connect(mapStateToProps, mapDispatchToProps)


export default compose(withStyles(styles), withConnect)(PrimarySearchAppBar)
