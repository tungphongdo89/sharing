import React, {Component} from 'react';
import {withStyles} from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import {Link} from 'react-router-dom';
import styles from './styles';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import PropTypes from 'prop-types';

class SignUpPage extends Component {
  render() {
    const {classes} = this.props;
    return (
      <div className={classes.backGroundLogin}>
        <Card className={classes.root}>
          <CardContent>
            <form action="">
              <div className="text-xs-center pb-xs">
                <Typography variant="caption">
                  Đăng nhập hệ thống
                </Typography>
              </div>
              <TextField id="username" label="User name" type="text" className={classes.textField} fullWidth
                         margin="normal"/>
              <TextField id="password" label="Password" type="password" className={classes.textField} fullWidth
                         margin="normal"/>
              <TextField id="cpassword" type="password" label="Confirm password" className={classes.textField} fullWidth
                         margin="normal"/>
              <FormControlLabel control={<Checkbox value="agree"/>} label="Tôi đã đọc chính sách và đống ý điều khoản"
                                className={classes.fullWidth}/>
              <Button variant="contained" color="primary" fullWidth type="submit">
                Sign Up
              </Button>
              <div className="pt-1 text-md-center">
                <Link to="/pages/login">
                  <Button> Đã có tài khoản</Button>
                </Link>
              </div>
            </form>
          </CardContent>
        </Card>
      </div>
    );
  }
}

SignUpPage.propTypes = {
  classes: PropTypes.object
}
export default withStyles(styles)(SignUpPage);
