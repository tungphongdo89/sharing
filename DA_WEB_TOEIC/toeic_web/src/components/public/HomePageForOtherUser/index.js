import React, {PureComponent} from 'react';
import styles from "./styles"
import {compose} from "redux";
import {withStyles} from "@material-ui/core";
import {connect} from "react-redux";
import ContentHomePage from "../GeneralDirectory/Content";


class HomePageOther extends PureComponent {
  render() {
    const {classes} = this.props
    return (
      <div className="content-wrapper">
        <ContentHomePage/>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    userInfo: state.auth.login
  };
};
const withConnect = connect(mapStateToProps)
export default compose(withStyles(styles), withConnect)(HomePageOther);
