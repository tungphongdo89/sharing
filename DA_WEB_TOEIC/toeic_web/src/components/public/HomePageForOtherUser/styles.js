const styles = theme => ({
  wrapper: {
    height: "100vh",
    flexDirection: 'row',
    width: "100%"
  },
  wrapperContent: {
    width: "100%",
    padding: 10,
    position: "relative",
  },
  shiftleft: {
    // marginLeft: -240,
    position: "absolute"
  },
  dashboard: {
    backgroundColor: "#fff",
    display: 'flex',
    verticalAlign: 'bottom'
  },
  content: {
    clear: "both"
  },
})
export default styles
