const styles = theme => ({
  backGroundLogin: {
    backgroundColor: theme.palette.primary.main,
    display: "flex",
    minHeight: "100vh",
    padding: 40,
    justifyContent: "center", // căn hàng ngang
    alignItems: "center",
    flexDirection: "column",
    flex: "1 0 auto",
    textAlign: "center"
  },

})
export default styles
