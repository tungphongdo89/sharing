import React, {Component} from 'react';
import {withStyles} from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
// import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import {Link} from 'react-router-dom';
import styles from './styles';
import PropTypes from 'prop-types';
import {connect} from "react-redux";
import {bindActionCreators, compose} from "redux";
import userActions from "../../../Actions/user.action";
import {Field, reduxForm} from 'redux-form';
import validate from "../../../commons/validateCommon/syncValidate";
import warn from "../../../commons/validateCommon/warnings";
import renderTextField from "../../commons/FormHelper/renderTextField";

class LoginPage extends Component {

  handleLogin = data => {
    const {userActionsCreated} = this.props;
    const {login} = userActionsCreated;
    login(data)
  }

  render() {
    const {classes, handleSubmit, submitting, invalid} = this.props;
    return (
      <div className={classes.backGroundLogin}>
        <Card className={classes.root}>
          <CardContent>
            <form onSubmit={handleSubmit(this.handleLogin)}>
              <div className="text-xs-center pb-xs">
                <Typography variant="caption">
                  Đăng nhập hệ thống
                </Typography>
              </div>
              <Field id="username" name="username" type="text" component={renderTextField} className={classes.textField}
                     fullWidth margin="normal"/>
              <Field id="password" name="password" type="password" component={renderTextField}
                     className={classes.textField} fullWidth margin="normal"/>
              <Button variant="contained" disabled={submitting || invalid} type="submit" color="primary" fullWidth>
                Login
              </Button>
              <div className="pt-1 text-md-center">
                <Link to="/signup">
                  <Button> Đăng ký tài khoản</Button>
                </Link>
              </div>
            </form>
          </CardContent>
          {/*<CardActions>*/}
          {/*<Button size="small">Learn More</Button>*/}
          {/*</CardActions>*/}
        </Card>
      </div>
    );
  }
}

LoginPage.propTypes = {
  classes: PropTypes.object,
  handleSubmit: PropTypes.func,
  userActionsCreated: PropTypes.shape({
    login: PropTypes.func
  }),

}
const mapDispatchToProps = dispatch => {
  return {
    userActionsCreated: bindActionCreators(userActions, dispatch)
  }
}
const mapStateToProps = (state, ownProps) => {
  return {}
}
const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
)
const withReduxForm = reduxForm({
  form: "LOGIN_FORM",
  enableReinitialize: true,
  validate,
  warn
})
export default compose(withStyles(styles), withConnect, withReduxForm)(LoginPage);
