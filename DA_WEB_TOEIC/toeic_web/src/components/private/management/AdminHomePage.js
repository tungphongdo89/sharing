import React, {Component} from 'react';
import {ROLE_ADMIN, ROLE_PREVIEW, ROLE_STUDENT, ROLE_TEACHER} from "../../../commons/constants/CONSTANTS";
import {connect} from "react-redux";
import PropTypes from "prop-types";
import {RouteConfig} from "../../../Router";
import {ADMIN_ROUTES} from "../../../commons/RouterConfig/Route";
import {Redirect} from "react-router-dom";
import {history} from "../../../history"

class AdminHomePage extends Component {

  componentWillMount() {
    const {userInfo} = this.props;
    this.checkAuth(userInfo);
  }

  componentWillReceiveProps(nextProps) {
    this.checkAuth(nextProps.userInfo);
  }

  checkAuth(userInfo) {
    console.log(userInfo)
    const {location} = this.props.history;
    if (!userInfo.isAuthenticated) {
      return <Redirect to='/'/>
      // if (history) {
      //   history.push("/public/home-page")
      // }
    } else {
      if (userInfo.userRole === ROLE_STUDENT) {
        if (!location.pathname.includes("private/student")) {

          alert("Bạn không có quyền truy cập!")
          history.push("/")
          // return <Redirect to='/'/>
        }
      } else if (userInfo.userRole === ROLE_ADMIN || userInfo.userRole === ROLE_PREVIEW) {
        if (!location.pathname.includes("private/admin")) {
          alert("Bạn không có quyền truy cập!")
          // history.push("/public/home-page")
          history.push("/private/admin/")
        }
      } else if (userInfo.userRole === ROLE_TEACHER) {
        if (!location.pathname.includes("private/teacher")) {
          alert("Bạn không có quyền truy cập!")
          // history.push("/public/home-page")
          history.push("/")
        }
      }
    }
  }


  renderAdminRoute() {
    let xhtml = null;
    xhtml = ADMIN_ROUTES.map(route => {
      return <RouteConfig key={route.path} path={route.path} component={route.component} exact={route.exact}
                          fullLayout={route.fullLayout ? route.fullLayout : false}
                          publicLayout={route.publicLayout ? route.publicLayout : false}/>
    })
    return xhtml;
  }

  render() {
    const {isAdmin, isStudent, isTeacher} = this.props
    return (
      <div>
        {this.renderAdminRoute()}
      </div>
    );
  }
}

AdminHomePage.propTypes = {
  userInfo: PropTypes.object
};

const mapStateToProps = (state) => ({
  userInfo: state.auth.login
});


export default connect(mapStateToProps)(AdminHomePage);
