const styles = ({
  title:{
    textAlign: 'right',
    fontFamily: 'ArialMT, Arial, sans-serif',
    fontSize: '13px',
  },
  error: {
    color: 'red',
    fontSize: 12
  }
});

export default styles;
