import React from 'react';
import {Field, Form, Formik} from "formik";
import {Button, Col, FormGroup, Label} from "reactstrap";
import {formSchema} from './validate';
import {bindActionCreators, compose} from "redux";
import {connect} from "react-redux"
import {withStyles} from "@material-ui/core";
import styles from "./styles";
import topicAction from '../../../../redux/actions/topic/topicAction';
import Input from "reactstrap/es/Input";
import Spinner from "reactstrap/es/Spinner";
import {showMessage} from "../../../../commons/utils/convertDataForToast";
import {FormattedMessage} from "react-intl";

class FormUpdateTopic extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      typeTopicCode: props.typeTopicCode,
      partTopicCode: props.partTopicCode,
      checkChangeType: false,
      changeData: false,
      firstRender: true,
    }
  }

  componentWillUnmount() {
    this.cancel(false);
  }
  cancel = (model2) => {
    this.props.updateModel(model2);
  }

  componentDidMount() {
    let ap_param = {};
    const {topicAction} = this.props;
    const {fetchPartByTopic} = topicAction;
    ap_param.parentCode = this.props.typeTopicCode;
    fetchPartByTopic(ap_param);
    this.setState({
      partTopicCode: this.props.partTopicCode
    })
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    const {isClose, isLogging, lstPartByTopic} = prevProps;
    const {checkChangeType} = prevState;
    if (isClose && isLogging) {
      this.cancel(false)
    }
    if (checkChangeType) {
      this.setState({
        partTopicCode: lstPartByTopic[0] ? lstPartByTopic[0].code : null,
        checkChangeType: false,
      })
    }
  }

  onChangePage = () => {
    const {onChangePage} = this.props;
    onChangePage()
  }
  onChangeType = (e) => {
    let apParamDTO = {};
    const {topicAction} = this.props;
    const {fetchPartByTopic} = topicAction;
    this.setState(
      {
        typeTopicCode: e.target.value,
      }
    )
    apParamDTO.parentCode = e.target.value;
    fetchPartByTopic(apParamDTO);
    this.setState({
      checkChangeType: true,
      reload: true,
      changeData: true
    })
    setTimeout(() => {
      this.setState({
        reload: false,
      })
    }, 1000)
  }

  render() {
    const {classes, lstTypeTopic, lstPartByTopic, firstRender} = this.props;
    const {loading} = this.state;

    const optionTypeTopic = lstTypeTopic ? lstTypeTopic.map((type) => {
      return <option value={type.code}> {showMessage(type.code.toLowerCase().replaceAll("_", "."))} </option>
    }) : null
    let optionPartByTopic = lstPartByTopic !== undefined ? lstPartByTopic.map((type) => {
      return <option value={type.code}>{showMessage(type.code.toLowerCase().replaceAll("_", "."))}</option>
    }) : null
    return (
      <>
        <style>{`
        #btnHighLight:focus-within {
          border: none !important;
          outline: 1px solid gray !important;
          border-radius: 3px !important;
        }
       
      `}
        </style>
        <div>
          <Formik
            initialValues={{
              topicName: this.props.topicName
            }}
            onSubmit={(values, actions) => {
              setTimeout(() => {
                actions.setSubmitting(false);
                const {topicAction} = this.props;
                const {updateTopic} = topicAction;
                let topic = {};
                topic.topicName = values.topicName.toString().trim();
                topic.typeTopicCode = this.state.typeTopicCode;
                if (this.state.typeTopicCode === "READING_UNIT" || this.state.typeTopicCode === "LISTENING_UNIT" || this.state.typeTopicCode === "LISTENING_FILL_UNIT")
                  topic.partTopicCode = this.state.partTopicCode;
                else {
                  topic.partTopicCode = null;
                }
                topic.topicId = this.props.topicId;
                topic.code = this.props.code;
                topic.createdTime = this.props.createdTime;
//              topic.lastUpdate = this.props.lastUpdate;

                updateTopic(topic);
              }, 1000);
            }}
            validationSchema={formSchema}
          >
            {({errors, touched, values}) => (
              <Form>
                <br/>
                <br/>
                <FormGroup className="form-group d-flex">
                  <Label className={classes.title} for="required" sm={4} xl={4} lg={4} xs={4}><FormattedMessage id={"topic.name"}/> (<span
                    style={{color: "red"}}>*</span>) </Label>
                  <Col sm={7} xl={7} lg={7} xs={7}>
                    <div className="w-100">
                      <Field
                        type="text"
                        name="topicName"
                        className={`form-control ${errors.topicName &&
                        touched.topicName &&
                        "is-invalid"}`}
                        onKeyUp={(e) => {
                          if (e.target.value.toString().trim() === '') {
                            values.topicName = ''
                            e.target.value = ""
                          }
                        }}
                        onBlur={(e) => {
                          values.topicName = values.topicName.trim();
                          e.target.value = e.target.value.trim();
                        }}
                      >
                      </Field>
                      {errors.topicName && touched.topicName ? (
                        <div className={classes.error}><FormattedMessage id={errors.topicName}/></div>
                      ) : null}
                    </div>
                  </Col>
                </FormGroup>
                <FormGroup className="form-group d-flex">
                  <Label className={classes.title} for="required" sm={4} xl={4} lg={4} xs={4}><FormattedMessage id={"type.of.topic"}/> (<span
                    style={{color: "red"}}>*</span>) </Label>
                  <Col sm={7} xl={7} lg={7} xs={7}>
                    <div className="w-100 d-flex">
                      <Input
                        type="select"
                        name="typeTopicCode"
                        defaultValue={this.props.typeTopicCode}
                        onChange={this.onChangeType}
                      >
                        {optionTypeTopic}
                      </Input>
                    </div>
                  </Col>
                </FormGroup>
                {
                  this.state.typeTopicCode === "READING_UNIT" || this.state.typeTopicCode === "LISTENING_UNIT" || this.state.typeTopicCode === "LISTENING_FILL_UNIT" ?
                    (<FormGroup className="form-group d-flex">
                      <Label className={classes.title} for="required" sm={4} xl={4} lg={4} xs={4}><FormattedMessage id={"part"}/> (<span
                        style={{color: "red"}}>*</span>) </Label>
                      <Col sm={7} xl={7} lg={7} xs={7}>
                        <div className="w-100">
                          <Input
                            type="select"
                            name="partTopicCode1"
                            value={this.state.partTopicCode}
                            onChange={(e) => {
                              this.setState({
                                partTopicCode: e.target.value
                              })
                            }}
                          >
                            {optionPartByTopic}

                          </Input>
                        </div>
                      </Col>
                    </FormGroup>) : ""
                }

                <hr/>
                <FormGroup className="form-group d-flex">
                  <Col>
                    <Button id='btnHighLight' color="light" onClick={() => {
                      this.cancel(false)
                    }}><FormattedMessage id={"cancel"}/></Button>
                  </Col>
                  {
                    this.props.isLogging && <Spinner color="primary"/>
                  }
                  <Col className="text-right">
                    <Button id='btnHighLight' color="primary" disabled={errors.topicName && touched.topicName}
                            type="submit">OK</Button>
                  </Col>
                  {/*</Col>*/}
                </FormGroup>
              </Form>
            )}
          </Formik>
        </div>
      </>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    lstTypeTopic: state.topicReducer.lstTypeTopic,
    lstPartByTopic: state.topicReducer.lstPartByTopic,
    values: state.topicReducer,
    isLogging: state.topicReducer.isLoading,
    isClose: state.topicReducer.isClose
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    topicAction: bindActionCreators(topicAction, dispatch)
  }
}

export default compose(withStyles(styles), connect(mapStateToProps, mapDispatchToProps))(FormUpdateTopic)
