import React, {Component} from 'react';
import {Field, Form, Formik} from "formik";
import {Button, Col, FormGroup, Label} from "reactstrap";
import {formSchema} from './validate';
import Spinner from "reactstrap/es/Spinner";
import {compose} from "redux";
import {connect} from "react-redux"
import {withStyles} from "@material-ui/core";
import styles from "./styles";
import {bindActionCreators} from "redux";
import topicAction from '../../../../redux/actions/topic/topicAction';
import Input from "reactstrap/es/Input";
import {FormattedMessage} from "react-intl";

class FormDeleteTopic extends Component {
  state = {
    loading: false,
    topicId:this.props.topicid
  }
  cancel = (model3) =>{
    this.props.deleteModel(model3);
  }

  componentWillReceiveProps(nextProps, nextContext) {
	    const {values} = nextProps;
	    if (!values.isLogging) {
	      this.setState({
	        loading: false
	      });
	    }
	  }

  componentWillUnmount() {
    this.cancel(false);
  }

  render() {
    const {loading} = this.state;
    const {classes} = this.props;
    
    return (
      <>
      <style>{`
        #btnHighLight:focus-within {
          border: none !important;
          outline: 1px solid gray !important;
          border-radius: 3px !important;
        }
       
      `}

      </style>
      <div>
        <Formik
        initialValues={{
            topicId:this.props.topicid
          }}
          onSubmit={(values, actions) => {
            this.setState({
              loading: true
            })
            setTimeout(() => {
              actions.setSubmitting(false);
              const {topicAction} = this.props;
              const {deleteTopic} = topicAction;
              const topic = {};
              topic.topicId = this.state.topicId;

              console.log("data ",topic)
               deleteTopic(topic);
              this.cancel(false)
            }, 1000);
          }}
//          validationSchema={formSchema}
        >
          {({errors, touched}) => (
            <Form>
              <br/>
              <br/>

              <FormGroup className="form-group d-flex">
                  <Col sm={12} xl={12} lg={12} xs={12}>
	                <div className="w-100">
	                   <h4 style={{fontSize: '16px', fontWeight: 'bold',textAlign:'center'}}><FormattedMessage id={"confirm.delete.topic.1"}/></h4>
	                </div>
	              </Col>
              </FormGroup>
              <hr/>
              <FormGroup className="form-group d-flex">
                <Col>
                  <Button id ='btnHighLight' color="light" onClick={()=>{
                    this.cancel(false)
                  }}><FormattedMessage id={"cancel"}/></Button>
                </Col>
                <Col>
                  {
                    loading && <Spinner color="primary"/>
                  }
                </Col>
                <Col className="text-right">
                  <Button  id='btnHighLight' color="primary" type="submit">OK</Button>
                </Col>
              </FormGroup>
            </Form>
          )}
        </Formik>
      </div>
      </>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    values: state.topicReducer
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    topicAction: bindActionCreators(topicAction, dispatch)
  }
}

export default compose(withStyles(styles), connect(mapStateToProps, mapDispatchToProps))(FormDeleteTopic)
