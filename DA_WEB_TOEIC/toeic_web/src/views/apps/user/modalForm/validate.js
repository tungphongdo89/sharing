import * as Yup from "yup";

export const formSchema = Yup.object().shape({
  topicName: Yup.string()
    .required("topic.name.not.blank")
    .min(1, "contains.at.least.6.character")
    .max(144, "topic.name.cannot.144.char")

});
