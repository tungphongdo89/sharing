import React, {Component} from 'react';
import {Field, Form, Formik} from "formik";
import {Button, Col, FormGroup, Label} from "reactstrap";
import {formSchema} from './validate';
import Spinner from "reactstrap/es/Spinner";
import {bindActionCreators, compose} from "redux";
import {connect} from "react-redux"
import {withStyles} from "@material-ui/core";
import styles from "./styles";
import topicAction from '../../../../redux/actions/topic/topicAction';
import Input from "reactstrap/es/Input";
import {LISTENING_FILL_UNIT} from "../../../../commons/constants/CONSTANTS";
import {FormattedMessage} from "react-intl";
import {showMessage} from "../../../../commons/utils/convertDataForToast";

let optionPartByTopic;
let select = false

class FormAddTopic extends Component {

  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      reload: false,
      typeTopicCode: LISTENING_FILL_UNIT,
      partTopicCode: "PART1.1",
      disableButtonOk: false,
      isCheck: true,
      checkChangeType: false
    }
  }

  cancel = (model) => {
    this.props.updateModel(model);
  }

  componentWillUnmount() {
    this.cancel(false);
  }

  componentDidMount() {
    let ap_param = {};
    const {topicAction} = this.props;
    const {fetchPartByTopic} = topicAction;
    ap_param.parentCode = this.state.typeTopicCode;
    fetchPartByTopic(ap_param);

  }

  // componentWillReceiveProps(nextProps, nextContext) {
  //   if (!nextProps.isLogging && this.state.loading) {
  //     this.setState({
  //       loading: false
  //     })
  //     ;
  //   }
  // }

  componentDidUpdate(prevProps, prevState, snapshot) {
    const {isClose, isLogging, lstPartByTopic} = prevProps;
    const {checkChangeType} = prevState;
    if (isClose && isLogging) {
      this.cancel(false)
    }
    if (checkChangeType) {
      this.setState({
        partTopicCode: lstPartByTopic[0] ? lstPartByTopic[0].code : null,
        checkChangeType: false
      })
    }
  }

  onChangeType = (e) => {
    const {lstPartByTopic} = this.props;
    let apParamDTO = {};
    const {topicAction} = this.props;
    const {fetchPartByTopic} = topicAction;
    this.setState(
      {
        typeTopicCode: e.target.value,
        partTopicCode: ""
      }
    )
    apParamDTO.parentCode = e.target.value;
    fetchPartByTopic(apParamDTO);
    this.setState({
      checkChangeType: true,
      reload: true,
    })
    setTimeout(() => {
      this.setState({
        reload: false,
      })
    }, 1000)
    document.getElementById('resetDefaultValue').click()
  }
  optionPartByTopic = "";
  optionTypeTopic = "";

  onChangePage = () => {
    const {onChangePage} = this.props;
    onChangePage();
  }

  componentWillUpdate(nextProps, nextState, nextContext) {
    const {classes, lstTypeTopic, lstPartByTopic} = nextProps;
    this.optionTypeTopic = lstTypeTopic ? lstTypeTopic.map((type) => {
      return <option value={type.code}> {showMessage(type.code.toLowerCase().replaceAll("_", "."))} </option>
    }) : null
    this.optionPartByTopic = lstPartByTopic !== undefined ? lstPartByTopic.map((part) => {
      return <option value={part.code}>{showMessage(part.code.toLowerCase().replaceAll("_", "."))} </option>
    }) : null
  }

  render() {
    const {classes} = this.props;
    const {loading} = this.state;
    return (
      <>
        <style>{`
        #btnHighLight:focus-within {
          border: none !important;
          outline: 1px solid gray !important;
          border-radius: 3px !important;
        }
       
      `}
        </style>
        <div>

          <Formik
            initialValues={{
              topicName: ""

            }}
            onSubmit={(values, actions) => {
              setTimeout(() => {
                actions.setSubmitting(false);
                const {topicAction} = this.props;
                const {addTopic, addTopicSuccess} = topicAction;
                let topic = {};
                topic.topicName = values.topicName.toString().trim();
                topic.typeTopicCode = this.state.typeTopicCode;
                if (this.state.typeTopicCode === "READING_UNIT" || this.state.typeTopicCode === "LISTENING_UNIT" || this.state.typeTopicCode === "LISTENING_FILL_UNIT")
                // if (this.state.typeTopicCode === "LISTENING_UNIT" && this.state.partTopicCode === "PART5") {
                //   topic.partTopicCode = "PART1";
                // } else {
                  topic.partTopicCode = this.state.partTopicCode;
                // }
                else {
                  topic.partTopicCode = null;
                }

                addTopic(topic);
              }, 1000);
              // this.onChangePage();
            }

            }
            validationSchema={formSchema}
          >
            {({errors,field, touched, isValid, values}) => (
              <Form>
                <br/>
                <br/>
                <FormGroup className="form-group d-flex">
                  <Label className={classes.title} for="required"
                         sm={4} xl={4} lg={4} xs={4}><FormattedMessage id={"topic.name"}/> (<span
                    style={{color: "red"}}>*</span>) </Label>
                  <Col sm={7} xl={7} lg={7} xs={7}>
                    <div className="w-100">
                      <Field
                        type="text"
                        name="topicName"
                        className={`form-control ${errors.topicName &&
                        touched.topicName &&
                        "is-invalid"}`}
                        onMouseLeave={(e) => {
                          if (e.target.value.toString().trim() === '') {
                            values.topicName = ''
                            e.target.value = ""
                          }
                        }}
                        EscapeOutside={(e) => {
                          if (e.target.value.toString().trim() === '') {
                            values.topicName = ''
                            e.target.value = ""
                          }
                        }}
                      >
                      </Field>
                      {errors.topicName && touched.topicName ? (
                        <div className={classes.error}><FormattedMessage id={errors.topicName}/></div>
                      ) : null}
                    </div>
                  </Col>
                </FormGroup>
                <FormGroup className="form-group d-flex">
                  <Label className={classes.title} for="required" sm={4} xl={4} lg={4} xs={4}><FormattedMessage
                    id={"type.of.topic"}/> (<span
                    style={{color: "red"}}>*</span>) </Label>
                  <Col sm={7} xl={7} lg={7} xs={7}>
                    <div className="w-100 ">
                      <Input
                        type="select"
                        name="typeTopicCode"
                        onChange={this.onChangeType}
                      >
                        {this.optionTypeTopic}
                      </Input>
                    </div>

                    {this.state.reload ? (
                      <div>
                        <br/>
                        <Spinner color="primary" className="reload-spinner"/>
                      </div>
                    ) : (
                      ""
                    )}
                  </Col>
                </FormGroup>

                {
                  this.state.typeTopicCode === "READING_UNIT" || this.state.typeTopicCode === "LISTENING_UNIT" || this.state.typeTopicCode === "LISTENING_FILL_UNIT" ?
                    (<form className="form-group d-flex">
                      <Label className={classes.title} for="required" sm={4} xl={4} lg={4} xs={4}><FormattedMessage
                        id={"part"}/> (<span
                        style={{color: "red"}}>*</span>) </Label>
                      <Col sm={7} xl={7} lg={7} xs={7}>
                        <div className="w-100">
                          <Input
                            type="select"
                            name="partTopicCode"
                            value={this.state.partTopicCode}
                            onChange={(e) => {
                              this.setState({
                                partTopicCode: e.target.value
                              })

                            }}
                          >
                            {this.optionPartByTopic}
                          </Input>
                        </div>
                      </Col>
                    </form>)
                    : ""
                }


                <hr/>
                <FormGroup className="form-group d-flex">
                  <Col>
                    <Button id='btnHighLight' color="light" onClick={() => {
                      this.cancel(false)
                    }}><FormattedMessage id={"cancel"}/></Button>
                  </Col>
                  {
                    this.props.isLogging && <Spinner color="primary"/>
                  }
                  <Col className="text-right">
                    <Button id='btnHighLight' color="primary"
                            disabled={errors.topicName && touched.topicName} type="submit">OK</Button>
                  </Col>
                  {/*</Col>*/}
                </FormGroup>
              </Form>
            )}
          </Formik>
        </div>
      </>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    lstTypeTopic: state.topicReducer.lstTypeTopic,
    lstPartByTopic: state.topicReducer.lstPartByTopic,
    values: state.topicReducer,
    isLogging: state.topicReducer.isLoading,
    isClose: state.topicReducer.isClose
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    topicAction: bindActionCreators(topicAction, dispatch)
  }
}

export default compose(withStyles(styles), connect(mapStateToProps, mapDispatchToProps))(FormAddTopic)
