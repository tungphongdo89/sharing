import React from "react"
import {bindActionCreators, compose} from 'redux';
import '../../../../assets/scss/pages/gridTopic.scss'
// import {createMuiTheme, MuiThemeProvider} from "@material-ui/core/styles";
import {connect} from "react-redux"
import topicAction from '../../../../redux/actions/topic/topicAction';
import FormAddTopic from '../modalForm/FormAddTopic';
import FormUpdateTopic from '../modalForm/FormUpdateTopic';
import FormDeleteTopic from '../modalForm/FormDeleteTopic';
import MUIDataTable from "mui-datatables";
import {Edit, Search, Trash2} from "react-feather";
import OverlayTrigger from "react-bootstrap/OverlayTrigger";
import Tooltip from "react-bootstrap/Tooltip";
import ClearIcon from '@material-ui/icons/Clear';
import {toast } from 'react-toastify';


import {
  Button,
  Card,
  CardBody,
  CardHeader,
  CardTitle,
  Col,
  Collapse,
  Form,
  FormGroup,
  Input,
  Label,
  Modal,
  ModalHeader,
  Row,
  Spinner
} from "reactstrap";
import "../../../../assets/scss/plugins/tables/_agGridStyleOverride.scss";
import "../../user/style.scss";
import {getUTCTimeSeconds} from "../../../../commons/utils/DateUtils";
import PaginationIconsAndText from './ListPagination';
import {IconButton, TableCell, TableFooter, TableRow} from "@material-ui/core";
import styled from "styled-components"
import "../../../../assets/css/style-modal-create-user.css"
import {showMessage} from "../../../../commons/utils/convertDataForToast";
import {FormattedMessage} from "react-intl";

let optionPartByTopic;
let parentCode = "";


const TableCellStyle = styled(TableCell)`
  &&&{
    display: grid;
    justify-content: center;
   }
`

class GridTopic extends React.Component {
  state = {
    modal: false,
    modal2: false,
    modal3: false,
    topicName: "",
    topicType: "",
    part: "",
    typeTopicCode: "",
    partTopicCode: "",
    topicId: "",
    code: "",
    createdTime: "",
    lastUpdate: "",
    pageSize: 10,
    isVisible: true,
    reload: false,
    collapse: true,
    count: 1,
    page: 0,
    rowsPerPageOptions: [10],
    defaultColDef: {
      sortable: true
    },
    searchVal: "",
    isSearch: null,
    item: {
      topicId: null,
      typeTopicCode: null,
      paramName: null,
      topicName: null,
      createdTime: null,
      lastUpdate: null,
      partTopicCode: null,
      code: null
    },
    checkChooseTopic: true,
    oldGridOption: []
  }

  componentWillMount() {
    const {topicAction} = this.props;
    const {fetchTypeTopic, fetchPartByTopic} = topicAction;
    fetchTypeTopic({});
    fetchPartByTopic({});
    this.setState({
      checkChooseTopic: true
    })
  }

  resetIsSearch = () => {
    this.setState({
      isSearch: null
    })
  }
  resetDeleteOrAddPage = () => {
    this.setState({
      isSearch: 1
    })
  }

  componentDidMount() {
    let topic = {};
    topic.page = this.state.page + 1;
    topic.pageSize = this.state.pageSize;
    const {topicAction} = this.props;
    const {fetchTopic} = topicAction;
    fetchTopic(topic);
    const {total} = this.props;
    // this.setState({
    //   count: total
    // })

  }

  toggleModal = () => {
    this.setState(prevState => ({
      modal: !prevState.modal,
    }))
  }
  toggleModal2 = (a, b, c, d, e, f, g, h) => {
    this.setState(prevState => ({
      modal2: !prevState.modal2,
      item: {
        topicId: a,
        typeTopicCode: b,
        paramName: c,
        topicName: d,
        createdTime: e,
        lastUpdate: f,
        partTopicCode: g,
        code: h
      }

    }))
  }

  componentDidCatch(error, info) {
    // Display fallback UI
    this.setState({hasError: true});
    // You can also log the error to an error reporting service
   toast.error("Lỗi rồi")
  }

  shouldComponentUpdate(nextProps, nextState, nextContext) {
    if (nextProps.hasError) {
      return false
    }
    return true
  }

  toggleModal3 = (a) => {
    this.setState(prevState => ({
      modal3: !prevState.modal3,
      topicId: a
    }))
  }


  cancel = (model, model2, model3) => {
    const {oldGridOption} = this.state;
    this.setState({
      modal: model,
      modal2: model2,
      modal3: model3,
      oldGridOption: model || model2 || model3 ? optionPartByTopic : oldGridOption,
    })
    const topic = this.getTopicFromState();
    topic.parentCode = parentCode
    const {topicAction} = this.props;
    const {fetchPartByTopic} = topicAction;
    fetchPartByTopic(topic);
  }

  refreshCard = () => {
    this.setState({reload: true})
    setTimeout(() => {
      this.setState({
        reload: false,
        typeTopicCode: "",
        partTopicCode: "",
        topicName: ""

      })
    }, 2000)
  }

  toggleCollapse = () => {
    this.setState(state => ({collapse: !state.collapse}))
  }
  onEntered = () => {
    this.setState({status: "Opened"})
  }
  onEntering = () => {
    this.setState({status: "Opening..."})
  }

  onEntered = () => {
    this.setState({status: "Opened"})
  }
  onExiting = () => {
    this.setState({status: "Closing..."})
  }
  onExited = () => {
    this.setState({status: "Closed"})
  }
  removeCard = () => {
    this.setState({isVisible: false})
  }


  changePage = (page) => {
    const {pageSize} = this.state;
    this.setState({
      page: page - 1
    })
    if (page === 1) {
      this.setState({
        isSearch: 1,
      })
    }
    const topic = this.getTopicFromState();
    topic.page = page
    topic.pageSize = pageSize
    const {topicAction} = this.props;
    const {fetchTopic, fetchPartByTopic} = topicAction;
    fetchTopic(topic);
    topic.parentCode = parentCode
    fetchPartByTopic(topic);
  };

  getTopicFromState = () => {
    let topic = {};
    topic.topicName = this.state.topicName;
    topic.typeTopicCode = this.state.typeTopicCode;
    topic.partTopicCode = this.state.partTopicCode;
    return topic;
  }

  componentWillUpdate(nextProps, nextState, nextContext) {
    optionPartByTopic = nextProps.lstPartByTopic !== undefined ? nextProps.lstPartByTopic.map((type) => {
      if (type.parentCode === "LISTENING_FILL_UNIT" && nextState.checkChooseTopic) {
        return <option value={type.code}>
          {/*{type.name + " (" + showMessage("listening.fill.unit") + ")"}*/}
          {showMessage(type.code.toLowerCase().replaceAll("_", ".")) + " (" + showMessage("listening.fill.unit") + ")"}
        </option>
      } else if (type.parentCode === "LISTENING_UNIT" && nextState.checkChooseTopic) {
        return <option value={type.code}>
          {/*{type.name + " (" + showMessage("listening.unit") + ")"}*/}
          {showMessage(type.code.toLowerCase().replaceAll("_", ".")) + " (" + showMessage("listening.unit") + ")"}
        </option>
      } else {
        return <option value={type.code}>
          {/*{type.name}*/}
          {showMessage(type.code.toLowerCase().replaceAll("_", "."))}
        </option>
      }
    }) : null

  }

  UNSAFE_componentWillReceiveProps(nextProps, nextContext) {
    const {values, total} = nextProps;
    if (!values.isLogging) {
      this.setState({
        loading: false,
        count: total
      });
    }
  }

  getCurrentPage = (currentPage) => {
    this.changePage(currentPage);
  }

  render() {
    debugger
    const {count} = this.state
    const {listTopic, isLogging, lstTypeTopic, total, lstPartByTopic} = this.props;
    const optionTypeTopic = lstTypeTopic ? lstTypeTopic.map((type) => {
      return <option value={type.code}>
        {/*{type.name}*/}
        {showMessage(type.code.toLowerCase().replaceAll("_", "."))}
      </option>
    }) : null
    const options = {
      filter: false,
      filterType: "dropdown",
      selectableRowsHideCheckboxes: true,
      selectableRows: "none",
      responsive: "standard",
      print: false,
      download: false,
      search: false,
      viewColumns: false,
      serverSide: true,
      customFooter: () => {
        return (
          <TableFooter>
            <TableRow>
              <TableCellStyle colSpan={6}>
                <PaginationIconsAndText
                  totalItem={count}
                  onChangPage={this.getCurrentPage}
                  checkSearch={this.state.isSearch}
                  resetIsSearch={this.resetIsSearch}
                />
              </TableCellStyle>
            </TableRow>
          </TableFooter>
        );
      }
    };
    const columns = [
      {
        name: "sl",
        label: showMessage("index"),
        options: {
          filter: true,
          sort: false,

          customHeadLabelRender: () => {
            return <div style={{fontWeight: 'bold'}}><FormattedMessage id="index"/></div>;
          },
          customBodyRender: (index) => {
            return (<div className="text-left" autoFocus={false}>
              {index}
            </div>)
          }
        }
      },
      {
        name: "topicId",
        label: "topicId",
        options: {
          filter: true,
          sort: false,
          display: false,
        }
      },

      {
        name: "typeTopicCode",
        label: showMessage("type.of.topic"),
        options: {
          filter: true,
          sort: false,
          customHeadLabelRender: () => {
            return <div style={{fontWeight: 'bold'}}><FormattedMessage id="type.of.topic"/></div>;
          },
          customBodyRender: (data) => {
            return data === 'TRANS_ENG_TO_VIET' ? (<div><FormattedMessage id="e.v.trans"/></div>) :
              data === 'LISTENING_UNIT' ? (<div><FormattedMessage id="listening.unit"/></div>) :
                data === 'READING_UNIT' ? (<div><FormattedMessage id="reading.unit"/></div>) :
                  data === 'TRANS_VIET_TO_ENG' ? (<div><FormattedMessage id="v.e.trans"/></div>) :
                    data === 'LISTENING_FILL_UNIT' ? (<div><FormattedMessage id="listening.fill.unit"/></div>) : null
          }
        }
      },
      {
        name: "partTopicCode",
        label: showMessage("part"),
        options: {
          filter: true,
          sort: false,
          customHeadLabelRender: () => {
            return <div style={{fontWeight: 'bold'}}><FormattedMessage id="part"/></div>;
          },
          customBodyRender: (data) => {
            return <div>
              {
                data ? <FormattedMessage id={data.toLowerCase().replaceAll("_", ".")}/> : ''
              }

            </div>
          }
        }
      },
      {
        name: "topicName",
        label: showMessage("topic.name"),
        options: {
          filter: true,
          sort: false,
          customHeadLabelRender: () => {
            return <div style={{fontWeight: 'bold'}}><FormattedMessage id="topic.name"/></div>;
          },
          customBodyRender: (data) => {
            return <div autoFocus={false} title={data.replace(/(<([^>]+)>)/ig, "")}
                        dangerouslySetInnerHTML={{__html: `${data}`}}
                        style={{
                          textOverflow: 'ellipsis',
                          whiteSpace: 'nowrap',
                          overflow: 'hidden',
                          display: 'inline-block',
                          maxWidth: '150px',
                          // width: '200px',
                          maxHeight: '80px',
                          width: '100%',
                        }}/>
          }
        }
      }, {
        name: "createdTime",
        label: showMessage("date.created"),
        options: {
          filter: true,
          sort: false,
          customHeadLabelRender: () => {
            return <div style={{fontWeight: 'bold'}}><FormattedMessage id="date.created"/></div>;
          },
          customBodyRender: (data) => {
            return (<div>
              {getUTCTimeSeconds(new Date(data))}
            </div>)
          }
        }
      }, {
        name: "lastUpdate",
        label: showMessage("last.update"),
        options: {
          filter: true,
          sort: false,
          customHeadLabelRender: () => {
            return <div style={{fontWeight: 'bold'}}><FormattedMessage id="last.update"/></div>;
          },
          customBodyRender: (data) => {
            return (<div>
              {getUTCTimeSeconds(new Date(data))}
            </div>)
          }
        }
      }, {
        name: "partTopicCode",
        label: <FormattedMessage id={"part"}/>,
        options: {
          filter: true,
          sort: false,
          display: false,
          customHeadLabelRender: () => {
            return <div style={{fontWeight: 'bold'}}>Mã <FormattedMessage id={"part"}/></div>;
          },
        }
      }, {
        name: "code",
        label: "mã",
        options: {
          filter: true,
          sort: false,
          display: false,
          customHeadLabelRender: () => {
            return <div style={{fontWeight: 'bold'}}>Mã</div>;
          },
        }
      }, {
        name: "",
        label: showMessage("action"),
        options: {
          filter: true,
          sort: false,
          customHeadLabelRender: () => {
            return <div style={{fontWeight: 'bold', marginLeft: '13px'}}><FormattedMessage id="action"/></div>;
          },
          customBodyRender: (data, dataRow) => {
            return (
              <>
                <style>{`
              #btnHighLight2 {
                border: none;
                background-color: white;
                padding: 0px;
                text-align: center;
              }
              #btnHighLight2:focus-within {
                border: 0.5px solid black !important;
                background-color: rgba !important;

                box-shadow: 2px 2px 5px gray;
              }
            `}</style>

                <div>
                  <OverlayTrigger placement="top"
                                  overlay={props => (<Tooltip {...props}><FormattedMessage id="edit"/></Tooltip>)}>
                    <button id="btnHighLight" style={{border: 'none', backgroundColor: 'rgb(0 0 0 / 0%)'}}
                            onClick={() => this.toggleModal2(dataRow.rowData[1], dataRow.rowData[2], dataRow.rowData[3], dataRow.rowData[4],
                              dataRow.rowData[5], dataRow.rowData[6], dataRow.rowData[7], dataRow.rowData[8])
                            }
                    >
                      <Edit size={15} className=" mr-1 fonticon-wrap"
                            style={{cursor: "pointer", color: "#2c00ff", marginLeft: '14px'}}

                      />
                    </button>
                  </OverlayTrigger>


                  <OverlayTrigger placement="top"
                                  overlay={props => (<Tooltip {...props}><FormattedMessage id="remove"/></Tooltip>)}>
                    <button id="btnHighLight" style={{border: 'none', backgroundColor: 'rgb(0 0 0 / 0%)'}}>
                      <Trash2
                        size={15} className="mr-1 fonticon-wrap"
                        style={{cursor: "pointer", color: "red", marginLeft: '14px'}}
                        onClick={() =>
                          this.toggleModal3(dataRow.rowData[1])
                        }/>
                    </button>
                  </OverlayTrigger>
                </div>
              </>
            )
          }
        }
      }
    ];
    const loadingComponent = (
      <div style={{
        position: 'absolute',
        zIndex: 110,
        top: 0,
        left: 0,
        width: '100%',
        height: '100%',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        background: 'rgba(255,255,255,0.8)'
      }}>
        <Spinner color="primary" className="reload-spinner"/>
      </div>
    );
    let listTopicNew = [];

    if (this.props.listTopic) {
      this.props.listTopic.map((item, index) => {
        listTopicNew.push({sl: (this.state.pageSize) * ((this.state.page + 1) - 1) + index + 1, ...item});
      });
    }
    return (
      <>
        <style>{`
          #btnHighLight:focus-within {
            border: none !important;
            outline: 1px solid gray !important;
            border-radius: 3px !important;
          }
          #userName:focus-within {
            text-shadow: 1px 1px #1890ff;
          }
        `}

        </style>
        <Row className="app-user-list">
          <Col sm="12">
            <Card
              // className={classnames("card-action card-reload", {
              //   "d-none": this.state.isVisible === false,
              //   "card-collapsed": this.state.status === "Closed",
              //   closing: this.state.status === "Closing...",
              //   opening: this.state.status === "Opening...",
              //   refreshing: this.state.reload
              // })}
            >
              <CardHeader>
                <CardTitle><FormattedMessage id="list.of.topic"/></CardTitle>
                {/* <div className="actions">
                  <ChevronDown
                    className="collapse-icon mr-50"
                    size={15}
                    onClick={this.toggleCollapse}
                  />
                  <RotateCw
                    className="mr-50"
                    size={15}
                    onClick={() => {
                      this.refreshCard()
                    }}
                  />
                  <X size={15} onClick={this.removeCard} />
                </div> */}
              </CardHeader>
              <Collapse
                isOpen={this.state.collapse}
                onExited={this.onExited}
                onEntered={this.onEntered}
                onExiting={this.onExiting}
                onEntering={this.onEntering}
              >
                <CardBody>
                  {this.state.reload ? (
                    <Spinner color="primary" className="reload-spinner"/>
                  ) : (
                    ""
                  )}
                  <Row>
                    <Col style={{paddingTop: '20px'}} lg="2" md="2" sm="2">
                      <FormGroup className="mb-0">
                        <Button
                          id="btnHighLight"
                          color="primary"
                          onClick={this.toggleModal}
                        > <FormattedMessage id={"add"}/>
                        </Button>
                      </FormGroup>

                      {/*-----------------------------------------Modal Form-------------------------------*/}
                      <Modal
                        isOpen={this.state.modal}
                        toggle={this.toggleModal}
                        className="modal-dialog-centered"
                      >
                        <Row className="flex flex-space-between flex-middle  bg-primary "
                             style={{width: '100%', marginLeft: '0.2px'}}>
                          <Col xs={11}>
                            <ModalHeader style={{background: '#7367f0', textAlign: 'center'}}
                            >
                              <h3 style={{color: 'white'}}><FormattedMessage id="add.new.topic"/></h3>
                            </ModalHeader>
                          </Col>
                          <Col xs={1} style={{paddingLeft: '0px', paddingRight: '0px'}}>
                            <IconButton onClick={this.toggleModal}>
                              <ClearIcon/>
                            </IconButton>
                          </Col>
                        </Row>
                        <FormAddTopic onChangePage={() => this.getCurrentPage(1)} updateModel={(model) => {
                          this.cancel(model)
                        }}/>

                      </Modal>


                      <Modal
                        isOpen={this.state.modal2}
                        toggle={this.toggleModal2}
                        className="modal-dialog-centered"
                      >
                        <Row className="flex flex-space-between flex-middle  bg-primary "
                             style={{width: '100%', marginLeft: '0.2px'}}>
                          <Col xs={11}>
                            <ModalHeader style={{background: '#7367f0', textAlign: 'center'}}
                            >
                              <h3 style={{color: 'white'}}><FormattedMessage id="update.topic"/></h3>
                            </ModalHeader>
                          </Col>
                          <Col xs={1} style={{paddingLeft: '0px', paddingRight: '0px'}}>
                            <IconButton onClick={this.toggleModal2}>
                              <ClearIcon/>
                            </IconButton>
                          </Col>
                        </Row>
                        <FormUpdateTopic topicName={this.state.item.topicName}
                                         typeTopicCode={this.state.item.typeTopicCode}
                                         topicId={this.state.item.topicId} code={this.state.item.code}
                                         createdTime={this.state.item.createdTime}
                                         lastUpdate={this.state.item.lastUpdate}
                                         onChangePage={() => this.getCurrentPage(1)}
                                         partTopicCode={this.state.item.partTopicCode} updateModel={(model2) => {
                          this.cancel(model2)
                        }}/>
                      </Modal>
                      <Modal
                        isOpen={this.state.modal3}
                        toggle={this.toggleModal3}
                        className="modal-dialog-centered"
                      >
                        <Row className="flex flex-space-between flex-middle  bg-primary "
                             style={{width: '100%', marginLeft: '0.2px'}}>
                          <Col xs={11}>
                            <ModalHeader style={{background: '#7367f0'}}
                            >
                              <h3 style={{color: 'white'}}><FormattedMessage id={"confirm.delete.topic"}/></h3>
                            </ModalHeader>
                          </Col>
                          <Col xs={1} style={{paddingLeft: '0px', paddingRight: '0px'}}>
                            <IconButton onClick={this.toggleModal3}>
                              <ClearIcon/>
                            </IconButton>
                          </Col>
                        </Row>
                        <FormDeleteTopic topicid={this.state.topicId} deleteModel={(model3) => {
                          this.cancel(model3)
                        }}/>
                      </Modal>
                    </Col>
                    <Col lg="3" md="3" sm="3">
                      <div className="search-bar">

                        <Form style={{paddingTop: '20px'}} onSubmit={(e) => {
                          e.preventDefault();
                          let topic = this.getTopicFromState();
                          topic.page = 1;
                          topic.pageSize = 10;
                          const {topicAction} = this.props;
                          const {fetchTopic} = topicAction;
                          fetchTopic(topic);
                          this.getCurrentPage(1)
                        }}>
                          <FormGroup className="position-relative has-icon-left">
                            <Input
                              autoFocus={true}

                              type="text"
                              // className="round"
                              placeholder={"" + showMessage("enter.topic.to.search")}
                              onChange={e => this.setState({topicName: e.target.value})}
                              onEnter={e => e.preventDefault()}
                            />
                            <div className="form-control-position px-1">
                              <Search size={15}/>
                            </div>
                          </FormGroup>
                        </Form>
                      </div>
                    </Col>
                    <Col>
                      <FormGroup className="mb-0">
                        <Label for="department"><FormattedMessage id={"type.of.topic"}/> </Label>
                        <Input
                          type="select"
                          name="typeTopicCode"
                          autoload={false}
                          cache={false}
                          onChange={e => {
                            let apParamDTO = {};
                            const {topicAction} = this.props;
                            const {fetchPartByTopic} = topicAction;
                            let booleanChoose = true;
                            if (e.target.value !== "" && e.target.value !== undefined) {
                              booleanChoose = false
                            }
                            this.setState(
                              {
                                typeTopicCode: e.target.value,
                                checkChooseTopic: booleanChoose
                              }
                            )
                            apParamDTO.parentCode = parentCode = e.target.value;
                            fetchPartByTopic(apParamDTO);
                            this.setState({reload: true})
                            setTimeout(() => {
                              this.setState({
                                reload: false,
                                partTopicCode: ""
                              })
                            }, 1000)
                            document.getElementById('resetDefaultValue').click()
                          }}
                        >
                          <option defaultChecked value="">{showMessage("all")}</option>
                          {optionTypeTopic}
                        </Input>
                      </FormGroup>
                    </Col>
                    <Col>
                      <form className="mb-0">
                        <Label for="department"><FormattedMessage id={"part"}/></Label>
                        <Input style={{minWidth: '168px'}}
                               type="select"
                               id="partTopicCodeForCheck"
                               name="partTopicCode"
                               disabled={lstPartByTopic ? lstPartByTopic.length > 0 ? false : true : true}
                               onChange={(e) => {
                                 this.setState(
                                   {
                                     partTopicCode: e.target.value
                                   }
                                 )
                               }}
                        >
                          <option defaultChecked value="" selected="selected">{showMessage("all")}</option>
                          {optionPartByTopic}
                        </Input>
                        <input type="reset" hidden value="reset" id="resetDefaultValue"/>
                      </form>
                    </Col>
                    <Col style={{paddingTop: '20px'}} lg="2" md="2" sm="2">
                      <Button type="submit" color="primary"
                              id="btnHighLight"
                              onClick={(e) => {

                                e.preventDefault();
                                debugger
                                let topic = this.getTopicFromState();
                                topic.page = 1;
                                topic.pageSize = 10;
                                const {topicAction} = this.props;
                                const {fetchTopic} = topicAction;
                                fetchTopic(topic);
                                this.getCurrentPage(1)
                              }}><FormattedMessage id={"search"}/></Button>
                    </Col>
                  </Row>
                </CardBody>
              </Collapse>
            </Card>
          </Col>
          <Col sm="12">
            <div style={{position: 'relative'}}>
              {isLogging && loadingComponent}
              <MUIDataTable
                columns={columns}
                data={listTopicNew}
                options={options}
              />
            </div>
          </Col>
        </Row>
      </>
    )
  }
}

const mapStateToProps = state => {
  return {
    listTopic: state.topicReducer.listTopic,
    values: state.topicReducer,
    total: state.topicReducer.totalTopic,
    isLogging: state.topicReducer.isLoading,
    lstTypeTopic: state.topicReducer.lstTypeTopic,
    lstPartByTopic: state.topicReducer.lstPartByTopic,

  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    topicAction: bindActionCreators(topicAction, dispatch)
  }

}

export default compose(connect(mapStateToProps, mapDispatchToProps))(GridTopic)
