import React from "react"
import {IconButton, TableCell, TableFooter, TableRow, withStyles} from "@material-ui/core";
import styles from "./styles";
import * as Icon from "react-bootstrap-icons"
import styled from "styled-components"
import {bindActionCreators, compose} from 'redux';
import {connect} from "react-redux"
import {
  Button,
  Card,
  CardBody,
  CardHeader,
  CardTitle,
  Col,
  Collapse,
  FormGroup,
  Input,
  Label,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Row,
  Spinner
} from "reactstrap"
import {Eye, Search} from "react-feather"
import classnames from "classnames"
import "../../../../assets/scss/plugins/tables/_agGridStyleOverride.scss"
import "../../../../assets/scss/pages/users.scss"
import userAction from "../../../../redux/actions/users/userActions";
import {Field, Form, Formik} from "formik"
import * as Yup from "yup"
import {history} from "../../../../history";
import "../../../../assets/css/style-modal-create-user.css"
import MUIDataTable from "mui-datatables";
import {Link} from "react-router-dom";
import PaginationIconsAndText from './ListPagination';
import OverlayTrigger from "react-bootstrap/OverlayTrigger";
import Tooltip from "react-bootstrap/Tooltip";
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import ClearIcon from '@material-ui/icons/Clear';
import {showMessage} from "../../../../commons/utils/convertDataForToast";
import {FormattedMessage} from "react-intl";

const FormGroupStyle = styled(FormGroup)`
  position: relative;
`

const IconDown = styled(Icon.ChevronDown)`
  position: absolute;
  top : ${props => props.checkError && props.checkError !== null ? "28px" : "12px"};
  right : 10px
`

const TableCellStyle = styled(TableCell)`
  &&&{
    display: grid;
    justify-content: center;
   }
`

const formSchema = Yup.object().shape({
  currentPoint: Yup.string()
    .matches(/^[0-9]+$/, "current.score.from.1.to.990")
    .min(1, "current.score.more.1.char")
    .max(3, "enter.up.to.3.char")
    .test("currentPoint", "current.score.from.1.to.990", value => value ? value <= 990 && value > 0 ? true : false : true),
  targetPoint: Yup.string()
    .matches(/^[0-9]+$/, "current.target.from.1.to.990")
    .min(1, "current.target.more.1.char")
    .max(3, "enter.up.to.3.char")
    .test("targetPoint", "current.target.from.1.to.990", value => value ? value <= 990 && value > 0 ? true : false : true),

});

class UsersList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      page: 1,
      modal: false,
      isLoading: false,
      pageSize: 10,
      isVisible: true,
      reload: false,
      collapse: true,
      status: "2",
      role: "",
      search: showMessage("enter.user.information"),
      searchValue: '',
      roleID: "0",
      loading: false,
      paymentStatus: "2",
      defaultColDef: {
        sortable: true
      },
      searchVal: "",
      paginationPageSize: 10,
      cacheBlockSize: 10,
      count: 1,
      rowsPerPageOptions: [10],
      labelField: "name",
      valueField: "id",
      isSearch: null,
      currentPoint: "",
      target: "",
    }
    this.inputFocus = React.createRef();
  }

  componentDidMount() {
    let user = {};
    user.page = this.state.page;
    user.pageSize = this.state.pageSize;
    const {userAction} = this.props;
    const {fetchUser} = userAction;
    fetchUser(user);
  }

  changePage = (page) => {
    const user = this.getUserFromState();
    this.setState({
      page: page
    })
    user.page = page
    const {userAction} = this.props;
    const {fetchUser} = userAction;
    fetchUser(user);
  };

  onChangPageReceive = (pageNumber) => {
    this.setState({
      page: pageNumber
    })
    if (pageNumber === 1) {
      this.setState({
        isSearch: 1,
      })
    }
    const searchPropertiesBE = {};
    searchPropertiesBE.keySearch = this.state.searchValue.toString().trim()
    searchPropertiesBE.currentScore = parseInt(this.state.currentPoint)
    searchPropertiesBE.target = this.state.target
    searchPropertiesBE.roleID = parseInt(this.state.roleID);
    searchPropertiesBE.paymentStatus = parseInt(this.state.paymentStatus);
    searchPropertiesBE.status = parseInt(this.state.status);
    searchPropertiesBE.pageSize = this.state.pageSize
    searchPropertiesBE.page = pageNumber
    const {userAction} = this.props;
    const {fetchUser} = userAction;
    fetchUser(searchPropertiesBE);
  }

  componentWillReceiveProps(preProps) {
    // props thay đổi nên sẽ bị ẩn ngay sau khi click
    // Xác định chính xác props nao thay đổi thì mới cho ẩn
    if (preProps.isLoadingInvitedUser !== this.props.isLoadingInvitedUser) {
      this.setState({
        isLoading: false
      })
    } else {
      this.setState({
        isLoading: false,
        modal: false
      })
    }
  }


  refreshCard = () => {
    this.setState({reload: true})
    setTimeout(() => {
      this.setState({
        reload: false,
        search: showMessage("enter.user.information"),
        roleID: "0",
        paymentStatus: "2",
        status: "2"
      })
    }, 500)
  }

  toggleCollapse = () => {
    this.setState(state => ({collapse: !this.state.collapse}))
  }
  onEntered = () => {
    this.setState({status: "Opened"})
  }
  onEntering = () => {
    this.setState({status: "Opening..."})
  }
  onEntered = () => {
    this.setState({status: "Opened"})
  }
  onExiting = () => {
    this.setState({status: "Closing..."})
  }
  onExited = () => {
    this.setState({status: "Closed"})
  }
  removeCard = () => {
    this.setState({isVisible: false})
  }
  toggleModal = () => {
    this.setState({
      modal: !this.state.modal,
      isLoading: false
    })
    if (this.state.modal === true) {
      if (this.inputFocus.current) {
        this.inputFocus.current.focus()
      }
    }
  }
  showLoading = () => {
    this.setState({
      isLoading: true
    })
  }

  UNSAFE_componentWillReceiveProps(nextProps, nextContext) {
    const {total} = nextProps;
    this.setState({
      count: total
    })
  }


  resetIsSearch = () => {
    this.setState({
      isSearch: null
    })
  }

  render() {
    const {count, page, rowsPerPageOptions} = this.state;
    const renderTooltip = props => (
      <Tooltip {...props}><FormattedMessage id={"Detail"}/></Tooltip>
    );


    const columns = [
      {
        name: 'sl',
        label: showMessage("index"),
        options: {
          sort: false,
          filter: false,
          customHeadLabelRender: () => {
            return <div style={{fontWeight: 'bold'}}><FormattedMessage id="index"/></div>;
          },
          customBodyRender: (index) => {
            return (<div className="text-left" autoFocus={false}>
              {index}
            </div>)
          }
        }
      },
      {
        name: "userCode",
        label: showMessage("user.code"),
        options: {
          sort: false,
          filter: false,
          customHeadLabelRender: () => {
            return <div style={{fontWeight: 'bold'}}><FormattedMessage id="user.code"/></div>;
          },
          customBodyRender: (data, dataIndex) => {
            return (<Link className="text-center"
                          style={{textAlign: 'right'}}
                          to="/pages/users/userDetail"
                          id="userName"
                          onClick={(e) => {
                            const {userAction} = this.props;
                            const {fetchUserDetail} = userAction;
                            fetchUserDetail(dataList[dataIndex.rowIndex]);
                            history.push("/pages/users/userDetail")
                          }
                          }>{data}</Link>)
          }
        }
      },
      {
        name: "userName",
        label: showMessage("email.login"),
        options: {
          filter: true,
          sort: false,
          customHeadLabelRender: () => {
            return <div style={{fontWeight: 'bold'}}><FormattedMessage id="email.login"/></div>;
          },
          customBodyRender: (data) => {
            return (<div style={{
              whiteSpace: 'nowrap',
              width: '150px',
              overflow: 'hidden',
              textOverflow: 'ellipsis'
            }} title={data}>{data}</div>)
          }
        }
      },
      {
        name: "currentScore",
        label: showMessage("current.score"),
        options: {
          filter: true,
          sort: false,
          customHeadLabelRender: () => {
            return <div style={{fontWeight: 'bold'}}><FormattedMessage id="current.score"/></div>;
          },
          customBodyRender: (data) => {
            return (<div style={{textAlign: 'right'}}>{data}</div>)
          }
        }
      },
      {
        name: "target",
        label: showMessage("target.score"),
        options: {
          filter: true,
          sort: false,
          customHeadLabelRender: () => {
            return <div style={{fontWeight: 'bold'}}><FormattedMessage id="target.score"/></div>;
          },
          customBodyRender: (data) => {
            return (<div style={{textAlign: 'right'}}>{data}</div>)
          }
        }
      },
      {
        name: "userAvatar",
        label: "Avatar",
        options: {
          filter: true,
          sort: false,
          customHeadLabelRender: () => {
            return <div style={{fontWeight: 'bold'}}>Avatar</div>;
          },
          customBodyRender: (data) => {
            return (<div className="d-flex align-items-center cursor-pointer" style={{marginLeft: '6%'}}>
              {
                data ?
                  <img
                    className="rounded-circle mr-50"
                    src={data}
                    alt="user avatar"
                    height="30"
                    width="30"

                  />
                  : <AccountCircleIcon/>
              }

            </div>)
          }
        }
      },
      {
        name: "codeRole",
        label: showMessage("user.type"),
        options: {
          filter: true,
          sort: false,
          customHeadLabelRender: () => {
            return <div style={{fontWeight: 'bold'}}><FormattedMessage id="user.type"/></div>;
          },
          customBodyRender: (data) => {
            return data === 'ADMIN' ? (<div className="text-left"><FormattedMessage id="admin"/></div>) :
              data === 'STUDENT' ? (<div className="text-left"><FormattedMessage id="student"/></div>) :
                data === 'PREVIEW' ? (<div className="text-left">Preview</div>) : null
          }

        }
      },
      {
        name: "status",
        label: <FormattedMessage id="account.status"/>,
        options: {
          filter: true,
          sort: false,
          customHeadLabelRender: () => {
            return <div style={{fontWeight: 'bold'}}><FormattedMessage id="account.status"/></div>;
          },
          customBodyRender: (data) => {
            return data === 1 ? (
                <div className="badge badge-pill badge-success" style={{color: 'white'}}><FormattedMessage id="active"/>
                </div>) :
              data === 0 ? (
                  <div className="badge badge-pill badge-light" style={{color: '#000'}}><FormattedMessage id="inactive"/>
                  </div>) :
                data === -1 ? (
                  <div className="badge badge-pill badge-warning" style={{color: 'white'}}><FormattedMessage
                    id="wait.for.confirmation"/></div>) : null

          }
        }
      },
      {
        name: "paymentStatus",
        label: showMessage("payment.status"),
        options: {
          filter: true,
          sort: false,
          customHeadLabelRender: () => {
            return <div style={{fontWeight: 'bold'}}><FormattedMessage id="payment.status"/></div>;
          },
          customBodyRender: (data) => {
            return data === 1 ? (<div className="badge badge-pill badge-success"><FormattedMessage id="paid"/></div>) :
              data === 0 ? (
                <div className="badge badge-pill badge-warning"><FormattedMessage id="wait.for.pay"/></div>) : null
          }
        }
      },
      {
        name: "status",
        label: showMessage("action"),
        options: {
          filter: true,
          sort: false,
          customHeadLabelRender: () => {
            return <div style={{fontWeight: 'bold'}}><FormattedMessage id="action"/></div>;
          },
          customBodyRender: (data, dataIndex) => {
            return (<div style={{cursor: "pointer", textAlign: 'center'}}>
              <OverlayTrigger placement="top" overlay={renderTooltip}>
                <button id="btnHighLight"
                        style={{border: 'none', backgroundColor: 'rgb(0 0 0 / 0%)', textAlign: 'center'}}
                        onClick={(e) => {
                          console.log(dataIndex)
                          const {userAction} = this.props;
                          const {fetchUserDetail} = userAction;
                          fetchUserDetail(dataList[dataIndex.rowIndex]);
                          history.push("/pages/users/userDetail")
                        }
                        }>
                  <Eye
                    className="mr-50"
                    size={15}
                    cursor="pointer"

                  />
                </button>
              </OverlayTrigger>
            </div>)
          }
        }
      }
    ];
    const options = {
      filter: false,
      filterType: "dropdown",
      responsive: "standard",
      selectableRowsHideCheckboxes: true,
      selectableRows: "none",
      print: false,
      download: false,
      search: false,
      viewColumns: false,
      serverSide: true,
      textLabels: {
        body: {
          noMatch: this.props.isLoading ?
            <p/> :
            showMessage("not.found.data"),
        },
      },
      customFooter: () => {
        return (
          <TableFooter>
            <TableRow>
              <TableCellStyle colSpan={6}>
                <PaginationIconsAndText
                  totalItem={count}
                  onChangPage={this.onChangPageReceive}
                  checkSearch={this.state.isSearch}
                  resetIsSearch={this.resetIsSearch}

                />
              </TableCellStyle>
            </TableRow>
          </TableFooter>
        );
      }
    };
    const {classes, dataList, loading} = this.props;
    const loadingComponent = (
      <div style={{
        position: 'absolute',
        zIndex: 110,
        top: 0,
        left: 0,
        width: '100%',
        height: '100%',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        background: 'rgba(255,255,255,0.8)'
      }}>
        <Spinner color="primary" className="reload-spinner"/>
      </div>
    );

    let newData = [];

    if (this.props.dataList) {
      this.props.dataList.map((item, index) => {
        newData.push({sl: (this.state.pageSize) * (this.state.page - 1) + index + 1, ...item});
      });
    }
    return (
      <>
        <style>{`
            #btnHighLight:focus-within {
              border: none !important;
              outline: 1px solid gray !important;
              border-radius: 3px !important;
            }
            #userName:focus-within {
              text-shadow: 1px 1px #1890ff;
            }
            #roleID:focus-within {
              outline: 1px solid #4839eb !important;
            }
            #paymentStatus:focus-within {
              outline: 1px solid #4839eb !important;
            }
            #status:focus-within {
              outline: 1px solid #4839eb !important;
            }
          `}

        </style>
        <Row className="app-user-list">
          <Col sm="12">
            <Card
              className={classnames("card-action card-reload", {
                "d-none": this.state.isVisible === false,
                "card-collapsed": this.state.status === "Closed",
                closing: this.state.status === "Closing...",
                opening: this.state.status === "Opening...",
                refreshing: this.state.reload
              })}
            >
              <CardHeader>
                <CardTitle><h2><FormattedMessage id="user.list"/></h2></CardTitle>
              </CardHeader>
              <Collapse
                isOpen={this.state.collapse}
                onExited={this.onExited}
                onEntered={this.onEntered}
                onExiting={this.onExiting}
                onEntering={this.onEntering}
              >
                <CardBody>
                  {this.state.reload ? (
                    <Spinner color="primary" className="reload-spinner"/>
                  ) : (
                    ""
                  )}

                  <Formik
                    initialValues={{
                      currentPoint: "",
                      targetPoint: "",
                    }}
                    onSubmit={(values) => {
                      this.setState({
                        currentPoint: values.currentPoint,
                        target: values.targetPoint
                      })
                      this.onChangPageReceive(1)
                    }}

                    validationSchema={formSchema}
                  >
                    {({errors, touched}) => (
                      <Form>
                        <FormGroup className="form-group d-flex w-100" style={{marginBottom: '0px'}}>
                          <Col xs={2} sm={2} md={2} lg={2}>
                            <FormGroup className="mb-0" style={{marginTop: '4%'}}>
                              <Button className={classes.buttonAdd}
                                      onClick={this.toggleModal}
                                      id="btnHighLight"
                                      style={{"color": "white", borderRadius: '5px', marginTop: '2%'}}
                                      variant="contained" color="primary"
                              > <FormattedMessage id="add"/>
                              </Button>
                            </FormGroup>

                            {/*-----------------------------------------Modal Form-------------------------------*/}

                            <Modal autoFocus={false}
                                   isOpen={this.state.modal}
                                   toggle={this.toggleModal}
                                   className="modal-dialog-centered"
                            >
                              <Row className="flex flex-space-between flex-middle  bg-primary "
                                   style={{width: '100%', marginLeft: '0.15px'}}>
                                <Col xs={11}>
                                  <ModalHeader style={{background: '#7367f0', textAlign: 'center'}}>
                                    <h3 style={{color: '#FFFFFF'}}><FormattedMessage id="add.new.user"/></h3>
                                  </ModalHeader>
                                </Col>
                                <Col xs={1} style={{paddingLeft: '0px', paddingRight: '0px'}}>
                                  <IconButton onClick={this.toggleModal} style={{marginLeft: '-4%'}}>
                                    <ClearIcon/>
                                  </IconButton>
                                </Col>
                              </Row>
                              <Formik
                                initialValues={{
                                  userName: "",
                                  roleIdOfUserInvited: 1
                                }}

                                validationSchema={Yup.object().shape({
                                  userName: Yup.string()
                                    .required("email.can.not.be.empty")
                                    .min(6, "email.more.6.char")
                                    .max(45,"email.longer.45.char")
                                    .matches(/^(([^<>()\[\]\\.,^%$#&*='\/?;:\s@!"ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựýỳỵỷỹ]+(\.[^<>!()\[\]\\.,^%$#&*='\/?;:\s@"ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳýỵỷỹ]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z0-9/-_.^-]+\.)+[a-zA-Z]{2,}))$/,
                                      "invalid.email"),
                                })}

                                onSubmit={(values) => {
                                  this.props.userAction.createUser({
                                    userName: values.userName.toString().trim(),
                                    roleIdOfUserInvited: values.roleIdOfUserInvited
                                  })
                                }}
                              >
                                {({errors, isSubmitting, touched}) => (
                                  <Form>
                                    <ModalBody>
                                      <FormGroup>
                                        <Label>Email (<span style={{color: "red"}}>*</span>)</Label>
                                        <Field
                                          autoFocus={true}
                                          type="email"
                                          name="userName"
                                          className={`form-control ${errors.userName &&
                                          touched.userName &&
                                          "is-invalid"}`}
                                          placeholder="Email Address"
                                        />
                                        {errors.userName && touched.userName ? <label style={{
                                          color: "red",
                                          width: "100%",
                                          textAlign: "left",
                                          marginLeft: '34%',
                                          fontWeight: '300',
                                          fontSize: '12px'
                                        }}><strong><FormattedMessage id={errors.userName}/></strong></label> : null}
                                      </FormGroup>
                                      <FormGroupStyle>
                                        <Label><FormattedMessage id="user.type"/> (<span style={{color: "red"}}>*</span>)</Label>
                                        <IconDown checkError={errors.userName}/>
                                        <Field component="select" name="roleIdOfUserInvited" disabled="disabled"
                                               className="form-control">
                                          <option value={1}>{showMessage("admin")}</option>
                                          <option value={2}>{showMessage("teacher")}</option>
                                          <option value={3}>{showMessage("student")}</option>
                                        </Field>
                                      </FormGroupStyle>
                                    </ModalBody>
                                    <ModalFooter>
                                      <button type="button" className="btn btn-secondary btn-modal"
                                              style={{"color": "black"}}
                                              onClick={this.toggleModal}><FormattedMessage id="cancel"/>
                                      </button>
                                      <div className="spin">
                                        {this.state.isLoading && <Spinner color="primary"/>}
                                      </div>
                                      {
                                        errors.userName ?
                                          <button type="submit" className="btn btn-primary btn-modal"
                                                  disabled="disabled">OK</button> :
                                          <button type="submit" className="btn btn-primary btn-modal" id="btnHighLight"
                                                  style={{"color": "white"}}
                                                  onClick={this.showLoading}>OK</button>
                                      }
                                    </ModalFooter>
                                  </Form>
                                )}
                              </Formik>
                            </Modal>

                            {/*---------------------*/}

                          </Col>
                          <Col sm={8} md={8} lg={8} xs={8}>
                            <span>&nbsp;</span>
                            <FormGroup className="position-relative has-icon-left">
                              <Input
                                autoFocus={true}
                                type="search"
                                name="searchValue"
                                id="searchValue"
                                placeholder={this.state.search}
                                onChange={e => {
                                  this.setState(
                                    {
                                      searchValue: e.target.value
                                    }
                                  )
                                }}
                              />
                              <div className="form-control-position px-1">
                                <Search size={15}/>
                              </div>
                            </FormGroup>
                          </Col>
                          <Col sm={2} md={2} lg={2} xs={2}>
                            <FormGroup className={classes.search} style={{marginTop: '4%'}}>
                              <Button type="submit" className={classes.buttonSearch}
                                      variant="contained" color="primary" id="btnHighLight"
                                      style={{"color": "white", borderRadius: '5px'}}
                              ><FormattedMessage id={"search"}/></Button>
                            </FormGroup>
                          </Col>
                        </FormGroup>
                        <FormGroup className="form-group d-flex">
                          <Col xs={2}>

                          </Col>
                          <Col sm={4} md={4} lg={4} xs={4}>
                            <span><FormattedMessage id="current.score"/></span>
                            <Field
                              type="text"
                              name="currentPoint"
                              id="currentPoint"
                              className={`form-control ${errors.currentPoint &&
                              touched.currentPoint &&
                              "is-invalid"}`}
                            />
                            {errors.currentPoint && touched.currentPoint ? (
                              <div className={classes.error}><FormattedMessage id={errors.currentPoint}/></div>
                            ) : null}
                          </Col>
                          <Col sm={4} md={4} lg={4} xs={4}>
                            <FormGroup className="mb-0">
                              <Label className={classes.fieldLabel} for="department"><FormattedMessage
                                id="target.score"/></Label>
                              <Field
                                type="text"
                                name="targetPoint"
                                id="targetPoint"
                                className={`form-control ${errors.targetPoint &&
                                touched.targetPoint &&
                                "is-invalid"}`}
                              />
                              {errors.targetPoint && touched.targetPoint ? (
                                <div className={classes.error}><FormattedMessage id={errors.targetPoint}/></div>
                              ) : null}
                            </FormGroup>
                          </Col>
                        </FormGroup>
                        <FormGroup className="form-group d-flex">

                          <Col xs={2}>

                          </Col>
                          <Col sm={2} md={2} lg={2} xs={2}>
                            <FormGroup className="mb-0">
                              <Label className={classes.fieldLabel} for="status"><FormattedMessage
                                id="user.type"/></Label>
                              <select name="roleID" id="roleID"
                                      style={{
                                        border: '1px solid #d9d9d9',
                                        borderRadius: '5px',
                                        height: 'calc(1.25 * 1em + 1.4rem + 1px)',
                                        width: '100%'
                                      }}
                                      onChange={e => {
                                        this.setState(
                                          {
                                            roleID: e.target.value
                                          }
                                        )
                                      }}>
                                <option value="0">{showMessage("all")}</option>
                                <option value="3">{showMessage("student")}</option>
                                <option value="1">{showMessage("admin")}</option>
                              </select>
                            </FormGroup>
                          </Col>
                          <Col sm={2} md={2} lg={2} xs={2} style={{marginLeft: '8%'}}>
                            <FormGroup className="mb-0">
                              <div>
                                <Label className={classes.fieldLabel} for="status"><FormattedMessage
                                  id="payment.status"/></Label>
                                <select name="paymentStatus" id="paymentStatus"
                                        style={{
                                          border: '1px solid #d9d9d9',
                                          borderRadius: '5px',
                                          height: 'calc(1.25 * 1em + 1.4rem + 1px)',
                                          width: '100%'
                                        }}
                                        onChange={e => {
                                          this.setState(
                                            {
                                              paymentStatus: e.target.value
                                            }
                                          )
                                        }}>
                                  <option value="2">{showMessage("all")}</option>
                                  <option value="0">{showMessage("wait.for.pay")}</option>
                                  <option value="1">{showMessage("paid")}</option>
                                </select>
                              </div>
                            </FormGroup>
                          </Col>
                          <Col sm={2} md={2} lg={2} xs={2} style={{marginLeft: '9%'}}>
                            <FormGroup className="mb-0">
                              <div className="w-100">
                                <Label className={classes.fieldLabel} for="status"><FormattedMessage
                                  id="account.status"/></Label>
                                <select name="status" id="status"
                                        style={{
                                          border: '1px solid #d9d9d9',
                                          borderRadius: '5px',
                                          height: 'calc(1.25 * 1em + 1.4rem + 1px)',
                                          width: '100%'
                                        }}
                                        onChange={e => {
                                          this.setState(
                                            {
                                              status: e.target.value
                                            }
                                          )
                                        }}>
                                  <option value="2">{showMessage("all")}</option>
                                  <option value="-1">{showMessage("wait.for.confirmation")}</option>
                                  <option value="1">{showMessage("active")}</option>
                                  <option value="0">{showMessage("inactive")}</option>
                                </select>
                              </div>
                            </FormGroup>
                          </Col>
                        </FormGroup>
                      </Form>
                    )}
                  </Formik>
                </CardBody>
              </Collapse>
            </Card>
          </Col>
          <Col sm="12">
            <div style={{position: 'relative'}}>
              {loading && loadingComponent}
              <MUIDataTable
                data={newData}
                columns={columns}
                options={options}

              />
            </div>
          </Col>
        </Row>
      </>
    )
  }
}

const mapStateToProps = state => {
  return {
    dataList: state.userReducer.lstUser,
    total: state.userReducer.total,
    loading: state.userReducer.loading,
    isLoadingInvitedUser: state.userReducer.isLoadingInvitedUser
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    userAction: bindActionCreators(userAction, dispatch)

  }
}


export default compose(withStyles(styles), connect(mapStateToProps, mapDispatchToProps))(UsersList);
