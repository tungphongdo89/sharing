const styles = theme => ({
  fieldLabel : {
    fontSize:'13.5px ! important',
  },
  buttonAdd:{
    paddingTop: '1em ! important',
    marginTop: '1em ! important',
    paddingLeft:'2em ! important',
  },
  buttonSearch:{
    paddingTop: '1em ! important',
    marginTop: '1em ! important',
  },
  search:{
  },
  error:{
    color:'red',
  }
});
export default styles;
