import React, {Component} from 'react';
import {Button} from "reactstrap"
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import loginActions from "../../../redux/actions/auth/loginActions";
import {useAuth0} from "../../../authServices/auth0/auth0Service";
import {getSessionCookie} from "../../../commons/configCookie";
import {history} from "../../../history";
import {toast} from "react-toastify";
import {showMessage} from "../../../commons/utils/convertDataForToast";
import {FormattedMessage} from "react-intl";
import {ArrowLeft} from "react-feather";

const logoutUser = (props) => {
  const {loginAction} = props
  const {logoutWithJWT} = loginAction
  logoutWithJWT()
}

const goToPreviousPath = (props) => {
  if (getSessionCookie()) {
    history.goBack()
  } else {
    debugger
    toast.error(showMessage("expired.session"));
    history.push("/");
    return logoutUser(props)
  }
}

class ButtonBack extends Component {
  render() {
    return (
      <Button  color="primary" className="btn-icon right" color="primary" left onClick={() => goToPreviousPath(this.props)} title={showMessage("back")}>
        <ArrowLeft/>
      </Button>
    );
  }
}

const mapStateToProps = state => {
  return {
    user: state.auth
  }
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    loginAction: bindActionCreators(loginActions, dispatch),
    useAuth0: useAuth0
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ButtonBack)
