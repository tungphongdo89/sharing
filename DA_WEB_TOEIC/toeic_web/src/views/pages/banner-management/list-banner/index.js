import React from "react"
import {bindActionCreators, compose} from 'redux';
import {connect} from "react-redux"
import {IconButton, TableCell, withStyles} from "@material-ui/core";
import {Field, Form, Formik} from "formik"
import styles from './styles'
import PaginationComponent from './pagingtion';
// import FormDeleteTest from '../../../formDeleteTest';
import DeleteBanner from '../modal/deleteBanner';
import {Table} from 'antd';

import {
  Button,
  Card,
  CardBody,
  CardHeader,
  CardTitle,
  Col,
  Collapse,
  FormGroup,
  Label,
  Modal,
  ModalHeader,
  Nav,
  NavItem,
  NavLink,
  Row,
  Spinner,
  TabContent,
  TabPane
} from "reactstrap"

import {Edit, Search, Trash2} from "react-feather"
import "../../../../assets/scss/plugins/tables/_agGridStyleOverride.scss"
import "../../../../assets/scss/pages/users.scss"
import {getUTCTimeSeconds} from "../../../../commons/utils/DateUtils";
import OverlayTrigger from "react-bootstrap/OverlayTrigger";
import Tooltip from "react-bootstrap/Tooltip";
import styled from "styled-components"
import BannerForm from '../modal/bannerForm';
import bannerAction from "../../../../redux/actions/banner/bannerAction";
import {MenuOutlined} from '@ant-design/icons';
import {sortableContainer, sortableElement, sortableHandle} from 'react-sortable-hoc';
import arrayMove from 'array-move';
import Switch from '@material-ui/core/Switch';
import 'antd/dist/antd.css';
import {formSchema} from './validate';
import ClearIcon from '@material-ui/icons/Clear';
import 'date-fns';
import {showMessage} from "../../../../commons/utils/convertDataForToast";
import {FormattedMessage} from "react-intl";

const TableCellStyle = styled(TableCell)`
  &&&{
    display: grid;
    justify-content: center;
   }
`

const SortableItem = sortableElement(props => {
  return (<tr {...props}  />)
})
const SortableContainer = sortableContainer(props => <tbody {...props} />);
const DragHandle = sortableHandle(() => (
  <MenuOutlined style={{cursor: 'pointer', color: '#999', marginRight: '12px', fontSize: '20px'}}/>
));

let errDate1 = "";
let errDate2 = "";

function validateDateFrom(date, value) {
  if (value.createDateFrom === "") {
    if (document.getElementById("createDateFromBanner") !== null && document.getElementById("createDateFromBanner").validationMessage !== "") {
      errDate1 = showMessage("date.not.correct.format");
    } else {
      errDate1 = ""
    }
  } else {
    if (!/^((2000|2400|2800|(19|2[0-9](0[48]|[2468][048]|[13579][26])))-02-29)$|^(((19|2[0-9])[0-9]{2})-02-(0[1-9]|1[0-9]|2[0-8]))$|^(((19|2[0-9])[0-9]{2})-(0[13578]|10|12)-(0[1-9]|[12][0-9]|3[01]))$|^(((19|2[0-9])[0-9]{2})-(0[469]|11)-(0[1-9]|[12][0-9]|30))$/i.test(value.createDateFrom)) {
      errDate1 = showMessage("date.not.correct.format");
    } else if (value.createDateFrom && value.createDateTo) {
      if (Date.parse(value.createDateFrom) > Date.parse(value.createDateTo)) {
        errDate1 = showMessage("compare.time");
      } else {
        errDate1 = ""
      }
    } else {
      errDate1 = ""
    }
  }
}

function validateDateTo(value) {
  if (value.createDateTo === "") {
    if (document.getElementById("createDateToBanner") !== null && document.getElementById("createDateToBanner").validationMessage !== "") {
      errDate2 = showMessage("date.not.correct.format");
    } else {
      errDate2 = ""
    }
  } else {
    if (!/^((2000|2400|2800|(19|2[0-9](0[48]|[2468][048]|[13579][26])))-02-29)$|^(((19|2[0-9])[0-9]{2})-02-(0[1-9]|1[0-9]|2[0-8]))$|^(((19|2[0-9])[0-9]{2})-(0[13578]|10|12)-(0[1-9]|[12][0-9]|3[01]))$|^(((19|2[0-9])[0-9]{2})-(0[469]|11)-(0[1-9]|[12][0-9]|30))$/i.test(value.createDateTo)) {
      errDate2 = showMessage("date.not.correct.format");
    } else if (value.createDateFrom && value.createDateTo) {
      if (Date.parse(value.createDateFrom) > Date.parse(value.createDateTo)) {
        errDate1 = showMessage("compare.time");
        ;
        errDate2 = ""
      } else {
        errDate1 = ""
      }
    } else {
      errDate2 = ""
    }
  }
}

class GridTest extends React.Component {
  state = {
    page: 1,
    count: 1,
    searchValue: "",
    currentPage: 0,
    pageSize: 10,
    isVisible: true,
    reload: false,
    collapse: true,
    status: "Opened",
    categoryType: "2",
    typeTopic: "",
    part: "",
    codeTopic: "",
    levelCode: "",
    statusTest: "2",
    createFrom: null,
    createTo: null,
    search: showMessage("enter.information.to.search"),
    rowsPerPageOptions: [10],
    bannerId: 0,
    typeCode: 0,
    isSearch: null,
    active: "1",
    modalDelete: false,
    modal: false,
    bannerName: "",
    pathImage: "",
    postLink: "",
    orderBanner: 0,
    bannerStatus: 0,
    isEdit: false,
    createdTime: '',
    dataSource: [],
    srcImage: '',
    checkZoom: false,
  }

  toggle = tab => {
    if (this.state.active !== tab) {
      this.setState({active: tab})
    }
  }


  UNSAFE_componentWillReceiveProps(nextProps, nextContext) {
    let total = nextProps.values.lstBannerActive;
    if (total) {
      this.setState({
        count: total,
        dataSource: total
      });
    }
  }


  componentDidMount() {
    let banner = {};
    banner.page = this.state.page;
    banner.pageSize = this.state.pageSize;
    const {bannerActions} = this.props;
    const {showBannerActive, getListBannerNotActive} = bannerActions;
    showBannerActive();
    getListBannerNotActive(banner);
    const {values} = this.props;
    const {total} = values;
    this.setState({
      count: total
    })
  }

  toggleModal = (bannerId, bannerName, pathImage, postLink, status, orderBanner, createdTime) => {
    this.props.bannerActions.updateStatusPopup()
    this.setState(prevState => ({
      bannerId: bannerId,
      bannerName: bannerName,
      pathImage: pathImage,
      postLink: postLink,
      bannerStatus: status,
      orderBanner: orderBanner,
      createdTime: createdTime,
      modal: !prevState.modal
    }))
  }

  toggleModalDelete = (bannerId) => {
    this.setState(prevState => ({
      bannerId: bannerId,
      modalDelete: !prevState.modalDelete
    }))
  }

  toggleModalZoom = (data) => {
    this.setState(prevState => ({
      checkZoom: !prevState.checkZoom,
      srcImage: data
    }))
  }

  toggleCollapse = () => {
    this.setState(state => ({collapse: !state.collapse}))
  }

  //-----------------------------------------------------------------------

  onChangPageReceive = (pageNumber) => {
    this.setState({
      page: pageNumber
    })
    if (pageNumber === 1) {
      this.setState({
        isSearch: 1,
      })
    }
    console.log(typeof this.state.createFrom);
    const searchPropertiesBE = {};
    searchPropertiesBE.keySearch = this.state.searchValue.toString().trim()
    searchPropertiesBE.createFrom = this.state.createFrom
    searchPropertiesBE.createTo = this.state.createTo
    searchPropertiesBE.updatedDate = this.state.createTo
    searchPropertiesBE.pageSize = this.state.pageSize
    searchPropertiesBE.page = pageNumber
    this.props.bannerActions.getListBannerNotActive(searchPropertiesBE)
  }


  resetIsSearch = () => {
    this.setState({
      isSearch: null
    })
  }

  //-----------------------------------------------------------------------
  deleteBanner = (data) => {
    this.setState({
      modalDelete: data
    })
  }

  cancel = (model) => {
    this.setState({
      modal: model,
    })
  }


  onSortEnd = ({oldIndex, newIndex}) => {
    if (oldIndex !== newIndex) {
      const newData = arrayMove([].concat(this.state.dataSource), oldIndex, newIndex).filter(el => !!el);
      this.setState({dataSource: newData});
      const {bannerActions} = this.props;
      const {updateOrderBanner} = bannerActions;
      updateOrderBanner(this.state.dataSource)
    }
  };

  DraggableBodyRow = ({className, style, ...restProps}) => {
    // function findIndex base on Table rowKey props and should always be a right array index
    const index = this.state.dataSource.findIndex(x => x.orderBanner === restProps['data-row-key']);
    return <SortableItem index={index} {...restProps}/>;
  };

  DraggableContainer = props => (
    <SortableContainer
      useDragHandle
      disableAutoscroll
      helperClass="row-dragging"
      onSortEnd={this.onSortEnd}
      {...props}
    />
  );
  updateOrderBannerValue = (value) => {
    this.setState({
      orderBanner: value
    })
  }

  render() {
    const {bannerId} = this.state;
    const count = this.props.values.total;
    const {classes} = this.props;
    const {lstBannerActive, lstBannerNotActive} = this.props;

    const options = {
      filter: false,
      filterType: 'checkbox',
      selectableRowsHideCheckboxes: true,
      responsive: "standard",
      selectableRows: "none",
      print: false,
      download: false,
      search: false,
      viewColumns: false,
      serverSide: true,
      textLabels: {
        body: {
          noMatch: this.props.loading ?
            <p/> :
            <FormattedMessage id={"data.not.found"}/>,
        },
      },
    };
    const columns = [
      {
        dataIndex: 'sl',
        title: showMessage("index"),
        className: 'drag-visible',
        width: window.screen.width * 0.05,
        render: data => {
          return (
            <>
              {
                this.state.active === '1' ?
                  <Row>
                    <Col xs={2}><DragHandle/></Col>
                    <Col xs={6}>
                      {data}
                    </Col>
                  </Row>
                  :
                  <>{data}</>
              }
            </>
          )
        }
      },
      {
        dataIndex: "bannerName",
        title: showMessage("banner.name"),
        className: 'drag-visible',
        width: window.screen.width * 0.15,
        render: data => (<div style={{
          whiteSpace: 'nowrap',
          width: '150px',
          overflow: 'hidden',
          textOverflow: 'ellipsis'
        }} title={data}>{data}</div>)
      },
      {
        dataIndex: "pathImage",
        title: showMessage("image"),
        width: window.screen.width * 0.07,
        className: 'drag-visible',
        render: data => (
          <img src={data} style={{width: '100px', height: '90px', cursor: 'pointer'}}
               className={classes.image} onClick={() => this.toggleModalZoom(data)}
          ></img>
        )
      },
      {
        dataIndex: "postLink",
        title: "Link",
        className: 'drag-visible',
        width: window.screen.width * 0.1,
        render: data => (<a
          style={{
            textOverflow: 'ellipsis',
            whiteSpace: 'nowrap',
            width: '150px',
            overflow: 'hidden',
            display: 'inline-block'
          }} title={data} href={data} id="postLink">{data}</a>)
      },
      {
        dataIndex: "status",
        title: showMessage("status"),
        className: 'drag-visible',
        width: window.screen.width * 0.1,
        render: data => {
          if (data === 1) {
            return (<Switch id="switch"
                            checked={true}
                            color="primary" autoFocus={false}/>)
          } else {
            return (<Switch id="switch"
                            checked={false}
                            color="primary" autoFocus={false}/>)
          }
        },
      },
      {
        dataIndex: "createdTime",
        title: showMessage("date.created"),
        className: 'drag-visible',
        width: window.screen.width * 0.11,
        render: data => <>{getUTCTimeSeconds(new Date(data))}</>
      },
      {
        dataIndex: "lastUpdate",
        title: showMessage("last.update"),
        className: 'drag-visible',
        width: window.screen.width * 0.11,
        render: data => <>{getUTCTimeSeconds(new Date(data))}</>
      },
      {
        dataIndex: "",
        title: showMessage("action"),
        className: 'drag-visible',
        render: (record) => {
          return (<div style={{cursor: 'pointer', textAlign: 'left'}}>
              <OverlayTrigger placement="top"
                              overlay={props => (<Tooltip {...props}><FormattedMessage id={"edit"}/></Tooltip>)}>
                <button id="btnHighLight" style={{
                  border: 'none',
                  backgroundColor: 'rgb(0 0 0 / 0%)',
                  marginBottom: '2px',
                  marginRight: '3px'
                }}
                        onClick={() => {
                          this.setState({
                            isEdit: true
                          })
                          if (this.state.active === '2') {
                            this.toggleModal(record.bannerId, record.bannerName, record.pathImage, record.postLink, record.status, record.orderBanner);
                          } else {
                            this.toggleModal(record.bannerId, record.bannerName, record.pathImage, record.postLink, record.status, record.orderBanner);
                          }
                          // this.props.bannerActions.updateStatusPopup(true)
                        }}
                >
                  <Edit size={16} className=" mr-1 fonticon-wrap" style={{cursor: "pointer", color: "#2c00ff"}}/>
                </button>
              </OverlayTrigger>
              <OverlayTrigger placement="top" overlay={props => (<Tooltip {...props}><FormattedMessage id={"delete"}/></Tooltip>)}>
                <button id="btnHighLight" style={{border: 'none', backgroundColor: 'rgb(0 0 0 / 0%)'}}
                        onClick={() => {
                          if (this.state.active === '2') {
                            this.toggleModalDelete(record.bannerId);
                          } else {
                            this.toggleModalDelete(record.bannerId);
                          }
                        }}
                >
                  <Trash2
                    size={16} className="mr-1 fonticon-wrap" style={{cursor: "pointer", color: "red"}}
                  />
                </button>
              </OverlayTrigger>
            </div>
          )
        }
      }
    ];

    if (this.state.page === 0) {
      this.state.page = 1;
    }
    let newDataActive = [];
    let newDataNotActive = [];

    if (this.state.dataSource) {
      this.state.dataSource.map((item, index) => {
        newDataActive.push({sl: index + 1, ...item});
      });
    }

    if (lstBannerNotActive && lstBannerNotActive.data) {
      lstBannerNotActive.data.map((item, index) => {
        newDataNotActive.push({sl: (this.state.pageSize) * (this.state.page - 1) + index + 1, ...item});
      });
    }

    const loadingComponent = (
      <div style={{
        position: 'absolute',
        zIndex: 110,
        top: 0,
        left: 0,
        width: '100%',
        height: '100%',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        background: 'rgba(255,255,255,0.8)'
      }}>
        <Spinner color="primary" className="reload-spinner"/>
      </div>
    );


    return (
      <>
        <style>{`
          #btnHighLight:focus-within {
            border: none !important;
            outline: 1px solid gray !important;
          }
          #switch:hover {
            background-color: white;
          }
          #switch:focus-within {
            background-color: white;
          }
          #postLink:focus-within {
            text-shadow: 1px 1px #1890ff;
          }
        `}

        </style>
        <Row>
          <Col sm="12">
            <Card>
              <CardHeader>
                <CardTitle><h2><FormattedMessage id={"list.of.banners"}/></h2></CardTitle>
              </CardHeader>
              <Collapse
                isOpen={this.state.collapse}
                onExited={this.onExited}
                onEntered={this.onEntered}
                onExiting={this.onExiting}
                onEntering={this.onEntering}
              >
                <CardBody>
                  {this.state.reload ? (
                    <Spinner color="primary" className="reload-spinner"/>
                  ) : (
                    ""
                  )}
                  <Formik
                    initialValues={{
                      createDateFrom: '',
                      createDateTo: '',
                      searchValue: '',
                    }}
                    onSubmit={(values, actions) => {
                      this.setState({
                        loading: true,
                        createFrom: values.createDateFrom,
                        createTo: values.createDateTo,
                        searchValue: values.searchValue
                      })
                      this.onChangPageReceive(1)
                    }}
                  >
                    {({errors, touched, values}) => (
                      <Form
                        // onKeyDown={(e)=> {
                        // debugger
                        // if (e.keyCode===27){
                        //   this.props.bannerActions.updateStatusPopup();
                        // }}}
                      >
                        <FormGroup className="form-group d-flex row">
                          <Col >
                            <FormGroup style={{paddingTop: '0.5em'}} className="mb-0">
                              <Button className={classes.buttonAdd}
                                      color="primary"
                                      onClick={() => {
                                        this.setState({
                                          isEdit: false
                                        })
                                        this.toggleModal()
                                        // this.props.bannerActions.updateStatusPopup(true)
                                      }}

                              > <FormattedMessage id={"add"}/>
                              </Button>
                            </FormGroup>
                            {
                              this.props.isLoading ?
                                <Modal
                                  isOpen={this.props.isLoading}
                                  toggle={this.toggleModal}
                                  className="modal-dialog-centered"
                                  onKeyPress={(e) => {
                                    if (e.keyCode === 27) {
                                      this.props.bannerActions.updateStatusPopup();
                                    }
                                  }}
                                >
                                  <Row className="flex flex-space-between flex-middle  bg-primary "
                                       style={{width: '100%', marginLeft: '0.2px'}}>
                                    <Col xs={11}>
                                      <ModalHeader className='text-centesr' style={{background: '#7367f0'}}>
                                        <h3
                                          style={{color: 'white'}}>{this.state.isEdit ?
                                          <FormattedMessage id={"update.banner"}/> :
                                          <FormattedMessage id={"add.new.banner"}/>}</h3>
                                      </ModalHeader>
                                    </Col>
                                    <Col xs={1} style={{paddingLeft: '0px', paddingRight: '0px'}}>
                                      <IconButton onClick={() => {
                                        this.props.bannerActions.updateStatusPopup()
                                      }}>
                                        <ClearIcon/>
                                      </IconButton>
                                    </Col>
                                  </Row>
                                  <BannerForm bannerId={this.state.bannerId}
                                              bannerName={this.state.bannerName}
                                              pathImage={this.state.pathImage}
                                              postLink={this.state.postLink}
                                              status={this.state.bannerStatus}
                                              orderBanner={this.state.orderBanner}
                                              updateOrderBannerValue={this.updateOrderBannerValue}
                                              onChangPageReceive={this.onChangPageReceive}
                                              isEdit={this.state.isEdit}
                                  />
                                </Modal>
                                : null
                            }

                            <Modal
                              isOpen={this.state.modalDelete}
                              toggle={this.toggleModalDelete}
                              className="modal-dialog-centered"
                              onKeyPress={(e) => {
                                if (e.keyCode === 27) {
                                  this.toggleModalDelete();
                                }
                              }}
                            >
                              <Row className="flex flex-space-between flex-middle  bg-primary "
                                   style={{width: '100%', marginLeft: '0.2px'}}>
                                <Col xs={11}>
                                  <ModalHeader style={{background: '#7367f0', textAlign: 'center'}}>
                                    <h3 style={{color: '#FFFFFF'}}><FormattedMessage id={"remove.banner"}/></h3>
                                  </ModalHeader>
                                </Col>
                                <Col xs={1} style={{paddingLeft: '0px', paddingRight: '0px'}}>
                                  <IconButton onClick={this.toggleModalDelete}>
                                    <ClearIcon/>
                                  </IconButton>
                                </Col>
                              </Row>
                              <DeleteBanner bannerId={bannerId}
                                            onChangPageReceive={() => this.onChangPageReceive(1)}
                                            deleteBanner={(model) => {
                                              this.deleteBanner(model)
                                            }}
                              />
                            </Modal>

                            <Modal
                              isOpen={this.state.checkZoom}
                              toggle={this.toggleModalZoom}
                              className="modal-dialog-centered"
                            >
                              <img src={this.state.srcImage} style={{width: '100%', cursor: 'pointer'}}
                                   className={classes.image}
                              ></img>
                            </Modal>
                          </Col>
                          {this.state.active === '2' ?
                            <>
                              <Col >
                                <span>&nbsp;</span>
                                <FormGroup className="position-relative has-icon-left">
                                  <div className="w-100">
                                    <Field
                                      autoFocus={true}
                                      type="text"
                                      name="searchValue"
                                      style={{height: '45px'}}
                                      placeholder={this.state.search}
                                      className={`form-control ${errors.searchValue &&
                                      touched.searchValue &&
                                      "is-invalid"}`}

                                    />
                                    <div className="form-control-position px-1">
                                      <Search size={15}/>
                                    </div>
                                  </div>
                                </FormGroup>
                              </Col>
                              <Col >
                                <FormGroup className="mb-0">
                                  <div className="w-100">
                                    <Label className={classes.fieldLabel} for="createDateFrom"><FormattedMessage id={"date.created"}/></Label>
                                    <Field
                                      id="createDateFromBanner"
                                      type="date"
                                      name="createDateFrom"
                                      style={{height: '45px'}}
                                      className={`form-control ${errors.createDateFrom &&
                                      touched.createDateFrom &&
                                      "is-invalid"}`}
                                      timezone="Asia/Singapore"
                                      validate={validateDateFrom(this, values)}

                                    />
                                    {errDate1 ? (
                                      <div className={classes.error}>{errDate1}</div>
                                    ) : null}

                                  </div>
                                </FormGroup>
                              </Col>
                              <Col >
                                <FormGroup className="mb-0">
                                  <div className="w-100">
                                    <Label className={classes.fieldLabel} for="createDateTo"><FormattedMessage id={"to.date"}/></Label>
                                    <Field
                                      type="date"
                                      id="createDateToBanner"
                                      name="createDateTo"
                                      style={{height: '45px'}}
                                      minDate='1900-01-01'
                                      className={`form-control ${errors.createDateTo &&
                                      touched.createDateTo &&
                                      "is-invalid"}`}
                                      validate={validateDateTo(values)}

                                    />
                                    {errDate2 ? (
                                      <div className={classes.error}>{errDate2}</div>
                                    ) : null}
                                  </div>
                                </FormGroup>
                              </Col>
                              <Col >
                                <FormGroup style={{paddingTop: '0.5em'}} className="mb-0">
                                  <Button type="submit" className={classes.buttonSearch} disabled={errDate1 || errDate2}
                                          color="primary"
                                          style={{"color": "black"}}
                                    // onClick={this.onChangPageReceive(1)}
                                  ><FormattedMessage id={"search"}/></Button>
                                </FormGroup>
                              </Col>
                            </> : null
                          }

                        </FormGroup>
                      </Form>
                    )}
                  </Formik>
                </CardBody>
              </Collapse>
            </Card>
          </Col>
          <Col sm="12">
            <Card>
              <CardBody>
                <div style={{position: 'relative'}}>
                  {this.props.loading && loadingComponent}

                  <Nav tabs>
                    <NavItem>
                      <NavLink
                        className={({
                          active: this.state.active === "1"
                        })}
                        onClick={() => {
                          this.toggle("1")
                        }}
                      >
                        <FormattedMessage id={"in.use"}/>
                      </NavLink>
                    </NavItem>
                    <NavItem>
                      <NavLink
                        className={({
                          active: this.state.active === "2"
                        })}
                        onClick={() => {
                          this.toggle("2")
                        }}
                      >
                        <FormattedMessage id={"not.used"}/>
                      </NavLink>
                    </NavItem>
                  </Nav>
                  <TabContent activeTab={this.state.active}>
                    <TabPane tabId="1">
                      <Table
                        columns={columns}
                        dataSource={newDataActive}
                        // options={options}
                        pagination={false}
                        rowKey="orderBanner"
                        components={{
                          body: {
                            wrapper: this.DraggableContainer,
                            row: this.DraggableBodyRow,
                          },
                        }}
                      />
                    </TabPane>
                    <TabPane tabId="2">
                      <Table
                        columns={columns}
                        dataSource={newDataNotActive}
                        options={options}
                        pagination={false}
                        onChange={this.onChangPageReceive}
                      />
                      <div style={{marginLeft: '40%', marginTop: '5%'}}>
                        <PaginationComponent
                          totalItem={count}
                          onChangPage={this.onChangPageReceive}
                          checkSearch={this.state.isSearch}
                          resetIsSearch={this.resetIsSearch}
                        />
                      </div>
                    </TabPane>
                  </TabContent>

                </div>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </>
    )
  }
}

const mapStateToProps = (state) => {

  return {
    values: state.bannerReducer,
    lstBannerActive: state.bannerReducer.lstBannerActive,
    lstBannerNotActive: state.bannerReducer.lstBannerNotActive,
    loading: state.bannerReducer.loading,
    isLoading: state.bannerReducer.isLoading
  }

}
const mapDispatchToProps = (dispatch) => {
  return {
    bannerActions: bindActionCreators(bannerAction, dispatch)
  }
}

export default compose(withStyles(styles()), connect(mapStateToProps, mapDispatchToProps))(GridTest)
