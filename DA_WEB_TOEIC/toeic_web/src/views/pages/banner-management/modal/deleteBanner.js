import React, {Component} from 'react';
import {Form} from "formik";
import {Button, Col, FormGroup} from "reactstrap";
import Spinner from "reactstrap/es/Spinner";
import {bindActionCreators, compose} from "redux";
import {connect} from "react-redux"
import bannerAction from "../../../../redux/actions/banner/bannerAction";
import {FormattedMessage} from "react-intl";

class DeleteBanner extends Component {
  state = {

  }

  cancel = (model) => {
    this.props.deleteBanner(model);
  }

  render() {
    const { bannerId} = this.props;
    return (
      <div>
        <Form>
          <br/>
          <hr/>
          <br/><br/>
          <FormGroup className="form-group d-flex text-center">
            <Col sm={12} md={12} lg={12} xl={12} xs={12}>
              <p style={{fontSize: '16px', fontWeight: 'bold'}}><FormattedMessage id={"confirm.delete.banner"}/></p>
            </Col>
          </FormGroup>
          <br/>
          <hr/>
          <FormGroup className="d-flex">
            <Col>
              <button type="button" className="btn btn-secondary btn-modal" style={{"color":"black"}}
                      onClick={()=>{this.cancel(false)}}><FormattedMessage id={"cancel"}/>
              </button>
            </Col>
            <Col className="text-center">
              {
                this.props.loading && <Spinner color="primary"/>
              }
            </Col>
            <Col className="text-right">
              <Button color="primary" className="btn btn-modal" style={{"color":"black"}} onClick={() => {
                this.setState({
                  loading: true
                })

                let banner = {};
                banner.bannerId = bannerId;
                const {bannerActions} = this.props;
                const {deleteBanner} = bannerActions;
                deleteBanner(banner);
                // setTimeout(() => {
                  this.cancel(false)
                // }, 1000)
                this.props.onChangPageReceive();
              }}>OK</Button>
            </Col>
          </FormGroup>
        </Form>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    loading: state.bannerReducer.loading
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    bannerActions: bindActionCreators(bannerAction, dispatch)
  }
}

export default compose(connect(mapStateToProps, mapDispatchToProps))(DeleteBanner)
