const styles = ({
  title: {
    textAlign: 'right',
    fontFamily: 'ArialMT, Arial, sans-serif',
    fontSize: '13px',
  },
  error: {
    color: 'red',
    fontSize: 12
  },

  selectOption: {
    whiteSpace: 'nowrap',  //pre
    maxWidth: '235px',
    overflow: 'hidden',
    textOverflow: 'ellipsis',
    cursor:'pointer',
    // display: 'inline-block'
  },

  orderBanner: {
    maxWidth:'150px',
    selectOption: {
      whiteSpace: 'pre',
      width: '150px',
      overflow: 'hidden',
      textOverflow: 'ellipsis'
    },
  }
});
export default styles;
