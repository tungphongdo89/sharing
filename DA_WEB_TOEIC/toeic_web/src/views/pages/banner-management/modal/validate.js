import * as Yup from "yup";
import {showMessage} from "../../../../commons/utils/convertDataForToast";

const FILE_SIZE = 5.001 * 1024 * 1024;

const SUPPORTED_FORMATS_CATEORY = [
  "image/jpg",
  "image/jpeg",
  "image/png"
];
export const formSchema = Yup.object().shape({
  bannerName: Yup.string()
    .required('banner.name.cannot.be.blank')
    .max(200, 'banner.more.200.char'),
  fileUpload: Yup
    .mixed()
    .test(
      "fileSize",
      "file.upload.maximum.5MB",
      value => value ? value.size <= FILE_SIZE ? true : false : true
    )
    .test(
      "fileFormat",
      "file.upload.format",
      (value) => value ? SUPPORTED_FORMATS_CATEORY.includes(value.type) ? true : false : true
    ),
  postLink: Yup.string()

    .matches(
      /^((http[s]?|ftp|https|HTTPS|HTTP|FTP):\/)?\/?((\/+)*\/)(.*)?(#[\w\-]+)?$/,
     "link.format"
    ),
});
