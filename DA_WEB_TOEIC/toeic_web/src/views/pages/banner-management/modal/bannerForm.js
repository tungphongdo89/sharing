import React, {Component} from 'react';
import {Field, Form, Formik, getIn} from "formik";
import {Button, Col, FormGroup, Label} from "reactstrap";
import {formSchema} from './validate';
import Spinner from "reactstrap/es/Spinner";
import {bindActionCreators, compose} from "redux";
import bannerAction from "../../../../redux/actions/banner/bannerAction"
import {connect} from "react-redux"
import {FormControl, InputLabel, Select, Switch, withStyles} from "@material-ui/core";
import styles from "./styles";
import Button2 from '@material-ui/core/Button';
import HighlightOffIcon from '@material-ui/icons/HighlightOff';
import * as Icon from "react-bootstrap-icons"
import {validateBanner} from "./validateBanner";
import {FormattedMessage} from "react-intl";
import {showMessage} from "../../../../commons/utils/convertDataForToast";

let orderbannerCheck = true;
let orderBannerId = 0;

class BannerForm extends Component {

  constructor(props) {
    super(props);
    this.fileInput = React.createRef();
    orderBannerId = this.props.orderBanner;
  }

  state = {
    disableButtonOk: true,
    show: false,
    checkUploadFile: false,
    dem: 1
  }

  cancel = () => {
    this.props.bannerActions.updateStatusPopup()
  }

  componentDidMount() {
    if (this.props.isEdit) {
      this.setState({
        disableButtonOk: false
      })
    }
    this.props.bannerActions.showBannerActive()
  }

  selectForm = (props) => {
    const {lstBannerActive1} = this.props;
    let lstBannerTemp = [];
    for (let i = 0; i < lstBannerActive1.length; i++) {
      if (lstBannerActive1[i]) {
        lstBannerTemp[lstBannerActive1[i].orderBanner] = lstBannerActive1[i]
      }
    }
    for (let i = 1; i < 6; i++) {
      if (!lstBannerTemp[i]) {
        lstBannerTemp[i] = null;
      }
    }

    let optionBannerActive = lstBannerTemp ? lstBannerTemp.map((banner, index) => {
      if (banner !== null) {
        if (banner.orderBanner === props.form.values.orderBanner) {
          return <option style={styles.selectOption} title={banner.bannerName} selected value={banner.orderBanner}
                         key={index}>({index}) {banner.bannerName}</option>
        } else {
          return <option style={styles.selectOption} title={banner.bannerName} value={banner.orderBanner}
                         key={index}>({index}) {banner.bannerName}</option>
        }
      } else {
        return <option value={index} key={index}>({index}) -- Select --</option>
      }
    }) : null


    return (
      <div>
        <FormControl>
          <InputLabel><FormattedMessage id={"choose.an.alternative.banner"}/></InputLabel>
          <Select
            onChange={(e) => {
              props.form.setFieldValue(props.field.name, e.target.value);
            }}
            type="select" name="orderBanner"
            className={props.field.name}
            placeholder={"" + showMessage("choose.an.alternative.banner")}
            className="basic-single"
            defaultValue={props.form.values.orderBanner}
            classNamePrefix="select"
            isClearable={true}
            style={{width: '265px'}}
          >
            <option value={0} selected={'selected'} hidden={true}><FormattedMessage
              id={"choose.an.alternative.banner"}/></option>
            {optionBannerActive}
          </Select>
        </FormControl>
        {orderbannerCheck && !props.form.values.orderBanner ?
          <span style={{color: "red"}}><FormattedMessage id={"replace.banner.not.blank"}/></span> : null}
      </div>
    )
  }

  RenderFileUpload = (props, value) => {
    if (this.state.dem === 1 && value.isEdit) {
      this.setState({
        disableButtonOk: false,
        dem: this.state.dem + 1
      })
    }
    const onFileChange = (e) => {
      this.setState({
        checkUploadFile: true,
        disableButtonOk: false
      })

      props.form.setFieldTouched(props.field.name, undefined);
      let reader = new FileReader();
      let file = e.target.files[0];
      if (file) {
        reader.onloadend = () => {
        };
        props.form.setFieldValue(props.field.name, file);
        if (this.fileInput.current) {
          this.fileInput.current.value = null
        }
      }
    }

    const onDeleteFile = () => {
      props.form.setFieldValue(props.field.name, null);
      this.setState({
        checkUploadFile: false
      })
    }

    const onFileClick = (value) => {
    }

    return (
      <div className="w-100">
        <input
          onClick={onFileClick}
          ref={this.fileInput}
          name={props.field.name}
          style={{display: "none"}}
          id={`contained-button-file ${props.field.name}`}
          type="file"
          onChange={onFileChange}
        />
        <label htmlFor={`contained-button-file ${props.field.name}`}
               onKeyDown={e => e.keyCode === 13 ? e.target.click() : ""}>
          <Button2 id={props.field.name} disabled={this.props.isViewDetail} variant="contained" color="primary"
                   component="span" name={props.field.name} type="submit">
            <Icon.Upload style={{marginRight: '5px'}}/> Upload
          </Button2>
        </label>
        <div style={{paddingTop: '5px', display: 'flex'}}>
          <div style={{whiteSpace: 'nowrap', textOverflow: "ellipsis", overflow: "hidden"}}>
            {
              props.field.value ?
                <span style={{paddingTop: '5px', whiteSpace: 'nowrap', textOverflow: "ellipsis", overflow: "hidden"}}
                      title={props.field.value.name}> {props.field.value.name}
              </span>
                : null
            }
          </div>
          <div style={{marginLeft: "2%"}}>
             <span>
            {props.field.value ?
              <HighlightOffIcon style={{color: 'red', cursor: 'pointer', marginLeft: '1%'}}
                                onClick={onDeleteFile}/> : ''
            }
          </span>
          </div>

        </div>
      </div>
    );
  };


  render() {
    const {classes, bannerActions, lstBannerActive1} = this.props;
    const {loading} = this.state;

    const ErrorMessage = ({name}) => (
      <Field name={name}>
        {({field, form, meta}) => {
          const error = getIn(form.errors, name);
          const touch = getIn(form.touched, name);
          return touch && error ? (
            <div className="invalid-feedback" style={{display: "contents"}}>{showMessage(error)}</div>) : null;
        }}
      </Field>
    );
    return (
      <div>
        <Formik
          initialValues={{
            bannerName: this.props.bannerName ? this.props.bannerName : "",
            fileUpload: null,
            pathImage: this.props.pathImage ? this.props.pathImage : "",
            postLink: this.props.postLink ? this.props.postLink : "",
            status: this.props.status ? this.props.status : 0,
            replaceBanner: 0,
            orderBanner: this.props.orderBanner ? this.props.orderBanner : 0,
            selectActiveBannerName: "",
          }}
          onSubmit={(values, actions) => {
            debugger
            if (values.status == 1 && (values.orderBanner === 0 || values.orderBanner === undefined)) {
              actions.setSubmitting(false);
            } else {
              this.setState({
                loading: true
              })
              let formData = new FormData()
              if (this.props.bannerId) {
                formData.append("bannerId", this.props.bannerId)
              }
              formData.append("bannerName", values.bannerName)
              formData.append("postLink", values.postLink)
              if (values.fileUpload !== null && values.fileUpload !== undefined) {
                formData.append("fileUpload", values.fileUpload)
              }
              if (this.props.pathImage) {
                formData.append("pathImage", values.pathImage)
              }
              formData.append("status", values.status)
              formData.append("orderBanner", values.orderBanner)
              console.log(formData);
              if (this.props.bannerId) {
                // this.setState({
                //   disableButtonOk: false
                // })
                bannerActions.updateBanner(formData)
              } else {
                bannerActions.createBanner(formData)
              }
              this.props.onChangPageReceive(1)
              setTimeout(() => {
                actions.setSubmitting(false);
                // this.cancel(false)
              }, 1000);
            }
          }}
          validationSchema={formSchema}
          validate={validateBanner}
        >
          {({errors, touched, values, handleChange, setFieldValue, isValid, isSubmitting}) => (
            <Form>
              <br/>
              <br/>
              <FormGroup className="form-group d-flex">
                <Label className={classes.title} for="required" sm={4} xl={4} lg={4} xs={4}><FormattedMessage
                  id={"banner.name"}/>(<span
                  style={{color: "red"}}>*</span>) </Label>
                <Col sm={7} xl={7} lg={7} xs={7}>
                  <div className="w-100">
                    <Field
                      type="text"
                      name="bannerName"
                      className={`form-control ${errors.bannerName &&
                      touched.bannerName &&
                      "is-invalid"}`}
                      onMouseLeave={(e) => {
                        if (e.target.value.toString().trim() === '') {
                          values.bannerName = ''
                          e.target.value = ""
                        }
                      }}
                      EscapeOutside={(e) => {
                        if (e.target.value.toString().trim() === '') {
                          values.bannerName = ''
                          e.target.value = ""
                        }
                      }}
                    >
                    </Field>
                    {errors.bannerName && touched.bannerName ? (
                      <div className={classes.error}>{showMessage(errors.bannerName)}</div>
                    ) : null}
                  </div>
                </Col>
              </FormGroup>
              <FormGroup className="form-group d-flex">
                <Label className={classes.title} for="required" sm={4} xl={4} lg={4} xs={4}><FormattedMessage
                  id={"image"}/> (<span
                  style={{color: "red"}}>*</span>) </Label>
                <Col sm={7} xl={7} lg={7} xs={7}>
                  <div>
                    <Field
                      name="fileUpload"
                      setFieldValue={this.props.setFieldValue}
                      errorMessage={errors[`fileUpload`] ? errors[`fileUpload`] : undefined}
                      touched={touched[`fileUpload`]}
                      style={{display: "flex"}}
                      onBlur={this.props.handleBlur}
                      component={e => this.RenderFileUpload(e, this.props)}

                    />

                    {
                      this.props.pathImage && !this.state.checkUploadFile ?
                        <div style={styles.selectOption} title={this.props.pathImage}>
                          {this.props.pathImage}
                        </div>
                        : null
                    }

                    {errors.fileUpload && touched.fileUpload ? (
                      <div className={classes.error}>{showMessage(errors.fileUpload)}</div>
                    ) : null}
                    {!this.state.checkUploadFile && !this.props.pathImage ? (
                      <div className={classes.error} style={{color: 'red'}}><FormattedMessage
                        id={"image.cannot.empty"}/></div>
                    ) : null}
                  </div>
                </Col>
              </FormGroup>
              <FormGroup className="form-group d-flex">
                <Label className={classes.title} for="required" sm={4} xl={4} lg={4} xs={4}>Link </Label>
                <Col sm={7} xl={7} lg={7} xs={7}>
                  <div className="w-100">
                    <Field
                      type="text"
                      name="postLink"
                      className={`form-control`}
                    >
                    </Field>
                  </div>
                  <ErrorMessage name="postLink"/>

                </Col>
              </FormGroup>
              <FormGroup className="form-group d-flex">
                <Label className={classes.title} for="required" sm={4} xl={4} lg={4} xs={4}><FormattedMessage
                  id={"status"}/>(<span
                  style={{color: "red"}}>*</span>) </Label>
                <Col sm={7} xl={7} lg={7} xs={7}>
                  <div className="w-100">
                    <div>
                      <Switch
                        // disabled={isViewDetail}
                        // onClick={() => this.showListBannerActive()}
                        name="status"
                        color="primary"
                        defaultChecked={this.props.status !== 0 ? this.props.status : false}
                        // checked={this.state.status}
                        onChange={(event, checked) => {
                          debugger
                          if (checked === true && (this.props.orderBanner === 0 || this.props.orderBanner === undefined)) {
                            setFieldValue("status", 1);
                            orderbannerCheck = true;
                            setFieldValue("orderBanner", orderBannerId)
                          } else {
                            this.props.updateOrderBannerValue(0);
                            setFieldValue("orderBanner", 0)
                            setFieldValue("status", 0)
                          }
                        }}
                      />
                      {errors.status && touched.status ? (
                        <div className="invalid-feedback mt-25">{errors.status}</div>
                      ) : null}
                    </div>

                  </div>
                </Col>
              </FormGroup>
              {values.status === 1 && (orderBannerId === 0 || orderBannerId === undefined) ?
                <FormGroup className="form-group d-flex">
                  <Label className={classes.title} for="required" sm={4} xl={4} lg={4} xs={4}><FormattedMessage
                    id={"alternative.banners"}/> (<span
                    style={{color: "red"}}>*</span>) </Label>
                  <Col sm={7} xl={7} lg={7} xs={7}>
                    <div className="w-100 d-flex">
                      <Field
                        setFieldValues={setFieldValue}
                        touched="orderBanner"
                        className="form-control"
                        name="orderBanner"
                        classStyle={classes}
                        component={this.selectForm}/>
                      {errors.orderBanner && touched.orderBanner ? (
                        <div className="invalid-feedback mt-25" style={{display: 'block'}}>{errors.orderBanner}</div>
                      ) : null}
                    </div>
                    <ErrorMessage name="validateBanner"/>
                  </Col>
                </FormGroup>
                : null
              }
              <hr/>
              <FormGroup className="form-group d-flex">
                <Col>
                  <button type="button" className="btn btn-secondary btn-modal" style={{"color": "black"}}
                          onClick={() => {
                            this.cancel(false)
                          }}><FormattedMessage id={"cancel"}/>
                  </button>
                </Col>
                {
                  loading && <Spinner color="primary"/>
                }
                <Col className="text-right focus">
                  <Button
                    color="primary" style={{"color": "black"}} className="btn btn-primary btn-modal" type="submit"
                    disabled={orderBannerId === 0
                    && !errors.status && values.orderBanner !== 1
                    && (this.state.disableButtonOk || isSubmitting || !isValid)}>OK</Button>
                </Col>
              </FormGroup>
            </Form>
          )}
        </Formik>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    lstBannerActive1: state.bannerReducer.lstBannerActive,
    loading: state.bannerReducer.loading,
    isLoading: state.bannerReducer.isLoading,
    closePopup: state.bannerReducer.closePopup
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    bannerActions: bindActionCreators(bannerAction, dispatch),
  }
}

export default compose(withStyles(styles), connect(mapStateToProps, mapDispatchToProps))(BannerForm)
