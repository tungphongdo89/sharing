const styles = (theme) =>({
  buttonAdd:{
    paddingTop: '1.2em ! important',
    marginTop: '1em ! important',
    paddingLeft:'2em ! important',
  },
  buttonSearch:{
    paddingTop: '1.2em ! important',
    marginTop: '1em ! important',

  },
  search:{
    paddingLeft:'4em'
  },
  error:{
    color:'red',

  },
  selectOptionValue:{
    height: '43px !important',
    paddingTop: '0px !important',
    paddingBottom: '0px !important',
    backgroundColor: 'red !important'
  }
})
export default styles;
