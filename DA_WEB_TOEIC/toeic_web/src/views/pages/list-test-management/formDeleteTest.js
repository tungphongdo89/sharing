import React, {Component} from 'react';
import {Form} from "formik";
import {Button, Col, FormGroup} from "reactstrap";
import Spinner from "reactstrap/es/Spinner";
import {bindActionCreators, compose} from "redux";
import {connect} from "react-redux"
import testAction from "../../../redux/actions/test";
import {FormattedMessage} from "react-intl";

class FormDeleteTest extends Component {
  state = {
    loading: false,

  }
  cancel = (model) => {
    this.props.deleteTest1(model);
  }


  render() {
    const {loading, testId, categoryName} = this.props;
    return (
      <div>
        <Form>
          <br/>
          <hr/>
          <br/><br/>
          <FormGroup className="form-group d-flex text-center">
            <Col sm={12} md={12} lg={12} xl={12} xs={12}>
              <p style={{fontSize: '16px', fontWeight: 'bold'}}><FormattedMessage id={"confirm.remove.test1"}/></p>
            </Col>
          </FormGroup>
          <br/>
          <hr/>
          <FormGroup className="d-flex">
            <Col>
              <Button color="light" onClick={() => {
                this.cancel(false)
              }}><FormattedMessage id={"cancel"}/></Button>
            </Col>
            <Col className="text-center">
              {
                loading && <Spinner color="primary"/>
              }
            </Col>
            <Col className="text-right">
              <Button color="primary" onClick={() => {
                this.setState({
                  loading: true
                })

                let test = {};
                test.id = testId;
                test.categoryName = categoryName;
                const {testAction} = this.props;
                const {deleteTest} = testAction;
                deleteTest(test);
                setTimeout(() => {
                  this.cancel(false)
                }, 1000)
              }}>OK</Button>
            </Col>
          </FormGroup>
        </Form>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    loading: state.testReducer.loadingTest
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    testAction: bindActionCreators(testAction, dispatch)
  }
}

export default compose(connect(mapStateToProps, mapDispatchToProps))(FormDeleteTest)
