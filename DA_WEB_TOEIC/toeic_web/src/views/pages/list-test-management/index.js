import React from "react"
import testAction from '../../../redux/actions/test';
import topicAction from '../../../redux/actions/topic/topicAction';
import {Link} from 'react-router-dom';
import {bindActionCreators, compose} from 'redux';
import {connect} from "react-redux"
import {IconButton, TableCell, TableFooter, TableRow, withStyles} from "@material-ui/core";
import {Form, Formik} from "formik"
import MUIDataTable from "mui-datatables";
import styles from './styles'
import PaginationComponent from './pagingtion';
import FormDeleteTest from './formDeleteTest';
import ListStudentFaild from './listStudentFaild';

import {
  Button,
  Card,
  CardBody,
  CardHeader,
  CardTitle,
  Col,
  Collapse,
  FormGroup,
  Input,
  Label,
  Modal,
  ModalHeader,
  Row,
  Spinner
} from "reactstrap"

import {Edit, Eye, Search, Trash2} from "react-feather"
import "../../../assets/scss/plugins/tables/_agGridStyleOverride.scss"
import "../../../assets/scss/pages/users.scss"
import {history} from "../../../history";
import OverlayTrigger from "react-bootstrap/OverlayTrigger";
import Tooltip from "react-bootstrap/Tooltip";
import styled from "styled-components"
import Select from 'react-select';
import ClearIcon from '@material-ui/icons/Clear';
import {showMessage} from "../../../commons/utils/convertDataForToast";
import {FormattedMessage} from "react-intl";

let optionPartByTopic;
let optionNameTopicByPartAndTypeTopic;
let optionTypeTopic;
let optionLevelCode;

const TableCellStyle = styled(TableCell)`
  &&&{
    display: grid;
    justify-content: center;
   }
`

const optionsSelect = [
  {value: '2', label: <FormattedMessage id={"all"}/>, height: '47px'},
  {value: '1', label: <FormattedMessage id={"in.use"}/>, height: '47px'},
  {value: '0', label: <FormattedMessage id={"not.used"}/>, height: '47px'}
]
const customStyles = {
  option: (provided,) => ({
    ...provided,
    height: 32
  }),
  control: base => ({
    ...base,
    height: 45,
    minHeight: 45
  })
}

class GridTest extends React.Component {
  state = {
    page: 1,
    count: 1,
    searchValue: "",
    currentPage: 0,
    pageSize: 10,
    isVisible: true,
    reload: false,
    collapse: true,
    collapseListStudent: true,
    status: "Opened",
    categoryType: "2",
    typeTopic: "",
    part: "",
    codeTopic: "",
    levelCode: "",
    statusTest: "2",
    countCategory: null,
    createTo: null,
    search: showMessage("enter.information.to.search"),
    rowsPerPageOptions: [10],
    testId: 0,
    categoryName:'',
    testName: "",
    typeCode: 0,
    isSearch: null
  }


  componentWillMount() {
    let data = {};
    const {testAction, topicAction} = this.props;
    const {fetchDataFiler} = testAction;
    const {fetchPartByTopic, fetchTopicNameByPartAndTypeTopic} = topicAction;
    fetchDataFiler(data);
    fetchPartByTopic(data);
    fetchTopicNameByPartAndTypeTopic(data);

  }

  // componentDidUpdate(prevProps, prevState, snapshot) {
  //   const {total} = prevProps;
  //   // if (total) {
  //   //   this.setState({
  //   //     count: total
  //   //   });
  //   // }
  // }

  UNSAFE_componentWillReceiveProps(nextProps, nextContext) {
    const {total} = nextProps.values.dataRes;
    if (total) {
      this.setState({
        count: total
      });
    }
    const {getLevelCode, lstTypeTopic} = nextProps.dataFilter;
    optionTypeTopic = lstTypeTopic !== undefined ? lstTypeTopic.map((type) => {
      return <option value={type.code}>{type.name}</option>
    }) : null;
    optionLevelCode = getLevelCode !== undefined ? getLevelCode.map((type) => {
      return <option value={type.code}>{type.name}</option>
    }) : null;
  }

  componentWillUpdate(nextProps, nextState, nextContext) {
    const {lstPartByTopic} = nextProps;
    optionPartByTopic = lstPartByTopic !== undefined ? lstPartByTopic.map((type) => {
      return <option value={type.code}>{type.name}</option>
    }) : null
    const {lstTopicByTypeTopicAndPart} = nextProps;
    optionNameTopicByPartAndTypeTopic = lstTopicByTypeTopicAndPart !== undefined ? lstTopicByTypeTopicAndPart.map((type) => {
      return <option value={type.code}>{type.topicName}</option>
    }) : null
  }

  componentDidMount() {
    let test = {};
    test.page = this.state.page;
    test.pageSize = this.state.pageSize;
    const {testAction} = this.props;
    const {fetchTest} = testAction;
    fetchTest(test)
    const {values} = this.props;
    const {total} = values;
    this.setState({
      count: total
    })
  }

  toggleModalListStudent = (testId, testName) => {
    this.setState(prevState => ({
      testName: testName,
      testId: testId,
      modalListStudent: !prevState.modalListStudent
    }))
  }
  toggleModal = (testId,categoryName) => {
    this.setState(prevState => ({
      testId: testId,
      categoryName:categoryName,
      modal: !prevState.modal
    }))
  }
  refreshCard = () => {
    this.setState({reload: true})
    setTimeout(() => {
      this.setState({
        reload: false,
        part: "",
        codeTopic: "",
        typeTopic: "",
        statusTest: "2",
        categoryType: "",
        levelCode: ""
      })
    }, 500)
  }
  toggleCollapseListStudent = () => {
    this.setState(state => ({collapseListStudent: !state.collapseListStudent}))
  }

  toggleCollapse = () => {
    this.setState(state => ({collapse: !state.collapse}))
  }
  onEntered = () => {
    this.setState({status: "Opened"})
  }
  onEntering = () => {
    this.setState({status: "Opening..."})
  }
  onExiting = () => {
    this.setState({status: "Closing..."})
  }
  onExited = () => {
    this.setState({status: "Closed"})
  }
  removeCard = () => {
    this.setState({isVisible: false})
  }

  //-----------------------------------------------------------------------

  onChangPageReceive = (pageNumber) => {
    this.setState({
      page: pageNumber
    })
    if (pageNumber === 1) {
      this.setState({
        isSearch: 1,
      })
    }
    const searchPropertiesBE = {};
    searchPropertiesBE.keySearch = this.state.searchValue.toString().trim()
    searchPropertiesBE.status = parseInt(this.state.statusTest)
    searchPropertiesBE.countCategory = this.state.countCategory
    searchPropertiesBE.createTo = this.state.createTo
    searchPropertiesBE.updatedDate = this.state.createTo
    searchPropertiesBE.pageSize = this.state.pageSize
    searchPropertiesBE.page = pageNumber
    this.props.testAction.fetchTest(searchPropertiesBE)
  }

  resetIsSearch = () => {
    this.setState({
      isSearch: null
    })
  }

  //-----------------------------------------------------------------------
  deleteTest = (data, id) => {
    this.setState({
      modal: data
    })
  }
  onHandleAdd = () => {
    const {testAction} = this.props;
    const {editDetailTest} = testAction;
    editDetailTest();
    history.push("/pages/test-form")
  }
  // handSelectOption=() =>{
  //   const optionSelectValue = this.props
  // }
  render() {
    const {testId,testName,categoryName} = this.state;
    const count = this.props.values.total;
    const {classes} = this.props;
    const options = {
      filter: false,
      filterType: 'checkbox',
      selectableRowsHideCheckboxes: true,
      responsive: "standard",
      selectableRows: "none",
      print: false,
      download: false,
      search: false,
      viewColumns: false,
      serverSide: true,
      textLabels: {
        body: {
          noMatch: this.props.isLoading ?
            <p/> :
            showMessage("not.found.data"),
        },
      },
      customFooter: () => {
        return (
          <TableFooter>
            <TableRow>
              <TableCellStyle colSpan={6}>
                <PaginationComponent
                  totalItem={count}
                  onChangPage={this.onChangPageReceive}
                  checkSearch={this.state.isSearch}
                  resetIsSearch={this.resetIsSearch}
                />
              </TableCellStyle>
            </TableRow>
          </TableFooter>
        );
      }
    };
    const columns = [
      {
        name: 'sl',
        label: showMessage("index"),
        options: {
          filter: true,
          sort: false,
          customHeadLabelRender: () => {
            return <div autoFocus={false} style={{fontWeight: 'bold'}}><FormattedMessage id="index"/></div>;
          },
          customBodyRender: (index) => {
            debugger
            return (<div className="text-left" autoFocus={false}>
              {index}
            </div>)
          }
        }
      },
      // {
      //   name: "id",
      //   label: "Mã bài thi",
      //   options: {
      //     filter: true,
      //     sort: true,
      //     customHeadLabelRender: () => {
      //       return <div style={{fontWeight: 'bold'}}>Mã bài thi</div>;
      //     },
      //     customBodyRender: (data) => {
      //       return (<div>
      //         {data}
      //       </div>)
      //     }
      //   }
      // },
      {
        name: "name",
        label: showMessage("test.name"),
        options: {
          filter: true,
          sort: false,
          customHeadLabelRender: () => {
            return <div autoFocus={false} style={{fontWeight: 'bold'}}><FormattedMessage id={"test.name"}/></div>;
          },
          customBodyRender: (data, dataRow) => {

            return (<Link
              className="text-left"
              onClick={() => {
                const {testAction, lstTest} = this.props;
                const {fetchDetailTest} = testAction;
                fetchDetailTest(lstTest[dataRow.rowIndex], true);
                history.push("/pages/category-form")
              }}
              style={{
                textOverflow: 'ellipsis',
                whiteSpace: 'nowrap',
                width: '150px',
                overflow: 'hidden',
                display: 'inline-block'
              }}
              title={data}
              autoFocus={false}
            >
              {data}</Link>)
          }
        }
      }, {
        name: "status",
        label: showMessage("status"),
        options: {
          filter: true,
          sort: false,
          // custom
          customHeadLabelRender: () => {
            return <div autoFocus={false} style={{fontWeight: 'bold'}}><FormattedMessage id={"status"}/></div>;
          },
          customBodyRender: (data) => {
            return data === 1 ? (<div autoFocus={false} className='badge badge-pill badge-success'><FormattedMessage id={"in.use"}/></div>) :
              data === 0 ? (
                <div autoFocus={false} className='badge badge-pill badge-light' style={{color: '#000'}}><FormattedMessage id={"not.used"}/></div>) : null
          }
        }
      },
      {
        name: "countCategory",
        label: showMessage("number.doing.test"),
        options: {
          filter: true,
          sort: false,
          customHeadLabelRender: () => {
            return <div autoFocus={false} style={{fontWeight: 'bold', textAlign: 'right'}}><FormattedMessage id={"number.doing.test"}/></div>;
          },
          customBodyRender: (data, dataRow) => {

            return (<div autoFocus={false} style={{textAlign: 'right'}}>
              {data}
            </div>)
          }
        }
      },
      {
        name: "totalFaild",
        label: showMessage("number.student.fails"),
        options: {
          filter: true,
          sort: false,
          customHeadLabelRender: () => {
            return <>
              <div autoFocus={false} style={{fontWeight: 'bold', textAlign: 'right'}}><FormattedMessage id={"number.student.fails"}/></div>
            </>
          },
          customBodyRender: (data, dataRow) => {
            return (<div
              autoFocus={false}
              style={{textAlign: 'right'}}>
              {
                data ?
                  <span size={15} className="fonticon-wrap" style={{cursor: "pointer", color: "red"}}
                        onClick={() => {
                          this.toggleModalListStudent(dataRow.rowData[6], dataRow.rowData[1]);
                        }}
                  >
                    {data}
                  </span> : 0
              }
            </div>)
          }
        }
      },
      // {
      //   name: "latestHomeworkTime",
      //   label: "Thời gian làm bài gần nhất",
      //   options: {
      //     filter: true,
      //     sort: false,
      //     customHeadLabelRender: () => {
      //       return <div style={{fontWeight: 'bold'}}>Thời gian làm bài gần nhất</div>;
      //     },
      //     customBodyRender: (data) => {
      //       return data !== null ? (getUTCTime(new Date(data))) : null
      //     }
      //   }
      // }, {
      //   name: "create_time",
      //   label: "Thời gian tạo",
      //   options: {
      //     filter: true,
      //     sort: false,
      //     customHeadLabelRender: () => {
      //       return <div style={{fontWeight: 'bold'}}>Thời gian tạo</div>;
      //     },
      //     customBodyRender: (data) => {
      //       return data !== null ? (getUTCTime(new Date(data))) : null
      //     }
      //   }
      // }, {
      //   name: "updated_time",
      //   label: "Cập nhật lần cuối",
      //   options: {
      //     filter: true,
      //     sort: false,
      //     customHeadLabelRender: () => {
      //       return <div style={{fontWeight: 'bold'}}>Cập nhật lần cuối</div>;
      //     },
      //     customBodyRender: (data) => {
      //       return data !== null ? (
      //         getUTCTime(new Date(data))
      //       ) : null
      //     }
      //   }
      // },
      {
        name: "",
        label: showMessage("action"),
        options: {
          filter: true,
          sort: false,
          customHeadLabelRender: () => {
            return <div autoFocus={false} style={{fontWeight: 'bold', textAlign: 'center', paddingRight: '16px'}}><FormattedMessage id={"action"}/></div>;
          },
          customBodyRender: (data, dataRow) => {
            return (
              <div style={{textAlign: 'center'}}>
                <OverlayTrigger placement="top" overlay={props => (<Tooltip {...props}><FormattedMessage id={"edit"}/></Tooltip>)}>
                  <button id="btnHighLight"
                          style={{border: 'none', backgroundColor: 'rgb(0 0 0 / 0%)', marginBottom: '2px'}}
                          // onClick={() => {
                          //   const {testAction, lstTest} = this.props;
                          //   const {fetchDetailTest} = testAction;
                          //   fetchDetailTest(lstTest[dataRow.rowIndex], false);
                          //   history.push("/pages/test-form")
                          // }}
                  >
                    <Edit size={15} className=" mr-1 fonticon-wrap" style={{cursor: "pointer", color: "#2c00ff"}}
                    /></button>
                </OverlayTrigger>
                <OverlayTrigger placement="top" overlay={props => (<Tooltip {...props}><FormattedMessage id={"remove"}/></Tooltip>)}>
                  <button id="btnHighLight"
                          style={{border: 'none', backgroundColor: 'rgb(0 0 0 / 0%)', marginBottom: '2px'}}
                          onClick={() => {
                            debugger
                            console.log("props ===>",dataRow)
                            this.toggleModal(dataRow.rowData[6],dataRow.rowData[7]);
                          }}>
                    <Trash2
                      size={15} className="mr-1 fonticon-wrap" style={{cursor: "pointer", color: "red"}}
                    />
                  </button>
                </OverlayTrigger>
                <OverlayTrigger placement="top" overlay={props => (<Tooltip {...props}><FormattedMessage id={"Detail"}/></Tooltip>)}>
                  <button id="btnHighLight"
                          style={{border: 'none', backgroundColor: 'rgb(0 0 0 / 0%)', marginBottom: '2px'}}
                          // onClick={() => {
                          //   const {testAction, lstTest} = this.props;
                          //   const {fetchDetailTest} = testAction;
                          //   fetchDetailTest(lstTest[dataRow.rowIndex], true);
                          //   history.push("/pages/test-form")
                          // }}
                  >
                    <Eye size={15} className=" mr-1 fonticon-wrap" style={{cursor: "pointer"}}
                    /></button>
                </OverlayTrigger>
              </div>

            )
          }
        }
      },
      {
        name: 'id',
        label: "id",
        options: {
          display: false,
          filter: true,
        }
      },
      {
        name: "categoryName",
        options: {
          display:false
        }
      },
    ];
    const {lstTest, isLoading} = this.props;
    if (this.state.page === 0) {
      this.state.page = 1;
    }
    let newData = [];
    lstTest.map((item, index) => {
      newData.push({sl: (this.state.pageSize) * (this.state.page - 1) + index + 1, ...item});
    });

    const loadingComponent = (
      <div style={{
        position: 'absolute',
        zIndex: 110,
        top: 0,
        left: 0,
        width: '100%',
        height: '100%',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        background: 'rgba(255,255,255,0.8)'
      }}>
        <Spinner color="primary" className="reload-spinner"/>
      </div>
    );


    return (
      <>
        <style>{`
          #statusTest span  {
            display: none !important;
          }
        `}
        </style>

        <Row>
          <Col sm="12">
            <Card>
              <CardHeader>
                <CardTitle><h2><FormattedMessage id={"list.of.tests"}/></h2></CardTitle>
              </CardHeader>
              <Collapse
                isOpen={this.state.collapse}
                onExited={this.onExited}
                onEntered={this.onEntered}
                onExiting={this.onExiting}
                onEntering={this.onEntering}
              >
                <CardBody>
                  {this.state.reload ? (
                    <Spinner color="primary" className="reload-spinner"/>
                  ) : (
                    ""
                  )}
                  <Formik initialValues={{
                    countCategory: "",
                    createDateTo: ""
                  }} onSubmit={() => {
                    this.setState({
                      loading: true
                    })
                  }}
                  >
                    {({errors, touched}) => (
                      <Form>
                        <FormGroup className="form-group d-flex">
                          <Col lg="2" md="2" sm="2">
                            <FormGroup style={{paddingTop: '0.5em'}} className="mb-0">
                              <Button className={classes.buttonAdd}
                                      color="primary"
                                      // onClick={this.onHandleAdd}
                                      style={{
                                        width: '100%',
                                        overflow: 'hidden',
                                        whiteSpace: 'nowrap',
                                        textOverflow: 'ellipsis',
                                      }}
                              > <FormattedMessage id={"add"}/>
                              </Button>
                            </FormGroup>
                            <Modal
                              isOpen={this.state.modal}
                              toggle={this.toggleModal}
                              className="modal-dialog-centered"
                            >
                              <Row className="flex flex-space-between flex-middle  bg-primary " style={{width:'100%', marginLeft:'0.2px'}}  >
                                <Col xs={11}>
                                  <ModalHeader  style={{ background: '#7367f0' }}
                                  >
                                    <h3 style={{ color: 'white' }}><FormattedMessage id={"confirm.remove.test"}/></h3>
                                  </ModalHeader>
                                </Col>
                                <Col xs={1} style={{paddingLeft: '0px', paddingRight:'0px'}}>
                                  <IconButton onClick={this.toggleModal}>
                                    <ClearIcon />
                                  </IconButton>
                                </Col>
                              </Row>

                              <FormDeleteTest testId={testId} categoryName={categoryName} deleteTest1={(model) => {
                                this.deleteTest(model)
                              }}/>
                            </Modal>
                            <Modal
                              isOpen={this.state.modalListStudent}
                              toggle={this.toggleModalListStudent}
                              className="modal-dialog-centered modal-lg"
                              style={{maxWidth: '70%'}}
                            >
                              <Row className="flex flex-space-between flex-middle  bg-primary "
                                   style={{width: '100%', marginLeft: '0.15px'}}>
                                <Col xs={11}>
                                  <ModalHeader style={{background: '#7367f0', textAlign: 'center'}}>
                                    <h3 style={{color: '#FFFFFF'}}><strong><FormattedMessage id={"list.student.fails"}/>: <span
                                      style={{color: "cyan"}}>{this.state.testName}</span></strong></h3>
                                  </ModalHeader>
                                </Col>
                                <Col xs={1} style={{paddingLeft: '0px', paddingRight: '0px',textAlign: 'right'}}>
                                  <IconButton onClick={this.toggleModalListStudent}>
                                    <ClearIcon/>
                                  </IconButton>
                                </Col>
                              </Row>

                              <ListStudentFaild testId={testId} testName={testName} cancel={this.toggleModalListStudent}/>
                            </Modal>
                          </Col>
                          <Col sm={4} md={4} lg={4} xs={4}>
                            <span>&nbsp;</span>
                            <FormGroup className="position-relative has-icon-left">
                              <div className="w-100">
                                <Input
                                  autoFocus
                                  type="search"
                                  name="searchValue"
                                  id="searchValue"
                                  style={{height: '45px'}}
                                  placeholder={this.state.search}
                                  onChange={e => {
                                    this.setState(
                                      {
                                        searchValue: e.target.value
                                      }
                                    )
                                  }}
                                />
                                <div className="form-control-position px-1">
                                  <Search size={15}/>
                                </div>
                              </div>
                            </FormGroup>
                          </Col>
                          <Col sm={4} md={4} lg={4} xs={4}>
                            <FormGroup className="position-relative has-icon-right">
                              <div className="w-100">
                                <Label className={classes.fieldLabel} for="status"><FormattedMessage id={"status"}/></Label>
                                <Select
                                  control='47px'
                                  height='43px'
                                  styles={customStyles}
                                  name="statusTest"
                                  id="statusTest"
                                  // style={styles.selectOptionValue}
                                  value={optionsSelect[2 - this.state.statusTest]}
                                  options={optionsSelect}
                                  // onChange={this.handSelectOption}
                                  onChange={e => {
                                    this.setState(
                                      {
                                        statusTest: e.value
                                      }
                                    )
                                  }}
                                />
                              </div>
                            </FormGroup>
                          </Col>
                          <Col sm={2} md={2} lg={2} xs={2}>
                            <FormGroup style={{paddingTop: '0.5em'}} className="mb-0">
                              <Button type="submit" className={classes.buttonSearch} color="primary"
                                      onClick={() => this.onChangPageReceive(1)}><FormattedMessage id={"search"}/></Button>
                            </FormGroup>
                          </Col>
                        </FormGroup>
                        <FormGroup className="form-group d-flex">
                          <Col sm={2} md={2} lg={2} xs={2}/>
                        </FormGroup>
                      </Form>
                    )}
                  </Formik>
                </CardBody>
              </Collapse>
            </Card>
          </Col>
          <Col sm="12">
            <div style={{position: 'relative'}} onClick={() => {
            }}>
              {isLoading && loadingComponent}

              <MUIDataTable
                columns={columns}
                data={newData}
                options={options}
              />
            </div>
          </Col>
        </Row>
      </>
    )
  }
}

const mapStateToProps = (state) => {

  return {
    lstTest: state.testReducer.lstTest,
    isLoading: state.testReducer.isLoading,
    values: state.testReducer,
    dataFilter: state.testReducer.dataFilter,
    detailTest: state.testReducer.detailTest,
    lstPartByTopic: state.topicReducer.lstPartByTopic,
    lstTopicByTypeTopicAndPart: state.topicReducer.lstTopicByTypeTopicAndPart,
  }

}
const mapDispatchToProps = (dispatch) => {
  return {
    testAction: bindActionCreators(testAction, dispatch),
    topicAction: bindActionCreators(topicAction, dispatch)
  }
}

export default compose(withStyles(styles()), connect(mapStateToProps, mapDispatchToProps))(GridTest)
