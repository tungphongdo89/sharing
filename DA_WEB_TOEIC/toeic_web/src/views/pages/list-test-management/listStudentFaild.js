import React, {Component} from 'react';
import MUIDataTable from "mui-datatables";
import {TableCell, TableFooter, TableRow, withStyles} from "@material-ui/core";
import PaginationComponent from "./pagingtion";
import styled from "styled-components";
import {bindActionCreators, compose} from "redux";
import testAction from "../../../redux/actions/test";
import topicAction from "../../../redux/actions/topic/topicAction";
import styles from "./styles";
import {connect} from "react-redux";
import Spinner from "reactstrap/es/Spinner";
import {Button, Col, Collapse, FormGroup} from "reactstrap";
import {showMessage} from "../../../commons/utils/convertDataForToast";
import {FormattedMessage} from "react-intl";

const TableCellStyle = styled(TableCell)`
  &&&{
    display: grid;
    justify-content: center;
   }
`

class ListStudentFaild extends Component {

  constructor(props) {
    super(props);
    this.state = {
      page: 1,
      count: 1,
      searchValue: "",
      currentPage: 0,
      pageSize: 10,
      isVisible: true,
      reload: false,
      collapse: true,
      collapseListStudent:true,
      status: "Opened",
      categoryType: "2",
      typeTopic: "",
      part: "",
      codeTopic: "",
      levelCode: "",
      statusTest: "2",
      createFrom: null,
      createTo: null,
      search: showMessage("enter.information.to.search"),
      rowsPerPageOptions: [10],
      testId: 0,
      testName: '',
      typeCode: 0,
      isSearch: null
    }
  }

  resetIsSearch = () => {
    this.setState({
      isSearch: null
    })
  }
  componentDidMount() {
    this.props.testAction.getStudentFaildByTestId({testId:this.props.testId,testName:this.props.testName,page:1,pageSize:this.state.pageSize})
  }

  onChangPageReceive = (pageNumber) => {
    this.setState({
      page: pageNumber
    })
    if (pageNumber === 1) {
      this.setState({
        isSearch: 1,
      })
    }
    const searchPropertiesBE = {};
    searchPropertiesBE.testId = this.props.testId;
    searchPropertiesBE.testName = this.props.testName;
    searchPropertiesBE.pageSize = this.state.pageSize
    searchPropertiesBE.page = pageNumber
    this.props.testAction.getStudentFaildByTestId(searchPropertiesBE)
  }

  cancel = () => {
    this.props.cancel();
  }
  render() {
    const count = this.props.values.totalStudentFail;
    const columns = [
      {
        name: 'sl',
        label: showMessage("index"),
        options: {
          filter: true,
          customHeadLabelRender: () => {
            return <div style={{fontWeight: 'bold'}}><FormattedMessage id="index"/></div>;
          },
          customBodyRender: (index) => {
            return (<div>
              {index}
            </div>)
          }
        }
      },
      {
        name: 'userName',
        label: "userName",
        options: {
          filter: true,
          customHeadLabelRender: () => {
            return <div style={{fontWeight: 'bold'}}><FormattedMessage id={"account.name"}/></div>;
          },
          customBodyRender: (data) => {
            return (<div style={{
              textOverflow: 'ellipsis',
              whiteSpace: 'nowrap',
              width: '150px',
              overflow: 'hidden',
              display: 'inline-block'}} title={data}>
              {data}
            </div>)
          }
        }
      },
      {
        name: 'userShowName',
        label: "userShowName",
        options: {
          filter: true,
          customHeadLabelRender: () => {
            return <div style={{fontWeight: 'bold'}}>Account</div>;
          },
          customBodyRender: (data) => {
            return (<div style={{
              textOverflow: 'ellipsis',
              whiteSpace: 'nowrap',
              width: '150px',
              overflow: 'hidden',
              display: 'inline-block'}} title={data}>
              {data}
            </div>)
          }
        }
      },
      {
        name: 'part1',
        label: "part1",
        options: {
          filter: true,
          customHeadLabelRender: () => {
            return <div style={{fontWeight: 'bold', textAlign:'center'}}>Part 1</div>;
          },
          customBodyRender: (data) => {
            return (<div style={{textAlign:'center'}}>
              {data}
            </div>)
          }
        }
      },
      {
        name: 'part2',
        label: "part2",
        options: {
          filter: true,
          customHeadLabelRender: () => {
            return <div style={{fontWeight: 'bold', textAlign:'center'}}>Part 2</div>;
          },
          customBodyRender: (data) => {
            return (<div style={{textAlign:'center'}}>
              {data}
            </div>)
          }
        }
      },
      {
        name: 'part3',
        label: "part3",
        options: {
          filter: true,
          customHeadLabelRender: () => {
            return <div style={{fontWeight: 'bold', textAlign:'center'}}>Part 3</div>;
          },
          customBodyRender: (data) => {
            return (<div style={{textAlign:'center'}}>
              {data}
            </div>)
          }
        }
      },
      {
        name: 'part4',
        label: "part4",
        options: {
          filter: true,
          customHeadLabelRender: () => {
            return <div style={{fontWeight: 'bold', textAlign:'center'}}>Part 4</div>;
          },
          customBodyRender: (data) => {
            return (<div style={{textAlign:'center'}}>
              {data}
            </div>)
          }
        }
      },
      {
        name: 'part5',
        label: "part5",
        options: {
          filter: true,
          customHeadLabelRender: () => {
            return <div style={{fontWeight: 'bold', textAlign:'center'}}>Part 5</div>;
          },
          customBodyRender: (data) => {
            return (<div style={{textAlign:'center'}}>
              {data}
            </div>)
          }
        }
      },
      {
        name: 'part6',
        label: "part6",
        options: {
          filter: true,
          customHeadLabelRender: () => {
            return <div style={{fontWeight: 'bold', textAlign:'center'}}>Part 6</div>;
          },
          customBodyRender: (data) => {
            return (<div style={{textAlign:'center'}}>
              {data}
            </div>)
          }
        }
      },
      {
        name: 'part7',
        label: "part7",
        options: {
          filter: true,
          customHeadLabelRender: () => {
            return <div style={{fontWeight: 'bold', textAlign:'center'}}>Part 7</div>;
          },
          customBodyRender: (data) => {
            return (<div style={{textAlign:'center'}}>
              {data}
            </div>)
          }
        }
      },
      {
        name: 'totalScore',
        label: "totalScore",
        options: {
          filter: true,
          customHeadLabelRender: () => {
            return <div style={{fontWeight: 'bold', textAlign:'center'}}><FormattedMessage id={"score"}/></div>;
          },
          customBodyRender: (index) => {
            return (<div style={{textAlign:'center'}}>
              {index}
            </div>)
          }
        }
      }
    ];
    const data = this.props.values.dataStudentFaild?this.props.values.dataStudentFaild:[]
    if (this.state.page === 0) {
      this.state.page = 1;
    }
    let newData = [];
    data.map((item, index) => {
      newData.push({sl: (this.state.pageSize) * (this.state.page - 1) + index + 1, ...item});
    });

//     const TableCellStyle = styled(TableCell)`
//   &&&{
//     display: grid;
//     justify-content: center;
//    }
// `
//     const count = this.props.values.total;
    const options = {
      filter: false,
      filterType: 'checkbox',
      selectableRowsHideCheckboxes: true,
      responsive: "standard",
      selectableRows: "none",
      print: false,
      download: false,
      search: false,
      viewColumns: false,
      serverSide: true,
      textLabels: {
        body: {
          noMatch: this.props.isLoading ?
            <p/> :
            <FormattedMessage id={"not.found.data"}/>,
        },
      },
      customFooter: () => {
        return (
          <TableFooter>
            <TableRow>
              <TableCellStyle colSpan={6}>
                <PaginationComponent
                  totalItem={count}
                  onChangPage={this.onChangPageReceive}
                  checkSearch={this.state.isSearch}
                  resetIsSearch={this.resetIsSearch}
                />
              </TableCellStyle>
            </TableRow>
          </TableFooter>
        );
      }

    };



    return (
      <div>
        {
          this.props.values.isLoadingPopUp == true ?
            <div style={{
              position: 'absolute',
              zIndex: 110,
              top: 0,
              left: 0,
              width: '100%',
              height: '100%',
              display: 'flex',
              justifyContent: 'center',
              alignItems: 'center',
              background: 'rgba(255,255,255,0.8)'
            }}>
              <Spinner color="primary" className="reload-spinner"/>
            </div> : null
        }
        <MUIDataTable
          columns={columns}
          data={newData}
          options={options}
        />
      </div>
    );
  }
}

const mapStateToProps = (state) => {

  return {
    lstTest: state.testReducer.lstTest,
    isLoading: state.testReducer.isLoading,
    values: state.testReducer,
    dataFilter: state.testReducer.dataFilter,
    detailTest: state.testReducer.detailTest,
    lstPartByTopic: state.topicReducer.lstPartByTopic,
    lstTopicByTypeTopicAndPart: state.topicReducer.lstTopicByTypeTopicAndPart,
  }

}
const mapDispatchToProps = (dispatch) => {
  return {
    testAction: bindActionCreators(testAction, dispatch),
    topicAction: bindActionCreators(topicAction, dispatch)
  }
}

export default compose(withStyles(styles()), connect(mapStateToProps, mapDispatchToProps))(ListStudentFaild)
