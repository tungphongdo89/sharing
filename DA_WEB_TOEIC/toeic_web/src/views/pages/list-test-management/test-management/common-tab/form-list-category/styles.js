const styles = (theme) =>({
  buttonAdd:{
    paddingTop: '1.2em ! important',
    marginTop: '1em ! important',
    paddingLeft:'2em ! important',
  },
  buttonSearch:{
    paddingTop: '1.2em ! important',
    marginTop: '1em ! important',

  },
  search:{
    paddingLeft:'4em'
  },
  error:{
    color:'red',

  },
  QuestionComponent:{
  width: '100%',
  borderRadius: '5px',
  border: 'black 1px dashed'
},

  IndexQuestion:{
  marginLeft: '3%',
  transform: 'translate(0,60%)'
},
 buttonIcon:{
  marginLeft: '3%',
  transform: 'translate(0,50%)'
},
 QuestionContent:{
  width: '90%',
  marginLeft: '5%',
  marginTop: '5%',
}

})
export default styles;
