import React from "react"
import {Col, FormGroup, Input, Label, Row} from "reactstrap"

import {bindActionCreators, compose} from "redux";
import {connect} from "react-redux";
import {Field, Form, getIn} from "formik";
import {Switch} from "@material-ui/core";
import categoryAction from "../../../../../../redux/actions/category-action";
import Spinner from "reactstrap/es/Spinner";
import HighlightOffIcon from '@material-ui/icons/HighlightOff';
import * as Icon from "react-bootstrap-icons"
import Button2 from '@material-ui/core/Button';
import 'antd/dist/antd.css';
import {FormattedMessage} from "react-intl";

class FormGeneral extends React.Component {
  constructor(props) {
    super(props);

    this.fileInput = React.createRef();
    this.state = {
      message: ''
    }
  }

  componentDidMount() {
    const {categoryActions} = this.props;
    const {getListTypeFileUpload} = categoryActions;
    getListTypeFileUpload();
  }

  SelectTypeInput1 = props => {
    const { lstTypeFile } = this.props;
    const optionTypeFile1 = lstTypeFile ? lstTypeFile.map((type, index) => {
      if (type.paramName == 'FILE') {
        return <option selected value={type.paramName} key={index}>{type.paramName}</option>
      } else {
        return <option value={type.paramName} key={index}>{type.paramName}</option>
      }
    }) : null



    const onSelectChange = (e) => {
      let test = props.field.name;
      props.form.setFieldError("fileUpload1", undefined);
      props.form.setFieldTouched("fileUpload1", undefined);
      props.form.setFieldValue("fileUpload1", null);
      props.form.setFieldError("pathFile1", undefined);
      props.form.setFieldTouched("pathFile1", undefined);
      props.form.setFieldValue("pathFile1", null);
      props.form.setFieldValue(props.field.name, e.target.value);
      debugger
    }

    return (
      <Input onChange={onSelectChange} type="select" disabled={this.props.isViewDetail} name={props.field.name}
        //     defaultValue={detailCategory && detailCategory.categoryId ? detailCategory.typeFile1 : ""}
      >
        <option value={0}><FormattedMessage id={'select.data.type'}/></option>
        {optionTypeFile1}
      </Input>
    );
  };


  SelectTypeInput2 = props => {
    const { lstTypeFile, detailCategory } = this.props;
    const optionTypeFile2 = lstTypeFile ? lstTypeFile.map((type, index) => {
      if (type.paramName == 'LINK') {
        return <option selected key={index} value={type.paramName}>{type.paramName}</option>
      } else {
        return <option key={index} value={type.paramName}>{type.paramName}</option>
      }
    }) : null

    const onSelectChange = (e) => {
      let test = props.field.name;
      props.form.setFieldError("fileUpload2", undefined);
      props.form.setFieldTouched("fileUpload2", undefined);
      props.form.setFieldValue("fileUpload2", null);
      props.form.setFieldError("pathFile2", undefined);
      props.form.setFieldTouched("pathFile2", undefined);
      props.form.setFieldValue("pathFile2", null);
      props.form.setFieldValue(props.field.name, e.target.value);
      console.log(this.props.values);
    }

    return (
      <Input onChange={onSelectChange} type="select" disabled={this.props.isViewDetail} name={props.field.name}
        //     defaultValue={detailCategory && detailCategory.categoryId ? detailCategory.typeFile1 : ""}
      >
        <option value={0}><FormattedMessage id={"select.data.type"}/></option>
        {optionTypeFile2}
      </Input>
    );
  };

  RenderFileUpload = props => {
    const onFileChange = (e) => {
      debugger
      props.form.setFieldError(props.field.name, undefined);
      // props.form.setFieldTouched(props.field.name, undefined);
      e.preventDefault();
      let reader = new FileReader();
      let file = e.target.files[0];
      if (file) {
        reader.onloadend = () => {
        };
        props.form.setFieldValue(props.field.name, file);
        if(this.fileInput.current){
          this.fileInput.current.value = null
        }
      }
    }
    const onDeleteFile = () => {
      props.form.setFieldValue(props.field.name, null);
    }

    const onFileClick = (value) =>{
    }

    return (
      <div className="w-100">
        <input
          onClick={onFileClick}
          ref={this.fileInput}
          disabled={this.props.isViewDetail}
          name={props.field.name}
          style={{ display: "none" }}
          id={`contained-button-file ${props.field.name}`}
          type="file"
          onChange={onFileChange}
        />
        <label htmlFor={`contained-button-file ${props.field.name}`}>
          <Button2 id={props.field.name} disabled={this.props.isViewDetail} variant="contained" color="primary"
                   component="span" name={props.field.name}>
            <Icon.Upload style={{ marginRight: '5px' }} /> Upload
          </Button2>
        </label>
        <div style={{ display: 'inline-block' }}>
          {
            props.field.value?
              props.field.value.name
              : null
          }
          {
            props.field.value ?
              <HighlightOffIcon style={{ color: 'red',cursor: 'pointer' }}
                                onClick={onDeleteFile} /> : ''
          }
        </div>
      </div>
    );
  };

  render() {
    const { setFieldValue, errors, touched, handleChange, isViewDetail, detailTest} = this.props;

    const loadingComponent = (
      <div style={{
        position: 'absolute',
        zIndex: 110,
        top: 0,
        left: 0,
        width: '100%',
        height: '100%',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        background: 'rgba(255,255,255,0.8)'
      }}>
        <Spinner color="primary" className="reload-spinner"/>
      </div>
    );

    const ErrorMessage = ({ name }) => (
      <Field name={name}>
        {({ field, form, meta }) => {
          const error = getIn(form.errors, name);
          const touch = getIn(form.touched, name);
          return touch && error ? (
            <div className="invalid-feedback" style={{ display: "contents" }}>{error}</div>) : null;
        }}
      </Field>
    );

    return (
      <div>
        <Form className="mt-2">
          <FormGroup className="form-group d-flex">
            <Label for="required" sm={3}><FormattedMessage id={"test.name"}/>(<span style={{color:"red"}}>*</span>) </Label>
            <Col sm={9}>
              <div className="w-100">
                <div>
                  <Field
                    disabled={isViewDetail}
                    type="text"
                    name="nameTest"
                    className={`form-control ${errors.nameTest &&
                    touched.nameTest &&
                    "is-invalid "}`}
                    defaultValue={detailTest && detailTest.id ? detailTest.name : ""}
                  />
                  {errors.nameTest && touched.nameTest ? (
                    <div className="invalid-feedback mt-25">{errors.nameTest}</div>
                  ) : null}
                </div>
              </div>
            </Col>
          </FormGroup>
          <FormGroup className="form-group d-flex">
              <Label for="required" sm={3}><FormattedMessage id={"status"}/>(<span style={{color:"red"}}>*</span>) </Label>
              <Col sm={9}>
                <div className="w-100">
                  <div>
                    <Switch
                      disabled={isViewDetail}
                      name="status"
                      color="primary"
                      defaultChecked={this.props.values.status}
                      onChange={(event, checked) => {
                        setFieldValue("status", checked ? 1 : 0);
                      }}
                    />
                    {errors.status && touched.status ? (
                      <div className="invalid-feedback mt-25">{errors.status}</div>
                    ) : null}
                  </div>

                </div>
              </Col>
          </FormGroup>
          <FormGroup className="form-group d-flex">
            <Label for="required" sm={3}>Đoạn nghe(<span style={{color:"red"}}>*</span>) </Label>
            <Col sm={9}>
              <Row className="mb-1">
                <Col sm={5}>
                  <div className="w-100">
                    <Field
                           name="typeFile1"
                           className="form-control" component={this.SelectTypeInput1}
                    />
                  </div>
                </Col>
                <Col sm={7}>
                  <div>
                  {
                    this.props.values.typeFile1 === "FILE"?
                      <Field
                        disabled={isViewDetail}
                        name="fileUpload1"
                        setFieldValue={this.props.setFieldValue}
                        errorMessage={errors[`fileUpload1`] ? errors[`fileUpload1`] : undefined}
                        touched={touched[`fileUpload1`]}
                        style={{ display: "flex" }}
                        onBlur={this.props.handleBlur}
                        component={this.RenderFileUpload}
                      />:
                      <Field as="textarea"
                             onChange={handleChange}
                             className={`form-control ${errors.pathFile1 &&
                             touched.pathFile1 &&
                             "is-invalid "}`}
                             name="pathFile1" rows="4"
                             disabled={isViewDetail}
                      />
                  }
                    {/*{this.props.detailTest.id*/}
                    {/*&& values.fileUpload1 == null ? this.props.detailTestTemp ?*/}
                      {/*this.props.detailTestTemp.typeFile1 == "FILE" ?*/}
                        {/*this.props.detailTestTemp.pathFile1 : null : null : null*/}
                    {/*}*/}
                  </div>
                  <ErrorMessage name="fileUpload1" />
                </Col>
              </Row>
              <Row>
                <Col sm={5}>
                  <div className="w-100">
                    <Field
                      name="typeFile2"
                      className="form-control" component={this.SelectTypeInput2}
                    />
                  </div>
                </Col>
                <Col sm={7}>
                  {
                    this.props.values.typeFile2 === "FILE"?
                      <Field
                        disabled={isViewDetail}
                        name="fileUpload2"
                        setFieldValue={this.props.setFieldValue}
                        errorMessage={errors[`fileUpload2`] ? errors[`fileUpload2`] : undefined}
                        touched={touched[`fileUpload2`]}
                        style={{ display: "flex" }}
                        onBlur={this.props.handleBlur}
                        component={this.RenderFileUpload}
                      />:
                      <Field as="textarea"
                             onChange={handleChange}
                             className={`form-control ${errors.pathFile2 &&
                             touched.pathFile2 &&
                             "is-invalid "}`}
                             name="pathFile2" rows="4"
                             disabled={isViewDetail}
                      />
                  }
                  <ErrorMessage name="fileUpload2" />
                  {/*{errors.pathFile && touched.pathFile ? (*/}
                  {/*  <div className="invalid-feedback mt-25">{errors.pathFile}</div>*/}
                  {/*) : null}*/}
                </Col>
              </Row>
            </Col>
          </FormGroup>
        </Form>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    lstTypeFile: state.categoryReducer.lstTypeFile,
    isViewDetail: state.testReducer.isViewDetail,
    detailTest: state.testReducer.detailTest,
    detailTestTemp: state.testReducer.detailTestTemp
  }
}

const mapDispatchToProps = dispatch => ({
  categoryActions: bindActionCreators(categoryAction, dispatch)
});

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
);

export default compose(
  withConnect
)(FormGeneral);
