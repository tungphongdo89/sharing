import React from "react"
import {Button, Card, CardBody, Col, FormGroup, Input, Label, Row} from "reactstrap"

import {bindActionCreators, compose} from "redux";
import {connect} from "react-redux";
import {Field, Form} from "formik";
import {Switch, TableCell, TableFooter, TableRow, withStyles} from "@material-ui/core";
import categoryAction from "../../../../../../redux/actions/category-action";
import Spinner from "reactstrap/es/Spinner";
import MUIDataTable from "mui-datatables";
import 'antd/dist/antd.css';
import './style.css';
import PaginationIconsAndText from './pagingtion';
import {Link} from "react-router-dom";
import {history} from "../../../../../../history";
import {getUTCTime} from "../../../../../../commons/utils/DateUtils";
import {Edit, Eye, Search, Trash2} from "react-feather";
import OverlayTrigger from "react-bootstrap/OverlayTrigger";
import Tooltip from "react-bootstrap/Tooltip";
import topicAction from "../../../../../../redux/actions/topic/topicAction";
import testActions from "../../../../../../redux/actions/test";
import styles from './styles'
import {showMessage} from "../../../../../../commons/utils/convertDataForToast";
import {FormattedMessage} from "react-intl";

let optionPartByTopic;
let optionNameTopicByPartAndTypeTopic;
let optionTypeTopic;
let optionLevelCode;

class FormListCategory extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      page: 0,
      count: 1,
      searchValue: "",
      pageSize: 10,
      isVisible: true,
      reload: false,
      collapse: true,
      status: "Opened",
      categoryType: "2",
      typeTopic: "",
      part: "",
      codeTopic: "",
      levelCode: "",
      statusCategory: "2",
      createFrom: "",
      createTo: "",
      search: showMessage("enter.information.to.search"),
      rowsPerPageOptions: [10],
      categoryId: 0,
      typeCode: 0,
      listChecked: []
    }
  }

  componentWillMount() {
    let data = {};
    const {categoryAction, topicAction} = this.props;
    const {fetchDataFiler} = categoryAction;
    const {fetchPartByTopic, fetchTopicNameByPartAndTypeTopic} = topicAction;
    fetchDataFiler(data);
    fetchPartByTopic(data);
    fetchTopicNameByPartAndTypeTopic(data);

  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    const {total} = prevProps;

    if (total) {
      this.setState({
        count: total
      });
    }


  }

  UNSAFE_componentWillReceiveProps(nextProps, nextContext) {
    const {total} = nextProps.values.dataRes;

    if (total) {
      this.setState({
        count: total
      });
    }
    const {getLevelCode, lstTypeTopic} = nextProps.dataFilter;

    optionTypeTopic = lstTypeTopic !== undefined ? lstTypeTopic.map((type) => {
      return <option value={type.code}>{type.name}</option>
    }) : null;
    optionLevelCode = getLevelCode !== undefined ? getLevelCode.map((type) => {
      return <option value={type.code}>{type.name}</option>
    }) : null;
  }

  componentWillUpdate(nextProps, nextState, nextContext) {
    const {lstPartByTopic} = nextProps;
    optionPartByTopic = lstPartByTopic !== undefined ? lstPartByTopic.map((type) => {
      return <option value={type.code}>{type.name}</option>
    }) : null
    const {lstTopicByTypeTopicAndPart} = nextProps;
    optionNameTopicByPartAndTypeTopic = lstTopicByTypeTopicAndPart !== undefined ? lstTopicByTypeTopicAndPart.map((type) => {
      return <option value={type.code}>{type.topicName}</option>
    }) : null
  }

  componentDidMount() {
    let category = {};
    category.page = this.state.page + 1;
    category.pageSize = this.state.pageSize;
    const {testAction} = this.props;
    const {getListCategory} = testAction;
    getListCategory(category);
    const {values} = this.props;
    const {total} = values;
    this.setState({
      count: total
    })
  }
  getCurrentPage = (currentPage)=>{
    this.changePage(currentPage);
  }

  toggleModal = (categoryId,typeCode) => {
    this.setState(prevState => ({
      categoryId: categoryId,
      typeCode: typeCode,
      modal: !prevState.modal
    }))
  }

  changePage = (page) => {
    const {pageSize} = this.state;
    this.setState({
      page: page
    })
    let category = {};
    category.page = page
    category.pageSize = pageSize
    const {testAction} = this.props;
    const {getListCategory} = testAction;
    getListCategory(category);
  };
  getCategoryFromState = () => {
    let category = {};
    category.keySearch = this.state.searchValue.toString().trim();
    category.status = parseInt(this.state.statusCategory);
    category.levelCode = this.state.levelCode;
    category.part = this.state.part;
    category.codeTopic = this.state.codeTopic;
    category.code = this.state.typeTopic;
    category.createdDate = this.state.createFrom;
    category.updatedDate = this.state.createTo;
    category.page = 1;
    category.pageSize = 10;
    return category;
  }

  render() {
    const { setFieldValue, errors, touched, handleChange, listCheckedCom} = this.props;
    const {detailTest, isViewDetail} = listCheckedCom;
    const {page, rowsPerPageOptions, count, categoryId,typeCode} = this.state;
    const {classes} = this.props;
    const options = {
      filter: false,
      filterType: 'checkbox',
      selectableRowsHideCheckboxes: true,
      responsive: "standard",
      selectableRows: "none",
      print: false,
      download: false,
      // count: count,
      search: false,
      viewColumns: false,
      // page: page,
      serverSide: true,
      // rowsPerPageOptions: rowsPerPageOptions,
      customFooter: () => {
        return (
          <TableFooter>
            <TableRow>
              <TableCell colSpan={6}>
                <PaginationIconsAndText count={count} getCurrentPage ={ (currentPage)=>{this.getCurrentPage(currentPage)}}/>
              </TableCell>
            </TableRow>
          </TableFooter>
        );
      }
    };
    const columns = [
      {
        name: "",
        label: "",
        options: {
          filter: true,
          customBodyRender: (data, dataRow) => {
            return (<FormGroup >
              <Label check >
                <Input type="checkbox"  style={{marginLeft: '4px',
                  marginTop: '-2px'}}
                       checked={ listCheckedCom.detailTest && listCheckedCom.detailTest.lst ? true : listCheckedCom.listCheckedListening || listCheckedCom.listCheckedReading ?
                         listCheckedCom.listCheckedListening.indexOf(lstCategory[dataRow.rowIndex]) !== -1 ||
                         listCheckedCom.listCheckedReading.indexOf(lstCategory[dataRow.rowIndex]) !== -1
                           ? true : false : false}
                       disabled={listCheckedCom.isViewDetail}
                       onChange={()=> {
                         debugger
                         const {testAction} = this.props;
                         const {updateListCheckedListening, updateListCheckedReading} = testAction;
                         if(lstCategory[dataRow.rowIndex].typeCode === 2){
                           updateListCheckedListening(lstCategory[dataRow.rowIndex])
                         } else {
                           updateListCheckedReading(lstCategory[dataRow.rowIndex])
                         }
                       }
                       }/>{' '}
              </Label>

            </FormGroup>)
          }
        }
      },
      {
        name: "categoryId",
        label: showMessage("category.code"),
        options: {
          filter: true,
          customBodyRender: (data) => {
            return (<div>
              {data}
            </div>)
          }
        }
      },
      {
        name: "nameCategory",
        label: showMessage("category.name"),
        options: {
          filter: true,
          sort: false,
          customBodyRender: (data, dataRow) => {

            return (<Link className="text-center"
                          onClick={(e) => {
                            const {categoryAction, lstCategory} = this.props;
                            const {fetchDetailCategory} = categoryAction;
                            fetchDetailCategory(lstCategory[dataRow.rowIndex], true);
                            history.push("/pages/category-form")
                          }
                          }>{data}</Link>)
          }
        }
      },
      {
        name: "levelCode",
        label: showMessage("level"),
        options: {
          filter: true,
          sort: false,
          customBodyRender: (data) => {
            return data === 'EASY' ? (<div><FormattedMessage id={"easy"}/></div>) :
              data === 'MEDIUM' ? (<div><FormattedMessage id={"medium"}/></div>) :
                data === 'DIFFICULT' ? (<div><FormattedMessage id={"difficult"}/></div>) : null
          }
        }
      },
      {
        name: "typeName",
        label: showMessage("type.of.category"),
        options: {
          filter: true,
          sort: false,
        }
      },
       {
        name: "nameTopic",
        label: showMessage("topic.name"),
        options: {
          filter: true,
          sort: false,
        }
      }, {
        name: "countQuestion",
        label: showMessage("num.of.ques"),
        options: {
          filter: true,
          sort: false,
        }
      },
      {
        name: "countScore",
        label: showMessage("total.score"),
        options: {
          filter: true,
          sort: false,
        }
      }
    ];
    const {lstCategory, isLoading} = this.props;

    const loadingComponent = (
      <div style={{
        position: 'absolute',
        zIndex: 110,
        top: 0,
        left: 0,
        width: '100%',
        height: '100%',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        background: 'rgba(255,255,255,0.8)'
      }}>
        <Spinner color="primary" className="reload-spinner"/>
      </div>
    );

    return (
      <div>
        <Card>
          <CardBody>
            <Form>
              <FormGroup>
                {
                  detailTest ? null :
                    <Row>
                      <Col xs="2">
                        <FormattedMessage id={"total.score.provisionally"}/>
                      </Col>
                      <Col xs="4">
                        <Field disabled name="totalScore" id="totalScore"
                               className="form-control"/>
                      </Col>
                      <Col xs="2">
                        <FormattedMessage id={"target.score"}/>(*)
                      </Col>
                      <Col xs="4">
                        <Field
                          // disabled={this.props.isViewDetail}
                          name="targetScore" id="targetScore"
                          className={`form-control ${this.props.errors.targetScore &&
                          this.props.touched.targetScore &&
                          "is-invalid"}`}
                        />
                        {this.props.errors.targetScore && this.props.touched.targetScore ? (
                          <div className="invalid-feedback mt-25">{this.props.errors.targetScore}</div>
                        ) : null}
                      </Col>
                    </Row>
                }
                <Row>
                  <div className={classes.QuestionComponent} style={{marginTop: '16px'}}>
                    { detailTest ? null :
                      <div>
                        <FormGroup className="form-group d-flex  mt-1">
                          <Col sm={3} md={3} lg={3} xs={3} className="ml-3">
                            <span>&nbsp;</span>
                            <FormGroup className="position-relative has-icon-left">
                              <div className="w-100">
                                <Input
                                  type="search"
                                  name="searchValue"
                                  id="searchValue"
                                  placeholder={this.state.search}
                                  onChange={e => {
                                    this.setState(
                                      {
                                        searchValue: e.target.value
                                      }
                                    )
                                  }}
                                />
                                <div className="form-control-position px-1">
                                  <Search size={15}/>
                                </div>
                              </div>
                            </FormGroup>
                          </Col>
                          <Col sm={3} md={3} lg={3} xs={3}>
                            <FormGroup className="mb-0">
                              <div>
                                <Label className={classes.fieldLabel} for="status"><FormattedMessage id={"status"}/></Label>
                                <Input
                                  type="select"
                                  name="statusCategory"
                                  id="statusCategory"
                                  value={this.state.statusCategory}
                                  onChange={e => {
                                    this.setState(
                                      {
                                        statusCategory: e.target.value
                                      }
                                    )
                                  }}
                                >
                                  <option defaultChecked value="2">{showMessage("all")}</option>
                                  <option value="1">{showMessage("in.use")}</option>
                                  <option value="0">{showMessage("not.used")}</option>
                                </Input>
                              </div>
                            </FormGroup>
                          </Col>
                          <Col sm={3} md={3} lg={3} xs={3}>
                            <span><FormattedMessage id={"type.of.category"}/></span>
                            <Input
                              type="select"
                              name="categoryType"
                              id="categoryType"
                              value={this.state.typeTopic}
                              onChange={e => {
                                let apParamDTO = {};
                                let topic = {};
                                const {topicAction} = this.props;
                                const {fetchPartByTopic, fetchTopicNameByPartAndTypeTopic} = topicAction;
                                this.setState(
                                  {
                                    typeTopic: e.target.value
                                  }
                                )
                                topic.partTopicCode = this.state.part;
                                topic.typeTopicCode = e.target.value;
                                apParamDTO.parentCode = e.target.value;
                                fetchTopicNameByPartAndTypeTopic(topic);
                                fetchPartByTopic(apParamDTO);
                                this.setState({reload: true})
                                setTimeout(() => {
                                  this.setState({
                                    reload: false,
                                  })
                                }, 1000)
                              }}
                            >
                              <option defaultChecked value="">{showMessage("all")}</option>
                              {optionTypeTopic}
                            </Input>
                          </Col>

                          <Col sm={3} md={3} lg={3} xs={3}>
                            <FormGroup style={{paddingTop: '0.5em'}} className="mb-0">
                              <Button type="submit" className={classes.buttonSearch} color="primary"
                                      onClick={() => {

                                        let category = this.getCategoryFromState();
                                        const {categoryAction} = this.props;
                                        const {fetchCategory} = categoryAction;
                                        fetchCategory(category);
                                      }}
                              >Tìm</Button>
                            </FormGroup>
                          </Col>
                        </FormGroup>
                        <FormGroup className="form-group d-flex">
                          <Col sm={3} md={3} lg={3} xs={3} className="ml-3">
                            <FormGroup className="mb-0">
                              <Label className={classes.fieldLabel} for="department"><FormattedMessage id={'topic'}/></Label>
                              <Input
                                type="select"
                                name="topicName"
                                value={this.state.codeTopic}
                                onChange={e => {
                                  this.setState(
                                    {
                                      codeTopic: e.target.value
                                    }
                                  )
                                }}
                              >
                                <option defaultChecked value="">{showMessage("all")}</option>
                                {optionNameTopicByPartAndTypeTopic}
                              </Input>
                            </FormGroup>
                          </Col>
                          <Col sm={3} md={3} lg={3} xs={3}>
                            <FormGroup className="mb-0">
                              <div className="w-100">
                                <Label className={classes.fieldLabel} for="status"><FormattedMessage id={"date.created"}/></Label>
                                <Input
                                  type="date"
                                  name="createTo"
                                  id="createTo"
                                  value={this.state.createTo}
                                  className={`form-control ${errors.createDateTo &&
                                  touched.createDateTo &&
                                  "is-invalid"}`}
                                  onChange={e => {
                                    this.setState(
                                      {
                                        createTo: e.target.value
                                      }
                                    )
                                  }}
                                />
                                {errors.createDateTo && touched.createDateTo ? (
                                  <div className={classes.error}>{errors.createDateTo}</div>
                                ) : null}
                              </div>
                            </FormGroup>
                          </Col>
                          <Col sm={3} md={3} lg={3} xs={3}>
                            <FormGroup className="mb-0">
                              <div className="w-100">
                                <Label className={classes.fieldLabel} for="status"><FormattedMessage id={'to.date'}/></Label>
                                <Input
                                  type="date"
                                  name="createFrom"
                                  id="createFrom"
                                  value={this.state.createFrom}
                                  className={`form-control ${errors.createDateTo &&
                                  touched.createDateTo &&
                                  "is-invalid"}`}
                                  onChange={e => {
                                    this.setState(
                                      {
                                        createFrom: e.target.value
                                      }
                                    )
                                  }}
                                />
                                {errors.createDateTo && touched.createDateTo ? (
                                  <div className={classes.error}>{errors.createDateTo}</div>
                                ) : null}
                              </div>
                            </FormGroup>
                           </Col>
                        </FormGroup>
                      </div>
                    }
                    <Col sm="12">
                      <div style={{position: 'relative'}}>
                        {isLoading ? loadingComponent:null}

                        <MUIDataTable
                          columns={columns}
                          data={isViewDetail && detailTest && detailTest.lst ? detailTest.lst : lstCategory}
                          options={options}
                        />
                      </div>
                    </Col>
                  </div>
                </Row>
              </FormGroup>
            </Form>
          </CardBody>
        </Card>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    lstCategory: state.testReducer.listCategory.data,
    isLoading: state.categoryReducer.isLoading,
    values: state.categoryReducer,
    dataFilter: state.categoryReducer.dataFilter,
    detailCategory: state.categoryReducer.detailCategory,
    lstPartByTopic: state.topicReducer.lstPartByTopic,
    lstTopicByTypeTopicAndPart: state.topicReducer.lstTopicByTypeTopicAndPart,
    listCheckedCom : state.testReducer,
  }
}

const mapDispatchToProps = dispatch => ({
  categoryAction: bindActionCreators(categoryAction, dispatch),
  topicAction: bindActionCreators(topicAction, dispatch),
  testAction: bindActionCreators(testActions, dispatch)
});

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
);

export default compose(withStyles(styles()),
  withConnect
)(FormListCategory);
