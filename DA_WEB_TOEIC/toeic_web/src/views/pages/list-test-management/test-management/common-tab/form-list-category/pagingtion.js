import React from "react"
import {Pagination, PaginationItem, PaginationLink} from "reactstrap"

class PaginationIconsAndText extends React.Component {
  state = {
    activeTab: "1",
    currentPage: 1,
    endPage: 1,
  }
  getCurrentPage = (page) => {
    this.props.getCurrentPage(page)
  }
  generationPage = (pageCount) => {
    let pagingPage = [];
    for (let i = 1; i <= pageCount; i++) {
      pagingPage.push(
        <PaginationItem active={i === this.state.currentPage ? true : false}>
          <PaginationLink value={i} onClick={() => {
            this.setState({
              currentPage: i
            })
            this.getCurrentPage(i)
          }}
          >{i}</PaginationLink>
        </PaginationItem>)
    }
    return pagingPage;

  }

  customPaging = (count) => {
    let page = Math.ceil(count / 10);
    if (page > 5) {
      return this.generationPage(5)
    } else {
      return this.generationPage(page)
    }
  }

  render() {
    debugger
    const {count} = this.props;
    let pageCount = Math.ceil(count / 10);
    return (
      <React.Fragment>
        <Pagination className="d-flex justify-content-center mt-3">
          <PaginationItem>
            <PaginationLink onClick={() => {
              const {currentPage} = this.state;
              if (currentPage !== 1) {
                this.setState({
                  currentPage: currentPage - 1
                })
                this.getCurrentPage(currentPage - 1)
              }
            }}>
              Previous
            </PaginationLink>
          </PaginationItem>
          {this.customPaging(count)}
          <PaginationItem>
            <PaginationLink onClick={() => {
              let {currentPage} = this.state;
              if (currentPage < pageCount) {
                this.setState({
                  currentPage: currentPage + 1
                })
                this.getCurrentPage(currentPage + 1)
              }
            }}>
              Next
            </PaginationLink>
          </PaginationItem>
        </Pagination>
      </React.Fragment>
    )
  }
}

export default PaginationIconsAndText
