import React from "react"
import {Button, Col, FormGroup, Input, Label,Row, Table} from "reactstrap"

import {bindActionCreators, compose} from "redux";
import {connect} from "react-redux";
import categoryAction from "../../../../../../redux/actions/category-action";
import Spinner from "reactstrap/es/Spinner";
import 'antd/dist/antd.css';
import './style.css';
import MenuIcon from '@material-ui/icons/Menu';
import testActions from "../../../../../../redux/actions/test";
import { DragDropContext, Droppable, Draggable } from "react-beautiful-dnd";
import styles from './style'
import {withStyles} from "@material-ui/core";
import {FormattedMessage} from "react-intl";


class FormStructure extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
    }
  }

  componentDidMount() {

  }
  removeCategory = (item) =>{
    const {testAction}= this.props;
    const {updateListCheckedListening, updateListCheckedReading} = testAction;
    if(item.typeCode === 2){
      updateListCheckedListening(item);
    } else {
      updateListCheckedReading(item);
    }
  }



  render() {
    const { setFieldValue, errors, touched, handleChange, classes} = this.props;

    const loadingComponent = (
      <div style={{
        position: 'absolute',
        zIndex: 110,
        top: 0,
        left: 0,
        width: '100%',
        height: '100%',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        background: 'rgba(255,255,255,0.8)'
      }}>
        <Spinner color="primary" className="reload-spinner"/>
      </div>
    );

    let lstCheckedListening = this.props.listCheckedCom.listCheckedListening ? this.props.listCheckedCom.listCheckedListening.map((item, index)=> {
      return (
        <tr>
          <td style={{width:'40%'}}>{item.nameCategory}</td>
          <td style={{width:'30%'}}>{item.countQuestion} <FormattedMessage id={"question1"}/></td>
          <td style={{color: '#5e50ee', width:'20%'}}>
            <button style={{border: 'none',
              backgroundColor: 'white'}} onClick={()=>this.removeCategory(item)}
            >Bỏ chọn</button></td>
        </tr>
      )
    }) : null
    let lstListeningDetailTemp = [];
    let lstReadingDetailTemp = [];
    let lst = this.props.listCheckedCom.detailTest ? this.props.listCheckedCom.detailTest.lst ? this.props.listCheckedCom.detailTest.lst.map((item, index)=>{
      if(item.typeCode === 2){
        lstListeningDetailTemp.push(item)
      } else {
        lstReadingDetailTemp.push(item)
      }
      }) : null : null

    let lstListeningDetail = lstListeningDetailTemp.map((item, index)=>{
      return (
        <tr >
          <td style={{width:'40%'}}>{item.nameCategory}</td>
          <td style={{width:'30%'}}>{item.countQuestion} <FormattedMessage id={"question1"}/></td>
        </tr>
      )
    })


    let lstReadingDetail = lstReadingDetailTemp.map((item, index)=>{
      return (
        <tr >
          <td style={{width:'40%'}}>{item.nameCategory}</td>
          <td style={{width:'30%'}}>{item.countQuestion} <FormattedMessage id={"question1"}/></td>
        </tr>
      )
    })


    let lstCheckedReading = this.props.listCheckedCom.listCheckedReading ? this.props.listCheckedCom.listCheckedReading.map((item, index)=> {
      return (
        <tr >
          <td style={{width:'40%'}}>{item.nameCategory}</td>
          <td style={{width:'30%'}}>{item.countQuestion} <FormattedMessage id={"question1"}/></td>
          <td style={{color: '#5e50ee', width:'20%'}}>
            <button style={{border: 'none',
              backgroundColor: 'white'}} onClick={()=>this.removeCategory(item)}
            ><FormattedMessage id={"unchecked"}/></button></td>
        </tr>
      )
    }) : null


    return (
      <div>
        <Row className=" mb-3">
          <Col sm={2}>
            <h2> <FormattedMessage id={"listening.section"}/></h2>
            <div className={classes.vr}>&nbsp;</div>
          </Col>
          <Col sm={10}>
            <Table>
              <tbody>
              {
                this.props.listCheckedCom.isViewDetail ? lstListeningDetail : lstCheckedListening

              }
              </tbody>
            </Table>
          </Col>
        </Row>
        <Row className="mb-3">
          <Col sm={2}>
            <h2><FormattedMessage id={"reading.section"}/></h2>
            <div className={classes.vr}>&nbsp;</div>
          </Col>
          <Col sm={10}>
            <Table>
              <tbody>
              {
                this.props.listCheckedCom.isViewDetail ? lstReadingDetail  : lstCheckedReading
              }
              </tbody>
            </Table>

          </Col>
        </Row>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    listCheckedCom : state.testReducer
  }
}

const mapDispatchToProps = dispatch => ({
  testAction: bindActionCreators(testActions, dispatch)
});

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
);

export default compose(withStyles(styles()),
  withConnect
)(FormStructure);
