import React from "react"
import {Button, Card, CardHeader, Nav, NavItem, NavLink, TabContent, TabPane,} from "reactstrap"

import {bindActionCreators, compose} from "redux";
import {connect} from "react-redux";
import {Typography} from "@material-ui/core";
import CardBody from "reactstrap/es/CardBody";
import FormGeneral from "./common-tab/form-general";
import FormListCategory from "./common-tab/form-list-category";
import {Form, Formik} from "formik";
import FormStructure from './common-tab/form-structure';
import * as Yup from "yup";
import {toast} from "react-toastify";
import Spinner from "reactstrap/es/Spinner";
import testActions from "../../../../redux/actions/test";
import {showMessage} from "../../../../commons/utils/convertDataForToast";
import {FormattedMessage} from "react-intl";


const FILE_SIZE = 5 * 1024 * 1024;
const SUPPORTED_FORMATS = [
  "image",
  "audio"
];
const SUPPORTED_FORMATS_TEST = [
  "image/jpg",
  "image/jpeg",
  "image/png",
  "audio/mpeg",
  "audio/mp3"
];

const formSchema = Yup.object().shape({
  nameTest: Yup.string().required(showMessage("test.name.cannot.be.empty")),
  fileUpload1: Yup
    .mixed()
    .test(
      "fileSize",
      showMessage("file.size.is.too.big"),
      value => value ? value.size <= FILE_SIZE ? true : false : true
    )
    .test(
      "fileFormat",
      showMessage("incorrect.file.upload"),
      (value) =>
        value ? SUPPORTED_FORMATS_TEST.includes(value.type) ? true : false : true
    )
    .nullable(),
  fileUpload2: Yup
    .mixed()
    .test(
      "fileSize",
      showMessage("file.size.is.too.big"),
      value => value ? value.size <= FILE_SIZE ? true : false : true
    )
    .test(
      "fileFormat",
      showMessage("incorrect.file.upload"),
      (value) =>
        value ? SUPPORTED_FORMATS_TEST.includes(value.type) ? true : false : true
    )
    .nullable(),
  // pathFile1: Yup.string().required("Không được để trống").nullable(),
  // pathFile2: Yup.string().required("Không được để trống").nullable(),

})

class TestForm extends React.Component {

  state = {
    active: "1",
    isLoading: false
  }

  componentDidMount() {
    const {testAction} = this.props;
    const {deleteListChecked} = testAction;
    deleteListChecked();

  }

  toggle = tab => {
    if (this.state.active !== tab) {
      this.setState({active: tab})
    }
  }

  componentWillReceiveProps(nextProps, nextContext) {
    const {loading} = nextProps;
    if (!loading) {
      this.setState({
        isLoading: false
      });
    }
  }

  render() {
    const {listCheckedCom} = this.props;
    const {detailTest, isViewDetail} = this.props;
    return (
      <div>
        <Formik
          enableReinitialize
          initialValues={{
            nameTest: detailTest ? detailTest.name : '',
            status: detailTest ? detailTest.status : 1,
            typeFile1: 'FILE',
            fileUpload1: detailTest ? detailTest.fileUpdate1 : null,
            fileUpload2: detailTest ? detailTest.fileUpdate2 : null,
            typeFile2: 'LINK',
            pathFile1: detailTest ? detailTest.pathFile1 : '',
            pathFile2: detailTest ? detailTest.pathFile2 : ''
          }}
          validationSchema={formSchema}
          onSubmit={(values, {setSubmitting}) => {
            setSubmitting(false);
            let formData = new FormData();
            debugger;
            formData.append("name", values.nameTest)//tên test
            formData.append("status", values.status)//trạng thái test
            if (values.fileUpload1 !== null && values.fileUpload1 !== undefined) {
              formData.append("fileUpdate1", values.fileUpload1)
            }
            if (values.fileUpload2 !== null && values.fileUpload2 !== undefined) {
              formData.append("fileUpdate2", values.fileUpload2)
            }
            formData.append("typeFile1", "FILE")
            formData.append("typeFile2", "LINK")
            formData.append("pathFile1", values.pathFile1)
            formData.append("pathFile2", values.pathFile2)
            if (listCheckedCom.listCheckedListening && listCheckedCom.listCheckedReading) {
              let lstCategory = [];
              let count = 0;
              for (let i = 0; i < listCheckedCom.listCheckedListening.length; i++) {
                formData.append("lst[" + count + "].categoryId", listCheckedCom.listCheckedListening[i].categoryId);
                count++
              }
              for (let j = 0; j < listCheckedCom.listCheckedReading.length; j++) {
                formData.append("lst[" + count + "].categoryId", listCheckedCom.listCheckedReading[j].categoryId);
                count++
              }
            }
            console.log(formData);
            this.props.testAction.createTest(formData);

          }}
        >
          {({errors, touched, values, handleChange, setFieldValue, handleBlur}) => (
            <Form>
              <Card>
                <CardHeader>
                  <div className=" w-100">
                    <Typography variant="h4" component="h2" gutterBottom style={{float: "left"}}>
                      {isViewDetail && detailTest && detailTest.id ? <FormattedMessage id={"test.details"}/>
                        : (detailTest && detailTest.id ? <FormattedMessage id={"edit.the.test"}/> :
                          <FormattedMessage id={"add.new.test"}/>)}
                    </Typography>
                    <div style={{float: "right"}}>
                      {
                        this.state.isLoading && <Spinner color="primary" className="mr-1"/>
                      }
                      <Button color="warning">Preview</Button>
                      <Button color="primary" className="ml-1" type="submit" onClick={() => {
                        if (JSON.stringify(errors).length > 2) {
                          toast.warn(showMessage("information.require"));
                        } else {
                          return null;
                        }
                      }}><FormattedMessage id={"save"}/></Button>

                    </div>
                  </div>
                </CardHeader>
                <CardBody>
                  <Nav tabs>
                    <NavItem>
                      <NavLink
                        className={({
                          active: this.state.active === "1"
                        })}
                        style={{fontSize: "1.25rem"}}
                        onClick={() => {
                          this.toggle("1")
                        }}

                      >
                        <FormattedMessage id={"general.information"}/>
                      </NavLink>
                    </NavItem>
                    <NavItem>
                      <NavLink
                        className={({
                          active: this.state.active === "2"
                        })}
                        style={{fontSize: "1.25rem"}}
                        onClick={() => {
                          this.toggle("2")
                        }}

                      >
                        <FormattedMessage id={"list.of.topic"}/>
                      </NavLink>
                    </NavItem>
                    <NavItem>
                      <NavLink
                        className={({
                          active: this.state.active === "3"
                        })}
                        style={{fontSize: "1.25rem"}}
                        onClick={() => {
                          this.toggle("3")
                        }}
                      >
                        <FormattedMessage id={"structure.of.test"}/>
                      </NavLink>
                    </NavItem>
                  </Nav>
                  <TabContent activeTab={this.state.active}>
                    <TabPane tabId="1">
                      <FormGeneral
                        values={values}
                        errors={errors}
                        touched={touched}
                        handleChange={handleChange}
                        setFieldValue={setFieldValue}
                        handleBlur={handleBlur}
                      />
                    </TabPane>
                    <TabPane tabId="2">
                      <FormListCategory
                        values={values}
                        errors={errors}
                        touched={touched}
                        setFieldValue={setFieldValue}
                        handleChange={handleChange}
                        handleBlur={handleBlur}
                      />
                    </TabPane>
                    <TabPane tabId="3">
                      <FormStructure
                        values={values}
                        errors={errors}
                        touched={touched}
                        handleChange={handleChange}
                        setFieldValue={setFieldValue}
                        handleBlur={handleBlur}
                      />
                    </TabPane>
                  </TabContent>
                </CardBody>
              </Card>
            </Form>
          )}
        </Formik>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    listCheckedCom: state.testReducer,
    isViewDetail: state.testReducer.isViewDetail,
    detailTest: state.testReducer.detailTest,
  }
}

const mapDispatchToProps = dispatch => ({
  testAction: bindActionCreators(testActions, dispatch)
})

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
);

export default compose(
  withConnect
)(TestForm);

