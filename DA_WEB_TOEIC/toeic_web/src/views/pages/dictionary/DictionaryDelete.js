import React, {Component} from 'react';
import {
  Dialog,
  IconButton
} from '@material-ui/core';

import ClearIcon from '@material-ui/icons/Clear';
import {Field, Form, Formik} from "formik";
import {Button, Col, FormGroup, Label, ModalHeader} from "reactstrap";
import '../../../assets/scss/pages/vocabulary.scss';
import Spinner from "reactstrap/es/Spinner";
import {compose} from "redux";
import {connect} from "react-redux"
import {bindActionCreators} from "redux";


import dictionaryAction from "../../../redux/actions/dictionary/dictionaryAction";
import {FormattedMessage} from "react-intl";

class DictionaryDelete extends Component {


  render() {
    const {open, id, dictionaryAction} = this.props
    return (
      <Dialog open={open}
              maxWidth='sm' fullWidth={true}
              onKeyDown={(e) => {
                if (e.keyCode === 27) {
                  this.props.onConfirmDialogClose()
                }
              }}
              onClose={()=>{
                this.props.onConfirmDialogClose()
              }}
      >
        <div className="flex flex-space-between flex-middle  bg-primary ">
          <ModalHeader className='text-centesr'>
            <h3 style={{color: 'white'}}> <FormattedMessage id={"remove.vocabulary"}/></h3>
          </ModalHeader>
          <IconButton onClick={() => this.props.onConfirmDialogClose()}>
            <ClearIcon/>
          </IconButton>
        </div>

        <Formik
          initialValues={{
            nameEng: '',
            transcribe: '',
          }}
          onSubmit={(values) => {
            this.props.backToFirstPage();
            dictionaryAction.deleteDictionary(id)

            setTimeout(() => {
              this.props.onConfirmDialogClose()
              this.props.resetDeleteOrAddPage();
            }, 1000)
          }}>
            <Form>
              <FormGroup className="form-group d-flex text-center">
                <Col sm={12} md={12} lg={12} xl={12} xs={12}>
                  <p style={{fontSize: '16px', fontWeight: 'bold', paddingTop:'70px'}}><FormattedMessage id={"confirm.word.dictionary"}/></p>
                </Col>
              </FormGroup>
              <br/>
              <hr/>
              <FormGroup className="d-flex">
                <Col>
                  <Button type="button" onClick={() => this.props.onConfirmDialogClose()}
                          className="btn btn-secondary btn-modal" style={{color: 'black'}}
                  ><FormattedMessage id={"cancel"}/></Button>
                </Col>
                <Col className="text-center">
                  {
                    this.props.loading && <Spinner color="primary"/>
                  }
                </Col>
                <Col className="text-right">
                  <Button color="primary" type="submit"
                  >OK</Button>
                </Col>
              </FormGroup>
            </Form>
        </Formik>
      </Dialog>
    );
  }
}


const mapStateToProps = (state) => {
  return {
    loading: state.dictionaryReducer.loading
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    dictionaryAction: bindActionCreators(dictionaryAction, dispatch),
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(DictionaryDelete)
