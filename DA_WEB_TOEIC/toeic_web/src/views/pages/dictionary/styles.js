import { outlineButtons } from "../../../components/reactstrap/buttons/ButtonsSourceCode";

const styles = ({
  title: {
    textAlign: 'right',
    fontFamily: 'ArialMT, Arial, sans-serif',
    fontSize: '17px',
    marginLeft: '70px'
  },
  error: {
    color: 'red',
    fontSize: 12
  },
  textCenter: {
    textAlign: 'right',
  },
  discription: {
    // lineHeight: '1.2',
    // verticalAlign: 'middle',
    //marginTop: '10px',
    display: 'block',
    //display: 'tableCell',
    paddingTop: '25px',
    maxWidth: '150px',
    // width: '200px',
    maxHeight: '150px',
    minHeight: '50px',
    // whiteSpace: 'nowrap',
    // overflow: 'hidden',
    // textOverflow: 'ellipsis',
    // width: '50px !important',
  },
  nameEng: {
    display: 'inline-block',
    whiteSpace: 'nowrap',
    overflow: 'hidden',
    textOverflow: 'ellipsis',
    color: 'rgba(22, 155, 213, 1)',
    width: '150px !important',
  },
  upLoadFile: {
    whiteSpace: 'nowrap',
    maxWidth: '500px',
    overflow: 'hidden',
    textOverflow: 'ellipsis',
    display: 'inline-block',
    
  },
  mp3: {
    whiteSpace: 'nowrap',
    width: '600px',
    overflow: 'hidden',
    textOverflow: 'ellipsis',
    display: 'inline-block'
  },
  ReadFilemp3: {
    whiteSpace: 'nowrap',
    width: '300px',
    overflow: 'hidden',
    textOverflow: 'ellipsis',
    paddingTop: '12px'
  },
  showdetail: {
    readOnly: true
  },
  iconPlay:{
    padding: '0px !important',
    color: 'rgba(22, 155, 213, 1)',
    maxWidth:'40px',
    paddingBottom: '3px'
  },
  btnPlayMp3:{
    background: 'none !important',
    border: 'none !important',
    margin: 'none !important'
  },
  btnData:{
  },
  synonymous:{
    textOverflow: 'ellipsis',
    whiteSpace: 'nowrap',
    // width: '200px',
    overflow: 'hidden',
    display: 'inline-block',
    maxWidth: '150px',
  },
  textTitle:{
    paddingLeft: '0',
    paddingRight: '0'
  }
});

export default styles;
