import * as Yup from "yup";
import {showMessage} from "../../../commons/utils/convertDataForToast";
const FILE_SIZE = 1 * 1024 * 1024;
const SUPPORTED_FORMATS_CATEORY = [

  "audio/mpeg",
  "audio/mp3"
];

export const formSchema = Yup.object().shape({
  nameEng: Yup.string()
    .required("vocabulary.name.cannot.be.blank")
    .min(1, "contains.at.least.1.character")
    .max(150, "vocabulary.max.150.character"),
  transcribe: Yup.string()
    .required("transcription.not.empty")
    .min(1, "contains.at.least.6.character")
    .max(150, "pronunciation.max.150.character"),
  synonymous: Yup.string()
    .max(500, "synonymous.max.500.character"),
  // description: Yup.string()
  //   .max(2000, showMessage("description.max.2000.character")),
  fileUpload: Yup
    .mixed()
    // .required('File phiên âm không được để trống!1')
    .test(
      "fileSize",
      "file.upload.maximum.1MB",
      value => value ? value.size <= FILE_SIZE ? true : false : true
    )
    .test(
      "fileFormat",
      "format.file.not.audio",
      (value) =>
        value ? SUPPORTED_FORMATS_CATEORY.includes(value.type) ? true : false : true
    ),
  // description: Yup.string().required('abc')
  // desNoHTML: Yup.string().required('ádads')
});
