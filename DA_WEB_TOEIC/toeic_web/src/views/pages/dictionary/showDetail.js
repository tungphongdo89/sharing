import React, { Component } from 'react';
import {
  Dialog,
  IconButton
} from '@material-ui/core';

import ClearIcon from '@material-ui/icons/Clear';
import { Field, Form, Formik } from "formik";
import { Button, Col, FormGroup, Label, ModalHeader } from "reactstrap";
import '../../../assets/scss/pages/vocabulary.scss';
import { Headphones } from "react-feather"
import { compose } from "redux";
import { connect } from "react-redux"
import { withStyles } from "@material-ui/core";
import styles from "./styles";
import TextArea from './textArea';
import {disableClickPropagation} from "leaflet/src/dom/DomEvent";
import {FormattedMessage} from "react-intl";



class ShowDetail extends Component {

  state = {
    file: null,
    description: null,
    audio: new Audio(''),

  }

  handleUploadFile = event => {
    this.setState({
      file: event
    })
  }


  handleTextArea = event => {
    let keyArea = event;
    this.setState({
      description: keyArea
    })
  }


  runMp3 = (data) => {
    this.stopMP3();
    let { audio } = this.state;
    audio = new Audio(data);
    this.setState({
      audio
    })
    audio.play();
  }


  stopMP3 = () => {

    const { audio } = this.state;
    audio.pause();
  }
  closeShowDetail = () => {
    this.stopMP3();
    this.props.onConfirmDialogClose()
  }

  render() {

    let { open, item, } = this.props;
    const { classes } = this.props;
    const { file, description } = this.state;
    return (

      <>
        <style>{`
            #btnHighLight:focus-within {
            outline: 1px solid !important;
            borderRadius: 5px !important;
            box-shadow: 3px 3px 3px 3px #ccc !important;
            }
            `}
        </style>
        <Dialog
          scroll={"body"}
          open={open}
          maxWidth='md' fullWidth={true}
          onKeyDown={(e) => {
            if (e.keyCode === 27) {
              this.props.onConfirmDialogClose()
            }
          }}
          onClose={()=>{
            this.props.onConfirmDialogClose()
          }}
        >
          <div className="flex flex-space-between flex-middle  bg-primary "  >
            <ModalHeader className='text-centesr' >
              <span style={{ color: 'white', fontSize: '27px' }}>  <FormattedMessage id={"vocabulary.details"}/></span>
            </ModalHeader>
            <IconButton onClick={this.closeShowDetail}>
              <ClearIcon />
            </IconButton>
          </div>


          <Form>
            <br />
            <br />
            <FormGroup className="form-group d-flex">

              <Label for="required" sm={2} className={classes.title} style={{paddingLeft: '0', paddingRight: '0'}} ><FormattedMessage id={"vocabulary.name"}/>  </Label>
              <Col sm={5} >

                <Field
                  type="text"
                  name="nameEng"
                  className='form-control'
                  value={item.nameEng}
                  disabled={true}
                >
                </Field>

              </Col>
              <Col   sm={3}>
                <Button autoFocus id="btnHighLight"
                        style={{  width: '206px', borderRadius:'5px', paddingBottom:'4px' }}  outline color="primary"
                        onClick={() => this.runMp3(item.mp3)}
                >      <Headphones size={20} className="mr-1 fonticon-wrap"
                                   style={{ cursor: "pointer", color: "rgba(22, 155, 213, 1)" , alignItems: 'center',justifyItems: 'center' }} />
                  <span style={{ color: "rgba(22, 155, 213, 1)", fontSize: 15 , alignItems: 'center',justifyItems: 'center'}}><FormattedMessage id={"listen.to.the.pronunciation"}/></span> </Button>

              </Col>
            </FormGroup>
            <FormGroup className="form-group d-flex">

              <Label for="required" sm={2} className={classes.title} style={{paddingLeft: '0', paddingRight: '0'}} ><FormattedMessage id={"pronunciation"}/>  </Label>
              <Col sm={8} >

                <Field
                  type="text"
                  name='transcribe'
                  className='form-control'
                  value={item.transcribe}
                  disabled={true}
                >
                </Field>

              </Col>
            </FormGroup>
            <FormGroup className="form-group d-flex">
              <Label for="required" sm={2} className={classes.title} style={{paddingLeft: '0', paddingRight: '0'}}><FormattedMessage id={"synonym"}/></Label>
              <Col sm={8}>
                <Field
                  type="text"
                  name="synonymous"
                  className={`form-control`}
                  value={item.synonymous}
                  disabled={true}>
                </Field>
              </Col>
            </FormGroup>
            <FormGroup className="form-group d-flex">

              <Label for="required" sm={2} className={classes.title} style={{paddingLeft: '0', paddingRight: '0'}} ><FormattedMessage id={"pronunciation.file"}/>  </Label>
              <Col sm={8} style={styles.ReadFilemp3}>

                <span title={item.mp3}>{item.mp3} </span>

              </Col>
            </FormGroup>


            <FormGroup className="form-group d-flex">

              <Label for="required" sm={2} className={classes.title} style={{paddingLeft: '0', paddingRight: '0'}} ><FormattedMessage id={"describe"}/></Label>
              <Col  sm={8}>
                <div>
                            <TextArea disables={true} textHTML={description} textDescription={item.description}
                                      handleTextArea={this.handleTextArea}/>
                </div>
              </Col>
            </FormGroup>
            <hr />
            <FormGroup className="form-group d-flex">


              <Col className="text-right  ">
                <Button color="primary" className="btn btn-primary btn-modal" style={{ marginRight: '100px',color: 'black' }}
                        onClick={this.closeShowDetail}><FormattedMessage id={"close"}/></Button>
              </Col>
              {/*</Col>*/}
            </FormGroup>

          </Form>

        </Dialog>
      </>
    );
  }
}


const mapStateToProps = (state) => {
  return {

  }
}
const mapDispatchToProps = (dispatch) => {
  return {

  }
}



export default compose(withStyles(styles), connect(mapStateToProps, mapDispatchToProps))(ShowDetail)
