import React from "react"
import Pagination from "react-js-pagination";

class PaginationIconsAndText extends React.Component {
  state = {
    activePage: 1,
    itemPerPage : 10
  }

  componentWillReceiveProps(nextProps, nextContext) {
    
    if(this.props.checkSearch === 1){
      this.setState({
        activePage: 1
      })
      this.props.resetIsSearch()
    }
  }

  handlePageChange = (pageNumber) => {

    this.setState({
      activePage : pageNumber
    })
    this.props.onChangPage(pageNumber)
  }

  render() {
    return (
      <Pagination
        itemClass="page-item"
        linkClass="page-link"
        prevPageText="Previous"
        nextPageText="Next"
        hideFirstLastPages={true}
        activePage={this.state.activePage}
        itemsCountPerPage={this.state.itemPerPage}
        totalItemsCount={this.props.totalItem}
        pageRangeDisplayed={5}
        onChange={ this.handlePageChange.bind(this) }
      />
    )
  }
}

export default PaginationIconsAndText
