import React, { useState, useEffect } from "react";
import styles from "./styles"
import {Pause} from "@material-ui/icons";
import {Headphones} from "react-bootstrap-icons";

const useAudio = url => {
  debugger
  const [audio] = useState(new Audio(url));
  const [playing, setPlaying] = useState(false);

  const toggle = () => setPlaying(!playing);

  useEffect(() => {
      playing ? audio.play() : audio.pause();
    },
    [playing]
  );

  useEffect(() => {
    audio.addEventListener('ended', () => setPlaying(false));
    return () => {
      audio.removeEventListener('ended', () => setPlaying(false));
    };
  }, []);

  return [playing, toggle];
};

const PlayerCustom = ({ url }) => {
  debugger
  const [playing, toggle] = useAudio(url);

  return (
    <div>
      <button onClick={toggle}>{playing ?
      <Pause size={20}  style={styles.iconPlay} className="col-4 pl-0 pr-0"/>:
      <Headphones size={20}  style={styles.iconPlay} className="col-4 pl-0 pr-0"/>
      }</button>
    </div>
  );
};

export default PlayerCustom;
