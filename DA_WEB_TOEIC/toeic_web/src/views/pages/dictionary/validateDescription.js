import {showMessage} from "../../../commons/utils/convertDataForToast";


export const validateDescription = (values, props) => {
  const errors = {};
  if ((values.fileUpload === "" || values.fileUpload === null) && values.mp3 === "") {
    errors.fileUpload = showMessage("pronunciation.not.blank");
  }
  // if (values.description === null || values.description === "") {
  //   //   errors.description = "mô tả 2"
  //   // }

  return errors;
};
