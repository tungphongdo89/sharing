import React, {useState, useEffect} from "react"
import Breadcrumbs from "../../../components/@vuexy/breadCrumbs/BreadCrumb"
import classnames from "classnames"
import "../../../assets/scss/plugins/tables/_agGridStyleOverride.scss"
import "../../../assets/scss/pages/users.scss"
import {Form, Formik} from "formik"
import {compose} from "redux";
import {connect} from "react-redux"
import Sound from 'react-sound';
import styles from "./styles";
import OverlayTrigger from "react-bootstrap/OverlayTrigger";
import Tooltip from "react-bootstrap/Tooltip";
import MUIDataTable from "mui-datatables";
import {TableCell, TableFooter, TableRow, withStyles} from "@material-ui/core";
import PaginationComponent from './pagingtion';
import styled from "styled-components"
import MultiPlayerMP3 from './MultiPlayerMP3'
// duonghaiduong
import {
  Card, CardHeader, CardBody, Row, Col, Button, CardTitle,
  Collapse,
  Spinner,
  FormGroup,
  Input
} from "reactstrap"
import {ChevronDown, Edit, Eye, RotateCw, Search, Trash2, X, Headphones, Pause} from "react-feather"
import VocabularyEditorAdd from './dictionaryEditorAdd';
import ShowDetail from './showDetail';
import dictionaryAction from "../../../redux/actions/dictionary/dictionaryAction";
import {bindActionCreators} from "redux";
import DictionaryDelete from './DictionaryDelete';
import {getUTCTime, getUTCTimeSeconds} from "../../../commons/utils/DateUtils";
import {ReactPlayerReading} from "../Practices/ListeningToeicShortTalkingPractice/style";
import * as Icon from "react-feather";
import {MusicPlayer} from "react-bootstrap-icons";
import AutoPlay from "../../../extensions/swiper/Autoplay";
import PlayerCustom from "./PlayerCustom"
import {showMessage} from "../../../commons/utils/convertDataForToast";
import {FormattedMessage} from "react-intl";

const audio = new Audio();

const TableCellStyle = styled(TableCell)`
  &&&{
    display: grid;
    justify-content: center;
   }
`

class Dictionary extends React.Component {
 
  state = {
    abc: false,
    playing: false,
    shouldOpenEditorAdd: false,
    item: null,
    collapse: true,
    reload: false,
    page: 1,
    pageSize: 10,
    audio:  [] ,// new Audio(''),
    isSearch: null,
    search: showMessage("dictionary.search"),
    searchValue: '',
    shouldOpenDelete: false,
    id: '',
    shouldOpenDetail: false,
    popupActive: "1",
    isEdit: false,
    playersList:[],
    setPlayers1:[]
  }


  onEntered = () => {
    this.setState({status: "Opened"})
  }
  onEntering = () => {
    this.setState({status: "Opening..."})
  }
  onExiting = () => {
    this.setState({status: "Closing..."})
  }
  onExited = () => {
    this.setState({status: "Closed"})
  }

  handleDialogClose = () => {

    this.setState(
      {
        shouldOpenEditorAdd: false,
      }
    )
  }

  handleDialogCloseShowDetail = () => {

    this.setState(
      {
        shouldOpenDetail: false,
      }
    )
  }

  componentDidMount() {
    this.fetchDictionaryPage();
    
  }
  fetchDictionaryPage=()=>{
    let dictionary = {};
    let playersList=[];
    dictionary.keySearch = this.state.searchValue.toString().trim()
    dictionary.page = this.state.page;
    dictionary.pageSize = this.state.pageSize;
    const {dictionaryAction} = this.props;
    const {fetchDictionary} = dictionaryAction;
    fetchDictionary(dictionary);
  }
  resetIsSearch = () => {
    this.setState({
      isSearch: null
    })
  }
  resetDeleteOrAddPage = () => {
    this.setState({
      isSearch: 1
    })
  }
  backToFirstPage = () => {
    this.setState({
      page: 1
    })
  }
  onChangPageReceive = (pageNumber) => {
    this.stopMP3();
    this.setState({
      page: pageNumber
    })
    if (pageNumber === 1) {
      this.setState({
        isSearch: 1,
      })
    }
    let dictionary = {};
    dictionary.keySearch = this.state.searchValue.toString().trim()
    dictionary.page = pageNumber;
    dictionary.pageSize = this.state.pageSize;
    const {dictionaryAction} = this.props;
    const {fetchDictionary} = dictionaryAction;

    fetchDictionary(dictionary);
  }
  componentDidUpdate(){
    var list = this.props.playersList;
    list.forEach((item,index)=>{
      list[index].audio.addEventListener('ended', () => {
          list[index].playing = false
        })
      }
    )  
    list.forEach((item,index)=>{
      list[index].audio.removeEventListener('ended', () => {
          list[index].playing = false
        })
      }
    ) 
    this.updatePlayerList(list);
  }

  runMp3 = (data) => {
    var list = this.props.playersList;
    list.forEach((item,index)=>{
      if(item.item.id == data[1] ){  //id
        list[index].playing = !item.playing
        if(list[index].playing) 
          list[index].audio.play() 
        else {
          list[index].audio.load() 
          list[index].audio.play() 
          list[index].playing = true
        }
          
      }
      else {
        list[index].playing = false
        list[index].audio.load();
      }
    }) 
    this.updatePlayerList(list);
  
    // this.stopMP3();
    // let {audio} = this.state;
    // audio = new Audio(data);
    // this.setState({
    //   audio
    // })
    
    // audio.play();
  }

  stopMP3 = () => {
    // const {audio} = this.state;
    // audio.load();
    var list = this.props.playersList;
    list.forEach((item,index)=>{
        list[index].playing = false
        list[index].audio.load();
    }) 
    this.updatePlayerList(list);
  }

  onOpenDelete = (id) => {
    this.stopMP3();
    console.log(id);
    this.setState({
      shouldOpenDelete: true,
      id
    })
  }

  onCloseDelete = () => {
    this.setState({
      shouldOpenDelete: false,
    })
  }

  openFormAddOrUpdate = () => {
     this.stopMP3();

    this.setState({
      shouldOpenEditorAdd: true,
      item: {
        id: '',
        nameEng: '',
        transcribe: '',
        mp3: '',
        description: '',
        synonymous: '',
        fileUpload: '',
      }
    })
  }

  openShowDetail = (dataRow) => {
     this.stopMP3();
    this.setState({
      shouldOpenDetail: true, item: {
        id: dataRow.rowData[1],
        nameEng: dataRow.rowData[2],
        transcribe: dataRow.rowData[3],
        mp3: dataRow.rowData[4],
        description: dataRow.rowData[6],
        createDate: dataRow.rowData[7],
        synonymous: dataRow.rowData[5],
      }
    })
  }
  openFormUpdate = (dataRow) => {
    this.stopMP3();
    this.setState({
      shouldOpenEditorAdd: true,
      item: {
        id: dataRow.rowData[1],
        nameEng: dataRow.rowData[2],
        transcribe: dataRow.rowData[3],
        mp3: dataRow.rowData[4],
        description: dataRow.rowData[6],
        createDate: dataRow.rowData[7],
        synonymous: dataRow.rowData[5]
      }
    })
  }
  updatePlayerList=(list)=>{
    this.props.dictionaryAction.updateDictionaryPlayerList(list);
  }
  render() {
    let {item, shouldOpenEditorAdd, shouldOpenDetail, shouldOpenDelete} = this.state
    const {classes, lstDictionary, loading, total,playersList} = this.props;
    const loadingComponent = (
      <div style={{
        position: 'absolute',
        zIndex: 110,
        top: 0,
        left: 0,
        width: '100%',
        height: '100%',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        background: 'rgba(255,255,255,0.8)'
      }}>
        <Spinner color="primary" className="reload-spinner"/>
      </div>
    );
    let newDataActive = [];
    if (lstDictionary) {
      lstDictionary.map((item, index) => {
        newDataActive.push({stt: (this.state.pageSize) * (this.state.page - 1) + index + 1, ...item});
      });
    }
    const options = {
      filter: false,
      filterType: 'checkbox',
      selectableRowsHideCheckboxes: true,
      // responsive: "standard",
      selectableRows: "none",
      print: false,
      download: false,
      search: false,
      viewColumns: false,
      serverSide: true,
      textLabels: {
        body: {
          noMatch: this.props.isLoading ?
            <p/> :
            <FormattedMessage id={"not.found.data"}/>,
        },
      },
      responsive: "standard",
      // resizableColumns: true,
      zoom: true,
      customFooter: () => {
        return (
          <TableFooter>
            <TableRow>
              <TableCellStyle colSpan={4}>
                <PaginationComponent
                  totalItem={total}
                  onChangPage={this.onChangPageReceive}
                  checkSearch={this.state.isSearch}
                  resetIsSearch={this.resetIsSearch}
                />
              </TableCellStyle>
            </TableRow>
          </TableFooter>
        );
      }
    };

    const columns = [
      {
        name: 'stt',
        label: showMessage("index"),
        options: {
          setCellProps: () => ({ style: { width:'50px', padding: '8px' }}),
          filter: true,
          sort: false,
          customHeadLabelRender: () => {
            return <div style={{fontWeight: 'bold'}}><FormattedMessage id="index"/></div>;
          },
          customBodyRender: (index) => {
            return (<div autoFocus={false} style={{textAlign: "center"}}>
              {index}
            </div>)
          }
        }
      },
      {
        name: "id",
        label: "ID",
        options: {
          filter: true,
          sort: false,
          display: false,
        }
      },

      {
        name: "nameEng",
        label: showMessage("vocabulary.name"),
        options: {
          filter: true,
          sort: false,
          customHeadLabelRender: () => {
            return <div style={{fontWeight: 'bold'}}><FormattedMessage id={"vocabulary.name"}/></div>;
          },
          customBodyRender: (data, rowData) => {
            var playing = playersList.forEach((item,index)=>{
              if(item.item.id == rowData.rowData[1] ) {
                  playing = item.playing
              }
            })
            return <div   
               style={{width:'150px',display: 'inline-block',
                      whiteSpace: 'nowrap',
                      overflow: 'hidden',
                      textOverflow: 'ellipsis',
                      color: 'rgba(22, 155, 213, 1)'
            }} 
              // style={{width:'100px !important'}}
              autoFocus={false}>
              <a onClick={() => this.runMp3(rowData.rowData)}  title={data}  >   
                  { playing ? <Pause size={15} className="mr-1 fonticon-wrap" style={{color: "rgba(22, 155, 213, 1)", cursor: "pointer"}} /> :
                 <Headphones size={15} className="mr-1 fonticon-wrap" style={{  color: "rgba(22, 155, 213, 1)", cursor: "pointer"}} />  }
                 {data} </a>
              {/* <MultiPlayerMP3 
               
                //lstDictionary = {lstDictionary}
                urls={[rowData.rowData[4]]}
                data={data}
                // id={rowData.rowData[1]}
                // playersList={playersList}
                // updatePlayerList={this.updatePlayerList}

              /> */}
            </div>
          }
        }
      },
      {
        name: "transcribe",
        label: showMessage("pronunciation"),
        options: {
          filter: true,
          sort: false,
          customHeadLabelRender: () => {
            return <div style={{fontWeight: 'bold', maxWidth: '100px',width: '80px'}} autoFocus={false}><FormattedMessage id={"pronunciation"}/></div>;
          },
          customBodyRender: (data) => {
            return <div>
              <div autoFocus={false}
                   style={{
                     textOverflow: 'ellipsis',
                     whiteSpace: 'nowrap',
                     width: '80px',
                     overflow: 'hidden',
                     display: 'inline-block',
                     maxWidth: '150px',

                   }}
                   title={data}>{data}</div>
            </div>
          }
        }
      },
      {
        name: "mp3",
        label: "Phiên âm",
        options: {
          filter: true,
          sort: false,
          display: false,
        }
      },
      {
        name: "synonymous",
        label: showMessage("synonym"),
        options: {
          filter: true,
          sort: false,
          customHeadLabelRender: () => {
            return <div style={{fontWeight: 'bold'}}><FormattedMessage id={"synonym"}/></div>;
          },
          customBodyRender: (data) => {
            return <div>
              <div autoFocus={false} style={styles.synonymous} title={data}>{data}</div>
            </div>
          }
        }
      },
      {
        name: "description",
        label: "Mô tả (VN)",
        options: {
          setCellProps: () => ({ style: { width:'150px',maxWidth:'200px',  
         
        }}),
          filter: true,
          sort: false,
          customHeadLabelRender: () => {
            return <div style={{fontWeight: 'bold'}}><FormattedMessage id={"describe"}/> (VN)</div>;
          },
          customBodyRender: (data) => {
            return (<div  >
              <div autoFocus={false} title={data.replace(/(<([^>]+)>)/ig, "")} dangerouslySetInnerHTML={{__html: `${data}`}}
                   style={styles.discription} className="descriptionDictionary" />
                    {/*<div style={styles.discription} className="descriptionDictionary" >{data}</div>*/}
            </div>)
          }
        }
      },
      {
        name: "createDate",
        label: "Thời gian tạo",
        options: {
          filter: true,
          sort: false,
          customHeadLabelRender: () => {
            return <div style={{fontWeight: 'bold'}}><FormattedMessage id={"date.created"}/></div>;
          },
          customBodyRender: (data) => {
            return <div>
              <div autoFocus={false}>{getUTCTime(new Date(data))}</div>
            </div>
          }
        }
      },
      {
        name: "updatedDate",
        label: "Cập nhật lần cuối",
        options: {
          filter: true,
          sort: false,
          customHeadLabelRender: () => {
            return <div style={{fontWeight: 'bold'}}><FormattedMessage id={"last.update"}/></div>;
          },
          customBodyRender: (data) => {
            return <div>
              <div autoFocus={false}>{getUTCTime(new Date(data))}</div>
            </div>
          }
        }
      },

      {
        name: "",
        label: showMessage("action"),
        options: {
          setCellProps: () => ({ style: { width:'150px' }}),
          filter: true,
          sort: false,
          customHeadLabelRender: () => {
            return <div style={{fontWeight: 'bold', paddingLeft:'10px'}}><FormattedMessage id={"action"}/></div>;
          },
          customBodyRender: (data, dataRow) => {
            return (

              <div style={{cursor: 'pointer', textAlign:'left'}}  >
                <OverlayTrigger placement="top" overlay={props => (<Tooltip {...props}><FormattedMessage id={"edit"}/></Tooltip>)}>
                  <button id="btnHighLight" style={{border: 'none', backgroundColor: 'rgb(0 0 0 / 0%)', marginBottom: '2px', marginRight:'3px'}}
                          onClick={() => {
                            this.props.dictionaryAction.updateDictionaryStatusPopup()
                            this.openFormUpdate(dataRow)
                            this.setState({
                              isEdit: true,
                            })
                          }}>
                    <span><Edit size={16} className=" mr-1 fonticon-wrap" style={{cursor: "pointer", color: "#2c00ff"}}  /></span>
                  </button>
                </OverlayTrigger>
                <OverlayTrigger placement="top" overlay={props => (<Tooltip {...props}><FormattedMessage id={"remove"}/></Tooltip>)}>
                  <button id="btnHighLight" style={{border: 'none', backgroundColor: 'rgb(0 0 0 / 0%)'}}
                          onClick={() => this.onOpenDelete(dataRow.rowData[1])}>
                    <Trash2
                      size={16} className="mr-1 fonticon-wrap" style={{cursor: "pointer", color: "red"}}
                    />
                  </button>
                </OverlayTrigger>
                <OverlayTrigger placement="top" overlay={props => (<Tooltip {...props}><FormattedMessage id={"Detail"}/></Tooltip>)}>
                  <button id="btnHighLight" style={{border: 'none', backgroundColor: 'rgb(0 0 0 / 0%)'}}
                          onClick={() => this.openShowDetail(dataRow)}>
                    <Eye size={16} className=" mr-1 fonticon-wrap" style={{cursor: "pointer"}}
                    />
                  </button>
                </OverlayTrigger>
              </div>

            )
          }
        }

      }

    ]


    return (
      <>

        <style>
          {`
          #btnHighLight:focus-within {
          border: none !important;
          outline: 1px solid gray !important;
          }

          #postLink:focus-within {
          text-shadow: 1px 1px #1890ff;
          }
          `}

        </style>
        <React.Fragment>
          <Row>
            <Col sm="12">
              <Card
              >
                <CardHeader>
                  <CardTitle><h2><FormattedMessage id={"vocabulary_list"}/></h2></CardTitle>
                </CardHeader>
                <Collapse
                  isOpen={this.state.collapse}
                  onExited={this.onExited}
                  onEntered={this.onEntered}
                  onExiting={this.onExiting}
                  onEntering={this.onEntering}
                >
                  <CardBody>
                    {this.state.reload ? (
                      <Spinner color="primary" className="reload-spinner"/>
                    ) : (
                      ""
                    )}
                    <Formik initialValues={{
                      createDateFrom: "",
                      createDateTo: ""
                    }} onSubmit={() => {
                      this.setState({
                        loading: true
                      })
                    }}
                    >
                      {({errors, touched}) => (
                        <Form onKeyDown={(e) => {
                          if (e.keyCode === 27) {
                            this.props.dictionaryAction.updateDictionaryStatusPopup()
                          }
                        }}>
                          <FormGroup className="form-group d-flex">
                            <Col lg="2" md="2" sm="2">
                              <FormGroup style={{paddingTop: '1.5em'}} className="mb-0">

                                <Button color="primary"
                                        onClick={() => {
                                          this.setState({
                                            isEdit: false
                                          })
                                          this.openFormAddOrUpdate()
                                          this.props.dictionaryAction.updateDictionaryStatusPopup()
                                        }}
                                        className={classes.buttonAdd}
                                ><FormattedMessage id={"add"}/></Button>
                                {
                                  this.props.isLoading && shouldOpenEditorAdd && (
                                    <VocabularyEditorAdd  
                                      resetDeleteOrAddPage={this.resetDeleteOrAddPage}
                                      item={item}
                                      keySearch = { this.state.searchValue }
                                      page={this.state.page}
                                      open={this.props.isLoading}
                                      onConfirmDialogClose={this.handleDialogClose}
                                      backToFirstPage={this.backToFirstPage}
                                    />
                                  )}

                                {shouldOpenDetail && (
                                  <ShowDetail
                                    item={item}
                                    page={this.state.page}
                                    open={shouldOpenDetail}
                                    onConfirmDialogClose={this.handleDialogCloseShowDetail}
                                  />
                                )}
                              </FormGroup>
                            </Col>
                            <Col sm={4} md={4} lg={4} xs={4}>
                              <span>&nbsp;</span>
                              <FormGroup className="position-relative has-icon-left">
                                <div className="w-100">
                                  <Input
                                    autoFocus
                                    type="search"
                                    name="searchValue"
                                    id="searchValue"
                                    placeholder={this.state.search}
                                    onChange={e => {
                                      this.setState(
                                        {
                                          searchValue: e.target.value
                                        }
                                      )
                                    }}
                                  />
                                  <div className="form-control-position px-1">
                                    <Search size={15}/>
                                  </div>
                                </div>
                              </FormGroup>
                            </Col>
                            <Col Col sm={2} md={2} lg={2} xs={2}>
                              <FormGroup style={{paddingTop: '1.5em'}} className="mb-0">
                                <Button type="submit" color="primary"
                                        className={classes.buttonSearch}
                                        onClick={() => this.onChangPageReceive(1)}
                                ><FormattedMessage id={"search"}/></Button>
                              </FormGroup>
                            </Col>
                          </FormGroup>
                        </Form>
                      )}</Formik>
                  </CardBody>
                </Collapse>
              </Card>
            </Col>

            {shouldOpenDelete && <DictionaryDelete
              resetDeleteOrAddPage={this.resetDeleteOrAddPage}
              open={this.state.shouldOpenDelete}
              id={this.state.id}
              onConfirmDialogClose={this.onCloseDelete}
              backToFirstPage={this.backToFirstPage}

              />}

            <Col sm="12">
              <div style={{position: 'relative'}}>
                {loading && loadingComponent}

                <MUIDataTable
                  columns={columns}
                  data={newDataActive}
                  options={options}
                />
              </div>
            </Col>
          </Row>
        </React.Fragment>
      </>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    lstDictionary: state.dictionaryReducer.lstDictionary,
    loading: state.dictionaryReducer.loading,
    isLoading: state.dictionaryReducer.isLoading,
    total: state.dictionaryReducer.total,
    playersList: state.dictionaryReducer.playersList,
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    dictionaryAction: bindActionCreators(dictionaryAction, dispatch),
  }
}
export default compose(withStyles(styles), connect(mapStateToProps, mapDispatchToProps))(Dictionary)
