import React, {Component} from 'react';
import ReactQuill from 'react-quill';
import 'react-quill/dist/quill.snow.css';
import 'react-quill/dist/quill.bubble.css';
import '../../../assets/scss/pages/vocabulary.scss';
import {FormattedMessage} from "react-intl";
import {showMessage} from "../../../commons/utils/convertDataForToast";

class textArea extends Component {
  constructor(props) {
    super(props);

    this.modules = {
      toolbar: [
        ['bold', 'italic', 'underline'],
        [{'list': 'ordered'}, {'list': 'bullet'}],
        [{'align': null}, {'align': 'center'}, {'align': 'right'}, {'align': 'justify'}],
        [{'indent': '-1'}, {'indent': '+1'}],
        ['link',],
      ]
    };
    this.placeholder = 'Compose an epic...';
    this.theme = 'snow'

    this.state = {
      comments: '',
      disables: false,
      textDesNotHTML: ''
    }

		this.rteChange = this.rteChange.bind(this);
	}

	rteChange = (content, delta, source, editor) => {
		let {handleTextArea,checkDescription} = this.props;
		let dataArea= editor.getHTML();
		let NoHMTL = editor.getHTML().replace(/(<([^>]+)>)/ig, "")
		this.setState({
			comments: dataArea,
      textDesNotHTML: NoHMTL
		})
    // checkDescription(NoHMTL);
		handleTextArea(this.state);

	}
	componentDidMount(){
		let {textDescription,disables}=this.props;
		this.setState({
      comments:textDescription,
			disables,
      textDesNotHTML: textDescription.replace(/(<([^>]+)>)/ig, "")
		})
	}
  render() {
    return (
      <>
      <ReactQuill
        readOnly={this.state.disables} theme="snow"
                  style={{height: '150px' , marginBottom: '50px', borderRadius: '5px', background: this.state.disables? '#f5f5f1' : 'white'}}
                  modules={this.state.disables ? {toolbar: false}  : this.modules}
                  onChange={this.rteChange}
                  value={this.state.comments || ''}
      >

          </ReactQuill>
        { this.state.textDesNotHTML.toString().trim() === "" ?
        <div style={{color: "red", fontSize: "12px"}}><FormattedMessage id={"description.not.empty"}/></div>
        : null}
      </>

    );
  }
};

export default textArea;
