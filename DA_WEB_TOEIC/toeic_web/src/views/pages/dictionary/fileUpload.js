import React, { Component } from 'react';
import HighlightOffIcon from '@material-ui/icons/HighlightOff';
import Button2 from '@material-ui/core/Button';
import * as Icon from "react-bootstrap-icons"
import { Button, Col, FormGroup, Label, ModalHeader } from "reactstrap";
import { Field, Form, Formik,getIn } from "formik";
import { compose } from "redux";
import { connect } from "react-redux"
import { withStyles } from "@material-ui/core";
import styles from "./styles";
import {FormattedMessage} from "react-intl";

class fileUpload extends Component {

    constructor(props) {
        super(props);

        this.fileInput = React.createRef();
      }
    state={
        file : this.props.file,
      checkUploadDic: false
    }
    RenderFileUpload = props => {

        const onFileChange = (e) => {
        let {handleUploadFile} = this.props;

            this.setState({
              checkUploadDic: true
            })
            props.form.setFieldTouched(props.field.name, undefined);
            let reader = new FileReader();
            let file = e.target.files[0];
            if (file) {
                reader.onloadend = () => {
                };

                props.form.setFieldValue(props.field.name, file);
                if(this.fileInput.current){
                  this.fileInput.current.value = null
                }
                this.setState({
                    file: file
                })

            }
            handleUploadFile(file);


        }
        const onDeleteFile = () => {
            props.form.setFieldValue(props.field.name, null);
            this.setState({
                file: ''
            })
            let {handleUploadFile} = this.props;
            handleUploadFile('');
        }

        const onFileClick = (value) => {

        }
        const  {classes,errors,touched,nameMp3} =this.props
        return (
            <div className="w-100">
                <input
                    onClick={onFileClick}
                    ref={this.fileInput}
                    disabled={this.props.isViewDetail}
                    name={props.field.name}
                    style={{ display: "none" }}
                    id={`contained-button-file ${props.field.name}`}
                    type="file"
                    onChange={onFileChange}
                />
                <label htmlFor={`contained-button-file ${props.field.name}`} onKeyDown={e => e.keyCode === 13 ? e.target.click() : ""}>
                    <Button2 id={props.field.name} disabled={this.props.isViewDetail} variant="contained" color="primary"
                        component="span" name={props.field.name}>
                        <Icon.Upload style={{ marginRight: '5px'}} />
                        <span style={{textTransform :'none',fontSize:'17px'}}>
                          {/*Tải lên*/}
                          <FormattedMessage id="upload"/>
                        </span>
                  </Button2>
                </label>
              <br/>
                <div style={{paddingTop:'10px'}}>
                    {
                      props.field.value ?
                      <span style={styles.upLoadFile} title={props.field.value.name} >
                       {props.field.value.name} </span>
                        : null
                    }
                    
                    <span> 
                    {
                    this.state.file ?
                    <HighlightOffIcon style={{ 
                     color: 'red', cursor: 'pointer', marginLeft:'1%', marginTop: '-15px' }}
                         onClick={onDeleteFile} /> : ''
                    }
                    </span>
                </div>
              <div>
              {errors.fileUpload && touched.fileUpload ?  (
                <div className={classes.error}><FormattedMessage id={errors.fileUpload}/></div>
              ) : null}
              {!this.state.checkUploadDic && !nameMp3 ? (
                <div className={classes.error} style={{color: 'red'}}><FormattedMessage id={"pronunciation.not.blank"}/></div>
              ) : null}
                
                  {
                    !this.state.file ? <div style={styles.mp3} title={nameMp3}> {nameMp3} </div> : null
                  }
               
              </div>
            </div>
        );
    };

    render() {
        const ErrorMessage = ({ name }) => (
            <Field name={name}>
              {({ field, form, meta }) => {
                const error = getIn(form.errors, name);
                const touch = getIn(form.touched, name);
                return touch && error ? (
                  <div className="invalid-feedback" style={{ display: "contents" }}>
                    {/*{error}*/}
                    <FormattedMessage id={error}/>
                  </div>) : null;
              }}
            </Field>
          );
        const  {classes,errors,touched,nameMp3} =this.props
        return (
            <FormGroup className="form-group d-flex">

            <Label for="required" sm={2} className={classes.title} style={{paddingLeft: '0', paddingRight: '0'}}>
              <FormattedMessage id={"pronunciation.file"}/> (<span style={{ color: "red" }}>*</span>)
            </Label>

            <Col sm={8} >
                <Field
                    name="fileUpload"
                    errorMessage={errors[`fileUpload`] ? errors[`fileUpload`] : undefined}
                    touched={touched[`fileUpload`]}
                    setFieldValue={this.props.setFieldValue}
                    style={{ display: "flex" }}
                    onBlur={this.props.handleBlur}
                    component={this.RenderFileUpload}
                />
             
                {/*<ErrorMessage name="fileUpload"/>*/}
            </Col>
        </FormGroup>
        );
    }
}



const mapStateToProps = (state) => {
    return {
        lstTypeTopic: state.topicReducer.lstTypeTopic,
        lstPartByTopic: state.topicReducer.lstPartByTopic,
        values: state.topicReducer,
        isLogging: state.topicReducer.isLoading
    }
}
const mapDispatchToProps = (dispatch) => {
    return {

    }
}


export default compose(withStyles(styles), connect(mapStateToProps, mapDispatchToProps))(fileUpload)
