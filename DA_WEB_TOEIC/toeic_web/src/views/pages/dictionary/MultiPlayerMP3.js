import React, { useState, useEffect } from 'react'
import styles from "./styles";
import {Headphones, Pause} from "react-bootstrap-icons";

const useMultiAudio = (urls) => {
  const [sources] = useState(
    urls.map(url => {
      return {
        url,
        audio: new Audio(url),
      }
    }),
  )
  const [players, setPlayers] = useState(
    urls.map(url => {
      return {
        url,
        playing: false,
      }
    }),
  )

  const toggle = targetIndex => () => {
    console.log('---players', urls)
    const newPlayers = [...players]
    const currentIndex = players.findIndex(p =>  p.playing === true)
    if (currentIndex !== -1 ) {
      newPlayers[currentIndex].playing = false
      if(currentIndex !== targetIndex){
        newPlayers[targetIndex].playing = false
        newPlayers[targetIndex].playing = true
      }
    } else {
      newPlayers[targetIndex].playing = true
    }
    setPlayers(newPlayers)
  }

  useEffect(() => {
    sources.forEach((source, i) => {
      players[i].playing ? source.audio.play() : source.audio.pause()
    })
  }, [sources, players])

  useEffect(() => {
    sources.forEach((source, i) => {
      source.audio.addEventListener('ended', () => {
        const newPlayers = [...players]
        newPlayers[i].playing = false
        setPlayers(newPlayers)
      })
    })
    return () => {
      sources.forEach((source, i) => {
        source.audio.removeEventListener('ended', () => {
          const newPlayers = [...players]
          newPlayers[i].playing = false
          setPlayers(newPlayers)
        })
      })
    }
  }, [])

  return [players, toggle]
}

const MultiPlayerMP3 = ({ urls, data}) => {
  let urlstest = '';
  urlstest = urls
  const [players, toggle] = useMultiAudio(urlstest)
  return (
    <div  >
      {players.map((player, i) => (
        <Player key={i} player={player} toggle={toggle(i)} data={data} />
      ))}
    </div>
  )
}

const Player = ({ player, toggle, data }) => (
  
  <div onClick={toggle} className="row" >
    {player.playing ?
      <Pause size={20}  style={styles.iconPlay} className="col-4 pl-0 pr-0"/>:
      <Headphones size={20}  style={styles.iconPlay} className="col-4 pl-0 pr-0"/>
    }
    <div className="col-8 pl-0 pr-0" style={styles.nameEng}  title={data}>{data}</div>
  </div>
)

export default MultiPlayerMP3
