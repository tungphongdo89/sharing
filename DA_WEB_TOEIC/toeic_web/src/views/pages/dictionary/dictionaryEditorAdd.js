import React, {Component} from 'react';
import {Dialog, IconButton, withStyles} from '@material-ui/core';
import {formSchema} from './validate';
import ClearIcon from '@material-ui/icons/Clear';
import {Field, Form, Formik} from "formik";
import {Button, Col, FormGroup, Label, ModalHeader} from "reactstrap";
import '../../../assets/scss/pages/vocabulary.scss';
import {bindActionCreators, compose} from "redux";
import {connect} from "react-redux"
import styles from "./styles";
import FileUpload from './fileUpload';
import TextArea from './textArea';

import dictionaryAction from "../../../redux/actions/dictionary/dictionaryAction";
import {validateDescription} from "./validateDescription";
import {FormattedMessage} from "react-intl";
import {showMessage} from "../../../commons/utils/convertDataForToast";


let dictioValidate = false
let checkSpacenameEng = false
let messageDescription = ""

class DictionaryEditorAdd extends Component {

  state = {
    file: null,
    description: null,
    isDisabled: true,
    checkAreaText: true,
    messageDescription: ""

  }

  handleUploadFile = event => {
    this.setState({
      file: event
    })
  }

  // checkDescription = (values) => {
  //   debugger
  //   if (values) {
  //     this.setState({
  //       messageDescription: values
  //     })
  //   } else {
  //     this.setState({
  //       messageDescription: ""
  //     })
  //   }
  //
  // }

  handleTextArea = event => {
    debugger
    if (event.textDesNotHTML !== "" && event.textDesNotHTML.toString().trim() !== "") {
      let keyArea = event.comments;
      let keyArea2 = event.textDesNotHTML.replaceAll('&nbsp;', '  ');
      this.setState({
        description: keyArea,
        checkAreaText: false,
        messageDescription: keyArea2.length > 2000 ? showMessage("description.max.2000.character") : ""
      })

    } else {
      this.setState({
        description: '',
        checkAreaText: true,
        messageDescription: ""
      })
    }
  }

  sys = e => {
    console.log(e.target.value)
    this.setState({
      synonymous: e.target.value
    })
    console.log(this.state.synonymous)
  }


  render() {
    debugger
    let {open, item, dictionaryAction, page, keySearch} = this.props;
    const {classes} = this.props;
    const {file, description, checkAreaText} = this.state;
    return (
      <Dialog
        aria-labelledby="form-dialog-title"
        scroll={"body"}
        open={open}
        maxWidth='md'
        fullWidth={true}
        onKeyDown={(e) => {
          if (e.keyCode === 27) {
            this.props.onConfirmDialogClose();
          }
        }}
        onClose={() => {
          this.props.onConfirmDialogClose()
        }}
      >
        <div className="flex flex-space-between flex-middle  bg-primary ">
          <ModalHeader className='text-centesr'>
            <span
              style={{color: 'white', fontSize: '27px'}}>  {item.id ?
              <>
                {/*'Cập nhật từ vựng'*/}
                {showMessage("update.dictionary")}{' '}
              </>
              :
              <>
                {/*'Thêm mới từ vựng'*/}
                {showMessage("add.dictionary")}{' '}
              </>
            }  </span>
          </ModalHeader>
          <IconButton onClick={() => this.props.onConfirmDialogClose()}>
            <ClearIcon/>
          </IconButton>
        </div>

        <Formik

          initialValues={{
            nameEng: item.nameEng ? item.nameEng : '',
            transcribe: item.transcribe ? item.transcribe : '',
            mp3: item.mp3 ? item.mp3 : "",
            fileUpload: item ? item.fileUpload : null,
            description: item.description ? item.description : '',
            synonymous: item.synonymous ? item.synonymous : ''
          }}

          onSubmit={(values) => {
            debugger
            let dicrionary = new FormData()
            dicrionary.append('nameEng', values.nameEng)
            dicrionary.append('transcribe', values.transcribe)
            dicrionary.append('synonymous', values.synonymous)
            if (file !== null && file !== undefined && file !== "") {
              dicrionary.append('fileUpload', file)
            }
            if (item.description !== null || item.description !== "") {
              dicrionary.append('description', description)
            }
            if (item.id !== null && item.id !== "" && item.id !== undefined) {
              dicrionary.append('id', item.id)
              dicrionary.append('createDate', new Date(item.createDate))
              //this.props.backToFirstPage();
              dictionaryAction.updateDictionary(dicrionary, page, keySearch)
            } else {
              this.props.backToFirstPage();
              dictionaryAction.createDictionary(dicrionary, keySearch)
              this.props.resetDeleteOrAddPage();
            }
            if (item.mp3) {
              dicrionary.append("mp3", item.mp3)
            }
          }}
          validationSchema={formSchema}
          validate={validateDescription}
        >
          {({errors, values, touched, handleChange, setFieldValue, handleBlur, isValid, isSubmitting}) => (
            <Form>
              <br/>
              <br/>
              <FormGroup className="form-group d-flex">
                <Label for="required" sm={2} className={classes.title} style={styles.textTitle}>
                  {/*Tên từ vựng*/}
                  <FormattedMessage id="vocabulary.name"/>{' '}
                  (<span
                  style={{color: "red"}}>*</span>) </Label>
                <Col sm={8}>
                  <Field
                    type="text"
                    name="nameEng"
                    className={`form-control ${errors.nameEng &&
                    touched.nameEng &&
                    "is-invalid"}`}
                    onMouseLeave={(e) => {
                      if (e.target.value.toString().trim() === '') {
                        values.nameEng = ''
                        e.target.value = ""
                      }
                    }}
                  >
                  </Field>
                  {(errors.nameEng && touched.nameEng) || checkSpacenameEng ? (
                    <div className={classes.error}>
                      {/*{errors.nameEng}*/}
                      <FormattedMessage id={errors.nameEng}/>
                    </div>
                  ) : null}
                </Col>
              </FormGroup>
              <FormGroup className="form-group d-flex">

                <Label for="required" sm={2} className={classes.title} style={styles.textTitle}>
                  {/*Phiên âm*/}
                  <FormattedMessage id="pronunciation"/>{' '}
                  (<span
                  style={{color: "red"}}>*</span>)
                </Label>
                <Col sm={8}>

                  <Field
                    type="text"
                    name='transcribe'
                    className={`form-control ${errors.transcribe &&
                    touched.transcribe &&
                    "is-invalid"}`}
                    onMouseLeave={(e) => {
                      if (e.target.value.toString().trim() === '') {
                        values.transcribe = ''
                        e.target.value = ""
                      }
                    }}
                  >
                  </Field>
                  {errors.transcribe && touched.transcribe ? (
                    <div className={classes.error}>
                      {/*{errors.transcribe}*/}
                      <FormattedMessage id={errors.transcribe}/>
                    </div>
                  ) : null}
                </Col>
              </FormGroup>
              <FormGroup className="form-group d-flex">
                <Label for="required" sm={2} className={classes.title} style={styles.textTitle}>
                  {/*Từ đồng nghĩa*/}
                  <FormattedMessage id={"synonym"}/>
                </Label>
                <Col sm={8}>
                  <Field
                    as="textarea"
                    rows="1"
                    style={{width: '100%'}}
                    maxLength={"501"}
                    className={`form-control ${errors.synonymous &&
                    touched.synonymous &&
                    "is-invalid"}`}
                    aria-label="maximum height"
                    name="synonymous"
                    onChange={handleChange}
                    value={values.synonymous}
                  />
                  {errors.synonymous && touched.synonymous ? (
                    <div className={classes.error}>
                      {/*{errors.synonymous}*/}
                      <FormattedMessage id={errors.synonymous}/>
                    </div>
                  ) : null}
                </Col>
              </FormGroup>
              <FileUpload
                file={file}
                handleUploadFile={this.handleUploadFile}
                values={values}
                errors={errors}
                touched={touched}
                setFieldValue={setFieldValue}
                handleChange={handleChange}
                handleBlur={handleBlur}
                nameMp3={item.mp3}>
              </FileUpload>
              <FormGroup className="form-group d-flex">
                <Label for="required" sm={2} className={classes.title} style={styles.textTitle}>
                  {/*Mô tả*/}
                  <FormattedMessage id="describe"/>{' '}
                  (<span
                  style={{color: "red"}}>*</span>)
                </Label>

                <Col sm={8}>
                  <TextArea
                    onBlur={this.props.handleBlur}
                    className={`form-control ${errors.description &&
                    touched.description &&
                    "is-invalid"}`}
                    name="description"
                    textHTML={description}
                    disables={false}
                    textDescription={item.description}
                    handleTextArea={this.handleTextArea}
                    // checkDescription={(value) => this.checkDescription(value)}
                  />
                  {this.state.messageDescription ? (
                    < div className={classes.error}>{this.state.messageDescription}</div>
                  ) : null}
                </Col>


              </FormGroup>
              <hr/>
              <FormGroup className="form-group d-flex">
                <Col className="text-left">
                  <Button color="light" style={{marginLeft: '100px', color: 'black'}}
                          onClick={() => this.props.onConfirmDialogClose()}
                          className="btn btn-secondary btn-modal">
                    {/*Hủy*/}
                    <FormattedMessage id="cancel"/>
                  </Button>
                </Col>
                <Col className="text-right">
                  <Button color="primary" className="btn btn-primary btn-modal"
                          style={{marginRight: '100px', color: 'black'}}
                          disabled={!isValid || this.state.checkAreaText || this.state.messageDescription}
                          type="submit">OK</Button>
                </Col>
              </FormGroup>
            </Form>
          )}
        </Formik>
      </Dialog>
    );
  }
}


const mapStateToProps = (state) => {
  return {}
}
const mapDispatchToProps = (dispatch) => {
  return {
    dictionaryAction: bindActionCreators(dictionaryAction, dispatch),
  }
}


export default compose(withStyles(styles), connect(mapStateToProps, mapDispatchToProps))(DictionaryEditorAdd)
