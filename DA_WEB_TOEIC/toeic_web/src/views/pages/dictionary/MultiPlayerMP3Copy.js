import React, { useState, useEffect } from 'react'
import styles from "./styles";
import {Headphones, Pause} from "react-bootstrap-icons";

const useMultiAudio = (urls) => {
  const [sources] = useState(
    urls => {
      return {
        urls,
        audio: new Audio(urls),
      }
    },
  )
  const [players, setPlayers] = useState(
    urls => {
      return {
        urls,
        playing: false,
      }
    },
  )

  const toggle = targetIndex => () => {
    debugger
    const newPlayers = players
    if(players.playing===true){
      newPlayers[targetIndex].playing = false
    }
    else {
      newPlayers[targetIndex].playing = true
    }
    setPlayers(newPlayers)
    // const currentIndex = players.findIndex(p => p.playing === true)
    // if (currentIndex !== -1 && currentIndex !== targetIndex) {
    //   newPlayers[currentIndex].playing = false
    //   newPlayers[targetIndex].playing = true
    // } else if (currentIndex !== -1) {
    //   newPlayers[targetIndex].playing = false
    // } else {
    //   newPlayers[targetIndex].playing = true
    // }
    // setPlayers(newPlayers)
  }

  useEffect(() => {
      players.playing ? sources.audio.play() : sources.audio.pause()
  }, [sources, players])

  useEffect(() => {

      sources.audio.addEventListener('ended', () => {
        const newPlayers = players
        newPlayers.playing = false
        setPlayers(newPlayers)
      })

    return () => {
        sources.audio.removeEventListener('ended', () => {
          const newPlayers = players
          newPlayers.playing = false
          setPlayers(newPlayers)
        })

    }
  }, [])

  return [players, toggle]
}

const MultiPlayerMP3Copy = ({ urls, data}) => {
  const [players, toggle] = useMultiAudio(urls)
  return (
    <div  >
        <Player player={players} toggle={toggle} data={data} />

    </div>
  )
}

const Player = ({ player, toggle, data }) => (

  <div onClick={toggle} className="row" >
    {player.playing ?
      <Pause size={20}  style={styles.iconPlay} className="col-4 pl-0 pr-0"/>:
      <Headphones size={20}  style={styles.iconPlay} className="col-4 pl-0 pr-0"/>
    }
    <div className="col-8 pl-0 pr-0" style={styles.nameEng}  title={data}>{data}</div>
  </div>
)


export default MultiPlayerMP3Copy
