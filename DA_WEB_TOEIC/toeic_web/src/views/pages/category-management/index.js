import React from "react"
import {Button, Card, CardHeader, Nav, NavItem, NavLink, TabContent, TabPane,} from "reactstrap"

import {bindActionCreators, compose} from "redux";
import {connect} from "react-redux";
import {Typography} from "@material-ui/core";
import CardBody from "reactstrap/es/CardBody";
import FormDetail from "./common-tab/form-detail";
import FormInput from "./common-tab/form-input";
import {Form, Formik} from "formik";
import FormAddQuestion from './common-tab/form-questions/index';
import * as Yup from "yup";
import categoryActions from "../../../redux/actions/category-action"
import {toast} from "react-toastify";
import Spinner from "reactstrap/es/Spinner";
// import {showMessage} from "../../../../commons/utils/convertDataForToast";
import {FormattedMessage} from "react-intl";
import {showMessage} from "../../../commons/utils/convertDataForToast";

const FILE_SIZE = 5 * 1024 * 1024;
const SUPPORTED_FORMATS = [
  "image",
  "audio"
];
const SUPPORTED_FORMATS_CATEORY = [
  "image/jpg",
  "image/jpeg",
  "image/png",
  "audio/mpeg",
  "audio/mp3"
];

const formSchema = Yup.object().shape({
  nameCategory: Yup.string().required(
    // "Tên đề bài không được để trống"
    showMessage("category.name.is.required")
  ),
  levelCode: Yup.string().required(
    // "Trình độ không được để trống"
    showMessage("level.is.required")
  ),
  typeCode: Yup.string().required(
    // "Loại đề bài không được để trống"
    showMessage("type.of.category.is.required")
  ),
  fileUpload1: Yup
    .mixed()
    .test(
      "fileSize",
      // "Tệp quá lớn",
      showMessage("file.is.too.big"),
      value => value ? value.size <= FILE_SIZE ? true : false : true
    )
    .test(
      "fileFormat",
      // "Tệp không đúng định dạng ảnh/âm thanh",
      showMessage("file.is.not.match.format"),
      (value) =>
        value ? SUPPORTED_FORMATS_CATEORY.includes(value.type) ? true : false : true
    )
    // .when("fileUpload2", (node, fileUpload2,schema) => {
    //   debugger;
    //   if (node.value && fileUpload2) {
    //     return node.value.name == fileUpload2.name
    //     && node.value.size == fileUpload2.size
    //     && node.value.type == fileUpload2.type ?
    //       this.createError({path: `fileUpload2`, message: "file đã có trong câu hỏi"})
    //       : schema
    //   } else {
    //     return schema
    //   }
    // })
    .nullable()
  ,
  fileUpload2: Yup
    .mixed()
    .test(
      "fileSize",
      // "Tệp quá lớn",
      showMessage("file.is.too.big"),
      value => value ? value.size <= FILE_SIZE ? true : false : true
    )
    .test(
      "fileFormat",
      // "Tệp không đúng định dạng ảnh/âm thanh",
      showMessage("file.is.not.match.format"),
      (value) =>
        value ? SUPPORTED_FORMATS_CATEORY.includes(value.type) ? true : false : true
    )
    .test("duplicateFile","File đã tồn tại trong đề bài" ,function(value){
      let {fileUpload1} = this.parent;
      if (fileUpload1 && value) {
        if( fileUpload1.name == value.name
        && fileUpload1.size == value.size
        && fileUpload1.type == value.type) {
          return false
        }else return true
      } else {
        return true
      }
    })
    // .when("fileUpload1",{
    //   is: fileUpload1 => (
    //     fileUpload1.name && fileUpload1.type && fileUpload1.size ? true : false ),
    //   then: Yup.string().of()
    // })
    .nullable(),
  part: Yup.string().nullable(),
  //===================validate Form Question====================
  listQuestion: Yup.array().of(
    Yup.object().shape({
      score: Yup.number().min(0, "Điểm không được âm").typeError("Điểm phải là dạng số")
        .required(
          // "Điểm câu hỏi không được để trống"
          showMessage("score.of.the.test.is.required")
        ),
      name: Yup.string().required(
        // "Câu hỏi phải có nội dung"
        showMessage("the.question.must.has.content")
      ),
      typeFile1: Yup.string().nullable(),
      typeFile2: Yup.string().nullable(),
      typeQuestion: Yup.number(),
      fileUpload1: Yup
        .mixed().test(
          "fileSize",
          // "Tệp quá lớn",
          showMessage("file.is.too.big"),
          (value) => value ? value.size <= FILE_SIZE : true
        )
        .test(
          "fileFormat",
          // "Tệp không đúng định dạng ảnh/âm thanh",
          showMessage("file.is.not.match.format"),
          (value) =>
            value ? SUPPORTED_FORMATS.includes(value.type.split("/")[0]) : true
        ).nullable(),
      fileUpload2: Yup
        .mixed().test(
          "fileSize",
          // "Tệp quá lớn",
          showMessage("file.is.too.big"),
          (value) => value ? value.size <= FILE_SIZE : true
        )
        .test(
          "fileFormat",
          // "Tệp không đúng định dạng ảnh/âm thanh",
          showMessage("file.is.not.match.format"),
          (value) =>
            value ? SUPPORTED_FORMATS.includes(value.type.split("/")[0]) : true
        ),
      listAnswerToChoose: Yup.array().of(
        Yup.object().shape({
          answer: Yup.string().required(
            // "Phương án không được để trống"
            showMessage("options.are.required"),
          ),
          startTime: Yup.string().nullable()
        }).test("duplicateAnswer",
          // "Có phương án bị trùng",
          showMessage("options.are.not.duplicate"),
          function (item) {
          let lstAnswerToChoose = this.parent
          let count =0;
          for(let answerToChoose of lstAnswerToChoose){
            if(answerToChoose.answer===item.answer){
              count++;
            }
          }
          if(count >= 2){
            return this.createError({path: `${this.path}.answer`, message:
                // "Phương án đã tồn tại"
                showMessage("option.is.already.exist")
            })
          }
          return true;
        })
      ).nullable(),
      listCorrectAnswers: Yup.array().of(
        Yup.string().required(
          // "Đáp án không được để trống"
          showMessage("answer.is.required")
        )
          .test("duplicateCorrectAnswer",
            // "Đáp án đã bị trùng",
            showMessage("answer.is.already.exist"),
            function (item) {
            let lstCorrectAnswerToChoose = this.parent
            let count =0;
            for(let correctAnswer of lstCorrectAnswerToChoose){
              if(correctAnswer===item){
                count++;
              }
            }
            if(count >= 2){
              return false;
            }
            return true;
          })
      )
    }).test("duplicateFile", "2 File trong câu hỏi bị trùng nhau", function (item) {
      const file1 = item.fileUpload1;
      const file2 = item.fileUpload2;
      if (file1 && file2) {
        return file1.name == file2.name
        && file1.size == file2.size
        && file1.type == file2.type
          ? this.createError({path: `${this.path}.fileUpload2`, message: "file đã có trong câu hỏi"})
          : true
      } else {
        return true
      }
    }),
  ).required(
    // "Đề bài của bạn phải có ít nhất 1 câu hỏi!"
    showMessage("your.category.must.has.question")
  ),
  totalScore: Yup.number().nullable(),
  targetScore: Yup.number().nullable()
    .when('totalScore', (totalScore, schema) => {
      return schema.max(totalScore, "Điểm mục tiêu phải nhỏ hơn tổng điểm!").min(0, "Điểm không được âm");
    }).typeError("Điểm mục tiêu phải là dạng số"),

})

class CategoryForm extends React.Component {

  state = {
    active: "1",
    isLoading: false
  }

  componentDidMount() {
    const { categoryAction } = this.props;
    const { getListTypeFileUpload } = categoryAction;
    getListTypeFileUpload()
  }

  toggle = tab => {
    if (this.state.active !== tab) {
      this.setState({active: tab})
    }
  }

  componentWillReceiveProps(nextProps, nextContext) {
    const {loading} = nextProps;
    if (!loading) {
      this.setState({
        isLoading: false
      });
    }
  }

  render() {
    const {isViewDetail, detailCategory} = this.props;
    return (
      <div>
        <Formik
          enableReinitialize
          initialValues={{
            nameCategory: detailCategory ? detailCategory.nameCategory : "",
            levelCode: detailCategory ? detailCategory.levelCode : "",
            status: detailCategory ? detailCategory.status ? detailCategory.status : 0 : 1,
            typeCode: detailCategory ? detailCategory.typeCode ? detailCategory.typeCode : "" : "",
            part: detailCategory ? detailCategory.part : "",
            parentId: detailCategory ? detailCategory.parentId : 0,
            typeFile1: detailCategory ? detailCategory.typeFile1 : "",
            pathFile1: detailCategory ? detailCategory.pathFile1 : "",
            typeFile2: detailCategory ? detailCategory.typeFile2 : "",
            pathFile2: detailCategory ? detailCategory.pathFile2 : "",
            transcript: detailCategory ? detailCategory.transcript : "",
            fileUpload1: detailCategory ? detailCategory.fileUpload1 : null,
            fileUpload2: detailCategory ? detailCategory.fileUpload2 : null,
            targetScore: detailCategory ? detailCategory.targetScore ? detailCategory.targetScore : 0 : 0,
            totalScore: detailCategory ? detailCategory.totalScore ? detailCategory.totalScore : 0 : 0,
            listQuestion: detailCategory
              ? detailCategory.listQuestion
                ? detailCategory.listQuestion.length < 1
                && !isViewDetail && detailCategory.categoryId
                  ? [
                    {
                      typeQuestion: 1,
                      score: 0,
                      name: "",
                      typeFile1: "LINK",
                      typeFile2: "LINK",
                      pathFile1: "",
                      pathFile2: "",
                      fileUpload1: null,
                      fileUpload2: null,
                      transcript: "",
                      listCorrectAnswers: [""],
                      description: "",
                      listAnswerToChoose: [{answer: "", startTime: null}]
                    }
                  ] : detailCategory.listQuestion :
                [
                  {
                    typeQuestion: 1,
                    score: 0,
                    name: "",
                    typeFile1: "LINK",
                    typeFile2: "LINK",
                    pathFile1: "",
                    pathFile2: "",
                    fileUpload1: null,
                    fileUpload2: null,
                    transcript: "",
                    listCorrectAnswers: [""],
                    description: "",
                    listAnswerToChoose: [{answer: "", startTime: null}]
                  }
                ] : [
                {
                  typeQuestion: 1,
                  score: 0,
                  name: "",
                  typeFile1: "LINK",
                  typeFile2: "LINK",
                  pathFile1: "",
                  pathFile2: "",
                  fileUpload1: null,
                  fileUpload2: null,
                  transcript: "",
                  listCorrectAnswers: [""],
                  description: "",
                  listAnswerToChoose: [{answer: "", startTime: null}]
                }
              ]
          }}
          validationSchema={formSchema}
          onSubmit={(values, {setSubmitting}) => {
            setSubmitting(false);
            this.setState({
              isLoading: true
            })
            const {categoryAction} = this.props;
            const {createCategory, updateCategory} = categoryAction;
            let formData = new FormData();
            debugger;
            formData.append("nameCategory", values.nameCategory)
            formData.append("levelCode", values.levelCode)
            formData.append("status", values.status)
            formData.append("typeCode", values.typeCode)
            formData.append("parentId", values.parentId)

            formData.append("typeFile1", values.typeFile1 !== null && values.typeFile1 !== undefined ? values.typeFile1 : "")

            formData.append("typeFile2", values.typeFile2 !== null && values.typeFile2 !== undefined ? values.typeFile2 : "")

            // Nếu có thi append ko thì thôi
            if (values.fileUpload1 !== null && values.fileUpload1 !== undefined) {
              formData.append("fileUpload1", values.fileUpload1)
            }

            if (values.fileUpload2 !== null && values.fileUpload2 !== undefined) {
              formData.append("fileUpload2", values.fileUpload2)
            }

            formData.append("pathFile1", values.pathFile1 !== null && values.pathFile1 !== undefined ? values.pathFile1 : "")

            formData.append("pathFile2", values.pathFile2 !== null && values.pathFile2 !== undefined ? values.pathFile2 : "")

            formData.append("transcript", values.transcript !== null && values.transcript !== undefined ? values.transcript : "")

            // Append các thuộc tính của question trong listQuestion
            values.listQuestion.forEach(function (item, index) {
              formData.append("listQuestion[" + index + "].typeQuestion", item.typeQuestion)
              formData.append("listQuestion[" + index + "].score", item.score)
              formData.append("listQuestion[" + index + "].name", item.name)
              formData.append("listQuestion[" + index + "].typeFile1", item.typeFile1)
              formData.append("listQuestion[" + index + "].typeFile2", item.typeFile2)
              formData.append("listQuestion[" + index + "].transcript", item.transcript)
              if (item.parentId !== null && item.parentId !== undefined) {
                formData.append("listQuestion[" + index + "].parentId", item.parentId)
              }
              if (item.status !== null && item.status !== undefined) {
                formData.append("listQuestion[" + index + "].status", item.status)
              }

              let indexQuestion = index
              // listAnsưerToChoose
              if (item.listAnswerToChoose) {
                item.listAnswerToChoose.forEach(function (itemAnswerToChoose, index) {
                  formData.append("listQuestion[" + indexQuestion + "].listAnswerToChoose[" + index + "].answer", itemAnswerToChoose.answer)
                  formData.append("listQuestion[" + indexQuestion + "].listAnswerToChoose[" + index + "].startTime", itemAnswerToChoose.startTime)
                })
              }

              // listAnswerCorrect
              let stringAnswerCorrect = ""
              if (item.listCorrectAnswers) {
                item.listCorrectAnswers.forEach(function (itemAnswerCorrect,index) {
                  // stringAnswerCorrect += itemAnswerCorrect
                  formData.append("listQuestion[" + indexQuestion + "].listCorrectAnswers["+index+"]", itemAnswerCorrect)
                })
              }


              //--------------------------------------------------------------------------------

              if (item.fileUpload1 !== null && item.fileUpload1 !== undefined) {
                formData.append("listQuestion[" + index + "].fileUpload1", item.fileUpload1)
              }

              if (item.fileUpload2 !== null && item.fileUpload2 !== undefined) {
                formData.append("listQuestion[" + index + "].fileUpload2", item.fileUpload2)
              }

              formData.append("listQuestion[" + index + "].pathFile1", item.pathFile1 !== null && item.pathFile1 !== undefined ? item.pathFile1 : "")

              formData.append("listQuestion[" + index + "].pathFile2", item.pathFile2 !== null && item.pathFile2 !== undefined ? item.pathFile2 : "")

              formData.append("listQuestion[" + index + "].description", item.description !== null && item.description !== undefined ? item.description : "")
            })
            if (detailCategory && detailCategory.categoryId) {
              formData.append("categoryId", detailCategory.categoryId)
              values.listQuestion.forEach(function (item, index) {
                if (item.id) {
                  formData.append("listQuestion[" + index + "].id", item.id)
                }
              })
              updateCategory(formData);
            } else {
              createCategory(formData);
            }
          }}
        >
          {({errors, touched, values, handleChange, setFieldValue, handleBlur}) => (
            <Form>
              <Card>
                <CardHeader>
                  <div className=" w-100">
                    <Typography variant="h4" component="h2" gutterBottom style={{float: "left"}}>
                      {isViewDetail && detailCategory && detailCategory.categoryId ?
                        <>
                           {/*'Chi tiết đề bài '*/}
                          <FormattedMessage id="detail.category" tagName="data" />
                        </>

                        : (detailCategory && detailCategory.categoryId ?
                          <>
                            {/*'Sửa đề bài'*/}
                            <FormattedMessage id="update.category" tagName="data" />
                          </>
                          :
                          <>
                            {/*'Thêm mới đề bài'*/}
                            <FormattedMessage id="add.category" tagName="data" />
                          </>
                          )}
                    </Typography>
                    <div style={{float: "right"}}>
                      {
                        this.state.isLoading && <Spinner color="primary" className="mr-1"/>
                      }
                      <Button color="warning">Preview</Button>
                      <Button color="primary" className="ml-1" type="submit" onClick={() => {
                        if (JSON.stringify(errors).length > 2) {
                          toast.warn("Bạn chưa nhập đầy đủ thông tin bắt buộc");
                        } else {
                          return null;
                        }
                      }}>Lưu</Button>

                    </div>
                  </div>
                </CardHeader>
                <CardBody>
                  <Nav tabs>
                    <NavItem>
                      <NavLink
                        className={({
                          active: this.state.active === "1"
                        })}
                        style={{fontSize: "1.25rem"}}
                        onClick={() => {
                          this.toggle("1")
                        }}

                      >
                        {/*Thông tin chung*/}
                        <FormattedMessage id="general.information" tagName="data" />
                      </NavLink>
                    </NavItem>
                    <NavItem>
                      <NavLink
                        className={({
                          active: this.state.active === "2"
                        })}
                        disabled={values && values.part &&
                        (values.part === "PART3" || values.part === "PART4" || values.part === "PART6" || values.part === "PART7" || values.part === "" || values.part === "PART8" || detailCategory.part === "PART8"
                          || detailCategory.part === "PART3" || detailCategory.part === "PART4" || detailCategory.part === "PART6" || detailCategory.part === "PART7" || detailCategory.part === "")
                          ? false : true
                        }
                        style={{fontSize: "1.25rem"}}
                        onClick={() => {
                          if (values.nameCategory === undefined || values.levelCode === undefined || values.typeCode === undefined) {
                            this.toggle("1")
                            toast.warn(
                              // "Vui lòng nhập đủ thông tin"
                              showMessage("please.complete.information")
                            )
                          } else {
                            this.toggle("2")
                          }

                        }}
                      >
                        {/*Dữ liệu đầu vào*/}
                        <FormattedMessage id="input.data" tagName="data" />
                      </NavLink>
                    </NavItem>
                    <NavItem>
                      <NavLink
                        className={({
                          active: this.state.active === "3"
                        })}
                        style={{fontSize: "1.25rem"}}
                        onClick={() => {
                          if (values.nameCategory === undefined || values.levelCode === undefined || values.typeCode === undefined) {
                            this.toggle("1")
                            toast.warn(
                              // "Vui lòng nhập đầy đủ thông tin"
                              showMessage("please.complete.information")
                            )
                          } else {
                            this.toggle("3")
                          }
                        }}
                      >
                        {/*Danh sách câu hỏi*/}
                        <FormattedMessage id="list.of.questions" tagName="data" />
                      </NavLink>
                    </NavItem>
                  </Nav>
                  <TabContent activeTab={this.state.active}>
                    <TabPane tabId="1">
                      <FormDetail
                        values={values}
                        errors={errors}
                        touched={touched}
                        handleChange={handleChange}
                        setFieldValue={setFieldValue}
                        handleBlur={handleBlur}
                        updateTypeExercise={(e) => {
                          values.typeCode = parseInt(e.target.value)
                        }}
                        updatePart={(e) => {
                          if(e.target.value == 3 || e.target.value == 4){
                            values.part = ""
                          } else {
                            values.part = e.target.value
                          }
                        }}
                        // updateTopic={(e) => {values.parentId = parseInt(e.target.value)}}
                      />
                    </TabPane>
                    <TabPane tabId="2">
                      <FormInput
                        values={values}
                        errors={errors}
                        touched={touched}
                        setFieldValue={setFieldValue}
                        handleChange={handleChange}
                        handleBlur={handleBlur}
                        // updateFileUpload1={(e) => {
                        //   values.fileUpload1 = e.target.files[0]
                        // }}
                        // updateFileUpload2={(e) => {
                        //   values.fileUpload2 = e.target.files[0]
                        // }}
                      />
                    </TabPane>
                    <TabPane tabId="3">
                      <FormAddQuestion
                        values={values}
                        errors={errors}
                        touched={touched}
                        handleChange={handleChange}
                        setFieldValue={setFieldValue}
                        handleBlur={handleBlur}
                      />
                    </TabPane>
                  </TabContent>
                </CardBody>
              </Card>
            </Form>
          )}
        </Formik>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    isViewDetail: state.categoryReducer.isViewDetail,
    detailCategory: state.categoryReducer.detailCategory,
    loading: state.categoryReducer.loading
  }
}

const mapDispatchToProps = dispatch => ({
  categoryAction: bindActionCreators(categoryActions, dispatch)
})

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
);

export default compose(
  withConnect
)(CategoryForm);

