const styles = () => ({
  buttonFile  :{
    width: '0.1px',
    height: '0.1px',
    opacity: '0',
    overflow: 'hidden',
    position: 'absolute',
    zIndex: '-1'
  },
  labelFile:{
    lineHeight: '36px',
    cursor: 'pointer',
    backgroundColor: '#5C5AA7',
    color: '#FFFFFF',
    borderRadius: '3px',
    webkitAppearance: 'none',
    border: 'none',
    outline: 'none',
    background: 'transparent',
    height: '36px',
    padding: '0px 16px',
    margin: '0',
    fontSize: '14px',
    fontWeight: '400',
    textAlign: 'center',
    minWidth: '70px',
    transition: 'all 0.3s ease-out'
  }
,
  buttonAdd:{
    paddingTop: '1.2em ! important',
    marginTop: '1em ! important',
    paddingLeft:'2em ! important',
  },
  buttonSearch:{
    paddingTop: '1.2em ! important',
    marginTop: '1em ! important',

  },
  search:{
    paddingLeft:'4em'
  },
  error:{
    color:'red',

  }
})


export default styles;
