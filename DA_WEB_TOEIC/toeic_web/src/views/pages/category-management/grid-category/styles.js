const styles = (theme) =>({
  buttonAdd:{
    paddingTop: '1.2em ! important',
    marginTop: '1em ! important',
    paddingLeft:'2em ! important',
  },
  buttonSearch:{
    paddingTop: '1.2em ! important',
    marginTop: '1em ! important',

  },
  search:{
    paddingLeft:'4em'
  },
  error:{
    color:'red',

  }
})
export default styles;
