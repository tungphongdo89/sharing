import React, {Component} from 'react';
import {Form} from "formik";
import {Button, Col, FormGroup} from "reactstrap";
import Spinner from "reactstrap/es/Spinner";
import {bindActionCreators, compose} from "redux";
import {connect} from "react-redux"
import categoryAction from '../../../../redux/actions/category-action/index';
import {showMessage} from "../../../../commons/utils/convertDataForToast";
import {FormattedMessage} from "react-intl";

class FormDeleteCategory extends Component {
  state = {
    loading: false,

  }
  cancel = (model) => {
    this.props.deleteCategory(model);
  }


  render() {
    const {loading, categoryId, typeCode} = this.props;
    return (
      <>
       <style>{`
          #btnHuy {
            padding: 10px 25px;
            border-radius: 5px;
            background-color: #babfc7;
            color: white;
            border: none;
          }

          #btnOK {
            padding: 10px 25px;
            border-radius: 5px;
            background-color: #4839eb;
            color: white;
            border: none;
          }
       `}</style>

        <div>
          <Form>
            <br/>
            <hr/>
            <br/><br/>
            <FormGroup className="form-group d-flex text-center">
              <Col sm={12} md={12} lg={12} xl={12} xs={12}>
                <p style={{fontSize: '16px', fontWeight: 'bold'}}>
                  {/*Bạn chắc chắn muốn xóa đề bài này?*/}
                  <FormattedMessage id="are.you.sure.you.want.to.delete.this.category" tagName="data" />
                </p>
              </Col>
            </FormGroup>
            <br/>
            <hr/>
            <FormGroup className="d-flex">
              <Col>
                <button id="btnHuy" onClick={() => {
                  this.cancel(false)
                }}>
                  {/*Hủy*/}
                  <FormattedMessage id="cancel" tagName="data" />
                </button>
              </Col>
              <Col className="text-center">
                {
                  loading && <Spinner color="primary"/>
                }
              </Col>
              <Col className="text-right">
                <button id="btnOK" onClick={() => {
                  debugger
                  this.setState({
                    loading: true
                  })

                  let category = {};
                  category.categoryId = categoryId;
                  category.typeCode = typeCode;
                  const {categoryAction} = this.props;
                  const {deleteCategory} = categoryAction;
                  deleteCategory(category);
                  setTimeout(() => {
                    this.cancel(false)
                  }, 3000)
                }}>OK</button>
              </Col>
            </FormGroup>
          </Form>
        </div>
      </>

    )
  }
}

const mapStateToProps = (state) => {
  return {
    loading: state.categoryReducer.loadingCategory
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    categoryAction: bindActionCreators(categoryAction, dispatch)
  }
}

export default compose(connect(mapStateToProps, mapDispatchToProps))(FormDeleteCategory)
