  import React from "react"
import {Pagination, PaginationItem, PaginationLink} from "reactstrap"
import {bindActionCreators, compose} from "redux";
import categoryAction from "../../../../redux/actions/category-action";
import topicAction from "../../../../redux/actions/topic/topicAction";
import {withStyles} from "@material-ui/core";
import styles from "./styles";
import {connect} from "react-redux";

class PaginationIconsAndText extends React.Component {
  state = {
    activeTab: "1",
    currentPage: 1,
    endPage: 1,
  }
  getCurrentPage = (page) => {
    this.props.getCurrentPage(page)
  }
  generationPage = (pageCount) => {
    let pagingPage = [];
    if(this.state.currentPage <= 5){
      if(pageCount < 5){
        for (let i = 1; i <= pageCount; i++) {
          pagingPage.push(
            <PaginationItem active={i === this.props.activePage ? true : false}>
              <PaginationLink value={i} onClick={() => {
                this.setState({
                  currentPage: i
                })
                this.getCurrentPage(i)
              }}
              >{i}</PaginationLink>
            </PaginationItem>)
        }
      }
      else{
        for (let i = 1; i <= 5; i++) {
          pagingPage.push(
            <PaginationItem active={i === this.props.activePage ? true : false}>
              <PaginationLink value={i} onClick={() => {
                this.setState({
                  currentPage: i
                })
                this.getCurrentPage(i)
              }}
              >{i}</PaginationLink>
            </PaginationItem>)
        }
      }

    }
    else {
      for (let i = this.state.currentPage - 4; i <= this.state.currentPage; i++) {
        pagingPage.push(
          <PaginationItem active={i === this.state.currentPage ? true : false}>
            <PaginationLink value={i} onClick={() => {
              this.setState({
                currentPage: i
              })
              this.getCurrentPage(i)
            }}
            >{i}</PaginationLink>
          </PaginationItem>)
      }
    }

    return pagingPage;

  }

  customPaging = (count) => {
    const {total} = this.props;
    let page = Math.ceil(count / 10);
    // if(total > 0){
    //   if (page > 5) {
    //     return this.generationPage(page)
    //   }
    //   else {
    //     return this.generationPage(page)
    //   }
    // }
    if (page > 5) {
      return this.generationPage(page)
    }
    else {
      return this.generationPage(page)
    }

  }

  render() {
    const {count} = this.props;
    let pageCount = Math.ceil(count / 10);
    return (
      <React.Fragment>
        <Pagination className="d-flex justify-content-center mt-3">
          {/*{1 !== this.state.currentPage?*/}
            {/*<PaginationItem disabled={1 === this.state.currentPage? true : false} >*/}
              {/*<PaginationLink onClick={() => {*/}
                {/*const {currentPage} = this.state;*/}
                {/*if (currentPage !== 1) {*/}
                  {/*this.setState({*/}
                    {/*currentPage: currentPage - 1*/}
                  {/*})*/}
                  {/*this.getCurrentPage(currentPage - 1)*/}
                {/*}*/}
              {/*}}>*/}
                {/*Previous*/}
              {/*</PaginationLink>*/}
            {/*</PaginationItem> : null*/}
          {/*}*/}
          <PaginationItem disabled={1 === this.state.currentPage? true : false} >
            <PaginationLink onClick={() => {
              const {currentPage} = this.state;
              if (currentPage !== 1) {
                this.setState({
                  currentPage: currentPage - 1
                })
                this.getCurrentPage(currentPage - 1)
              }
            }}>
              Previous
            </PaginationLink>
          </PaginationItem>

          {this.customPaging(count)}


            <PaginationItem disabled={pageCount === this.state.currentPage? true : false}>
              <PaginationLink onClick={() => {
                let {currentPage} = this.state;
                if (currentPage < pageCount) {
                  this.setState({
                    currentPage: currentPage + 1
                  })
                  this.getCurrentPage(currentPage + 1)
                }
              }}>
                Next
              </PaginationLink>
            </PaginationItem>

        </Pagination>
      </React.Fragment>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    total: state.categoryReducer.total,
    totalTopic: state.topicReducer.totalTopic
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
  }
}

export default compose(withStyles(styles()), connect(mapStateToProps, mapDispatchToProps))(PaginationIconsAndText)

// export default PaginationIconsAndText
