import React from "react"
import categoryAction from '../../../../redux/actions/category-action/index';
import topicAction from '../../../../redux/actions/topic/topicAction';
import {Link} from 'react-router-dom';
import {bindActionCreators, compose} from 'redux';
import {connect} from "react-redux"
import {IconButton, TableCell, TableFooter, TableRow, withStyles} from "@material-ui/core";
import {Form, Formik, Field} from "formik"
import MUIDataTable from "mui-datatables";
import styles from './styles'
import FormDeleteCategory from './formDeleteCategory';
import * as Yup from "yup"
import {
  Button,
  Card,
  CardBody,
  CardHeader,
  CardTitle,
  Col,
  Collapse,
  FormGroup,
  Input,
  Label,
  Modal,
  ModalHeader,
  Row,
  Spinner
} from "reactstrap"

import {ChevronDown, Edit, Eye, RotateCw, Search, Trash2, X} from "react-feather"
import classnames from "classnames"
import "../../../../assets/scss/plugins/tables/_agGridStyleOverride.scss"
import "../../../../assets/scss/pages/users.scss"
import {getUTCTime} from "../../../../commons/utils/DateUtils";
import {history} from "../../../../history";
import PaginationIconsAndText from './pagingtion';
import OverlayTrigger from "react-bootstrap/OverlayTrigger";
import Tooltip from "react-bootstrap/Tooltip";
import ClearIcon from '@material-ui/icons/Clear';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import FormDeleteTest from "../../list-test-management";
import {showMessage} from "../../../../commons/utils/convertDataForToast";
import {FormattedMessage} from "react-intl";

let optionPartByTopic;
let optionNameTopicByPartAndTypeTopic;
let optionTypeTopic;
let optionLevelCode;

const formSchema = Yup.object().shape({
  createDateFrom: Yup.string()
    .matches(/^((2000|2400|2800|(19|2[0-9](0[48]|[2468][048]|[13579][26])))-02-29)$|^(((19|2[0-9])[0-9]{2})-02-(0[1-9]|1[0-9]|2[0-8]))$|^(((19|2[0-9])[0-9]{2})-(0[13578]|10|12)-(0[1-9]|[12][0-9]|3[01]))$|^(((19|2[0-9])[0-9]{2})-(0[469]|11)-(0[1-9]|[12][0-9]|30))$/, "Ngày tháng không đúng định dạng"),
  createDateTo: Yup.string()
    .matches(/^((2000|2400|2800|(19|2[0-9](0[48]|[2468][048]|[13579][26])))-02-29)$|^(((19|2[0-9])[0-9]{2})-02-(0[1-9]|1[0-9]|2[0-8]))$|^(((19|2[0-9])[0-9]{2})-(0[13578]|10|12)-(0[1-9]|[12][0-9]|3[01]))$|^(((19|2[0-9])[0-9]{2})-(0[469]|11)-(0[1-9]|[12][0-9]|30))$/, "Ngày tháng không đúng định dạng")

});

let errDate1 = "";
let errDate2 = "";

function validateDateFrom(date,value) {
  debugger
  if (value.createDateFrom === "") {
    if (document.getElementById("createDateFromCategory") !== null && document.getElementById("createDateFromCategory").validationMessage !== "") {
      errDate1 = showMessage("date.not.correct.format");
    } else {
      errDate1 = ""
    }
  } else {
    if (!/^((2000|2400|2800|(19|2[0-9](0[48]|[2468][048]|[13579][26])))-02-29)$|^(((19|2[0-9])[0-9]{2})-02-(0[1-9]|1[0-9]|2[0-8]))$|^(((19|2[0-9])[0-9]{2})-(0[13578]|10|12)-(0[1-9]|[12][0-9]|3[01]))$|^(((19|2[0-9])[0-9]{2})-(0[469]|11)-(0[1-9]|[12][0-9]|30))$/i.test(value.createDateFrom)) {
      errDate1 = showMessage("date.not.correct.format");
    } else if (value.createDateFrom && value.createDateTo) {
      if (Date.parse(value.createDateFrom) > Date.parse(value.createDateTo)) {
        errDate1 = showMessage("compare.time");
      } else {
        errDate1 = ""
      }
    } else {
      errDate1 = ""
    }
  }
}

function validateDateTo(value) {
  if (value.createDateTo === "") {
    if (document.getElementById("createDateToCategory") !== null && document.getElementById("createDateToCategory").validationMessage !== "") {
      errDate2 = showMessage("date.not.correct.format");
    } else {
      errDate2 = ""
    }
  } else {
    if (!/^((2000|2400|2800|(19|2[0-9](0[48]|[2468][048]|[13579][26])))-02-29)$|^(((19|2[0-9])[0-9]{2})-02-(0[1-9]|1[0-9]|2[0-8]))$|^(((19|2[0-9])[0-9]{2})-(0[13578]|10|12)-(0[1-9]|[12][0-9]|3[01]))$|^(((19|2[0-9])[0-9]{2})-(0[469]|11)-(0[1-9]|[12][0-9]|30))$/i.test(value.createDateTo)) {
      errDate2 = showMessage("date.not.correct.format");
    } else if (value.createDateFrom && value.createDateTo) {
      if (Date.parse(value.createDateFrom) > Date.parse(value.createDateTo)) {
        errDate1 = showMessage("compare.time");;
        errDate2 = ""
      } else {
        errDate1 = ""
      }
    } else {
      errDate2 = ""
    }
  }
}

class GridCategory extends React.Component {
  state = {
    page: 1,
    count: 1,
    searchValue: "",
    pageSize: 10,
    isVisible: true,
    reload: false,
    collapse: true,
    status: "Opened",
    categoryType: "2",
    typeTopic: "",
    part: "",
    codeTopic: "",
    levelCode: "",
    statusCategory: "2",
    createFrom:'',
    createTo:'',
    search: "Nhập thông tin tìm kiếm",
    rowsPerPageOptions: [10],
    categoryId: 0,
    typeCode: 0,
    addDescriptionForPart: true,
    messageFrom: '',
    messageTo: ''
  }

  componentWillMount() {
    let data = {};
    const {categoryAction, topicAction} = this.props;
    const {fetchDataFiler} = categoryAction;
    const {fetchPartByTopic, fetchTopicNameByPartAndTypeTopic} = topicAction;
    fetchDataFiler(data);
    fetchPartByTopic(data);
    fetchTopicNameByPartAndTypeTopic(data);
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    const {total} = prevProps;

    if (total) {
      this.setState({
        count: total
      });
    }


  }

  UNSAFE_componentWillReceiveProps(nextProps, nextContext) {
    const {total} = nextProps.values.dataRes;

    if (total) {
      this.setState({
        count: total
      });
    }
    const {getLevelCode, lstTypeTopic} = nextProps.dataFilter;

    optionTypeTopic = lstTypeTopic !== undefined ? lstTypeTopic.map((type) => {
      return <option value={type.code}>
        {/*{type.name}*/}
        { showMessage(type.code.toLowerCase().replaceAll("_",".")) }
      </option>
    }) : null;
    optionLevelCode = getLevelCode !== undefined ? getLevelCode.map((type) => {
      return <option value={type.code}>
        {/*{type.name}*/}
        { showMessage(type.code.toLowerCase().replaceAll("_",".")) }
      </option>
    }) : null;
  }

  componentWillUpdate(nextProps, nextState, nextContext) {
    const {lstPartByTopic} = nextProps;

    optionPartByTopic = lstPartByTopic !== undefined ? lstPartByTopic.map((type) => {
      if (type.parentCode === "LISTENING_FILL_UNIT" && this.state.addDescriptionForPart === true) {
        return <option value={type.code}>
          {/*{type.name + " (" + showMessage("listening.fill.unit") + ")"}*/}
          {showMessage(type.code.toLowerCase().replaceAll("_",".")) + " (" + showMessage("listening.fill.unit") + ")"}
        </option>
      } else if (type.parentCode === "LISTENING_UNIT" && this.state.addDescriptionForPart === true) {
        return <option value={type.code}>
          {showMessage(type.code.toLowerCase().replaceAll("_",".")) + " (" + showMessage("listening.unit") + ")"}
        </option>
      } else {
        return <option value={type.code}>
          {/*{type.name}*/}
          {showMessage(type.code.toLowerCase().replaceAll("_","."))}
        </option>
      }
    }) : null

    const {lstTopicByTypeTopicAndPart} = nextProps;
    // lstTopicByTypeTopicAndPart.sort(function(a, b) {
    //   if ( a.topicName < b.topicName ){
    //     return -1;
    //   }
    //   if ( a.topicName > b.topicName ){
    //     return 1;
    //   }
    //   return 0;
    // })

    lstTopicByTypeTopicAndPart.sort((a, b) => a.topicName.localeCompare(b.topicName))

    optionNameTopicByPartAndTypeTopic = lstTopicByTypeTopicAndPart !== undefined ? lstTopicByTypeTopicAndPart.map((type) => {
      return <option title={type.topicName.length > 20 ? type.topicName : ""} value={type.code}>
        {type.topicName.length > 20 ? type.topicName.substring(0, 20) + "..." : type.topicName}
      </option>
    }) : null
  }

  componentDidMount() {
    let category = {};
    category.page = this.state.page;
    category.pageSize = this.state.pageSize;
    const {categoryAction} = this.props;
    const {fetchCategory} = categoryAction;
    fetchCategory(category)
    const {values} = this.props;
    const {total} = values;
    this.setState({
      count: total
    })
  }

  toggleModal = (categoryId,typeCode) => {
    this.setState(prevState => ({
      categoryId: categoryId,
      typeCode: typeCode,
      modal: !prevState.modal
    }))
  }
  refreshCard = () => {
    this.setState({reload: true})
    setTimeout(() => {
      this.setState({
        reload: false,
        part: "",
        codeTopic: "",
        typeTopic: "",
        statusCategory: "2",
        categoryType: "",
        levelCode: ""
      })
    }, 500)
  }

  toggleCollapse = () => {
    this.setState(state => ({collapse: !state.collapse}))
  }
  onEntered = () => {
    this.setState({status: "Opened"})
  }
  onEntering = () => {
    this.setState({status: "Opening..."})
  }
  onEntered = () => {
    this.setState({status: "Opened"})
  }
  onExiting = () => {
    this.setState({status: "Closing..."})
  }
  onExited = () => {
    this.setState({status: "Closed"})
  }
  removeCard = () => {
    this.setState({isVisible: false})
  }


  getCurrentPage = (currentPage)=>{
    this.changePage(currentPage);
  }

  changePage = (page) => {
    const {pageSize} = this.state;
    this.setState({
      page: page
    })
    let category = {};
    category.keySearch = this.state.searchValue.toString().trim();
    category.status = parseInt(this.state.statusCategory);
    category.levelCode = this.state.levelCode;
    category.part = this.state.part;
    category.codeTopic = this.state.codeTopic;
    category.code = this.state.typeTopic;
    category.createdDateFrom = this.state.createFrom;
    category.createdDateTo = this.state.createTo;
    category.page = page
    category.pageSize = pageSize
    const {categoryAction} = this.props;
    const {fetchCategory} = categoryAction;
    fetchCategory(category);
  };


  getCategoryFromState = () => {
    debugger
    let category = {};
    category.keySearch = this.state.searchValue.toString().trim();
    category.status = parseInt(this.state.statusCategory);
    category.levelCode = this.state.levelCode;
    category.part = this.state.part;
    category.codeTopic = this.state.codeTopic;
    category.code = this.state.typeTopic;
    category.createdDateFrom = this.state.createFrom;
    category.createdDateTo = this.state.createTo;
    category.page = 1;
    category.pageSize = 10;
    return category;
  }

  deleteCategory = (data) => {
    this.setState({
      modal: data
    })
  }

  handleDateChangeRaw = (e) => {
    e.preventDefault();
  }

  onHandleAdd = () => {
    const {categoryAction} = this.props;
    const {editDetailCategory} = categoryAction;
    editDetailCategory();
    history.push("/pages/category-form")
  }

  render() {
    debugger
    const {page, rowsPerPageOptions, count, categoryId,typeCode} = this.state;
    const {classes} = this.props;
    const options = {
      filter: false,
      filterType: 'checkbox',
      selectableRowsHideCheckboxes: true,
      responsive: "standard",
      selectableRows: "none",
      print: false,
      download: false,
      // count: count,
      search: false,
      viewColumns: false,
      // page: page,
      serverSide: true,
      // rowsPerPageOptions: rowsPerPageOptions,
      customFooter: () => {
        return (
          <TableFooter>
            <TableRow>
              <TableCell colSpan={6}>
                <PaginationIconsAndText activePage={this.state.page} count={count}
                                        getCurrentPage ={ (currentPage)=>{this.getCurrentPage(currentPage)}}/>
              </TableCell>
            </TableRow>
          </TableFooter>
        );
      }
    };
    // const label22 = {
    //   render(){
    //     return (<div style={{textAlign: 'right'}}>
    //       Mã đề bài
    //     </div>)
    //   }
    //
    // }
    const columns = [
      {
        name: 'sl',
        label: showMessage("index"),
        options: {
          filter: true,
          sort: false,
          customHeadLabelRender: () => {
            return <div autoFocus={false} style={{fontWeight: 'bold'}}>
              {/*STT*/}
              <FormattedMessage id="index" tagName="data" />
            </div>;
          },
          customBodyRender: (index) => {
            return (<div style={{textAlign: 'left'}} autoFocus={false}>
              {index}
            </div>)
          }
        }
      },
      {
        name: "categoryId",
        label: "Mã đề bài",
        // label: label22,
        options: {
          filter: true,
          sort: false,
          customHeadLabelRender: () => {
            return <div style={{fontWeight: 'bold'}}>
              {/*Mã đề bài*/}
              <FormattedMessage id="category.code" tagName="data" />
            </div>;
          },
          customBodyRender: (data) => {
            return (<div style={{textAlign: 'left'}}>
              {data}
            </div>)
          }
        }
      },
      {
        name: "typeCode",
        options: {
          filter: true,
         display: false
        }
      },{
        name: "nameCategory",
        label: "Tên đề bài",
        options: {
          filter: true,
          sort: false,
          customHeadLabelRender: () => {
            return <div style={{fontWeight: 'bold'}}>
              {/*Tên đề bài*/}
              <FormattedMessage id="category.name" tagName="data" />
            </div>;
          },
          customBodyRender: (data, dataRow) => {

            return (
              <div style={{width:'80px',display: 'inline-block',
                whiteSpace: 'nowrap',
                overflow: 'hidden',
                textOverflow: 'ellipsis',
                color: 'rgba(22, 155, 213, 1)'
              }} >
                <Link className="text-center"
                      title={data}
                      onClick={(e) => {
                        const {categoryAction, lstCategory} = this.props;
                        const {fetchDetailCategory} = categoryAction;
                        fetchDetailCategory(lstCategory[dataRow.rowIndex], true);
                        history.push("/pages/category-form")
                      }
                      }>{data}</Link>
              </div>
              )
          }
        }
      },
      {
        name: "levelCode",
        label: "Trình độ",
        options: {
          filter: true,
          sort: false,
          customHeadLabelRender: () => {
            return <div style={{fontWeight: 'bold'}}>
              {/*Trình độ*/}
              <FormattedMessage id="level" tagName="data" />
            </div>;
          },
          customBodyRender: (data) => {
            return data === 'EASY' ? (<div>{showMessage("easy")}</div>) :
              data === 'MEDIUM' ? (<div>{showMessage("medium")}</div>) :
                data === 'DIFFICULT' ? (<div>{showMessage("difficult")}</div>) : null
          }
        }
      },
      {
        name: "code",
        label: "Loại đề bài",
        options: {
          filter: true,
          sort: false,
          customHeadLabelRender: () => {
            return <div style={{fontWeight: 'bold'}}>
              {/*Loại đề bài*/}
              <FormattedMessage id="type.of.category" tagName="data" />
            </div>;
          },
          customBodyRender: (data) => {
            return <div>
              <FormattedMessage id={data.toLowerCase().replaceAll("_",".")}/>
            </div>
          }
        }
      }, {
        name: "part",
        label: "Phần",
        options: {
          filter: true,
          sort: false,
          customHeadLabelRender: () => {
            return <div style={{fontWeight: 'bold'}}>
              {/*Phần*/}
              <FormattedMessage id="part" tagName="data" />
            </div>;
          },
          customBodyRender: (data) => {
            return <div>
              <FormattedMessage id={data.toLowerCase().replaceAll("_",".")}/>
            </div>
          }
        }
      }, {
        name: "nameTopic",
        label: "Chủ đề",
        options: {
          filter: true,
          sort: false,
          customHeadLabelRender: () => {
            return <div style={{fontWeight: 'bold'}}>
              {/*Chủ đề*/}
              <FormattedMessage id="topic" tagName="data" />
            </div>;
          },
          customBodyRender: (data, dataRow) => {
            return (
              <div title={data} style={{width:'110px',display: 'inline-block',
                whiteSpace: 'nowrap',
                overflow: 'hidden',
                textOverflow: 'ellipsis',
                color: 'black'
              }} >
                {data}
              </div>
            )
          }
        }
      }, {
        name: "status",
        label: "Trạng thái",
        options: {
          filter: true,
          sort: false,
          customHeadLabelRender: () => {
            return <div style={{fontWeight: 'bold'}}>
              {/*Trạng thái*/}
              <FormattedMessage id="status" tagName="data" />
            </div>;
          },
          customBodyRender: (data) => {
            return data === 1 ? (<div className='badge badge-pill badge-success'>
                {/*Áp dụng*/}
                <FormattedMessage id="being.applied" tagName="data" />
            </div>) :
              data === 0 ? (
                <div className='badge badge-pill badge-light' style={{color: '#000'}}>
                  {/*Ngừng áp dụng*/}
                  <FormattedMessage id="discontinued" tagName="data" />
                </div>) : null
          }
        }
      }, {
        name: "createdDate",
        label: "Thời gian tạo",
        options: {
          filter: true,
          sort: false,
          customHeadLabelRender: () => {
            return <div style={{fontWeight: 'bold', textAlign: 'left'}}>
              {/*Thời gian tạo*/}
              <FormattedMessage id="created.time" tagName="data" />
            </div>;
          },
          customBodyRender: (data) => {
            return (
              <div style={{textAlign: 'center'}}>{data !== null ? (getUTCTime(new Date(data))) : null}
              </div>
            )
          }
        }
      },
      {
        name: "updatedDate",
        label: "Cập nhật lần cuối",
        options: {
          filter: true,
          sort: false,
          customHeadLabelRender: () => {
            return <div style={{fontWeight: 'bold', textAlign: 'left'}}>
              {/*Cập nhật lần cuối*/}
              <FormattedMessage id="last.updated.time" tagName="data" />
            </div>;
          },
          customBodyRender: (data) => {
            return (
              <div style={{textAlign: 'center'}}>{data !== null ? (getUTCTime(new Date(data))) : null}
              </div>
            )
          }
        }
      },


      {
        name: "",
        label: "Thao tác",
        options: {
          filter: true,
          sort: false,
          customHeadLabelRender: () => {
            return <div autoFocus={false} style={{fontWeight: 'bold', textAlign: 'center', paddingRight: '16px'}}>
              {/*Thao tác*/}
              <FormattedMessage id="manipulation" tagName="data" />
            </div>;
          },
          customBodyRender: (data, dataRow) => {
            return (
              <div style={{textAlign: 'center'}}>
                <OverlayTrigger placement="top" overlay={props => (<Tooltip {...props}>
                  {/*Sửa*/}
                  <FormattedMessage id="edit" tagName="data" />
                </Tooltip>)}>
                  <button id="btnHighLight"
                          style={{border: 'none', backgroundColor: 'rgb(0 0 0 / 0%)', marginBottom: '2px'}}
                          onClick={() => {
                            const {categoryAction, lstCategory} = this.props;
                            const {fetchDetailCategory} = categoryAction;
                            fetchDetailCategory(lstCategory[dataRow.rowIndex], false);
                            history.push("/pages/category-form")
                          }}>
                    <Edit size={15} className=" mr-1 fonticon-wrap" style={{cursor: "pointer", color: "#2c00ff"}}
                    /></button>
                </OverlayTrigger>
                <OverlayTrigger placement="top" overlay={props => (<Tooltip {...props}>
                  {/*Xóa*/}
                  <FormattedMessage id="delete" tagName="data" />
                </Tooltip>)}>
                  <button id="btnHighLight"
                          style={{border: 'none', backgroundColor: 'rgb(0 0 0 / 0%)', marginBottom: '2px'}}
                          onClick={() => {
                            console.log("props ===>",dataRow)
                            this.toggleModal(dataRow.rowData[1],dataRow.rowData[2]);
                          }}>
                    <Trash2
                      size={15} className="mr-1 fonticon-wrap" style={{cursor: "pointer", color: "red"}}
                    />
                  </button>
                </OverlayTrigger>
                <OverlayTrigger placement="top" overlay={props => (<Tooltip {...props}>
                  {/*Xem chi tiết*/}
                  <FormattedMessage id="Detail" tagName="data" />
                </Tooltip>)}>
                  <button id="btnHighLight"
                          style={{border: 'none', backgroundColor: 'rgb(0 0 0 / 0%)', marginBottom: '2px'}}
                          onClick={() => {
                            const {categoryAction, lstCategory} = this.props;
                            const {fetchDetailCategory} = categoryAction;
                            fetchDetailCategory(lstCategory[dataRow.rowIndex], true);
                            history.push("/pages/category-form")
                          }}>
                    <Eye size={15} className=" mr-1 fonticon-wrap" style={{cursor: "pointer"}}
                    /></button>
                </OverlayTrigger>
              </div>

            )
          }
        }
      },
    ];

    const validateDate =(value)=>{
      //alert(value)
    }

    const {lstCategory, isLoading} = this.props;
    let newData = [];
    lstCategory.map((item, index) => {
      newData.push({sl: (this.state.pageSize) * (this.state.page - 1) + index + 1, ...item});
    });
    const loadingComponent = (
      <div style={{
        position: 'absolute',
        zIndex: 110,
        top: 0,
        left: 0,
        width: '100%',
        height: '100%',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        background: 'rgba(255,255,255,0.8)'
      }}>
        <Spinner color="primary" className="reload-spinner"/>
      </div>
    );

    return (
      <>
        <style>{`
          #btnHighLightAdd {
            width: 70%;
            height: 40px;
            margin-top: 12px;
            margin-left: 15%;
            border: 1px solid #4839eb;
            background-color: #7367f0;
            border-radius: 5px;
            color: white;
          }

          #styleInput{
            padding: 8px;
            border-radius: 5px;
            border-color: lightgrey;
            width: 100%;
          }

          #btnHighLight:focus-within {
            border: 2px solid black !important;
            box-shadow: 2px 2px 5px gray;
          }

          #btnSubmit {
            margin-top: 0% !important;
            padding: 0.85rem 2rem !important;
          }

          #btnSubmit:focus-within {
            border: none !important;
            outline: 1px solid gray !important;
          }

        `}</style>
        <Row>
          <Col sm="12">
            <Card>
              <CardHeader>
                <CardTitle><h2>
                  {/*Danh sách đề bài*/}
                  <FormattedMessage id="list.of.categories" tagName="data" />
                </h2></CardTitle>
              </CardHeader>
              <Collapse
                isOpen={this.state.collapse}
                onExited={this.onExited}
                onEntered={this.onEntered}
                onExiting={this.onExiting}
                onEntering={this.onEntering}
              >
                <CardBody>
                  {this.state.reload ? (
                    <Spinner color="primary" className="reload-spinner"/>
                  ) : (
                    ""
                  )}
                  <Formik initialValues={{
                    createDateFrom: '',
                    createDateTo: '',
                  }} onSubmit={(values, actions) => {
                    debugger
                    this.setState({
                      loading: true,
                      page: 1,
                      createFrom: values.createDateFrom,
                      createTo: values.createDateTo
                    })

                    let category = this.getCategoryFromState();
                    const {categoryAction} = this.props;
                    const {fetchCategory} = categoryAction;
                    fetchCategory(category);
                  }}
                  validationSchema={formSchema}
                  >
                    {({errors, touched, values}) => (
                      <Form>
                        <FormGroup className="form-group d-flex">
                          <Col lg="2" md="2" sm="2">
                            <FormGroup style={{paddingTop: '0.5em'}} className="mb-0">
                              <button id="btnHighLightAdd" type="button"
                                      onClick={
                                        this.onHandleAdd
                                      }
                              >
                                {/*Thêm mới*/}
                                <FormattedMessage id="add.new" tagName="data" />
                              </button>
                            </FormGroup>

                            <Modal
                              isOpen={this.state.modal}
                              toggle={this.toggleModal}
                              className="modal-dialog-centered"
                            >
                              <Row className="flex flex-space-between flex-middle  bg-primary " style={{width:'100%', marginLeft:'0.2px'}}  >
                                <Col xs={11}>
                                  <ModalHeader  style={{ background: '#7367f0' }}>
                                    <h3 style={{ color: 'white' }}>
                                      {/*Xác nhận xóa đề bài*/}
                                      <FormattedMessage id="confirm.deleting.category" tagName="data" />
                                    </h3>
                                  </ModalHeader>
                                </Col>
                                <Col xs={1} style={{paddingLeft: '0px', paddingRight:'0px'}}>
                                  <IconButton onClick={this.toggleModal}>
                                    <ClearIcon />
                                  </IconButton>
                                </Col>
                              </Row>

                              <FormDeleteCategory categoryId={categoryId} typeCode={typeCode} deleteCategory={(model) => {
                                this.deleteCategory(model)
                              }}/>
                            </Modal>

                          </Col>
                          <Col sm={2} md={2} lg={2} xs={2}>
                            <span>&nbsp;</span>
                            <FormGroup className="position-relative has-icon-left">
                              <div className="w-100">
                                <Input
                                  autoFocus={true}
                                  type="search"
                                  name="searchValue"
                                  id="searchValue"
                                  placeholder={
                                    showMessage("search.info")
                                  }
                                  onChange={e => {
                                    debugger
                                    this.setState(
                                      {
                                        searchValue: e.target.value
                                      }
                                    )
                                  }}
                                />
                                <div className="form-control-position px-1">
                                  <Search size={15}/>
                                </div>
                              </div>
                            </FormGroup>
                          </Col>
                          <Col sm={2} md={2} lg={2} xs={2}>
                            <Label className={classes.fieldLabel} for="department">
                              {/*Loại đề bài*/}
                              <FormattedMessage id="type.of.category" tagName="data" />
                            </Label>
                            <select
                              type="select"
                              name="categoryType"
                              id="styleInput"
                              value={this.state.typeTopic}
                              onChange={e => {
                                let apParamDTO = {};
                                let topic = {};
                                const {topicAction} = this.props;
                                const {fetchPartByTopic, fetchTopicNameByPartAndTypeTopic} = topicAction;
                                this.setState(
                                  {
                                    typeTopic: e.target.value,
                                    part: "",
                                    codeTopic: "",

                                  }
                                )
                                if(e.target.value === ""){
                                  this.setState(
                                    {
                                      addDescriptionForPart: true
                                    }
                                  )
                                }
                                else {
                                  this.setState(
                                    {
                                      addDescriptionForPart: false
                                    }
                                  )
                                }

                                topic.partTopicCode = this.state.part;
                                topic.typeTopicCode = e.target.value;
                                apParamDTO.parentCode = e.target.value;
                                fetchTopicNameByPartAndTypeTopic(topic);
                                fetchPartByTopic(apParamDTO);
                                this.setState({reload: true})
                                setTimeout(() => {
                                  this.setState({
                                    reload: false,
                                  })
                                }, 1000)
                              }}
                            >
                              <option defaultChecked value="">{showMessage("all")}</option>
                              {optionTypeTopic}
                            </select>
                          </Col>
                          <Col sm={2} md={2} lg={2} xs={2}>
                            <FormGroup className="mb-0">
                              <Label className={classes.fieldLabel} for="department">
                                {/*Phần*/}
                                <FormattedMessage id="part" tagName="data" />
                              </Label>
                              <select
                                name="part"
                                id="styleInput"
                                value={this.state.part}
                                onChange={e => {
                                  const {topicAction} = this.props;
                                  const {fetchTopicNameByPartAndTypeTopic} = topicAction;
                                  this.setState(
                                    {
                                      part: e.target.value,
                                      codeTopic: "",
                                    }
                                  )
                                  let topic = {};
                                  topic.typeTopicCode = this.state.typeTopic
                                  topic.partTopicCode = e.target.value;
                                  fetchTopicNameByPartAndTypeTopic(topic);
                                  this.setState({reload: true})
                                  setTimeout(() => {
                                    this.setState({
                                      reload: false,
                                    })
                                  }, 1000)
                                }}
                              >
                                <option value="">{showMessage("all")}</option>
                                {optionPartByTopic}

                              </select>
                            </FormGroup>
                          </Col> <Col sm={2} md={2} lg={2} xs={2}>
                          <FormGroup className="mb-0">
                            <Label className={classes.fieldLabel} for="department">
                              {/*Chủ đề*/}
                              <FormattedMessage id="topic" tagName="data" />
                            </Label>
                            <select
                              type="select"
                              name="topicName"
                              id="styleInput"
                              value={this.state.codeTopic}
                              onChange={e => {
                                this.setState(
                                  {
                                    codeTopic: e.target.value
                                  }
                                )
                              }}
                            >
                              <option defaultChecked value="">{showMessage("all")}</option>
                              {optionNameTopicByPartAndTypeTopic}
                            </select>
                          </FormGroup>
                        </Col>
                          <Col sm={2} md={2} lg={2} xs={2}>
                            <FormGroup className="mb-0">
                              <Label className={classes.fieldLabel}></Label>
                              <div className="w-100">
                                <Button id="btnSubmit" type="submit" className={classes.buttonSearch} color="primary"
                                >
                                  {/*Tìm*/}
                                  <FormattedMessage id="search" tagName="data" />
                                </Button>
                              </div>

                            </FormGroup>
                          </Col>
                        </FormGroup>
                        <FormGroup className="form-group d-flex">
                          <Col sm={2} md={2} lg={2} xs={2}></Col>
                          <Col sm={2} md={2} lg={2} xs={2}>
                            <FormGroup className="mb-0">
                              <Label className={classes.fieldLabel} for="status">
                                {/*Trình độ*/}
                                <FormattedMessage id="level" tagName="data" />
                              </Label>
                              <select
                                type="select"
                                name="levelCode"
                                id="styleInput"
                                value={this.state.levelCode}
                                onChange={e => {

                                  this.setState(
                                    {
                                      levelCode: e.target.value
                                    }
                                  )
                                }}
                              >
                                <option defaultChecked value="">{showMessage("all")}</option>
                                {optionLevelCode}
                              </select>
                            </FormGroup>
                          </Col>
                          <Col sm={2} md={2} lg={2} xs={2}>
                            <FormGroup className="mb-0">
                              <div>
                                <Label className={classes.fieldLabel} for="status">
                                  {/*Trạng thái*/}
                                  <FormattedMessage id="status" tagName="data" />
                                </Label>
                                <select
                                  type="select"
                                  name="statusCategory"
                                  id="styleInput"
                                  value={this.state.statusCategory}
                                  onChange={e => {
                                    this.setState(
                                      {
                                        statusCategory: e.target.value
                                      }
                                    )
                                  }}
                                >
                                  <option defaultChecked value="2">{showMessage("all")}</option>
                                  <option value="1">
                                    {/*Áp dụng*/}
                                    {showMessage("being.applied")}
                                  </option>
                                  <option value="0">
                                    {/*Ngừng áp dụng*/}
                                    {showMessage("discontinued")}
                                  </option>
                                </select>
                              </div>
                            </FormGroup>
                          </Col>
                          <Col sm={2} md={2} lg={2} xs={2}>
                            <FormGroup className="mb-0">
                              <div className="w-100">
                                <Label className={classes.fieldLabel} for="status">
                                  {/*Tạo đề bài từ*/}
                                  <FormattedMessage id="create.category.from" tagName="data" />
                                </Label>
                                <Field
                                  id="createDateFromCategory"
                                  type="date"
                                  name="createDateFrom"
                                  className={`form-control ${errors.createDateFrom &&
                                  touched.createDateFrom &&
                                  "is-invalid"}`}
                                  validate={validateDateFrom(this,values)}

                                />
                                {errDate1 ? (
                                  <div style={{fontSize: '90%', marginTop: '4%'}} className={classes.error}>{errDate1}</div>
                                ) : null}

                                {/*<DatePicker*/}
                                  {/*type="date"*/}
                                  {/*name="createFrom"*/}
                                  {/*style={{height: '50px !important'}}*/}
                                  {/*className={`form-control ${errors.createDateFrom &&*/}
                                  {/*touched.createDateFrom &&*/}
                                  {/*"is-invalid"}`}*/}
                                  {/*// onChangeRaw={this.handleDateChangeRaw}*/}
                                  {/*isClearable*/}
                                  {/*selected={this.state.createFrom}*/}
                                  {/*onChange={date => this.setState({createFrom: date})}*/}
                                  {/*dateFormat="dd/MM/yyyy"*/}
                                  {/*placeholderText="dd/mm/yyyy"*/}
                                  {/*popperPlacement="right-end"*/}
                                  {/*onCalendarClose ={()=>{*/}
                                    {/*if(this.state.createFrom && this.state.createTo){*/}
                                      {/*if(this.state.createFrom > this.state.createTo){*/}
                                        {/*this.setState({*/}
                                          {/*messageFrom : "Ngày tạo đề bài từ phải nhỏ hơn Tạo đề bài đến"*/}
                                        {/*})*/}
                                      {/*} else {*/}
                                        {/*this.setState({*/}
                                          {/*messageFrom: ''*/}
                                        {/*})*/}
                                      {/*}*/}
                                    {/*}*/}
                                  {/*}}*/}
                                {/*/>*/}

                                {/*{errors.createDateFrom && touched.createDateFrom ? (*/}
                                  {/*<div className={classes.error}>{errors.createDateFrom}</div>*/}
                                {/*) : null}*/}
                                {/*{this.state.messageFrom ? (*/}
                                  {/*<div style={{fontSize: '90%', marginTop: '4%'}} className={classes.error}>*/}
                                    {/*{this.state.messageFrom}*/}
                                  {/*</div>*/}
                                {/*) : null}*/}
                              </div>
                            </FormGroup>
                          </Col><Col sm={2} md={2} lg={2} xs={2}>
                          <FormGroup className="mb-0">
                            <div className="w-100">
                              <Label className={classes.fieldLabel} for="status">
                                {/*Tạo đề tài đến*/}
                                <FormattedMessage id="create.category.to" tagName="data" />
                              </Label>
                              <Field
                                type="date"
                                id="createDateToCategory"
                                name="createDateTo"
                                minDate='1900-01-01'
                                className={`form-control ${errors.createDateTo &&
                                touched.createDateTo &&
                                "is-invalid"}`}
                                validate={validateDateTo(values)}

                              />
                              {errDate2 ? (
                                <div className={classes.error}>{errDate2}</div>
                              ) : null}

                              {/*<DatePicker*/}
                                {/*// ref="createDateTo"*/}
                                {/*type="date"*/}
                                {/*name="createTo"*/}
                                {/*style={{height: '38px'}}*/}
                                {/*className={`form-control ${errors.createDateTo &&*/}
                                {/*touched.createDateTo &&*/}
                                {/*"is-invalid"}`}*/}
                                {/*// onChangeRaw={this.handleDateChangeRaw}*/}
                                {/*isClearable*/}
                                {/*selected={this.state.createTo}*/}
                                {/*onChange={date => this.setState({createTo: date})}*/}
                                {/*dateFormat="dd/MM/yyyy"*/}
                                {/*placeholderText="dd/mm/yyyy"*/}
                                {/*popperPlacement="right-end"*/}
                                {/*onCalendarClose ={()=>{*/}
                                  {/*if(this.state.createFrom && this.state.createTo){*/}
                                    {/*if(this.state.createFrom > this.state.createTo){*/}
                                      {/*this.setState({*/}
                                        {/*messageTo : "Ngày Tạo đề bài đến phải lớn hơn ngày Tạo đề bài từ"*/}
                                      {/*})*/}
                                    {/*} else {*/}
                                      {/*this.setState({*/}
                                        {/*messageTo: ''*/}
                                      {/*})*/}
                                    {/*}*/}
                                  {/*}*/}
                                {/*}}*/}
                              {/*/>*/}

                              {/*{errors.createDateTo && touched.createDateTo ? (*/}
                                {/*<div className={classes.error}>{errors.createDateTo}</div>*/}
                              {/*) : null}*/}
                              {/*{this.state.messageTo ? (*/}
                                {/*<div style={{fontSize: '90%', marginTop: '4%'}} className={classes.error}>*/}
                                  {/*{this.state.messageTo}*/}
                                {/*</div>*/}
                              {/*) : null}*/}
                            </div>
                          </FormGroup>
                        </Col>
                        </FormGroup>
                      </Form>
                    )}
                  </Formik>
                </CardBody>
              </Collapse>
            </Card>
          </Col>
          <Col sm="12">
            <div style={{position: 'relative'}}>
              {isLoading && loadingComponent}

              <MUIDataTable
                columns={columns}
                data={newData}
                options={options}
              />
            </div>
          </Col>
        </Row>
      </>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    lstCategory: state.categoryReducer.lstCategory,
    isLoading: state.categoryReducer.isLoading,
    values: state.categoryReducer,
    dataFilter: state.categoryReducer.dataFilter,
    detailCategory: state.categoryReducer.detailCategory,
    lstPartByTopic: state.topicReducer.lstPartByTopic,
    lstTopicByTypeTopicAndPart: state.topicReducer.lstTopicByTypeTopicAndPart,

  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    categoryAction: bindActionCreators(categoryAction, dispatch),
    topicAction: bindActionCreators(topicAction, dispatch)
  }
}

export default compose(withStyles(styles()), connect(mapStateToProps, mapDispatchToProps))(GridCategory)
