import React from "react"
import {Button, Col, FormGroup, Input, Label,} from "reactstrap"

import {bindActionCreators, compose} from "redux";
import {connect} from "react-redux";
import {Field, Form} from "formik";
import {Switch} from "@material-ui/core";
import categoryAction from "../../../../../redux/actions/category-action";
import Spinner from "reactstrap/es/Spinner";
import Select from "react-select";

class FormDetail extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      levelCode: this.props.values.levelCode,
      status: this.props.values.status,
      typeCode: this.props.values.typeCode,
      part: this.props.values.part,
      parentId: this.props.values.parentId,
      isLoadingPart: false,
      isLoadingTopic: false,
      optionTopic: []
    }
  }

  componentDidMount() {
    const {categoryActions, detailCategory} = this.props;
    const {getListTypeExercise, getListTypeLevel, getListPartExercise, getListTopicByPart, getListTypeFileUpload} = categoryActions;
    getListTypeExercise();
    getListTypeLevel();
    if(detailCategory && detailCategory.typeCode && detailCategory.part){
      getListPartExercise(detailCategory.typeCode);
      getListTopicByPart(detailCategory.part);
    }
  }
  onChangeTypeCode = (e) => {
    const {categoryActions, updateTypeExercise, updatePart} = this.props;
    this.setState({
      typeCode: e.target.value
    })
    updateTypeExercise(e)
    const {getListPartExercise} = categoryActions;
    getListPartExercise((e.target.value));
    if (e.target.value == 1 || e.target.value == 2 || e.target.value == 5) {
      this.setState({isLoadingPart: true})
    } else {
      this.setState({
        isLoadingTopic: true,
        part: ""
      })
      updatePart(e)
    }
    setTimeout(() => {
      this.setState({
        isLoadingPart: false,
        isLoadingTopic: false
      })
    }, 1000)
  }

  onChangePart = (e) => {
    const {typeCode} = this.state;
    const {categoryActions, updatePart} = this.props;
    this.setState({
      part: e.target.value
    })
    updatePart(e)
    const {getListTopicByPart} = categoryActions;
    getListTopicByPart(e.target.value);
    this.setState({isLoadingTopic: true})
    setTimeout(() => {
      this.setState({
        isLoadingTopic: false,
      })
    }, 1000)
  }


  render() {
    const {
      errors, touched, handleChange, handleBlur, setFieldValue, isViewDetail, detailCategory,
      lstTypeExercise, lstTypeLevel, loadingViewDetail, lstTopicByPart, lstPartExercise, lstTopicTrans
    } = this.props;
    const {typeCode: code} = this.state;

    const optionTypeExercise = lstTypeExercise ? lstTypeExercise.map((type, index) => {
      if (type.paramValue == this.state.typeCode) {
        return <option selected value={type.paramValue} key={index}>{type.paramName}</option>
      } else {
        return <option value={type.paramValue} key={index}>{type.paramName}</option>
      }
    }) : null

    const optionTypeLevel = lstTypeLevel ? lstTypeLevel.map((level, index) => {
      if(level.paramCode == this.state.levelCode){
         return <option selected value={level.paramCode} key={index}>{level.paramName}</option>
        //return { value: level.paramCode, label:level.paramName }
      } else {
        return <option value={level.paramCode} key={index}>{level.paramName}</option>
        // return { value: level.paramCode, label:level.paramName }
      }
    }) : null

    const optionPartExercise = lstPartExercise !== undefined ? lstPartExercise.map((partCode, index) => {
      if (partCode.paramCode == this.state.part) {
        return <option selected value={partCode.paramCode} key={index}>{partCode.paramName}</option>
      } else {
        return <option value={partCode.paramCode} key={index}>{partCode.paramName}</option>
      }
    }) : null

    const optionTopicByPart = lstTopicByPart ? lstTopicByPart.map((topic, index) => {

      if (topic.topicId == this.state.parentId) {
        return <option selected value={topic.topicId} key={index}>{topic.topicName}</option>
      } else {
        return <option value={topic.topicId} key={index}>{topic.topicName}</option>
      }
    }) : null

    const loadingComponent = (
      <div style={{
        position: 'absolute',
        zIndex: 110,
        top: 0,
        left: 0,
        width: '100%',
        height: '100%',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        background: 'rgba(255,255,255,0.8)'
      }}>
        <Spinner color="primary" className="reload-spinner"/>
      </div>
    );

    return (
      <div>
        {loadingViewDetail && loadingComponent}
        <Form className="mt-2">
          <FormGroup className="form-group d-flex">
            <Label for="required" sm={2}>Tên đề bài(<span style={{color:"red"}}>*</span>) </Label>
            <Col sm={9}>
              <div className="w-100">
                <div>
                  <Field
                    disabled={isViewDetail}
                    type="text"
                    name="nameCategory"
                    className={`form-control ${errors.nameCategory &&
                    touched.nameCategory &&
                    "is-invalid "}`}
                    defaultValue={detailCategory && detailCategory.categoryId ? detailCategory.nameCategory : ""}
                  />
                  {errors.nameCategory && touched.nameCategory ? (
                    <div className="invalid-feedback mt-25">{errors.nameCategory}</div>
                  ) : null}
                </div>


              </div>
            </Col>
          </FormGroup>
          <FormGroup className="form-group d-flex">
            <Col xs={6} className="d-flex" style={{paddingLeft: "0px", paddingRight: "0px"}}>
              <Label for="required" sm={4}>Trình độ(<span style={{color:"red"}}>*</span>) </Label>
              <Col sm={8}>
                <div className="w-100">
                  <div>
                    <Input
                      disabled={isViewDetail}
                      type="select" name="levelCode" id="levelCode" onChange={handleChange}
                      onBlur={handleBlur}
                      className={`form-control ${errors.levelCode && touched.levelCode && "is-invalid "}`}>
                      <option value={0}>Chọn trình độ</option>
                      {optionTypeLevel}
                    </Input>
                    {/*<Select*/}
                      {/*isDisabled={isViewDetail}*/}
                      {/*classNamePrefix="select"*/}
                      {/*defaultValue={{value: detailCategory.levelCode, label: detailCategory}}*/}
                      {/*name="levelCode"*/}
                      {/*options={optionTypeLevel}*/}
                      {/*className={`React ${errors.levelCode && touched.levelCode && "is-invalid "}`}*/}
                    {/*/>*/}
                    {errors.levelCode && touched.levelCode ? (
                      <div className="invalid-feedback mt-25" style={{display: 'block'}}>{errors.levelCode}</div>

                    ) : null}
                  </div>
                </div>
              </Col>
            </Col>
            <Col xs={6} className="d-flex">
              <Label for="required" sm={4}>Trạng thái(<span style={{color:"red"}}>*</span>) </Label>
              <Col sm={8}>
                <div className="w-100">
                  <div>
                    <Switch
                      // disabled={isViewDetail}
                      name="status"
                      color="primary"
                      defaultChecked={this.state.status}
                      // checked={this.state.status}
                      onChange={(event, checked) => {
                        setFieldValue("status", checked ? 1 : 0);
                      }}
                    />
                    {errors.status && touched.status ? (
                      <div className="invalid-feedback mt-25">{errors.status}</div>
                    ) : null}
                  </div>

                </div>
              </Col>
            </Col>
          </FormGroup>
          <FormGroup className="form-group d-flex">
            <Label for="required" sm={2}>Loại đề bài(<span style={{color:"red"}}>*</span>) </Label>
            <Col sm={9}>
              <div className="w-100">
                <div>
                  <Input
                    disabled={isViewDetail}
                    type="select" name="typeCode" id="typeCode"
                    onChange={(e) => this.onChangeTypeCode(e)}
                    className={`form-control ${errors.typeCode && touched.typeCode && "is-invalid "}`}>
                    <option value={0}>Chọn loại đề bài</option>
                    {optionTypeExercise}
                  </Input>
                  {errors.typeCode && touched.typeCode ? (
                    <div className="invalid-feedback mt-25" style={{display: 'block'}}>{errors.typeCode}</div>
                  ) : null}
                </div>

              </div>
            </Col>
          </FormGroup>

          {
            parseInt(code) === 1 || parseInt(code) === 2 || parseInt(code) === 5  ?
              (
                <FormGroup className="form-group d-flex">
                  <Label for="required" sm={2}>Phần </Label>
                  <Col sm={9}>
                    <div className="w-100">
                      <div>
                        <Input type="select" name="part" id="part"
                               onChange={(e) => this.onChangePart(e)}
                               disabled={isViewDetail}
                        >
                          <option value={0}>Chọn phần</option>
                          {optionPartExercise}
                        </Input>
                        {errors.part && touched.part ? (
                          <div className="invalid-feedback mt-25">{errors.part}</div>
                        ) : null}
                      </div>

                    </div>
                  </Col>
                  {
                    this.state.isLoadingPart && <Spinner color="primary" className="mr-1"/>
                  }
                </FormGroup>
              ) : null
          }
          {
            parseInt(code) > 0 && parseInt(code) !== 3 && parseInt(code) !== 4 ?
              (
                <FormGroup className="form-group d-flex">
                  <Label for="required" sm={2}>Chủ đề </Label>
                  <Col sm={9}>
                    <div className="w-100">
                      <div>
                        <Input type="select" name="parentId" id="parentId" disabled={isViewDetail}
                               onChange={handleChange}
                        >
                          <option value={0}>Chọn chủ đề</option>
                          {
                             optionTopicByPart
                          }
                        </Input>
                        {errors.parentId && touched.parentId ? (
                          <div className="invalid-feedback mt-25">{errors.parentId}</div>
                        ) : null}
                      </div>
                    </div>
                  </Col>
                  {
                    this.state.isLoadingTopic && <Spinner color="primary" className="mr-1"/>
                  }
                </FormGroup>
              ) : null
          }
        </Form>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    isViewDetail: state.categoryReducer.isViewDetail,
    detailCategory: state.categoryReducer.detailCategory,
    lstTypeExercise: state.categoryReducer.lstTypeExercise,
    lstPartExercise: state.categoryReducer.lstPartExercise,
    lstTypeLevel: state.categoryReducer.lstTypeLevel,
    lstTopicTrans: state.categoryReducer.lstTopicTrans,
    lstTopicByPart: state.categoryReducer.lstTopicByPart,
    loadingPart: state.categoryReducer.loadingPart,
    loadingTopic: state.categoryReducer.loadingTopic,
    loadingViewDetail: state.categoryReducer.loadingViewDetail
  }
}

const mapDispatchToProps = dispatch => ({
  categoryActions: bindActionCreators(categoryAction, dispatch)
});

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
);

export default compose(
  withConnect
)(FormDetail);
