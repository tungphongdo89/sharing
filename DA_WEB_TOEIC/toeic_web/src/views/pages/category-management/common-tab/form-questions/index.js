import React, {Component} from 'react';
import {
  Card,
  CardBody,
  FormGroup,
  Button,
  Row,
  Col, Badge
} from "reactstrap"
import {Formik, Field, Form, withFormik, FieldArray} from "formik"
import formSchema from "./validate";
import './style.css';
import * as Icon from "react-feather";
import {connect} from "react-redux";
import {compose} from "redux";
import Question from "./question/question";


class FormQuestions extends Component {

  constructor(props) {
    super(props);
    this.state = {}
  }


  render() {
    const {values, isViewDetail} = this.props;
    if (values.listQuestion.length <= 0 && isViewDetail) {
      return (
        <div>
          <Card>
            <CardBody>
              <h1>
                Đề bài này chưa có câu hỏi nào
              </h1>
            </CardBody>
          </Card>
        </div>
      )
    } else {
      return (
        <div>
          <Card>
            <CardBody>
              <Form>
                <FormGroup>
                  <Row>
                    <Col xs="3">
                      Tổng điểm tạm tính
                    </Col>
                    <Col xs="3">
                      <Field disabled name="totalScore" id="totalScore"
                             className="form-control"/>
                    </Col>
                    <Col xs="3">
                      Điểm mục tiêu
                    </Col>
                    <Col xs="3">
                      <Field disabled={this.props.isViewDetail} name="targetScore" id="targetScore"
                             className={`form-control ${this.props.errors.targetScore &&
                             this.props.touched.targetScore &&
                             "is-invalid"}`}
                      />
                      {this.props.errors.targetScore && this.props.touched.targetScore ? (
                        <div className="invalid-feedback mt-25">{this.props.errors.targetScore}</div>
                      ) : null}
                    </Col>
                  </Row>
                </FormGroup>
                <Question errors={this.props.errors} touched={this.props.touched} values={this.props.values}/>
              </Form>
            </CardBody>
          </Card>
        </div>
      );
    }
  }
}


const mapDispatchToProps = dispatch => {
  return {}
}

const mapStateToProps = (state, ownProps) => {
  return {
    isViewDetail: state.categoryReducer.isViewDetail
  }
}
const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
)

export default compose(withConnect)(FormQuestions);
