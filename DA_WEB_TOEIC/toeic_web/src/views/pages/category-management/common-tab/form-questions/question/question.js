import React, {Component} from 'react';
import {Badge, Button, Col, FormGroup, Input, Row} from "reactstrap";
import './style.css';
import * as Icon from "react-feather";
import {Field, FieldArray, getIn} from "formik";
import {connect} from "react-redux";
import {compose} from "redux";
import Button2 from '@material-ui/core/Button';
import HighlightOffIcon from "@material-ui/icons/HighlightOff";
import moment from "moment";
import {TimePicker} from 'antd';
import 'antd/dist/antd.css';
import {CODE_PART1_LF, CODE_PART2_LF, CODE_PART3_LF, CODE_PART4_LF,
  CODE_PART1, CODE_PART2, CODE_PART3, CODE_PART4, CODE_PART5, CODE_PART6, CODE_PART7, CODE_PART8} from "../../../../../../commons/constants/CONSTANTS";


class Question extends Component {
  constructor(props) {
    super(props);

    this.fileInput = React.createRef();
    this.state = {
      message: ''
    }
  }

  RCTimePicker = props => {
    function onTimeChange(time, timeString) {
      props.form.setFieldValue(props.field.name, timeString);
    }

    if (props.field.value === null || props.field.value.trim() === "") {
      return (
        <TimePicker
          disabled={this.props.isViewDetail}
          className="form-control"
          onChange={onTimeChange}
          defaultOpenValue={moment("00:00:00", 'HH:mm:ss')}
        />
      );
    } else {
      return (
        <TimePicker
          disabled={this.props.isViewDetail}
          className="form-control"
          onChange={onTimeChange}
          defaultValue={moment(props.field.value
            ? props.field.value.match("(^[0-1]\\d|[2][0-3])(:[0-5]\\d){2}$")
              ? props.field.value : "00:00:00" : "00:00:00", 'HH:mm:ss')}
        />
      );
    }
  };

  correctAnswerToChoose = (props) => {
    let indexQuestion = props.field.name.split("[")[1].substring(0, 1);
    let indexCorrectAnswer = props.field.name.split("[")[2].substring(0, 1);
    let lstAnswers = props.form.values.listQuestion[indexQuestion].listAnswerToChoose;
    const onSelectAnswerChange = (e) => {
      props.form.setFieldTouched(props.field.name,true)
      props.form.setFieldValue(props.field.name,e.target.value)
    }
    return (
      <Input value={props.field.value} onChange={onSelectAnswerChange} type="select" disabled={this.props.isViewDetail}
             name={props.field.name}>
        <option value="">Chọn đáp án đúng</option>
        {
          lstAnswers.map((item, index) => {
            return item.answer ?
              <option key={index} value={item.answer}>{item.answer}</option> : null
          })
        }
      </Input>
    );
  };

  SelectTypePicker = props => {

    const onSelectChange = (e) => {
      let lst = props.field.name.split("]");
      let test = lst[1];
      if (test.match(/([1])/)) {
        let fieldData = lst[0] + "]fileUpload1";
        let fieldPath = lst[0] + "]pathFile1";
        props.form.setFieldError(fieldData, undefined);
        props.form.setFieldTouched(fieldData, undefined);
        props.form.setFieldValue(fieldData, null);
        props.form.setFieldError(fieldPath, undefined);
        props.form.setFieldTouched(fieldPath, undefined);
        props.form.setFieldValue(fieldPath, null);

      }
      if (test.match(/([2])/)) {
        let fieldData = lst[0] + "]fileUpload2";
        let fieldPath = lst[0] + "]pathFile2";
        props.form.setFieldError(fieldData, undefined);
        props.form.setFieldTouched(fieldData, undefined);
        props.form.setFieldValue(fieldData, null);
        props.form.setFieldError(fieldPath, undefined);
        props.form.setFieldTouched(fieldPath, undefined);
        props.form.setFieldValue(fieldPath, null);
      }
      props.form.setFieldValue(props.field.name, e.target.value);
    }
    return (
      <Input value={props.field.value} onChange={onSelectChange} type="select" disabled={this.props.isViewDetail}
             name={props.field.name}>
        {this.props.lstTypeFile.map((type, index) => (
          <option key={index} value={type.paramName}>{type.paramName}</option>
        ))}
      </Input>
    );
  };

  FilePicker = props => {
    debugger;
    let lst = props.field.name.split("]");
    const onFileChange = (e) => {
      props.form.setFieldError(props.field.name, undefined);
      props.form.setFieldTouched(props.field.name, undefined);
      e.preventDefault();
      let reader = new FileReader();
      let file = e.target.files[0];
      if (file) {
        reader.onloadend = () => {
        };
        props.form.setFieldValue(props.field.name, file);
        if(this.fileInput.current){
          this.fileInput.current.value = null
        }
      }
    }
    const onDeleteFile = () => {
      props.form.setFieldValue(props.field.name, null);
    }

    const onFileClick = (value) =>{
    }

    return (
      <div className="w-100">
        <input
          onClick={onFileClick}
          disabled={this.props.isViewDetail}
          name={props.field.name}
          style={{display: "none"}}
          id={`contained-button-file ${props.field.name}`}
          type="file"
          onChange={onFileChange}
          ref={this.fileInput}
        />
        <label htmlFor={`contained-button-file ${props.field.name}`}>
          <Button2 id={props.field.name} disabled={this.props.isViewDetail} variant="contained" color="primary"
                   component="span">
            <Icon.Upload style={{marginRight: '5px'}}/> Upload
          </Button2>
        </label>
        <div style={{display: 'inline-block'}}>
          {props.errorMessage && props.touched
            ? props.errorMessage
            : props.field.value
              ? props.field.value.name
              : null
          }
          {
            props.field.value ?
              <HighlightOffIcon style={{color: 'red'}}
                                onClick={onDeleteFile}/> : ''
          }
        </div>
      </div>
    );
  };


  handleTotalScore = props => {

    const scoreChange = e => {
      let oldTotal = parseFloat(props.form.values.totalScore)
        ? parseFloat(props.form.values.totalScore) : 0;
      let oldScore = parseFloat(props.field.value)
        ? parseFloat(props.field.value) : 0;
      let newScore = parseFloat(e.target.value)
        ? parseFloat(e.target.value) : 0
      let total = oldTotal - oldScore + newScore;
      props.form.setFieldValue(props.field.name, e.target.value)
      props.form.setFieldTouched(props.field.name, true)
      if (total) {
        props.form.setFieldValue("totalScore", total)
      }
    }

    return (
      <div>
        <input disabled={this.props.isViewDetail} onChange={scoreChange} name={props.field.name}
               value={props.field.value} className="form-control"/>
        {props.errorMessage
          ? <div className="invalid-feedback mt-25">{props.errorMessage}</div>
          : null
        }
      </div>
    );
  }

  SelectTypeQuestionPicker = props => {

    const onSelectChange = (e) => {
      let lst = props.field.name.split("]");
      if (e.target.value == 2) {
        props.form.setFieldValue(lst[0] + "]listAnswerToChoose", [{answer: "null", startTime: null}]);
      } else {
        props.form.setFieldValue(lst[0] + "]listAnswerToChoose", [{answer: "", startTime: null}])
      }
      props.form.setFieldValue(props.field.name, e.target.value)
    }

    return (
      <Input defaultValue={props.field.value} onChange={onSelectChange} type="select" disabled={this.props.isViewDetail}
             name={props.field.name}>
        <option value={1}>Câu hỏi trắc nghiệm</option>
        <option value={2}>Câu hỏi trả lời ngắn</option>
      </Input>
    );
  }

  render() {
    const ErrorMessage = ({name}) => (
      <Field name={name}>
        {({field, form, meta}) => {
          const error = getIn(form.errors, name);
          const touch = getIn(form.touched, name);
          return touch && error ? (
            <div className="invalid-feedback" style={{display: "contents" , fontSize : "15px" , fontWeight : "bold"}}>{error}</div>) : null;
        }}
      </Field>
    );
    const {values, errors, touched, setFieldValue, isViewDetail} = this.props;
    return (
      <FieldArray
        name="listQuestion"
        render={arrayHelpers => (
          <div>
            {
              this.props.values.listQuestion.map((listQuestion, index) => (
                  <div key={index}>
                    <Row>
                      <Col xs="12">
                        <Row>
                          <Col xs="2" className="IndexQuestion">
                            <Badge color="light-primary" className="block badge-lg">
                              <span>Câu {index + 1}</span>
                            </Badge>
                          </Col>
                          <Col style={{display: this.props.isViewDetail ? "none" : null}} xs="2" className="buttonIcon">
                            {
                              this.props.values.listQuestion.length > 1 ?
                                <Button onClick={() => arrayHelpers.remove(index)}
                                        color="danger" className="btn-icon rounded-circle" style={{marginLeft: '8px'}}>
                                  <Icon.Minus size={10}/>
                                </Button>
                                : <Button color="danger" onClick={() => {
                                }} className="btn-icon rounded-circle" style={{marginLeft: '8px'}}>
                                  <Icon.Minus size={10}/>
                                </Button>
                            }
                            <Button onClick={() => arrayHelpers.insert(index + 1, {
                              typeQuestion: 1,
                              score: 0,
                              name: "",
                              typeFile1: "LINK",
                              typeFile2: "LINK",
                              pathFile1: "",
                              pathFile2: "",
                              fileUpload1: null,
                              fileUpload2: null,
                              transcript: "",
                              listCorrectAnswers: [""],
                              description: "",
                              listAnswerToChoose: [{answer: "", startTime: ""}]
                            })}
                                    color="info" className="btn-icon rounded-circle" style={{marginLeft: '8px'}}>
                              <Icon.Plus size={10}/>
                            </Button>
                          </Col>
                        </Row>
                        <Row>
                          <div className="QuestionComponent">
                            <div className="QuestionContent">
                              <FormGroup>
                                <Row>
                                  <Col xs="2">
                                    Loại câu hỏi(<span style={{color: "red"}}>*</span>)
                                  </Col>
                                  <Col xs="4" className="text-left">
                                    <Field disabled={this.props.isViewDetail}
                                           name={`listQuestion[${index}]typeQuestion`}
                                           as="select" className="form-control"
                                           component={this.SelectTypeQuestionPicker}
                                    >
                                      <option value={1}>Câu hỏi trắc nghiệm</option>
                                      <option value={2}>Câu hỏi trả lời ngắn</option>
                                    </Field>
                                  </Col>
                                  <Col xs="2">
                                    Điểm câu hỏi(<span style={{color: "red"}}>*</span>)
                                  </Col>
                                  <Col xs="4" className="text-left">
                                    <Field disabled={this.props.isViewDetail} name={`listQuestion[${index}]score`}
                                           className="form-control"
                                           component={this.handleTotalScore}
                                           setFieldValue={this.props.setFieldValue}
                                           errorMessage={errors[`listQuestion[${index}]score`] ? errors[`listQuestion[${index}]score`] : undefined}
                                           touched={touched[`listQuestion[${index}]score`]}
                                           style={{display: "flex"}}
                                           onBlur={this.props.handleBlur}
                                    />
                                    <ErrorMessage name={`listQuestion[${index}]score`}/>
                                  </Col>
                                </Row>
                              </FormGroup>
                              <FormGroup>
                                <Row>
                                  <Col xs="2">
                                    Nội dung
                                  </Col>
                                  <Col xs="10">
                                    <Field disabled={this.props.isViewDetail} name={`listQuestion[${index}]name`}
                                           as="textarea"
                                           className="form-control"
                                    />
                                    <ErrorMessage name={`listQuestion[${index}]name`}/>
                                  </Col>
                                </Row>
                              </FormGroup>
                              {
                                ["PART1", "PART2",CODE_PART1_LF,CODE_PART2_LF, CODE_PART3_LF, CODE_PART4_LF].includes(this.props.values.part) ?
                                  (<FormGroup>
                                    <Row>
                                      <Col xs="2">
                                        Loại dữ liệu 1
                                      </Col>
                                      <Col xs="3">
                                        <Field disabled={this.props.isViewDetail}
                                               name={`listQuestion[${index}]typeFile1`}
                                               className="form-control" component={this.SelectTypePicker}
                                        />
                                      </Col>
                                      <Col xs="7">
                                        {
                                          ["LINK", "TEXT"].includes(listQuestion.typeFile1) ?
                                            <Field disabled={this.props.isViewDetail}
                                                   name={`listQuestion[${index}]pathFile1`}
                                                   as="textarea"
                                                   className="form-control"
                                            />
                                            :
                                            <div>
                                              <Field
                                                disabled={this.props.isViewDetail}
                                                name={`listQuestion[${index}]fileUpload1`}
                                                setFieldValue={this.props.setFieldValue}
                                                errorMessage={errors[`listQuestion[${index}]fileUpload1`] ? errors[`listQuestion[${index}]fileUpload1`] : undefined}
                                                touched={touched[`listQuestion[${index}]fileUpload1`]}
                                                style={{display: "flex"}}
                                                onBlur={this.props.handleBlur}
                                                component={this.FilePicker}
                                              />
                                              {this.props.categoryId
                                              && listQuestion.fileUpload1 == null ? this.props.categoryTemp.listQuestion[index] ?
                                                this.props.categoryTemp.listQuestion[index].typeFile1 == "FILE" ?
                                                  this.props.categoryTemp.listQuestion[index].pathFile1 : null : null : null
                                              }
                                            </div>
                                        }
                                        <ErrorMessage name={`listQuestion[${index}]fileUpload1`}/>
                                      </Col>
                                    </Row>
                                  </FormGroup>) : null
                              }
                              {
                                ["PART1", CODE_PART1_LF].includes(this.props.values.part) ?
                                  (<FormGroup>
                                    <Row>
                                      <Col xs="2">
                                        Loại dữ liệu 2
                                      </Col>
                                      <Col xs="3">
                                        <Field disabled={this.props.isViewDetail}
                                               name={`listQuestion[${index}]typeFile2`}
                                               className="form-control" component={this.SelectTypePicker}
                                        />
                                      </Col>
                                      <Col xs="7">
                                        {
                                          ["LINK", "TEXT"].includes(listQuestion.typeFile2) ?
                                            <Field disabled={this.props.isViewDetail}
                                                   name={`listQuestion[${index}]pathFile2`}
                                                   as="textarea"
                                                   className="form-control"
                                            />
                                            :
                                            <div>
                                              <Field
                                                disabled={this.props.isViewDetail}
                                                name={`listQuestion[${index}]fileUpload2`}
                                                setFieldValue={this.props.setFieldValue}
                                                errorMessage={errors[`listQuestion[${index}]fileUpload2`] ? errors[`listQuestion[${index}]fileUpload2`] : undefined}
                                                touched={touched[`listQuestion[${index}]fileUpload2`]}
                                                style={{display: "flex"}}
                                                onBlur={this.props.handleBlur}
                                                component={this.FilePicker}
                                              />
                                              {this.props.categoryId
                                              && listQuestion.fileUpload2 == null ? this.props.categoryTemp.listQuestion[index] ?
                                                this.props.categoryTemp.listQuestion[index].typeFile2 == "FILE" ?
                                                  this.props.categoryTemp.listQuestion[index].pathFile2 : null : null : null
                                              }
                                            </div>
                                        }
                                        <ErrorMessage name={`listQuestion[${index}]fileUpload2`}/>
                                      </Col>
                                    </Row>
                                  </FormGroup>) : null
                              }
                              {
                                ["PART1", "PART2", "PART3", "PART4", "PART8", CODE_PART1_LF,CODE_PART2_LF, CODE_PART3_LF, CODE_PART4_LF].includes(this.props.values.part) ?
                                  (<FormGroup>
                                    <Row>
                                      <Col xs="2">
                                        Transcript
                                      </Col>
                                      <Col xs="10">
                                        <Field disabled={this.props.isViewDetail}
                                               name={`listQuestion[${index}]transcript`}
                                               as="textarea" className="form-control"
                                        />
                                      </Col>
                                    </Row>
                                  </FormGroup>) : null
                              }
                              {
                                ["PART1", "PART2", "PART3", "PART4", "PART5", "PART6", "PART7","PART8",
                                CODE_PART1_LF,CODE_PART2_LF,CODE_PART3_LF,CODE_PART4_LF].includes(this.props.values.part)
                                && listQuestion.typeQuestion == 1 ?
                                  (<FormGroup>
                                    <FieldArray
                                      name={`listQuestion[${index}]listAnswerToChoose`}
                                      render={arrayHelpers => (
                                        <div>
                                          {
                                            listQuestion.listAnswerToChoose.map((answers, index1) => (
                                              <div key={index1}>
                                                <Row>
                                                  <Col xs="2">
                                                    Phương
                                                    án {listQuestion.listAnswerToChoose.length > 1 ? (index1 + 1) : ""}(<span
                                                    style={{color: "red"}}>*</span>)
                                                  </Col>
                                                  <Col xs="3">
                                                    <Field disabled={this.props.isViewDetail}
                                                           name={`listQuestion[${index}]listAnswerToChoose[${index1}]answer`}
                                                           className="form-control"
                                                    />
                                                    <ErrorMessage
                                                      name={`listQuestion[${index}]listAnswerToChoose[${index1}]answer`}/>
                                                  </Col>
                                                  <Col xs="2">
                                                    Bắt đầu lúc
                                                  </Col>
                                                  <Col xs="3">
                                                    <Field disabled={this.props.isViewDetail}
                                                           name={`listQuestion[${index}]listAnswerToChoose[${index1}]startTime`}
                                                           className="form-control" component={this.RCTimePicker}
                                                    />
                                                  </Col>
                                                  <Col style={{display: this.props.isViewDetail ? "none" : null}}
                                                       xs="2">
                                                    {
                                                      listQuestion.listAnswerToChoose.length > 1
                                                        ?
                                                        <Button onClick={() => arrayHelpers.remove(index1)}
                                                                color="danger"
                                                                className="btn-icon rounded-circle"
                                                                style={{marginLeft: '8px'}}>
                                                          <Icon.MinusCircle size={10}/>
                                                        </Button>
                                                        :
                                                        <Button onClick={() => {
                                                        }} color="danger" className="btn-icon rounded-circle"
                                                                style={{marginLeft: '8px'}}>
                                                          <Icon.MinusCircle size={10}/>
                                                        </Button>

                                                    }
                                                    <Button onClick={() => arrayHelpers.insert(index1 + 1, {
                                                      answer: "", startTime: ""
                                                    })} color="info" className="btn-icon rounded-circle"
                                                            style={{marginLeft: '8px'}}>
                                                      <Icon.PlusCircle size={10}/>
                                                    </Button>
                                                  </Col>
                                                </Row><br/>
                                              </div>
                                            ))
                                          }
                                        </div>
                                      )}
                                    />
                                  </FormGroup>) : null
                              }
                              {
                                ["PART1", "PART2", "PART3", "PART4", "PART5", "PART6", "PART7","PART8", "PART9",
                                CODE_PART1_LF, CODE_PART2_LF,CODE_PART3_LF,CODE_PART4_LF].includes(this.props.values.part)
                                || this.props.values.typeCode == 3 || this.props.values.typeCode == 4 ?
                                  (<FormGroup>
                                    <FieldArray
                                      name={`listQuestion[${index}]listCorrectAnswers`}
                                      render={arrayHelpers => (
                                        <div>
                                          {
                                            listQuestion.listCorrectAnswers.map((correctAnswer, index2) => (
                                              <div key={index2}>
                                                <Row>
                                                  <Col xs="2">
                                                    Đáp
                                                    án {listQuestion.listCorrectAnswers.length > 1 ? index2 + 1 : ""}(<span
                                                    style={{color: "red"}}>*</span>)
                                                  </Col>
                                                  <Col xs="8">
                                                    {
                                                      ["PART1", "PART2", "PART3", "PART4", "PART5", "PART6", "PART7","PART8",
                                                      CODE_PART1_LF,CODE_PART2_LF,CODE_PART3_LF,CODE_PART4_LF].includes(this.props.values.part)
                                                      && listQuestion.typeQuestion == 1 ?
                                                      <Field disabled={this.props.isViewDetail}
                                                             name={`listQuestion[${index}]listCorrectAnswers[${index2}]`}
                                                             className="form-control"
                                                             setFieldValue={this.props.setFieldValue}
                                                             errorMessage={errors[`listQuestion[${index}]listCorrectAnswers[${index2}]`] ? errors[`listQuestion[${index}]listCorrectAnswers[${index2}]`] : undefined}
                                                             touched={touched[`listQuestion[${index}]listCorrectAnswers[${index2}]`]}
                                                             style={{display: "flex"}}
                                                             onBlur={this.props.handleBlur}
                                                             component={this.correctAnswerToChoose}
                                                      />:
                                                        <Field disabled={this.props.isViewDetail}
                                                               name={`listQuestion[${index}]listCorrectAnswers[${index2}]`}
                                                               className="form-control"
                                                        />
                                                    }
                                                    <ErrorMessage
                                                      name={`listQuestion[${index}]listCorrectAnswers[${index2}]`}/>
                                                  </Col>
                                                  <Col style={{
                                                    display: this.props.isViewDetail
                                                    || (this.props.values.typeCode == 2
                                                      &&
                                                      ["PART1", "PART2", "PART3", "PART5", "PART6", "PART7","PART8", "PART9",
                                                      CODE_PART1_LF,CODE_PART2_LF,CODE_PART3_LF].includes(this.props.values.part))
                                                    || this.props.values.typeCode == 1
                                                      ? "none" : "null"
                                                  }}
                                                       xs="2">
                                                    {
                                                      listQuestion.listCorrectAnswers.length > 1
                                                        ?
                                                        <Button onClick={() => arrayHelpers.remove(index2)}
                                                                color="danger"
                                                                className="btn-icon rounded-circle"
                                                                style={{marginLeft: '8px'}}>
                                                          <Icon.MinusCircle size={10}/>
                                                        </Button>
                                                        :
                                                        <Button onClick={() => {
                                                        }} color="danger" className="btn-icon rounded-circle"
                                                                style={{marginLeft: '8px'}}>
                                                          <Icon.MinusCircle size={10}/>
                                                        </Button>
                                                    }
                                                    <Button onClick={() => arrayHelpers.insert(index2 + 1, "")}
                                                            color="info"
                                                            className="btn-icon rounded-circle"
                                                            style={{marginLeft: '8px'}}>
                                                      <Icon.PlusCircle size={10}/>
                                                    </Button>
                                                  </Col>
                                                </Row><br/>
                                              </div>
                                            ))
                                          }
                                        </div>
                                      )}
                                    />
                                  </FormGroup>) : null
                              }
                              {
                                ["PART1", "PART2", "PART3", "PART4", "PART5", "PART6", "PART7","PART8",
                                CODE_PART1_LF,CODE_PART2_LF,CODE_PART3_LF,CODE_PART4_LF].includes(this.props.values.part) ?
                                  (<FormGroup>
                                    <Row>
                                      <Col xs="2">
                                        Giải thích
                                      </Col>
                                      <Col xs="10">
                                        <Field disabled={this.props.isViewDetail}
                                               name={`listQuestion[${index}]description`}
                                               as="textarea" className="form-control"
                                        />
                                      </Col>
                                    </Row>
                                  </FormGroup>) : null
                              }
                              {/*<Button onClick={() => {*/}
                                {/*console.log("errors:", this.props.errors);*/}
                                {/*console.log("touched:", this.props.touched);*/}
                                {/*console.log("listQuestion: ", this.props.values.listQuestion);*/}
                                {/*console.log("category: ", this.props.values);*/}
                                {/*console.log("categoryId: ", this.props.categoryId);*/}
                                {/*// console.log("ref:", this.fileInput.current.value)*/}
                              {/*}}>*/}
                                {/*test Error*/}
                              {/*</Button>*/}
                            </div>
                          </div>
                        </Row>
                      </Col>
                    </Row>
                  </div>
                )
              )}
          </div>
        )}
      />
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {}
}

const mapStateToProps = (state, ownProps) => {
  return {
    part: state.categoryReducer.part,
    isViewDetail: state.categoryReducer.isViewDetail,
    categoryId: state.categoryReducer.detailCategory.categoryId,
    categoryTemp: state.categoryReducer.detailCategoryTemp,
    lstTypeFile: state.categoryReducer.lstTypeFile
  }
}
const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
)

export default compose(withConnect)(Question);
