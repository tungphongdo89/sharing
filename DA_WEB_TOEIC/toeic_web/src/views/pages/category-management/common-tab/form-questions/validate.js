import * as Yup from "yup";

const formSchema = Yup.object().shape({
  listQuestion: Yup.array().of(
    Yup.object().shape({
      score: Yup.string().matches(/^[0-9]+$/,"Điểm phải là dạng số"),
      content: Yup.string().required("Câu hỏi phải có nội dung"),
      // typeData1: 1,
      // typeData2: 3,
      // data1: null,
      // data2: null,
      // transcript: "test",
      // rightAnswer: [],
      // description: "123123",
      // answersToChoose:[],
      // timeStart:[]
    })
  ).required("Đề bài của bạn phải có ít nhất 1 câu hỏi!"),
  targetScore: Yup.string().matches(/^[0-9]+$/,"Điểm mục tiêu phải là dạng số")
})

export default formSchema;
