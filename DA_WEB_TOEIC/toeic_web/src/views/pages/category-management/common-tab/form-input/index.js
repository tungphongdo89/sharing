import React from "react"
import {
  Col,
  FormGroup,
  Label,
  Input
} from "reactstrap"
import { bindActionCreators, compose } from "redux";
import { connect } from "react-redux";
import { Field, Form, getIn } from "formik";
import * as Icon from "react-bootstrap-icons"
import styles from "./../../styles"
import { withStyles } from "@material-ui/core"
import HighlightOffIcon from '@material-ui/icons/HighlightOff';
import categoryAction from "../../../../../redux/actions/category-action";
import Button2 from '@material-ui/core/Button';


class FormInput extends React.Component {

  constructor(props) {
    super(props);

    this.fileInput = React.createRef();
    this.state = {
      message: ''
    }
  }

  SelectTypeInput1 = props => {
    const { lstTypeFile, detailCategory } = this.props;
    const optionTypeFile1 = lstTypeFile ? lstTypeFile.map((type, index) => {
      if (type.paramName == props.form.values.typeFile1) {
        return <option selected value={type.paramName} key={index}>{type.paramName}</option>
      } else {
        return <option value={type.paramName} key={index}>{type.paramName}</option>
      }
    }) : null



    const onSelectChange = (e) => {
      let test = props.field.name;
      props.form.setFieldError("fileUpload1", undefined);
      props.form.setFieldTouched("fileUpload1", undefined);
      props.form.setFieldValue("fileUpload1", null);
      props.form.setFieldError("pathFile1", undefined);
      props.form.setFieldTouched("pathFile1", undefined);
      props.form.setFieldValue("pathFile1", null);
      props.form.setFieldValue(props.field.name, e.target.value);
    }

    return (
      <Input onChange={onSelectChange} type="select" disabled={this.props.isViewDetail} name={props.field.name}
      //     defaultValue={detailCategory && detailCategory.categoryId ? detailCategory.typeFile1 : ""}
      >
        <option value={0}>Chọn loại dữ liệu</option>
        {optionTypeFile1}
      </Input>
    );
  };

  SelectTypeInput2 = props => {
    const { lstTypeFile, detailCategory } = this.props;
    const optionTypeFile2 = lstTypeFile ? lstTypeFile.map((type, index) => {
      if (type.paramName == props.form.values.typeFile2) {
        return <option selected key={index} value={type.paramName}>{type.paramName}</option>
      } else {
        return <option key={index} value={type.paramName}>{type.paramName}</option>
      }
    }) : null

    const onSelectChange = (e) => {
      let test = props.field.name;
      props.form.setFieldError("fileUpload2", undefined);
      props.form.setFieldTouched("fileUpload2", undefined);
      props.form.setFieldValue("fileUpload2", null);
      props.form.setFieldError("pathFile2", undefined);
      props.form.setFieldTouched("pathFile2", undefined);
      props.form.setFieldValue("pathFile2", null);
      props.form.setFieldValue(props.field.name, e.target.value);
    }

    return (
      <Input onChange={onSelectChange} type="select" disabled={this.props.isViewDetail} name={props.field.name}
      //     defaultValue={detailCategory && detailCategory.categoryId ? detailCategory.typeFile1 : ""}
      >
        <option value={0}>Chọn loại dữ liệu</option>
        {optionTypeFile2}
      </Input>
    );
  };

  RenderFileUpload = props => {
    const onFileChange = (e) => {
      debugger
      props.form.setFieldError(props.field.name, undefined);
      // props.form.setFieldTouched(props.field.name, undefined);
      e.preventDefault();
      let reader = new FileReader();
      let file = e.target.files[0];
      if (file) {
        reader.onloadend = () => {
        };
        props.form.setFieldValue(props.field.name, file);
        if(this.fileInput.current){
          this.fileInput.current.value = null
        }
      }
    }
    const onDeleteFile = () => {
      props.form.setFieldValue(props.field.name, null);
    }

    const onFileClick = (value) =>{
    }

    return (
      <div className="w-100">
        <input
          onClick={onFileClick}
          ref={this.fileInput}
          disabled={this.props.isViewDetail}
          name={props.field.name}
          style={{ display: "none" }}
          id={`contained-button-file ${props.field.name}`}
          type="file"
          onChange={onFileChange}
        />
        <label htmlFor={`contained-button-file ${props.field.name}`}>
          <Button2 id={props.field.name} disabled={this.props.isViewDetail} variant="contained" color="primary"
            component="span" name={props.field.name}>
            <Icon.Upload style={{ marginRight: '5px' }} /> Upload
             </Button2>
        </label>
        <div style={{ display: 'inline-block' }}>
          {
            props.field.value?
            props.field.value.name
              : null
          }
          {
            props.field.value ?
              <HighlightOffIcon style={{ color: 'red',cursor: 'pointer' }}
                onClick={onDeleteFile} /> : ''
          }
        </div>
      </div>
    );
  };

  render() {
    const ErrorMessage = ({ name }) => (
      <Field name={name}>
        {({ field, form, meta }) => {
          const error = getIn(form.errors, name);
          const touch = getIn(form.touched, name);
          return touch && error ? (
            <div className="invalid-feedback" style={{ display: "contents" }}>{error}</div>) : null;
        }}
      </Field>
    );
    const { values, errors, touched, handleChange, isViewDetail, categoryTemp, detailCategory } = this.props;
    return (
      <div>
        <Form className="mt-2">
          <FormGroup className="form-group d-flex">
            <Label for="required" sm={2}>Loại dữ liệu 1</Label>
            <Col sm={3}>
              <div className="w-100">
                <Field disabled={this.props.isViewDetail}
                  name="typeFile1"
                  className="form-control" component={this.SelectTypeInput1}
                />
              </div>
            </Col>
            <Col sm={7}>
              <div className="w-100">
                {values.typeFile1 === 'FILE' ?
                  <div>
                    <Field
                      disabled={this.props.isViewDetail}
                      name="fileUpload1"
                      setFieldValue={this.props.setFieldValue}
                      errorMessage={errors[`fileUpload1`] ? errors[`fileUpload1`] : undefined}
                      touched={touched[`fileUpload1`]}
                      style={{ display: "flex" }}
                      onBlur={this.props.handleBlur}
                      component={this.RenderFileUpload}
                    />
                    {this.props.detailCategory.categoryId
                      && values.fileUpload1 == null ? this.props.categoryTemp ?
                        this.props.categoryTemp.typeFile1 == "FILE" ?
                          this.props.categoryTemp.pathFile1 : null : null : null
                    }
                  </div>
                  :
                  <Field as="textarea"
                    onChange={handleChange}
                    className="form-control"
                    name="pathFile1" rows="4"
                    disabled={isViewDetail}
                  />
                }
                <ErrorMessage name="fileUpload1" />
              </div>
            </Col>
          </FormGroup>
          <FormGroup className="form-group d-flex">
            <Label for="required" sm={2}>Loại dữ liệu 2</Label>
            <Col sm={3}>
              <div className="w-100">
                <Field disabled={this.props.isViewDetail}
                  name="typeFile2"
                  className="form-control" component={this.SelectTypeInput2}
                />
              </div>
            </Col>
            <Col sm={7}>
              <div className="w-100">
                {values.typeFile2 === 'FILE' ?
                  <div>
                    <Field
                      disabled={this.props.isViewDetail}
                      name="fileUpload2"
                      setFieldValue={this.props.setFieldValue}
                      errorMessage={errors[`fileUpload2`] ? errors[`fileUpload2`] : undefined}
                      touched={touched[`fileUpload2`]}
                      style={{ display: "flex" }}
                      onBlur={this.props.handleBlur}
                      component={this.RenderFileUpload}
                    />
                    {this.props.detailCategory.categoryId
                      && values.fileUpload2 == null ? this.props.categoryTemp ?
                        this.props.categoryTemp.typeFile2 == "FILE" ?
                          this.props.categoryTemp.pathFile2 : null : null : null
                    }
                  </div>
                  :
                  <Field as="textarea"
                    onChange={handleChange}
                    className="form-control"
                    name="pathFile2" rows="4"
                    disabled={isViewDetail}
                  />
                }
                <ErrorMessage name="fileUpload2" />
              </div>
            </Col>
          </FormGroup>
          <FormGroup className="form-group d-flex">
            <Label for="required" sm={2}>Transcript</Label>
            <Col sm={10}>
              <div className="w-100">
                <div>
                  <Field as="textarea"
                    onChange={handleChange}
                    className="form-control"
                    name="transcript" rows="4"
                    disabled={isViewDetail}
                    defaultValue={categoryTemp && categoryTemp.transcript && categoryTemp.transcript ? values.transcript : ""}
                  />
                  {errors.transcript && touched.transcript ? (
                    <div className="invalid-feedback mt-25" style={{ display: 'block' }}>{errors.transcript}</div>
                  ) : null}
                </div>

              </div>
            </Col>
          </FormGroup>
        </Form>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    isViewDetail: state.categoryReducer.isViewDetail,
    detailCategory: state.categoryReducer.detailCategory,
    lstTypeFile: state.categoryReducer.lstTypeFile,
    categoryTemp: state.categoryReducer.detailCategoryTemp
  }
}

const mapDispatchToProps = dispatch => ({
  categoryActions: bindActionCreators(categoryAction, dispatch)
});
const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
);

export default compose(
  withStyles(styles),
  withConnect
)(FormInput);

