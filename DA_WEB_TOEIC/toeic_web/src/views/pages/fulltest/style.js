
const styles = () => ({
  ContainerStyle:{
    width : 'auto',
    marginLeft: '5%',
    marginRight: '5%'
  },
  topicName:{
    textAlign: 'center',
    fontSize: '150%'
  },
  titleModal:{
    backgroundColor: '#1da9ed',
    margin: '0',
    height: '50px',
    borderRadius: '8px',
    color: 'white',
    position: 'relative'
  },
  bodySnp:{
    paddingLeft: '0px',
    paddingRight: '0px'
  },
  bodyModal:{
    backgroundColor: 'white',
    margin: '0 6px',
    border: '1px solid #0f7aae'
  },
  questionMargin:{
    marginBottom: '3%',
    color: 'black'
  },
  answerToChooseMargin:{
    marginBottom: '3%',
    paddingLeft: '5%'
  },
  labelRadioButton:{
    display: 'contents',
    fontSize: '100%'
  },
  divSubmit:{
    textAlign: 'center',
    margin: '5% 0 5% 0'
  },
  legend:{
    color: 'black',
    textAlign: 'left',
    backgroundColor: 'lightgray',
    width: 'auto',
    padding: '5px 20px',
    fontSize: 'medium'
  },
  fieldset:{
    padding: '0 2% 2% 2%',
    textAlign: 'left',
    backgroundColor: '#d3d3d347'
  },
  questionName:{
    paddingLeft: '0px !important',
    marginBottom: '2%',
    color: 'black'
  },
  legendDescription:{
    color: 'white !important',
    textAlign: 'left !important',
    backgroundColor: 'green !important',
    width: 'auto !important',
    padding: '1px 20px !important',
    fontSize: 'medium !important'
  },
  fieldsetDescription:{
    color: 'green !important',
    border: '1px solid green !important',
    padding: '0 2% 2% 2% !important',
    textAlign: 'left !important'
  },
  nextQuestion: {
    textAlign: 'center',
    padding: '0px',
    marginTop: '10%',
    cursor: 'pointer'
  },
// #iconNextQuestion{
//   margin: 0px !important;
//   cursor: pointer;
//   color: #b2aeae ;
// }
  /*#iconNextQuestion:hover{*/
  /*color: green;*/
  /*}*/
  btnRepeat:{
    backgroundColor: 'white',
    border: '2px solid orange',
    color: 'orange',
    fontSize: '80%',
    borderRadius: '5px !important'
  },
  legendListQuestion:{
    color: 'rgb(22, 155, 213) !important',
    textAlign: 'left !important',
    backgroundColor: 'white !important',
    width: 'auto !important',
    padding: '1px 20px !important',
    fontSize: '1.5rem !important',
    border: '1px solid rgba(121, 121, 121, 1) !important',
    marginLeft:'7% !important',
  },
  fieldsetListQuestion:{
    color: 'black !important',
    border: '1px solid rgba(121, 121, 121, 1) !important',
    padding: '0 2% 2% 2% !important',
    textAlign: 'left !important',
    marginTop:'2% !important',
    fontWeight:'700',
  },
  title: {
    textAlign: 'right',
    fontFamily: 'ArialMT, Arial, sans-serif',
    fontSize: '17px',
    marginLeft: '70px'
  },
  error: {
    color: 'red',
    fontSize: 12
  },
  textCenter: {
    textAlign: 'right',
  },
  discription: {
    marginTop: '15px',
    textOverflow: 'ellipsis',
    whiteSpace: 'nowrap',
    overflow: 'hidden',
    display: 'inline-block',
    paddingTop: '12px',
    maxWidth: '150px',
    // width: '200px',
    maxHeight: '80px',
    width: '100%',
  },
  nameEng: {
    whiteSpace: 'nowrap',
    overflow: 'hidden',
    textOverflow: 'ellipsis',
    color: 'rgba(22, 155, 213, 1)',
    width: '100px !important',
    maxWidth: '100px',
    margin: '0px !important'
  },
  upLoadFile: {
    whiteSpace: 'nowrap',
    width: '600px',
    overflow: 'hidden',
    textOverflow: 'ellipsis',
    display: 'inline-block'
  },
  mp3: {
    whiteSpace: 'nowrap',
    width: '600px',
    overflow: 'hidden',
    textOverflow: 'ellipsis',
    display: 'inline-block'
  },
  ReadFilemp3: {
    whiteSpace: 'nowrap',
    width: '300px',
    overflow: 'hidden',
    textOverflow: 'ellipsis',
    paddingTop: '12px'
  },
  showdetail: {
    readOnly: true
  },
  iconPlay:{
    padding: '0px !important',
    color: 'rgba(22, 155, 213, 1)',
    maxWidth:'40px',
    paddingBottom: '3px'
  },
  btnPlayMp3:{
    background: 'none !important',
    border: 'none !important',
    margin: 'none !important'
  },
  btnData:{
  },
  synonymous:{
    textOverflow: 'ellipsis',
    whiteSpace: 'nowrap',
    // width: '200px',
    overflow: 'hidden',
    display: 'inline-block',
    maxWidth: '150px',
  },
  textTitle:{
    paddingLeft: 'none'
  },
  buttonListenAgain:{
    backgroundColor: 'white !important',
    border: '2px solid orange !important',
    color: 'orange !important',
    fontSize: '80%',
    borderRadius: '5px !important',
    display: 'flex',
    height: '20px !important',
    width: '75px',
    margin: '0'
  }
})
export default styles;
