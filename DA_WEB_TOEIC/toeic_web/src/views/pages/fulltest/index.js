import React, { Component } from 'react';
import {connect} from "react-redux";
import {bindActionCreators, compose} from "redux";
import {Badge, Col, Container, Row, Spinner} from "reactstrap";
import styles from "./style";
import {withStyles} from "@material-ui/core";
import ListeningTest from './listening-test';
import ReadingTest from './reading-test';
import AccessTimeIcon from '@material-ui/icons/AccessTime';
import testAction from "../../../redux/actions/do-test-management";
import {history} from "../../../history";
import doTestAction from "../../../redux/actions/do-test-management";
import historyTestAction from "../../../redux/actions/history-test";

import { Button, Modal, ModalBody } from 'reactstrap';
import {showMessage} from "../../../commons/utils/convertDataForToast";
import {ArrowLeft} from "react-feather";
import {FormattedMessage} from "react-intl";

class Index extends Component {

  constructor(props) {
    super(props);
    this.state = {
      load: false,
      loadModal: false,
      data: false,
      listSttQuesListeningChoosen: [],
      listSttQuesReadingChoosen: [],
      modalConfirmSubmit: false,
      modalConfirmDoTest:true,
      modalTimeIsUp: false,
      modalAutoTimeIsUp: true,
      second: 10,
      minute: 0,
      hours:0,
      keepDoTest: true,
      keepDoTest2: false,
      waitClosePopup: true,
    }
  }

  getlistQuesListeningPrimitive =()=> {
    const list = this.props.listQuestionMinitest;

    let listQuesListeningPrimitive = [];

    for(let i=0;i<list[0].listCategoryMinitestDTOS.length;i++){
      for(let j=0;j<list[0].listCategoryMinitestDTOS[i].listQuestionMinitestDTOS.length;j++){
        let question = {};
        question.id = Number(list[0].listCategoryMinitestDTOS[i].listQuestionMinitestDTOS[j].id);
        question.sentenceNo = Number(list[0].listCategoryMinitestDTOS[i].listQuestionMinitestDTOS[j].sentenceNo);
        question.answerChoosen = "null";
        question.indexSubAnswer = null;
        question.categoryId = Number(list[0].listCategoryMinitestDTOS[i].categoryId);
        question.partName = list[0].partName;
        listQuesListeningPrimitive.push(question)
      }
    }

    for(let i=0;i<list[1].listCategoryMinitestDTOS.length;i++){
      for(let j=0;j<list[1].listCategoryMinitestDTOS[i].listQuestionMinitestDTOS.length;j++){
        let question = {};
        question.id = Number(list[1].listCategoryMinitestDTOS[i].listQuestionMinitestDTOS[j].id);
        question.sentenceNo = Number(list[1].listCategoryMinitestDTOS[i].listQuestionMinitestDTOS[j].sentenceNo);
        question.answerChoosen = "null";
        question.indexSubAnswer = null;
        question.categoryId = Number(list[1].listCategoryMinitestDTOS[i].categoryId);
        question.partName = list[1].partName;
        listQuesListeningPrimitive.push(question)
      }
    }

    for(let i=0;i<list[2].listCategoryMinitestDTOS.length;i++){
      for(let j=0;j<list[2].listCategoryMinitestDTOS[i].listQuestionMinitestDTOS.length;j++){
        let question = {};
        question.id = Number(list[2].listCategoryMinitestDTOS[i].listQuestionMinitestDTOS[j].id);
        question.sentenceNo = Number(list[2].listCategoryMinitestDTOS[i].listQuestionMinitestDTOS[j].sentenceNo);
        question.answerChoosen = "null";
        question.indexSubAnswer = null;
        question.categoryId = Number(list[2].listCategoryMinitestDTOS[i].categoryId);
        question.partName = list[2].partName;
        listQuesListeningPrimitive.push(question)
      }
    }

    for(let i=0;i<list[3].listCategoryMinitestDTOS.length;i++){
      for(let j=0;j<list[3].listCategoryMinitestDTOS[i].listQuestionMinitestDTOS.length;j++){
        let question = {};
        question.id = Number(list[3].listCategoryMinitestDTOS[i].listQuestionMinitestDTOS[j].id);
        question.sentenceNo = Number(list[3].listCategoryMinitestDTOS[i].listQuestionMinitestDTOS[j].sentenceNo);
        question.answerChoosen = "null";
        question.indexSubAnswer = null;
        question.categoryId = Number(list[3].listCategoryMinitestDTOS[i].categoryId);
        question.partName = list[3].partName;
        listQuesListeningPrimitive.push(question)
      }
    }

    return listQuesListeningPrimitive;
  }

  getlistQuesReadingPrimitive =()=> {
    const list = this.props.listQuestionMinitest;

    let listQuesReadingPrimitive = [];

    for(let i=0;i<list[4].listCategoryMinitestDTOS.length;i++){
      for(let j=0;j<list[4].listCategoryMinitestDTOS[i].listQuestionMinitestDTOS.length;j++){
        let question = {};
        question.id = Number(list[4].listCategoryMinitestDTOS[i].listQuestionMinitestDTOS[j].id);
        question.sentenceNo = Number(list[4].listCategoryMinitestDTOS[i].listQuestionMinitestDTOS[j].sentenceNo);
        question.answerChoosen = "null";
        question.indexSubAnswer = null;
        question.categoryId = Number(list[4].listCategoryMinitestDTOS[i].categoryId);
        question.partName = list[4].partName;
        listQuesReadingPrimitive.push(question)
      }
    }


    for(let i=0;i<list[5].listCategoryMinitestDTOS.length;i++){
      for(let j=0;j<list[5].listCategoryMinitestDTOS[i].listQuestionMinitestDTOS.length;j++){
        let question = {};
        question.id = Number(list[5].listCategoryMinitestDTOS[i].listQuestionMinitestDTOS[j].id);
        question.sentenceNo = Number(list[5].listCategoryMinitestDTOS[i].listQuestionMinitestDTOS[j].sentenceNo);
        question.answerChoosen = "null";
        question.indexSubAnswer = null;
        question.categoryId = Number(list[5].listCategoryMinitestDTOS[i].categoryId);
        question.partName = list[5].partName;
        listQuesReadingPrimitive.push(question)
      }
    }

    for(let i=0;i<list[6].listCategoryMinitestDTOS.length;i++){
      for(let j=0;j<list[6].listCategoryMinitestDTOS[i].listQuestionMinitestDTOS.length;j++){
        let question = {};
        question.id = Number(list[6].listCategoryMinitestDTOS[i].listQuestionMinitestDTOS[j].id);
        question.sentenceNo = Number(list[6].listCategoryMinitestDTOS[i].listQuestionMinitestDTOS[j].sentenceNo);
        question.answerChoosen = "null";
        question.indexSubAnswer = null;
        question.categoryId = Number(list[6].listCategoryMinitestDTOS[i].categoryId);
        question.partName = list[6].partName;
        listQuesReadingPrimitive.push(question)
      }
    }

    return listQuesReadingPrimitive;

  }

  submitAnswer =(selfOrAuto)=>{
    debugger
    const {listQuestionListeningChoosenDTOS, listQuestionReadingChoosenDTOS , testAction, hourForFulltest,minuteForFulltest, secondForFulltest } = this.props;
    const { modalConfirmSubmit, modalTimeIsUp, modalAutoTimeIsUp} = this.state;
    let h = parseInt(( (hourForFulltest * 3600 + minuteForFulltest * 60 + secondForFulltest) - (this.state.hours * 3600 + this.state.minute * 60 + this.state.second)) / 3600)

    let m = parseInt(( (hourForFulltest * 3600 + minuteForFulltest * 60 + secondForFulltest) - (this.state.hours * 3600 + this.state.minute * 60 + this.state.second)) % 3600 / 60 )

    let s =  ( (hourForFulltest * 3600 + minuteForFulltest * 60 + secondForFulltest) - (this.state.hours * 3600 + this.state.minute * 60 + this.state.second)) % 3600 % 60
    testAction.saveDoTime(h.toString() + ':'+ m.toString()+ ':' + s.toString());
    let MinitestSubmitAnswer = {};

    if(listQuestionListeningChoosenDTOS.length > 0){
      MinitestSubmitAnswer.listQuestionListeningChoosenDTOS = listQuestionListeningChoosenDTOS;
    }
    else {
      MinitestSubmitAnswer.listQuestionListeningChoosenDTOS = this.getlistQuesListeningPrimitive();
    }

    if(listQuestionReadingChoosenDTOS.length > 0){
      MinitestSubmitAnswer.listQuestionReadingChoosenDTOS = listQuestionReadingChoosenDTOS;
    }
    else {
      MinitestSubmitAnswer.listQuestionReadingChoosenDTOS = this.getlistQuesReadingPrimitive();
    }
    MinitestSubmitAnswer.userId = this.props.login.userInfoLogin.userId
    MinitestSubmitAnswer.userShowName = this.props.login.userInfoLogin.userShowName
    MinitestSubmitAnswer.testId = this.props.detailTest.id
    MinitestSubmitAnswer.totalTime = h.toString() + ':'+ m.toString()+ ':' + s.toString();
    this.setState({
      load: true,
      loadModal: true
    })

    if(selfOrAuto){
      // this.modalAutoTimeIsUp(false)
      this.setState({
        modalAutoTimeIsUp: !modalAutoTimeIsUp
      })
    }
    else{
      setTimeout(()=>{
        this.setState({
          load: false,
          modalConfirmSubmit: !modalConfirmSubmit,
          modalTimeIsUp: !modalTimeIsUp,
          loadModal: false

        })
      }, 2000)
      this.modalTimeIsUp(false)
    }


    const {getResultFullTest} = testAction;
    if(this.props.detailTest){
      MinitestSubmitAnswer.testName = this.props.detailTest.name
    }
    getResultFullTest(MinitestSubmitAnswer);

    const {getListQuesListeningChoosen, getListQuesReadingChoosen} = testAction;
    let listListening = [], listReading = [];
    getListQuesListeningChoosen(listListening);
    getListQuesReadingChoosen(listReading);

    const {doReadingAfterListeningFulltest} = testAction;
    doReadingAfterListeningFulltest(true)

  }


  //------------------modal Xác nhận bắt đầu làm bài----------------
  toggleConfirmDoTest = () => {
    const {modalConfirmDoTest} = this.state;
    const {testAction,doTestActions,detailTest} = this.props;
    const {noTranslationWhenDoingTest} = testAction;
    noTranslationWhenDoingTest(true)
    const {getDetailFullTest} = doTestActions;
    let fullTest ={};
    fullTest.id = detailTest.id;
    fullTest.name = detailTest.name;
    getDetailFullTest(fullTest);
    this.setState({
      modalConfirmDoTest: !modalConfirmDoTest,
      keepDoTest2: true,
      waitClosePopup: false
    })
  }

  modalConfirmDoTest = () => {
    const {modalConfirmDoTest} = this.state;
    return (
      <div>
        <Modal isOpen={modalConfirmDoTest} toggle={() => { history.push('/')}}>
          <div className="text-center" style={{padding: '2%', fontSize: '160%',
            fontWeight: 'bold', borderBottom: '1px solid gray', color: '#959494'}}>

            <Row>
              <Col xs={11} style={{textAlign:'center', paddingLeft: '10%'}}>
                {/*Xác nhận nộp bài*/}
                <FormattedMessage id={"confirm.doing.test"}/>
              </Col>
              <Col xs={1} style={{textAlign:'center'}}>
                <a href="/" style={{color: 'rgb(149, 148, 148)'}}>x</a>
              </Col>
            </Row>

            {/*<span ><FormattedMessage id={"confirm.doing.test"}/></span>*/}
            {/*<span  className="float-right" id="sttQues" style={{ fontWeight: '800'}}>*/}
              {/*<a href="/" style={{color: 'rgb(149, 148, 148)'}}>x</a>*/}
            {/*</span>*/}
          </div>
          <ModalBody>
            <div style={{textAlign: 'center'}}>
              <h3><FormattedMessage id={"confirm.end.test"}/></h3>
            </div>
          </ModalBody>
          <Row style={{padding: '2%', borderTop: '1px solid gray', margin: '0'}}>
            <Col xs="6" md="6" sm="6" lg="6">
              <a href="/"><Button id="btnHighLight" color="secondary"><FormattedMessage id={"cancel"}/></Button></a>{' '}
            </Col>
            <Col xs="6" md="6" sm="6" lg="6" style={{textAlign: 'right'}}>
              <button id="btnHighLight" className="btn btn-primary" onClick={() => this.toggleConfirmDoTest()}><FormattedMessage id={"start.taking.test"}/></button>
            </Col>
          </Row>
        </Modal>
      </div>
    );
  }
  // ------------------end modal xác nhận bắt đầu làm bài----------------



  //------------------modal xác nhận nộp bài----------------
  toggleConfirmSubmit = () => {
    const {modalConfirmSubmit, modalAutoTimeIsUp} = this.state;
    const {testAction} = this.props;
    const {noTranslationWhenDoingTest} = testAction;
    noTranslationWhenDoingTest(false)
    this.setState({
      modalConfirmSubmit: !modalConfirmSubmit
    })
  }

  modalConfirmSubmit = () => {
    const {modalConfirmSubmit} = this.state;
    return (
      <div>
        {/*<Button color="danger" >Test Modal</Button>*/}
        <Modal isOpen={modalConfirmSubmit}>
          <div style={{padding: '2%', fontSize: '160%',
            fontWeight: '600', borderBottom: '1px solid gray', color: '#959494'}}>

            <Row>
              <Col xs={11} style={{textAlign:'center', paddingLeft: '10%'}}>
                {/*Xác nhận nộp bài*/}
                <FormattedMessage id="confirm.submitting.this.test" tagName="data" />
              </Col>
              <Col xs={1} style={{textAlign:'center'}}>
                <span id="sttQues" style={{marginLeft: '26%', fontWeight: '800'}} onClick={() => this.toggleConfirmSubmit()}>x</span>
              </Col>
            </Row>

            {/*<span style={{marginLeft: '28%'}}><FormattedMessage id={"confirm.test.submit"}/></span>*/}
            {/*<span id="sttQues" style={{marginLeft: '26%', fontWeight: '800'}} onClick={() => this.toggleConfirmSubmit()}>x</span>*/}
          </div>
          <ModalBody>
            <div style={{textAlign: 'center'}}>
              <h5><FormattedMessage id={"answered.not.all.question"}/></h5>
              <h5><FormattedMessage id={"confirm.sure.submit.test"}/></h5>
            </div>
          </ModalBody>
          <Row style={{padding: '2%', borderTop: '1px solid gray', margin: '0'}}>
            <Col xs="6" md="6" sm="6" lg="6">
              <Button color="danger" id="btnHighLight"
                      onClick={ () => this.toggleTimeIsUp()}><FormattedMessage id={"submit"}/></Button>{' '}
            </Col>
            <Col xs="6" md="6" sm="6" lg="6" style={{textAlign: 'right'}}>
              <button id="btnHighLight" className="btn btn-primary" onClick={() => this.toggleConfirmSubmit()}><FormattedMessage id={"continue.doing.test"}/></button>
            </Col>
          </Row>
        </Modal>
      </div>
    );
  }
  // ------------------end modal xác nhận nộp bài----------------



  //---modal nhắc còn 5p---------
  toggleRemainder = () => {
    const {keepDoTest} = this.state;
    this.setState({
      keepDoTest: !keepDoTest
    })
  }

  modalRemainder = () => {
    const {keepDoTest} = this.state;
    return (
      <div>
        <Modal isOpen={keepDoTest}>
          <div style={{padding: '2%', fontSize: '160%',
            fontWeight: '600', borderBottom: '1px solid gray', color: '#959494'}}>
            <span style={{marginLeft: '37%'}}><FormattedMessage id={"notification"}/></span>
            <span id="sttQues" style={{marginLeft: '35%', fontWeight: '800'}} onClick={() => this.toggleRemainder()}>x</span>
          </div>
          <ModalBody>
            <div style={{textAlign: 'center'}}>
              <h5><FormattedMessage id={"coming.to.end.time"}/></h5>
              <h5><FormattedMessage id={"last.5.minutes"}/></h5>
            </div>
          </ModalBody>
          <Row style={{padding: '2%', borderTop: '1px solid gray', margin: '0'}}>
            <Col xs="6" md="6" sm="6" lg="6">
              <Button color="danger" id="btnHighLight"
                      onClick={ () => this.toggleConfirmSubmit(true)}><FormattedMessage id={"submit"}/></Button>{' '}
            </Col>
            <Col xs="6" md="6" sm="6" lg="6" style={{textAlign: 'right'}}>
              <button id="btnHighLight" className="btn btn-primary" onClick={() => this.toggleRemainder()}><FormattedMessage id={"continue.doing.test"}/></button>
            </Col>
          </Row>
        </Modal>
      </div>
    );
  }
  //---end modal nhắc còn 5p-----------



  //--- modal time is up ------------
  toggleTimeIsUp = () => {
    const {modalTimeIsUp} = this.state;
    this.setState({
      modalTimeIsUp: !modalTimeIsUp,
      keepDoTest2: false
    })
  }

  modalTimeIsUp = () => {
    const {modalTimeIsUp} = this.state;
    const {hourForFulltest, minuteForFulltest, secondForFulltest} = this.props;
    return (
      <div>
        {/*<Button color="danger" >Test Modal</Button>*/}
        <Modal isOpen={modalTimeIsUp}>
          {this.props.loadingSubmitAnswer && this.props.listQuestionAndAnswerMinitest.length === 0 ? this.loadingComponent : null}
          <div style={{padding: '2%', fontSize: '160%',
            fontWeight: '600', borderBottom: '1px solid gray', color: '#959494'}}>
            <span style={{marginLeft: '37%'}}><FormattedMessage id={"notification"}/></span>
            <span id="sttQues" style={{marginLeft: '33%', fontWeight: '800'}}>
              <a href="/" style={{color: 'rgb(149, 148, 148)'}}>x</a>
            </span>
          </div>
          <ModalBody>
            <div style={{textAlign: 'center'}}>
              <h5><FormattedMessage id={"congratulations.test"}/></h5>
              <h5><FormattedMessage id={"total.test.time"}/>: <span style={{color: 'red', fontWeight: '600'}}>
                {parseInt(((hourForFulltest * 3600 + minuteForFulltest * 60 + secondForFulltest) - (this.state.hours * 3600 + this.state.minute *60 + this.state.second)) / 60)}
                :
                {
                  ((hourForFulltest * 3600 + minuteForFulltest * 60 + secondForFulltest) - (this.state.hours * 3600 + this.state.minute *60 + this.state.second)) % 60
                }
              </span></h5>
            </div>
          </ModalBody>
          <Row style={{padding: '2%',  borderTop: '1px solid gray', margin: '0'}}>
            <Col xs={6} style={{float:'left'}}>
              <a href="/"><Button color="secondary" id="btnHighLight"><FormattedMessage id={"cancel"}/></Button></a>
            </Col>
            <Col xs={1}>

            </Col>
            <Col xs={5} style={{float: 'right'}}>
              <button id="btnHighLight" className="btn btn-primary" onClick={() => this.submitAnswer(false)} style={{marginLeft:'21%'}}>
                <FormattedMessage id={"view.results"}/>
              </button>

            </Col>
          </Row>
        </Modal>
      </div>
    );
  }
  //--- modal time is up ------------

  //--- modal auto time is up ------------

  modalAutoTimeIsUp = () => {
    const {hourForFulltest, minuteForFulltest, secondForFulltest} = this.props;
    const {modalAutoTimeIsUp} = this.state;
    return (
      <div>
        <Modal isOpen={modalAutoTimeIsUp}>
          {this.props.loadingSubmitAnswer && this.props.listQuestionAndAnswerMinitest === [] ? this.loadingComponent : null}
          <div style={{padding: '2%', fontSize: '160%',
            fontWeight: '600', borderBottom: '1px solid gray', color: '#959494'}}>
            <span style={{marginLeft: '37%'}}><FormattedMessage id={"notification"}/></span>
            <span id="sttQues" style={{marginLeft: '33%', fontWeight: '800'}}>
              <a href="/" style={{color: 'rgb(149, 148, 148)'}}>x</a>
            </span>
          </div>
          <ModalBody>
            <div style={{textAlign: 'center'}}>
              <h5><FormattedMessage id={"congratulations.test"}/></h5>
              <h5><FormattedMessage id={"total.test.time"}/>: <span style={{color: 'red', fontWeight: '600'}}>
               {parseInt(((hourForFulltest * 3600 + minuteForFulltest * 60 + secondForFulltest) - (this.state.hours * 3600 + this.state.minute *60 + this.state.second)) / 60)}
                :
                {
                  ((hourForFulltest * 3600 + minuteForFulltest * 60 + secondForFulltest) - (this.state.hours * 3600 + this.state.minute *60 + this.state.second)) % 60
                }
              </span></h5>
            </div>
          </ModalBody>
          <Row style={{padding: '2%',  borderTop: '1px solid gray', margin: '0'}}>
            <Col xs="4" md="4" sm="4" lg="4">
              <a href="/"><Button color="secondary"><FormattedMessage id={"cancel"}/></Button></a>
            </Col>
            <Col xs="4" md="4" sm="4" lg="4">
              {/*<Button color="danger" id="btnHighLight"*/}
                      {/*onClick={ () => this.doTestAgain()}>Làm lại</Button>*/}
            </Col>
            <Col xs="4" md="4" sm="4" lg="4" style={{textAlign: 'right'}}>
              <button id="btnHighLight" className="btn btn-primary" onClick={() => this.submitAnswer(true)}>
                <FormattedMessage id={"view.results"}/>
              </button>

            </Col>
          </Row>
        </Modal>
      </div>
    );


  }
  //--- modal auto time is up------------

  componentDidMount() {
    const {cleanListChoosen} = this.props.testAction;
    // cleanListChoosen();
  }

  // componentWillMount() {
  //   if(this.props.viewHistory){
  //     this.setState({
  //       waitClosePopup: false
  //     })
  //   }
  // }

  setCountdown =()=>{
    const {second, minute, load, keepDoTest2, hours} = this.state;
    if(keepDoTest2 === true){
      if(hours === 0){
        if(minute === 0){
          if(second > 0){
            setTimeout(()=>{
              this.setState({
                second: second - 1
              })
            }, 1000)
          }
        }
        else{
          if(second > 0){
            setTimeout(()=>{
              this.setState({
                second: second - 1
              })
            }, 1000)
          }
          if(second === 0){
            setTimeout(()=>{
              this.setState({
                minute: minute - 1,
                second: 59
              })
            }, 1000)
          }
        }
      } else {
        if(minute === 0){
          if(second > 0){
            setTimeout(()=>{
              this.setState({
                second: second - 1
              })
            }, 1000)
          } else {
            setTimeout(()=>{
              this.setState({
                hours: hours - 1,
                minute: 59,
                second: 59
              })
            }, 1000)
          }
        }
        else{
          if(second > 0){
            setTimeout(()=>{
              this.setState({
                second: second - 1
              })
            }, 1000)
          }
          if(second === 0){
            setTimeout(()=>{
              this.setState({
                minute: minute - 1,
                second: 59
              })
            }, 1000)
          }
        }
      }
    }

    return(
      <>{this.state.hours < 10 ? 0 : null}{this.state.hours}:{this.state.minute < 10 ? 0 : null}{this.state.minute}:{this.state.second < 10 ? 0 : null}{this.state.second}</>
    )
  }

  linkToElement =(string)=>{
    const element = document.getElementById(string);
    element.scrollIntoView()
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if(this.props.showAnswer !== prevProps.showAnswer){
      const element = document.getElementById("topPage");
      element.scrollIntoView()
    }
  }

  loadingComponent = (
    <div style={{
      position: 'absolute',
      zIndex: 110,
      top: 0,
      left: 0,
      width: '100%',
      height: '100%',
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      background: 'rgba(255,255,255,0.8)',
    }}>
      <Spinner color="primary" className="reload-spinner"/>
    </div>
  );

  backToHistory =()=> {
    let {historyTestActions} = this.props;
    let {updateActiveTabWhenFail} = historyTestActions;
    updateActiveTabWhenFail("2");
    history.push("/pages/profiles");
  }

  render() {
    const {classes, listQuestionMinitest, listQuestionAndAnswerMinitest, showAnswer,
      listQuestionListeningChoosenDTOS, listQuestionReadingChoosenDTOS, loadingQuestion, loadingSubmitAnswer, listHistoryFullTest} = this.props;
    const {load} = this.state;
    let totalQuestionListening = 0;
    let totalCorrectAnswerListening = 0;
    let totalQuestionReading = 0;
    let totalCorrectAnswerReading = 0;
    let totalQuestionListeningHistory = 0;
    let totalCorrectAnswerListeningHistory = 0;
    let totalQuestionReadingHistory = 0;
    let totalCorrectAnswerReadingHistory = 0;
    if(showAnswer === true){
      for(let i=0; i<4;i++){
        if(listQuestionAndAnswerMinitest[i]){
          totalQuestionListening += listQuestionAndAnswerMinitest[i].totalQuestion;
          totalCorrectAnswerListening += listQuestionAndAnswerMinitest[i].totalCorectAnswer;
        }
      }
      for(let i=4; i<listQuestionAndAnswerMinitest.length;i++){
        if(listQuestionAndAnswerMinitest[i]){
          totalQuestionReading += listQuestionAndAnswerMinitest[i].totalQuestion;
          totalCorrectAnswerReading += listQuestionAndAnswerMinitest[i].totalCorectAnswer;
        }
      }
    }

    if(this.props.viewHistory && this.props.loadingResult){
      for(let i=0; i<4;i++){
        if(listHistoryFullTest[i]){
          totalQuestionListeningHistory += listHistoryFullTest[i].totalQuestion;
          totalCorrectAnswerListeningHistory += listHistoryFullTest[i].totalCorectAnswer;
        }
      }
      for(let i=4; i<listHistoryFullTest.length;i++){
        if(listHistoryFullTest[i]){
          totalQuestionReadingHistory += listHistoryFullTest[i].totalQuestion;
          totalCorrectAnswerReadingHistory += listHistoryFullTest[i].totalCorectAnswer;
        }
      }
    }
    let sizeOfListeningQues = 0;
    if(listQuestionMinitest.length > 0){
      sizeOfListeningQues = this.getlistQuesListeningPrimitive().length;
      console.log("sizeOfListeningQues", sizeOfListeningQues)
    }

    return (
      <>
        <style>{`
          #buttonTest:hover{
            background-color: #91d6f3 !important;
          }
          #btnSubmit{
            background-color: #02a1ed !important;
          }

          #btnSubmit:focus-within {
            border: 1px solid black !important;
            text-shadow: 2px 2px 5px gray;
          }

          #btnSubmit2{
            background-color: #02a1ed !important;
            font-size: 150%;
            font-weight: 700;
          }
          th{
            font-size: 120% !important;
            color: black !important;
            border: 1px solid gray !important;
          }
          td{
            border: 1px solid gray !important;
          }

          #scrollQuestion {
            height: 90vh;
            overflow: scroll;
            padding-bottom: 7px;
          }

          #sttQues:hover {
            cursor: pointer;
            background-color: #ede9e9;
          }

          #btnHighLight:focus-within {
            border: 1px solid black !important;
            text-shadow: 2px 2px 5px gray;
          }

          #btnBackHistory:focus-within {
            border: 1px solid black !important;
          }

        `}</style>

        {/*{*/}
          {/*!loadingQuestion && !this.props.loadingResult ? this.loadingComponent : null*/}
        {/*}*/}
        <Container fluid={true} style={{ backgroundColor: 'white', padding: '2%'}}>
          {/*{*/}
            {/*!loadingQuestion && !this.props.loadingResult ? this.loadingComponent : null*/}
          {/*}*/}
          {
            this.props.viewHistory ?
              <Col id="topPage" xs="12" md="12" sm="12" lg="12">
                <Button id="btnBackHistory" color="primary" className="btn-icon right" color="primary" left onClick={this.backToHistory} title={showMessage("back")}>
                  <ArrowLeft/>
                </Button>
                <Row>
                  <Col xs="4" md="4" sm="4" lg="4">
                    <h1 style={{fontWeight: '700', fontSize:'2rem'}}>
                      {/*Bài thi*/}
                      <FormattedMessage id={"exam"}/>{' '}
                      {this.props.detailHistoryFullTest ? this.props.detailHistoryFullTest.testName : ""}
                    </h1>
                  </Col>
                  <Col xs="4" md="4" sm="4" lg="4" style={{textAlign: 'center'}}>
                    <h4 style={{color: 'gray', fontWeight: '700'}}><FormattedMessage id={"total.test.time"}/></h4>
                    <h3 style={{color: 'orange', fontWeight: '700'}}>
                      <>
                        {this.props.detailHistoryFullTest ? this.props.detailHistoryFullTest.totalTime : ""}
                      </>
                    </h3>
                  </Col>
                  <Col xs="4" md="4" sm="4" lg="4" style={{textAlign: 'center'}}>
                    <h4 style={{color: 'gray', fontWeight: '700'}}><FormattedMessage id={"total.score.achieved"}/></h4>
                    <h3 style={{color: 'green', fontWeight: '700'}}>
                      {this.props.detailHistoryFullTest ? this.props.detailHistoryFullTest.totalScore : 0}
                    </h3>
                  </Col>
                </Row>
                <Row>
                  <Col xs={12}>
                    {
                      !this.props.loadingResult ? this.loadingComponent : null
                    }
                    <Row className={classes.colTable} style={{padding: '1%'}}>
                      <table className="table table-bordered"
                             style={{border: '1px solid gray',
                               backgroundColor: 'white',
                               marginTop: '3%', color: 'black', textAlign: 'center'}}>
                        <thead>
                        <tr>
                          <th><FormattedMessage id={"format.test"}/></th>
                          <th style={{textAlign:'left'}}><FormattedMessage id={"part"}/></th>
                          <th style={{textAlign:'right'}}><FormattedMessage id={"numberCorrect"}/></th>
                          <th><FormattedMessage id={"total.of.correct.answer"}/></th>
                          <th><FormattedMessage id={"score"}/></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                          <td rowSpan={4}><b><FormattedMessage id={"listening.part"}/></b></td>
                          <td style={{textAlign:'left'}}>Part 1: <FormattedMessage id={"part1"}/></td>
                          <td style={{textAlign:'right'}}>{this.props.detailHistoryFullTest ? this.props.detailHistoryFullTest.part1 : ""}</td>
                          <td rowSpan={4}><b>{totalCorrectAnswerListeningHistory}/{totalQuestionListeningHistory}</b></td>
                          <td rowSpan={4}><b>{this.props.detailHistoryFullTest ? this.props.detailHistoryFullTest.listeningScore : 0}</b></td>
                        </tr>
                        <tr>
                          <td style={{textAlign:'left'}}>Part 2: <FormattedMessage id={"part2"}/></td>
                          <td style={{textAlign:'right'}}>{this.props.detailHistoryFullTest ? this.props.detailHistoryFullTest.part2 : ""}</td>
                        </tr>
                        <tr>
                          <td style={{textAlign:'left'}}>Part 3: <FormattedMessage id={"part3"}/></td>
                          <td style={{textAlign:'right'}}>{this.props.detailHistoryFullTest ? this.props.detailHistoryFullTest.part3 : ""}</td>
                        </tr>
                        <tr>
                          <td style={{textAlign:'left'}}>Part 4: <FormattedMessage id={"part4"}/></td>
                          <td style={{textAlign:'right'}}>{this.props.detailHistoryFullTest ? this.props.detailHistoryFullTest.part4 : ""}</td>
                        </tr>
                        <tr>
                          <td rowSpan={4}><b>
                            {/*Bài đọc*/}
                            <FormattedMessage id={"reading.part"}/>
                          </b></td>
                          <td style={{textAlign:'left'}}>Part 5: <FormattedMessage id={"part5.fulltest"}/></td>
                          <td style={{textAlign:'right'}}>{this.props.detailHistoryFullTest ? this.props.detailHistoryFullTest.part5 : ""}</td>
                          <td rowSpan={4}><b>{totalCorrectAnswerReadingHistory}/{totalQuestionReadingHistory}</b></td>
                          <td rowSpan={4}><b>{this.props.detailHistoryFullTest ? this.props.detailHistoryFullTest.readingScore : 0}</b></td>
                        </tr>
                        <tr>
                          <td style={{textAlign:'left'}}>Part 6: <FormattedMessage id={"part6"}/></td>
                          <td style={{textAlign:'right'}}>{this.props.detailHistoryFullTest ? this.props.detailHistoryFullTest.part6 : ""}</td>
                        </tr>
                        <tr>
                          <td style={{textAlign:'left'}}>Part 7: <FormattedMessage id={"part7.fulltest"}/></td>
                          <td style={{textAlign:'right'}}>{this.props.detailHistoryFullTest ? this.props.detailHistoryFullTest.part7 : ""}</td>
                        </tr>

                        </tbody>
                      </table>
                    </Row>
                  </Col>
                </Row>
              </Col>
              :
            showAnswer === false && !this.props.viewHistory ?
            <Col id="topPage" xs="12" md="12" sm="12" lg="12">
              <h1 style={{fontWeight: '700',fontSize:'2rem' }}>
                {/*Bài thi*/}
                <FormattedMessage id={"exam"}/>{' '}
                {this.props.detailTest ? this.props.detailTest.name : ""}
              </h1>
            </Col>
            :
            <Col id="topPage" xs="12" md="12" sm="12" lg="12">
              <Row>
                <Col xs="4" md="4" sm="4" lg="4">
                  <h1 style={{fontWeight: '700', fontSize:'2rem'}}>
                    {/*Bài thi*/}
                    <FormattedMessage id={"exam"}/>{' '}
                    {this.props.detailTest ? this.props.detailTest.name : ""}
                  </h1>
                </Col>
                <Col xs="4" md="4" sm="4" lg="4" style={{textAlign: 'center'}}>
                  <h4 style={{color: 'gray', fontWeight: '700'}}>
                    {/*Tổng thời gian làm bài*/}
                    <FormattedMessage id={"total.test.time"}/>
                  </h4>
                  <h3 style={{color: 'orange', fontWeight: '700'}}>
                    <>{this.props.doTime}
                    </>
                  </h3>
                </Col>
                <Col xs="4" md="4" sm="4" lg="4" style={{textAlign: 'center'}}>
                  <h4 style={{color: 'gray', fontWeight: '700'}}><FormattedMessage id={"total.score.achieved"}/></h4>
                  <h3 style={{color: 'green', fontWeight: '700'}}>
                    {
                      listQuestionAndAnswerMinitest && listQuestionAndAnswerMinitest && listQuestionAndAnswerMinitest[0] && listQuestionAndAnswerMinitest[4] ?
                      listQuestionAndAnswerMinitest[0].sumScore + listQuestionAndAnswerMinitest[4].sumScore : 0}
                  </h3>
                </Col>
              </Row>
              <Row>
                <Col xs={12}>
                  {
                    !loadingQuestion || this.props.loadingResult ? this.loadingComponent : null
                  }
                  <Row className={classes.colTable} style={{padding: '1%'}}>
                    <table className="table table-bordered"
                           style={{border: '1px solid gray',
                             backgroundColor: 'white',
                             marginTop: '3%', color: 'black', textAlign: 'center'}}>
                      <thead>
                      <tr>
                        <th><FormattedMessage id={"format.test"}/></th>
                        <th style={{textAlign:'left'}}><FormattedMessage id={"part"}/></th>
                        <th style={{textAlign:'right'}}><FormattedMessage id={"numberCorrect"}/></th>
                        <th><FormattedMessage id={"total.of.correct.answer"}/></th>
                        <th><FormattedMessage id={"score"}/></th>
                      </tr>
                      </thead>
                      <tbody>
                      <tr>
                        <td rowSpan={4}><b><FormattedMessage id={"listening.part"}/></b></td>
                        <td style={{textAlign:'left'}}>Part 1: <FormattedMessage id={"part1"}/></td>
                        <td style={{textAlign:'right'}}>{listQuestionAndAnswerMinitest[0] ? listQuestionAndAnswerMinitest[0].totalCorectAnswer : 0}/{listQuestionAndAnswerMinitest[0] ? listQuestionAndAnswerMinitest[0].totalQuestion : 0}</td>
                        <td rowSpan={4}><b>{totalCorrectAnswerListening}/{totalQuestionListening}</b></td>
                        <td rowSpan={4}><b>{listQuestionAndAnswerMinitest[0] ? listQuestionAndAnswerMinitest[0].sumScore : 0}</b></td>
                      </tr>
                      <tr>
                        <td style={{textAlign:'left'}}>Part 2: <FormattedMessage id={"part2"}/></td>
                        <td style={{textAlign:'right'}}>{listQuestionAndAnswerMinitest[1] ? listQuestionAndAnswerMinitest[1].totalCorectAnswer : 0}/{listQuestionAndAnswerMinitest[1] ? listQuestionAndAnswerMinitest[1].totalQuestion : 0}</td>
                      </tr>
                      <tr>
                        <td style={{textAlign:'left'}}>Part 3: <FormattedMessage id={"part3"}/></td>
                        <td style={{textAlign:'right'}}>{listQuestionAndAnswerMinitest[2] ? listQuestionAndAnswerMinitest[2].totalCorectAnswer : 0}/{listQuestionAndAnswerMinitest[2] ? listQuestionAndAnswerMinitest[2].totalQuestion : 0}</td>
                      </tr>
                      <tr>
                        <td style={{textAlign:'left'}}>Part 4: <FormattedMessage id={"part4"}/></td>
                        <td style={{textAlign:'right'}}>{listQuestionAndAnswerMinitest[3] ? listQuestionAndAnswerMinitest[3].totalCorectAnswer : 0}/{listQuestionAndAnswerMinitest[3] ? listQuestionAndAnswerMinitest[3].totalQuestion : 0}</td>
                      </tr>
                      <tr>
                        <td rowSpan={4}><b>
                          {/*Bài đọc*/}
                          <FormattedMessage id={"reading.part"}/>
                        </b></td>
                        <td style={{textAlign:'left'}}>Part 5: <FormattedMessage id={"part5.fulltest"}/></td>
                        <td style={{textAlign:'right'}}>{listQuestionAndAnswerMinitest[4] ? listQuestionAndAnswerMinitest[4].totalCorectAnswer : 0}/{listQuestionAndAnswerMinitest[4] ? listQuestionAndAnswerMinitest[4].totalQuestion : 0}</td>
                        <td rowSpan={4}><b>{totalCorrectAnswerReading}/{totalQuestionReading}</b></td>
                        <td rowSpan={4}><b>{listQuestionAndAnswerMinitest[4] ? listQuestionAndAnswerMinitest[4].sumScore : 0}</b></td>
                      </tr>
                      <tr>
                        <td style={{textAlign:'left'}}>Part 6: <FormattedMessage id={"part6"}/></td>
                        <td style={{textAlign:'right'}}>{listQuestionAndAnswerMinitest[5] ? listQuestionAndAnswerMinitest[5].totalCorectAnswer : 0}/{listQuestionAndAnswerMinitest[5] ? listQuestionAndAnswerMinitest[5].totalQuestion : 0}</td>
                      </tr>
                      <tr>
                        <td style={{textAlign:'left'}}>Part 7: <FormattedMessage id={"part7.fulltest"}/></td>
                        <td style={{textAlign:'right'}}>{listQuestionAndAnswerMinitest[6] ? listQuestionAndAnswerMinitest[6].totalCorectAnswer : 0}/{listQuestionAndAnswerMinitest[6] ? listQuestionAndAnswerMinitest[6].totalQuestion : 0}</td>
                      </tr>

                      </tbody>
                    </table>
                  </Row>
                </Col>
              </Row>
            </Col>
          }
          {this.state.waitClosePopup && !this.props.viewHistory ?
              showAnswer === false && !this.props.viewHistory  ? <>{this.modalConfirmDoTest()}</> : <Row>
                <Col xs="9" md="9" sm="9" lg="9" className="overflow">
                  <div>
                    <ListeningTest stopListening={this.state.keepDoTest2} />
                    {this.props.doReadingFulltest || this.props.viewHistory ?
                      <ReadingTest/>
                      :
                      null
                    }
                    {/*---------------------------modal-------------------------*/}
                    {showAnswer === false && !this.props.viewHistory ? <>{this.modalConfirmDoTest()}</> : null}
                    {showAnswer === false || !this.props.viewHistory ? <>{this.modalConfirmSubmit()}</> : null}
                    {this.state.minute === 4 && this.state.hours === 0  ?
                      <>{this.modalRemainder()}</> : null}

                    {showAnswer === false || !this.props.viewHistory ?
                      <>
                        {this.state.hours === 0 && this.state.minute === 0 && this.state.second === 0 ?
                          <>{
                          this.modalAutoTimeIsUp()
                        }</>
                          : null
                        }
                      </> : null
                    }

                    {showAnswer === false ? this.modalTimeIsUp() : null}

                    {/*---------------------------end modal-------------------------*/}

                    {showAnswer === false && !this.props.viewHistory ?
                      <Col xs="12" md="12" sm="12" lg="12" className={classes.divSubmit}>
                        <button id="btnSubmit" className="btn btn-primary" style={{fontWeight: '700', fontSize:'1.5rem'}}
                          // onClick={this.submitAnswer}
                                onClick={() => this.toggleConfirmSubmit()}
                        >
                          {/*Nộp bài*/}
                          <FormattedMessage id={"submit"}/>
                        </button>
                      </Col> : null
                    }
                  </div>
                </Col>

                <Col xs="3" md="3" sm="3" lg="3">
                  <div className="sticky-top" style={{zIndex:'0'}}>

                    {showAnswer === false  && !this.props.viewHistory ?
                      <div style={{display: 'flex', justifyContent: 'center'}}>
                        {/*{this.state.load === true ? <>{this.loadingComponent}</> : ""}*/}

                        <AccessTimeIcon style={{fontSize: 60, color: 'gray'}}/>
                        <div style={{padding: '6px 0 0 0px', textAlign: 'center'}}>
                          <h4 style={{color: 'gray', fontWeight: '700'}}><FormattedMessage id={"time.remaining"}/></h4>
                          <h3 style={{color: 'orange', fontWeight: '700'}}>
                            {loadingQuestion === false ?
                              <>{this.state.minute < 10 ? 0 : null}{this.state.minute}:{this.state.second < 10 ? 0 : null}{this.state.second}</>
                              :
                              <>{this.setCountdown()}</>
                            }
                          </h3>
                        </div>
                      </div> : null
                    }

                    {
                      this.props.viewHistory ?
                        <div id="scrollQuestion">
                          {listHistoryFullTest.length > 0 ?
                            <>
                              {/*----------------------PART 1-8 ------------------------*/}
                              {listHistoryFullTest.map((quesMinitest, index) =>
                                <Col xs="12" md="12" sm="12" lg="12" style={{marginTop: '5%'}}>
                                  <h6  style={{color: '#169BD5', marginLeft: '5%', fontWeight: '700'}}
                                       onClick={() => this.linkToElement(String(quesMinitest.partName))}>
                                    <u style={{fontSize:'1.3rem'}}>Part {index + 1}:</u>
                                  </h6>
                                  <Row style={{textAlign: 'center', border: '1px solid #1da9ed', paddingLeft: '9%', margin:'0'}}>
                                    {quesMinitest.listCategoryMinitestDTOS.map((category, indexCategory) =>
                                      <>
                                        {category.listQuestionMinitestDTOS.map((question, indexQuestion) =>
                                          <>
                                            {question.indexIncorrectAnswer === -1 ?
                                              <Col xs="12" md="6" sm="12" lg="4" style={{textAlign: 'center'}}>
                                                <div onClick={() => this.linkToElement(String(question.sentenceNo))}
                                                     style={{
                                                       border: '1px solid gray',
                                                       width: '30px',
                                                       height: '30px',
                                                       borderRadius: '50%',
                                                       marginBottom: '5px',
                                                       marginTop: '5px',
                                                       position: 'relative',
                                                       backgroundColor: 'green',
                                                       color: 'white',
                                                       cursor: 'pointer'
                                                     }}>
                                                  <div style={{
                                                    position: 'absolute',
                                                    transform: 'translate(-50%, -50%)',
                                                    top: '50%',
                                                    marginLeft: '50%',
                                                    cursor: 'pointer'
                                                  }}>
                                                    {question.sentenceNo}
                                                  </div>
                                                </div>
                                              </Col>
                                              :
                                              <Col xs="12" md="6" sm="12" lg="4" style={{textAlign: 'center'}}>
                                                <div onClick={() => this.linkToElement(String(question.sentenceNo))}
                                                     style={{
                                                       border: '1px solid gray',
                                                       width: '30px',
                                                       height: '30px',
                                                       borderRadius: '50%',
                                                       marginBottom: '5px',
                                                       marginTop: '5px',
                                                       position: 'relative',
                                                       backgroundColor: 'red',
                                                       color: 'white',
                                                       cursor: 'pointer'
                                                     }}>
                                                  <div style={{
                                                    position: 'absolute',
                                                    transform: 'translate(-50%, -50%)',
                                                    top: '50%',
                                                    marginLeft: '50%',
                                                    cursor: 'pointer'
                                                  }}>
                                                    {question.sentenceNo}
                                                  </div>
                                                </div>
                                              </Col>
                                            }
                                          </>
                                        )}
                                      </>
                                    )}
                                  </Row>
                                </Col>
                              )}
                              {/*----------------------End PART 1-8 ------------------------*/}
                            </>
                            : null
                          }
                        </div>
                        :
                        showAnswer === false  ?
                          <div id="scrollQuestion">
                            {/*{loadingQuestion !== undefined ?*/}
                            {/*<>{loadingQuestion === false || !loadingSubmitAnswer ? <>{this.loadingComponent}</> : ""}</> : null*/}
                            {/*}*/}
                            {listQuestionMinitest.length > 0 ?
                              <>
                                <fieldset className={classes.fieldsetListQuestion}>
                                  <legend className={classes.legendListQuestion}><FormattedMessage id={"list.of.questions"}/></legend>
                                  {/*----------------------PART 1 ------------------------*/}
                                  <Col xs="12" md="12" sm="12" lg="12" style={{marginTop: '5%'}}>
                                    <h6 style={{color: '#169BD5', fontWeight: '700'}}
                                        onClick={() => this.linkToElement(String(listQuestionMinitest[0].partName))}>
                                      <u style={{fontSize:'1.3rem'}}>Part 1:</u>
                                    </h6>
                                    <Row style={{textAlign: 'center', border: '1px solid #1da9ed', margin: '0', paddingLeft: '9%'}}>
                                      {listQuestionMinitest[0].listCategoryMinitestDTOS.map((category, indexCategory) =>
                                        <>
                                          {category.listQuestionMinitestDTOS.map((question, indexQuestion) =>
                                            <>
                                              {listQuestionListeningChoosenDTOS.length > 0 ?
                                                <>
                                                  {undefined !== listQuestionListeningChoosenDTOS[question.sentenceNo - 1] ?
                                                    listQuestionListeningChoosenDTOS[question.sentenceNo - 1].answerChoosen !== "null" ?
                                                      <Col xs="12" md="6" sm="12" lg="4">
                                                        <div onClick={() => this.linkToElement(String(question.sentenceNo))}
                                                             style={{
                                                               border: '1px solid black',
                                                               width: '30px',
                                                               height: '30px',
                                                               borderRadius: '50%',
                                                               marginBottom: '5px',
                                                               marginTop: '5px',
                                                               position: 'relative',
                                                               backgroundColor: '#72716a',
                                                               color: 'white',
                                                               cursor: 'pointer'
                                                             }}>
                                                          <div style={{
                                                            position: 'absolute',
                                                            transform: 'translate(-50%, -50%)',
                                                            top: '50%',
                                                            marginLeft: '50%',
                                                            cursor: 'pointer'
                                                          }}>
                                                            {question.sentenceNo}
                                                          </div>
                                                        </div>
                                                      </Col>
                                                      :
                                                      <Col xs="12" md="6" sm="12" lg="4">
                                                        <div onClick={() => this.linkToElement(String(question.sentenceNo))}
                                                             style={{
                                                               border: '1px solid black',
                                                               width: '30px',
                                                               height: '30px',
                                                               borderRadius: '50%',
                                                               marginBottom: '5px',
                                                               marginTop: '5px',
                                                               position: 'relative',
                                                               cursor: 'pointer'
                                                             }}>
                                                          <div style={{
                                                            position: 'absolute',
                                                            transform: 'translate(-50%, -50%)',
                                                            top: '50%',
                                                            marginLeft: '50%',
                                                            cursor: 'pointer'
                                                          }}>
                                                            {question.sentenceNo}
                                                          </div>
                                                        </div>
                                                      </Col> : null
                                                  }
                                                </>
                                                : <Col xs="12" md="6" sm="12" lg="4">
                                                  <div onClick={() => this.linkToElement(String(question.sentenceNo))}
                                                       style={{
                                                         border: '1px solid black',
                                                         width: '30px',
                                                         height: '30px',
                                                         borderRadius: '50%',
                                                         marginBottom: '5px',
                                                         marginTop: '5px',
                                                         position: 'relative',
                                                         cursor: 'pointer'
                                                       }}>
                                                    <div style={{
                                                      position: 'absolute',
                                                      transform: 'translate(-50%, -50%)',
                                                      top: '50%',
                                                      marginLeft: '50%',
                                                      cursor: 'pointer'
                                                    }}>
                                                      {question.sentenceNo}
                                                    </div>
                                                  </div>
                                                </Col>
                                              }

                                            </>
                                          )}
                                        </>
                                      )}
                                    </Row>
                                  </Col>
                                  {/*----------------------End PART 1 ------------------------*/}

                                  {/*----------------------PART 2 ------------------------*/}
                                  <Col xs="12" md="12" sm="12" lg="12" style={{marginTop: '5%'}}>
                                    <h6 style={{color: '#169BD5', fontWeight: '700'}}
                                        onClick={() => this.linkToElement(String(listQuestionMinitest[1].partName))}>
                                      <u style={{fontSize:'1.3rem'}}>Part 2:</u>
                                    </h6>
                                    <Row style={{textAlign: 'center', border: '1px solid #1da9ed', margin: '0', paddingLeft: '9%'}}>
                                      {listQuestionMinitest[1].listCategoryMinitestDTOS.map((category, indexCategory) =>
                                        <>
                                          {category.listQuestionMinitestDTOS.map((question, indexQuestion) =>
                                            <>
                                              {listQuestionListeningChoosenDTOS.length > 0 ?
                                                <>
                                                  {undefined !== listQuestionListeningChoosenDTOS[question.sentenceNo - 1] ?
                                                    listQuestionListeningChoosenDTOS[question.sentenceNo - 1].answerChoosen !== "null" ?
                                                      <Col xs="12" md="6" sm="12" lg="4" >
                                                        <div onClick={() => this.linkToElement(String(question.sentenceNo))}
                                                             style={{
                                                               border: '1px solid black',
                                                               width: '30px',
                                                               height: '30px',
                                                               borderRadius: '50%',
                                                               marginBottom: '5px',
                                                               marginTop: '5px',
                                                               position: 'relative',
                                                               backgroundColor: '#72716a',
                                                               color: 'white',
                                                               cursor: 'pointer'
                                                             }}>
                                                          <div style={{
                                                            position: 'absolute',
                                                            transform: 'translate(-50%, -50%)',
                                                            top: '50%',
                                                            marginLeft: '50%',
                                                            cursor: 'pointer'
                                                          }}>
                                                            {question.sentenceNo}
                                                          </div>
                                                        </div>
                                                      </Col>
                                                      :
                                                      <Col xs="12" md="6" sm="12" lg="4">
                                                        <div onClick={() => this.linkToElement(String(question.sentenceNo))}
                                                             style={{
                                                               border: '1px solid black',
                                                               width: '30px',
                                                               height: '30px',
                                                               borderRadius: '50%',
                                                               marginBottom: '5px',
                                                               marginTop: '5px',
                                                               position: 'relative',
                                                               cursor: 'pointer'
                                                             }}>
                                                          <div style={{
                                                            position: 'absolute',
                                                            transform: 'translate(-50%, -50%)',
                                                            top: '50%',
                                                            marginLeft: '50%',
                                                            cursor: 'pointer'
                                                          }}>
                                                            {question.sentenceNo}
                                                          </div>
                                                        </div>
                                                      </Col> : null
                                                  }
                                                </>
                                                : <Col xs="12" md="6" sm="12" lg="4">
                                                  <div onClick={() => this.linkToElement(String(question.sentenceNo))}
                                                       style={{
                                                         border: '1px solid black',
                                                         width: '30px',
                                                         height: '30px',
                                                         borderRadius: '50%',
                                                         marginBottom: '5px',
                                                         marginTop: '5px',
                                                         position: 'relative',
                                                         cursor: 'pointer'
                                                       }}>
                                                    <div style={{
                                                      position: 'absolute',
                                                      transform: 'translate(-50%, -50%)',
                                                      top: '50%',
                                                      marginLeft: '50%',
                                                      cursor: 'pointer'
                                                    }}>
                                                      {question.sentenceNo}
                                                    </div>
                                                  </div>
                                                </Col>
                                              }

                                            </>
                                          )}
                                        </>
                                      )}
                                    </Row>
                                  </Col>
                                  {/*----------------------End PART 2 ------------------------*/}

                                  {/*----------------------PART 3 ------------------------*/}
                                  <Col xs="12" md="12" sm="12" lg="12" style={{marginTop: '5%'}}>
                                    <h6 style={{color: '#169BD5', fontWeight: '700'}}
                                        onClick={() => this.linkToElement(String(listQuestionMinitest[2].partName))}>
                                      <u style={{fontSize:'1.3rem'}}>Part 3:</u>
                                    </h6>
                                    <Row style={{textAlign: 'center', border: '1px solid #1da9ed', margin: '0', paddingLeft: '9%'}}>
                                      {listQuestionMinitest[2].listCategoryMinitestDTOS.map((category, indexCategory) =>
                                        <>
                                          {category.listQuestionMinitestDTOS.map((question, indexQuestion) =>
                                            <>
                                              {listQuestionListeningChoosenDTOS.length > 0 ?
                                                <>
                                                  {undefined !== listQuestionListeningChoosenDTOS[question.sentenceNo - 1] ?
                                                    listQuestionListeningChoosenDTOS[question.sentenceNo - 1].answerChoosen !== "null" ?
                                                      <Col xs="12" md="6" sm="12" lg="4" >
                                                        <div onClick={() => this.linkToElement(String(question.sentenceNo))}
                                                             style={{
                                                               border: '1px solid black',
                                                               width: '30px',
                                                               height: '30px',
                                                               borderRadius: '50%',
                                                               marginBottom: '5px',
                                                               marginTop: '5px',
                                                               position: 'relative',
                                                               backgroundColor: '#72716a',
                                                               color: 'white',
                                                               cursor: 'pointer'
                                                             }}>
                                                          <div style={{
                                                            position: 'absolute',
                                                            transform: 'translate(-50%, -50%)',
                                                            top: '50%',
                                                            marginLeft: '50%',
                                                            cursor: 'pointer'
                                                          }}>
                                                            {question.sentenceNo}
                                                          </div>
                                                        </div>
                                                      </Col>
                                                      :
                                                      <Col xs="12" md="6" sm="12" lg="4">
                                                        <div onClick={() => this.linkToElement(String(question.sentenceNo))}
                                                             style={{
                                                               border: '1px solid black',
                                                               width: '30px',
                                                               height: '30px',
                                                               borderRadius: '50%',
                                                               marginBottom: '5px',
                                                               marginTop: '5px',
                                                               position: 'relative',
                                                               cursor: 'pointer'
                                                             }}>
                                                          <div style={{
                                                            position: 'absolute',
                                                            transform: 'translate(-50%, -50%)',
                                                            top: '50%',
                                                            marginLeft: '50%',
                                                            cursor: 'pointer'
                                                          }}>
                                                            {question.sentenceNo}
                                                          </div>
                                                        </div>
                                                      </Col> : null
                                                  }
                                                </>
                                                : <Col xs="12" md="6" sm="12" lg="4">
                                                  <div onClick={() => this.linkToElement(String(question.sentenceNo))}
                                                       style={{
                                                         border: '1px solid black',
                                                         width: '30px',
                                                         height: '30px',
                                                         borderRadius: '50%',
                                                         marginBottom: '5px',
                                                         marginTop: '5px',
                                                         position: 'relative',
                                                         cursor: 'pointer'
                                                       }}>
                                                    <div style={{
                                                      position: 'absolute',
                                                      transform: 'translate(-50%, -50%)',
                                                      top: '50%',
                                                      marginLeft: '50%',
                                                      cursor: 'pointer'
                                                    }}>
                                                      {question.sentenceNo}
                                                    </div>
                                                  </div>
                                                </Col>
                                              }

                                            </>
                                          )}
                                        </>
                                      )}
                                    </Row>
                                  </Col>
                                  {/*----------------------End PART 3 ------------------------*/}

                                  {/*----------------------PART 4 ------------------------*/}
                                  <Col xs="12" md="12" sm="12" lg="12" style={{marginTop: '5%'}}>
                                    <h6 style={{color: '#169BD5', fontWeight: '700'}}
                                        onClick={() => this.linkToElement(String(listQuestionMinitest[3].partName))}>
                                      <u style={{fontSize:'1.3rem'}}>Part 4:</u>
                                    </h6>
                                    <Row style={{textAlign: 'center', border: '1px solid #1da9ed', margin: '0', paddingLeft: '9%'}}>
                                      {listQuestionMinitest[3].listCategoryMinitestDTOS.map((category, indexCategory) =>
                                        <>
                                          {category.listQuestionMinitestDTOS.map((question, indexQuestion) =>
                                            <>
                                              {listQuestionListeningChoosenDTOS.length > 0 ?
                                                <>
                                                  {undefined !== listQuestionListeningChoosenDTOS[question.sentenceNo - 1] ?
                                                    listQuestionListeningChoosenDTOS[question.sentenceNo - 1].answerChoosen !== "null" ?
                                                      <Col xs="12" md="6" sm="12" lg="4" >
                                                        <div onClick={() => this.linkToElement(String(question.sentenceNo))}
                                                             style={{
                                                               border: '1px solid black',
                                                               width: '30px',
                                                               height: '30px',
                                                               borderRadius: '50%',
                                                               marginBottom: '5px',
                                                               marginTop: '5px',
                                                               position: 'relative',
                                                               backgroundColor: '#72716a',
                                                               color: 'white',
                                                               cursor: 'pointer'
                                                             }}>
                                                          <div style={{
                                                            position: 'absolute',
                                                            transform: 'translate(-50%, -50%)',
                                                            top: '50%',
                                                            marginLeft: '50%',
                                                            cursor: 'pointer'
                                                          }}>
                                                            {question.sentenceNo}
                                                          </div>
                                                        </div>
                                                      </Col>
                                                      :
                                                      <Col xs="12" md="6" sm="12" lg="4">
                                                        <div onClick={() => this.linkToElement(String(question.sentenceNo))}
                                                             style={{
                                                               border: '1px solid black',
                                                               width: '30px',
                                                               height: '30px',
                                                               borderRadius: '50%',
                                                               marginBottom: '5px',
                                                               marginTop: '5px',
                                                               position: 'relative',
                                                               cursor: 'pointer'
                                                             }}>
                                                          <div style={{
                                                            position: 'absolute',
                                                            transform: 'translate(-50%, -50%)',
                                                            top: '50%',
                                                            marginLeft: '50%',
                                                            cursor: 'pointer'
                                                          }}>
                                                            {question.sentenceNo}
                                                          </div>
                                                        </div>
                                                      </Col> : null
                                                  }
                                                </>
                                                : <Col xs="12" md="6" sm="12" lg="4">
                                                  <div onClick={() => this.linkToElement(String(question.sentenceNo))}
                                                       style={{
                                                         border: '1px solid black',
                                                         width: '30px',
                                                         height: '30px',
                                                         borderRadius: '50%',
                                                         marginBottom: '5px',
                                                         marginTop: '5px',
                                                         position: 'relative',
                                                         cursor: 'pointer'
                                                       }}>
                                                    <div style={{
                                                      position: 'absolute',
                                                      transform: 'translate(-50%, -50%)',
                                                      top: '50%',
                                                      marginLeft: '50%',
                                                      cursor: 'pointer'
                                                    }}>
                                                      {question.sentenceNo}
                                                    </div>
                                                  </div>
                                                </Col>
                                              }

                                            </>
                                          )}
                                        </>
                                      )}
                                    </Row>
                                  </Col>
                                  {/*----------------------End PART 4 ------------------------*/}

                                  {this.props.doReadingFulltest ?
                                    <>
                                      {/*----------------------PART 5 ------------------------*/}
                                      <Col xs="12" md="12" sm="12" lg="12" style={{marginTop: '5%'}}>
                                        <h6 style={{color: '#169BD5', fontWeight: '700'}}
                                            onClick={() => this.linkToElement(String(listQuestionMinitest[4].partName))}>
                                          <u style={{fontSize:'1.3rem'}}>Part 5:</u>
                                        </h6>
                                        <Row style={{textAlign: 'center', border: '1px solid #1da9ed', margin: '0', paddingLeft: '9%'}}>
                                          {listQuestionMinitest[4].listCategoryMinitestDTOS.map((category, indexCategory) =>
                                            <>
                                              {category.listQuestionMinitestDTOS.map((question, indexQuestion) =>
                                                <>
                                                  {listQuestionReadingChoosenDTOS.length > 0 ?
                                                    <>
                                                      {undefined !== listQuestionReadingChoosenDTOS[question.sentenceNo-1-sizeOfListeningQues] ?
                                                        listQuestionReadingChoosenDTOS[question.sentenceNo-1-sizeOfListeningQues].answerChoosen !== "null" ?
                                                          <Col xs="12" md="6" sm="12" lg="4" >
                                                            <div onClick={() => this.linkToElement(String(question.sentenceNo))}
                                                                 style={{
                                                                   border: '1px solid black',
                                                                   width: '30px',
                                                                   height: '30px',
                                                                   borderRadius: '50%',
                                                                   marginBottom: '5px',
                                                                   marginTop: '5px',
                                                                   position: 'relative',
                                                                   backgroundColor: '#72716a',
                                                                   color: 'white',
                                                                   cursor: 'pointer'
                                                                 }}>
                                                              <div style={{
                                                                position: 'absolute',
                                                                transform: 'translate(-50%, -50%)',
                                                                top: '50%',
                                                                marginLeft: '50%',
                                                                cursor: 'pointer'
                                                              }}>
                                                                {question.sentenceNo}
                                                              </div>
                                                            </div>
                                                          </Col>
                                                          :
                                                          <Col xs="12" md="6" sm="12" lg="4">
                                                            <div onClick={() => this.linkToElement(String(question.sentenceNo))}
                                                                 style={{
                                                                   border: '1px solid black',
                                                                   width: '30px',
                                                                   height: '30px',
                                                                   borderRadius: '50%',
                                                                   marginBottom: '5px',
                                                                   marginTop: '5px',
                                                                   position: 'relative',
                                                                   cursor: 'pointer'
                                                                 }}>
                                                              <div style={{
                                                                position: 'absolute',
                                                                transform: 'translate(-50%, -50%)',
                                                                top: '50%',
                                                                marginLeft: '50%',
                                                                cursor: 'pointer'
                                                              }}>
                                                                {question.sentenceNo}
                                                              </div>
                                                            </div>
                                                          </Col> : null
                                                      }
                                                    </>
                                                    : <Col xs="12" md="6" sm="12" lg="4">
                                                      <div onClick={() => this.linkToElement(String(question.sentenceNo))}
                                                           style={{
                                                             border: '1px solid black',
                                                             width: '30px',
                                                             height: '30px',
                                                             borderRadius: '50%',
                                                             marginBottom: '5px',
                                                             marginTop: '5px',
                                                             position: 'relative',
                                                             cursor: 'pointer'
                                                           }}>
                                                        <div style={{
                                                          position: 'absolute',
                                                          transform: 'translate(-50%, -50%)',
                                                          top: '50%',
                                                          marginLeft: '50%',
                                                          cursor: 'pointer'
                                                        }}>
                                                          {question.sentenceNo}
                                                        </div>
                                                      </div>
                                                    </Col>
                                                  }

                                                </>
                                              )}
                                            </>
                                          )}
                                        </Row>
                                      </Col>
                                      {/*----------------------End PART 5 ------------------------*/}

                                      {/*----------------------PART 6 ------------------------*/}
                                      <Col xs="12" md="12" sm="12" lg="12" style={{marginTop: '5%'}}>
                                        <h6 style={{color: '#169BD5', fontWeight: '700'}}
                                            onClick={() => this.linkToElement(String(listQuestionMinitest[5].partName))}>
                                          <u style={{fontSize:'1.3rem'}}>Part 6:</u>
                                        </h6>
                                        <Row style={{textAlign: 'center', border: '1px solid #1da9ed', margin: '0', paddingLeft: '9%'}}>
                                          {listQuestionMinitest[5].listCategoryMinitestDTOS.map((category, indexCategory) =>
                                            <>
                                              {category.listQuestionMinitestDTOS.map((question, indexQuestion) =>
                                                <>
                                                  {listQuestionReadingChoosenDTOS.length > 0 ?
                                                    <>
                                                      {undefined !== listQuestionReadingChoosenDTOS[question.sentenceNo-1-sizeOfListeningQues] ?
                                                        listQuestionReadingChoosenDTOS[question.sentenceNo-1-sizeOfListeningQues].answerChoosen !== "null" ?
                                                          <Col xs="12" md="6" sm="12" lg="4" >
                                                            <div onClick={() => this.linkToElement(String(question.sentenceNo))}
                                                                 style={{
                                                                   border: '1px solid black',
                                                                   width: '30px',
                                                                   height: '30px',
                                                                   borderRadius: '50%',
                                                                   marginBottom: '5px',
                                                                   marginTop: '5px',
                                                                   position: 'relative',
                                                                   backgroundColor: '#72716a',
                                                                   color: 'white',
                                                                   cursor: 'pointer'
                                                                 }}>
                                                              <div style={{
                                                                position: 'absolute',
                                                                transform: 'translate(-50%, -50%)',
                                                                top: '50%',
                                                                marginLeft: '50%',
                                                                cursor: 'pointer'
                                                              }}>
                                                                {question.sentenceNo}
                                                              </div>
                                                            </div>
                                                          </Col>
                                                          :
                                                          <Col xs="12" md="6" sm="12" lg="4">
                                                            <div onClick={() => this.linkToElement(String(question.sentenceNo))}
                                                                 style={{
                                                                   border: '1px solid black',
                                                                   width: '30px',
                                                                   height: '30px',
                                                                   borderRadius: '50%',
                                                                   marginBottom: '5px',
                                                                   marginTop: '5px',
                                                                   position: 'relative',
                                                                   cursor: 'pointer'
                                                                 }}>
                                                              <div style={{
                                                                position: 'absolute',
                                                                transform: 'translate(-50%, -50%)',
                                                                top: '50%',
                                                                marginLeft: '50%',
                                                                cursor: 'pointer'
                                                              }}>
                                                                {question.sentenceNo}
                                                              </div>
                                                            </div>
                                                          </Col> : null
                                                      }
                                                    </>
                                                    : <Col xs="12" md="6" sm="12" lg="4">
                                                      <div onClick={() => this.linkToElement(String(question.sentenceNo))}
                                                           style={{
                                                             border: '1px solid black',
                                                             width: '30px',
                                                             height: '30px',
                                                             borderRadius: '50%',
                                                             marginBottom: '5px',
                                                             marginTop: '5px',
                                                             position: 'relative',
                                                             cursor: 'pointer'
                                                           }}>
                                                        <div style={{
                                                          position: 'absolute',
                                                          transform: 'translate(-50%, -50%)',
                                                          top: '50%',
                                                          marginLeft: '50%',
                                                          cursor: 'pointer'
                                                        }}>
                                                          {question.sentenceNo}
                                                        </div>
                                                      </div>
                                                    </Col>
                                                  }

                                                </>
                                              )}
                                            </>
                                          )}
                                        </Row>
                                      </Col>
                                      {/*----------------------End PART 6 ------------------------*/}

                                      {/*----------------------PART 7 ------------------------*/}
                                      <Col xs="12" md="12" sm="12" lg="12" style={{marginTop: '5%'}}>
                                        <h6 style={{color: '#169BD5', fontWeight: '700'}}
                                            onClick={() => this.linkToElement(String(listQuestionMinitest[6].partName))}>
                                          <u style={{fontSize:'1.3rem'}}>Part 7:</u>
                                        </h6>
                                        <Row style={{textAlign: 'center', border: '1px solid #1da9ed', margin: '0', paddingLeft: '9%'}}>
                                          {listQuestionMinitest[6].listCategoryMinitestDTOS.map((category, indexCategory) =>
                                            <>
                                              {category.listQuestionMinitestDTOS.map((question, indexQuestion) =>
                                                <>
                                                  {listQuestionReadingChoosenDTOS.length > 0 ?
                                                    <>
                                                      {undefined !== listQuestionReadingChoosenDTOS[question.sentenceNo-1-sizeOfListeningQues] ?
                                                        listQuestionReadingChoosenDTOS[question.sentenceNo-1-sizeOfListeningQues].answerChoosen !== "null" ?
                                                          <Col xs="12" md="6" sm="12" lg="4" >
                                                            <div onClick={() => this.linkToElement(String(question.sentenceNo))}
                                                                 style={{
                                                                   border: '1px solid black',
                                                                   width: '30px',
                                                                   height: '30px',
                                                                   borderRadius: '50%',
                                                                   marginBottom: '5px',
                                                                   marginTop: '5px',
                                                                   position: 'relative',
                                                                   backgroundColor: '#72716a',
                                                                   color: 'white',
                                                                   cursor: 'pointer'
                                                                 }}>
                                                              <div style={{
                                                                position: 'absolute',
                                                                transform: 'translate(-50%, -50%)',
                                                                top: '50%',
                                                                marginLeft: '50%',
                                                                cursor: 'pointer'
                                                              }}>
                                                                {question.sentenceNo}
                                                              </div>
                                                            </div>
                                                          </Col>
                                                          :
                                                          <Col xs="12" md="6" sm="12" lg="4">
                                                            <div onClick={() => this.linkToElement(String(question.sentenceNo))}
                                                                 style={{
                                                                   border: '1px solid black',
                                                                   width: '30px',
                                                                   height: '30px',
                                                                   borderRadius: '50%',
                                                                   marginBottom: '5px',
                                                                   marginTop: '5px',
                                                                   position: 'relative',
                                                                   cursor: 'pointer'
                                                                 }}>
                                                              <div style={{
                                                                position: 'absolute',
                                                                transform: 'translate(-50%, -50%)',
                                                                top: '50%',
                                                                marginLeft: '50%',
                                                                cursor: 'pointer'
                                                              }}>
                                                                {question.sentenceNo}
                                                              </div>
                                                            </div>
                                                          </Col> : null
                                                      }
                                                    </>
                                                    : <Col xs="12" md="6" sm="12" lg="4">
                                                      <div onClick={() => this.linkToElement(String(question.sentenceNo))}
                                                           style={{
                                                             border: '1px solid black',
                                                             width: '30px',
                                                             height: '30px',
                                                             borderRadius: '50%',
                                                             marginBottom: '5px',
                                                             marginTop: '5px',
                                                             position: 'relative',
                                                             cursor: 'pointer'
                                                           }}>
                                                        <div style={{
                                                          position: 'absolute',
                                                          transform: 'translate(-50%, -50%)',
                                                          top: '50%',
                                                          marginLeft: '50%',
                                                          cursor: 'pointer'
                                                        }}>
                                                          {question.sentenceNo}
                                                        </div>
                                                      </div>
                                                    </Col>
                                                  }

                                                </>
                                              )}
                                            </>
                                          )}
                                        </Row>
                                      </Col>
                                      {/*----------------------End PART 7 ------------------------*/}
                                    </> : null
                                  }

                                </fieldset>
                              </> : null
                            }
                          </div>
                          :
                          <div id="scrollQuestion">
                            {/*{loadingQuestion !== undefined ?*/}
                            {/*<>{loadingQuestion === false ? <>{this.loadingComponent}</> : ""}</> : null*/}
                            {/*}*/}
                            {listQuestionAndAnswerMinitest.length > 0 ?
                              <>
                                {/*----------------------PART 1-8 ------------------------*/}
                                {listQuestionAndAnswerMinitest.map((quesMinitest, index) =>
                                  <Col xs="12" md="12" sm="12" lg="12" style={{marginTop: '5%'}}>
                                    <h6  style={{color: '#169BD5', marginLeft: '5%', fontWeight: '700'}}
                                         onClick={() => this.linkToElement(String(quesMinitest.partName))}>
                                      <u style={{fontSize:'1.3rem'}}>Part {index + 1}:</u>
                                    </h6>
                                    <Row style={{textAlign: 'center', border: '1px solid #1da9ed', paddingLeft: '9%', margin:'0'}}>
                                      {quesMinitest.listCategoryMinitestDTOS.map((category, indexCategory) =>
                                        <>
                                          {category.listQuestionMinitestDTOS.map((question, indexQuestion) =>
                                            <>
                                              {question.indexIncorrectAnswer === -1 ?
                                                <Col xs="12" md="6" sm="12" lg="4" style={{textAlign: 'center'}}>
                                                  <div onClick={() => this.linkToElement(String(question.sentenceNo))}
                                                       style={{
                                                         border: '1px solid gray',
                                                         width: '30px',
                                                         height: '30px',
                                                         borderRadius: '50%',
                                                         marginBottom: '5px',
                                                         marginTop: '5px',
                                                         position: 'relative',
                                                         backgroundColor: 'green',
                                                         color: 'white',
                                                         cursor: 'pointer'
                                                       }}>
                                                    <div style={{
                                                      position: 'absolute',
                                                      transform: 'translate(-50%, -50%)',
                                                      top: '50%',
                                                      marginLeft: '50%',
                                                      cursor: 'pointer'
                                                    }}>
                                                      {question.sentenceNo}
                                                    </div>
                                                  </div>
                                                </Col>
                                                :
                                                <Col xs="12" md="6" sm="12" lg="4" style={{textAlign: 'center'}}>
                                                  <div onClick={() => this.linkToElement(String(question.sentenceNo))}
                                                       style={{
                                                         border: '1px solid gray',
                                                         width: '30px',
                                                         height: '30px',
                                                         borderRadius: '50%',
                                                         marginBottom: '5px',
                                                         marginTop: '5px',
                                                         position: 'relative',
                                                         backgroundColor: 'red',
                                                         color: 'white',
                                                         cursor: 'pointer'
                                                       }}>
                                                    <div style={{
                                                      position: 'absolute',
                                                      transform: 'translate(-50%, -50%)',
                                                      top: '50%',
                                                      marginLeft: '50%',
                                                      cursor: 'pointer'
                                                    }}>
                                                      {question.sentenceNo}
                                                    </div>
                                                  </div>
                                                </Col>
                                              }
                                            </>
                                          )}
                                        </>
                                      )}
                                    </Row>
                                  </Col>
                                )}
                                {/*----------------------End PART 1-8 ------------------------*/}
                              </>
                              : null
                            }
                          </div>

                    }

                  </div>

                </Col>
              </Row>
            :
            <Row>
              <Col xs="9" md="9" sm="9" lg="9" className="overflow">
                <div>
                  <ListeningTest stopListening={this.state.keepDoTest2}/>
                  {this.props.doReadingFulltest || this.props.viewHistory ?
                    <ReadingTest/>
                    :
                    null
                  }
                  {/*---------------------------modal-------------------------*/}
                  {showAnswer === false && !this.props.viewHistory ? <>{this.modalConfirmDoTest()}</> : null}
                  {showAnswer === false || !this.props.viewHistory ? <>{this.modalConfirmSubmit()}</> : null}
                  {this.state.minute === 4 && this.state.hours === 0  ?
                    <>{this.modalRemainder()}</> : null}

                  {showAnswer === false || !this.props.viewHistory ?
                    <>
                      {this.state.hours === 0 && this.state.minute === 0 && this.state.second === 0 ?
                        <>{this.modalAutoTimeIsUp(true)}</>
                        : null
                      }
                    </> : null
                  }

                  {showAnswer === false ? this.modalTimeIsUp() : null}

                  {/*---------------------------end modal-------------------------*/}

                  {showAnswer === false && !this.props.viewHistory ?
                    <Col xs="12" md="12" sm="12" lg="12" className={classes.divSubmit}>
                      <button id="btnSubmit" className="btn btn-primary" style={{fontWeight: '700', fontSize:'1.5rem'}}
                        // onClick={this.submitAnswer}
                              onClick={() => this.toggleConfirmSubmit()}
                      >
                        {/*Nộp bài*/}
                        <FormattedMessage id={"submit"}/>
                      </button>
                    </Col> : null
                  }
                </div>
              </Col>

              <Col xs="3" md="3" sm="3" lg="3">
                <div className="sticky-top" style={{zIndex:'0'}}>

                  {showAnswer === false  && !this.props.viewHistory ?
                    <div style={{display: 'flex', justifyContent: 'center'}}>
                      {/*{this.state.load === true ? <>{this.loadingComponent}</> : ""}*/}

                      <AccessTimeIcon style={{fontSize: 60, color: 'gray'}}/>
                      <div style={{padding: '6px 0 0 0px', textAlign: 'center'}}>
                        <h4 style={{color: 'gray', fontWeight: '700'}}><FormattedMessage id={"time.remaining"}/></h4>
                        <h3 style={{color: 'orange', fontWeight: '700'}}>
                          {loadingQuestion === false ?
                            <>{this.state.minute < 10 ? 0 : null}{this.state.minute}:{this.state.second < 10 ? 0 : null}{this.state.second}</>
                            :
                            <>{this.setCountdown()}</>
                          }
                        </h3>
                      </div>
                    </div> : null
                  }

                  {
                    this.props.viewHistory ?
                      <div id="scrollQuestion">
                        {listHistoryFullTest.length > 0 ?
                          <>
                            {/*----------------------PART 1-8 ------------------------*/}
                            {listHistoryFullTest.map((quesMinitest, index) =>
                              <Col xs="12" md="12" sm="12" lg="12" style={{marginTop: '5%'}}>
                                <h6  style={{color: '#169BD5', marginLeft: '5%', fontWeight: '700'}}
                                     onClick={() => this.linkToElement(String(quesMinitest.partName))}>
                                  <u style={{fontSize:'1.3rem'}}>Part {index + 1}:</u>
                                </h6>
                                <Row style={{textAlign: 'center', border: '1px solid #1da9ed', paddingLeft: '9%', margin:'0'}}>
                                  {quesMinitest.listCategoryMinitestDTOS.map((category, indexCategory) =>
                                    <>
                                      {category.listQuestionMinitestDTOS.map((question, indexQuestion) =>
                                        <>
                                          {question.indexIncorrectAnswer === -1 ?
                                            <Col xs="12" md="6" sm="12" lg="4" style={{textAlign: 'center'}}>
                                              <div onClick={() => this.linkToElement(String(question.sentenceNo))}
                                                   style={{
                                                     border: '1px solid gray',
                                                     width: '30px',
                                                     height: '30px',
                                                     borderRadius: '50%',
                                                     marginBottom: '5px',
                                                     marginTop: '5px',
                                                     position: 'relative',
                                                     backgroundColor: 'green',
                                                     color: 'white',
                                                     cursor: 'pointer'
                                                   }}>
                                                <div style={{
                                                  position: 'absolute',
                                                  transform: 'translate(-50%, -50%)',
                                                  top: '50%',
                                                  marginLeft: '50%',
                                                  cursor: 'pointer'
                                                }}>
                                                  {question.sentenceNo}
                                                </div>
                                              </div>
                                            </Col>
                                            :
                                            <Col xs="12" md="6" sm="12" lg="4" style={{textAlign: 'center'}}>
                                              <div onClick={() => this.linkToElement(String(question.sentenceNo))}
                                                   style={{
                                                     border: '1px solid gray',
                                                     width: '30px',
                                                     height: '30px',
                                                     borderRadius: '50%',
                                                     marginBottom: '5px',
                                                     marginTop: '5px',
                                                     position: 'relative',
                                                     backgroundColor: 'red',
                                                     color: 'white',
                                                     cursor: 'pointer'
                                                   }}>
                                                <div style={{
                                                  position: 'absolute',
                                                  transform: 'translate(-50%, -50%)',
                                                  top: '50%',
                                                  marginLeft: '50%',
                                                  cursor: 'pointer'
                                                }}>
                                                  {question.sentenceNo}
                                                </div>
                                              </div>
                                            </Col>
                                          }
                                        </>
                                      )}
                                    </>
                                  )}
                                </Row>
                              </Col>
                            )}
                            {/*----------------------End PART 1-8 ------------------------*/}
                          </>
                          : null
                        }
                      </div>
                      :
                    showAnswer === false  ?
                    <div id="scrollQuestion">
                      {/*{loadingQuestion !== undefined ?*/}
                      {/*<>{loadingQuestion === false || !loadingSubmitAnswer ? <>{this.loadingComponent}</> : ""}</> : null*/}
                      {/*}*/}
                      {listQuestionMinitest.length > 0 ?
                        <>
                          <fieldset className={classes.fieldsetListQuestion}>
                            <legend className={classes.legendListQuestion}><FormattedMessage id={"list.of.questions"}/></legend>
                            {/*----------------------PART 1 ------------------------*/}
                            <Col xs="12" md="12" sm="12" lg="12" style={{marginTop: '5%'}}>
                              <h6 style={{color: '#169BD5', fontWeight: '700'}}
                                  onClick={() => this.linkToElement(String(listQuestionMinitest[0].partName))}>
                                <u style={{fontSize:'1.3rem'}}>Part 1:</u>
                              </h6>
                              <Row style={{textAlign: 'center', border: '1px solid #1da9ed', margin: '0', paddingLeft: '9%'}}>
                                {listQuestionMinitest[0].listCategoryMinitestDTOS.map((category, indexCategory) =>
                                  <>
                                    {category.listQuestionMinitestDTOS.map((question, indexQuestion) =>
                                      <>
                                        {listQuestionListeningChoosenDTOS.length > 0 ?
                                          <>
                                            {undefined !== listQuestionListeningChoosenDTOS[question.sentenceNo - 1] ?
                                              listQuestionListeningChoosenDTOS[question.sentenceNo - 1].answerChoosen !== "null" ?
                                                <Col xs="12" md="6" sm="12" lg="4">
                                                  <div onClick={() => this.linkToElement(String(question.sentenceNo))}
                                                       style={{
                                                         border: '1px solid black',
                                                         width: '30px',
                                                         height: '30px',
                                                         borderRadius: '50%',
                                                         marginBottom: '5px',
                                                         marginTop: '5px',
                                                         position: 'relative',
                                                         backgroundColor: '#72716a',
                                                         color: 'white',
                                                         cursor: 'pointer'
                                                       }}>
                                                    <div style={{
                                                      position: 'absolute',
                                                      transform: 'translate(-50%, -50%)',
                                                      top: '50%',
                                                      marginLeft: '50%',
                                                      cursor: 'pointer'
                                                    }}>
                                                      {question.sentenceNo}
                                                    </div>
                                                  </div>
                                                </Col>
                                                :
                                                <Col xs="12" md="6" sm="12" lg="4">
                                                  <div onClick={() => this.linkToElement(String(question.sentenceNo))}
                                                       style={{
                                                         border: '1px solid black',
                                                         width: '30px',
                                                         height: '30px',
                                                         borderRadius: '50%',
                                                         marginBottom: '5px',
                                                         marginTop: '5px',
                                                         position: 'relative',
                                                         cursor: 'pointer'
                                                       }}>
                                                    <div style={{
                                                      position: 'absolute',
                                                      transform: 'translate(-50%, -50%)',
                                                      top: '50%',
                                                      marginLeft: '50%',
                                                      cursor: 'pointer'
                                                    }}>
                                                      {question.sentenceNo}
                                                    </div>
                                                  </div>
                                                </Col> : null
                                            }
                                          </>
                                          : <Col xs="12" md="6" sm="12" lg="4">
                                            <div onClick={() => this.linkToElement(String(question.sentenceNo))}
                                                 style={{
                                                   border: '1px solid black',
                                                   width: '30px',
                                                   height: '30px',
                                                   borderRadius: '50%',
                                                   marginBottom: '5px',
                                                   marginTop: '5px',
                                                   position: 'relative',
                                                   cursor: 'pointer'
                                                 }}>
                                              <div style={{
                                                position: 'absolute',
                                                transform: 'translate(-50%, -50%)',
                                                top: '50%',
                                                marginLeft: '50%',
                                                cursor: 'pointer'
                                              }}>
                                                {question.sentenceNo}
                                              </div>
                                            </div>
                                          </Col>
                                        }

                                      </>
                                    )}
                                  </>
                                )}
                              </Row>
                            </Col>
                            {/*----------------------End PART 1 ------------------------*/}

                            {/*----------------------PART 2 ------------------------*/}
                            <Col xs="12" md="12" sm="12" lg="12" style={{marginTop: '5%'}}>
                              <h6 style={{color: '#169BD5', fontWeight: '700'}}
                                  onClick={() => this.linkToElement(String(listQuestionMinitest[1].partName))}>
                                <u style={{fontSize:'1.3rem'}}>Part 2:</u>
                              </h6>
                              <Row style={{textAlign: 'center', border: '1px solid #1da9ed', margin: '0', paddingLeft: '9%'}}>
                                {listQuestionMinitest[1].listCategoryMinitestDTOS.map((category, indexCategory) =>
                                  <>
                                    {category.listQuestionMinitestDTOS.map((question, indexQuestion) =>
                                      <>
                                        {listQuestionListeningChoosenDTOS.length > 0 ?
                                          <>
                                            {undefined !== listQuestionListeningChoosenDTOS[question.sentenceNo - 1] ?
                                              listQuestionListeningChoosenDTOS[question.sentenceNo - 1].answerChoosen !== "null" ?
                                                <Col xs="12" md="6" sm="12" lg="4" >
                                                  <div onClick={() => this.linkToElement(String(question.sentenceNo))}
                                                       style={{
                                                         border: '1px solid black',
                                                         width: '30px',
                                                         height: '30px',
                                                         borderRadius: '50%',
                                                         marginBottom: '5px',
                                                         marginTop: '5px',
                                                         position: 'relative',
                                                         backgroundColor: '#72716a',
                                                         color: 'white',
                                                         cursor: 'pointer'
                                                       }}>
                                                    <div style={{
                                                      position: 'absolute',
                                                      transform: 'translate(-50%, -50%)',
                                                      top: '50%',
                                                      marginLeft: '50%',
                                                      cursor: 'pointer'
                                                    }}>
                                                      {question.sentenceNo}
                                                    </div>
                                                  </div>
                                                </Col>
                                                :
                                                <Col xs="12" md="6" sm="12" lg="4">
                                                  <div onClick={() => this.linkToElement(String(question.sentenceNo))}
                                                       style={{
                                                         border: '1px solid black',
                                                         width: '30px',
                                                         height: '30px',
                                                         borderRadius: '50%',
                                                         marginBottom: '5px',
                                                         marginTop: '5px',
                                                         position: 'relative',
                                                         cursor: 'pointer'
                                                       }}>
                                                    <div style={{
                                                      position: 'absolute',
                                                      transform: 'translate(-50%, -50%)',
                                                      top: '50%',
                                                      marginLeft: '50%',
                                                      cursor: 'pointer'
                                                    }}>
                                                      {question.sentenceNo}
                                                    </div>
                                                  </div>
                                                </Col> : null
                                            }
                                          </>
                                          : <Col xs="12" md="6" sm="12" lg="4">
                                            <div onClick={() => this.linkToElement(String(question.sentenceNo))}
                                                 style={{
                                                   border: '1px solid black',
                                                   width: '30px',
                                                   height: '30px',
                                                   borderRadius: '50%',
                                                   marginBottom: '5px',
                                                   marginTop: '5px',
                                                   position: 'relative',
                                                   cursor: 'pointer'
                                                 }}>
                                              <div style={{
                                                position: 'absolute',
                                                transform: 'translate(-50%, -50%)',
                                                top: '50%',
                                                marginLeft: '50%',
                                                cursor: 'pointer'
                                              }}>
                                                {question.sentenceNo}
                                              </div>
                                            </div>
                                          </Col>
                                        }

                                      </>
                                    )}
                                  </>
                                )}
                              </Row>
                            </Col>
                            {/*----------------------End PART 2 ------------------------*/}

                            {/*----------------------PART 3 ------------------------*/}
                            <Col xs="12" md="12" sm="12" lg="12" style={{marginTop: '5%'}}>
                              <h6 style={{color: '#169BD5', fontWeight: '700'}}
                                  onClick={() => this.linkToElement(String(listQuestionMinitest[2].partName))}>
                                <u style={{fontSize:'1.3rem'}}>Part 3:</u>
                              </h6>
                              <Row style={{textAlign: 'center', border: '1px solid #1da9ed', margin: '0', paddingLeft: '9%'}}>
                                {listQuestionMinitest[2].listCategoryMinitestDTOS.map((category, indexCategory) =>
                                  <>
                                    {category.listQuestionMinitestDTOS.map((question, indexQuestion) =>
                                      <>
                                        {listQuestionListeningChoosenDTOS.length > 0 ?
                                          <>
                                            {undefined !== listQuestionListeningChoosenDTOS[question.sentenceNo - 1] ?
                                              listQuestionListeningChoosenDTOS[question.sentenceNo - 1].answerChoosen !== "null" ?
                                                <Col xs="12" md="6" sm="12" lg="4" >
                                                  <div onClick={() => this.linkToElement(String(question.sentenceNo))}
                                                       style={{
                                                         border: '1px solid black',
                                                         width: '30px',
                                                         height: '30px',
                                                         borderRadius: '50%',
                                                         marginBottom: '5px',
                                                         marginTop: '5px',
                                                         position: 'relative',
                                                         backgroundColor: '#72716a',
                                                         color: 'white',
                                                         cursor: 'pointer'
                                                       }}>
                                                    <div style={{
                                                      position: 'absolute',
                                                      transform: 'translate(-50%, -50%)',
                                                      top: '50%',
                                                      marginLeft: '50%',
                                                      cursor: 'pointer'
                                                    }}>
                                                      {question.sentenceNo}
                                                    </div>
                                                  </div>
                                                </Col>
                                                :
                                                <Col xs="12" md="6" sm="12" lg="4">
                                                  <div onClick={() => this.linkToElement(String(question.sentenceNo))}
                                                       style={{
                                                         border: '1px solid black',
                                                         width: '30px',
                                                         height: '30px',
                                                         borderRadius: '50%',
                                                         marginBottom: '5px',
                                                         marginTop: '5px',
                                                         position: 'relative',
                                                         cursor: 'pointer'
                                                       }}>
                                                    <div style={{
                                                      position: 'absolute',
                                                      transform: 'translate(-50%, -50%)',
                                                      top: '50%',
                                                      marginLeft: '50%',
                                                      cursor: 'pointer'
                                                    }}>
                                                      {question.sentenceNo}
                                                    </div>
                                                  </div>
                                                </Col> : null
                                            }
                                          </>
                                          : <Col xs="12" md="6" sm="12" lg="4">
                                            <div onClick={() => this.linkToElement(String(question.sentenceNo))}
                                                 style={{
                                                   border: '1px solid black',
                                                   width: '30px',
                                                   height: '30px',
                                                   borderRadius: '50%',
                                                   marginBottom: '5px',
                                                   marginTop: '5px',
                                                   position: 'relative',
                                                   cursor: 'pointer'
                                                 }}>
                                              <div style={{
                                                position: 'absolute',
                                                transform: 'translate(-50%, -50%)',
                                                top: '50%',
                                                marginLeft: '50%',
                                                cursor: 'pointer'
                                              }}>
                                                {question.sentenceNo}
                                              </div>
                                            </div>
                                          </Col>
                                        }

                                      </>
                                    )}
                                  </>
                                )}
                              </Row>
                            </Col>
                            {/*----------------------End PART 3 ------------------------*/}

                            {/*----------------------PART 4 ------------------------*/}
                            <Col xs="12" md="12" sm="12" lg="12" style={{marginTop: '5%'}}>
                              <h6 style={{color: '#169BD5', fontWeight: '700'}}
                                  onClick={() => this.linkToElement(String(listQuestionMinitest[3].partName))}>
                                <u style={{fontSize:'1.3rem'}}>Part 4:</u>
                              </h6>
                              <Row style={{textAlign: 'center', border: '1px solid #1da9ed', margin: '0', paddingLeft: '9%'}}>
                                {listQuestionMinitest[3].listCategoryMinitestDTOS.map((category, indexCategory) =>
                                  <>
                                    {category.listQuestionMinitestDTOS.map((question, indexQuestion) =>
                                      <>
                                        {listQuestionListeningChoosenDTOS.length > 0 ?
                                          <>
                                            {undefined !== listQuestionListeningChoosenDTOS[question.sentenceNo - 1] ?
                                              listQuestionListeningChoosenDTOS[question.sentenceNo - 1].answerChoosen !== "null" ?
                                                <Col xs="12" md="6" sm="12" lg="4" >
                                                  <div onClick={() => this.linkToElement(String(question.sentenceNo))}
                                                       style={{
                                                         border: '1px solid black',
                                                         width: '30px',
                                                         height: '30px',
                                                         borderRadius: '50%',
                                                         marginBottom: '5px',
                                                         marginTop: '5px',
                                                         position: 'relative',
                                                         backgroundColor: '#72716a',
                                                         color: 'white',
                                                         cursor: 'pointer'
                                                       }}>
                                                    <div style={{
                                                      position: 'absolute',
                                                      transform: 'translate(-50%, -50%)',
                                                      top: '50%',
                                                      marginLeft: '50%',
                                                      cursor: 'pointer'
                                                    }}>
                                                      {question.sentenceNo}
                                                    </div>
                                                  </div>
                                                </Col>
                                                :
                                                <Col xs="12" md="6" sm="12" lg="4">
                                                  <div onClick={() => this.linkToElement(String(question.sentenceNo))}
                                                       style={{
                                                         border: '1px solid black',
                                                         width: '30px',
                                                         height: '30px',
                                                         borderRadius: '50%',
                                                         marginBottom: '5px',
                                                         marginTop: '5px',
                                                         position: 'relative',
                                                         cursor: 'pointer'
                                                       }}>
                                                    <div style={{
                                                      position: 'absolute',
                                                      transform: 'translate(-50%, -50%)',
                                                      top: '50%',
                                                      marginLeft: '50%',
                                                      cursor: 'pointer'
                                                    }}>
                                                      {question.sentenceNo}
                                                    </div>
                                                  </div>
                                                </Col> : null
                                            }
                                          </>
                                          : <Col xs="12" md="6" sm="12" lg="4">
                                            <div onClick={() => this.linkToElement(String(question.sentenceNo))}
                                                 style={{
                                                   border: '1px solid black',
                                                   width: '30px',
                                                   height: '30px',
                                                   borderRadius: '50%',
                                                   marginBottom: '5px',
                                                   marginTop: '5px',
                                                   position: 'relative',
                                                   cursor: 'pointer'
                                                 }}>
                                              <div style={{
                                                position: 'absolute',
                                                transform: 'translate(-50%, -50%)',
                                                top: '50%',
                                                marginLeft: '50%',
                                                cursor: 'pointer'
                                              }}>
                                                {question.sentenceNo}
                                              </div>
                                            </div>
                                          </Col>
                                        }

                                      </>
                                    )}
                                  </>
                                )}
                              </Row>
                            </Col>
                            {/*----------------------End PART 4 ------------------------*/}

                            {this.props.doReadingFulltest ?
                              <>
                                {/*----------------------PART 5 ------------------------*/}
                                <Col xs="12" md="12" sm="12" lg="12" style={{marginTop: '5%'}}>
                                  <h6 style={{color: '#169BD5', fontWeight: '700'}}
                                      onClick={() => this.linkToElement(String(listQuestionMinitest[4].partName))}>
                                    <u style={{fontSize:'1.3rem'}}>Part 5:</u>
                                  </h6>
                                  <Row style={{textAlign: 'center', border: '1px solid #1da9ed', margin: '0', paddingLeft: '9%'}}>
                                    {listQuestionMinitest[4].listCategoryMinitestDTOS.map((category, indexCategory) =>
                                      <>
                                        {category.listQuestionMinitestDTOS.map((question, indexQuestion) =>
                                          <>
                                            {listQuestionReadingChoosenDTOS.length > 0 ?
                                              <>
                                                {undefined !== listQuestionReadingChoosenDTOS[question.sentenceNo-1-sizeOfListeningQues] ?
                                                  listQuestionReadingChoosenDTOS[question.sentenceNo-1-sizeOfListeningQues].answerChoosen !== "null" ?
                                                    <Col xs="12" md="6" sm="12" lg="4" >
                                                      <div onClick={() => this.linkToElement(String(question.sentenceNo))}
                                                           style={{
                                                             border: '1px solid black',
                                                             width: '30px',
                                                             height: '30px',
                                                             borderRadius: '50%',
                                                             marginBottom: '5px',
                                                             marginTop: '5px',
                                                             position: 'relative',
                                                             backgroundColor: '#72716a',
                                                             color: 'white',
                                                             cursor: 'pointer'
                                                           }}>
                                                        <div style={{
                                                          position: 'absolute',
                                                          transform: 'translate(-50%, -50%)',
                                                          top: '50%',
                                                          marginLeft: '50%',
                                                          cursor: 'pointer'
                                                        }}>
                                                          {question.sentenceNo}
                                                        </div>
                                                      </div>
                                                    </Col>
                                                    :
                                                    <Col xs="12" md="6" sm="12" lg="4">
                                                      <div onClick={() => this.linkToElement(String(question.sentenceNo))}
                                                           style={{
                                                             border: '1px solid black',
                                                             width: '30px',
                                                             height: '30px',
                                                             borderRadius: '50%',
                                                             marginBottom: '5px',
                                                             marginTop: '5px',
                                                             position: 'relative',
                                                             cursor: 'pointer'
                                                           }}>
                                                        <div style={{
                                                          position: 'absolute',
                                                          transform: 'translate(-50%, -50%)',
                                                          top: '50%',
                                                          marginLeft: '50%',
                                                          cursor: 'pointer'
                                                        }}>
                                                          {question.sentenceNo}
                                                        </div>
                                                      </div>
                                                    </Col> : null
                                                }
                                              </>
                                              : <Col xs="12" md="6" sm="12" lg="4">
                                                <div onClick={() => this.linkToElement(String(question.sentenceNo))}
                                                     style={{
                                                       border: '1px solid black',
                                                       width: '30px',
                                                       height: '30px',
                                                       borderRadius: '50%',
                                                       marginBottom: '5px',
                                                       marginTop: '5px',
                                                       position: 'relative',
                                                       cursor: 'pointer'
                                                     }}>
                                                  <div style={{
                                                    position: 'absolute',
                                                    transform: 'translate(-50%, -50%)',
                                                    top: '50%',
                                                    marginLeft: '50%',
                                                    cursor: 'pointer'
                                                  }}>
                                                    {question.sentenceNo}
                                                  </div>
                                                </div>
                                              </Col>
                                            }

                                          </>
                                        )}
                                      </>
                                    )}
                                  </Row>
                                </Col>
                                {/*----------------------End PART 5 ------------------------*/}

                                {/*----------------------PART 6 ------------------------*/}
                                <Col xs="12" md="12" sm="12" lg="12" style={{marginTop: '5%'}}>
                                  <h6 style={{color: '#169BD5', fontWeight: '700'}}
                                      onClick={() => this.linkToElement(String(listQuestionMinitest[5].partName))}>
                                    <u style={{fontSize:'1.3rem'}}>Part 6:</u>
                                  </h6>
                                  <Row style={{textAlign: 'center', border: '1px solid #1da9ed', margin: '0', paddingLeft: '9%'}}>
                                    {listQuestionMinitest[5].listCategoryMinitestDTOS.map((category, indexCategory) =>
                                      <>
                                        {category.listQuestionMinitestDTOS.map((question, indexQuestion) =>
                                          <>
                                            {listQuestionReadingChoosenDTOS.length > 0 ?
                                              <>
                                                {undefined !== listQuestionReadingChoosenDTOS[question.sentenceNo-1-sizeOfListeningQues] ?
                                                  listQuestionReadingChoosenDTOS[question.sentenceNo-1-sizeOfListeningQues].answerChoosen !== "null" ?
                                                    <Col xs="12" md="6" sm="12" lg="4" >
                                                      <div onClick={() => this.linkToElement(String(question.sentenceNo))}
                                                           style={{
                                                             border: '1px solid black',
                                                             width: '30px',
                                                             height: '30px',
                                                             borderRadius: '50%',
                                                             marginBottom: '5px',
                                                             marginTop: '5px',
                                                             position: 'relative',
                                                             backgroundColor: '#72716a',
                                                             color: 'white',
                                                             cursor: 'pointer'
                                                           }}>
                                                        <div style={{
                                                          position: 'absolute',
                                                          transform: 'translate(-50%, -50%)',
                                                          top: '50%',
                                                          marginLeft: '50%',
                                                          cursor: 'pointer'
                                                        }}>
                                                          {question.sentenceNo}
                                                        </div>
                                                      </div>
                                                    </Col>
                                                    :
                                                    <Col xs="12" md="6" sm="12" lg="4">
                                                      <div onClick={() => this.linkToElement(String(question.sentenceNo))}
                                                           style={{
                                                             border: '1px solid black',
                                                             width: '30px',
                                                             height: '30px',
                                                             borderRadius: '50%',
                                                             marginBottom: '5px',
                                                             marginTop: '5px',
                                                             position: 'relative',
                                                             cursor: 'pointer'
                                                           }}>
                                                        <div style={{
                                                          position: 'absolute',
                                                          transform: 'translate(-50%, -50%)',
                                                          top: '50%',
                                                          marginLeft: '50%',
                                                          cursor: 'pointer'
                                                        }}>
                                                          {question.sentenceNo}
                                                        </div>
                                                      </div>
                                                    </Col> : null
                                                }
                                              </>
                                              : <Col xs="12" md="6" sm="12" lg="4">
                                                <div onClick={() => this.linkToElement(String(question.sentenceNo))}
                                                     style={{
                                                       border: '1px solid black',
                                                       width: '30px',
                                                       height: '30px',
                                                       borderRadius: '50%',
                                                       marginBottom: '5px',
                                                       marginTop: '5px',
                                                       position: 'relative',
                                                       cursor: 'pointer'
                                                     }}>
                                                  <div style={{
                                                    position: 'absolute',
                                                    transform: 'translate(-50%, -50%)',
                                                    top: '50%',
                                                    marginLeft: '50%',
                                                    cursor: 'pointer'
                                                  }}>
                                                    {question.sentenceNo}
                                                  </div>
                                                </div>
                                              </Col>
                                            }

                                          </>
                                        )}
                                      </>
                                    )}
                                  </Row>
                                </Col>
                                {/*----------------------End PART 6 ------------------------*/}

                                {/*----------------------PART 7 ------------------------*/}
                                <Col xs="12" md="12" sm="12" lg="12" style={{marginTop: '5%'}}>
                                  <h6 style={{color: '#169BD5', fontWeight: '700'}}
                                      onClick={() => this.linkToElement(String(listQuestionMinitest[6].partName))}>
                                    <u style={{fontSize:'1.3rem'}}>Part 7:</u>
                                  </h6>
                                  <Row style={{textAlign: 'center', border: '1px solid #1da9ed', margin: '0', paddingLeft: '9%'}}>
                                    {listQuestionMinitest[6].listCategoryMinitestDTOS.map((category, indexCategory) =>
                                      <>
                                        {category.listQuestionMinitestDTOS.map((question, indexQuestion) =>
                                          <>
                                            {listQuestionReadingChoosenDTOS.length > 0 ?
                                              <>
                                                {undefined !== listQuestionReadingChoosenDTOS[question.sentenceNo-1-sizeOfListeningQues] ?
                                                  listQuestionReadingChoosenDTOS[question.sentenceNo-1-sizeOfListeningQues].answerChoosen !== "null" ?
                                                    <Col xs="12" md="6" sm="12" lg="4" >
                                                      <div onClick={() => this.linkToElement(String(question.sentenceNo))}
                                                           style={{
                                                             border: '1px solid black',
                                                             width: '30px',
                                                             height: '30px',
                                                             borderRadius: '50%',
                                                             marginBottom: '5px',
                                                             marginTop: '5px',
                                                             position: 'relative',
                                                             backgroundColor: '#72716a',
                                                             color: 'white',
                                                             cursor: 'pointer'
                                                           }}>
                                                        <div style={{
                                                          position: 'absolute',
                                                          transform: 'translate(-50%, -50%)',
                                                          top: '50%',
                                                          marginLeft: '50%',
                                                          cursor: 'pointer'
                                                        }}>
                                                          {question.sentenceNo}
                                                        </div>
                                                      </div>
                                                    </Col>
                                                    :
                                                    <Col xs="12" md="6" sm="12" lg="4">
                                                      <div onClick={() => this.linkToElement(String(question.sentenceNo))}
                                                           style={{
                                                             border: '1px solid black',
                                                             width: '30px',
                                                             height: '30px',
                                                             borderRadius: '50%',
                                                             marginBottom: '5px',
                                                             marginTop: '5px',
                                                             position: 'relative',
                                                             cursor: 'pointer'
                                                           }}>
                                                        <div style={{
                                                          position: 'absolute',
                                                          transform: 'translate(-50%, -50%)',
                                                          top: '50%',
                                                          marginLeft: '50%',
                                                          cursor: 'pointer'
                                                        }}>
                                                          {question.sentenceNo}
                                                        </div>
                                                      </div>
                                                    </Col> : null
                                                }
                                              </>
                                              : <Col xs="12" md="6" sm="12" lg="4">
                                                <div onClick={() => this.linkToElement(String(question.sentenceNo))}
                                                     style={{
                                                       border: '1px solid black',
                                                       width: '30px',
                                                       height: '30px',
                                                       borderRadius: '50%',
                                                       marginBottom: '5px',
                                                       marginTop: '5px',
                                                       position: 'relative',
                                                       cursor: 'pointer'
                                                     }}>
                                                  <div style={{
                                                    position: 'absolute',
                                                    transform: 'translate(-50%, -50%)',
                                                    top: '50%',
                                                    marginLeft: '50%',
                                                    cursor: 'pointer'
                                                  }}>
                                                    {question.sentenceNo}
                                                  </div>
                                                </div>
                                              </Col>
                                            }

                                          </>
                                        )}
                                      </>
                                    )}
                                  </Row>
                                </Col>
                                {/*----------------------End PART 7 ------------------------*/}
                              </> : null
                            }

                          </fieldset>
                        </> : null
                      }
                    </div>
                    :
                    <div id="scrollQuestion">
                      {/*{loadingQuestion !== undefined ?*/}
                      {/*<>{loadingQuestion === false ? <>{this.loadingComponent}</> : ""}</> : null*/}
                      {/*}*/}
                      {listQuestionAndAnswerMinitest.length > 0 ?
                        <>
                          {/*----------------------PART 1-8 ------------------------*/}
                          {listQuestionAndAnswerMinitest.map((quesMinitest, index) =>
                            <Col xs="12" md="12" sm="12" lg="12" style={{marginTop: '5%'}}>
                              <h6  style={{color: '#169BD5', marginLeft: '5%', fontWeight: '700'}}
                                   onClick={() => this.linkToElement(String(quesMinitest.partName))}>
                                <u style={{fontSize:'1.3rem'}}>Part {index + 1}:</u>
                              </h6>
                              <Row style={{textAlign: 'center', border: '1px solid #1da9ed', paddingLeft: '9%', margin:'0'}}>
                                {quesMinitest.listCategoryMinitestDTOS.map((category, indexCategory) =>
                                  <>
                                    {category.listQuestionMinitestDTOS.map((question, indexQuestion) =>
                                      <>
                                        {question.indexIncorrectAnswer === -1 ?
                                          <Col xs="12" md="6" sm="12" lg="4" style={{textAlign: 'center'}}>
                                            <div onClick={() => this.linkToElement(String(question.sentenceNo))}
                                                 style={{
                                                   border: '1px solid gray',
                                                   width: '30px',
                                                   height: '30px',
                                                   borderRadius: '50%',
                                                   marginBottom: '5px',
                                                   marginTop: '5px',
                                                   position: 'relative',
                                                   backgroundColor: 'green',
                                                   color: 'white',
                                                   cursor: 'pointer'
                                                 }}>
                                              <div style={{
                                                position: 'absolute',
                                                transform: 'translate(-50%, -50%)',
                                                top: '50%',
                                                marginLeft: '50%',
                                                cursor: 'pointer'
                                              }}>
                                                {question.sentenceNo}
                                              </div>
                                            </div>
                                          </Col>
                                          :
                                          <Col xs="12" md="6" sm="12" lg="4" style={{textAlign: 'center'}}>
                                            <div onClick={() => this.linkToElement(String(question.sentenceNo))}
                                                 style={{
                                                   border: '1px solid gray',
                                                   width: '30px',
                                                   height: '30px',
                                                   borderRadius: '50%',
                                                   marginBottom: '5px',
                                                   marginTop: '5px',
                                                   position: 'relative',
                                                   backgroundColor: 'red',
                                                   color: 'white',
                                                   cursor: 'pointer'
                                                 }}>
                                              <div style={{
                                                position: 'absolute',
                                                transform: 'translate(-50%, -50%)',
                                                top: '50%',
                                                marginLeft: '50%',
                                                cursor: 'pointer'
                                              }}>
                                                {question.sentenceNo}
                                              </div>
                                            </div>
                                          </Col>
                                        }
                                      </>
                                    )}
                                  </>
                                )}
                              </Row>
                            </Col>
                          )}
                          {/*----------------------End PART 1-8 ------------------------*/}
                        </>
                        : null
                      }
                    </div>

                  }

                </div>

              </Col>
            </Row>
          }

        </Container>

      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    listQuestionMinitest : state.doTestReducer.listQuestionFullTest,
    listQuestionListeningChoosenDTOS: state.doTestReducer.listQuestionListeningChoosenDTOS,
    listQuestionReadingChoosenDTOS: state.doTestReducer.listQuestionReadingChoosenDTOS,
    listQuestionAndAnswerMinitest: state.doTestReducer.listQuestionAndAnswerFullTest,
    loadingQuestion: state.doTestReducer.loadingQuestion,
    loadingSubmitAnswer: state.doTestReducer.loadingSubmitAnswer,
    showAnswer: state.doTestReducer.showAnswer,
    detailTest: state.doTestReducer.detailTest,
    doReadingFulltest: state.doTestReducer.doReadingFulltest,
    login: state.auth.login,
    detailHistoryFullTest: state.historyTestReducer.detailHistoryFullTest,
    viewHistory: state.historyTestReducer.viewHistory,
    loadingResult: state.historyTestReducer.loadingResult,
    listHistoryFullTest: state.historyTestReducer.listQuestionAndAnswerFullTest,
    hourForFulltest: state.doTestReducer.hourForFulltest,
    minuteForFulltest: state.doTestReducer.minuteForFulltest,
    secondForFulltest: state.doTestReducer.secondForFulltest,
    doTime: state.doTestReducer.doTime
  }
}
const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    testAction : bindActionCreators(testAction , dispatch),
    doTestActions: bindActionCreators(doTestAction, dispatch),
    historyTestActions: bindActionCreators(historyTestAction, dispatch)
  }
}
const withConnect = connect(mapStateToProps, mapDispatchToProps)


export default compose(withStyles(styles), withConnect)(Index);
