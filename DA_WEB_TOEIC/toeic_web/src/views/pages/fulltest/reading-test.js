import React, { Component } from 'react';
import {connect} from "react-redux";
import {bindActionCreators, compose} from "redux";
import {
  Badge,
  Button,
  Col,
  Container,
  Form,
  FormGroup,
  Input,
  Label, PopoverBody,
  Row,
  Spinner,
  UncontrolledPopover
} from "reactstrap"
import styles from "./style";
import {withStyles} from "@material-ui/core";
import {ReactPlayerReading} from "../Practices/ListeningToeicShortTalkingPractice/style";

import testAction from "../../../redux/actions/do-test-management";
import * as Icon from "react-feather";
import {FormattedMessage} from "react-intl";

class ReadingTest extends Component {

  constructor(props) {
    super(props);
    this.state = {
      load: false,
      listQuesReading: [],
      listSttQuesListeningChoosen: this.props.listSttQuestionListeningChoosen,
      listSttQuesReadingChoosen: this.props.listSttQuestionReadingChoosen,
      listQuestionReadingPrimitive: []
    }
  }

  // componentDidMount() {
  //   this.setState({
  //     load: true
  //   })
  //   setTimeout(()=>{
  //     this.setState({
  //       load: false
  //     })
  //   }, 2000)
  // }

  cutAnswerToChoose(answerToChoose){
    let char = 0;
    let answerArray = [];
    let subAnswer = "";

    let count=0,index=0;
    for( count=-1,index=-2; index != -1; count++,index=answerToChoose.indexOf("|",index+1) );

    for(let i=1;i<=count;i++){
      char = answerToChoose.indexOf("|");
      subAnswer = answerToChoose.substring(0, char);
      answerArray.push(subAnswer);
      answerToChoose = answerToChoose.substring(char + 1);
    }
    return answerArray;
  }

  lowerCaseAll =(string)=>{
    return string.toLowerCase();
  }
  upperCaseFirstLetter =(string)=>{
    string = this.lowerCaseAll(string);
    return string.trim().charAt(0).toUpperCase() + string.trim().slice(1);
  }

  cutSubAnswer(subAnswer){
    let index = subAnswer.indexOf(".");
    let title = subAnswer.substring(0, index);
    let content = subAnswer.substring(index+1);

    let array = [];
    array.push(title);
    array.push(content);
    return array;
  }

  cutInputValue(value){
    let index = value.indexOf("|");
    let title = value.substring(0, index);
    let content = value.substring(index+1);

    let array = [];
    array.push(title);
    array.push(content);
    return array;
  }

  compare =(a, b)=>{
    if ( a.sentenceNo < b.sentenceNo ){
      return -1;
    }
    if ( a.sentenceNo > b.sentenceNo ){
      return 1;
    }
    return 0;
  }

  getlistQuesReadingPrimitive =(question2)=> {
    const list = this.props.listQuestionMinitest;
    let {listQuestionReadingPrimitive} = this.state;

    //chọn lần 1
    if(listQuestionReadingPrimitive.length === 0){
      let listQuesReadingPrimitive = [];

      for(let i=0;i<list[4].listCategoryMinitestDTOS.length;i++){
        for(let j=0;j<list[4].listCategoryMinitestDTOS[i].listQuestionMinitestDTOS.length;j++){
          let question = {};
          question.id = Number(list[4].listCategoryMinitestDTOS[i].listQuestionMinitestDTOS[j].id);
          question.sentenceNo = Number(list[4].listCategoryMinitestDTOS[i].listQuestionMinitestDTOS[j].sentenceNo);
          question.answerChoosen = "null";
          question.indexSubAnswer = null;
          question.categoryId = Number(list[4].listCategoryMinitestDTOS[i].categoryId);
          question.partName = list[4].partName;
          if(question2.id === question.id){
            listQuesReadingPrimitive.push(question2)
          }
          else{
            listQuesReadingPrimitive.push(question)
          }
        }
      }


      for(let i=0;i<list[5].listCategoryMinitestDTOS.length;i++){
        for(let j=0;j<list[5].listCategoryMinitestDTOS[i].listQuestionMinitestDTOS.length;j++){
          let question = {};
          question.id = Number(list[5].listCategoryMinitestDTOS[i].listQuestionMinitestDTOS[j].id);
          question.sentenceNo = Number(list[5].listCategoryMinitestDTOS[i].listQuestionMinitestDTOS[j].sentenceNo);
          question.answerChoosen = "null";
          question.indexSubAnswer = null;
          question.categoryId = Number(list[5].listCategoryMinitestDTOS[i].categoryId);
          question.partName = list[5].partName;
          if(question2.id === question.id){
            listQuesReadingPrimitive.push(question2)
          }
          else{
            listQuesReadingPrimitive.push(question)
          }
        }
      }

      for(let i=0;i<list[6].listCategoryMinitestDTOS.length;i++){
        for(let j=0;j<list[6].listCategoryMinitestDTOS[i].listQuestionMinitestDTOS.length;j++){
          let question = {};
          question.id = Number(list[6].listCategoryMinitestDTOS[i].listQuestionMinitestDTOS[j].id);
          question.sentenceNo = Number(list[6].listCategoryMinitestDTOS[i].listQuestionMinitestDTOS[j].sentenceNo);
          question.answerChoosen = "null";
          question.indexSubAnswer = null;
          question.categoryId = Number(list[6].listCategoryMinitestDTOS[i].categoryId);
          question.partName = list[6].partName;
          if(question2.id === question.id){
            listQuesReadingPrimitive.push(question2)
          }
          else{
            listQuesReadingPrimitive.push(question)
          }
        }
      }

      // for(let i=0;i<list[7].listCategoryMinitestDTOS.length;i++){
      //   for(let j=0;j<list[7].listCategoryMinitestDTOS[i].listQuestionMinitestDTOS.length;j++){
      //     let question = {};
      //     question.id = Number(list[7].listCategoryMinitestDTOS[i].listQuestionMinitestDTOS[j].id);
      //     question.stt = Number(list[7].listCategoryMinitestDTOS[i].listQuestionMinitestDTOS[j].stt);
      //     question.answerChoosen = "null";
      //     question.indexSubAnswer = null;
      //     question.categoryId = Number(list[7].listCategoryMinitestDTOS[i].categoryId);
      //     question.partName = list[7].partName;
      //     if(question2.id === question.id){
      //       listQuesReadingPrimitive.push(question2)
      //     }
      //     else{
      //       listQuesReadingPrimitive.push(question)
      //     }
      //   }
      // }

      this.setState({
        listQuestionReadingPrimitive: listQuesReadingPrimitive
      })
      return listQuesReadingPrimitive;
    }
    // >= chọn lần 2
    else{
      for(let i=0; i<listQuestionReadingPrimitive.length;i++){
        if(question2.id === listQuestionReadingPrimitive[i].id){
          listQuestionReadingPrimitive[i].id = question2.id;
          listQuestionReadingPrimitive[i].sentenceNo = question2.sentenceNo;
          listQuestionReadingPrimitive[i].answerChoosen = question2.answerChoosen;
          listQuestionReadingPrimitive[i].indexSubAnswer = question2.indexSubAnswer;
          listQuestionReadingPrimitive[i].categoryId = question2.categoryId;
          listQuestionReadingPrimitive[i].partName = question2.partName;
        }
      }
      this.setState({
        listQuestionReadingPrimitive: listQuestionReadingPrimitive
      })
      return listQuestionReadingPrimitive;
    }


  }

  onChangeValue =(e)=> {
    const {testAction} = this.props;
    let question = {};

    let answerChoosen;
    let id = this.cutSubAnswer(this.cutInputValue(e.target.value)[1])[0];
    let indexSubAnswerAndCategoryIdAndSttAndPartName = this.cutSubAnswer(this.cutInputValue(e.target.value)[1])[1];
    let indexSubAnswer = this.cutInputValue(indexSubAnswerAndCategoryIdAndSttAndPartName)[0];
    if(indexSubAnswer === "0"){
      answerChoosen = "A"
    } else if(indexSubAnswer === "1"){
      answerChoosen = "B"
    } else if(indexSubAnswer === "2"){
      answerChoosen = "C"
    } else if(indexSubAnswer === "3"){
      answerChoosen = "D"
    }
    let categoryIdAndSttAndPartName = this.cutInputValue(indexSubAnswerAndCategoryIdAndSttAndPartName)[1];
    let categoryId = this.cutSubAnswer(categoryIdAndSttAndPartName)[0];
    let sttAndPartName = this.cutSubAnswer(categoryIdAndSttAndPartName)[1];
    let sentenceNo = this.cutInputValue(sttAndPartName)[0];
    let partName = this.cutInputValue(sttAndPartName)[1];

    question.id = Number(id);
    question.sentenceNo = Number(sentenceNo);
    question.answerChoosen = answerChoosen;
    question.indexSubAnswer = Number(indexSubAnswer);
    if(categoryId === "null"){
      question.categoryId = 0;
    }
    else{
      question.categoryId = Number(categoryId);
    }
    question.partName = partName;
    //

    //list mới
    let list = this.getlistQuesReadingPrimitive(question);
    //list mới

    const {getListQuesReadingChoosen} = testAction;
    getListQuesReadingChoosen(list);
  }

  checkingAnswerChoosen =(indexIncorrectAnswer, indexCorrectAnswer, indexSubAnswer)=>{
    // th câu đúng
    if(indexIncorrectAnswer !== -1){
      if(indexSubAnswer === indexIncorrectAnswer){
        return true;
      }
      else{
        return false;
      }
    }
    //th câu sai
    else{
      if(indexSubAnswer === indexCorrectAnswer){
        return true;
      }
      else {
        return false;
      }
    }
  }

  loadingComponent = (
    <div style={{
      position: 'absolute',
      zIndex: 110,
      top: 0,
      left: 0,
      width: '100%',
      height: '100%',
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      background: 'rgba(255,255,255,0.8)',
    }}>
      <Spinner color="primary" className="reload-spinner"/>
    </div>
  );


  render() {
    const {classes, listQuestionMinitest , listQuestionAndAnswerMinitest, showAnswer, loadingQuestion,listHistoryFullTest,
      loadingSubmitAnswer} = this.props;
    // const {showAnswer} = this.state;

    return (
      <>
        <style>{`
          #iconTranslate:hover {
            stroke: green !important
          }

        `}

        </style>
      <div className="mt-3">
        {loadingQuestion !== undefined ?
          <>{loadingQuestion === false && !this.props.loadingResult ? <>{this.loadingComponent}</> : ""}</> : null
        }
        {
          loadingSubmitAnswer === true && listQuestionAndAnswerMinitest.length === 0 ? this.loadingComponent  : null
        }
        <Badge style={{backgroundColor: "rgba(32 171 231)", width: "100%", height: "60%", marginLeft: "0%",
          padding: "10px 0 0 0", borderRadius:'10px'}} className="badge-xl block">
          <div>
            <div className="text-left" style={{marginLeft: '0px'}}>
              <div className={classes.titleModal}>
                <h3 style={{color:'white', marginLeft: '2%', marginTop:'1%', fontSize:'2rem'}}>Reading Test</h3>
              </div>
            </div>
          </div>
        </Badge>
        <div style={{position: 'relative', margin: '0 1%'}}>

          <Row className={classes.bodyModal} style={{margin: '0 0% 0 0%', padding: '0 0%', width: "100%" }}>
            <div style={{margin:'3% 3%'}}>
              {
                listQuestionAndAnswerMinitest.length > 0 || this.props.listHistoryFullTest.length > 0 ? null :
                  <h5>In the Reading test, you will read a variety of texts and answer several different types of reading comprehensive questions.

                    The entire Reading test will last 75 minutes. There are three parts, and directions are given for each part. You are

                    encouraged to answer as many questions as possible within the time allowed. You must mark your answers on the

                    separate answer sheet. Do not write your answer in your test book.</h5>
              }

              {
                this.props.viewHistory ?
                  <>
                    {listHistoryFullTest.length > 0 ?
                      <>
                        {/*----------------------------------------PART5-----------------------------------------------*/}
                        <div className="mt-3">
                          <h1 id="PART5" style={{fontWeight: '700'}}>PART 5</h1>

                          <Row style={{marginTop: '3%', marginLeft: '6%'}}>
                            {listHistoryFullTest[4].listCategoryMinitestDTOS.map((category, indexCategory) =>{
                                {/*<h4 style={{fontWeight: '700', marginTop: '5%'}}>Đoạn {indexCategory + 1}.</h4>*/}
                                let smallItem = category.listQuestionMinitestDTOS.map((question, indexQuestion)=>{
                                  return (
                                    <>
                                      <Col id={String(question.sentenceNo)} xs={6}>
                                        <h6 style={{fontWeight: '700'}}>{question.sentenceNo}. {question.name}
                                          {/*phần dịch câu hỏi*/}
                                          <button style={{border: 'none', backgroundColor: 'white', padding: '0', marginLeft: '2%'}}
                                                  id={"q" + question.sentenceNo} className="mr-1 mb-1">
                                            <Icon.AlertCircle  size={20} />
                                          </button>
                                          <UncontrolledPopover trigger="focus" placement="bottom" target={"q" + question.sentenceNo}

                                          >
                                            <PopoverBody style={{borderColor: 'green !important', color: 'white', backgroundColor:'green'}}>
                                              <FormattedMessage id={"translate"}/>: {question.translatingQuestion !== null ? question.translatingQuestion : <FormattedMessage id={"no.translate"}/>}
                                            </PopoverBody>
                                          </UncontrolledPopover>
                                          {/*end phần dịch câu hỏi*/}
                                        </h6>

                                        {/*{question ? question.answersToChoose.split("|").map((subAnswer, indexSubAnswer) =>*/}
                                        {question ? this.cutAnswerToChoose(question.answersToChoose).map((subAnswer, indexSubAnswer) =>
                                          <>
                                            <Col xs="12" md="12" sm="12" lg="12" style={{display: 'flex', padding: '0%'}}>
                                              <Col xs="1" md="2" sm="2" lg="1" style={{padding: '0%'}}>
                                                {indexSubAnswer === question.indexCorrectAnswer ?
                                                  <Icon.Check size={25} style={{color: 'green'}}/> : null
                                                }
                                                {indexSubAnswer === question.indexIncorrectAnswer ?
                                                  <Icon.X size={25} style={{color: '#b21717'}} /> : null
                                                }
                                                {/*<Icon.Check size={25} style={{color: 'green'}}/>*/}
                                              </Col>
                                              <Col xs="11" md="10" sm="10" lg="11" style={{padding: '0%', display: 'flex'}}>
                                                <FormGroup check style={{marginBottom:'4%'}}>
                                                  <Label  check className={classes.labelRadioButton} style={{cursor: 'pointer'}}>
                                                    <Input type="radio" name={question.sentenceNo}
                                                           checked={this.checkingAnswerChoosen(question.indexIncorrectAnswer,
                                                             question.indexCorrectAnswer, indexSubAnswer)}
                                                    />
                                                    ({ indexSubAnswer===0 ? "A" : (indexSubAnswer===1 ? "B" : (indexSubAnswer===2 ? "C" : "D")) }) {this.upperCaseFirstLetter(subAnswer)}
                                                  </Label>
                                                </FormGroup>
                                                {/*phần dịch câu đáp án*/}
                                                <button style={{border: 'none', backgroundColor: 'white', padding: '0', marginLeft: '10px'}}
                                                        id={"a" + question.sentenceNo + indexSubAnswer} className="mr-1 mb-1">
                                                  <Icon.AlertCircle  size={20} />
                                                </button>
                                                <UncontrolledPopover trigger="focus" placement="bottom" target={"a" + question.sentenceNo + indexSubAnswer}>
                                                  <PopoverBody style={{borderColor: 'green !important', color: 'white', backgroundColor:'green'}}>
                                                    <FormattedMessage id={"translate"}/>: {indexSubAnswer===0 ? question.translatingQuesA : (indexSubAnswer===1 ? question.translatingQuesB : (indexSubAnswer===2 ? question.translatingQuesC : question.translatingQuesD))}
                                                  </PopoverBody>
                                                </UncontrolledPopover>
                                                {/*phần dịch câu đáp án*/}
                                              </Col>

                                            </Col>
                                          </>
                                        ) : null}
                                        <Col xs="12" md="12" sm="12" lg="12" style={{marginBottom: '10%', paddingRight: '10%'}}>
                                          <fieldset className={classes.fieldsetDescription}>
                                            <legend className={classes.legendDescription}><FormattedMessage id={"description"}/></legend>
                                            <i>{null === question.description ? <>><FormattedMessage id={"no.explanation"}/></> : question.description}</i>
                                          </fieldset>
                                        </Col>
                                      </Col>

                                    </>
                                  )
                                })
                                return (<>{smallItem}</>)
                              }
                            )}
                          </Row>

                        </div>
                        {/*----------------------------------------end PART5-----------------------------------------------*/}


                        {/*----------------------------------------PART6-----------------------------------------------*/}
                        <div className="mt-3">
                          <h1 id="PART6" style={{fontWeight: '700'}}>PART 6</h1>

                          {listHistoryFullTest[5].listCategoryMinitestDTOS.map((category, indexCategory) =>
                            <>
                              <h4 style={{fontWeight: '700', marginTop: '5%'}}>Part 6.{indexCategory + 1}</h4>
                              <div style={{marginLeft: '7%'}}>
                                <h5 style={{fontWeight:'700'}}>
                                  {category.listQuestionMinitestDTOS[0].name}-
                                  {category.listQuestionMinitestDTOS[category.listQuestionMinitestDTOS.length - 1].name}:
                                  refer to the following email
                                </h5>
                              </div>
                              <Row style={{marginTop: '3%', marginLeft: '6%'}}>
                                <Col xs={12}>
                                  <div style={{backgroundColor: '#d3d3d347', padding: '10px',lineHeight:'2'}}>
                                    {category.pathFile1}
                                  </div>
                                </Col>

                                {category.listQuestionMinitestDTOS.map((question, indexQuestion) =>
                                  <>
                                    <Col id={String(question.sentenceNo)} xs={6} style={{marginTop:'1%'}}>
                                      <h5 style={{fontWeight:'700'}}>{question.sentenceNo}. {question.name}</h5>
                                      {this.cutAnswerToChoose(question.answersToChoose).map((subAnswer, indexSubAnswer) =>
                                        <>
                                          <Col xs="12" md="12" sm="12" lg="12" style={{display: 'flex', padding: '0%'}}>
                                            <Col xs="1" md="2" sm="2" lg="1" style={{padding: '0%'}}>
                                              {indexSubAnswer === question.indexCorrectAnswer ?
                                                <Icon.Check size={25} style={{color: 'green'}}/> : null
                                              }
                                              {indexSubAnswer === question.indexIncorrectAnswer ?
                                                <Icon.X size={25} style={{color: '#b21717'}} /> : null
                                              }
                                            </Col>
                                            <Col xs="11" md="10" sm="10" lg="11" style={{padding: '0%', display: 'flex'}}>
                                              <FormGroup check style={{marginBottom:'4%'}}>
                                                <Label  check className={classes.labelRadioButton} style={{cursor: 'pointer'}}>
                                                  <Input type="radio" name={question.sentenceNo}
                                                         checked={this.checkingAnswerChoosen(question.indexIncorrectAnswer,
                                                           question.indexCorrectAnswer, indexSubAnswer)}
                                                  />
                                                  ({ indexSubAnswer===0 ? "A" : (indexSubAnswer===1 ? "B" : (indexSubAnswer===2 ? "C" : "D")) }) {this.upperCaseFirstLetter(subAnswer)}
                                                  {/*phần dịch câu đáp án*/}
                                                  <button style={{border: 'none', backgroundColor: 'white', padding: '0', marginLeft: '10px'}}
                                                          id={"a" + question.sentenceNo + indexSubAnswer} className="mr-1 mb-1">
                                                    <Icon.AlertCircle  size={20} />
                                                  </button>
                                                  <UncontrolledPopover trigger="focus" placement="bottom" target={"a" + question.sentenceNo + indexSubAnswer}

                                                  >
                                                    <PopoverBody style={{borderColor: 'green !important', color: 'white', backgroundColor:'green'}}>
                                                      <FormattedMessage id={"translate"}/>: {indexSubAnswer===0 ? question.translatingQuesA : (indexSubAnswer===1 ? question.translatingQuesB : (indexSubAnswer===2 ? question.translatingQuesC : question.translatingQuesD))}
                                                    </PopoverBody>
                                                  </UncontrolledPopover>
                                                  {/*phần dịch câu đáp án*/}
                                                </Label>

                                              </FormGroup>

                                            </Col>
                                          </Col>
                                        </>
                                      )}
                                      <Col xs="12" md="12" sm="12" lg="12" style={{marginBottom: '10%'}}>
                                        <fieldset className={classes.fieldsetDescription}>
                                          <legend className={classes.legendDescription}><FormattedMessage id={"description"}/></legend>
                                          <i>{null === question.description ? <>><FormattedMessage id={"no.explanation"}/> </> : question.description}</i>
                                        </fieldset>
                                      </Col>
                                    </Col>

                                  </>
                                )}
                              </Row>
                            </>
                          )}
                        </div>
                        {/*---------------------------------------end PART6--------------------------------------------*/}


                        {/*---------------------------------------PART7--------------------------------------------*/}
                        <div className="mt-3">
                          <h3 id="PART7" style={{fontWeight: '700'}}>PART 7</h3>

                          {listHistoryFullTest[6].listCategoryMinitestDTOS.map((category, indexCategory) =>
                            <>
                              <h4 style={{fontWeight: '700', marginTop: '5%'}}>Part 7.{indexCategory + 1}</h4>
                              <Row style={{marginTop: '3%'}}>
                                { category.pathFile1.includes("https://") === true ?
                                  <Col xs="12" md="12" sm="12" lg="12">
                                    <img className={classes.imgClass}
                                         style={{width: '100%', border: '5px solid lightgray'}}
                                         src={category.pathFile1}/>
                                  </Col>
                                  :
                                  <Col xs="12" md="12" sm="12" lg="12">
                                    <div style={{backgroundColor: '#d3d3d347', padding: '10px', lineHeight:'2'}}>
                                      {category.pathFile1}
                                    </div>
                                  </Col>
                                }
                                <Col xs="12" md="12" sm="12" lg="12" style={{marginTop: '5%'}}>
                                  <Row style={{marginTop: '3%', marginLeft: '5%'}}>
                                    {category.listQuestionMinitestDTOS.map((question, indexQuestion) =>
                                      <>

                                        <Col xs="6" md="6" sm="6" lg="6"  id={String(question.sentenceNo)} >
                                          <h5 style={{fontWeight:'700'}}>{question.sentenceNo}. {question.name}
                                            {/*phần dịch câu hỏi*/}
                                            <button style={{border: 'none', backgroundColor: 'white', padding: '0', marginLeft: '2%'}}
                                                    id={"q" + question.sentenceNo} className="mr-1 mb-1">
                                              <Icon.AlertCircle  size={20} />
                                            </button>
                                            <UncontrolledPopover trigger="focus" placement="bottom" target={"q" + question.sentenceNo}

                                            >
                                              <PopoverBody style={{borderColor: 'green !important', color: 'white', backgroundColor:'green'}}>
                                                <FormattedMessage id={"translate"}/>: {question.translatingQuestion !== null ? question.translatingQuestion : <FormattedMessage id={"no.translate"}/>}
                                              </PopoverBody>
                                            </UncontrolledPopover>
                                            {/*end phần dịch câu hỏi*/}
                                          </h5>
                                          {this.cutAnswerToChoose(question.answersToChoose).map((subAnswer, indexSubAnswer) =>
                                            <>
                                              <Col xs="12" md="12" sm="12" lg="12" style={{display: 'flex', padding: '0%'}}>
                                                <Col xs="1" md="2" sm="2" lg="1" style={{padding: '0%'}}>
                                                  {indexSubAnswer === question.indexCorrectAnswer ?
                                                    <Icon.Check size={25} style={{color: 'green'}}/> : null
                                                  }
                                                  {indexSubAnswer === question.indexIncorrectAnswer ?
                                                    <Icon.X size={25} style={{color: '#b21717'}} /> : null
                                                  }
                                                </Col>
                                                <Col xs="11" md="10" sm="10" lg="11" style={{padding: '0%', display: 'flex'}}>
                                                  <FormGroup check style={{marginBottom:'4%'}}>
                                                    <Label  check className={classes.labelRadioButton} style={{cursor: 'pointer'}}>
                                                      <Input type="radio" name={question.sentenceNo}
                                                             checked={this.checkingAnswerChoosen(question.indexIncorrectAnswer,
                                                               question.indexCorrectAnswer, indexSubAnswer)}
                                                      />
                                                      ({ indexSubAnswer===0 ? "A" : (indexSubAnswer===1 ? "B" : (indexSubAnswer===2 ? "C" : "D")) }) {this.upperCaseFirstLetter(subAnswer)}

                                                    </Label>
                                                    {/*phần dịch câu đáp án*/}
                                                    <button style={{border: 'none', backgroundColor: 'white', padding: '0', marginLeft: '10px'}}
                                                            id={"a" + question.sentenceNo + indexSubAnswer} className="mr-1 mb-1">
                                                      <Icon.AlertCircle  size={20} />
                                                    </button>
                                                    <UncontrolledPopover trigger="focus" placement="bottom" target={"a" + question.sentenceNo + indexSubAnswer}

                                                    >
                                                      <PopoverBody style={{borderColor: 'green !important', color: 'white', backgroundColor:'green'}}>
                                                        <FormattedMessage id={"translate"}/>: {indexSubAnswer===0 ? question.translatingQuesA : (indexSubAnswer===1 ? question.translatingQuesB : (indexSubAnswer===2 ? question.translatingQuesC : question.translatingQuesD))}
                                                      </PopoverBody>
                                                    </UncontrolledPopover>
                                                    {/*phần dịch câu đáp án*/}
                                                  </FormGroup>

                                                </Col>
                                              </Col>
                                            </>
                                          )}
                                          <Col xs="12" md="12" sm="12" lg="12" style={{marginBottom: '10%'}}>
                                            <fieldset className={classes.fieldsetDescription}>
                                              <legend className={classes.legendDescription}><FormattedMessage id={"description"}/></legend>
                                              <i>{null === question.description ? <>><FormattedMessage id={"no.explanation"}/></> : question.description}</i>
                                            </fieldset>
                                          </Col>
                                        </Col>

                                      </>
                                    )}
                                  </Row>
                                </Col>
                              </Row>
                            </>
                          )}
                        </div>
                        {/*--------------------------------------- end PART7--------------------------------------------*/}
                      </>
                      : null
                    }
                  </>
                  :
                showAnswer === false ?
                <>
                  {listQuestionMinitest.length > 0 ?
                    <>
                      {/*----------------------------------------PART5-----------------------------------------------*/}
                      <div className="mt-3">
                        <h1 id="PART5" style={{fontWeight: '700'}}>PART 5</h1>
                        <h5>Directions: A word or phrase is missing in each of the sentences below. Four answer choices
                          are given below each

                          sentence. Select the best answer to complete the sentence. Then mark the letter (A), (B), (C),
                          or (D) on your answer sheet</h5>

                        <Row style={{marginTop: '3%', marginLeft: '6%'}}>
                          {listQuestionMinitest[4].listCategoryMinitestDTOS.map((category, indexCategory) =>{
                            let smallItem = category.listQuestionMinitestDTOS.map((question, indexQuestion)=>{
                              return (
                                <Col id={String(question.sentenceNo)} xs={6} style={{marginTop:'2%'}}>
                                  <h5 style={{fontWeight: '700'}}>{question.sentenceNo}. {question.name}</h5>
                                  <div className="w-100">
                                    {/*{question ? question.answersToChoose.split("|").map((subAnswer, indexSubAnswer) =>*/}
                                    {question ? this.cutAnswerToChoose(question.answersToChoose).map((subAnswer, indexSubAnswer) =>
                                      <FormGroup check style={{marginLeft: '10%', marginBottom: '3%'}}>
                                        <Label check className={classes.labelRadioButton} style={{cursor: 'pointer'}}>
                                          <Input type="radio" name={question.sentenceNo}
                                                 onChange={this.onChangeValue}
                                                 value={subAnswer + "|" + question.id + "." + indexSubAnswer
                                                 + "|" + listQuestionMinitest[4].listCategoryMinitestDTOS[0].categoryId + "." + question.sentenceNo
                                                 + "|" + listQuestionMinitest[4].partName}
                                          />
                                          ({indexSubAnswer === 0 ? "A" : (indexSubAnswer === 1 ? "B" : (indexSubAnswer === 2 ? "C" : "D"))}) {this.upperCaseFirstLetter(subAnswer)}
                                        </Label>
                                      </FormGroup>
                                    ) : null}
                                  </div>
                                </Col>
                              )
                            })
                            return (<>{smallItem}</>)
                          }
                          )}
                        </Row>

                      </div>
                      {/*----------------------------------------end PART5-----------------------------------------------*/}


                      {/*----------------------------------------PART6-----------------------------------------------*/}
                      <div className="mt-3">
                        <h1 id="PART6" style={{fontWeight: '700'}}>PART 6</h1>
                        <h5>Directions: Read the text that follow. A word or phrase is missing in some of the sentences.
                          Four answer choices are given

                          below each of the sentences. Select the best answer to complete the text. Then mark the letter
                          (A), (B), (C), or (D) on your

                          answer sheet</h5>

                        {listQuestionMinitest[5].listCategoryMinitestDTOS.map((category, indexCategory) =>
                          <>
                            <h4 style={{fontWeight: '700', marginTop: '5%'}}>Part 6.{indexCategory + 1}</h4>
                            <div style={{marginLeft: '7%', marginTop:'2%'}}>
                              <h5 style={{fontWeight: '700'}}>
                              {category.listQuestionMinitestDTOS[0].name}-
                              {category.listQuestionMinitestDTOS[category.listQuestionMinitestDTOS.length - 1].name}.
                              Refer to the following email
                            </h5>
                            </div>
                            <Row style={{marginTop: '3%', marginLeft: '9%'}}>
                              <Col xs={12}>
                                <div style={{backgroundColor: '#d3d3d347', padding: '10px',lineHeight: '2' }}>
                                  {category.pathFile1}
                                </div>
                              </Col>
                                {category.listQuestionMinitestDTOS.map((question, indexQuestion) =>
                                  <Col id={String(question.sentenceNo)} xs={6} style={{marginTop:'2%'}}>
                                    <h5 style={{fontWeight:'700'}}>{question.sentenceNo}. {question.name}</h5>
                                    {this.cutAnswerToChoose(question.answersToChoose).map((subAnswer, indexSubAnswer) =>
                                      <FormGroup check style={{marginLeft: '8%', marginBottom: '5%'}}>
                                        <Label check className={classes.labelRadioButton} style={{cursor: 'pointer'}}>
                                          <Input type="radio" name={question.sentenceNo}
                                                 onChange={this.onChangeValue}
                                                 value={subAnswer + "|" + question.id + "." + indexSubAnswer
                                                 + "|" + category.categoryId + "." + question.sentenceNo
                                                 + "|" + listQuestionMinitest[5].partName}
                                          />
                                          ({indexSubAnswer === 0 ? "A" : (indexSubAnswer === 1 ? "B" : (indexSubAnswer === 2 ? "C" : "D"))}) {this.upperCaseFirstLetter(subAnswer)}
                                        </Label>
                                      </FormGroup>
                                    )}
                                  </Col>
                                )}

                            </Row>
                          </>
                        )}
                      </div>
                      {/*---------------------------------------end PART6--------------------------------------------*/}


                      {/*---------------------------------------PART7--------------------------------------------*/}
                      <div className="mt-3">
                        <h3 id="PART7" style={{fontWeight: '700'}}>PART 7</h3>
                        <h5>Directions: In this part you will read a selection of texts, such as magazine and newspaper
                          articles, letters and

                          advertisements. Each text is followed by several questions. Select the best answer for each
                          question and mark the letter

                          (A), (B), (C), or (D) on your answer sheet</h5>

                        {listQuestionMinitest[6].listCategoryMinitestDTOS.map((category, indexCategory) =>
                          <>
                            <h4 style={{fontWeight: '700', marginTop: '5%'}}>Part 7.{indexCategory + 1}</h4>
                            <div style={{marginLeft: '10%', marginTop:'2%'}}>
                              <h5 style={{fontWeight: '700'}}>
                                {category.listQuestionMinitestDTOS[0].sentenceNo}-
                                {category.listQuestionMinitestDTOS[category.listQuestionMinitestDTOS.length - 1].sentenceNo}.
                                Refer to the following email
                              </h5>
                            </div>
                            <Row style={{marginTop: '3%'}}>
                              {category.pathFile1.includes("https://") === true ?
                                <Col xs="12" md="12" sm="12" lg="12">
                                  <img className={classes.imgClass}
                                       style={{width: '100%', border: '5px solid lightgray'}}
                                       src={category.pathFile1}/>
                                </Col>
                                :
                                <Col xs="12" md="12" sm="12" lg="12">
                                  <div style={{backgroundColor: '#d3d3d347', padding: '10px', lineHeight:'2'}}>
                                    {category.pathFile1}
                                  </div>
                                </Col>
                              }
                              <Col xs={12} style={{marginTop: '3%'}}>
                                <Row  style={{marginTop: '3%', marginLeft: '10%'}}>
                                {category.listQuestionMinitestDTOS.map((question, indexQuestion) =>
                                    <Col xs={6} id={String(question.sentenceNo)}>
                                      <h5 style={{fontWeight:'700'}}>{question.sentenceNo}. {question.name}</h5>
                                      {this.cutAnswerToChoose(question.answersToChoose).map((subAnswer, indexSubAnswer) =>
                                        <FormGroup check style={{marginLeft: '8%', marginBottom: '5%'}}>
                                          <Label check className={classes.labelRadioButton} style={{cursor: 'pointer'}}>
                                            <Input type="radio" name={question.sentenceNo}
                                                   onChange={this.onChangeValue}
                                                   value={subAnswer + "|" + question.id + "." + indexSubAnswer
                                                   + "|" + category.categoryId + "." + question.sentenceNo
                                                   + "|" + listQuestionMinitest[6].partName}
                                            />
                                            ({indexSubAnswer === 0 ? "A" : (indexSubAnswer === 1 ? "B" : (indexSubAnswer === 2 ? "C" : "D"))}) {this.upperCaseFirstLetter(subAnswer)}
                                          </Label>
                                        </FormGroup>
                                      )}
                                    </Col>
                                )}
                                </Row>
                              </Col>
                            </Row>
                          </>
                        )}
                      </div>
                      {/*--------------------------------------- end PART7--------------------------------------------*/}
                    </>
                    : null
                  }
                </>
                :
                <>
                  {listQuestionAndAnswerMinitest.length > 0 ?
                    <>
                      {/*----------------------------------------PART5-----------------------------------------------*/}
                      <div className="mt-3">
                        <h1 id="PART5" style={{fontWeight: '700'}}>PART 5</h1>

                        <Row style={{marginTop: '3%', marginLeft: '6%'}}>
                          {listQuestionAndAnswerMinitest[4].listCategoryMinitestDTOS.map((category, indexCategory) =>{
                            {/*<h4 style={{fontWeight: '700', marginTop: '5%'}}>Đoạn {indexCategory + 1}.</h4>*/}
                            let smallItem = category.listQuestionMinitestDTOS.map((question, indexQuestion)=>{
                              return (
                                <>
                                  <Col id={String(question.sentenceNo)} xs={6}>
                                    <h6 style={{fontWeight: '700'}}>{question.sentenceNo}. {question.name}
                                      {/*phần dịch câu hỏi*/}
                                      <button style={{border: 'none', backgroundColor: 'white', padding: '0', marginLeft: '2%'}}
                                              id={"q" + question.sentenceNo} className="mr-1 mb-1">
                                        <Icon.AlertCircle  size={20} />
                                      </button>
                                      <UncontrolledPopover trigger="focus" placement="bottom" target={"q" + question.sentenceNo}

                                      >
                                        <PopoverBody style={{borderColor: 'green !important', color: 'white', backgroundColor:'green'}}>
                                          <FormattedMessage id={"translate"}/>: {question.translatingQuestion !== null ? question.translatingQuestion : <FormattedMessage id={"no.translate"}/>}
                                        </PopoverBody>
                                      </UncontrolledPopover>
                                      {/*end phần dịch câu hỏi*/}
                                    </h6>

                                      {/*{question ? question.answersToChoose.split("|").map((subAnswer, indexSubAnswer) =>*/}
                                    {question ? this.cutAnswerToChoose(question.answersToChoose).map((subAnswer, indexSubAnswer) =>
                                        <>
                                          <Col xs="12" md="12" sm="12" lg="12" style={{display: 'flex', padding: '0%'}}>
                                            <Col xs="1" md="2" sm="2" lg="1" style={{padding: '0%'}}>
                                              {indexSubAnswer === question.indexCorrectAnswer ?
                                                <Icon.Check size={25} style={{color: 'green'}}/> : null
                                              }
                                              {indexSubAnswer === question.indexIncorrectAnswer ?
                                                <Icon.X size={25} style={{color: '#b21717'}} /> : null
                                              }
                                              {/*<Icon.Check size={25} style={{color: 'green'}}/>*/}
                                            </Col>
                                            <Col xs="11" md="10" sm="10" lg="11" style={{padding: '0%', display: 'flex'}}>
                                              <FormGroup check style={{marginBottom:'4%'}}>
                                                <Label  check className={classes.labelRadioButton} style={{cursor: 'pointer'}}>
                                                  <Input type="radio" name={question.sentenceNo}
                                                         checked={this.checkingAnswerChoosen(question.indexIncorrectAnswer,
                                                           question.indexCorrectAnswer, indexSubAnswer)}
                                                  />
                                                  ({ indexSubAnswer===0 ? "A" : (indexSubAnswer===1 ? "B" : (indexSubAnswer===2 ? "C" : "D")) }) {this.upperCaseFirstLetter(subAnswer)}
                                                </Label>
                                              </FormGroup>
                                                {/*phần dịch câu đáp án*/}
                                                <button style={{border: 'none', backgroundColor: 'white', padding: '0', marginLeft: '10px'}}
                                                        id={"a" + question.sentenceNo + indexSubAnswer} className="mr-1 mb-1">
                                                  <Icon.AlertCircle  size={20} />
                                                </button>
                                                <UncontrolledPopover trigger="focus" placement="bottom" target={"a" + question.sentenceNo + indexSubAnswer}>
                                                  <PopoverBody style={{borderColor: 'green !important', color: 'white', backgroundColor:'green'}}>
                                                    <FormattedMessage id={"translate"}/>: {indexSubAnswer===0 ? question.translatingQuesA : (indexSubAnswer===1 ? question.translatingQuesB : (indexSubAnswer===2 ? question.translatingQuesC : question.translatingQuesD))}
                                                  </PopoverBody>
                                                </UncontrolledPopover>
                                                {/*phần dịch câu đáp án*/}
                                            </Col>

                                          </Col>
                                        </>
                                      ) : null}
                                    <Col xs="12" md="12" sm="12" lg="12" style={{marginBottom: '10%', paddingRight: '10%'}}>
                                      <fieldset className={classes.fieldsetDescription}>
                                        <legend className={classes.legendDescription}><FormattedMessage id={"description"}/></legend>
                                        <i>{null === question.description ? <>><FormattedMessage id={"no.explanation"}/></> : question.description}</i>
                                      </fieldset>
                                    </Col>
                                  </Col>

                                </>
                              )
                            })
                            return (<>{smallItem}</>)
                          }
                          )}
                        </Row>

                      </div>
                      {/*----------------------------------------end PART5-----------------------------------------------*/}


                      {/*----------------------------------------PART6-----------------------------------------------*/}
                      <div className="mt-3">
                        <h1 id="PART6" style={{fontWeight: '700'}}>PART 6</h1>

                        {listQuestionAndAnswerMinitest[5].listCategoryMinitestDTOS.map((category, indexCategory) =>
                          <>
                            <h4 style={{fontWeight: '700', marginTop: '5%'}}>Part 6.{indexCategory + 1}</h4>
                            <div style={{marginLeft: '7%'}}>
                              <h5 style={{fontWeight:'700'}}>
                              {category.listQuestionMinitestDTOS[0].name}-
                              {category.listQuestionMinitestDTOS[category.listQuestionMinitestDTOS.length - 1].name}:
                              refer to the following email
                              </h5>
                            </div>
                            <Row style={{marginTop: '3%', marginLeft: '6%'}}>
                              <Col xs={12}>
                                <div style={{backgroundColor: '#d3d3d347', padding: '10px',lineHeight:'2'}}>
                                  {category.pathFile1}
                                </div>
                              </Col>

                                {category.listQuestionMinitestDTOS.map((question, indexQuestion) =>
                                  <>
                                    <Col id={String(question.sentenceNo)} xs={6} style={{marginTop:'1%'}}>
                                      <h5 style={{fontWeight:'700'}}>{question.sentenceNo}. {question.name}</h5>
                                      {this.cutAnswerToChoose(question.answersToChoose).map((subAnswer, indexSubAnswer) =>
                                        <>
                                          <Col xs="12" md="12" sm="12" lg="12" style={{display: 'flex', padding: '0%'}}>
                                            <Col xs="1" md="2" sm="2" lg="1" style={{padding: '0%'}}>
                                              {indexSubAnswer === question.indexCorrectAnswer ?
                                                <Icon.Check size={25} style={{color: 'green'}}/> : null
                                              }
                                              {indexSubAnswer === question.indexIncorrectAnswer ?
                                                <Icon.X size={25} style={{color: '#b21717'}} /> : null
                                              }
                                            </Col>
                                            <Col xs="11" md="10" sm="10" lg="11" style={{padding: '0%', display: 'flex'}}>
                                              <FormGroup check style={{marginBottom:'4%'}}>
                                                <Label  check className={classes.labelRadioButton} style={{cursor: 'pointer'}}>
                                                  <Input type="radio" name={question.sentenceNo}
                                                         checked={this.checkingAnswerChoosen(question.indexIncorrectAnswer,
                                                           question.indexCorrectAnswer, indexSubAnswer)}
                                                  />
                                                  ({ indexSubAnswer===0 ? "A" : (indexSubAnswer===1 ? "B" : (indexSubAnswer===2 ? "C" : "D")) }) {this.upperCaseFirstLetter(subAnswer)}

                                                </Label>
                                                {/*phần dịch câu đáp án*/}
                                                <button style={{border: 'none', backgroundColor: 'white', padding: '0', marginLeft: '10px'}}
                                                        id={"a" + question.sentenceNo + indexSubAnswer} className="mr-1 mb-1">
                                                  <Icon.AlertCircle  size={20} />
                                                </button>
                                                <UncontrolledPopover trigger="focus" placement="bottom" target={"a" + question.sentenceNo + indexSubAnswer}

                                                >
                                                  <PopoverBody style={{borderColor: 'green !important', color: 'white', backgroundColor:'green'}}>
                                                    <FormattedMessage id={"translate"}/>: {indexSubAnswer===0 ? question.translatingQuesA : (indexSubAnswer===1 ? question.translatingQuesB : (indexSubAnswer===2 ? question.translatingQuesC : question.translatingQuesD))}
                                                  </PopoverBody>
                                                </UncontrolledPopover>
                                                {/*phần dịch câu đáp án*/}
                                              </FormGroup>

                                            </Col>
                                          </Col>
                                        </>
                                      )}
                                      <Col xs="12" md="12" sm="12" lg="12" style={{marginBottom: '10%'}}>
                                        <fieldset className={classes.fieldsetDescription}>
                                          <legend className={classes.legendDescription}><FormattedMessage id={"description"}/></legend>
                                          <i>{null === question.description ? <>><FormattedMessage id={"no.explanation"}/> </> : question.description}</i>
                                        </fieldset>
                                      </Col>
                                    </Col>

                                  </>
                                )}
                            </Row>
                          </>
                        )}
                      </div>
                      {/*---------------------------------------end PART6--------------------------------------------*/}


                      {/*---------------------------------------PART7--------------------------------------------*/}
                      <div className="mt-3">
                        <h3 id="PART7" style={{fontWeight: '700'}}>PART 7</h3>

                        {listQuestionAndAnswerMinitest[6].listCategoryMinitestDTOS.map((category, indexCategory) =>
                          <>
                            <h4 style={{fontWeight: '700', marginTop: '5%'}}>Part 7.{indexCategory + 1}</h4>
                            <Row style={{marginTop: '3%'}}>
                              { category.pathFile1.includes("https://") === true ?
                                <Col xs="12" md="12" sm="12" lg="12">
                                  <img className={classes.imgClass}
                                       style={{width: '100%', border: '5px solid lightgray'}}
                                       src={category.pathFile1}/>
                                </Col>
                                :
                                <Col xs="12" md="12" sm="12" lg="12">
                                  <div style={{backgroundColor: '#d3d3d347', padding: '10px', lineHeight:'2'}}>
                                    {category.pathFile1}
                                  </div>
                                </Col>
                              }
                              <Col xs="12" md="12" sm="12" lg="12" style={{marginTop: '5%'}}>
                                <Row style={{marginTop: '3%', marginLeft: '5%'}}>
                                {category.listQuestionMinitestDTOS.map((question, indexQuestion) =>
                                  <>

                                      <Col xs="6" md="6" sm="6" lg="6"  id={String(question.sentenceNo)} >
                                        <h5 style={{fontWeight:'700'}}>{question.sentenceNo}. {question.name}
                                          {/*phần dịch câu hỏi*/}
                                          <button style={{border: 'none', backgroundColor: 'white', padding: '0', marginLeft: '10px'}}
                                                  id={"q" + question.sentenceNo} className="mr-1 mb-1">
                                            <Icon.AlertCircle  size={20} />
                                          </button>
                                          <UncontrolledPopover trigger="focus" placement="bottom" target={"q" + question.sentenceNo}

                                          >
                                            <PopoverBody style={{borderColor: 'green !important', color: 'white', backgroundColor:'green'}}>
                                              <FormattedMessage id={"translate"}/>: {question.translatingQuestion !== null ? question.translatingQuestion : <FormattedMessage id={"no.translate"}/>}
                                            </PopoverBody>
                                          </UncontrolledPopover>
                                          {/*end phần dịch câu hỏi*/}
                                        </h5>
                                        {this.cutAnswerToChoose(question.answersToChoose).map((subAnswer, indexSubAnswer) =>
                                          <>
                                            <Col xs="12" md="12" sm="12" lg="12" style={{display: 'flex', padding: '0%'}}>
                                              <Col xs="1" md="2" sm="2" lg="1" style={{padding: '0%'}}>
                                                {indexSubAnswer === question.indexCorrectAnswer ?
                                                  <Icon.Check size={25} style={{color: 'green'}}/> : null
                                                }
                                                {indexSubAnswer === question.indexIncorrectAnswer ?
                                                  <Icon.X size={25} style={{color: '#b21717'}} /> : null
                                                }
                                              </Col>
                                              <Col xs="11" md="10" sm="10" lg="11" style={{padding: '0%', display: 'flex'}}>
                                                <FormGroup check style={{marginBottom:'4%'}}>
                                                  <Label  check className={classes.labelRadioButton} style={{cursor: 'pointer'}}>
                                                    <Input type="radio" name={question.sentenceNo}
                                                           checked={this.checkingAnswerChoosen(question.indexIncorrectAnswer,
                                                             question.indexCorrectAnswer, indexSubAnswer)}
                                                    />
                                                    ({ indexSubAnswer===0 ? "A" : (indexSubAnswer===1 ? "B" : (indexSubAnswer===2 ? "C" : "D")) }) {this.upperCaseFirstLetter(subAnswer)}

                                                  </Label>
                                                  {/*phần dịch câu đáp án*/}
                                                  <button style={{border: 'none', backgroundColor: 'white', padding: '0', marginLeft: '10px'}}
                                                          id={"a" + question.sentenceNo + indexSubAnswer} className="mr-1 mb-1">
                                                    <Icon.AlertCircle  size={20} />
                                                  </button>
                                                  <UncontrolledPopover trigger="focus" placement="bottom" target={"a" + question.sentenceNo + indexSubAnswer}

                                                  >
                                                    <PopoverBody style={{borderColor: 'green !important', color: 'white', backgroundColor:'green'}}>
                                                      <FormattedMessage id={"translate"}/>: {indexSubAnswer===0 ? question.translatingQuesA : (indexSubAnswer===1 ? question.translatingQuesB : (indexSubAnswer===2 ? question.translatingQuesC : question.translatingQuesD))}
                                                    </PopoverBody>
                                                  </UncontrolledPopover>
                                                  {/*phần dịch câu đáp án*/}
                                                </FormGroup>

                                              </Col>
                                            </Col>
                                          </>
                                        )}
                                        <Col xs="12" md="12" sm="12" lg="12" style={{marginBottom: '10%'}}>
                                          <fieldset className={classes.fieldsetDescription}>
                                            <legend className={classes.legendDescription}><FormattedMessage id={"description"}/></legend>
                                            <i>{null === question.description ? <>><FormattedMessage id={"no.explanation"}/></> : question.description}</i>
                                          </fieldset>
                                        </Col>
                                      </Col>

                                  </>
                                )}
                                </Row>
                              </Col>
                            </Row>
                          </>
                        )}
                      </div>
                      {/*--------------------------------------- end PART7--------------------------------------------*/}
                    </>
                    : null
                  }
                </>
              }
            </div>
          </Row>
        </div>
      </div>
        </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    listQuestionMinitest : state.doTestReducer.listQuestionFullTest,
    listQuestionListeningChoosenDTOS: state.doTestReducer.listQuestionListeningChoosenDTOS,
    listQuestionReadingChoosenDTOS: state.doTestReducer.listQuestionReadingChoosenDTOS,
    listQuestionAndAnswerMinitest: state.doTestReducer.listQuestionAndAnswerFullTest,
    loadingQuestion: state.doTestReducer.loadingQuestion,
    loadingSubmitAnswer: state.doTestReducer.loadingSubmitAnswer,
    showAnswer: state.doTestReducer.showAnswer,
    viewHistory: state.historyTestReducer.viewHistory,
    listHistoryFullTest: state.historyTestReducer.listQuestionAndAnswerFullTest,
    loadingResult: state.historyTestReducer.loadingResult
  }
}
const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    testAction : bindActionCreators(testAction , dispatch),
  }
}
const withConnect = connect(mapStateToProps, mapDispatchToProps)

export default compose(withStyles(styles), withConnect)(ReadingTest);
