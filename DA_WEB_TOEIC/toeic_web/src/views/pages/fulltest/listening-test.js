import React, { Component } from 'react';
import {connect} from "react-redux";
import {bindActionCreators, compose} from "redux";
import {Badge, Col, Container, Form, FormGroup, Input, Label, Row, Spinner,UncontrolledPopover, Button,PopoverHeader,PopoverBody} from "reactstrap"
import styles from "./style";
import {withStyles} from "@material-ui/core";
import {ReactPlayerReading} from "../Practices/ListeningToeicShortTalkingPractice/style";

import testAction from "../../../redux/actions/do-test-management";
import * as Icon from "react-feather";
import * as Styles from "../Practices/Listening/ListeningWordFill/question";
import MultiPlayerMP3 from "./MultiPlayerMP3";
import {CODE_PART1_LF, CODE_PART2_LF} from "../../../commons/constants/CONSTANTS";
import {FormattedMessage} from "react-intl";

class ListeningTest extends Component {

  constructor(props) {
    super(props);
    this.state = {
      load: false,
      listQuesListening: [],
      listQuesReading: [],
      listSttQuesListeningChoosen: this.props.listSttQuestionListeningChoosen,
      listSttQuesReadingChoosen: this.props.listSttQuestionReadingChoosen,
      listQuestionListeningPrimitive: [],
      play: false,
      playing: true,
      secondListenAgain: 0,
      currentPlaying: true,
      play0: false,
      play2: false,
      play3: false,
      play4: false,
      play1: false,
      indexListenAgain: -1,
      stt: 0,
      pathFile: "",
      audio: new Audio(""),
      playWhenViewHistory: false,
      playAfterSubmit: false
    }
  }

  componentDidMount() {
    this.setState({
      load: true
    })
    setTimeout(()=>{
      this.setState({
        load: false
      })
    }, 2000)
  }

  cutAnswerToChoose(answerToChoose){
    let char = 0;
    let answerArray = [];
    let subAnswer = "";

    let count=0,index=0;
    for( count=-1,index=-2; index != -1; count++,index=answerToChoose.indexOf("|",index+1) );

    for(let i=1;i<=count;i++){
      char = answerToChoose.indexOf("|");
      subAnswer = answerToChoose.substring(0, char);
      answerArray.push(subAnswer);
      answerToChoose = answerToChoose.substring(char + 1);
    }
    return answerArray;
  }

  lowerCaseAll =(string)=>{
    return string.toLowerCase();
  }
  upperCaseFirstLetter =(string)=>{
    string = this.lowerCaseAll(string);
    return string.trim().charAt(0).toUpperCase() + string.trim().slice(1);
  }

  cutSubAnswer(subAnswer){
    let index = subAnswer.indexOf(".");
    let title = subAnswer.substring(0, index);
    let content = subAnswer.substring(index+1);

    let array = [];
    array.push(title);
    array.push(content);
    return array;
  }

  cutInputValue(value){
    let index = value.indexOf("|");
    let title = value.substring(0, index);
    let content = value.substring(index+1);

    let array = [];
    array.push(title);
    array.push(content);
    return array;
  }

  compare =(a, b)=>{
    if ( a.sentenceNo < b.sentenceNo ){
      return -1;
    }
    if ( a.sentenceNo > b.sentenceNo ){
      return 1;
    }
    return 0;
  }

  onPlaying =()=>{
    if(this.state.pathFile !== ""){
      this.state.audio.pause();
    }
    this.setState({
      play: true
    })

  }

  onPlayingWhenViewHistory =()=>{
    if(this.state.pathFile !== ""){
      this.state.audio.pause();
    }
    this.setState({
      playWhenViewHistory: true
    })

  }

  onPlayingAfterSubmit =()=>{
    if(this.state.pathFile !== ""){
      this.state.audio.pause();
    }
    this.setState({
      playAfterSubmit: true,
      play0: false,
      play2: false,
      play3: false,
      play4: false,
      play1: false
    })

  }

  onPausing =()=>{
    this.setState({
      play: false
    })
  }

  onPausingWhenViewHistory =()=>{
    this.setState({
      playWhenViewHistory: false
    })
  }

  onPausingAfterSubmit =()=>{
    this.setState({
      playAfterSubmit: false
    })
  }

  onEnding =()=>{
    const {testAction} = this.props;
    const {doReadingAfterListeningFulltest} = testAction;
    doReadingAfterListeningFulltest(true)

  }

  getlistQuesListeningPrimitive =(question2)=> {
    const list = this.props.listQuestionMinitest;
    let {listQuestionListeningPrimitive} = this.state;

    //chọn lần 1
    if(listQuestionListeningPrimitive.length === 0){
      let listQuesListeningPrimitive = [];

      for(let i=0;i<list[0].listCategoryMinitestDTOS.length;i++){
        for(let j=0;j<list[0].listCategoryMinitestDTOS[i].listQuestionMinitestDTOS.length; j++){
          let question = {};
          question.id = Number(list[0].listCategoryMinitestDTOS[i].listQuestionMinitestDTOS[j].id);
          question.sentenceNo = Number(list[0].listCategoryMinitestDTOS[i].listQuestionMinitestDTOS[j].sentenceNo);
          question.answerChoosen = "null";
          question.indexSubAnswer = null;
          question.categoryId = Number(list[0].listCategoryMinitestDTOS[i].categoryId);
          question.partName = list[0].partName;
          if(question2.id === question.id){
            listQuesListeningPrimitive.push(question2)
          }
          else{
            listQuesListeningPrimitive.push(question)
          }
        }
      }

      for(let i=0;i<list[1].listCategoryMinitestDTOS.length;i++){
        for(let j=0; j< list[1].listCategoryMinitestDTOS[i].listQuestionMinitestDTOS.length; j++){
          let question = {};
          question.id = Number(list[1].listCategoryMinitestDTOS[i].listQuestionMinitestDTOS[j].id);
          question.sentenceNo = Number(list[1].listCategoryMinitestDTOS[i].listQuestionMinitestDTOS[j].sentenceNo);
          question.answerChoosen = "null";
          question.indexSubAnswer = null;
          question.categoryId = Number(list[1].listCategoryMinitestDTOS[i].categoryId);
          question.partName = list[1].partName;
          if(question2.id === question.id){
            listQuesListeningPrimitive.push(question2)
          }
          else{
            listQuesListeningPrimitive.push(question)
          }
        }
      }

      for(let i=0;i<list[2].listCategoryMinitestDTOS.length;i++){
        for(let j=0;j<list[2].listCategoryMinitestDTOS[i].listQuestionMinitestDTOS.length;j++){
          let question = {};
          question.id = Number(list[2].listCategoryMinitestDTOS[i].listQuestionMinitestDTOS[j].id);
          question.sentenceNo = Number(list[2].listCategoryMinitestDTOS[i].listQuestionMinitestDTOS[j].sentenceNo);
          question.answerChoosen = "null";
          question.indexSubAnswer = null;
          question.categoryId = Number(list[2].listCategoryMinitestDTOS[i].categoryId);
          question.partName = list[2].partName;
          if(question2.id === question.id){
            listQuesListeningPrimitive.push(question2)
          }
          else{
            listQuesListeningPrimitive.push(question)
          }
        }
      }

      for(let i=0;i<list[3].listCategoryMinitestDTOS.length;i++){
        for(let j=0;j<list[3].listCategoryMinitestDTOS[i].listQuestionMinitestDTOS.length;j++){
          let question = {};
          question.id = Number(list[3].listCategoryMinitestDTOS[i].listQuestionMinitestDTOS[j].id);
          question.sentenceNo = Number(list[3].listCategoryMinitestDTOS[i].listQuestionMinitestDTOS[j].sentenceNo);
          question.answerChoosen = "null";
          question.indexSubAnswer = null;
          question.categoryId = Number(list[3].listCategoryMinitestDTOS[i].categoryId);
          question.partName = list[3].partName;
          if(question2.id === question.id){
            listQuesListeningPrimitive.push(question2)
          }
          else{
            listQuesListeningPrimitive.push(question)
          }
        }
      }
      this.setState({
        listQuestionListeningPrimitive: listQuesListeningPrimitive
      })
      return listQuesListeningPrimitive;
    }
    // >= chọn lần 2
    else{
      for(let i=0; i<listQuestionListeningPrimitive.length;i++){
        if(question2.id === listQuestionListeningPrimitive[i].id){
          listQuestionListeningPrimitive[i].id = question2.id;
          listQuestionListeningPrimitive[i].sentenceNo = question2.sentenceNo;
          listQuestionListeningPrimitive[i].answerChoosen = question2.answerChoosen;
          listQuestionListeningPrimitive[i].indexSubAnswer = question2.indexSubAnswer;
          listQuestionListeningPrimitive[i].categoryId = question2.categoryId;
          listQuestionListeningPrimitive[i].partName = question2.partName;
        }
      }
      this.setState({
        listQuestionListeningPrimitive: listQuestionListeningPrimitive
      })
      return listQuestionListeningPrimitive;
    }


  }

  onChangeValue =(e)=> {
    const {testAction} = this.props;
    let question = {};
    let answerChoosen;
    let id = this.cutSubAnswer(this.cutInputValue(e.target.value)[1])[0];
    let indexSubAnswerAndCategoryIdAndSttAndPartName = this.cutSubAnswer(this.cutInputValue(e.target.value)[1])[1];
    let indexSubAnswer = this.cutInputValue(indexSubAnswerAndCategoryIdAndSttAndPartName)[0];
    if(indexSubAnswer === "0"){
      answerChoosen = "A"
    } else if(indexSubAnswer === "1"){
      answerChoosen = "B"
    } else if(indexSubAnswer === "2"){
      answerChoosen = "C"
    } else if(indexSubAnswer === "3"){
      answerChoosen = "D"
    }
    let categoryIdAndSttAndPartName = this.cutInputValue(indexSubAnswerAndCategoryIdAndSttAndPartName)[1];
    let categoryId = this.cutSubAnswer(categoryIdAndSttAndPartName)[0];
    let sttAndPartName = this.cutSubAnswer(categoryIdAndSttAndPartName)[1];
    let sentenceNo = this.cutInputValue(sttAndPartName)[0];
    let partName = this.cutInputValue(sttAndPartName)[1];

    question.id = Number(id);
    question.sentenceNo = Number(sentenceNo);
    question.answerChoosen = answerChoosen;
    question.indexSubAnswer = Number(indexSubAnswer);
    if(categoryId === "null"){
      question.categoryId = 0;
    }
    else{
      question.categoryId = Number(categoryId);
    }
    question.partName = partName;

    //list mới
    let list = this.getlistQuesListeningPrimitive(question);
    //list mới

    const {getListQuesListeningChoosen} = testAction;
    getListQuesListeningChoosen(list);

  }

  checkingAnswerChoosen =(indexIncorrectAnswer, indexCorrectAnswer, indexSubAnswer)=>{
    // th câu đúng
    if(indexIncorrectAnswer !== -1){
      if(indexSubAnswer === indexIncorrectAnswer){
        return true;
      }
      else{
        return false;
      }
    }
    //th câu sai
    else{
      if(indexSubAnswer === indexCorrectAnswer){
        return true;
      }
      else {
        return false;
      }
    }
  }

  ref = player => {
    this.player = player
  }


  cutStartTime(startTime, indexSubAnswer){
    let st = "";
    this.cutAnswerToChoose(startTime).map((a, i) => {
      if(i === indexSubAnswer) {
        st = this.cutAnswerToChoose(startTime)[i].trim();
      }
    })
    return st;
  }

  renderDescription=(description)=>{
    let cut = description.split('\n')
    let des = cut.map((itemQues, index)=>{
      return <p>{itemQues}</p>
    })
    return des;
  }

  playingListeningAfterSubmit = (question, indexQuestion) => {
    this.setState({
      playAfterSubmit: false
    })
    if(question.sentenceNo <= 31){
      if(question.pathFileQuesA || question.pathFileQuesB || question.pathFileQuesC || question.pathFileQuesD ){
        if(indexQuestion === 0){
          this.setState({
            indexListenAgain: indexQuestion,
            stt: question.sentenceNo,
            play0: true,
            play1: false,
            play2: false,
            play3: false,
            pathFile:question.pathFileQuesA
          }, ()=>{
            this.state.play0 === true ? this.InitAudio(question.pathFileQuesA).play() : this.InitAudio(question.pathFileQuesA).pause();
          })
        } else if(indexQuestion === 1){
          this.setState({
            indexListenAgain: indexQuestion,
            stt: question.sentenceNo,
            play0: false,
            play1: true,
            play2: false,
            play3: false,
            pathFile:question.pathFileQuesB
          }, ()=>{
            this.state.play1 === true ? this.InitAudio(question.pathFileQuesB).play() : this.InitAudio(question.pathFileQuesB).pause();
          })
        } else if(indexQuestion === 2){
          this.setState({
            indexListenAgain: indexQuestion,
            stt: question.sentenceNo,
            play0: false,
            play1: false,
            play2: true,
            play3: false,
            pathFile:question.pathFileQuesC
          },()=>{
            this.state.play2 === true ? this.InitAudio(question.pathFileQuesC).play() : this.InitAudio(question.pathFileQuesC).pause();
          })
        } else if(indexQuestion === 3){
          this.setState({
            indexListenAgain: indexQuestion,
            stt: question.sentenceNo,
            play0: false,
            play1: false,
            play2: false,
            play3: true,
            pathFile:question.pathFileQuesD
          },()=>{
            this.state.play3 === true ? this.InitAudio(question.pathFileQuesD).play() : this.InitAudio(question.pathFileQuesD).pause();
          })
        }
      }
    } else if(question.sentenceNo <= 100 && question.sentenceNo > 31){
      if( question.pathFileQues ){
        if(indexQuestion === 0){
          this.setState({
            indexListenAgain: indexQuestion,
            stt: question.sentenceNo,
            play0: true,
            play1: false,
            play2: false,
            play3: false,
            pathFile:question.pathFileQues
          }, ()=>{
            this.state.play0 === true ? this.InitAudio(question.pathFileQues).play() : this.InitAudio(question.pathFileQues).pause();
          })
        }
      }
    }
  }
  playingListeningWhenViewHistory = (question, indexQuestion) => {
    this.setState({
      playWhenViewHistory: false
    })
    if(question.sentenceNo <= 31){
      if(question.pathFileQuesA || question.pathFileQuesB || question.pathFileQuesC || question.pathFileQuesD ){
        if(indexQuestion === 0){
          this.setState({
            indexListenAgain: indexQuestion,
            stt: question.sentenceNo,
            play0: !this.state.play0,
            play1: false,
            play2: false,
            play3: false,
            pathFile:question.pathFileQuesA
          }, ()=>{
            this.state.play0 === true ? this.InitAudio(question.pathFileQuesA).play() : this.InitAudio(question.pathFileQuesA).pause();
          })
        } else if(indexQuestion === 1){
          this.setState({
            indexListenAgain: indexQuestion,
            stt: question.sentenceNo,
            play0: false,
            play1: !this.state.play1,
            play2: false,
            play3: false,
            pathFile:question.pathFileQuesB
          }, ()=>{
            this.state.play1 === true ? this.InitAudio(question.pathFileQuesB).play() : this.InitAudio(question.pathFileQuesB).pause();
          })
        } else if(indexQuestion === 2){
          this.setState({
            indexListenAgain: indexQuestion,
            stt: question.sentenceNo,
            play0: false,
            play1: false,
            play2: !this.state.play2,
            play3: false,
            pathFile:question.pathFileQuesC
          },()=>{
            this.state.play2 === true ? this.InitAudio(question.pathFileQuesC).play() : this.InitAudio(question.pathFileQuesC).pause();
          })
        } else if(indexQuestion === 3){
          this.setState({
            indexListenAgain: indexQuestion,
            stt: question.sentenceNo,
            play0: false,
            play1: false,
            play2: false,
            play3: !this.state.play3,
            pathFile:question.pathFileQuesD
          },()=>{
            this.state.play3 === true ? this.InitAudio(question.pathFileQuesD).play() : this.InitAudio(question.pathFileQuesD).pause();
          })
        }
      }
    } else if(question.sentenceNo <= 100 && question.sentenceNo > 31){
      if( question.pathFileQues ){
        if(indexQuestion === 0){
          this.setState({
            indexListenAgain: indexQuestion,
            stt: question.sentenceNo,
            play0: true,
            play1: false,
            play2: false,
            play3: false,
            pathFile:question.pathFileQues
          }, ()=>{
            this.state.play0 === true ? this.InitAudio(question.pathFileQues).play() : this.InitAudio(question.pathFileQues).pause();
          })
        }
      }
    }
  }

  InitAudio = (url) =>{
    let audio = new Audio(url)
    this.setState({
      audio: audio
    })
    audio.onended = () =>{
      this.setState({
        indexListenAgain: -1
      })
    }
    return audio;
  }

  loadingComponent = (
    <div style={{
      position: 'absolute',
      zIndex: 110,
      top: 0,
      left: 0,
      width: '100%',
      height: '100%',
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      background: 'rgba(255,255,255,0.8)',
    }}>
      <Spinner color="primary" className="reload-spinner"/>
    </div>
  );

  render() {
    const {classes, listQuestionMinitest, listQuestionAndAnswerMinitest, showAnswer, loadingQuestion,listHistoryFullTest,
      loadingSubmitAnswer} = this.props;
    return (
      <>
        <style>{`
          #iconTranslate:hover {
            stroke: green !important
          }

          #btnHighlightRepeat:focus-within {
            border: 1px solid black !important;
          }
        `}

        </style>
        <div className="mt-2">
          {
            listQuestionMinitest.length === 0 && loadingQuestion === false && !this.props.loadingResult ? this.loadingComponent  : null
          }
          {
            loadingSubmitAnswer === true && listQuestionAndAnswerMinitest.length === 0 ? this.loadingComponent  : null
          }
          <Badge style={{backgroundColor: "rgba(32 171 231)", width: "100%", height: "60%", marginLeft: "0%",
            padding: "10px 0 0 0", borderRadius:'10px'}} className="badge-xl block">
            <div>
              <div className="text-left" style={{marginLeft: '0px'}}>
                <div className={classes.titleModal}>
                  <h3 style={{color:'white', marginLeft: '2%', marginTop:'1%', fontSize:'2rem'}}>Listening Test</h3>
                </div>
              </div>
            </div>
          </Badge>
          <div style={{position: 'relative', margin: '0 1%'}}>
            {/*{this.state.load === true ? <>{loadingComponent}</> : ""}*/}
            <Row className={classes.bodyModal} style={{margin: '0 0% 0 0%', padding: '0 0%', width: "100%" }}>
              <div style={{margin:'3% 3%'}}>
                {
                  listQuestionAndAnswerMinitest.length > 0 || this.props.listHistoryFullTest.length > 0 ? null :
                    <h5>In the Listening test, you will be asked to demonstrate how well you understand spoken English. The entire Listening test

                      will last approximately 45 minutes. There are four parts, and directions are given for each part. You must mark your

                      answers on the separate answer sheet. Do not write your answer in your test book.</h5>
                }

                <div >
                  <div >

                    {
                      this.props.viewHistory  ?
                        <ReactPlayerReading
                          ref={this.ref}
                          url = {this.props.listHistoryFullTest ? this.props.listHistoryFullTest[0] && this.props.listHistoryFullTest[0].pathFile ?
                            this.props.listHistoryFullTest[0].pathFile :
                            this.props.listHistoryFullTest[1] &&  this.props.listHistoryFullTest[1].pathFile : "" }
                          playing={false}
                          controls={true}
                          onPlay={this.onPlayingWhenViewHistory}
                          onPause={this.onPausingWhenViewHistory}
                          onEnded={() => this.onEnding()}
                          config={{
                            file: {
                              attributes: {
                                controlsList: 'nodownload'
                              }
                            }
                          }}
                          width="320px"
                          height="100px"
                        >
                        </ReactPlayerReading>
                        :
                        <ReactPlayerReading
                          ref={this.ref}
                          url = {this.props.listQuestionMinitest ? this.props.listQuestionMinitest[0] && this.props.listQuestionMinitest[0].pathFile ? this.props.listQuestionMinitest[0].pathFile :
                            this.props.listQuestionMinitest[1] &&  this.props.listQuestionMinitest[1].pathFile : "" }
                          playing={this.props.loadingQuestion ? this.props.stopListening : false}
                          controls={listQuestionAndAnswerMinitest.length > 0 ? false : true}
                          onPlay={this.onPlaying}
                          onPause={this.onPausing}
                          onEnded={() => this.onEnding()}
                          config={{
                            file: {
                              attributes: {
                                controlsList: 'nodownload'
                              }
                            }
                          }}
                          width="320px"
                          height="100px"
                        >
                        </ReactPlayerReading>
                    }
                    {
                      listQuestionAndAnswerMinitest.length > 0 ?
                        <ReactPlayerReading
                          ref={this.ref}
                          url = {this.props.listQuestionMinitest ? this.props.listQuestionMinitest[0] && this.props.listQuestionMinitest[0].pathFile ? this.props.listQuestionMinitest[0].pathFile :
                            this.props.listQuestionMinitest[1] &&  this.props.listQuestionMinitest[1].pathFile : "" }
                          playing={this.state.playAfterSubmit}
                          controls={true}
                          onPlay={this.onPlayingAfterSubmit}
                          onPause={this.onPausingAfterSubmit}
                          onEnded={() => this.onEnding()}
                          config={{
                            file: {
                              attributes: {
                                controlsList: 'nodownload'
                              }
                            }
                          }}
                          width="320px"
                          height="100px"
                          style={{marginTop:'-15%'}}
                        >
                        </ReactPlayerReading> : null
                    }

                  </div>
                </div>

                {
                  this.props.viewHistory ?
                    <>
                      {listHistoryFullTest.length > 0 ?
                        <>
                          {/*--------------------------------------PART1----------------------------------------*/}
                          <div>
                            <h1 id="PART1" style={{fontWeight: '700'}}>PART 1</h1>
                            <>
                              {listHistoryFullTest[0].listCategoryMinitestDTOS.map((category, indexCategory) =>{
                                  let smallItem = category.listQuestionMinitestDTOS.map((question,indexQuestion)=>{
                                    return (
                                      <>
                                        <Row id={String(question.sentenceNo)}  style={{marginTop: '4%', marginLeft: '10%'}}>
                                          <Col xs={1}><h5 style={{fontWeight:'700'}}>{question.sentenceNo}.</h5></Col>
                                          <Col xs={6}>
                                            <img className={classes.imgClass} style={{width: '100%', border: '5px solid lightgray'}}
                                                 src={question.pathFile2 ? question.pathFile2 : ""}/>
                                          </Col>
                                          <Col xs="5">
                                            {question ? this.cutAnswerToChoose(question.answersToChoose).map((subAnswer, indexSubAnswer) =>
                                              <>
                                                <Col xs="12" md="12" sm="12" lg="12" style={{display: 'flex', padding: '0%', marginBottom:'3%'}}>
                                                  <Col xs="1" md="2" sm="2" lg="1" style={{padding: '0%', paddingRight:'12%'}}>
                                                    {indexSubAnswer === question.indexCorrectAnswer ?
                                                      <Icon.Check size={25} style={{color: 'green'}}/> : null
                                                    }
                                                    {indexSubAnswer === question.indexIncorrectAnswer ?
                                                      <Icon.X size={25} style={{color: '#b21717'}} /> : null
                                                    }
                                                  </Col>

                                                  <Col xs="11" md="10" sm="10" lg="11" style={{ display: 'flex'}}>
                                                    <FormGroup check style={{marginBottom:'4%', display:'contents'}}>
                                                      <Label  check className={classes.labelRadioButton} style={{cursor: 'pointer'}}>
                                                        <Input type="radio" name={question.sentenceNo}
                                                               checked={this.checkingAnswerChoosen(question.indexIncorrectAnswer,
                                                                 question.indexCorrectAnswer, indexSubAnswer)}
                                                        />
                                                        <>({ indexSubAnswer===0 ? "A" : (indexSubAnswer===1 ? "B" : (indexSubAnswer===2 ? "C" : "D")) }) </>
                                                      </Label>

                                                      <Button outline className={classes.buttonListenAgain} style={{padding: '0', display:'inline-flex', marginLeft:'10px', width:'auto'}} id='btnHighlightRepeat'
                                                              onClick={() => this.playingListeningWhenViewHistory(question ? question : {}, indexSubAnswer)}>
                                                        <div style={{padding: '0 5px', display:'inline-flex'}}>
                                                          {this.state.indexListenAgain === indexSubAnswer && this.state.stt === question.sentenceNo
                                                            ?
                                                            <Icon.Volume1 size={12} style={{marginTop:'2.5px', marginRight:'2px'}} />
                                                            :
                                                            <Icon.Play size={11} style={{marginTop:'2.5px', marginRight:'2px'}}/>
                                                          }
                                                          <p style={{marginTop:'-2px'}}><FormattedMessage id="listenAgain" tagName="data" /></p>

                                                        </div>

                                                      </Button>
                                                    </FormGroup>
                                                  </Col>

                                                </Col>
                                              </>

                                            ) : null }
                                          </Col>

                                        </Row>
                                        <Row>
                                          <Col xs="12" md="12" sm="12" lg="12" style={{marginBottom: '10%', marginTop:'2%'}}>
                                            <fieldset className={classes.fieldsetDescription}>
                                              <legend className={classes.legendDescription}><FormattedMessage id={"description"}/></legend>
                                              <i>{null === question.description ? <><FormattedMessage id={"no.explanation"}/></> : this.renderDescription(question.description)}</i>
                                            </fieldset>
                                          </Col>
                                        </Row>
                                      </>
                                    )
                                  })
                                  return (<>{smallItem}</>)
                                }
                              )}
                            </>
                          </div>
                          {/*--------------------------------------end PART1----------------------------------------*/}



                          {/*--------------------------------------PART2----------------------------------------*/}
                          <div className="mt-3" style={{marginTop: '5% !important'}}>
                            <h1 id="PART2" style={{fontWeight: '700'}}>PART 2</h1>
                            <Row style={{marginTop: '2%', marginLeft: '10%'}}>
                              {listHistoryFullTest[1].listCategoryMinitestDTOS.map((category, indexCategory) =>{
                                  let smallItem = category.listQuestionMinitestDTOS.map((question, indexQuestion)=>{
                                    return (
                                      <Col xs={4} style={{marginBottom:'2%'}}>
                                        <Row id={String(question.sentenceNo)} >
                                          <Col xs="1" md="1" sm="1" lg="1"><h5 style={{fontWeight: '700'}}>{question.sentenceNo}.</h5></Col>
                                          <Col xs="11" md="11" sm="11" lg="11" >
                                            {question ? this.cutAnswerToChoose(question.answersToChoose).map((subAnswer, indexSubAnswer) =>
                                              <>
                                                <Col xs="12" md="12" sm="12" lg="12" style={{display: 'flex', padding: '0%', marginBottom:'3%'}}>

                                                  <Col xs="2" md="2" sm="3" lg="2" style={{paddingLeft: '0%'}}>
                                                    {indexSubAnswer === question.indexCorrectAnswer ?
                                                      <Icon.Check size={25} style={{color: 'green'}}/> : null
                                                    }
                                                    {indexSubAnswer === question.indexIncorrectAnswer ?
                                                      <Icon.X size={25} style={{color: '#b21717'}} /> : null
                                                    }
                                                    {/*<Icon.Check size={25} style={{color: 'green'}}/>*/}
                                                  </Col>
                                                  <Col xs="10" md="10" sm="9" lg="10" style={{padding: '0%', display: 'flex'}}>
                                                    <FormGroup check style={{marginBottom:'4%'}}>
                                                      <Label  check className={classes.labelRadioButton} style={{cursor: 'pointer'}}>
                                                        <Input type="radio" name={question.sentenceNo}
                                                               checked={this.checkingAnswerChoosen(question.indexIncorrectAnswer,
                                                                 question.indexCorrectAnswer, indexSubAnswer)}
                                                        />
                                                        <>({ indexSubAnswer===0 ? "A" : (indexSubAnswer===1 ? "B" : (indexSubAnswer===2 ? "C" : "D")) }) </>
                                                      </Label>
                                                      <Button outline className={classes.buttonListenAgain} style={{padding: '0', display:'inline-flex', marginLeft:'10px', width:'auto'}} id='btnHighlightRepeat'
                                                              onClick={() => this.playingListeningWhenViewHistory(question ? question : {}, indexSubAnswer)}>
                                                        <div style={{padding: '0 5px', display:'inline-flex'}}>
                                                          {this.state.indexListenAgain === indexSubAnswer && this.state.stt === question.sentenceNo
                                                            ?
                                                            <Icon.Volume1 size={12} style={{marginTop:'2.5px', marginRight:'2px'}} />
                                                            :
                                                            <Icon.Play size={11} style={{marginTop:'2.5px', marginRight:'2px'}}/>
                                                          }
                                                          <p style={{marginTop:'-2px'}}><FormattedMessage id="listenAgain" tagName="data" /></p>
                                                        </div>
                                                      </Button>
                                                    </FormGroup>
                                                  </Col>

                                                </Col>
                                              </>
                                            ) : null }
                                          </Col>
                                          <Col xs="12" md="12" sm="12" lg="12" style={{marginBottom: '10%'}}>
                                            <fieldset className={classes.fieldsetDescription}>
                                              <legend className={classes.legendDescription}><FormattedMessage id={"description"}/></legend>
                                              <i>{null === question.description ? <><FormattedMessage id={"no.explanation"}/></> : this.renderDescription(question.description)}</i>
                                            </fieldset>
                                          </Col>
                                        </Row>
                                      </Col>
                                    )
                                  })
                                  return (<>{smallItem}</>)
                                }
                              )}
                            </Row>
                          </div>
                          {/*--------------------------------------end PART2----------------------------------------*/}



                          {/*-----------------------------------------PART3------------------------------------------*/}
                          <div className="mt-3">
                            <h1 id="PART3" style={{fontWeight: '700'}}>PART 3</h1>

                            {listHistoryFullTest[2].listCategoryMinitestDTOS.map((category, indexCategory) =>
                              <>
                                {null === category.pathFile2 ?
                                  <>
                                    <h4 style={{fontWeight: '700', marginTop: '5%'}}>Part 3.{indexCategory + 1}</h4>
                                    <Row style={{marginTop:'3%', marginLeft: '10%'}}>
                                      {category.listQuestionMinitestDTOS.map((question, indexQuestion) =>
                                        <>
                                          <Col xs="6" md="6" sm="6" lg="6" id={String(question.sentenceNo)}>
                                            <h5 style={{fontWeight:'700'}}>{question.sentenceNo}. {question.name}
                                              {/*phần dịch câu hỏi*/}
                                              <button style={{border: 'none', backgroundColor: 'white', padding: '0', marginLeft: '2%'}}
                                                      id={"q" + question.sentenceNo} className="mr-1 mb-1">
                                                <Icon.AlertCircle  size={20} />
                                              </button>
                                              <UncontrolledPopover trigger="focus" placement="bottom" target={"q" + question.sentenceNo}

                                              >
                                                <PopoverBody style={{borderColor: 'green !important', color: 'white', backgroundColor:'green'}}>
                                                  <FormattedMessage id={"translate"}/>: {question.translatingQuestion !== null ? question.translatingQuestion : <FormattedMessage id={"no.translate"}/>}
                                                </PopoverBody>
                                              </UncontrolledPopover>
                                              {/*end phần dịch câu hỏi*/}
                                              <Button outline className={classes.buttonListenAgain} style={{padding: '0', display:'inline-flex', marginLeft:'10px', width:'auto'}} id='btnHighlightRepeat'
                                                      onClick={() => this.playingListeningWhenViewHistory(question ? question : {}, 0)}>
                                                <div style={{padding: '0 5px', display:'inline-flex'}}>
                                                  { this.state.stt === question.sentenceNo
                                                    ?
                                                    <Icon.Volume1 size={12} style={{marginTop:'2.5px', marginRight:'2px'}} />
                                                    :
                                                    <Icon.Play size={11} style={{marginTop:'2.5px',  marginRight:'2px'}}/>
                                                  }
                                                  <p style={{marginTop:'-2px', fontSize:'0.8rem'}}><FormattedMessage id="listenAgain" tagName="data" /></p>
                                                </div>
                                              </Button>

                                            </h5>
                                            {this.cutAnswerToChoose(question.answersToChoose).map((subAnswer, indexSubAnswer) =>
                                              <>
                                                <Col xs="12" md="12" sm="12" lg="12" style={{display: 'flex', padding: '0%'}}>
                                                  <Col xs="1" md="2" sm="2" lg="1" style={{padding: '0%'}}>
                                                    {indexSubAnswer === question.indexCorrectAnswer ?
                                                      <Icon.Check size={25} style={{color: 'green'}}/> : null
                                                    }
                                                    {indexSubAnswer === question.indexIncorrectAnswer ?
                                                      <Icon.X size={25} style={{color: '#b21717'}} /> : null
                                                    }
                                                    {/*<Icon.Check size={25} style={{color: 'green'}}/>*/}
                                                  </Col>
                                                  <Col xs="11" md="10" sm="10" lg="11" style={{padding: '0%', display: 'flex'}}>
                                                    <FormGroup check style={{marginBottom:'4%'}}>
                                                      <Label  check className={classes.labelRadioButton} style={{cursor: 'pointer'}}>
                                                        <Input type="radio" name={question.sentenceNo}
                                                               checked={this.checkingAnswerChoosen(question.indexIncorrectAnswer,
                                                                 question.indexCorrectAnswer, indexSubAnswer)}
                                                        />
                                                        ({ indexSubAnswer===0 ? "A" : (indexSubAnswer===1 ? "B" : (indexSubAnswer===2 ? "C" : "D")) }) {this.upperCaseFirstLetter(subAnswer)}
                                                        {/*phần dịch câu đáp án*/}
                                                        <button style={{border: 'none', backgroundColor: 'white', padding: '0', marginLeft: '10px'}}
                                                                id={"a" + question.sentenceNo + indexSubAnswer} className="mr-1 mb-1">
                                                          <Icon.AlertCircle  size={20} />
                                                        </button>
                                                        <UncontrolledPopover trigger="focus" placement="bottom" target={"a" + question.sentenceNo + indexSubAnswer}

                                                        >
                                                          <PopoverBody style={{borderColor: 'green !important',color: 'white', backgroundColor:'green'}}>
                                                            <FormattedMessage id={"translate"}/>: {indexSubAnswer===0 ? question.translatingQuesA : (indexSubAnswer===1 ? question.translatingQuesB : (indexSubAnswer===2 ? question.translatingQuesC : question.translatingQuesD))}
                                                          </PopoverBody>
                                                        </UncontrolledPopover>
                                                        {/*end phần dịch câu đáp án*/}
                                                      </Label>

                                                    </FormGroup>

                                                  </Col>
                                                </Col>
                                              </>
                                            )}
                                            <Col xs={12} style={{marginBottom: '10%', marginTop:'2%'}}>
                                              <fieldset className={classes.fieldsetDescription}>
                                                <legend className={classes.legendDescription}><FormattedMessage id={"description"}/></legend>
                                                <i>{null === question.description ? <><FormattedMessage id={"no.explanation"}/> </> : this.renderDescription(question.description)}</i>
                                              </fieldset>
                                            </Col>
                                          </Col>
                                        </>
                                      )}
                                    </Row>
                                  </>
                                  :
                                  <>
                                    <h4 style={{fontWeight: '700', marginTop: '5%'}}>Part 3.{indexCategory + 1}</h4>
                                    <Row style={{marginTop:'3%', marginLeft: '10%'}}>
                                      <Col xs="6" md="6" sm="6" lg="6">
                                        <img className={classes.imgClass} style={{width: '100%', border: '5px solid lightgray'}}
                                             src={category.pathFile2}/>
                                        <h6 style={{textAlign:'center', fontStyle: 'italic'}}>
                                          (Question {category.listQuestionMinitestDTOS[0].sentenceNo}-
                                          {category.listQuestionMinitestDTOS[category.listQuestionMinitestDTOS.length - 1].sentenceNo})
                                        </h6>
                                      </Col>
                                      {category.listQuestionMinitestDTOS.map((question, indexQuestion) =>
                                        <>
                                          <Col id={String(question.sentenceNo)}  xs={6}>
                                            <h5 style={{fontWeight:'700'}}>{question.sentenceNo}. {question.name}
                                              {/*phần dịch câu hỏi*/}
                                              <button style={{border: 'none', backgroundColor: 'white', padding: '0', marginLeft: '2%'}}
                                                      id={"q" + question.sentenceNo} className="mr-1 mb-1">
                                                <Icon.AlertCircle  size={20} />
                                              </button>
                                              <UncontrolledPopover trigger="focus" placement="bottom" target={"q" + question.sentenceNo}

                                              >
                                                <PopoverBody style={{borderColor: 'green !important',color: 'white', backgroundColor:'green'}}>
                                                  <FormattedMessage id={"translate"}/>: {question.translatingQuestion !== null ? question.translatingQuestion : <FormattedMessage id={"no.translate"}/>}
                                                </PopoverBody>
                                              </UncontrolledPopover>
                                              {/*end phần dịch câu hỏi*/}
                                              <Button outline className={classes.buttonListenAgain} style={{padding: '0', display:'inline-flex', marginLeft:'10px', width:'auto'}} id='btnHighlightRepeat'
                                                      onClick={() => this.playingListeningWhenViewHistory(question ? question : {}, 0)}>
                                                <div style={{padding: '0 5px', display:'inline-flex'}}>
                                                  { this.state.stt === question.sentenceNo
                                                    ?
                                                    <Icon.Volume1 size={12} style={{marginTop:'2.5px', marginRight:'2px'}} />
                                                    :
                                                    <Icon.Play size={11} style={{marginTop:'2.5px',  marginRight:'2px'}}/>
                                                  }
                                                  <p style={{marginTop:'-2px', fontSize:'0.8rem'}}><FormattedMessage id="listenAgain" tagName="data" /></p>
                                                </div>
                                              </Button>
                                            </h5>
                                            {this.cutAnswerToChoose(question.answersToChoose).map((subAnswer, indexSubAnswer) =>
                                              <>
                                                <Col xs="12" md="12" sm="12" lg="12" style={{display: 'flex', padding: '0%'}}>
                                                  <Col xs="1" md="2" sm="2" lg="1" style={{padding: '0%'}}>
                                                    {indexSubAnswer === question.indexCorrectAnswer ?
                                                      <Icon.Check size={25} style={{color: 'green'}}/> : null
                                                    }
                                                    {indexSubAnswer === question.indexIncorrectAnswer ?
                                                      <Icon.X size={25} style={{color: '#b21717'}} /> : null
                                                    }
                                                    {/*<Icon.Check size={25} style={{color: 'green'}}/>*/}
                                                  </Col>
                                                  <Col xs="11" md="10" sm="10" lg="11" style={{padding: '0%', display: 'flex'}}>
                                                    <FormGroup check style={{marginBottom:'4%'}}>
                                                      <Label  check className={classes.labelRadioButton} style={{cursor: 'pointer'}}>
                                                        <Input type="radio" name={question.sentenceNo}
                                                               checked={this.checkingAnswerChoosen(question.indexIncorrectAnswer,
                                                                 question.indexCorrectAnswer, indexSubAnswer)}
                                                        />
                                                        ({ indexSubAnswer===0 ? "A" : (indexSubAnswer===1 ? "B" : (indexSubAnswer===2 ? "C" : "D")) }) {this.upperCaseFirstLetter(subAnswer)}
                                                        {/*phần dịch câu đáp án*/}
                                                        <button style={{border: 'none', backgroundColor: 'white', padding: '0', marginLeft: '10px'}}
                                                                id={"a" + question.sentenceNo + indexSubAnswer} className="mr-1 mb-1">
                                                          <Icon.AlertCircle  size={20} />
                                                        </button>
                                                        <UncontrolledPopover trigger="focus" placement="bottom" target={"a" + question.sentenceNo + indexSubAnswer}

                                                        >
                                                          <PopoverBody style={{borderColor: 'green !important', color: 'white', backgroundColor:'green'}}>
                                                            <FormattedMessage id={"translate"}/>: {indexSubAnswer===0 ? question.translatingQuesA : (indexSubAnswer===1 ? question.translatingQuesB : (indexSubAnswer===2 ? question.translatingQuesC : question.translatingQuesD))}
                                                          </PopoverBody>
                                                        </UncontrolledPopover>
                                                        {/*end phần dịch câu đáp án*/}
                                                      </Label>

                                                    </FormGroup>

                                                  </Col>
                                                </Col>
                                              </>
                                            )}
                                            <Col xs={12} style={{marginBottom: '10%', marginTop:'2%'}}>
                                              <fieldset className={classes.fieldsetDescription}>
                                                <legend className={classes.legendDescription}><FormattedMessage id={"description"}/></legend>
                                                <i>{null === question.description ? <><FormattedMessage id={"no.explanation"}/></> : this.renderDescription(question.description)}</i>
                                              </fieldset>
                                            </Col>
                                          </Col>

                                        </>
                                      )}
                                    </Row>
                                  </>
                                }
                              </>
                            )}
                          </div>
                          {/*-------------------------------------------end PART3---------------------------------------*/}


                          {/*--------------------------------------------PART4--------------------------------------------*/}
                          <div className="mt-3">
                            <h1 id="PART4" style={{fontWeight: '700'}}>PART 4</h1>

                            {listHistoryFullTest[3].listCategoryMinitestDTOS.map((category, indexCategory) =>
                              <>
                                {null === category.pathFile2 ?
                                  <>
                                    <h4 style={{fontWeight: '700', marginTop: '5%'}}>Part 4.{indexCategory + 1}</h4>
                                    <Row  style={{marginTop:'3%', marginLeft: '10%'}}>
                                      {category.listQuestionMinitestDTOS.map((question, indexQuestion) =>
                                        <>
                                          <Col xs={6} id={String(question.sentenceNo)} >
                                            <h5 style={{fontWeight:'700'}}>{question.sentenceNo}. {question.name}
                                              {/*phần dịch câu hỏi*/}
                                              <button style={{border: 'none', backgroundColor: 'white', padding: '0', marginLeft: '2%'}}
                                                      id={"q" + question.sentenceNo} className="mr-1 mb-1">
                                                <Icon.AlertCircle  size={20} />
                                              </button>
                                              <UncontrolledPopover trigger="focus" placement="bottom" target={"q" + question.sentenceNo}

                                              >
                                                <PopoverBody style={{borderColor: 'green !important',color: 'white', backgroundColor:'green'}}>
                                                  <FormattedMessage id={"translate"}/>: {question.translatingQuestion !== null ? question.translatingQuestion : <FormattedMessage id={"no.translate"}/>}
                                                </PopoverBody>
                                              </UncontrolledPopover>
                                              {/*end phần dịch câu hỏi*/}
                                              <Button outline className={classes.buttonListenAgain} style={{padding: '0', display:'inline-flex', marginLeft:'10px', width:'auto'}} id='btnHighlightRepeat'
                                                      onClick={() => this.playingListeningWhenViewHistory(question ? question : {}, 0)}>
                                                <div style={{padding: '0 5px', display:'inline-flex'}}>
                                                  { this.state.stt === question.sentenceNo
                                                    ?
                                                    <Icon.Volume1 size={12} style={{marginTop:'2.5px', marginRight:'2px'}} />
                                                    :
                                                    <Icon.Play size={11} style={{marginTop:'2.5px',  marginRight:'2px'}}/>
                                                  }
                                                  <p style={{marginTop:'-2px', fontSize:'0.8rem'}}><FormattedMessage id="listenAgain" tagName="data" /></p>
                                                </div>
                                              </Button>
                                            </h5>

                                            {this.cutAnswerToChoose(question.answersToChoose).map((subAnswer, indexSubAnswer) =>
                                              <>
                                                <Col xs="12" md="12" sm="12" lg="12" style={{display: 'flex', padding: '0%'}}>
                                                  <Col xs="1" md="2" sm="2" lg="1" style={{padding: '0%'}}>
                                                    {indexSubAnswer === question.indexCorrectAnswer ?
                                                      <Icon.Check size={25} style={{color: 'green'}}/> : null
                                                    }
                                                    {indexSubAnswer === question.indexIncorrectAnswer ?
                                                      <Icon.X size={25} style={{color: '#b21717'}} /> : null
                                                    }
                                                    {/*<Icon.Check size={25} style={{color: 'green'}}/>*/}
                                                  </Col>
                                                  <Col xs="11" md="10" sm="10" lg="11" style={{padding: '0%', display: 'flex'}}>
                                                    <FormGroup check style={{marginBottom:'4%'}}>
                                                      <Label  check className={classes.labelRadioButton} style={{cursor: 'pointer'}}>
                                                        <Input type="radio" name={question.sentenceNo}
                                                               checked={this.checkingAnswerChoosen(question.indexIncorrectAnswer,
                                                                 question.indexCorrectAnswer, indexSubAnswer)}
                                                        />
                                                        ({ indexSubAnswer===0 ? "A" : (indexSubAnswer===1 ? "B" : (indexSubAnswer===2 ? "C" : "D")) }) {this.upperCaseFirstLetter(subAnswer)}
                                                        {/*phần dịch câu đáp án*/}
                                                        <button style={{border: 'none', backgroundColor: 'white', padding: '0', marginLeft: '10px'}}
                                                                id={"a" + question.sentenceNo + indexSubAnswer} className="mr-1 mb-1">
                                                          <Icon.AlertCircle  size={20} />
                                                        </button>
                                                        <UncontrolledPopover trigger="focus" placement="bottom" target={"a" + question.sentenceNo + indexSubAnswer}

                                                        >
                                                          <PopoverBody style={{borderColor: 'green !important', color: 'white', backgroundColor:'green'}}>
                                                            <FormattedMessage id={"translate"}/>: {indexSubAnswer===0 ? question.translatingQuesA : (indexSubAnswer===1 ? question.translatingQuesB : (indexSubAnswer===2 ? question.translatingQuesC : question.translatingQuesD))}
                                                          </PopoverBody>
                                                        </UncontrolledPopover>
                                                        {/*phần dịch câu đáp án*/}
                                                      </Label>

                                                    </FormGroup>

                                                  </Col>
                                                </Col>
                                              </>
                                            )}
                                            <Col xs="12" md="12" sm="12" lg="12" style={{marginBottom: '10%'}}>
                                              <fieldset className={classes.fieldsetDescription}>
                                                <legend className={classes.legendDescription}><FormattedMessage id={"description"}/></legend>
                                                <i>{null === question.description ? <><FormattedMessage id={"no.explanation"}/></> : this.renderDescription(question.description)}</i>
                                              </fieldset>
                                            </Col>
                                          </Col>
                                        </>
                                      )}
                                    </Row>
                                  </>
                                  :
                                  <>
                                    <h4 style={{fontWeight: '700', marginTop: '5%'}}>Part 4.{indexCategory+1}</h4>
                                    <Row style={{marginTop:'3%', marginLeft: '10%'}}>
                                      <Col xs="6" md="6" sm="6" lg="6">
                                        <img className={classes.imgClass} style={{width: '100%', border: '5px solid lightgray'}}
                                             src={category.pathFile2}/>
                                        <h6 style={{textAlign:'center', fontStyle: 'italic'}}>
                                          (Question {category.listQuestionMinitestDTOS[0].sentenceNo}-
                                          {category.listQuestionMinitestDTOS[category.listQuestionMinitestDTOS.length - 1].sentenceNo})
                                        </h6>
                                      </Col>
                                      {category.listQuestionMinitestDTOS.map((question, indexQuestion) =>
                                        <>
                                          <Col id={String(question.sentenceNo)}  xs={6}>
                                            <h5 style={{fontWeight:'700'}}>{question.sentenceNo}. {question.name}
                                              {/*phần dịch câu hỏi*/}
                                              <button style={{border: 'none', backgroundColor: 'white', padding: '0', marginLeft: '2%'}}
                                                      id={"q" + question.sentenceNo} className="mr-1 mb-1">
                                                <Icon.AlertCircle  size={20} />
                                              </button>
                                              <UncontrolledPopover trigger="focus" placement="bottom" target={"q" + question.sentenceNo}

                                              >
                                                <PopoverBody style={{borderColor: 'green !important', color: 'white', backgroundColor:'green'}}>
                                                  <FormattedMessage id={"translate"}/>: {question.translatingQuestion !== null ? question.translatingQuestion : <FormattedMessage id={"no.translate"}/>}
                                                </PopoverBody>
                                              </UncontrolledPopover>
                                              {/*end phần dịch câu hỏi*/}
                                              <Button outline className={classes.buttonListenAgain} style={{padding: '0', display:'inline-flex', marginLeft:'10px', width:'auto'}} id='btnHighlightRepeat'
                                                      onClick={() => this.playingListeningWhenViewHistory(question ? question : {}, 0)}>
                                                <div style={{padding: '0 5px', display:'inline-flex'}}>
                                                  { this.state.stt === question.sentenceNo
                                                    ?
                                                    <Icon.Volume1 size={12} style={{marginTop:'2.5px', marginRight:'2px'}} />
                                                    :
                                                    <Icon.Play size={11} style={{marginTop:'2.5px',  marginRight:'2px'}}/>
                                                  }
                                                  <p style={{marginTop:'-2px', fontSize:'0.8rem'}}><FormattedMessage id="listenAgain" tagName="data" /></p>
                                                </div>
                                              </Button>
                                            </h5>
                                            {this.cutAnswerToChoose(question.answersToChoose).map((subAnswer, indexSubAnswer) =>
                                              <>
                                                <Col xs="12" md="12" sm="12" lg="12" style={{display: 'flex', padding: '0%'}}>
                                                  <Col xs="1" md="2" sm="2" lg="1" style={{padding: '0%'}}>
                                                    {indexSubAnswer === question.indexCorrectAnswer ?
                                                      <Icon.Check size={25} style={{color: 'green'}}/> : null
                                                    }
                                                    {indexSubAnswer === question.indexIncorrectAnswer ?
                                                      <Icon.X size={25} style={{color: '#b21717'}} /> : null
                                                    }
                                                    {/*<Icon.Check size={25} style={{color: 'green'}}/>*/}
                                                  </Col>
                                                  <Col xs="11" md="10" sm="10" lg="11" style={{padding: '0%', display: 'flex'}}>
                                                    <FormGroup check style={{marginBottom:'4%'}}>
                                                      <Label  check className={classes.labelRadioButton} style={{cursor: 'pointer'}}>
                                                        <Input type="radio" name={question.sentenceNo}
                                                               checked={this.checkingAnswerChoosen(question.indexIncorrectAnswer,
                                                                 question.indexCorrectAnswer, indexSubAnswer)}
                                                        />
                                                        ({ indexSubAnswer===0 ? "A" : (indexSubAnswer===1 ? "B" : (indexSubAnswer===2 ? "C" : "D")) }) {this.upperCaseFirstLetter(subAnswer)}
                                                        {/*phần dịch câu đáp án*/}
                                                        <button style={{border: 'none', backgroundColor: 'white', padding: '0', marginLeft: '10px'}}
                                                                id={"a" + question.sentenceNo + indexSubAnswer} className="mr-1 mb-1">
                                                          <Icon.AlertCircle  size={20} />
                                                        </button>
                                                        <UncontrolledPopover trigger="focus" placement="bottom" target={"a" + question.sentenceNo + indexSubAnswer}

                                                        >
                                                          <PopoverBody style={{borderColor: 'green !important', color: 'white', backgroundColor:'green'}}>
                                                            <FormattedMessage id={"translate"}/>: {indexSubAnswer===0 ? question.translatingQuesA : (indexSubAnswer===1 ? question.translatingQuesB : (indexSubAnswer===2 ? question.translatingQuesC : question.translatingQuesD))}
                                                          </PopoverBody>
                                                        </UncontrolledPopover>
                                                        {/*phần dịch câu đáp án*/}
                                                      </Label>

                                                    </FormGroup>

                                                  </Col>
                                                </Col>
                                              </>
                                            )}
                                            <Col xs="12" md="12" sm="12" lg="12" style={{marginBottom: '10%'}}>
                                              <fieldset className={classes.fieldsetDescription}>
                                                <legend className={classes.legendDescription}><FormattedMessage id={"description"}/></legend>
                                                <i>{null === question.description ? <><FormattedMessage id={"no.explanation"}/> </> : this.renderDescription(question.description)}</i>
                                              </fieldset>
                                            </Col>
                                          </Col>

                                        </>
                                      )}
                                    </Row>
                                  </>
                                }
                              </>
                            )}
                          </div>
                          {/*-------------------------------------------- end PART4--------------------------------------------*/}
                        </> : null
                      }
                    </> :
                    showAnswer === false ?
                      <>
                        {
                          listQuestionMinitest.length > 0 ?
                            <>
                              {/*--------------------------------------PART1----------------------------------------*/}
                              <div>
                                <h1 id="PART1" style={{fontWeight: '700'}}>PART 1</h1>
                                <h5>Directions: For each question in this part, you will hear four statements about a picture in your test book. When you hear the

                                  statements, you must select the one statement that best describes what you see in the picture. Then find the number of the

                                  question on your answer sheet and mark your answer. The statements will not be printed in your test book and will be

                                  spoken only one time.</h5>

                                <>
                                  {listQuestionMinitest[0].listCategoryMinitestDTOS.map((category, indexCategory) =>{
                                      let smallItem = category.listQuestionMinitestDTOS.map((question, indexQuestion)=>{
                                        return (
                                          <Row id={String(question.sentenceNo)} style={{marginTop: '4%', marginLeft: '10%'}}>
                                            <Col xs={1}><h5 style={{fontWeight:'700'}}>{question.sentenceNo}.</h5></Col>
                                            <Col xs={6}>
                                              <img className={classes.imgClass} style={{width: '100%', border: '5px solid lightgray'}}
                                                   src={question.pathFile2 ? question.pathFile2 : ""}/>
                                            </Col>
                                            <Col xs="5">
                                              {question ? this.cutAnswerToChoose(question.answersToChoose).map((subAnswer, indexSubAnswer) =>
                                                <FormGroup check style={{marginLeft:'15%', marginBottom:'3%'}}>
                                                  <Label  check className={classes.labelRadioButton} style={{cursor: 'pointer'}}>
                                                    <Input type="radio" name={question.sentenceNo}
                                                           onChange={this.onChangeValue}
                                                           value={subAnswer+"|"+question.id+"."+indexSubAnswer
                                                           +"|"+category.categoryId +"."+question.sentenceNo
                                                           +"|"+listQuestionMinitest[0].partName}
                                                    />
                                                    <>({ indexSubAnswer===0 ? "A" : (indexSubAnswer===1 ? "B" : (indexSubAnswer===2 ? "C" : "D")) })</>
                                                  </Label>
                                                </FormGroup>
                                              ) : null }
                                            </Col>

                                          </Row>
                                        )
                                      })
                                      return (<>{smallItem}</>)
                                    }
                                  )}
                                </>
                              </div>
                              {/*--------------------------------------end PART1----------------------------------------*/}



                              {/*--------------------------------------PART2----------------------------------------*/}
                              <div className="mt-3" style={{marginTop: '5% !important'}}>
                                <h1 id="PART2" style={{fontWeight: '700'}}>PART 2</h1>
                                <h5>Directions: You will hear a question or statement and three responses spoken in English. They will not be printed in your

                                  test book and will be spoken only one time. Select the best response to the question or statement and mark the letter (A),

                                  (B), or (C) on your answer sheet. </h5>

                                <Row style={{marginTop: '4%', marginLeft: '10%'}}>
                                  {listQuestionMinitest[1].listCategoryMinitestDTOS.map((category, indexCategory) => {
                                      let smallItem = category.listQuestionMinitestDTOS.map((question, indexQuestion) => {
                                        return (
                                          <Col xs="4" md="4" sm="4" lg="4" style={{marginBottom: '3%'}}>
                                            <Row id={String(question.sentenceNo)}>
                                              <Col xs={1} sm={12}><h5 style={{fontWeight: '700'}}>{question.sentenceNo}.</h5></Col>
                                              <Col xs={6} sm={12}>
                                                {question ? this.cutAnswerToChoose(question.answersToChoose).map((subAnswer, indexSubAnswer) =>
                                                  <FormGroup check style={{marginLeft: '7%', marginBottom: '6%'}}>
                                                    <Label check className={classes.labelRadioButton} style={{cursor: 'pointer'}}>
                                                      <Input type="radio" name={question.sentenceNo}
                                                             onChange={this.onChangeValue}
                                                             value={subAnswer + "|" + question.id + "." + indexSubAnswer
                                                             + "|" + listQuestionMinitest[1].listCategoryMinitestDTOS[0].categoryId + "." + question.sentenceNo
                                                             + "|" + listQuestionMinitest[1].partName}
                                                      />
                                                      <>({ indexSubAnswer===0 ? "A" : (indexSubAnswer===1 ? "B" : (indexSubAnswer===2 ? "C" : "D")) })</>
                                                    </Label>
                                                  </FormGroup>
                                                ) : null}
                                              </Col>
                                            </Row>
                                          </Col>
                                        )
                                      })
                                      return (<>{smallItem}</>)
                                    }
                                  )}
                                </Row>
                              </div>
                              {/*--------------------------------------end PART2----------------------------------------*/}



                              {/*-----------------------------------------PART3------------------------------------------*/}
                              <div className="mt-3">
                                <h1 id="PART3" style={{fontWeight: '700'}}>PART 3</h1>
                                <h5>Directions: You will hear some conversations between two people. You will be asked to answer three questions about what

                                  the speakers say in each conversation. Select the best response for each question and mark the letter (A), (B), (C), or (D)

                                  on your answer sheet. The conversations will not be printed in your test book and will be spoken only one time.</h5>

                                {listQuestionMinitest[2].listCategoryMinitestDTOS.map((category, indexCategory) =>
                                  <>
                                    {null === category.pathFile2 ?
                                      <>
                                        <h4 style={{fontWeight: '700', marginTop: '5%'}}>Part 3.{indexCategory + 1}</h4>
                                        <Row  style={{marginTop:'3%', marginLeft: '10%'}}>
                                          {category.listQuestionMinitestDTOS.map((question, indexQuestion) =>
                                            <Col xs={6} style={{marginTop:'2%'}} id={String(question.sentenceNo)}>
                                              <h5 style={{fontWeight:'700'}}>{question.sentenceNo}. {question.name}</h5>
                                              {this.cutAnswerToChoose(question.answersToChoose).map((subAnswer, indexSubAnswer) =>
                                                <FormGroup check style={{marginLeft:'8%', marginBottom:'5%'}}>
                                                  <Label  check className={classes.labelRadioButton} style={{cursor: 'pointer'}}>
                                                    <Input type="radio" name={question.sentenceNo}
                                                           onChange={this.onChangeValue}
                                                           value={subAnswer+"|"+question.id+"."+indexSubAnswer
                                                           +"|"+category.categoryId +"."+question.sentenceNo
                                                           +"|"+listQuestionMinitest[2].partName}
                                                    />
                                                    ({ indexSubAnswer===0 ? "A" : (indexSubAnswer===1 ? "B" : (indexSubAnswer===2 ? "C" : "D")) }) {this.upperCaseFirstLetter(subAnswer)}
                                                  </Label>
                                                </FormGroup>
                                              )}
                                            </Col>

                                          )}
                                        </Row>
                                      </>
                                      :
                                      <>
                                        <h4 style={{fontWeight: '700', marginTop: '5%'}}>Part 3.{indexCategory + 1}</h4>
                                        <Row style={{marginTop:'3%', marginLeft: '10%'}}>
                                          <Col xs="6" md="6" sm="6" lg="6">
                                            <img className={classes.imgClass} style={{width: '100%', border: '5px solid lightgray'}}
                                                 src={category.pathFile2}/>
                                            <h6 style={{textAlign:'center', fontStyle: 'italic'}}>
                                              (Question {category.listQuestionMinitestDTOS[0].sentenceNo}-
                                              {category.listQuestionMinitestDTOS[category.listQuestionMinitestDTOS.length - 1].sentenceNo})
                                            </h6>
                                          </Col>
                                          {category.listQuestionMinitestDTOS.map((question, indexQuestion) =>
                                            <Col id={String(question.sentenceNo)}  xs={6} style={{marginTop:'2%'}}>
                                              <h5 style={{fontWeight:'700'}}>{question.sentenceNo}. {question.name}</h5>
                                              {this.cutAnswerToChoose(question.answersToChoose).map((subAnswer, indexSubAnswer) =>
                                                <FormGroup check style={{marginLeft:'8%', marginBottom:'5%'}}>
                                                  <Label  check className={classes.labelRadioButton} style={{cursor: 'pointer'}}>
                                                    <Input type="radio" name={question.sentenceNo}
                                                           onChange={this.onChangeValue}
                                                           value={subAnswer+"|"+question.id+"."+indexSubAnswer
                                                           +"|"+category.categoryId+"."+question.sentenceNo
                                                           +"|"+listQuestionMinitest[2].partName}
                                                    />
                                                    ({ indexSubAnswer===0 ? "A" : (indexSubAnswer===1 ? "B" : (indexSubAnswer===2 ? "C" : "D")) }) {this.upperCaseFirstLetter(subAnswer)}
                                                  </Label>
                                                </FormGroup>
                                              )}
                                            </Col>
                                          )}
                                        </Row>
                                      </>
                                    }
                                  </>
                                )}
                              </div>
                              {/*-------------------------------------------end PART3---------------------------------------*/}


                              {/*--------------------------------------------PART4--------------------------------------------*/}
                              <div className="mt-3">
                                <h1 id="PART4" style={{fontWeight: '700'}}>PART 4</h1>
                                <h5>Directions: You will hear some talks given by a single speaker. You will be asked to answer three questions about what the

                                  speaker say in each talk. Select the best response for each question and mark the letter (A), (B), (C), or (D) on your answer

                                  sheet. The talks will not be printed in your test book and will be spoken only one time.</h5>

                                {listQuestionMinitest[3].listCategoryMinitestDTOS.map((category, indexCategory) =>
                                  <>
                                    {null === category.pathFile2 ?
                                      <>
                                        <h4 style={{fontWeight: '700', marginTop: '5%'}}>Part 4.{indexCategory + 1}</h4>
                                        <Row style={{marginTop:'3%', marginLeft: '10%'}}>
                                          {category.listQuestionMinitestDTOS.map((question, indexQuestion) =>
                                            <Col xs={6} id={String(question.sentenceNo)} style={{marginTop:'2%'}}>
                                              <h5 style={{fontWeight:'700'}}>{question.sentenceNo}. {question.name}</h5>
                                              {this.cutAnswerToChoose(question.answersToChoose).map((subAnswer, indexSubAnswer) =>
                                                <FormGroup check style={{marginLeft:'8%', marginBottom:'5%'}}>
                                                  <Label  check className={classes.labelRadioButton} style={{cursor: 'pointer'}}>
                                                    <Input type="radio" name={question.sentenceNo}
                                                           onChange={this.onChangeValue}
                                                           value={subAnswer+"|"+question.id+"."+indexSubAnswer
                                                           +"|"+category.categoryId+"."+question.sentenceNo
                                                           +"|"+listQuestionMinitest[3].partName}
                                                    />
                                                    ({ indexSubAnswer===0 ? "A" : (indexSubAnswer===1 ? "B" : (indexSubAnswer===2 ? "C" : "D")) }) {this.upperCaseFirstLetter(subAnswer)}
                                                  </Label>
                                                </FormGroup>
                                              )}
                                            </Col>
                                          )}
                                        </Row>
                                      </>
                                      :
                                      <>
                                        <h4 style={{fontWeight: '700', marginTop: '5%'}}>Part 4.{indexCategory + 1}</h4>
                                        <Row style={{marginTop:'3%', marginLeft: '10%'}}>
                                          <Col xs="6" md="6" sm="6" lg="6">
                                            <img className={classes.imgClass} style={{width: '100%', border: '5px solid lightgray'}}
                                                 src={category.pathFile2}/>
                                            <h6 style={{textAlign:'center', fontStyle: 'italic'}}>
                                              (Question {category.listQuestionMinitestDTOS[0].sentenceNo}-
                                              {category.listQuestionMinitestDTOS[category.listQuestionMinitestDTOS.length - 1].sentenceNo})
                                            </h6>
                                          </Col>
                                          {category.listQuestionMinitestDTOS.map((question, indexQuestion) =>
                                            <Col id={String(question.sentenceNo)}  xs={6} style={{marginTop:'2%'}}>
                                              <h5 style={{fontWeight:'700'}}>{question.sentenceNo}. {question.name}</h5>
                                              {this.cutAnswerToChoose(question.answersToChoose).map((subAnswer, indexSubAnswer) =>
                                                <FormGroup check style={{marginLeft:'8%', marginBottom:'5%'}}>
                                                  <Label  check className={classes.labelRadioButton} style={{cursor: 'pointer'}}>
                                                    <Input type="radio" name={question.sentenceNo}
                                                           onChange={this.onChangeValue}
                                                           value={subAnswer+"|"+question.id+"."+indexSubAnswer
                                                           +"|"+category.categoryId+"."+question.sentenceNo
                                                           +"|"+listQuestionMinitest[3].partName}
                                                    />
                                                    ({ indexSubAnswer===0 ? "A" : (indexSubAnswer===1 ? "B" : (indexSubAnswer===2 ? "C" : "D")) }) {this.upperCaseFirstLetter(subAnswer)}
                                                  </Label>
                                                </FormGroup>
                                              )}
                                            </Col>
                                          )}
                                        </Row>
                                      </>
                                    }
                                  </>
                                )}
                              </div>
                              {/*-------------------------------------------- end PART4--------------------------------------------*/}
                            </> : null
                        }
                      </>
                      :
                      <>
                        {listQuestionAndAnswerMinitest.length > 0 ?
                          <>
                            {/*--------------------------------------PART1----------------------------------------*/}
                            <div>
                              <h1 id="PART1" style={{fontWeight: '700'}}>PART 1</h1>
                              <>
                                {listQuestionAndAnswerMinitest[0].listCategoryMinitestDTOS.map((category, indexCategory) =>{
                                    let smallItem = category.listQuestionMinitestDTOS.map((question,indexQuestion)=>{
                                      return (
                                        <>
                                          <Row id={String(question.sentenceNo)}  style={{marginTop: '4%', marginLeft: '10%'}}>
                                            <Col xs={1}><h5 style={{fontWeight:'700'}}>{question.sentenceNo}.</h5></Col>
                                            <Col xs={6}>
                                              <img className={classes.imgClass} style={{width: '100%', border: '5px solid lightgray'}}
                                                   src={question.pathFile2 ? question.pathFile2 : ""}/>
                                            </Col>
                                            <Col xs="5">
                                              {question ? this.cutAnswerToChoose(question.answersToChoose).map((subAnswer, indexSubAnswer) =>
                                                <>
                                                  <Col xs="12" md="12" sm="12" lg="12" style={{display: 'flex', padding: '0%', marginBottom:'3%'}}>
                                                    <Col xs={1}  style={{padding: '0%', paddingRight:'7%'}}>
                                                      {indexSubAnswer === question.indexCorrectAnswer ?
                                                        <Icon.Check size={25} style={{color: 'green'}}/> : null
                                                      }
                                                      {indexSubAnswer === question.indexIncorrectAnswer ?
                                                        <Icon.X size={25} style={{color: '#b21717'}} /> : null
                                                      }
                                                    </Col>
                                                    <Col xs="11" md="10" sm="10" lg="11" style={{ display: 'flex'}}>
                                                      <FormGroup check style={{marginBottom:'4%'}}>
                                                        <Label  check className={classes.labelRadioButton} style={{cursor: 'pointer'}}>
                                                          <Input type="radio" name={question.sentenceNo}
                                                                 checked={this.checkingAnswerChoosen(question.indexIncorrectAnswer,
                                                                   question.indexCorrectAnswer, indexSubAnswer)}
                                                          />
                                                          <>({ indexSubAnswer===0 ? "A" : (indexSubAnswer===1 ? "B" : (indexSubAnswer===2 ? "C" : "D")) })</>

                                                        </Label>
                                                        <Button outline className={classes.buttonListenAgain} style={{padding: '0', display:'inline-flex', marginLeft:'10px', width:'auto'}} id='btnHighlightRepeat'
                                                                onClick={() => this.playingListeningAfterSubmit(question ? question : {}, indexSubAnswer)}>
                                                          <div style={{padding: '0 5px', display:'inline-flex'}}>
                                                            {this.state.indexListenAgain === indexSubAnswer && this.state.stt === question.sentenceNo
                                                              ?
                                                              <Icon.Volume1 size={12} style={{marginTop:'2.5px', marginRight:'2px'}} />
                                                              :
                                                              <Icon.Play size={11} style={{marginTop:'2.5px', marginRight:'2px'}}/>
                                                            }
                                                            <p style={{marginTop:'-2px'}}><FormattedMessage id="listenAgain" tagName="data" /></p>
                                                          </div>
                                                        </Button>
                                                      </FormGroup>
                                                    </Col>

                                                  </Col>
                                                </>
                                              ) : null }
                                            </Col>

                                          </Row>
                                          <Row>
                                            <Col xs="12" md="12" sm="12" lg="12" style={{marginBottom: '10%', marginTop:'2%'}}>
                                              <fieldset className={classes.fieldsetDescription}>
                                                <legend className={classes.legendDescription}><FormattedMessage id={"description"}/></legend>
                                                <i>{null === question.description ? <><FormattedMessage id={"no.explanation"}/></> : this.renderDescription(question.description)}</i>
                                              </fieldset>
                                            </Col>
                                          </Row>
                                        </>
                                      )
                                    })
                                    return (<>{smallItem}</>)
                                  }
                                )}
                              </>
                            </div>
                            {/*--------------------------------------end PART1----------------------------------------*/}



                            {/*--------------------------------------PART2----------------------------------------*/}
                            <div className="mt-3" style={{marginTop: '5% !important'}}>
                              <h1 id="PART2" style={{fontWeight: '700'}}>PART 2</h1>
                              <Row style={{marginTop: '2%', marginLeft: '10%'}}>
                                {listQuestionAndAnswerMinitest[1].listCategoryMinitestDTOS.map((category, indexCategory) =>{
                                    let smallItem = category.listQuestionMinitestDTOS.map((question, indexQuestion)=>{
                                      return (
                                        <Col xs={4} style={{marginBottom:'2%'}}>
                                          <Row id={String(question.sentenceNo)} >
                                            <Col xs="1" md="1" sm="1" lg="1"><h5 style={{fontWeight: '700'}}>{question.sentenceNo}.</h5></Col>
                                            <Col xs="11" md="11" sm="11" lg="11">
                                              {question ? this.cutAnswerToChoose(question.answersToChoose).map((subAnswer, indexSubAnswer) =>
                                                <>
                                                  <Col xs="12" md="12" sm="12" lg="12" style={{display: 'flex', padding: '0%', marginBottom:'3%'}}>
                                                    <Col xs="2" md="2" sm="3" lg="2" style={{paddingLeft: '0%'}}>
                                                      {indexSubAnswer === question.indexCorrectAnswer ?
                                                        <Icon.Check size={25} style={{color: 'green'}}/> : null
                                                      }
                                                      {indexSubAnswer === question.indexIncorrectAnswer ?
                                                        <Icon.X size={25} style={{color: '#b21717'}} /> : null
                                                      }
                                                      {/*<Icon.Check size={25} style={{color: 'green'}}/>*/}
                                                    </Col>
                                                    <Col xs="10" md="10" sm="9" lg="10" style={{padding: '0%', display: 'flex'}}>
                                                      <FormGroup check style={{marginBottom:'4%'}}>
                                                        <Label  check className={classes.labelRadioButton} style={{cursor: 'pointer'}}>
                                                          <Input type="radio" name={question.sentenceNo}
                                                                 checked={this.checkingAnswerChoosen(question.indexIncorrectAnswer,
                                                                   question.indexCorrectAnswer, indexSubAnswer)}
                                                          />
                                                          <>({ indexSubAnswer===0 ? "A" : (indexSubAnswer===1 ? "B" : (indexSubAnswer===2 ? "C" : "D")) })</>
                                                        </Label>
                                                        <Button outline className={classes.buttonListenAgain} style={{padding: '0', display:'inline-flex', marginLeft:'10px', width:'auto'}} id='btnHighlightRepeat'
                                                                onClick={() => this.playingListeningAfterSubmit(question ? question : {}, indexSubAnswer)}>
                                                          <div style={{padding: '0 5px', display:'inline-flex'}}>
                                                            {this.state.indexListenAgain === indexSubAnswer && this.state.stt === question.sentenceNo
                                                              ?
                                                              <Icon.Volume1 size={12} style={{marginTop:'2.5px', marginRight:'2px'}} />
                                                              :
                                                              <Icon.Play size={11} style={{marginTop:'2.5px', marginRight:'2px'}}/>
                                                            }
                                                            <p style={{marginTop:'-2px'}}><FormattedMessage id="listenAgain" tagName="data" /></p>
                                                          </div>
                                                        </Button>
                                                      </FormGroup>
                                                    </Col>

                                                  </Col>
                                                </>
                                              ) : null }
                                            </Col>
                                            <Col xs="12" md="12" sm="12" lg="12" style={{marginBottom: '10%'}}>
                                              <fieldset className={classes.fieldsetDescription}>
                                                <legend className={classes.legendDescription}><FormattedMessage id={"description"}/></legend>
                                                <i>{null === question.description ? <><FormattedMessage id={"no.explanation"}/></> : this.renderDescription(question.description)}</i>
                                              </fieldset>
                                            </Col>
                                          </Row>
                                        </Col>
                                      )
                                    })
                                    return (<>{smallItem}</>)
                                  }
                                )}
                              </Row>
                            </div>
                            {/*--------------------------------------end PART2----------------------------------------*/}



                            {/*-----------------------------------------PART3------------------------------------------*/}
                            <div className="mt-3">
                              <h1 id="PART3" style={{fontWeight: '700'}}>PART 3</h1>

                              {listQuestionAndAnswerMinitest[2].listCategoryMinitestDTOS.map((category, indexCategory) =>
                                <>
                                  {null === category.pathFile2 ?
                                    <>
                                      <h4 style={{fontWeight: '700', marginTop: '5%'}}>Part 3.{indexCategory + 1}</h4>
                                      <Row style={{marginTop:'3%', marginLeft: '10%'}}>
                                        {category.listQuestionMinitestDTOS.map((question, indexQuestion) =>
                                          <>
                                            <Col xs="6" md="6" sm="6" lg="6" id={String(question.sentenceNo)}>
                                              <h5 style={{fontWeight:'700'}}>{question.sentenceNo}. {question.name}
                                                {/*phần dịch câu hỏi*/}
                                                <button style={{border: 'none', backgroundColor: 'white', padding: '0', marginLeft: '2%'}}
                                                        id={"q" + question.sentenceNo} className="mr-1 mb-1">
                                                  <Icon.AlertCircle  size={20} />
                                                </button>
                                                <UncontrolledPopover trigger="focus" placement="bottom" target={"q" + question.sentenceNo}

                                                >
                                                  <PopoverBody style={{borderColor: 'green !important', color: 'white', backgroundColor:'green'}}>
                                                    <FormattedMessage id={"translate"}/>: {question.translatingQuestion !== null ? question.translatingQuestion : <FormattedMessage id={"no.translate"}/>}
                                                  </PopoverBody>
                                                </UncontrolledPopover>
                                                {/*end phần dịch câu hỏi*/}
                                                <Button outline className={classes.buttonListenAgain} style={{padding: '0', display:'inline-flex', marginLeft:'10px', width:'auto'}} id='btnHighlightRepeat'
                                                        onClick={() => this.playingListeningWhenViewHistory(question ? question : {}, 0)}>
                                                  <div style={{padding: '0 5px', display:'inline-flex'}}>
                                                    { this.state.stt === question.sentenceNo
                                                      ?
                                                      <Icon.Volume1 size={12} style={{marginTop:'2.5px', marginRight:'2px'}} />
                                                      :
                                                      <Icon.Play size={11} style={{marginTop:'2.5px',  marginRight:'2px'}}/>
                                                    }
                                                    <p style={{marginTop:'-2px', fontSize:'0.8rem'}}><FormattedMessage id="listenAgain" tagName="data" /></p>
                                                  </div>

                                                </Button>

                                              </h5>
                                              {this.cutAnswerToChoose(question.answersToChoose).map((subAnswer, indexSubAnswer) =>
                                                <>
                                                  <Col xs="12" md="12" sm="12" lg="12" style={{display: 'flex', padding: '0%'}}>
                                                    <Col xs="1" md="2" sm="2" lg="1" style={{padding: '0%'}}>
                                                      {indexSubAnswer === question.indexCorrectAnswer ?
                                                        <Icon.Check size={25} style={{color: 'green'}}/> : null
                                                      }
                                                      {indexSubAnswer === question.indexIncorrectAnswer ?
                                                        <Icon.X size={25} style={{color: '#b21717'}} /> : null
                                                      }
                                                      {/*<Icon.Check size={25} style={{color: 'green'}}/>*/}
                                                    </Col>
                                                    <Col xs="11" md="10" sm="10" lg="11" style={{padding: '0%', display: 'flex'}}>
                                                      <FormGroup check style={{marginBottom:'4%'}}>
                                                        <Label  check className={classes.labelRadioButton} style={{cursor: 'pointer'}}>
                                                          <Input type="radio" name={question.sentenceNo}
                                                                 checked={this.checkingAnswerChoosen(question.indexIncorrectAnswer,
                                                                   question.indexCorrectAnswer, indexSubAnswer)}
                                                          />
                                                          ({ indexSubAnswer===0 ? "A" : (indexSubAnswer===1 ? "B" : (indexSubAnswer===2 ? "C" : "D")) }) {this.upperCaseFirstLetter(subAnswer)}
                                                        </Label>
                                                        {/*phần dịch câu đáp án*/}
                                                        <button style={{border: 'none', backgroundColor: 'white', padding: '0', marginLeft: '10px'}}
                                                                id={"a" + question.sentenceNo + indexSubAnswer} className="mr-1 mb-1">
                                                          <Icon.AlertCircle  size={20} />
                                                        </button>
                                                        <UncontrolledPopover trigger="focus" placement="bottom" target={"a" + question.sentenceNo + indexSubAnswer}

                                                        >
                                                          <PopoverBody style={{borderColor: 'green !important', color: 'white', backgroundColor:'green'}}>
                                                            <FormattedMessage id={"translate"}/>: {indexSubAnswer===0 ? question.translatingQuesA : (indexSubAnswer===1 ? question.translatingQuesB : (indexSubAnswer===2 ? question.translatingQuesC : question.translatingQuesD))}
                                                          </PopoverBody>
                                                        </UncontrolledPopover>
                                                        {/*end phần dịch câu đáp án*/}
                                                      </FormGroup>

                                                    </Col>
                                                  </Col>
                                                </>
                                              )}
                                              <Col xs={12} style={{marginBottom: '10%', marginTop:'2%'}}>
                                                <fieldset className={classes.fieldsetDescription}>
                                                  <legend className={classes.legendDescription}><FormattedMessage id={"description"}/></legend>
                                                  <i>{null === question.description ? <><FormattedMessage id={"no.explanation"}/> </> : question.description}</i>
                                                </fieldset>
                                              </Col>
                                            </Col>
                                          </>
                                        )}
                                      </Row>
                                    </>
                                    :
                                    <>
                                      <h4 style={{fontWeight: '700', marginTop: '5%'}}>Part 3.{indexCategory + 1}</h4>
                                      <Row style={{marginTop:'3%', marginLeft: '10%'}}>
                                        <Col xs="6" md="6" sm="6" lg="6">
                                          <img className={classes.imgClass} style={{width: '100%', border: '5px solid lightgray'}}
                                               src={category.pathFile2}/>
                                          <h6 style={{textAlign:'center', fontStyle: 'italic'}}>
                                            (Question {category.listQuestionMinitestDTOS[0].sentenceNo}-
                                            {category.listQuestionMinitestDTOS[category.listQuestionMinitestDTOS.length - 1].sentenceNo})
                                          </h6>
                                        </Col>
                                        {category.listQuestionMinitestDTOS.map((question, indexQuestion) =>
                                          <>
                                            <Col id={String(question.sentenceNo)}  xs={6}>
                                              <h5 style={{fontWeight:'700'}}>{question.sentenceNo}. {question.name}
                                                {/*phần dịch câu hỏi*/}
                                                <button style={{border: 'none', backgroundColor: 'white', padding: '0', marginLeft: '2%'}}
                                                        id={"q" + question.sentenceNo} className="mr-1 mb-1">
                                                  <Icon.AlertCircle  size={20} />
                                                </button>
                                                <UncontrolledPopover trigger="focus" placement="bottom" target={"q" + question.sentenceNo}

                                                >
                                                  <PopoverBody style={{borderColor: 'green !important', color: 'white', backgroundColor:'green'}}>
                                                    <FormattedMessage id={"translate"}/>: {question.translatingQuestion !== null ? question.translatingQuestion : <FormattedMessage id={"no.translate"}/>}
                                                  </PopoverBody>
                                                </UncontrolledPopover>
                                                {/*end phần dịch câu hỏi*/}
                                                <Button outline className={classes.buttonListenAgain} style={{padding: '0', display:'inline-flex', marginLeft:'10px', width:'auto'}} id='btnHighlightRepeat'
                                                        onClick={() => this.playingListeningWhenViewHistory(question ? question : {}, 0)}>
                                                  <div style={{padding: '0 5px', display:'inline-flex'}}>
                                                    { this.state.stt === question.sentenceNo
                                                      ?
                                                      <Icon.Volume1 size={12} style={{marginTop:'2.5px', marginRight:'2px'}} />
                                                      :
                                                      <Icon.Play size={11} style={{marginTop:'2.5px',  marginRight:'2px'}}/>
                                                    }
                                                    <p style={{marginTop:'-2px', fontSize:'0.8rem'}}><FormattedMessage id="listenAgain" tagName="data" /></p>
                                                  </div>
                                                </Button>
                                              </h5>
                                              {this.cutAnswerToChoose(question.answersToChoose).map((subAnswer, indexSubAnswer) =>
                                                <>
                                                  <Col xs="12" md="12" sm="12" lg="12" style={{display: 'flex', padding: '0%'}}>
                                                    <Col xs="1" md="2" sm="2" lg="1" style={{padding: '0%'}}>
                                                      {indexSubAnswer === question.indexCorrectAnswer ?
                                                        <Icon.Check size={25} style={{color: 'green'}}/> : null
                                                      }
                                                      {indexSubAnswer === question.indexIncorrectAnswer ?
                                                        <Icon.X size={25} style={{color: '#b21717'}} /> : null
                                                      }
                                                      {/*<Icon.Check size={25} style={{color: 'green'}}/>*/}
                                                    </Col>
                                                    <Col xs="11" md="10" sm="10" lg="11" style={{padding: '0%', display: 'flex'}}>
                                                      <FormGroup check style={{marginBottom:'4%'}}>
                                                        <Label  check className={classes.labelRadioButton} style={{cursor: 'pointer'}}>
                                                          <Input type="radio" name={question.sentenceNo}
                                                                 checked={this.checkingAnswerChoosen(question.indexIncorrectAnswer,
                                                                   question.indexCorrectAnswer, indexSubAnswer)}
                                                          />
                                                          ({ indexSubAnswer===0 ? "A" : (indexSubAnswer===1 ? "B" : (indexSubAnswer===2 ? "C" : "D")) }) {this.upperCaseFirstLetter(subAnswer)}
                                                        </Label>
                                                        {/*phần dịch câu đáp án*/}
                                                        <button style={{border: 'none', backgroundColor: 'white', padding: '0', marginLeft: '10px'}}
                                                                id={"a" + question.sentenceNo + indexSubAnswer} className="mr-1 mb-1">
                                                          <Icon.AlertCircle  size={20} />
                                                        </button>
                                                        <UncontrolledPopover trigger="focus" placement="bottom" target={"a" + question.sentenceNo + indexSubAnswer}

                                                        >
                                                          <PopoverBody style={{borderColor: 'green !important', color: 'white', backgroundColor:'green'}}>
                                                            <FormattedMessage id={"translate"}/>: {indexSubAnswer===0 ? question.translatingQuesA : (indexSubAnswer===1 ? question.translatingQuesB : (indexSubAnswer===2 ? question.translatingQuesC : question.translatingQuesD))}
                                                          </PopoverBody>
                                                        </UncontrolledPopover>
                                                        {/*end phần dịch câu đáp án*/}
                                                      </FormGroup>

                                                    </Col>
                                                  </Col>
                                                </>
                                              )}
                                              <Col xs={12} style={{marginBottom: '10%', marginTop:'2%'}}>
                                                <fieldset className={classes.fieldsetDescription}>
                                                  <legend className={classes.legendDescription}><FormattedMessage id={"description"}/></legend>
                                                  <i>{null === question.description ? <><FormattedMessage id={"no.explanation"}/></> : question.description}</i>
                                                </fieldset>
                                              </Col>
                                            </Col>

                                          </>
                                        )}
                                      </Row>
                                    </>
                                  }
                                </>
                              )}
                            </div>
                            {/*-------------------------------------------end PART3---------------------------------------*/}


                            {/*--------------------------------------------PART4--------------------------------------------*/}
                            <div className="mt-3">
                              <h1 id="PART4" style={{fontWeight: '700'}}>PART 4</h1>

                              {listQuestionAndAnswerMinitest[3].listCategoryMinitestDTOS.map((category, indexCategory) =>
                                <>
                                  {null === category.pathFile2 ?
                                    <>
                                      <h4 style={{fontWeight: '700', marginTop: '5%'}}>Part 4.{indexCategory + 1}</h4>
                                      <Row  style={{marginTop:'3%', marginLeft: '10%'}}>
                                        {category.listQuestionMinitestDTOS.map((question, indexQuestion) =>
                                          <>
                                            <Col xs={6} id={String(question.sentenceNo)} >
                                              <h5 style={{fontWeight:'700'}}>{question.sentenceNo}. {question.name}
                                                {/*phần dịch câu hỏi*/}
                                                <button style={{border: 'none', backgroundColor: 'white', padding: '0', marginLeft: '2%'}}
                                                        id={"q" + question.sentenceNo} className="mr-1 mb-1">
                                                  <Icon.AlertCircle  size={20} />
                                                </button>
                                                <UncontrolledPopover trigger="focus" placement="bottom" target={"q" + question.sentenceNo}

                                                >
                                                  <PopoverBody style={{borderColor: 'green !important', color: 'white', backgroundColor:'green'}}>
                                                    <FormattedMessage id={"translate"}/>: {question.translatingQuestion !== null ? question.translatingQuestion : <FormattedMessage id={"no.translate"}/>}
                                                  </PopoverBody>
                                                </UncontrolledPopover>
                                                {/*end phần dịch câu hỏi*/}
                                                <Button outline className={classes.buttonListenAgain} style={{padding: '0', display:'inline-flex', marginLeft:'10px', width:'auto'}} id='btnHighlightRepeat'
                                                        onClick={() => this.playingListeningWhenViewHistory(question ? question : {}, 0)}>
                                                  <div style={{padding: '0 5px', display:'inline-flex'}}>
                                                    { this.state.stt === question.sentenceNo
                                                      ?
                                                      <Icon.Volume1 size={12} style={{marginTop:'2.5px', marginRight:'2px'}} />
                                                      :
                                                      <Icon.Play size={11} style={{marginTop:'2.5px',  marginRight:'2px'}}/>
                                                    }
                                                    <p style={{marginTop:'-2px', fontSize:'0.8rem'}}><FormattedMessage id="listenAgain" tagName="data" /></p>
                                                  </div>
                                                </Button>
                                              </h5>

                                              {this.cutAnswerToChoose(question.answersToChoose).map((subAnswer, indexSubAnswer) =>
                                                <>
                                                  <Col xs="12" md="12" sm="12" lg="12" style={{display: 'flex', padding: '0%'}}>
                                                    <Col xs="1" md="2" sm="2" lg="1" style={{padding: '0%'}}>
                                                      {indexSubAnswer === question.indexCorrectAnswer ?
                                                        <Icon.Check size={25} style={{color: 'green'}}/> : null
                                                      }
                                                      {indexSubAnswer === question.indexIncorrectAnswer ?
                                                        <Icon.X size={25} style={{color: '#b21717'}} /> : null
                                                      }
                                                      {/*<Icon.Check size={25} style={{color: 'green'}}/>*/}
                                                    </Col>
                                                    <Col xs="11" md="10" sm="10" lg="11" style={{padding: '0%', display: 'flex'}}>
                                                      <FormGroup check style={{marginBottom:'4%'}}>
                                                        <Label  check className={classes.labelRadioButton} style={{cursor: 'pointer'}}>
                                                          <Input type="radio" name={question.sentenceNo}
                                                                 checked={this.checkingAnswerChoosen(question.indexIncorrectAnswer,
                                                                   question.indexCorrectAnswer, indexSubAnswer)}
                                                          />
                                                          ({ indexSubAnswer===0 ? "A" : (indexSubAnswer===1 ? "B" : (indexSubAnswer===2 ? "C" : "D")) }) {this.upperCaseFirstLetter(subAnswer)}
                                                        </Label>
                                                        {/*phần dịch câu đáp án*/}
                                                        <button style={{border: 'none', backgroundColor: 'white', padding: '0', marginLeft: '10px'}}
                                                                id={"a" + question.sentenceNo + indexSubAnswer} className="mr-1 mb-1">
                                                          <Icon.AlertCircle  size={20} />
                                                        </button>
                                                        <UncontrolledPopover trigger="focus" placement="bottom" target={"a" + question.sentenceNo + indexSubAnswer}

                                                        >
                                                          <PopoverBody style={{borderColor: 'green !important', color: 'white', backgroundColor:'green'}}>
                                                            <FormattedMessage id={"translate"}/>: {indexSubAnswer===0 ? question.translatingQuesA : (indexSubAnswer===1 ? question.translatingQuesB : (indexSubAnswer===2 ? question.translatingQuesC : question.translatingQuesD))}
                                                          </PopoverBody>
                                                        </UncontrolledPopover>
                                                        {/*phần dịch câu đáp án*/}
                                                      </FormGroup>

                                                    </Col>
                                                  </Col>
                                                </>
                                              )}
                                              <Col xs="12" md="12" sm="12" lg="12" style={{marginBottom: '10%'}}>
                                                <fieldset className={classes.fieldsetDescription}>
                                                  <legend className={classes.legendDescription}><FormattedMessage id={"description"}/></legend>
                                                  <i>{null === question.description ? <><FormattedMessage id={"no.explanation"}/></> : question.description}</i>
                                                </fieldset>
                                              </Col>
                                            </Col>
                                          </>
                                        )}
                                      </Row>
                                    </>
                                    :
                                    <>
                                      <h4 style={{fontWeight: '700', marginTop: '5%'}}>Part 4.{indexCategory+1}</h4>
                                      <Row style={{marginTop:'3%', marginLeft: '10%'}}>
                                        <Col xs="6" md="6" sm="6" lg="6">
                                          <img className={classes.imgClass} style={{width: '100%', border: '5px solid lightgray'}}
                                               src={category.pathFile2}/>
                                          <h6 style={{textAlign:'center', fontStyle: 'italic'}}>
                                            (Question {category.listQuestionMinitestDTOS[0].sentenceNo}-
                                            {category.listQuestionMinitestDTOS[category.listQuestionMinitestDTOS.length - 1].sentenceNo})
                                          </h6>
                                        </Col>
                                        {category.listQuestionMinitestDTOS.map((question, indexQuestion) =>
                                          <>
                                            <Col id={String(question.sentenceNo)}  xs={6}>
                                              <h5 style={{fontWeight:'700'}}>{question.sentenceNo}. {question.name}
                                                {/*phần dịch câu hỏi*/}
                                                <button style={{border: 'none', backgroundColor: 'white', padding: '0', marginLeft: '10px'}}
                                                        id={"q" + question.sentenceNo} className="mr-1 mb-1">
                                                  <Icon.AlertCircle  size={20} />
                                                </button>
                                                <UncontrolledPopover trigger="focus" placement="bottom" target={"q" + question.sentenceNo}

                                                >
                                                  <PopoverBody style={{borderColor: 'green !important', color: 'white', backgroundColor:'green'}}>
                                                    <FormattedMessage id={"translate"}/>: {question.translatingQuestion !== null ? question.translatingQuestion : <FormattedMessage id={"no.translate"}/>}
                                                  </PopoverBody>
                                                </UncontrolledPopover>
                                                {/*end phần dịch câu hỏi*/}
                                                <Button outline className={classes.buttonListenAgain} style={{padding: '0', display:'inline-flex', marginLeft:'10px', width:'auto'}} id='btnHighlightRepeat'
                                                        onClick={() => this.playingListeningWhenViewHistory(question ? question : {}, 0)}>
                                                  <div style={{padding: '0 5px', display:'inline-flex'}}>
                                                    { this.state.stt === question.sentenceNo
                                                      ?
                                                      <Icon.Volume1 size={12} style={{marginTop:'2.5px', marginRight:'2px'}} />
                                                      :
                                                      <Icon.Play size={11} style={{marginTop:'2.5px',  marginRight:'2px'}}/>
                                                    }
                                                    <p style={{marginTop:'-2px', fontSize:'0.8rem'}}><FormattedMessage id="listenAgain" tagName="data" /></p>
                                                  </div>
                                                </Button>
                                              </h5>
                                              {this.cutAnswerToChoose(question.answersToChoose).map((subAnswer, indexSubAnswer) =>
                                                <>
                                                  <Col xs="12" md="12" sm="12" lg="12" style={{display: 'flex', padding: '0%'}}>
                                                    <Col xs="1" md="2" sm="2" lg="1" style={{padding: '0%'}}>
                                                      {indexSubAnswer === question.indexCorrectAnswer ?
                                                        <Icon.Check size={25} style={{color: 'green'}}/> : null
                                                      }
                                                      {indexSubAnswer === question.indexIncorrectAnswer ?
                                                        <Icon.X size={25} style={{color: '#b21717'}} /> : null
                                                      }
                                                      {/*<Icon.Check size={25} style={{color: 'green'}}/>*/}
                                                    </Col>
                                                    <Col xs="11" md="10" sm="10" lg="11" style={{padding: '0%', display: 'flex'}}>
                                                      <FormGroup check style={{marginBottom:'4%'}}>
                                                        <Label  check className={classes.labelRadioButton} style={{cursor: 'pointer'}}>
                                                          <Input type="radio" name={question.sentenceNo}
                                                                 checked={this.checkingAnswerChoosen(question.indexIncorrectAnswer,
                                                                   question.indexCorrectAnswer, indexSubAnswer)}
                                                          />
                                                          ({ indexSubAnswer===0 ? "A" : (indexSubAnswer===1 ? "B" : (indexSubAnswer===2 ? "C" : "D")) }) {this.upperCaseFirstLetter(subAnswer)}
                                                        </Label>
                                                        {/*phần dịch câu đáp án*/}
                                                        <button style={{border: 'none', backgroundColor: 'white', padding: '0', marginLeft: '10px'}}
                                                                id={"a" + question.sentenceNo + indexSubAnswer} className="mr-1 mb-1">
                                                          <Icon.AlertCircle  size={20} />
                                                        </button>
                                                        <UncontrolledPopover trigger="focus" placement="bottom" target={"a" + question.sentenceNo + indexSubAnswer}

                                                        >
                                                          <PopoverBody style={{borderColor: 'green !important', color: 'white', backgroundColor:'green'}}>
                                                            <FormattedMessage id={"translate"}/>: {indexSubAnswer===0 ? question.translatingQuesA : (indexSubAnswer===1 ? question.translatingQuesB : (indexSubAnswer===2 ? question.translatingQuesC : question.translatingQuesD))}
                                                          </PopoverBody>
                                                        </UncontrolledPopover>
                                                        {/*phần dịch câu đáp án*/}
                                                      </FormGroup>

                                                    </Col>
                                                  </Col>
                                                </>
                                              )}
                                              <Col xs="12" md="12" sm="12" lg="12" style={{marginBottom: '10%'}}>
                                                <fieldset className={classes.fieldsetDescription}>
                                                  <legend className={classes.legendDescription}><FormattedMessage id={"description"}/></legend>
                                                  <i>{null === question.description ? <><FormattedMessage id={"no.explanation"}/> </> : question.description}</i>
                                                </fieldset>
                                              </Col>
                                            </Col>

                                          </>
                                        )}
                                      </Row>
                                    </>
                                  }
                                </>
                              )}
                            </div>
                            {/*-------------------------------------------- end PART4--------------------------------------------*/}
                          </> : null
                        }
                      </>
                }

              </div>
            </Row>
          </div>
        </div>
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    listQuestionMinitest : state.doTestReducer.listQuestionFullTest,
    listQuestionListeningChoosenDTOS: state.doTestReducer.listQuestionListeningChoosenDTOS,
    listQuestionReadingChoosenDTOS: state.doTestReducer.listQuestionReadingChoosenDTOS,
    listQuestionAndAnswerMinitest: state.doTestReducer.listQuestionAndAnswerFullTest,
    loadingQuestion: state.doTestReducer.loadingQuestion,
    loadingSubmitAnswer: state.doTestReducer.loadingSubmitAnswer,
    showAnswer: state.doTestReducer.showAnswer,
    detailTest: state.doTestReducer.detailTest,
    viewHistory: state.historyTestReducer.viewHistory,
    listHistoryFullTest: state.historyTestReducer.listQuestionAndAnswerFullTest,
    loadingResult: state.historyTestReducer.loadingResult,
    detailHistoryFullTest: state.historyTestReducer.detailHistoryFullTest
  }
}
const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    testAction : bindActionCreators(testAction , dispatch),

  }
}
const withConnect = connect(mapStateToProps, mapDispatchToProps)

export default compose(withStyles(styles), withConnect)(ListeningTest);

