import React, { useState, useEffect } from 'react'
import styles from "./style";
import {Headphones, Pause} from "react-bootstrap-icons";

const useMultiAudio = (urls,loadingQuestion, props) => {
  const [sources] = useState(
    urls.map(url => {
      return {
        url,
        audio: new Audio(url),
      }
    }),
  )
  const [players, setPlayers] = useState(
    urls.map(url => {
      return {
        url,
        playing: loadingQuestion,
      }
    }),
  )


  useEffect(() => {
    sources.forEach((source, i) => {
      players[i].playing ? source.audio.play() : source.audio.pause()
    })
  }, [sources, players])

  useEffect(() => {
    sources.forEach((source, i) => {
      source.audio.addEventListener('ended', () => {
        const newPlayers = [...players]
        newPlayers[i].playing = false
        setPlayers(newPlayers)
        props.testAction. doReadingAfterListeningFulltest(true);
      })
    })
    return () => {
      sources.forEach((source, i) => {
        source.audio.removeEventListener('ended', () => {
          const newPlayers = [...players]
          newPlayers[i].playing = false
          setPlayers(newPlayers)
        })
      })
    }
  }, [])

  return [players]
}

const MultiPlayerMP3 = ({ urls, loadingQuestion, props}) => {
  let urlstest = '';
  urlstest = urls
  const [players] = useMultiAudio(urlstest,loadingQuestion, props)
  return (
    <div  >
      {players.map((player, i) => (
        <Player key={i} player={player} loadingQuestion={loadingQuestion} props={props}/>
      ))}
    </div>
  )
}

const Player = ({ player, loadingQuestion, props }) => (
  
  <div className="row" >
    {player.playing ?
      <Pause size={20}  style={styles.iconPlay} className="col-4 pl-0 pr-0"/>:
      <Headphones size={20}  style={styles.iconPlay} className="col-4 pl-0 pr-0"/>
    }
  </div>
)

export default MultiPlayerMP3
