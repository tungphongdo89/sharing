import React, {Component} from 'react';
import {
  Badge,
  Card, CardBody,
  Row, Col,
  CardTitle,
} from 'reactstrap';
import {withStyles} from "@material-ui/styles";
import {connect} from "react-redux";
import {bindActionCreators, compose} from "redux";
import * as styles from "./style";
import PropTypes from 'prop-types';
import Category from "./category";
import readingActions from "../../../../../redux/actions/practice/reading";
import Spinner from "reactstrap/es/Spinner";
import {FormattedMessage} from "react-intl";
 
import {
  Container
} 
from "reactstrap"

class PracticeReadingSingleAndDual extends Component {

  constructor(props) {
    super(props);
    this.state = {
      totalQuest:0,
      indexQuestion: 0,
    }
  }

  componentDidMount() {
    const {readingReducer,listQuestionSingle} = this.props;
    let count = 0;
    if(readingReducer.checkReview === true){
      for(let i = 0; i < readingReducer.index ; i++){
        count += listQuestionSingle[i].listQuestion.length;
      }
      this.setState({...this.state,totalQuest:count,indexQuestion:readingReducer.index})
    }
  }

  nextQuestion = (data) => {
    let index = this.state.indexQuestion;
    const {readingActions} = this.props;
    readingActions.clearTempReadingSingleWhenNextQues();
    this.setState({indexQuestion: index + 1,totalQuest:(this.state.totalQuest+data)})
  }
  scrollTopPage=()=>{
    const element = document.getElementById("topFormContainer");
    element.scrollIntoView()
  }
  render() {
    debugger
    const {classes, listQuestionSingle,readingReducer} = this.props;
    let QuestionItems = listQuestionSingle ? listQuestionSingle.map((cate, index) => {
      return (
        <div style={{position: "relative"}} >
          <Badge style={{backgroundColor: "rgb(22,155,213)"}} className="badge-xl block">
            <Row>
              <Col className="text-left" xs={6}>
                <div style={{position: 'relative'}}>
                  <div className="titaleModal">
                    <strong>
                      {/*Đề*/}
                      <FormattedMessage id="category"/>{' '}
                      {index+1}/{listQuestionSingle.length} -
                      {/*câu*/}
                      <FormattedMessage id="question"/>{' '}
                      {this.state.totalQuest+1}{' '}
                      {/*đến*/}
                      <FormattedMessage id="to"/>{' '}
                      {cate.listQuestion.length+this.state.totalQuest}</strong>
                  </div>
                </div>
              </Col>
              <Col className="text-right" xs={6} style={{fontSize: "75%", textAlign: "center"}}>
                <div style={{position: 'relative'}}>
                  <div className="titaleModal">
                    <strong><i>
                      {/*Số câu đúng*/}
                      <FormattedMessage id="numberCorrect"/>{' '}
                      : {readingReducer.totalTrue}/{readingReducer.totalQuestion}</i></strong>
                  </div>
                </div>
              </Col>
            </Row>
          </Badge>
          <div className={classes.borderdiv}>
            {
              this.props.readingReducer.tempAnsweredReadingSingleAndDualQuest?
                <Category
                  nextQuestion={this.nextQuestion}
                  category={this.props.readingReducer.tempAnsweredReadingSingleAndDualQuest}
                  index={index}
                  len={listQuestionSingle.length}
                  scrollTopPage={this.scrollTopPage}
                />
                :
                <Category
                  nextQuestion={this.nextQuestion}
                  category={cate}
                  index={index}
                  len={listQuestionSingle.length}
                  scrollTopPage={this.scrollTopPage}
                />
            }
          </div>
        </div>
      )
    }) : ""


    return (
      <Container id="topFormContainer" style={{marginLeft: "auto",marginRight:"auto"}}>
        <div className="text-center" style={{width:"100%"}}>
          {
            readingReducer.partPractice === "PART7" || readingReducer.part === "PART7" ?
              <h3><strong>
                Part VII:{' '}
                {/*Bài đọc đoạn đơn*/}
                <FormattedMessage id="reading.part7"/>
              </strong></h3>:""
          }
          {
            readingReducer.partPractice === "PART8" || readingReducer.part === "PART8" ?
              <h3><strong>Part VIII:{' '}
                {/*Bài đọc đoạn kép*/}
                <FormattedMessage id="reading.part8"/>
              </strong></h3>:""
          }
        </div>
        <Card>
          <CardBody>
            {
              QuestionItems[this.state.indexQuestion]
            }
            {
              readingReducer.loading ?
                <div style={{
                  position: 'absolute',
                  zIndex: 110,
                  top: 0,
                  left: 0,
                  width: '100%',
                  height: '100%',
                  display: 'flex',
                  justifyContent: 'center',
                  alignItems: 'center',
                  background: 'rgba(255,255,255,0.8)'
                }}>
                  <Spinner color="primary" className="reload-spinner"/>
                </div> : null
            }
          </CardBody>
        </Card>
        </Container>
    );
  }
}

PracticeReadingSingleAndDual.propTypes = {
  classes: PropTypes.object
}

const mapDispatchToProps = dispatch => {
  return {
    readingActions:bindActionCreators(readingActions,dispatch)
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    readingReducer: state.readingReducer,
    listQuestionSingle: state.readingReducer.listCategorySingleAndDual
  }
}
const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
)
export default compose(withStyles(styles.style), withConnect)(PracticeReadingSingleAndDual);
