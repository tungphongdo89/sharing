import React, {Component} from 'react';
import {Badge, Button, Col, Input, Label, Row} from "reactstrap";
import * as styles from "./style";
import Spinner from "reactstrap/es/Spinner";
import {bindActionCreators, compose} from "redux";
import readingAction from "../../../../../redux/actions/practice/reading";
import {connect} from "react-redux";
import {withStyles} from "@material-ui/styles";
import Radio from "../../../../../components/@vuexy/radio/RadioVuexy";
import {Field, Form, Formik, getIn} from "formik";
import * as Yup from "yup";
import * as Icon from "react-feather";
import {ChevronsRight} from "react-feather";
import {toast} from "react-toastify"
import {history} from "../../../../../history";
import { Fragment } from 'react';
import UncontrolledPopover from 'reactstrap/lib/UncontrolledPopover';
import PopoverBody from 'reactstrap/lib/PopoverBody';
 
import {
  FormGroup
} 
from "reactstrap"
import {FormattedMessage} from "react-intl";
import {showMessage} from "../../../../../commons/utils/convertDataForToast";

const formSchema = Yup.object().shape({
  category: Yup.object().shape({
    listQuestion: Yup.array().of(
      Yup.object().shape({
        userChoose: Yup.string().nullable()
      }).test("Missing User Choose", "Câu hỏi chưa được trả lời!", function (item) {
        if (item.userChoose && item.userChoose !== null) {
          return true
        } else {
          this.createError({path: `${this.path}`, message: showMessage("please.answer.all.of.the.questions")})
        }
      })
    )
  })
})
class Category extends Component {
  
  componentDidMount(){
    this.props.scrollTopPage();
  }

  radioButtonAnswer = (props) => {
    const onChangeChecked = (e) => {
      props.form.setFieldTouched(props.field.name, true)
      props.form.setFieldValue(props.field.name, e.target.value)
    }
    
    const {question,classes} = props;
    let libAlphabet = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"];
    const i =props.indexAnswersToChoose;
    const transcript = this.props.readingReducer.tempAnsweredReadingSingleAndDualQuest  ? <Fragment > 
    <button   className={classes.btntranslate}   style={{   border: 'none', backgroundColor: 'white', padding: '0',marginTop:'0%', marginLeft: '2%'}}
          id={"a" + question.id + i} className="mr-1 mb-1"  
          onClick = {e=>{e.preventDefault()}} >
      <Icon.AlertCircle  size={20} />
      </button>
      <UncontrolledPopover trigger="focus" placement="bottom" target={"a" + question.id + i}
      >
      <PopoverBody style={{borderColor: 'green !important',
      color: 'white', backgroundColor: 'green', borderRadius: '5px'}}>
      <FormattedMessage id={"translate"}/>: {i===0 ? question.translatingQuesA : (i===1 ? question.translatingQuesB : (i===2 ? question.translatingQuesC : question.translatingQuesD))}
      </PopoverBody>
      </UncontrolledPopover></Fragment> : '' ;
    return (
      // <Row className="container-fluid" style={{display: 'flex',padding: "0px 0px 0px 0px" }}>
      <Fragment>
        <Col  xs="2" md="2" sm="2" lg="2">
          { 
            props.question.correct != undefined ?
              props.question.answer ===  libAlphabet[i] ?
                <Icon.Check style={{color: 'green', paddingLeft: "0px", marginLeft: "40%"}}/>
                : props.question.userChoose === props.label ?
                <Icon.X style={{color: 'red', paddingLeft: "0px", marginLeft: "40%"}}/>
                : ""
              : ""
          }
        </Col>
        <Col  xs="10" md="10" sm="10" lg="10" style={{paddingBottom:'2%' }}>   
        {/* style={{display: 'flex'}} */}
          {
            props.question.correct != undefined ? 
              props.question.userChoose === props.label ?  
                <Fragment>
                  {/* <FormGroup check style={{  marginBottom: '3%'}}>
                    <Label  style={{  fontSize: '1rem'}}>
                      <Input
                        id={props.label}
                        type="radio"
                        checked="true"
                        onChange={onChangeChecked}
                        name={props.field.name}
                        value={props.label}
                        readOnly="true"
                      /> {"("+libAlphabet[props.indexAnswersToChoose]+") " + props.label}  
                    </Label> 
                  </FormGroup> */}
                      <Input
                        id={props.label}
                        type="radio"
                        checked="true"
                        onChange={onChangeChecked}
                        name={props.field.name}
                        value={props.label}
                        readOnly="true"
                      /> 
                      <label for={props.label} style={{display: 'inline', fontSize: '1rem'  }} >{"("+libAlphabet[props.indexAnswersToChoose]+") " + props.label}  
                         {   transcript }</label>
                   
                 </Fragment>
                 :
                 <Fragment>
                      <Input
                        id={props.label}
                        type="radio"
                        onChange={onChangeChecked}
                        name={props.field.name}
                        value={props.label}
                        disabled="true"
                      /> 
                       <label  for={props.label} style={{display: 'inline', fontSize: '1rem'}} > {"("+libAlphabet[props.indexAnswersToChoose]+") " + props.label} 
                          { transcript  }
                       </label>
                  </Fragment> :
               <Fragment>
                      <Input
                        id={props.label}
                        type="radio"
                        onChange={onChangeChecked}
                        color="info"
                        name={props.field.name}
                        value={props.label}
                      /> 
                      <label  for={props.label} style={{display: 'inline', fontSize: '1rem'}} >
                           {"("+libAlphabet[props.indexAnswersToChoose]+") " + props.label}
                           {  transcript  }
                      </label>
               
               </Fragment>
               
          }
          <br/>
          {/*<Button onClick={()=>{*/}
          {/*  console.log("1",props.question.correct != undefined)*/}
          {/*  console.log("2",props.question.userChoose === props.label)*/}
          {/*}} >test</Button>*/}
        </Col>
        </Fragment>
    )
  }


  render() {
    // const ErrorMessage = ({name}) => (
    //   <Field name={name}>
    //     {({field, form, meta}) => {
    //       const error = getIn(form.errors, name);
    //       const touch = getIn(form.touched, name);
    //       return touch && error ? (
    //         <div className="invalid-feedback" style={{display: "contents"}}>{error}</div>
    //       ) : null;
    //     }}
    //   </Field>
    // );
    const {readingReducer, category, index, classes, len} = this.props
    return (
      <Formik
        enableReinitialize
        initialValues={{
          category: this.props.category
        }}
        validationSchema={formSchema}
        onSubmit={(values, {setSubmitting}) => {
          setSubmitting(false);
          const {readingAction} = this.props;
          const {submitSingleAndDualReadingQuestion} = readingAction
          submitSingleAndDualReadingQuestion(values.category)
          this.props.scrollTopPage();

        }}
      >
        {({errors, touched, values, setFieldValue}) => (
          <Form>
            <div style={{padding: "3% 0px 0px 0px"}}>
              <Row style={{width:"100%",marginLeft: "auto", marginRight: "auto"}}>
                <Col xs={6}>
                  {
                    category.pathFile1 ?
                      <div style={{
                        backgroundColor: "#f2f2f2",
                        padding: "1% 1% 1% 1%",
                        width: "90%",
                        marginLeft: "auto",
                        marginRight: "auto"
                      }}>
                         
                          {/* { category.typeFile1 === 'FILE' ? */}
                            <div width="100%" height="100%" className="text-center">
                              <img width="100%" height="100%" src={`${category.pathFile1}`}/>
                            </div>
                             {/* ?:
                            <div>
                               <div className="text-center" style={{fontSize: "large", fontWeight: "bold"}}>
                                {category.title1}
                              </div>
                              <div>
                                {category.pathFile1}
                              </div>  
                            </div>  } */}
                        
                      </div> : ""
                  }
                  <br/>
                  {
                    category.pathFile2 ?
                      <div style={{
                        backgroundColor: "#f2f2f2",
                        padding: "1% 1% 1% 1%",
                        width: "90%",
                        marginLeft: "auto",
                        marginRight: "auto"
                      }}>
                        {
                            <div width="100%" height="100%" className="text-center">
                              <img width="100%" height="100%" src={category.pathFile2}/>
                            </div>
                        }
                      </div> : ""
                  }
                  <br/>
                  {
                    category.pathFile3 ?
                      <div style={{
                        backgroundColor: "#f2f2f2",
                        padding: "1% 1% 1% 1%",
                        width: "90%",
                        marginLeft: "auto",
                        marginRight: "auto"
                      }}>
                        {
                            <div width="100%" height="100%" className="text-center">
                              <img width="100%" height="100%" src={category.pathFile3}/>
                            </div>
                        }
                      </div> : ""
                  }
                </Col>
                <Col xs={5} className="text-center" >
                  {
                    category.listQuestion ? category.listQuestion.map((quest, indexQuest) => (
                      <div key={indexQuest} className="text-left">
                        <div>
                          <div>
                            <strong style={{display: 'inline'}} >
                               
                              {(indexQuest+1) + "." + quest.name}
                              {
                              this.props.readingReducer.tempAnsweredReadingSingleAndDualQuest ?
                              <Fragment > 
                              <button   style={{  border: 'none', backgroundColor: 'white', padding: '0', marginLeft: '1%'}}
                                    id={"a" + quest.id } className="mr-1 mb-1"  
                                    onClick = {e=>{e.preventDefault()}} >
                                <Icon.AlertCircle  size={20} />
                                </button>
                                <UncontrolledPopover trigger="focus"  placement="bottom" target={"a" + quest.id}
                                 
                                >
                                <PopoverBody style={{borderColor: 'green !important',
                                color: 'white', backgroundColor: 'green', borderRadius: '5px'}}>
                                <FormattedMessage id={"translate"}/>: { quest.translatingQuestion }
                                </PopoverBody>
                                </UncontrolledPopover></Fragment> :''
                                 }
                            </strong>
                              
                          </div>
                          <br/>
                        </div>
                       
                        { 
                          quest.listAnswerToChoose ? quest.listAnswerToChoose.map((answer, indexAnswersToChoose) =>{
                            
                            return(
                            <Row key={indexAnswersToChoose} style={{padding: "0px 0px 0px 0px"}}>
                              <Field name={`category.listQuestion[${indexQuest}]userChoose`}
                                     indexAnswersToChoose={indexAnswersToChoose}
                                     label={answer.answer}
                                     classes= {classes}
                                     className="form-control"
                                     component={this.radioButtonAnswer}
                                     setFieldValue={setFieldValue}
                                     errorMessage={errors[`category.listQuestion[${indexQuest}]userChoose`] ? errors[`category.listQuestion[${indexQuest}]userChoose`] : undefined}
                                     touched={touched[`category.listQuestion[${indexQuest}]userChoose`]}
                                     question={quest}
                                     style={{display: "flex"}}

                              />  
                            </Row>
                          )}) : ""
                        }  
                        {/*<div>*/}
                        {/*  <ErrorMessage name={`category.listQuestion[${indexQuest}]`}/>*/}
                        {/*</div>*/}
                        {
                          quest.correct != undefined ?
                            <div>
                              <div>
                                <div xs="6" className={classes.title}>
                                  <Badge color="success" style={{background:"green"}} className="mr-1 mb-1 badge-square">
                                    <span>
                                      {/*Giải thích*/}
                                      <FormattedMessage id="explanation"/>
                                    </span>
                                  </Badge>
                                </div>
                              </div>
                              <div>
                                <div style={{border: "green 2px solid", padding: "1% 1% 3% 1%", fontStyle: 'italic'}}
                                     className={classes.content + " text-success text-left"}>
                                  <span>{quest.description ? quest.description : <FormattedMessage id={"no.explanation"}/>}</span>
                                </div>
                              </div>
                            </div> : ""
                        }
                        <br/>
                      </div>
                    )) : ""
                  }

                </Col>
                <Col xs={1}>
                  {
                    this.props.readingReducer.tempAnsweredReadingSingleAndDualQuest ?
                      !readingReducer.checkReview ?
                        index === (len - 1) ?
                          <Icon.ChevronsRight onClick={() => {
                            history.push("/pages/aggregatedResults")
                          }} size={50} style={{width: "100%"}}/>
                          :
                          <Icon.ChevronsRight onClick={() => {
                            this.props.nextQuestion(this.props.category.listQuestion.length)
                          }} size={50} style={{width: "100%"}}/>
                        : <Icon.ChevronsRight onClick={() => {
                          history.push("/pages/aggregatedResults")
                        }} size={50} style={{width: "100%"}}/>
                      : ""
                  }
                </Col>
              </Row>
              <Row>
                <Col xs={12} className="text-center" >
                  {
                    readingReducer.tempAnsweredReadingSingleAndDualQuest ? ""
                      :
                      (errors.category && touched.category) || (!touched.category && !errors.category) ?
                        <Button onClick={() => {
                          toast.error(showMessage("please.answer.all.of.the.questions"))
                        }} style={{ marginTop: "5%",  marginBottom: "3%"}} color="info">
                          <strong>
                            {/*Nộp Bài*/}
                            <FormattedMessage id="submit"/>
                          </strong>
                        </Button>
                        :
                        <Button type="submit" style={{ marginTop: "5%",marginBottom: "3%"}} color="info">
                          <strong>
                            {/*Nộp Bài*/}
                            <FormattedMessage id="submit"/>
                          </strong>
                        </Button>
                  }
                  {/*<Button onClick={() => {*/}
                  {/*  console.log("values:", values)*/}
                  {/*  console.log("errors:", errors)*/}
                  {/*  console.log("touched:", touched)*/}
                  {/*}} style={{marginBottom: "3%"}} color="info">*/}
                  {/*  checkErrors*/}
                  {/*</Button>*/}
                </Col>
              </Row>
            </div>
          </Form>
        )}
      </Formik>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    readingAction: bindActionCreators(readingAction, dispatch)
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    readingReducer: state.readingReducer,
  }
}
const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
)

export default compose(withStyles(styles.style), withConnect)(Category);
