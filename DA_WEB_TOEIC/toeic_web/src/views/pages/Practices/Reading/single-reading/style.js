import styled from "styled-components"
import ReactPlayer from "react-player"


export const style = (theme) => ({
  borderdiv: {
    width: "99%",
    marginLeft: "0.5%",
    borderBottom: "1px rgba(22, 155, 213, 1) solid",
    borderRight: "1px rgba(22, 155, 213, 1) solid",
    borderLeft: "1px rgba(22, 155, 213, 1) solid",
  },
  blackSwipe: {
    color: "black"
  },
  content: {
    marginBottom: "5%",
    marginLeft: "10%",
    width: "80%",
  },
  title: {
    marginLeft: "12%",
    transform: "translate(0,60%)",
  },
  titaleModal: {
    marginLeft: "2%",
    position: "absolute",
    top: "50%",
    MsTransform: "translateY(-50%)",
    transform: "translateY(-50%)",
    fontSize: "130%",
  },
  btntranslate :{
    border: 'none',
    '&:focus': {
      border: 'none',
    },
    '&:hover': {
      border: 'none',
    }
  }
  // buttonBeforeAnswer: {
  //   width: "79%",
  //   padding: "3% 0",
  //   borderRadius: "10px ! important",
  //   background: "rgba(22, 155, 213, 1)",
  //   color: "white",
  //   border: "rgba(22, 155, 213, 1)"
  // },
})
export const ReactPlayerReading = styled(ReactPlayer)`
  margin : 0 auto 30px;

  & audio{
    outline : none;
  }
`
