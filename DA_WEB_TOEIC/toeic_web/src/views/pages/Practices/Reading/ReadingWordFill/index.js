import React, { Component } from 'react';
import * as Styles from "./style"
import Question from "./question"
import {connect} from "react-redux";
import {bindActionCreators, compose} from "redux";
import readingActions from "../../../../../redux/actions/practice/reading";
import practiceListenMultiActions from "../../../../../redux/actions/practice/listening";
import {FormattedMessage} from "react-intl";
import Spinner from "reactstrap/es/Spinner";

class Index extends Component {

  constructor(props) {
    super(props);
    this.state = {
      listSelectedAnswer : [],
      stt: this.props.index,
    }
  }

  componentDidMount() {
    const { listeningActions, readingActions} = this.props;
    const { updateCheckReviewFalse} = readingActions;
    const {cleanListQuestionAndAnswerListening} = listeningActions;
    // cleanListQuestionAndAnswerListening();
    updateCheckReviewFalse();
    const element = document.getElementById("questionPart");
    element.scrollIntoView()
  }


  next = () => {
    this.props.readingActions.updateIsSubmit();
    this.setState({
      stt: this.state.stt + 1
    })
  }

  loadingComponent = (
    <div style={{
      position: 'absolute',
      zIndex: 110,
      top: '110%',
      left: 0,
      width: '100%',
      height: '100%',
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      background: 'rgba(255,255,255,0.8)'
    }}>
      <Spinner color="primary" className="reload-spinner"/>
    </div>
  );

  render() {
    const {listQuestionReduce} = this.props;
    let listQuestion = listQuestionReduce ? listQuestionReduce.map((question , index) => {
            return (
              <Question
                        key={index}
                        question={question}
                        index={index}
                        stt={this.state.stt}
                        // selectAnswer={this.selectAnswer}
                        idQuestion={question.id}
                        next={this.next}
                        isSubmit={this.props.isSubmit}
              ></Question>)
    }) : null

    return (
      <Styles.ContainerStyle id="questionPart">
        <h3 style={{textAlign:'center', marginBottom:'2%', fontWeight:'700'}}>
          <FormattedMessage id="practices.reading.incompleteSentences.namePart" tagName="data" />
        </h3>
        {listQuestion ? listQuestion[this.state.stt] : null}
        {this.props.loadingGetQuestionReadFill === true ? this.loadingComponent : null}
      </Styles.ContainerStyle>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  readingActions : bindActionCreators(readingActions, dispatch),
  listeningActions : bindActionCreators(practiceListenMultiActions, dispatch)
})

const mapStateToProps = (state) => {
  return {
    listQuestionReduce: state.readingReducer.listQuestion,
    resultQuestionReduce: state.readingReducer.resultQuestion,
    index: state.readingReducer.index,
    isSubmit: state.readingReducer.isSubmit,
    loadingGetQuestionReadFill:state.readingReducer.loadingGetQuestionReadFill
  }
}
const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
)

export default compose(withConnect)(Index);
