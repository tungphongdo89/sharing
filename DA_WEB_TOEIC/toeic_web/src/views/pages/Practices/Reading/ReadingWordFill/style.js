import styled from "styled-components"
import { Card , CardBody , CardTitle , CardHeader , Row , Col , Button , Container , Label } from "reactstrap"

export const ContainerStyle = styled(Container)`
  width : 70%;
`

export const CartStyle = styled(Card)`
  margin-bottom: 20px;
  border : 0;
`

export const CardHeaderStyle = styled(CardHeader)`
  background: #169bd5;
  height: 3rem;
  &&&{
    border-radius: 10px;
     background: rgba(22, 155, 213, 1);
  }
`

export const CardTitleStyle = styled(CardTitle)`
   margin-top:-1.5rem;
  width : 100%;
`

export const ColTitle = styled(Col)`
  text-align : right;
`

export const CardBodyStyle = styled(CardBody)`
  margin : 0 1.5%;
  padding: 45px 0 0;
  border: 1px solid rgba(22, 155, 213, 1);
  border-top : 0;
`

export const RowQuestion = styled(Row)`
  padding : 0 7%;
  text-align : center;
  margin-bottom : 30px;
`

export const RowContent = styled(Row)`
  padding : 0 5%;
`

export const RowDescription = styled(Row)`
  position : relative;
  width : 90%;
  margin : 30px auto;
  padding : 10px 2%;
  border : 1px solid green;
  left: 1%;
`

export const LabelDescription = styled(Label)`
  position: absolute;
  top: -14px;
  background: green;
  color : white;
  padding: 0 2%;
  text-align: center;
`

export const LabelAnswer = styled(Label)`
  width : 100%;
  color: green;
`

export const ColItemStyle = styled(Col)`
  text-align: center;
  margin-bottom: 20px;
`;

export const ButtonQuestionStyle = styled(Button)`
  width: 100%;
  padding: 20px 0;
  border-radius: 20px;
  text-align: center;
  background-color : rgba(22, 155, 213, 1);
`

export const ButtonAnswerStyle = styled(Button)`
  width: 100%;
  padding: 20px 0;
  margin: 0 auto 30px;
  border-radius: 20px;
  text-align: center;
  
`

export const RowButton = styled(Row)`
  margin : 20px 0;
`

export const ButtonSubmit = styled(Button)`
  width : 15%;
  border-radius : 15px;
  margin : 0 auto;
  padding : 10px 0;
`
