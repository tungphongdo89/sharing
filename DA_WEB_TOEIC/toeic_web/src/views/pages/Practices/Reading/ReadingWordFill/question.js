import React, { Component } from 'react';
import {Row, Col, Label, Button, UncontrolledPopover, PopoverBody} from "reactstrap"
import * as Styles from "./style"
import {ChevronsRight, AlertCircle} from "react-feather";
import * as Icon from "react-bootstrap-icons";
import {bindActionCreators, compose} from "redux";
import readingActions from "../../../../../redux/actions/practice/reading";
import {connect} from "react-redux";
import Spinner from "reactstrap/es/Spinner";
import {history} from "../../../../../history";
import {FormattedMessage} from "react-intl";
import {toast} from "react-toastify";
import {showMessage} from "../../../../../commons/utils/convertDataForToast";

class Question extends Component {

  constructor(props) {
    super(props);
    this.state = {
      selectedItem : "",
      numberSelected: 0,
      checkReview: this.props.checkedReview
    }
  }

  selectAnswer = (index , i) => {
    let answer;
    if(index === 0){
      answer = 'A';
    } else if(index === 1){
      answer = 'B';
    } else if(index === 2){
      answer = 'C';
    } else {
      answer = 'D';
    }
    this.setState({
      selectedItem: answer,
      numberSelected: ++this.state.numberSelected
    })
    const {readingActions} = this.props;
    const {getResultQuestionOfReadingWordFill} = readingActions;
    getResultQuestionOfReadingWordFill(this.props.question.id, answer, this.state.numberSelected);
    // this.props.selectAnswer(answer , index)
    // if(this.props.listQuestionAndAnswer[this.props.index] && this.props.listQuestionAndAnswer[this.props.index].indexInCorrect !== null &&
    //   (this.props.listQuestionAndAnswer[this.props.index].numberSelected  === 1 || this.props.listQuestionAndAnswer[this.props.index].numberSelected  === 2)){
    //   toast.error("ban chon sai roi")
    // }
  }

 // componentDidUpdate(prevProps, prevState, snapshot) {
 //    debugger
 //   console.log(prevProps)
 //   console.log(prevState)
 // }

  componentWillReceiveProps(nextProps, nextContext) {
    console.log(nextProps)
    if(nextProps.listQuestionAndAnswer[this.props.index] && nextProps.listQuestionAndAnswer[this.props.index].indexInCorrect !== null && !nextProps.isSelect &&
      nextProps.listQuestionAndAnswer[this.props.index].numberSelected  < 3){
      toast.error(showMessage("chosen.incorrect.chosen.again"));
    }
  }


  next = () =>{
    if(this.props.index + 1 === this.props.listQuestionReduce.length || this.state.checkReview){
      history.push("/pages/aggregatedResults");
    }else {
      this.props.next();
    }
  }

  loadingComponent = (
    <div style={{
      position: 'absolute',
      zIndex: 110,
      top: 0,
      left: 0,
      width: '100%',
      height: '100%',
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      background: 'rgba(255,255,255,0.8)'
    }}>
      <Spinner color="primary" className="reload-spinner"/>
    </div>
  );

  renderAnswer =(answer,index)=>{
    const {listQuestionAndAnswer} = this.props;
    let xhtml = null;
    if((listQuestionAndAnswer[this.props.index].indexCorrect !== null && index === listQuestionAndAnswer[this.props.index].indexCorrect) ||
      (listQuestionAndAnswer[this.props.index].numberSelected && listQuestionAndAnswer[this.props.index].numberSelected  === 3)){
      xhtml = (<Styles.ButtonAnswerStyle color="success" style={{borderRadius:'10px',fontWeight:'bold'}} id="btnHighLight">
        <Icon.Check2 size={20} style={{marginRight:'4%'}}></Icon.Check2> { index===0 ? "A" : (index===1 ? "B" : (index===2 ? "C" : "D")) }. {answer}
      </Styles.ButtonAnswerStyle>)
    } else if(listQuestionAndAnswer[this.props.index].indexCorrect !== null && index !== listQuestionAndAnswer[this.props.index].indexCorrect){
      xhtml = ( <Styles.ButtonAnswerStyle disabled color="secondary" style={{borderRadius:'10px',fontWeight:'bold'}} >
        { index===0 ? "A" : (index===1 ? "B" : (index===2 ? "C" : "D")) }. {answer}
      </Styles.ButtonAnswerStyle>)
    } else if(listQuestionAndAnswer[this.props.index].indexInCorrect !== null && index === listQuestionAndAnswer[this.props.index].indexInCorrect){
      xhtml =(<Styles.ButtonAnswerStyle color="danger" style={{borderRadius:'10px',fontWeight:'bold'}} id="btnHighLight">
        <Icon.X size={20} style={{marginRight:'4%'}}></Icon.X> { index===0 ? "A" : (index===1 ? "B" : (index===2 ? "C" : "D")) }.{answer}
      </Styles.ButtonAnswerStyle>)
    } else {
      xhtml = ( <Styles.ButtonAnswerStyle color="info" style={{borderRadius:'10px', fontWeight:'bold'}}  onClick={() => this.selectAnswer(index , this.props.index)} id="btnHighLight">
        { index===0 ? "A" : (index===1 ? "B" : (index===2 ? "C" : "D")) }. {answer}
      </Styles.ButtonAnswerStyle>)
    }
    return xhtml;
  }

  renderAfter3Click = (answer, index)=>{
    const {listQuestionAndAnswer} = this.props;
    let xhtml = null;
    if((listQuestionAndAnswer[this.props.index].indexCorrect !== null && index == listQuestionAndAnswer[this.props.index].indexCorrect)
      && (listQuestionAndAnswer[this.props.index].numberSelected && listQuestionAndAnswer[this.props.index].numberSelected  === 3)){
      xhtml = (<Styles.ButtonAnswerStyle color="success" style={{borderRadius:'10px',fontWeight:'bold'}} id="btnHighLight" >
        <Icon.Check2 size={20} style={{marginRight:'4%'}}></Icon.Check2> { index===0 ? "A" : (index===1 ? "B" : (index===2 ? "C" : "D")) }. {answer}
      </Styles.ButtonAnswerStyle>)
    } else if(listQuestionAndAnswer[this.props.index].indexInCorrect !== null && index === listQuestionAndAnswer[this.props.index].indexInCorrect
      && (listQuestionAndAnswer[this.props.index].numberSelected && listQuestionAndAnswer[this.props.index].numberSelected  === 3)){
      xhtml = (<Styles.ButtonAnswerStyle color="danger" style={{borderRadius:'10px',fontWeight:'bold'}} id="btnHighLight" >
        <Icon.X size={20} style={{marginRight:'4%'}}></Icon.X> { index===0 ? "A" : (index===1 ? "B" : (index===2 ? "C" : "D")) }. {answer}
      </Styles.ButtonAnswerStyle>)
    } else {
      xhtml = (<Styles.ButtonAnswerStyle disabled color="secondary" style={{borderRadius:'10px',fontWeight:'bold'}} id="btnHighLight" >
        { index===0 ? "A" : (index===1 ? "B" : (index===2 ? "C" : "D")) }. {answer}
      </Styles.ButtonAnswerStyle>)
    }
    return xhtml;
  }


  render() {
    let questionProps = this.props.question
    const {resultQuestionReduce,isSelect, totalTrue, listQuestionAndAnswer} = this.props;

    let listQuestionMap = questionProps.answersToChoose ? questionProps.answersToChoose.toLowerCase().split("|",4).map((answer, index) => {
      return <Styles.ColItemStyle xs="5" xl="5" key={index}>
        <Styles.ButtonQuestionStyle color="info"
                                    onClick={() => this.selectAnswer(index , this.props.index)}
                                    style={{borderRadius:'10px', fontWeight:'bold'}}
                                    id="btnHighLight"
        >
          { index===0 ? "A" : (index===1 ? "B" : (index===2 ? "C" : "D")) }. {answer}
        </Styles.ButtonQuestionStyle>
    </Styles.ColItemStyle>
    }): ''

    let listCorrectAnswerMap = listQuestionAndAnswer[this.props.index] && listQuestionAndAnswer[this.props.index].answersToChoose ?
      listQuestionAndAnswer[this.props.index].answersToChoose.toLowerCase().split("|", 4).map((answer, index) => {
      return (
        <Styles.ColItemStyle xs="6" xl="6" key={index}>
          {
            this.renderAnswer(answer,index)
          }
        </Styles.ColItemStyle>)
    }): null

    let listCorrectAnswerMapAfter3Click =listQuestionAndAnswer[this.props.index] && listQuestionAndAnswer[this.props.index].answersToChoose ?
      listQuestionAndAnswer[this.props.index].answersToChoose.toLowerCase().split("|", 4).map((answer, index) => {
      return (
        <Styles.ColItemStyle xs="6" xl="6" key={index}>
          { this.renderAfter3Click(answer,index)}
        </Styles.ColItemStyle>)
    }): null


    let renderSelect = ()=>{
      if(this.props.isSubmit || this.state.checkReview ){
        if(listQuestionAndAnswer[this.props.index] &&
          listQuestionAndAnswer[this.props.index].numberSelected && listQuestionAndAnswer[this.props.index].numberSelected  === 3){
          return listCorrectAnswerMapAfter3Click;
        } else {
          return listCorrectAnswerMap
        }
      } else {
        return listQuestionMap
      }
    }

    return (
      <>
        <style>{`
          #btnHighLight:focus-within {
            border: 1px solid black !important;
            border-radius: 10px;
          }
           #btnNextQuestion {
            border: none;
            background-color: white;
          }

          #btnNextQuestion:focus-within {
            color: green;
            cursor: pointer;
            border-color: black;
          }
        `}</style>
      <Styles.CartStyle>
        { isSelect ? this.loadingComponent : null}
        <Styles.CardHeaderStyle>
          <Styles.CardTitleStyle>
            <Row>
              <Col xs="6" sm="6" md="6" lg="6" xl="6" style={{color:'white', fontWeight:'700', fontSize:'1.8rem'}}>
                <FormattedMessage id="question" tagName="data" /> {this.props.index + 1}/{this.props.listQuestionReduce.length}
              </Col>

              <Styles.ColTitle xs="6" sm="6" md="6" lg="6" xl="6" style={{color:'white', fontWeight:'700', fontSize:'1.8rem'}}>
                <FormattedMessage id="numberCorrect" tagName="data" /> : {totalTrue ? totalTrue : 0}/{this.props.listQuestionReduce.length}
              </Styles.ColTitle>
            </Row>
          </Styles.CardTitleStyle>
        </Styles.CardHeaderStyle>

        <Styles.CardBodyStyle>
          <Styles.RowQuestion>
            <Col style={{fontWeight:'bold', lineHeight:'2'}}>{questionProps.name}</Col>
          </Styles.RowQuestion>

          <Styles.RowContent>
              {/*{ this.props.isSubmit || this.state.checkReview ? listQuestionAndAnswer[this.props.index] &&*/}
              {/*listQuestionAndAnswer[this.props.index].numberSelected && listQuestionAndAnswer[this.props.index].numberSelected  === 3 ?*/}
                {/*listCorrectAnswerMapAfter3Click :listCorrectAnswerMap*/}
               {/*: listQuestionMap}*/}
            <Col xs={11}>
              <Row style={{marginLeft: this.props.isSubmit || this.state.checkReview ? '20%' : '20%', marginRight:this.props.isSubmit || this.state.checkReview ? '12%' :''}}>
               {renderSelect()}
              </Row>
            </Col>
            <Col xs={1}>
              {
                this.props.isSubmit || this.state.checkReview ? ((listQuestionAndAnswer[this.props.index]  &&
                  listQuestionAndAnswer[this.props.index].userChoose.toLowerCase() === listQuestionAndAnswer[this.props.index].answer.toLowerCase())
                  || (listQuestionAndAnswer[this.props.index]  && listQuestionAndAnswer[this.props.index].numberSelected === 3)
                  || this.props.numberSelected === 3)
                  ?
                  <button onClick={() => this.next()} className="float-right"  id="btnNextQuestion">
                    <ChevronsRight size={40}  />
                  </button> : null : null
              }
            </Col>
          </Styles.RowContent>

          {
            this.props.isSubmit || this.state.checkReview ?  ((listQuestionAndAnswer[this.props.index]  &&
            listQuestionAndAnswer[this.props.index].userChoose.toLowerCase() === listQuestionAndAnswer[this.props.index].answer.toLowerCase())
              || (listQuestionAndAnswer[this.props.index]  && listQuestionAndAnswer[this.props.index].numberSelected === 3)
            || this.props.numberSelected === 3)
            ?
            <Styles.RowDescription>
              <Styles.LabelDescription><FormattedMessage id="description" tagName="data" /></Styles.LabelDescription>
              <Label style={{color: 'green'}}>{listQuestionAndAnswer[this.props.index] ? listQuestionAndAnswer[this.props.index].description : ''}</Label>
            </Styles.RowDescription>
          : null : null
          }
        </Styles.CardBodyStyle>
      </Styles.CartStyle>
        </>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  readingActions : bindActionCreators(readingActions, dispatch)
})

const mapStateToProps = (state) => {
  return {
    listQuestionReduce: state.readingReducer.listQuestion,
    resultQuestionReduce: state.readingReducer.resultQuestion,
    isSelect: state.readingReducer.isSelect,
    totalTrue: state.readingReducer.totalTrue,
    checkedReview: state.readingReducer.checkReview,
    listQuestionAndAnswer: state.readingReducer.listQuestionAndAnswerTemp,
    numberSelected: state.readingReducer.numberSelected
  }
}
const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
)

export default compose(withConnect)(Question);
