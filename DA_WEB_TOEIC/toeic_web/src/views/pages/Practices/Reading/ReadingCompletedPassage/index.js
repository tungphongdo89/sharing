import React, {PureComponent} from 'react';
import {connect} from "react-redux";
import {bindActionCreators, compose} from "redux";
import Question from "./question";
import readingActions from "../../../../../redux/actions/practice/reading";
import practiceListenMultiActions from "../../../../../redux/actions/practice/listening";
import {withStyles} from "@material-ui/core";
import styles from "./style";
import Spinner from "reactstrap/es/Spinner";

class Index extends PureComponent {

  constructor(props){
    super(props);
    this.state = {
      playing: false,
      timeAgain: 0,
      data: false,
      index: this.props.index,
      load: false,
      answer: {},
      correctQues: 0,
      listAnswerEntered: {},
      isSubmit: false,
      stt: this.props.index,
    }
  }

  next = (number) => {
    this.setState({
      stt: this.state.stt + 1,
      correctQues: number,
    })
  }

  componentDidMount() {
    const {readingActions, listeningActions} = this.props;
    const { updateCheckReviewFalse} = readingActions;
    const {cleanListQuestionAndAnswerListening} = listeningActions;
    cleanListQuestionAndAnswerListening();
    updateCheckReviewFalse();
  }
  componentWillReceiveProps(nextProps, nextContext) {
    this.setState({
      correctQues: 0
    })
  }

  loadingComponent = (
    <div style={{
      position: 'absolute',
      zIndex: 110,
      top: '110%',
      left: 0,
      width: '100%',
      height: '100%',
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      background: 'rgba(255,255,255,0.8)'
    }}>
      <Spinner color="primary" className="reload-spinner"/>
    </div>
  );

  render() {
    const {listQuestionReduce, classes} = this.props;
    let listInQuestion =listQuestionReduce ? listQuestionReduce ? listQuestionReduce.map((question, index) => {
        return  <Question key={index}
                          question={question}
                          onSubmit={this.onSubmit}
                          index={index}
                          stt={this.state.stt}
                          next={this.next}
                          correctQues={this.state.correctQues}
        ></Question>
      }) : null
      : null

    return (
      <div className={classes.ContainerStyle}>
        {listInQuestion ? listInQuestion[this.state.stt] : ''}
        {this.props.loadingGetQuestionReadFill === true ? this.loadingComponent : null}
      </div>
    );

  }
}

const mapStateToProps = state => {
  return {
    listQuestionReduce: state.readingReducer.listQuestion,
    resultQuestionReduce: state.readingReducer.resultQuestion,
    index: state.readingReducer.index,
    loadingGetQuestionReadFill: state.readingReducer.loadingGetQuestionReadFill
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    readingActions : bindActionCreators(readingActions, dispatch),
    listeningActions : bindActionCreators(practiceListenMultiActions, dispatch)
  }
}
const withConnect = connect(mapStateToProps,mapDispatchToProps)
export default compose(withStyles(styles), withConnect)(Index);
