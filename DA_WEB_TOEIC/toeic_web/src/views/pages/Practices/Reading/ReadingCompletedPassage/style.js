
const styles = () => ({
  ContainerStyle:{
    width : 'auto',
    marginLeft: '5%',
    marginRight: '5%'
  },
  topicName:{
    textAlign: 'center',
    fontSize: '150%',
    fontWeight:'500'
  },
  titleModal:{
    backgroundColor: '#1da9ed',
    margin: '0 12%',
    height: '50px',
    borderRadius: '8px',
    color: 'white',
    position: 'relative'
  },
  bodySnp:{
    paddingLeft: '0px',
    paddingRight: '0px'
  },
  bodyModal:{
    backgroundColor: 'white',
    margin: '0 6px',
    border: '1px solid #0f7aae'
  },
  questionMargin:{
  marginBottom: '3%',
  color: 'black'
  },
  answerToChooseMargin:{
    marginBottom: '3%',
    paddingLeft: '5%'
  },
  labelRadioButton:{
    display: 'contents',
    fontSize: '100%'
  },
  divSubmit: {
    textAlign: 'center',
    margin: '5% 0 5% 0',
  },
  legend:{
    color: 'black',
    textAlign: 'left',
    backgroundColor: 'lightgray',
    width: 'auto',
    padding: '5px 20px',
    fontSize: 'medium'
  },
  fieldset:{
    padding: '0 2% 2% 2%',
    textAlign: 'left',
    backgroundColor: '#d3d3d347'
  },
  questionName:{
    paddingLeft: '0px !important',
    marginBottom: '2%',
    color: 'black'
  },
  legendDescription:{
    color: 'white',
    textAlign: 'left',
    backgroundColor: 'green',
    width: 'auto',
    padding: '1px 20px',
    fontSize: 'small'
  },
  fieldsetDescription:{
    border: '2px solid green',
    padding: '0 2% 2% 2%',
    textAlign: 'left',
    color: 'green'
  },
  nextQuestion: {
    textAlign: 'center',
    padding: '0px',
    marginTop: '10%',
    cursor: 'pointer'
  },
// #iconNextQuestion{
//   margin: 0px !important;
//   cursor: pointer;
//   color: #b2aeae ;
// }
/*#iconNextQuestion:hover{*/
/*color: green;*/
/*}*/
  btnRepeat:{
    backgroundColor: 'white',
    border: '2px solid orange',
    color: 'orange',
    height: '80%',
    fontSize: '80%',
    borderRadius: '5px'

  }
  })
export default styles;
