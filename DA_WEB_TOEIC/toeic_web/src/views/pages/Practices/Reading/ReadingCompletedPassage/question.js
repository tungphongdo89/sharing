import React, {PureComponent} from 'react';
import {connect} from "react-redux";
import * as Icon from "react-feather";
import {bindActionCreators, compose} from "redux";
import styles from "./style";

import {
  Container,
  Row,
  Col,
  Input,
  Form,
  Spinner, FormGroup, Label, Badge, UncontrolledPopover, PopoverBody
} from "reactstrap"

import readingActions from "../../../../../redux/actions/practice/reading";
import {toast} from "react-toastify";
import practiceListenMultiActions from "../../../../../redux/actions/practice/listening";
import {history} from "../../../../../history";
import {withStyles} from "@material-ui/core";
import {FormattedMessage} from "react-intl";
import {showMessage} from "../../../../../commons/utils/convertDataForToast";

class Question extends PureComponent {

  constructor(props) {
    super(props);
    this.state = {
      answer: [],
      correctQues: this.props.correctQues,
      isSubmit: this.props.isSubmit,
      indexQuestion: {},
      indexSubAnswer: {},
      answerChoose: {},
      correct: 0,
      totalSubCorrect: 0,
      stt: this.props.stt,
      checkReview: this.props.checkedReview
    }
  }


  //cut ca dap an de chon ra 1 list
  cutAnswerToChoose(answerToChoose) {
    let answerArray = [];
    if (answerToChoose) {
      answerArray = answerToChoose.split("|", 4);
    }
    return answerArray;
  }

  //tung gia tri dau vao laf : A.dap an sai  |   B.dap an dung
  cutSubAnswer(subAnswer) {
    let index = subAnswer.indexOf("|");
    let title = subAnswer.substring(0, index);
    let content = subAnswer.substring(index + 1);

    let array = [];
    array.push(title);
    array.push(content);
    return array;
  }

  cutAnswer(subAnswer) {
    let index = subAnswer.indexOf(".");
    let title = subAnswer.substring(0, index);
    let content = subAnswer.substring(index + 1);

    let array = [];
    array.push(title);
    array.push(content);
    return array;
  }

  cutInputValue(value) {
    let index = value.indexOf("|");
    let title = value.substring(0, index);
    let content = value.substring(index + 1);

    let array = [];
    array.push(title);
    array.push(content);
    return array;
  }


  onChangeValue = (e, question) => {
    // let answerChoose = this.cutInputValue(e.target.value)[0];
    let answerContent = this.cutInputValue(e.target.value)[1];
    let indexQuestion = this.cutAnswer(answerContent)[0];
    let indexSubAnswer = this.cutAnswer(answerContent)[1];
    let answerChoose;
    if(indexSubAnswer == 0){
      answerChoose = 'A';
    } else if(indexSubAnswer == 1){
      answerChoose = 'B';
    } else if(indexSubAnswer == 2){
      answerChoose = 'C';
    } else {
      answerChoose = 'D';
    }

    let {answer} = this.state;

    let newListChoose = this.state.answerChoose
    let newListIndexQuestion = this.state.indexQuestion
    let newListIndexSubAnswer = this.state.indexSubAnswer

    newListChoose[indexQuestion] = answerChoose
    newListIndexQuestion[indexQuestion] = parseInt(indexQuestion)
    newListIndexSubAnswer[indexQuestion] = parseInt(indexSubAnswer)
    answer.push({newListChoose, newListIndexQuestion, newListIndexSubAnswer})
    const {readingActions} = this.props;
    const {saveUserChoose} = readingActions;
    saveUserChoose(question.id, answerChoose);
    this.setState({
      indexQuestion: newListIndexQuestion,
      indexSubAnswer: newListIndexSubAnswer,
      answerChoose: newListChoose,
      answer: answer
    })

  }

  showAnswer = () => {
    const {readingActions, question} = this.props;
    const {getResultQuestionOfReadingCompletedPassage} = readingActions;
    if (Object.values(this.state.answerChoose).length < question.listAnswerToChoose.length) {
      toast.error(showMessage("please.choose.full.answer"));
    } else {
      let data = {};
      data.categoryId = question.idCategory;
      data.listQuestionAnswer = this.props.listUserChoose;
      getResultQuestionOfReadingCompletedPassage(data);
      this.setState({
        isSubmit: true
      })
    }
  }

  componentWillReceiveProps(nextProps, nextContext) {
    const {answerChoose, indexSubAnswer, indexQuestion, totalSubCorrect} = this.state;
    const {question} = this.props;
    let correctAnswer = {};
    let correct = 0;
    const {resultQuestionReduce} = nextProps;

    Object.keys(resultQuestionReduce).map((ans, index) => {
      correctAnswer[index] = resultQuestionReduce[index] ? resultQuestionReduce[index].answer.toLowerCase().split("|", 1) : '';
      if (answerChoose[index] === correctAnswer[index][0]) {
        if (question.listAnswerToChoose[indexQuestion[index]]) {
          question.listAnswerToChoose[indexQuestion[index]].correctIndex = indexSubAnswer[index];
          question.listAnswerToChoose[indexQuestion[index]].incorrectIndex = null;
          correct++;
          this.setState({
            totalSubCorrect: totalSubCorrect + question.listAnswerToChoose[indexQuestion[index]].score
          })
        }
      } else {
        //vị trí câu chọn sai
        if (question.listAnswerToChoose[indexQuestion[index]] !== undefined) {
          question.listAnswerToChoose[indexQuestion[index]].incorrectIndex = indexSubAnswer[index];

          // //duyệt mảng subanswer để tìm ra vị trí câu đúng
          this.cutAnswerToChoose(question.listAnswerToChoose[indexQuestion[index]].answersToChoose).map((a, i) => {
              if (a === correctAnswer[index][0]) {
                question.listAnswerToChoose[indexQuestion[index]].correctIndex = i;
              }
            }
          )
        }
      }

    })
    this.setState({
      correctQues: this.state.correctQues + correct,
      correct: correct
    })

  }

  loadingComponent = (
    <div style={{
      position: 'absolute',
      zIndex: 110,
      top: 0,
      left: 0,
      width: '100%',
      height: '100%',
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      background: 'rgba(255,255,255,0.8)'
    }}>
      <Spinner color="primary" className="reload-spinner"/>
    </div>
  );

  submitForm =(e)=>{
    e.preventDefault();
  }

  next = (number) => {
    const {readingActions} = this.props;
    const {saveListCorrect, updateIsSubmit, cleanResultQuestionOfReadingCompletedPassage} = readingActions;
    saveListCorrect(this.state.correct, this.props.question.listAnswerToChoose.length)
    updateIsSubmit();
    cleanResultQuestionOfReadingCompletedPassage();
    if (this.props.index + 1 === this.props.listQuestionReduce.length || this.state.checkReview) {
      history.push("/pages/aggregatedResults");
    } else {
      this.props.next(number);
    }
  }

  sumNumberCorrect = () => {
    let sumCorrect = 0;
    for (let i = 0; i < this.props.lstCorrect.length; i++) {
      sumCorrect += this.props.lstCorrect[i].numberCorrect;
    }
    return sumCorrect;
  }

  render() {
    const {resultQuestionReduce, listQuestionReduce, stt, listQuestionAndAnswerTemp} = this.props;
    let listQuestion = [];
    let total = 0;
    let totalSub = 0;
    let questionNumber = 0;
    let totalScore = 0;
    if (undefined !== listQuestionReduce) {
      total = listQuestionReduce.length;
      listQuestion = listQuestionReduce[stt];
      for (let i = 0; i < listQuestionReduce.length; i++) {
        if (listQuestionReduce[i].listAnswerToChoose) {
          totalSub = totalSub + listQuestionReduce[i].listAnswerToChoose.length;
          for (let j = 0; j < listQuestionReduce[i].listAnswerToChoose.length; j++) {
            totalScore = totalScore + listQuestionReduce[i].listAnswerToChoose[j].score;
          }
          if (i <= stt && stt <= listQuestionReduce.length - 1) {
            questionNumber = questionNumber + listQuestionReduce[stt].listAnswerToChoose.length;
          }
        }
      }
    }
    const {readingActions, classes} = this.props;
    const {saveScoreCorrect, updateCheckReview} = readingActions;
    saveScoreCorrect(totalScore, this.state.correct, this.state.totalSubCorrect, this.state.stt);

    return (
      <>
        <h2 className={classes.topicName}><b>Part VI: <FormattedMessage id="practices.reading.completedPassage.namePart" tagName="data" /> </b></h2>
        <Container style={{marginTop: '5%'}}>
          <Badge style={{
            backgroundColor: "rgba(32 171 231)", width: "100%", height: "60%", marginLeft: "0%",
            padding: "10px 0 0 0"
          }} className="badge-xl block">
            <Row>
              <Col className="text-left" xs={6} style={{marginLeft: '0px'}}>
                <div style={{position: 'relative'}}>
                  <div className={classes.titleModal}>
                    <h3 style={{
                      marginTop: '2%',
                      width: '5%',
                      color: 'white',
                      fontWeight: '600'
                    }}><FormattedMessage id="subject" tagName="data" /> {this.props.stt + 1}/{total} - <FormattedMessage id="question" tagName="data" /> {questionNumber - (listQuestion.listAnswerToChoose ? listQuestion.listAnswerToChoose.length : 0) + 1} <FormattedMessage id="to" tagName="data" /> {questionNumber}</h3>
                  </div>
                </div>
              </Col>
              <Col className="text-right" xs={6} style={{fontSize: "75%", textAlign: "center"}}>
                <div style={{position: 'relative'}}>
                  <div className={classes.titleModal}>
                    <h4 style={{
                      marginTop: '2%',
                      color: 'white'
                    }}><i> <FormattedMessage id="numberCorrect" tagName="data" />: { this.props.numberCorrectAnswer  }/{totalSub}</i>
                    </h4>
                  </div>
                </div>
              </Col>
            </Row>
          </Badge>
          <Col sm="12" className={classes.bodySnp}>
            {this.props.isSelected ? this.loadingComponent : null}
            <div style={{position: 'relative'}}>
              <Row className={classes.bodyModal} style={{margin: '0 0% 0 0%', padding: '0 0%', width: "100%"}}>

                <Col xs={6} className="mt-2">
                  <Col xs="12" md="12" sm="12" lg="12" className={classes.questionMargin}>
                    <fieldset className={classes.fieldset}>
                      <h3 style={{textAlign: 'center', color: 'green'}}><i></i></h3>
                      <p style={{lineHeight: '2rem'}}>{listQuestion.pathFile1}</p>
                    </fieldset>
                  </Col>
                </Col>
                <Col xs={5} className="mt-2">
                  {
                    this.props.question.listAnswerToChoose ?
                      this.props.question.listAnswerToChoose.map((question, index) => {
                        return (
                          <div>
                            <div style={{height: '25%'}}>
                              <h4>{(index + 1) + "." + question.name}</h4>
                              {
                                this.props.isSubmit || this.state.checkReview ?
                                  <Col xs="12" md="12" sm="12" lg="12" className={classes.answerToChooseMargin}>
                                    <Form style={{marginBottom: '5%'}} onSubmit={(e) => this.submitForm(e)}>
                                      {
                                        this.cutAnswerToChoose(listQuestionAndAnswerTemp[stt] && listQuestionAndAnswerTemp[stt][index] ? listQuestionAndAnswerTemp[stt][index].answersToChoose : '') ?
                                          this.cutAnswerToChoose(listQuestionAndAnswerTemp[stt] && listQuestionAndAnswerTemp[stt][index] ? listQuestionAndAnswerTemp[stt][index].answersToChoose : '').map((subAnswer, i) =>
                                            <Col xs="12" md="12" sm="12" lg="12"
                                                 style={{display: 'flex', padding: '0%'}}>
                                              <Col xs="1" md="1" sm="1" lg="1" style={{padding: '0%'}}>
                                                {listQuestionAndAnswerTemp[stt] && listQuestionAndAnswerTemp[stt][index] && listQuestionAndAnswerTemp[stt][index].indexCorrect !== null &&
                                                  i === listQuestionAndAnswerTemp[stt][index].indexCorrect ?
                                                    <Icon.Check size={25} style={{color: 'green'}}/> : null
                                                }
                                                {listQuestionAndAnswerTemp[stt] && listQuestionAndAnswerTemp[stt][index] && listQuestionAndAnswerTemp[stt][index].indexInCorrect !== null &&
                                                  i === listQuestionAndAnswerTemp[stt][index].indexInCorrect ?
                                                    <Icon.X size={25} style={{color: '#b21717'}}/> : null
                                                }
                                              </Col>
                                              <Col xs="11" md="11" sm="11" lg="11"
                                                   style={{padding: '0%', display: 'flex'}}>
                                                <FormGroup check style={{marginLeft: '2%', marginBottom: '3%'}}>
                                                  <Label check onChange={this.onChangeValue}
                                                         style={{color: 'black', fontSize: '1rem'}}
                                                  >
                                                    <Input type="radio" name={"radio" + index}
                                                           checked={listQuestionAndAnswerTemp[stt] && listQuestionAndAnswerTemp[stt][index] && listQuestionAndAnswerTemp[stt][index].indexCorrect !== null &&
                                                           listQuestionAndAnswerTemp[stt][index].indexInCorrect === null &&  i === listQuestionAndAnswerTemp[stt][index].indexCorrect ? true :
                                                             listQuestionAndAnswerTemp[stt] && listQuestionAndAnswerTemp[stt][index] && listQuestionAndAnswerTemp[stt][index].indexCorrect !== null &&
                                                             listQuestionAndAnswerTemp[stt][index].indexInCorrect !== null && i === listQuestionAndAnswerTemp[stt][index].indexInCorrect ? true : false}
                                                    />{' '}
                                                    ({i === 0 ? "A" : (i === 1 ? "B" : (i === 2 ? "C" : "D"))}) {this.cutSubAnswer(subAnswer)[1]}

                                                    {/*phần dịch câu đáp án*/}
                                                    <button style={{border: 'none', backgroundColor: 'white', padding: '0', marginLeft: '5px'}}
                                                            id={"a" + (listQuestionAndAnswerTemp[stt] && listQuestionAndAnswerTemp[stt][index] ? listQuestionAndAnswerTemp[stt][index].id : "") + i} className="mr-1 mb-1">
                                                      <Icon.AlertCircle  size={20} />
                                                    </button>
                                                    <UncontrolledPopover trigger="focus" placement="bottom" target={"a" +(listQuestionAndAnswerTemp[stt] && listQuestionAndAnswerTemp[stt][index] ? listQuestionAndAnswerTemp[stt][index].id : "") + i}
                                                    >
                                                      <PopoverBody style={{borderColor: 'green !important',
                                                        color: 'white', backgroundColor: 'green', borderRadius: '5px'}}>
                                                        <FormattedMessage id={"translate"}/>: {i===0 ? (listQuestionAndAnswerTemp[stt] && listQuestionAndAnswerTemp[stt][index] ? listQuestionAndAnswerTemp[stt][index].translatingQuesA : "") :
                                                        (i===1 ? (listQuestionAndAnswerTemp[stt] && listQuestionAndAnswerTemp[stt][index] ? listQuestionAndAnswerTemp[stt][index].translatingQuesB : "") :
                                                          (i===2 ? (listQuestionAndAnswerTemp[stt] && listQuestionAndAnswerTemp[stt][index] ? listQuestionAndAnswerTemp[stt][index].translatingQuesC : "") :
                                                            (listQuestionAndAnswerTemp[stt] && listQuestionAndAnswerTemp[stt][index] ? listQuestionAndAnswerTemp[stt][index].translatingQuesD : "")))}
                                                      </PopoverBody>
                                                    </UncontrolledPopover>
                                                    {/*phần dịch câu đáp án*/}
                                                  </Label>

                                                </FormGroup>


                                              </Col>
                                            </Col>
                                          ) : null}
                                    </Form>
                                    <Col xs="12" md="12" sm="12" lg="12"
                                         style={{marginBottom: '10%', paddingLeft: '0px', paddingRight: '0px'}}>
                                      <fieldset className={classes.fieldsetDescription} style={{
                                        border: '2px solid green',
                                        padding: '0 2% 2% 2%'
                                      }}>
                                        <legend className={classes.legendDescription}><FormattedMessage id="description" tagName="data" /></legend>
                                        <i>{listQuestionAndAnswerTemp[stt] && listQuestionAndAnswerTemp[stt][index] ? listQuestionAndAnswerTemp[stt][index].description : ""}</i>
                                      </fieldset>
                                    </Col>
                                  </Col>

                                  :
                                  this.cutAnswerToChoose(this.props.question.listAnswerToChoose[index].answersToChoose) ?
                                    this.cutAnswerToChoose(this.props.question.listAnswerToChoose[index].answersToChoose).map((item, indexAnswer) => {
                                      return (
                                        <FormGroup check style={{marginLeft: '15%', marginBottom: '3%'}}>
                                          <Label onChange={(e) => this.onChangeValue(e, question)} check
                                                 className={classes.labelRadioButton} style={{cursor: 'pointer'}}>
                                            <Input type="radio" value={item + "|" + index + "." + indexAnswer}
                                                   name={"subAnswer" + index}
                                            />{'   '}
                                            ({indexAnswer === 0 ? "A" : (indexAnswer === 1 ? "B" : (indexAnswer === 2 ? "C" : "D"))}) {this.cutSubAnswer(item)[1]}
                                          </Label>
                                        </FormGroup>
                                      )
                                    }) : null
                              }
                            </div>

                          </div>
                        )
                      }) : null
                  }
                </Col>
                <Col xs={1} style={{paddingLeft: '0px', paddingRight: '0px'}}>
                  {this.state.isSubmit || this.state.checkReview
                    ?
                    <Col xs="1" md="1" sm="1" lg="1" className={classes.nextQuestion}>
                      <Icon.ChevronsRight
                        size={50}
                        className="mr-4 fonticon-wrap"
                        onClick={() => {
                          this.next(this.state.correctQues)
                        }}
                      />
                    </Col>
                    : null
                  }
                </Col>
                {

                  !this.props.isSubmit ? !this.state.checkReview ?
                    <Col xs="12" md="12" sm="12" lg="12" className={classes.divSubmit}>
                      <button className="btn btn-info"
                              onClick={this.showAnswer}
                              style={{backgroundColor: 'background-color: #02a1ed !important', fontWeight: '700'}}
                      ><FormattedMessage id="submit" tagName="data" />
                      </button>
                    </Col>
                    : null : null
                }

              </Row>
            </div>
          </Col>
        </Container>
      </>
    );
  }
}

const mapStateToProps = state => {
  return {
    listQuestionReduce: state.readingReducer.listQuestion,
    resultQuestionReduce: state.readingReducer.resultQuestion,
    isSelected: state.readingReducer.isSelect,
    numberCorrect: state.readingReducer.numberCorrectAnswer,
    checkedReview: state.readingReducer.checkReview,
    lstQuestionCheckedCorrect: state.readingReducer.lstQuestionCheckCorrect,
    lstCorrect: state.readingReducer.lstCorrect,
    listUserChoose: state.readingReducer.listUserChoose,
    isSubmit: state.readingReducer.isSubmit,
    listQuestionAndAnswerTemp: state.readingReducer.listQuestionAndAnswerTemp,
    numberCorrectAnswer: state.readingReducer.numberCorrectAnswer
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    readingActions: bindActionCreators(readingActions, dispatch),
    listeningActions: bindActionCreators(practiceListenMultiActions, dispatch)
  }
}
const withConnect = connect(mapStateToProps, mapDispatchToProps)
export default compose(withStyles(styles), withConnect)(Question);
