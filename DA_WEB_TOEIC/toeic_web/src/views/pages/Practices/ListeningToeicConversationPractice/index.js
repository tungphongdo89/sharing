import React, {PureComponent} from 'react';
import {connect} from "react-redux";
import * as Icon from "react-feather";
import {bindActionCreators, compose} from "redux";
import {styles, ReactPlayerReading} from "./style";
import {withStyles} from "@material-ui/core";

import {
  Container,
  Row,
  Col,
  Input,
  Form,
  Spinner, FormGroup, Label, Badge, UncontrolledPopover, PopoverBody, Button
} from "reactstrap"

// import style from './translation.module.css'
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import practiceListenMultiActions from "../../../../redux/actions/practice/listening";
import historyPracticesAction from "../../../../redux/actions/history-practices/";
import {FormattedMessage} from "react-intl";
import historyPracticseReducer from "../../../../redux/reducers/historyPractices/historyPracticesReducer";
import {history} from "../../../../history";
import {showMessage} from "../../../../commons/utils/convertDataForToast";
import {ArrowLeft} from "react-feather";
import historyTestAction from "../../../../redux/actions/history-test";


class ListeningToeicConversation extends PureComponent {


  constructor(props){
    super(props);
    this.state = {
      playing: false,
      timeAgain: 0,
      data: false,
      index: this.props.index,
      load: false,
      checked0: false,
      checked1: false,
      checked2: false,
      position0: "",
      position1: "",
      position2: "",
      position3: "",
      position4: "",
      answer: {},
      correctQues: 0,
      totalScoreAchived: 0,
      checkReview: false,
      reload: false,
      play0: null,
      play2: null,
      play3: null,
      play4: null,
      play1: null,
      play: false,

      playPathFile: true,
      playPathFileSubmit: false,

      playQuestion1: false,
      playQuesA1: false,
      playQuesB1: false,
      playQuesC1: false,
      playQuesD1: false,

      playQuestion2: false,
      playQuesA2: false,
      playQuesB2: false,
      playQuesC2: false,
      playQuesD2: false,

      playQuestion3: false,
      playQuesA3: false,
      playQuesB3: false,
      playQuesC3: false,
      playQuesD3: false,

      checkPlayingAgain: -5,
      tempCheckPlayingAgain: -5,

      currentPlaying: false
    }
  }



  cutAnswerToChoose(answerToChoose){
    let char = 0;
    let answerArray = [];
    let subAnswer = "";

    let count=0,index=0; index = 0;
    for( count=-1,index=-2; index != -1; count++,index=answerToChoose.indexOf("|",index+1) );

    for(let i=1;i<=count;i++){
      char = answerToChoose.indexOf("|");
      subAnswer = answerToChoose.substring(0, char);
      answerArray.push(subAnswer);
      answerToChoose = answerToChoose.substring(char + 1);
    }
    return answerArray;
  }

  cutStartTime(startTime){
    let st = "";
    this.cutAnswerToChoose(startTime).map((a) => {
      if(a.trim() !== ""){
        st = a.trim();
      }
    })
    console.log(st+"cc");
    return st;
  }

  cutSubAnswer(subAnswer){
    let index = subAnswer.indexOf(".");
    let title = subAnswer.substring(0, index);
    let content = subAnswer.substring(index+1);

    let array = [];
    array.push(title);
    array.push(content);
    return array;
  }

  cutInputValue(value){
    let index = value.indexOf("|");
    let title = value.substring(0, index);
    let content = value.substring(index+1);

    let array = [];
    array.push(title);
    array.push(content);
    return array;
  }

  nextQuestion =()=>{
    debugger
    this.setState({
      playPathFile: true,
      playPathFileSubmit: false,

      playQuestion1: false,
      playQuesA1: false,
      playQuesB1: false,
      playQuesC1: false,
      playQuesD1: false,

      playQuestion2: false,
      playQuesA2: false,
      playQuesB2: false,
      playQuesC2: false,
      playQuesD2: false,

      playQuestion3: false,
      playQuesA3: false,
      playQuesB3: false,
      playQuesC3: false,
      playQuesD3: false,

      checkPlayingAgain: -5

    })
    let index = this.state.index;
    let checkReview = this.state.checkReview;
    if(index<=2 && checkReview===false){
      this.setState({
        load: true,
        index: index + 1,
        data: false,
        checked0: false,
        checked1: false,
        checked2: false,
        play: false,
        play0: null,
        play1: null,
        play2: null,
        play3: null,
        play4: null,
        playing: false
      })
      setTimeout(()=>{
        this.setState({
          load: false
        })
      }, 500)
    }
    else{
      this.setState({
        load: true,
        index: index + 3,
        play: false,
        play0: null,
        play1: null,
        play2: null,
        play3: null,
        play4: null,
        playing: false
      })
      setTimeout(()=>{
        this.setState({
          load: false
        })
      }, 500)
    }

  }



  onChangeValue =(e)=> {
    debugger
    console.log("name: ", e.target.name)

    let answerChoose = this.cutInputValue(e.target.value)[0];
    let answerContent = this.cutInputValue(e.target.value)[1];
    let indexQuestion = this.cutSubAnswer(answerContent)[0];
    let indexSubAnswer = this.cutSubAnswer(answerContent)[1];

    // let {answer, position0, position1, position2, position3, position4} = this.state;
    if(indexQuestion === "0"){
      console.log("vào 0");
      this.setState({
        position0: answerContent,
        checked0: true
      })
    }
    if(indexQuestion === "1"){
      console.log("vào 1");
      this.setState({
        position0: answerContent,
        checked1: true
      })
    }
    if(indexQuestion === "2"){
      console.log("vào 2");
      this.setState({
        position0: answerContent,
        checked2: true
      })
    }


    let {answer} = this.state;


    console.log("answerChoose: ", answerChoose);
    console.log("answerContent: ", answerContent);
    console.log("indexQuestion: ", indexQuestion);
    console.log("indexSubAnswer: ", indexSubAnswer);

    let answerX = "";
    if(indexSubAnswer === "0"){
      answerX = "A";
    }
    if(indexSubAnswer === "1"){
      answerX = "B";
    }
    if(indexSubAnswer === "2"){
      answerX = "C";
    }
    if(indexSubAnswer === "3"){
      answerX = "D";
    }

    if(indexQuestion==="0"){
      // answer.answer1 = answerChoose;
      answer.answer1 = answerX;
      answer.indexQuestion1 = parseInt(indexQuestion);
      answer.indexSubAnswer1 = parseInt(indexSubAnswer);
      this.setState({
        answer: answer
      })
    }
    if(indexQuestion==="1"){
      answer.answer2 = answerX;
      answer.indexQuestion2 = parseInt(indexQuestion);
      answer.indexSubAnswer2 = parseInt(indexSubAnswer);
      this.setState({
        answer: answer
      })
    }
    if(indexQuestion==="2"){
      answer.answer3 = answerX;
      answer.indexQuestion3 = parseInt(indexQuestion);
      answer.indexSubAnswer3 = parseInt(indexSubAnswer);
      this.setState({
        answer: answer
      })
    }
    if(indexQuestion==="3"){
      answer.answer4 = answerX;
      answer.indexQuestion4 = parseInt(indexQuestion);
      answer.indexSubAnswer4 = parseInt(indexSubAnswer);
      this.setState({
        answer: answer
      })
    }
    if(indexQuestion==="4"){
      answer.answer5 = answerX;
      answer.indexQuestion5 = parseInt(indexQuestion);
      answer.indexSubAnswer5 = parseInt(indexSubAnswer);
      this.setState({
        answer: answer
      })
    }
    console.log("final answer: ", this.state.answer);
  }

  showAnswer =()=>{
    debugger
    this.setState({

      playQuestion1: false,
      playQuesA1: false,
      playQuesB1: false,
      playQuesC1: false,
      playQuesD1: false,

      playQuestion2: false,
      playQuesA2: false,
      playQuesB2: false,
      playQuesC2: false,
      playQuesD2: false,

      playQuestion3: false,
      playQuesA3: false,
      playQuesB3: false,
      playQuesC3: false,
      playQuesD3: false,
    })
    let {answer, index, correctQues, checked0, checked1, checked2, totalScoreAchived} = this.state;

    if(checked0 === true && checked1 === true && checked2 === true){
      this.setState({
        playPathFile: false,
        playPathFileSubmit: true,
        data: true,
        load: true
      })
      setTimeout(()=>{
        this.setState({
          load: false
        })
      }, 500)

      let listCategories = this.props.listCategories;
      let listQuestion = listCategories[index];

      let correctAnswer1 = listQuestion.listQuestion[0].answer;
      let correctAnswer2 = listQuestion.listQuestion[1].answer;
      let correctAnswer3 = "";
      if(undefined !== listQuestion.listQuestion[2]){
        correctAnswer3 = listQuestion.listQuestion[2].answer;
      }
      let correctAnswer4 = "";
      if(undefined !== listQuestion.listQuestion[3]){
        correctAnswer4 = listQuestion.listQuestion[3].answer;
      }
      let correctAnswer5 = "";
      if(undefined !== listQuestion.listQuestion[4]){
        correctAnswer5 = listQuestion.listQuestion[4].answer;
      }

      let correctQuesNumber = 0;
      let scoreAchived = 0;
      // if(answer.answer1 === this.cutInputValue(correctAnswer1)[0]){
      if(answer.answer1 === correctAnswer1){
        listCategories[index].listQuestion[answer.indexQuestion1].correctIndex = answer.indexSubAnswer1;
        console.log("listCategoriesChecked 1", listCategories);
        correctQuesNumber = correctQuesNumber + 1;
        scoreAchived = scoreAchived + listCategories[index].listQuestion[answer.indexQuestion1].score;
      }
      else {
        //vị trí câu chọn sai
        if(undefined !== listCategories[index].listQuestion[answer.indexQuestion1]){
          listCategories[index].listQuestion[answer.indexQuestion1].incorrectIndex = answer.indexSubAnswer1;
          //duyệt mảng subanswer để tìm ra vị trí câu đúng
          this.cutAnswerToChoose(listCategories[index].listQuestion[answer.indexQuestion1].answersToChoose).map((a, i)=>
            {
              let ans = "";
              if(i===0){
                ans = "A"
              }
              if(i===1){
                ans = "B"
              }
              if(i===2){
                ans = "C"
              }
              if(i===3){
                ans = "D"
              }
              if (ans === correctAnswer1) {
                listCategories[index].listQuestion[answer.indexQuestion1].correctIndex = i;
              }
            }
          )
        }
      }

      if(answer.answer2 === correctAnswer2){
        listCategories[index].listQuestion[answer.indexQuestion2].correctIndex = answer.indexSubAnswer2;
        console.log("listCategoriesChecked 2", listCategories);
        correctQuesNumber = correctQuesNumber + 1;
        scoreAchived = scoreAchived + listCategories[index].listQuestion[answer.indexQuestion2].score;
      }
      else {
        if(undefined !== listCategories[index].listQuestion[answer.indexQuestion2]){
          listCategories[index].listQuestion[answer.indexQuestion2].incorrectIndex = answer.indexSubAnswer2;
          this.cutAnswerToChoose(listCategories[index].listQuestion[answer.indexQuestion2].answersToChoose).map((a, i)=>
            {
              let ans = "";
              if(i===0){
                ans = "A"
              }
              if(i===1){
                ans = "B"
              }
              if(i===2){
                ans = "C"
              }
              if(i===3){
                ans = "D"
              }
              if (ans === correctAnswer2) {
                listCategories[index].listQuestion[answer.indexQuestion2].correctIndex = i;
              }
            }
          )
        }
      }


      if(answer.answer3 === correctAnswer3){
        listCategories[index].listQuestion[answer.indexQuestion3].correctIndex = answer.indexSubAnswer3;
        console.log("listCategoriesChecked 3", listCategories);
        correctQuesNumber = correctQuesNumber + 1;
        scoreAchived = scoreAchived + listCategories[index].listQuestion[answer.indexQuestion3].score;
      }
      else {
        if(undefined !== listCategories[index].listQuestion[answer.indexQuestion3]){
          listCategories[index].listQuestion[answer.indexQuestion3].incorrectIndex = answer.indexSubAnswer3;
          this.cutAnswerToChoose(listCategories[index].listQuestion[answer.indexQuestion3].answersToChoose).map((a, i)=>
            {
              let ans = "";
              if(i===0){
                ans = "A"
              }
              if(i===1){
                ans = "B"
              }
              if(i===2){
                ans = "C"
              }
              if(i===3){
                ans = "D"
              }
              if (ans === correctAnswer3) {
                listCategories[index].listQuestion[answer.indexQuestion3].correctIndex = i;
              }
            }
          )
        }
      }


      if(answer.answer4 === correctAnswer4){
        listCategories[index].listQuestion[answer.indexQuestion4].correctIndex = answer.indexSubAnswer4;
        console.log("listCategoriesChecked 4", listCategories);
        correctQuesNumber = correctQuesNumber + 1;
        scoreAchived = scoreAchived + listCategories[index].listQuestion[answer.indexQuestion4].score;
      }
      else {
        if(undefined !== listCategories[index].listQuestion[answer.indexQuestion4]){
          listCategories[index].listQuestion[answer.indexQuestion4].incorrectIndex = answer.indexSubAnswer4;
          this.cutAnswerToChoose(listCategories[index].listQuestion[answer.indexQuestion4].answersToChoose).map((a, i)=>
            {
              let ans = "";
              if(i===0){
                ans = "A"
              }
              if(i===1){
                ans = "B"
              }
              if(i===2){
                ans = "C"
              }
              if(i===3){
                ans = "D"
              }
              if (ans === correctAnswer4) {
                listCategories[index].listQuestion[answer.indexQuestion4].correctIndex = i;
              }
            }
          )
        }
      }


      if(answer.answer5 === correctAnswer5){
        listCategories[index].listQuestion[answer.indexQuestion5].correctIndex = answer.indexSubAnswer5;
        console.log("listCategoriesChecked 5", listCategories);
        correctQuesNumber = correctQuesNumber + 1;
        scoreAchived = scoreAchived + listCategories[index].listQuestion[answer.indexQuestion5].score;
      }
      else {
        if(undefined !== listCategories[index].listQuestion[answer.indexQuestion5]){
          listCategories[index].listQuestion[answer.indexQuestion5].incorrectIndex = answer.indexSubAnswer5;
          this.cutAnswerToChoose(listCategories[index].listQuestion[answer.indexQuestion5].answersToChoose).map((a, i)=>
            {
              let ans = "";
              if(i===0){
                ans = "A"
              }
              if(i===1){
                ans = "B"
              }
              if(i===2){
                ans = "C"
              }
              if(i===3){
                ans = "D"
              }
              if (ans === correctAnswer5) {
                listCategories[index].listQuestion[answer.indexQuestion5].correctIndex = i;
              }
            }
          )
        }
      }
      //list tổng hợp kết quả
      listCategories[index].correctQuesNumber = correctQuesNumber;

      this.setState({
        correctQues: correctQues + correctQuesNumber,
        totalScoreAchived: totalScoreAchived + scoreAchived
      })
    }
    else {
      console.log("chưa chọn");
      // toast.error("Vui lòng chọn đầy đủ đáp án");
      toast.error(showMessage("please.answer.all.of.the.questions"))
      return;
    }
  }

  lowerCaseAll =(string)=>{
    return string.toLowerCase();
  }

  upperCaseFirstLetter =(string)=>{
    string = this.lowerCaseAll(string);
    return string.trim().charAt(0).toUpperCase() + string.trim().slice(1);
  }

  submitForm =(e)=>{
    e.preventDefault();
  }

  review =(e, index)=>{

    this.setState({
      playPathFile: true,

      playQuestion1: false,
      playQuesA1: false,
      playQuesB1: false,
      playQuesC1: false,
      playQuesD1: false,

      playQuestion2: false,
      playQuesA2: false,
      playQuesB2: false,
      playQuesC2: false,
      playQuesD2: false,

      playQuestion3: false,
      playQuesA3: false,
      playQuesB3: false,
      playQuesC3: false,
      playQuesD3: false,
    })

    this.setState({
      load: true,
      index: index,
      data: true,
      checkReview: true
    })
    setTimeout(()=>{
      this.setState({
        load: false
      })
    }, 500)
  }

  doSameExercise =()=>{
    this.setState({
      correctQues: 0
    })
    const {multiListenQuestionAction, historyPracticesAction} = this.props;
    const {cleanHiddenBtnBeforeGetListening}= historyPracticesAction;
    cleanHiddenBtnBeforeGetListening();

    const {fetchListeningToeicConverstation} = multiListenQuestionAction;

    let data = {};
    let topicId = this.props.listCategories[0].topicId;
    let levelCode = this.props.listCategories[0].levelCode;
    let currentLevelCode = this.props.listCategories[0].currentLevelCode;
    console.log("currentLevelCode: ", currentLevelCode);
    data.topicId = topicId;
    data.part = "PART3";
    data.levelCode = currentLevelCode;
    fetchListeningToeicConverstation(data);

  }

  saveResult =()=> {
    debugger
    const {listCategories, historyPracticesAction} = this.props;

    const {cleanHiddenBtnBeforeGetListening}= historyPracticesAction;
    cleanHiddenBtnBeforeGetListening();

    let numberCorrect = 0;
    for (let i = 0; i < listCategories.length; i++) {
      for (let j = 0; j < listCategories[i].listQuestion.length; j++) {
        if (undefined === listCategories[i].listQuestion[j].incorrectIndex) {
          numberCorrect += 1;
        }
      }
    }
    let historyPractices = {};
    historyPractices.typeCode = 2;
    historyPractices.part = "PART3";
    historyPractices.levelCode = listCategories[0].currentLevelCode;
    historyPractices.topicId = listCategories[0].topicId;
    historyPractices.numberCorrect = "";
    historyPractices.userId = this.props.studentAccountReducer.userInfoLogin.userId;;

    let listDetailHistoryListening = [];

    let totalNumerCorrect = 0;
    let totalQuestion = 0;

    for (let i = 0; i < listCategories.length; i++) {
      for (let j = 0; j < listCategories[i].listQuestion.length; j++) {
        let detailHistoryListening = {};
        // detailHistoryListening.parentId = listCategories[i].categoryId;
        detailHistoryListening.correctIndex = listCategories[i].listQuestion[j].correctIndex;
        if (listCategories[i].listQuestion[j].incorrectIndex !== undefined) {
          detailHistoryListening.incorrectIndex = listCategories[i].listQuestion[j].incorrectIndex;
        } else {
          detailHistoryListening.incorrectIndex = null;
        }

        detailHistoryListening.questionId = listCategories[i].listQuestion[j].id;
        detailHistoryListening.categoryId = listCategories[i].categoryId;

        listDetailHistoryListening.push(detailHistoryListening);
      }
    }

    for (let i = 0; i < listCategories.length; i++) {
      for (let j = 0; j < listCategories[i].listQuestion.length; j++) {
        if(listCategories[i].listQuestion[j].incorrectIndex === null){
          totalNumerCorrect += 1;
          totalQuestion += 1;
        }
        else {
          totalQuestion += 1;
        }
      }
    }

    historyPractices.numberCorrect = totalNumerCorrect + "/" + totalQuestion;
    historyPractices.listDetailHistoryListening = listDetailHistoryListening;
    // console.log("history practices: ", historyPractices);


    const {saveResultHistoryListening} = historyPracticesAction;
    saveResultHistoryListening(historyPractices);
  }


  tempCheckPlayingAgain2 = -5;

  onStarting =()=> {
    // debugger
    // this.setState({
    //   play: true,
    //   checkPlayingAgain: this.state.checkPlayingAgain,
    //   // tempCheckPlayingAgain: -5
    // })
  }

  onPlaying =()=>{
    debugger
    this.setState({
      play: true,
      // checkPlayingAgain: this.tempCheckPlayingAgain2,
      // tempCheckPlayingAgain: -5
    })
    // this.tempCheckPlayingAgain2 = -5;
  }

  onPausing =()=>{
    debugger
    // this.tempCheckPlayingAgain2 =this.state.checkPlayingAgain;
    this.setState({
      play: false,
      // tempCheckPlayingAgain: this.state.checkPlayingAgain,
      // checkPlayingAgain: -5
    })
  }

  ref = player => {
    this.player = player
  }

  playingListening =(question, indexQuestion, indexSubAnswer)=> {
    debugger
    if(indexQuestion === -3){
      this.setState({
        playPathFile: false,
        playPathFileSubmit: false,

        playQuestion1: true,
        playQuesA1: false,
        playQuesB1: false,
        playQuesC1: false,
        playQuesD1: false,

        playQuestion2: false,
        playQuesA2: false,
        playQuesB2: false,
        playQuesC2: false,
        playQuesD2: false,

        playQuestion3: false,
        playQuesA3: false,
        playQuesB3: false,
        playQuesC3: false,
        playQuesD3: false,

        checkPlayingAgain: indexQuestion
      })
    }

    if(indexQuestion === -2){
      this.setState({
        playPathFile: false,
        playPathFileSubmit: false,

        playQuestion1: false,
        playQuesA1: false,
        playQuesB1: false,
        playQuesC1: false,
        playQuesD1: false,

        playQuestion2: true,
        playQuesA2: false,
        playQuesB2: false,
        playQuesC2: false,
        playQuesD2: false,

        playQuestion3: false,
        playQuesA3: false,
        playQuesB3: false,
        playQuesC3: false,
        playQuesD3: false,

        checkPlayingAgain: indexQuestion
      })
    }

    if(indexQuestion === -1){
      this.setState({
        playPathFile: false,
        playPathFileSubmit: false,

        playQuestion1: false,
        playQuesA1: false,
        playQuesB1: false,
        playQuesC1: false,
        playQuesD1: false,

        playQuestion2: false,
        playQuesA2: false,
        playQuesB2: false,
        playQuesC2: false,
        playQuesD2: false,

        playQuestion3: true,
        playQuesA3: false,
        playQuesB3: false,
        playQuesC3: false,
        playQuesD3: false,

        checkPlayingAgain: indexQuestion
      })
    }

    if(indexQuestion === 0){
        if(indexSubAnswer === 0){
          this.setState({
            playPathFile: false,
            playPathFileSubmit: false,

            playQuestion1: false,
            playQuesA1: true,
            playQuesB1: false,
            playQuesC1: false,
            playQuesD1: false,

            playQuestion2: false,
            playQuesA2: false,
            playQuesB2: false,
            playQuesC2: false,
            playQuesD2: false,

            playQuestion3: false,
            playQuesA3: false,
            playQuesB3: false,
            playQuesC3: false,
            playQuesD3: false,
          })
        }

        if(indexSubAnswer === 1){
          this.setState({
            playPathFile: false,
            playPathFileSubmit: false,

            playQuestion1: false,
            playQuesA1: false,
            playQuesB1: true,
            playQuesC1: false,
            playQuesD1: false,

            playQuestion2: false,
            playQuesA2: false,
            playQuesB2: false,
            playQuesC2: false,
            playQuesD2: false,

            playQuestion3: false,
            playQuesA3: false,
            playQuesB3: false,
            playQuesC3: false,
            playQuesD3: false,
          })
        }

        if(indexSubAnswer === 2){
          this.setState({
            playPathFile: false,
            playPathFileSubmit: false,

            playQuestion1: false,
            playQuesA1: false,
            playQuesB1: false,
            playQuesC1: true,
            playQuesD1: false,

            playQuestion2: false,
            playQuesA2: false,
            playQuesB2: false,
            playQuesC2: false,
            playQuesD2: false,

            playQuestion3: false,
            playQuesA3: false,
            playQuesB3: false,
            playQuesC3: false,
            playQuesD3: false,
          })
        }

        if(indexSubAnswer === 3){
          this.setState({
            playPathFile: false,
            playPathFileSubmit: false,

            playQuestion1: false,
            playQuesA1: false,
            playQuesB1: false,
            playQuesC1: false,
            playQuesD1: true,

            playQuestion2: false,
            playQuesA2: false,
            playQuesB2: false,
            playQuesC2: false,
            playQuesD2: false,

            playQuestion3: false,
            playQuesA3: false,
            playQuesB3: false,
            playQuesC3: false,
            playQuesD3: false,
          })
        }
      }

    if(indexQuestion === 1){
      if(indexSubAnswer === 0){
        this.setState({
          playPathFile: false,
          playPathFileSubmit: false,

          playQuestion1: false,
          playQuesA1: false,
          playQuesB1: false,
          playQuesC1: false,
          playQuesD1: false,

          playQuestion2: false,
          playQuesA2: true,
          playQuesB2: false,
          playQuesC2: false,
          playQuesD2: false,

          playQuestion3: false,
          playQuesA3: false,
          playQuesB3: false,
          playQuesC3: false,
          playQuesD3: false,
        })
      }

      if(indexSubAnswer === 1){
        this.setState({
          playPathFile: false,
          playPathFileSubmit: false,

          playQuestion1: false,
          playQuesA1: false,
          playQuesB1: false,
          playQuesC1: false,
          playQuesD1: false,

          playQuestion2: false,
          playQuesA2: false,
          playQuesB2: true,
          playQuesC2: false,
          playQuesD2: false,

          playQuestion3: false,
          playQuesA3: false,
          playQuesB3: false,
          playQuesC3: false,
          playQuesD3: false,
        })
      }

      if(indexSubAnswer === 2){
        this.setState({
          playPathFile: false,
          playPathFileSubmit: false,

          playQuestion1: false,
          playQuesA1: false,
          playQuesB1: false,
          playQuesC1: false,
          playQuesD1: false,

          playQuestion2: false,
          playQuesA2: false,
          playQuesB2: false,
          playQuesC2: true,
          playQuesD2: false,

          playQuestion3: false,
          playQuesA3: false,
          playQuesB3: false,
          playQuesC3: false,
          playQuesD3: false,
        })
      }

      if(indexSubAnswer === 3){
        this.setState({
          playPathFile: false,
          playPathFileSubmit: false,

          playQuestion1: false,
          playQuesA1: false,
          playQuesB1: false,
          playQuesC1: false,
          playQuesD1: false,

          playQuestion2: false,
          playQuesA2: false,
          playQuesB2: false,
          playQuesC2: false,
          playQuesD2: true,

          playQuestion3: false,
          playQuesA3: false,
          playQuesB3: false,
          playQuesC3: false,
          playQuesD3: false,
        })
      }
    }

    if(indexQuestion === 2){
      if(indexSubAnswer === 0){
        this.setState({
          playPathFile: false,
          playPathFileSubmit: false,

          playQuestion1: false,
          playQuesA1: false,
          playQuesB1: false,
          playQuesC1: false,
          playQuesD1: false,

          playQuestion2: false,
          playQuesA2: false,
          playQuesB2: false,
          playQuesC2: false,
          playQuesD2: false,

          playQuestion3: false,
          playQuesA3: true,
          playQuesB3: false,
          playQuesC3: false,
          playQuesD3: false,
        })
      }

      if(indexSubAnswer === 1){
        this.setState({
          playPathFile: false,
          playPathFileSubmit: false,

          playQuestion1: false,
          playQuesA1: false,
          playQuesB1: false,
          playQuesC1: false,
          playQuesD1: false,

          playQuestion2: false,
          playQuesA2: false,
          playQuesB2: false,
          playQuesC2: false,
          playQuesD2: false,

          playQuestion3: false,
          playQuesA3: false,
          playQuesB3: true,
          playQuesC3: false,
          playQuesD3: false,
        })
      }

      if(indexSubAnswer === 2){
        this.setState({
          playPathFile: false,
          playPathFileSubmit: false,

          playQuestion1: false,
          playQuesA1: false,
          playQuesB1: false,
          playQuesC1: false,
          playQuesD1: false,

          playQuestion2: false,
          playQuesA2: false,
          playQuesB2: false,
          playQuesC2: false,
          playQuesD2: false,

          playQuestion3: false,
          playQuesA3: false,
          playQuesB3: false,
          playQuesC3: true,
          playQuesD3: false,
        })
      }

      if(indexSubAnswer === 3){
        this.setState({
          playPathFile: false,
          playPathFileSubmit: false,

          playQuestion1: false,
          playQuesA1: false,
          playQuesB1: false,
          playQuesC1: false,
          playQuesD1: false,

          playQuestion2: false,
          playQuesA2: false,
          playQuesB2: false,
          playQuesC2: false,
          playQuesD2: false,

          playQuestion3: false,
          playQuesA3: false,
          playQuesB3: false,
          playQuesC3: false,
          playQuesD3: true,
        })
      }
    }

  }

  endingFile =()=> {
    this.setState({
      checkPlayingAgain: -5,

      playPathFile: true,
      playPathFileSubmit: false,

      playQuestion1: false,
      playQuesA1: false,
      playQuesB1: false,
      playQuesC1: false,
      playQuesD1: false,

      playQuestion2: false,
      playQuesA2: false,
      playQuesB2: false,
      playQuesC2: false,
      playQuesD2: false,

      playQuestion3: false,
      playQuesA3: false,
      playQuesB3: false,
      playQuesC3: false,
      playQuesD3: false,
    })
  }

  componentDidMount() {
    // this.setState({
    //   load: true
    // })
    // setTimeout(()=>{
    //   this.setState({
    //     load: false
    //   })
    // }, 1500)
  }


  componentDidUpdate(prevProps){
    debugger
    if(this.props.listCategories !== prevProps.listCategories){
      this.setState({
        index: 0,
        load: true,
        data: false,
        checkReview: false,
        totalScoreAchived: 0
      })
      setTimeout(()=>{
        this.setState({
          load: false
        })
      }, 1500)
    }

    if(this.props.index !== prevProps.index){
      this.setState({
        index: this.props.index
      })
    }

    // if(){
    //
    // }

  }

  backToHistory =()=> {
    let {historyPracticesAction,historyTestAction} = this.props;
    let {changeActiveTab} = historyPracticesAction;
    changeActiveTab("4");
    historyTestAction.updateActiveTabWhenFail("4");
    history.push("/pages/profiles");
  }

  render() {
    debugger
    const {data, index, totalScoreAchived} = this.state;
    const {listCategories, classes} = this.props;
    let listQuestion = [];
    let total = 0;
    let totalSub = 0;
    let totalScore = 0;
    let questionNumber = 0;
    if(undefined !== listCategories){
      total = listCategories.length;
      listQuestion = listCategories[index];
      for(let i=0;i<listCategories.length;i++){
        totalSub = totalSub + listCategories[i].listQuestion.length;
        for(let j=0;j<listCategories[i].listQuestion.length;j++){
          totalScore = totalScore + listCategories[i].listQuestion[j].score;
        }
        if(i <= index && index <= listCategories.length - 1){
          questionNumber = questionNumber + listCategories[index].listQuestion.length;
        }
      }
    }
    console.log("tổng điểm: ", totalScore);
    // let positionCheck = position;
    const loadingComponent = (
      <div style={{
        position: 'absolute',
        zIndex: 110,
        top: 0,
        left: 0,
        width: '100%',
        height: '100%',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        background: 'rgba(255,255,255,0.8)',
      }}>
        <Spinner color="primary" className="reload-spinner"/>
      </div>
    );

    return (
      <>
        <style>{`
          th{
            font-size: 120% !important;
            color: black !important;
            border: 1px solid gray !important;
          }
          td{
            border: 1px solid gray !important;
          }
          #iconNextQuestion:hover{
            color: green !important;
          }
          #btnSubmit{
            background-color: #02a1ed !important;
          }
          #btnSaveResult{
            background-color: #ff9f43 !important;
          }
          #btnSaveResult:focus-within{
            border: 1px solid black;
          }
          #btnSubmit:focus-within{
            border: 1px solid black;
          }
          #btnHighLightLink {
            background-color: white;
            border: none;
            color: #7367f0;
            padding: 0 2px;
          }
          #btnHighLightLink:focus-within {
            border: 1px solid black !important;
          }
          #btnBackHistory{
            margin-left: 2%;
            border-color: 1px solid blue;
            background-color: #7367f0;
            color: white;
            padding: 8.5px 10px;
            border-radius: 5px;
            border: none;
          }
          #btnBackHistory:focus-within {
            border: 1px solid black !important;
          }

          #btnHighlightRepeat:focus-within {
            border: 1px solid black !important;
          }
        `}</style>
        {this.state.index + 1 <= total && undefined !== listCategories  ?
          <>
            <h2 className={classes.topicName}><b>
              Part III:{' '}
              {/*Bài nghe đoạn hội thoại */}
              <FormattedMessage id="conversation.listening.part" tagName="data" />{' '}
            </b></h2>
            <Container style={{marginTop: '5%'}}>
              <Row xs="12" md="12" sm="12" lg="12" className={classes.titleModal2}>
                <Col xs="6" md="6" sm="6" lg="6" className={classes.totalQuesion}>
                  <div className={classes.totalQuesionChild}>
                    {/*Đề {this.state.index + 1}/{total} - Câu {questionNumber - listQuestion.listQuestion.length + 1 } đến {questionNumber}*/}
                    {/*Đề */}
                    <FormattedMessage id="category" tagName="data" />{' '}
                    {this.state.index + 1}/{total} - {' '}
                    {/*Câu*/}
                    <FormattedMessage id="question" tagName="data" />{' '}
                    {questionNumber - listQuestion.listQuestion.length + 1 }{' '}
                    {/*đến */}
                    <FormattedMessage id="to" tagName="data" />{' '}
                    {questionNumber}
                  </div>
                </Col>
              </Row>
              <Col sm="12" className={classes.bodySnp}>
                <div style={{position: 'relative', margin: '0 14px'}}>
                  <Row className={classes.bodyModal} style={{margin: '0 0% 0 0%', padding: '0 0%'}}>
                    <Col xs="12" md="12" sm="12" lg="12">
                      {/*<Question listCategories={this.state.listCategories}/>*/}
                      <>
                        {undefined !== listQuestion ?
                          <>
                            {this.state.playPathFile === true ?
                              <ReactPlayerReading
                                ref={this.ref}
                                url={listQuestion.pathFile1 ? listQuestion.pathFile1 : ""}
                                playing={false}
                                controls={true}
                                onPlay={this.onPlaying}
                                onPause={this.onPausing}
                                config={{
                                  file: {
                                    attributes: {
                                      controlsList: 'nodownload'
                                    }
                                  }
                                }}
                                width="320px"
                                height="100px"
                              >
                              </ReactPlayerReading> : null
                            }

                            {this.state.playPathFileSubmit === true ?
                              <ReactPlayerReading
                                ref={this.ref}
                                url={listQuestion.pathFile1 ? listQuestion.pathFile1 : ""}
                                playing={false}
                                controls={true}
                                onPlay={this.onPlaying}
                                onPause={this.onPausing}
                                config={{
                                  file: {
                                    attributes: {
                                      controlsList: 'nodownload'
                                    }
                                  }
                                }}
                                width="320px"
                                height="100px"
                              >
                              </ReactPlayerReading> : null
                            }



                            {/*Câu hỏi 1*/}
                            {this.state.playQuestion1 === true ?
                              <ReactPlayerReading
                                ref={this.ref}
                                url={listQuestion.listQuestion[0].pathFileQues ? listQuestion.listQuestion[0].pathFileQues : ""}
                                playing={this.state.playQuestion1}
                                controls={this.state.playQuestion1}
                                onStart={this.onStarting}
                                onPlay={this.onPlaying}
                                onPause={this.onPausing}
                                onEnded = {this.endingFile}
                                config={{
                                  file: {
                                    attributes: {
                                      controlsList: 'nodownload'
                                    }
                                  }
                                }}
                                width="320px"
                                height="100px"
                              >
                              </ReactPlayerReading> : null
                            }

                            {this.state.playQuesA1 === true ?
                              <ReactPlayerReading
                                ref={this.ref}
                                url={listQuestion.listQuestion[0].pathFileQuesA ? listQuestion.listQuestion[0].pathFileQuesA : ""}
                                playing={this.state.playQuesA1}
                                controls={this.state.playQuesA1}
                                onPlay={this.onPlaying}
                                onPause={this.onPausing}
                                config={{
                                  file: {
                                    attributes: {
                                      controlsList: 'nodownload'
                                    }
                                  }
                                }}
                                width="320px"
                                height="100px"
                              >
                              </ReactPlayerReading> : null
                            }

                            {this.state.playQuesB1 === true ?
                              <ReactPlayerReading
                                ref={this.ref}
                                url={listQuestion.listQuestion[0].pathFileQuesB ? listQuestion.listQuestion[0].pathFileQuesB : ""}
                                playing={this.state.playQuesB1}
                                controls={this.state.playQuesB1}
                                onPlay={this.onPlaying}
                                onPause={this.onPausing}
                                config={{
                                  file: {
                                    attributes: {
                                      controlsList: 'nodownload'
                                    }
                                  }
                                }}
                                width="320px"
                                height="100px"
                              >
                              </ReactPlayerReading> : null
                            }

                            {this.state.playQuesC1 === true ?
                              <ReactPlayerReading
                                ref={this.ref}
                                url={listQuestion.listQuestion[0].pathFileQuesC ? listQuestion.listQuestion[0].pathFileQuesC : ""}
                                playing={this.state.playQuesC1}
                                controls={this.state.playQuesC1}
                                onPlay={this.onPlaying}
                                onPause={this.onPausing}
                                config={{
                                  file: {
                                    attributes: {
                                      controlsList: 'nodownload'
                                    }
                                  }
                                }}
                                width="320px"
                                height="100px"
                              >
                              </ReactPlayerReading> : null
                            }

                            {this.state.playQuesD1 === true ?
                              <ReactPlayerReading
                                ref={this.ref}
                                url={listQuestion.listQuestion[0].pathFileQuesD ? listQuestion.listQuestion[0].pathFileQuesD : ""}
                                playing={this.state.playQuesD1}
                                controls={this.state.playQuesD1}
                                onPlay={this.onPlaying}
                                onPause={this.onPausing}
                                config={{
                                  file: {
                                    attributes: {
                                      controlsList: 'nodownload'
                                    }
                                  }
                                }}
                                width="320px"
                                height="100px"
                              >
                              </ReactPlayerReading> : null
                            }

                            {/*Câu hỏi 2*/}
                            {this.state.playQuestion2 === true ?
                              <ReactPlayerReading
                                ref={this.ref}
                                url={listQuestion.listQuestion[1].pathFileQues ? listQuestion.listQuestion[1].pathFileQues : ""}
                                playing={this.state.playQuestion2}
                                controls={this.state.playQuestion2}
                                onPlay={this.onPlaying}
                                onPause={this.onPausing}
                                onEnded = {this.endingFile}
                                config={{
                                  file: {
                                    attributes: {
                                      controlsList: 'nodownload'
                                    }
                                  }
                                }}
                                width="320px"
                                height="100px"
                              >
                              </ReactPlayerReading> : null
                            }

                            {this.state.playQuesA2 === true ?
                              <ReactPlayerReading
                                ref={this.ref}
                                url={listQuestion.listQuestion[1].pathFileQuesA ? listQuestion.listQuestion[1].pathFileQuesA : ""}
                                playing={this.state.playQuesA2}
                                controls={this.state.playQuesA2}
                                onPlay={this.onPlaying}
                                onPause={this.onPausing}
                                config={{
                                  file: {
                                    attributes: {
                                      controlsList: 'nodownload'
                                    }
                                  }
                                }}
                                width="320px"
                                height="100px"
                              >
                              </ReactPlayerReading> : null
                            }

                            {this.state.playQuesB2 === true ?
                              <ReactPlayerReading
                                ref={this.ref}
                                url={listQuestion.listQuestion[1].pathFileQuesB ? listQuestion.listQuestion[1].pathFileQuesB : ""}
                                playing={this.state.playQuesB2}
                                controls={this.state.playQuesB2}
                                onPlay={this.onPlaying}
                                onPause={this.onPausing}
                                config={{
                                  file: {
                                    attributes: {
                                      controlsList: 'nodownload'
                                    }
                                  }
                                }}
                                width="320px"
                                height="100px"
                              >
                              </ReactPlayerReading> : null
                            }

                            {this.state.playQuesC2 === true ?
                              <ReactPlayerReading
                                ref={this.ref}
                                url={listQuestion.listQuestion[1].pathFileQuesC ? listQuestion.listQuestion[1].pathFileQuesC : ""}
                                playing={this.state.playQuesC2}
                                controls={this.state.playQuesC2}
                                onPlay={this.onPlaying}
                                onPause={this.onPausing}
                                config={{
                                  file: {
                                    attributes: {
                                      controlsList: 'nodownload'
                                    }
                                  }
                                }}
                                width="320px"
                                height="100px"
                              >
                              </ReactPlayerReading> : null
                            }

                            {this.state.playQuesD2 === true ?
                              <ReactPlayerReading
                                ref={this.ref}
                                url={listQuestion.listQuestion[1].pathFileQuesD ? listQuestion.listQuestion[1].pathFileQuesD : ""}
                                playing={this.state.playQuesD2}
                                controls={this.state.playQuesD2}
                                onPlay={this.onPlaying}
                                onPause={this.onPausing}
                                config={{
                                  file: {
                                    attributes: {
                                      controlsList: 'nodownload'
                                    }
                                  }
                                }}
                                width="320px"
                                height="100px"
                              >
                              </ReactPlayerReading> : null
                            }

                            {/*Câu hỏi 3*/}
                            {this.state.playQuestion3 === true ?
                              <ReactPlayerReading
                                ref={this.ref}
                                url={listQuestion.listQuestion[2].pathFileQues ? listQuestion.listQuestion[2].pathFileQues : ""}
                                playing={this.state.playQuestion3}
                                controls={this.state.playQuestion3}
                                onPlay={this.onPlaying}
                                onPause={this.onPausing}
                                onEnded = {this.endingFile}
                                config={{
                                  file: {
                                    attributes: {
                                      controlsList: 'nodownload'
                                    }
                                  }
                                }}
                                width="320px"
                                height="100px"
                              >
                              </ReactPlayerReading> : null
                            }

                            {this.state.playQuesA3 === true ?
                              <ReactPlayerReading
                                ref={this.ref}
                                url={listQuestion.listQuestion[2].pathFileQuesA ? listQuestion.listQuestion[2].pathFileQuesA : ""}
                                playing={this.state.playQuesA3}
                                controls={this.state.playQuesA3}
                                onPlay={this.onPlaying}
                                onPause={this.onPausing}
                                config={{
                                  file: {
                                    attributes: {
                                      controlsList: 'nodownload'
                                    }
                                  }
                                }}
                                width="320px"
                                height="100px"
                              >
                              </ReactPlayerReading> : null
                            }

                            {this.state.playQuesB3 === true ?
                              <ReactPlayerReading
                                ref={this.ref}
                                url={listQuestion.listQuestion[2].pathFileQuesB ? listQuestion.listQuestion[2].pathFileQuesB : ""}
                                playing={this.state.playQuesB3}
                                controls={this.state.playQuesB3}
                                onPlay={this.onPlaying}
                                onPause={this.onPausing}
                                config={{
                                  file: {
                                    attributes: {
                                      controlsList: 'nodownload'
                                    }
                                  }
                                }}
                                width="320px"
                                height="100px"
                              >
                              </ReactPlayerReading> : null
                            }

                            {this.state.playQuesC3 === true ?
                              <ReactPlayerReading
                                ref={this.ref}
                                url={listQuestion.listQuestion[2].pathFileQuesC ? listQuestion.listQuestion[2].pathFileQuesC : ""}
                                playing={this.state.playQuesC3}
                                controls={this.state.playQuesC3}
                                onPlay={this.onPlaying}
                                onPause={this.onPausing}
                                config={{
                                  file: {
                                    attributes: {
                                      controlsList: 'nodownload'
                                    }
                                  }
                                }}
                                width="320px"
                                height="100px"
                              >
                              </ReactPlayerReading> : null
                            }

                            {this.state.playQuesD3 === true ?
                              <ReactPlayerReading
                                ref={this.ref}
                                url={listQuestion.listQuestion[2].pathFileQuesD ? listQuestion.listQuestion[2].pathFileQuesD : ""}
                                playing={this.state.playQuesD3}
                                controls={this.state.playQuesD3}
                                onPlay={this.onPlaying}
                                onPause={this.onPausing}
                                config={{
                                  file: {
                                    attributes: {
                                      controlsList: 'nodownload'
                                    }
                                  }
                                }}
                                width="320px"
                                height="100px"
                              >
                              </ReactPlayerReading> : null
                            }

                          </>
                          : null
                        }

                        {listQuestion.pathFile2 === null ?
                          <Col xs="12" md="12" sm="12" lg="12" >
                            {this.state.load === true ? <>{loadingComponent}</> : ""}
                            {this.props.isLoading === true ? <>{loadingComponent}</> : ""}
                            {undefined !== listQuestion ?
                              <>
                                {data === false ?
                                  (<Row className={classes.bodyQuestion}>
                                    {listQuestion.listQuestion.map((question, indexQuestion) =>
                                      <>
                                        <Col xs="12" md="6" sm="12" lg="6" className={classes.questionMargin}>
                                          <b>{indexQuestion+1 + ". " + question.name}</b>
                                        </Col>
                                        <Col xs="12" md="6" sm="12" lg="6" className={classes.answerToChooseMargin}>
                                          <Form>
                                            {this.cutAnswerToChoose(question.answersToChoose).map((subAnswer, indexSubAnswer) =>
                                              <FormGroup check style={{marginBottom: '2%'}}>
                                                <Label onChange={this.onChangeValue} check className={classes.labelRadioButton}>
                                                  <Input type="radio" value={subAnswer+"|"+indexQuestion+"."+indexSubAnswer}
                                                         id={indexQuestion+"."+indexSubAnswer}
                                                         name={indexQuestion}/>{'   '}
                                                  {/*{this.cutSubAnswer(subAnswer)[0]}} {this.upperCaseFirstLetter(this.cutSubAnswer(subAnswer)[1])}*/}
                                                  ({ indexSubAnswer===0 ? "A" : (indexSubAnswer===1 ? "B" : (indexSubAnswer===2 ? "C" : "D")) }) {this.upperCaseFirstLetter(subAnswer)}
                                                </Label>
                                              </FormGroup>
                                            )}
                                          </Form>
                                        </Col>
                                      </>
                                    )}
                                    <Col xs="12" md="12" sm="12" lg="12" className={classes.divSubmit}>
                                      <button id="btnSubmit" className="btn btn-primary"
                                              onClick={this.showAnswer}
                                      >
                                        <FormattedMessage id="submit" tagName="data" />
                                        {/*nộp bài*/}
                                      </button>
                                    </Col>
                                  </Row>)
                                  :
                                  <>
                                    {/*đáp án*/}
                                    <Row xs="12" md="12" sm="12" lg="12" style={{marginTop: '3%'}}>
                                      <Col xs="12" md="5" sm="12" lg="5" style={{padding: '0px'}}>
                                        <Col xs="12" md="12" sm="12" lg="12" className={classes.questionMargin}>
                                          <fieldset className={classes.fieldset}>
                                            <legend className={classes.legend}>Transcript</legend>
                                            <Col xs="12" md="12" sm="12" lg="12">
                                              <i>{this.cutInputValue(listQuestion.transcript)[0]}</i>
                                              <hr style={{backgroundColor: 'lightgray'}}/>
                                              <i>{this.cutInputValue(listQuestion.transcript)[1]}</i>
                                            </Col>
                                          </fieldset>
                                        </Col>
                                      </Col>

                                      <Col xs="12" md="6" sm="12" lg="6">
                                        {listQuestion.listQuestion.map((question, indexQuestion) =>
                                          <Col xs="12" md="12" sm="12" lg="12" className={classes.answerToChooseMargin}>
                                            <Col xs="12" md="12" sm="12" lg="12" className={classes.questionName}>

                                              <h5 style={{fontWeight:'700'}}>{indexQuestion + 1 + ". " + question.name}
                                                {/*phần dịch câu hỏi*/}
                                                <button style={{border: 'none', backgroundColor: 'white', padding: '0', marginLeft: '2%'}}
                                                        id={"q" + question.id} className="mr-1 mb-1">
                                                  <Icon.AlertCircle  size={20} />
                                                </button>
                                                <UncontrolledPopover trigger="focus" placement="bottom" target={"q" + question.id}
                                                >
                                                  <PopoverBody style={{borderColor: 'green !important',
                                                    color: 'white', backgroundColor: 'green', borderRadius: '5px'}}>
                                                    <FormattedMessage id="translate" />: {question.translatingQuestion !== null ? question.translatingQuestion : "chưa có phần dịch"}
                                                  </PopoverBody>
                                                </UncontrolledPopover>
                                                {/*end phần dịch câu hỏi*/}

                                                <Button outline className={classes.buttonListenAgain}
                                                        style={{padding: '0', display:'inline-flex',  width:'auto'}} id='btnHighlightRepeat'
                                                        onClick={()=>this.playingListening(question, indexQuestion - 3, indexQuestion)}>
                                                  <div style={{padding: '0 5px', display:'inline-flex'}}>
                                                    {this.state.checkPlayingAgain === (indexQuestion - 3) ?

                                                      <Icon.Volume1 size={12} style={{marginTop:'2.5px', marginRight:'2px'}} />
                                                      :
                                                      <Icon.Play size={11} style={{marginTop:'2.5px', marginRight:'2px'}}/>
                                                    }
                                                    <p style={{marginTop:'-2px'}}><FormattedMessage id="listenAgain" tagName="data" /></p>

                                                  </div>
                                                </Button>

                                                {/*<Button outline className={classes.buttonListenAgain} style={{padding: '0', display:'inline-flex'}} id='btnHighlightRepeat'*/}
                                                        {/*onClick={()=>this.playingListening(question, indexQuestion - 3, indexQuestion)}>*/}
                                                  {/*{*/}
                                                    {/*this.state.checkPlayingAgain === (indexQuestion - 3) ?*/}
                                                      {/*<Icon.Volume1 size={12} className={classes.iconPlay} />*/}
                                                      {/*:*/}
                                                      {/*<Icon.Play className={classes.iconPlay} size={11}/>*/}
                                                  {/*}*/}
                                                  {/*<p style={{marginTop:'-2px'}}><FormattedMessage id="listenAgain" tagName="data" /></p>*/}

                                                {/*</Button>*/}
                                              </h5>

                                            </Col>
                                            <Form style={{marginBottom: '5%'}} onSubmit={(e) => this.submitForm(e)}>
                                              {this.cutAnswerToChoose(question.answersToChoose).map((subAnswer, i) =>
                                                <Col xs="12" md="12" sm="12" lg="12" style={{display: 'flex', padding: '0%'}}>
                                                  <Col xs="1" md="2" sm="1" lg="1" style={{padding: '0%'}}>
                                                    {i === question.correctIndex?
                                                      <Icon.Check size={25} style={{color: 'green'}}/> : null
                                                    }
                                                    {i === question.incorrectIndex?
                                                      <Icon.X size={25} style={{color: '#b21717'}} /> : null
                                                    }
                                                  </Col>
                                                  <Col xs="11" md="10" sm="11" lg="11" style={{padding: '0%', display: 'flex'}}>
                                                    <FormGroup check style={{marginBottom: '2%'}}>
                                                      <Label onChange={this.onChangeValue} check className={classes.labelRadioButton}>
                                                        <Input type="radio" name="subAnswer"
                                                               checked={undefined !== question.incorrectIndex && i === question.incorrectIndex ? true
                                                                 : ( (undefined === question.incorrectIndex || null === question.incorrectIndex ) && i === question.correctIndex ? true : false)
                                                               }
                                                        />{'       '}
                                                        ({ i===0 ? "A" : (i===1 ? "B" : (i===2 ? "C" : "D")) }) {this.upperCaseFirstLetter(subAnswer)}
                                                      </Label>
                                                      {/*phần dịch câu đáp án*/}
                                                      <button style={{border: 'none', backgroundColor: 'white', padding: '0', marginLeft: '3px'}}
                                                              id={"a" + question.id + i} className="mr-1 mb-1">
                                                        <Icon.AlertCircle  size={20} />
                                                      </button>
                                                      <UncontrolledPopover trigger="focus" placement="bottom" target={"a" + question.id + i}
                                                      >
                                                        <PopoverBody style={{borderColor: 'green !important',
                                                          color: 'white', backgroundColor: 'green', borderRadius: '5px'}}>
                                                          <FormattedMessage id="translate" />: {i===0 ? question.translatingQuesA : (i===1 ? question.translatingQuesB : (i===2 ? question.translatingQuesC : question.translatingQuesD))}
                                                        </PopoverBody>
                                                      </UncontrolledPopover>
                                                      {/*phần dịch câu đáp án*/}
                                                    </FormGroup>

                                                    <div>
                                                      {/*{i === question.correctIndex?*/}
                                                        {/*<button className={classes.btnRepeat}*/}
                                                                {/*onClick={()=>this.playingListening(question, indexQuestion)}>*/}

                                                          {/*{(this.state.play0 === indexQuestion &&  this.state.play === true)*/}
                                                          {/*||(this.state.play1 === indexQuestion &&  this.state.play === true)*/}
                                                          {/*||(this.state.play2 === indexQuestion &&  this.state.play === true)*/}
                                                          {/*||(this.state.play3 === indexQuestion &&  this.state.play === true)*/}
                                                            {/*?*/}
                                                            {/*<Icon.Volume1 size={12} className={classes.iconPlay} /> :*/}
                                                            {/*<Icon.Play className={classes.iconPlay} size={11}/>*/}
                                                          {/*}*/}
                                                          {/*/!*Nghe lại*!/*/}
                                                          {/*<FormattedMessage id="listenAgain" tagName="data" />*/}
                                                        {/*</button> : null*/}
                                                      {/*}*/}

                                                      {/*<button className={classes.btnRepeat}*/}
                                                              {/*onClick={()=>this.playingListening(question, indexQuestion, i)}>*/}

                                                        {/*<Icon.Play className={classes.iconPlay} size={11}/>*/}
                                                        {/*/!*Nghe lại*!/*/}
                                                        {/*<FormattedMessage id="listenAgain" tagName="data" />*/}
                                                      {/*</button>*/}

                                                    </div>
                                                  </Col>

                                                </Col>
                                              )}
                                            </Form>
                                            <Col xs="12" md="12" sm="12" lg="12" style={{marginBottom: '10%'}}>
                                              <fieldset className={classes.fieldsetDescription}>
                                                <legend className={classes.legendDescription}>
                                                  <FormattedMessage id="description" tagName="data" />
                                                  {/*Giải thích*/}
                                                </legend>
                                                <i>{question.description}</i>
                                              </fieldset>
                                            </Col>
                                          </Col>
                                        )}
                                      </Col>
                                      {this.state.index + 1 === total + 1 ? "" :
                                        <Col xs="12" md="1" sm="12" lg="1" className={classes.btnNextQuestion}>
                                          <Icon.ChevronsRight
                                            size={64}
                                            className="mr-4 fonticon-wrap"
                                            id = "iconNextQuestion"
                                            style={{
                                              margin: '0px !important',
                                              cursor: 'pointer',
                                              color: 'black'
                                            }}
                                            onClick={this.nextQuestion}
                                          />
                                        </Col>
                                      }
                                    </Row>
                                  </>
                                }
                              </>
                              : null
                            }
                          </Col>
                          :
                          <Col xs="12" md="12" sm="12" lg="12">
                            {this.state.load === true ? <>{loadingComponent}</> : ""}
                            {this.props.isLoading === true ? <>{loadingComponent}</> : ""}
                            {undefined !== listQuestion ?
                              <>
                                {data === false ?
                                  (<Row className={classes.bodyQuestion}>
                                    <Col xs="12" md="6" sm="12" lg="6" className={classes.questionMargin}>
                                      <img className={classes.imgClass} style={{width: '100%', border: '5px solid lightgray'}}
                                           src={listQuestion.pathFile2 ? listQuestion.pathFile2 : ""}/>
                                    </Col>
                                    <Col xs="12" md="6" sm="12" lg="6">
                                      {listQuestion.listQuestion.map((question, indexQuestion) =>
                                        <>
                                          <Col xs="12" md="12" sm="12" lg="12" className={classes.answerToChooseMargin}>
                                            <Col xs="12" md="12" sm="12" lg="12" className={classes.questionName}>
                                              <b>{indexQuestion + 1 + ". " + question.name}</b>
                                            </Col>
                                            <Form style={{marginLeft: '5%'}}>
                                              {this.cutAnswerToChoose(question.answersToChoose).map((subAnswer, indexSubAnswer) =>
                                                <FormGroup check style={{marginBottom: '2%'}}>
                                                  <Label onChange={this.onChangeValue} check
                                                         className={classes.labelRadioButton}>
                                                    <Input type="radio"
                                                           value={subAnswer + "|" + indexQuestion + "." + indexSubAnswer}
                                                           id={indexQuestion + "." + indexSubAnswer}
                                                           name={indexQuestion}/>{'   '}
                                                    ({ indexSubAnswer===0 ? "A" : (indexSubAnswer===1 ? "B" : (indexSubAnswer===2 ? "C" : "D")) }) {this.upperCaseFirstLetter(subAnswer)}
                                                  </Label>
                                                </FormGroup>
                                              )}
                                            </Form>
                                          </Col>
                                        </>
                                      )}
                                    </Col>
                                    <Col xs="12" md="12" sm="12" lg="12" className={classes.divSubmit}>
                                      <button id="btnSubmit" className="btn btn-primary"
                                              onClick={this.showAnswer}
                                      >
                                        <FormattedMessage id="submit" tagName="data" />
                                        {/*nộp bài*/}
                                      </button>
                                    </Col>
                                  </Row>)
                                  :
                                  <>
                                    {/*đáp án*/}
                                    <Row xs="12" md="12" sm="12" lg="12" style={{marginTop: '3%'}}>
                                      <Col xs="12" md="5" sm="12" lg="5" style={{padding: '0px'}}>
                                        <Col xs="12" md="12" sm="12" lg="12" className={classes.questionMargin}>
                                          <img className={classes.imgClass} style={{width: '100%', border: '5px solid lightgray'}}
                                               src={listQuestion.pathFile2 ? listQuestion.pathFile2 : ""}/>
                                        </Col>
                                        <Col xs="12" md="12" sm="12" lg="12" className={classes.questionMargin}>
                                          <fieldset className={classes.fieldset}>
                                            <legend className={classes.legend}>Transcript</legend>
                                            <Col xs="12" md="12" sm="12" lg="12">
                                              <i>{this.cutInputValue(listQuestion.transcript)[0]}</i>
                                              <hr style={{backgroundColor: 'lightgray'}}/>
                                              <i>{this.cutInputValue(listQuestion.transcript)[1]}</i>
                                            </Col>
                                          </fieldset>
                                        </Col>
                                      </Col>

                                      <Col xs="12" md="6" sm="12" lg="6">
                                        {listQuestion.listQuestion.map((question, indexQuestion) =>
                                          <Col xs="12" md="12" sm="12" lg="12" className={classes.answerToChooseMargin}>

                                            <Col xs="12" md="12" sm="12" lg="12" className={classes.questionName}>
                                              {/*<div>*/}
                                                {/*<b>{indexQuestion + 1 + ". " + question.name}</b>*/}
                                                {/*/!*phần dịch câu hỏi*!/*/}
                                                {/*<button style={{border: 'none', backgroundColor: 'white', padding: '0', marginLeft: '3px'}}*/}
                                                        {/*id={"q" + question.id} className="mr-1 mb-1">*/}
                                                  {/*<Icon.AlertCircle  size={20} />*/}
                                                {/*</button>*/}
                                                {/*<UncontrolledPopover trigger="focus" placement="bottom" target={"q" + question.id}*/}

                                                {/*>*/}
                                                  {/*<PopoverBody style={{borderColor: 'green !important',*/}
                                                    {/*color: 'white', backgroundColor: 'green', borderRadius: '5px'}}>*/}
                                                    {/*<FormattedMessage id="translate" />: {question.translatingQuestion !== null ? question.translatingQuestion : "chưa có phần dịch"}*/}
                                                  {/*</PopoverBody>*/}
                                                {/*</UncontrolledPopover>*/}
                                              {/*</div>*/}
                                              {/*<div>*/}
                                                {/*<button className={classes.btnRepeat}*/}
                                                        {/*onClick={()=>this.playingListening(question, indexQuestion - 3, indexQuestion)}>*/}

                                                  {/*{this.state.checkPlayingAgain === (indexQuestion - 3) ?*/}
                                                    {/*<Icon.Volume1 size={12} className={classes.iconPlay} />*/}
                                                    {/*:*/}
                                                    {/*<Icon.Play className={classes.iconPlay} size={11}/>*/}
                                                  {/*}*/}
                                                  {/*/!*Nghe lại*!/*/}
                                                  {/*<FormattedMessage id="listenAgain" tagName="data" />*/}
                                                {/*</button>*/}
                                              {/*</div>*/}
                                              {/*/!*end phần dịch câu hỏi*!/*/}

                                              <h5 style={{fontWeight:'700'}}>{indexQuestion + 1 + ". " + question.name}
                                                {/*phần dịch câu hỏi*/}
                                                <button style={{border: 'none', backgroundColor: 'white', padding: '0', marginLeft: '2%'}}
                                                        id={"q" + question.id} className="mr-1 mb-1">
                                                  <Icon.AlertCircle  size={20} />
                                                </button>
                                                <UncontrolledPopover trigger="focus" placement="bottom" target={"q" + question.id}
                                                >
                                                  <PopoverBody style={{borderColor: 'green !important',
                                                    color: 'white', backgroundColor: 'green', borderRadius: '5px'}}>
                                                    <FormattedMessage id="translate" />: {question.translatingQuestion !== null ? question.translatingQuestion : "chưa có phần dịch"}
                                                  </PopoverBody>
                                                </UncontrolledPopover>
                                                {/*end phần dịch câu hỏi*/}

                                                <Button outline className={classes.buttonListenAgain}
                                                        style={{padding: '0', display:'inline-flex',  width:'auto'}} id='btnHighlightRepeat'
                                                        onClick={()=>this.playingListening(question, indexQuestion - 3, indexQuestion)}>
                                                  <div style={{padding: '0 5px', display:'inline-flex'}}>
                                                    {this.state.checkPlayingAgain === (indexQuestion - 3) ?

                                                      <Icon.Volume1 size={12} style={{marginTop:'2.5px', marginRight:'2px'}} />
                                                      :
                                                      <Icon.Play size={11} style={{marginTop:'2.5px', marginRight:'2px'}}/>
                                                    }
                                                    <p style={{marginTop:'-2px'}}><FormattedMessage id="listenAgain" tagName="data" /></p>

                                                  </div>
                                                </Button>

                                                {/*<Button outline className={classes.buttonListenAgain} style={{padding: '0', display:'inline-flex'}} id='btnHighlightRepeat'*/}
                                                        {/*onClick={()=>this.playingListening(question, indexQuestion - 3, indexQuestion)}>*/}
                                                  {/*{*/}
                                                    {/*this.state.checkPlayingAgain === (indexQuestion - 3) ?*/}
                                                      {/*<Icon.Volume1 size={12} className={classes.iconPlay} />*/}
                                                      {/*:*/}
                                                      {/*<Icon.Play className={classes.iconPlay} size={11}/>*/}
                                                  {/*}*/}
                                                  {/*<p style={{marginTop:'-2px'}}><FormattedMessage id="listenAgain" tagName="data" /></p>*/}

                                                {/*</Button>*/}
                                              </h5>

                                            </Col>

                                            <Form style={{marginBottom: '5%'}} onSubmit={(e) => this.submitForm(e)}>
                                              {this.cutAnswerToChoose(question.answersToChoose).map((subAnswer, i) =>
                                                <Col xs="12" md="12" sm="12" lg="12"
                                                     style={{display: 'flex', padding: '0%'}}>
                                                  <Col xs="1" md="2" sm="1" lg="1" style={{padding: '0%'}}>
                                                    {i === question.correctIndex ?
                                                      <Icon.Check size={25} style={{color: 'green'}}/> : null
                                                    }
                                                    {i === question.incorrectIndex ?
                                                      <Icon.X size={25} style={{color: '#b21717'}}/> : null
                                                    }
                                                  </Col>
                                                  <Col xs="11" md="10" sm="11" lg="11"
                                                       style={{padding: '0%', display: 'flex'}}>
                                                    <FormGroup check style={{marginBottom: '2%'}}>
                                                      <Label onChange={this.onChangeValue} check
                                                             className={classes.labelRadioButton}>
                                                        <Input type="radio" name="subAnswer"
                                                               checked={undefined !== question.incorrectIndex && i === question.incorrectIndex ? true
                                                                 : ( (undefined === question.incorrectIndex || null === question.incorrectIndex) && i === question.correctIndex ? true : false)
                                                               }
                                                        />{'       '}
                                                        ({ i===0 ? "A" : (i===1 ? "B" : (i===2 ? "C" : "D")) }) {this.upperCaseFirstLetter(subAnswer)}
                                                      </Label>
                                                      {/*phần dịch câu đáp án*/}
                                                      <button style={{border: 'none', backgroundColor: 'white', padding: '0', marginLeft: '3px'}}
                                                              id={"a" + question.id + i} className="mr-1 mb-1">
                                                        <Icon.AlertCircle  size={20} />
                                                      </button>
                                                      <UncontrolledPopover trigger="focus" placement="bottom" target={"a" + question.id + i}
                                                      >
                                                        <PopoverBody style={{borderColor: 'green !important',
                                                          color: 'white', backgroundColor: 'green', borderRadius: '5px'}}>
                                                          <FormattedMessage id="translate" />: {i===0 ? question.translatingQuesA : (i===1 ? question.translatingQuesB : (i===2 ? question.translatingQuesC : question.translatingQuesD))}
                                                        </PopoverBody>
                                                      </UncontrolledPopover>
                                                      {/*phần dịch câu đáp án*/}
                                                    </FormGroup>

                                                    <div>
                                                      {/*{i === question.correctIndex ?*/}
                                                        {/*<button className={classes.btnRepeat}*/}
                                                                {/*onClick={()=>this.playingListening(question, indexQuestion, i)}>*/}

                                                          {/*{(this.state.play0 === indexQuestion &&  this.state.play === true)*/}
                                                          {/*||(this.state.play1 === indexQuestion &&  this.state.play === true)*/}
                                                          {/*||(this.state.play2 === indexQuestion &&  this.state.play === true)*/}
                                                          {/*||(this.state.play3 === indexQuestion &&  this.state.play === true)*/}
                                                            {/*?*/}
                                                            {/*<Icon.Volume1 size={12} className={classes.iconPlay} /> :*/}
                                                            {/*<Icon.Play className={classes.iconPlay} size={11}/>*/}
                                                          {/*}*/}
                                                          {/*/!*Nghe lại*!/*/}
                                                          {/*<FormattedMessage id="listenAgain" tagName="data" />*/}
                                                        {/*</button> : null*/}
                                                      {/*}*/}

                                                      {/*<button className={classes.btnRepeat}*/}
                                                              {/*onClick={()=>this.playingListening(question, indexQuestion, i)}>*/}

                                                          {/*<Icon.Play className={classes.iconPlay} size={11}/>*/}
                                                        {/*/!*Nghe lại*!/*/}
                                                        {/*<FormattedMessage id="listenAgain" tagName="data" />*/}
                                                      {/*</button>*/}

                                                    </div>
                                                  </Col>
                                                </Col>
                                              )}
                                            </Form>
                                            <Col xs="12" md="12" sm="12" lg="12" style={{marginBottom: '10%'}}>
                                              <fieldset className={classes.fieldsetDescription}>
                                                <legend className={classes.legendDescription}>
                                                  <FormattedMessage id="description" tagName="data" />
                                                  {/*Giải thích*/}
                                                </legend>
                                                <i>{question.description}</i>
                                              </fieldset>
                                            </Col>
                                          </Col>
                                        )}
                                      </Col>
                                      {this.state.index + 1 === total + 1 ? "" :
                                        <Col xs="12" md="1" sm="12" lg="1" className={classes.btnNextQuestion}>
                                          <Icon.ChevronsRight
                                            size={64}
                                            className="mr-4 fonticon-wrap"
                                            id="iconNextQuestion"
                                            style={{
                                              margin: '0px !important',
                                              cursor: 'pointer',
                                              color: 'black'
                                            }}
                                            onClick={this.nextQuestion}
                                          />
                                        </Col>
                                      }
                                    </Row>
                                  </>
                                }
                              </>
                              : null
                            }
                          </Col>
                        }
                        <ToastContainer />
                      </>
                    </Col>
                  </Row>
                </div>
              </Col>
            </Container>
          </>
          :
          <>
            {this.props.index === 3 ?
              <>
                {/*<button id="btnBackHistory" onClick={this.backToHistory}>*/}
                  {/*Quay lại*/}
                {/*</button>*/}
                <button id="btnBackHistory" style={{marginLeft: '2%'}} left
                        onClick={this.backToHistory} title={showMessage("back")}>
                  <ArrowLeft width="16" height="16"/>
                </button>
              </>

              : null
            }

            <Row className={classes.colTable}>
              <div style={{color: 'black', fontSize: '200%', fontWeight: '600'}}>
                {/*Kết quả bài nghe tổng hợp*/}
                <FormattedMessage id="total.result.listening.part" tagName="data" />{' '}
              </div>
              <table className="table table-bordered"
                     style={{border: '2px solid gray',
                       backgroundColor: 'white',
                       marginTop: '3%'}}>

                <thead>
                <tr>
                  <th>
                    {/*STT*/}
                    <FormattedMessage id="index" tagName="data" />
                  </th>
                  <th>
                    {/*Chủ đề*/}
                    <FormattedMessage id="topic" tagName="data" />
                  </th>
                  <th>
                    {/*Số câu đúng*/}
                    <FormattedMessage id="total.of.correct.answer" tagName="data" />
                  </th>
                  <th></th>
                </tr>
                </thead>
                <tbody>
                {listCategories.map( (category, index) =>
                  <tr>
                    <td>{index + 1}</td>
                    <td>{category.topicName}</td>
                    {category.correctQuesNumber === category.listQuestion.length ?
                      <td style={{color: 'green'}}>{category.correctQuesNumber}/{category.listQuestion.length}</td> :
                      <td style={{color: 'red'}}>{category.correctQuesNumber}/{category.listQuestion.length}</td>
                    }
                    <td>
                      {/*<a style={{color: '#02a1ed', borderBottom: '1px solid'}} onClick={(e) =>this.review(e, index)}><i>Xem lại</i></a>*/}
                      <button id="btnHighLightLink" color="link" onClick={(e) =>this.review(e, index)}>
                        {/*Xem lại*/}
                        <FormattedMessage id="review" tagName="data" />{' '}
                      </button>
                    </td>
                  </tr>
                )}
                </tbody>
              </table>

              {this.props.index === 3 ?
                null :
                <Col xs="12" md="12" sm="12" lg="12" className={classes.footerTotalResult}>
                  {this.props.loadingSaveResult ? <>{loadingComponent}</> : null}
                  {this.props.isLoading === true ? <>{loadingComponent}</> : ""}
                  <div style={{marginTop: '3%'}}>
                    {/*Chúc mừng bạn đã hoàn thành bài luyện tập! */}
                    <FormattedMessage id="congratulations.you.finished.the.exercise" tagName="data" />{' '}
                  </div>
                  <div>
                    {/*Tiếp tục luyện tập thêm các bài tương tự?*/}
                    <FormattedMessage id="do.you.want.to.keep.doing.the.same.exercise" tagName="data" />{' '}
                  </div>
                  {this.props.hiddenBtnSaveResult === false ?
                    <button id="btnSaveResult" className="btn btn-primary" style={{marginTop: '3%', marginRight: '2%'}}
                            onClick={this.saveResult}
                    >
                      <FormattedMessage id="save" tagName="data" />
                    </button> : null
                  }
                  <button id="btnSubmit" className="btn btn-primary" style={{marginTop: '3%'}}
                          onClick={this.doSameExercise}>
                    <FormattedMessage id="do.the.same" tagName="data" />
                  </button>
                </Col>
              }
            </Row>
          </>
        }
      </>
    );
  }
}

const mapStateToProps = state => {
  return {
    userInfo: state.auth.login,
    listCategories: state.multiListeningReducer.listCategories,
    reload: state.multiListeningReducer.reload,
    noTranslating: state.doTestReducer.noTranslating,
    loadingSaveResult: state.historyPracticseReducer.loadingSaveResult,
    studentAccountReducer: state.auth.login,
    index: state.multiListeningReducer.index,
    hiddenBtnSaveResult: state.historyPracticseReducer.hiddenBtnSaveResult,
    isLoading: state.multiListeningReducer.isLoading
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    // practiceListenMultiActions: bindActionCreators(practiceListenMultiActions,dispatch),
    multiListenQuestionAction: bindActionCreators(practiceListenMultiActions,dispatch),
    historyPracticesAction: bindActionCreators(historyPracticesAction,dispatch),
    historyTestAction: bindActionCreators(historyTestAction,dispatch)
  }
}
const withConnect = connect(mapStateToProps, mapDispatchToProps)
export default compose(withStyles(styles), withConnect)(ListeningToeicConversation);
// export default connect(mapStateToProps , mapDispatchToProps)(ListeningToeicConversation)
