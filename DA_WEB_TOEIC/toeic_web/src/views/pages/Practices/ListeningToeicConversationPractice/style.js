import styled from "styled-components"
import ReactPlayer from "react-player"

export const ReactPlayerReading = styled(ReactPlayer)`
  margin : 0 auto 30px;

  & audio{
    outline : none;
  }
`

export const styles =()=>({
  topicName:{
    textAlign: 'center',
    fontSize: '150%'
  },
  titleModal:{
    backgroundColor: '#1da9ed',
    margin: '0 12%',
    height: '50px',
    borderRadius: '8px',
    color: 'white',
    position: 'relative'
  },
  titleModal2:{
    backgroundColor: '#1da9ed',
    marginLeft: '0% !important',
    marginRight: '0% !important',
    height: '50px',
    borderRadius: '5px'
  },
  totalQuesion:{
    position: 'relative',
  },
  totalQuesionChild:{
    color: 'white',
    top: '50%',
    position: 'absolute',
    // -ms-transform: translateY(-50%);
    transform: 'translateY(-50%)',
    fontSize: '170%'
  },
  totalQuesionChild2:{
    color: 'white',
    top: '50%',
    position: 'absolute',
    // -ms-transform: translateY(-50%);
    transform: 'translateY(-50%)',
    textAlign: 'right',
    width: '100%',
    paddingRight: '10%',
    fontSize: '150%'
  },
  titleModalChild:{
    marginLeft: '2%',
    position: 'absolute',
    top: '50%',
    // -ms-transform: translateY(-50%);
    transform: 'translateY(-50%)',
    fontSize: '180%'
  },
  bodySnp:{
    paddingLeft: '0px',
    paddingRight: '0px'
  },
  bodyModal:{
    backgroundColor: 'white',
    margin: '0 6px',
    border: '1px solid #0f7aae'
  },


  /*css question*/
  audio:{
    width: '40%',
    height: '100p',
  },
  bodyQuestion:{
    marginLeft: '3% !important',
  },
  questionMargin:{
    marginBottom: '3%',
    color: 'black'
  },
  answerToChooseMargin:{
    marginBottom: '3%',
    paddingLeft: '5%'
  },
  labelRadioButton: {
    display: 'contents',
    fontSize: '100%'
  },
  divSubmit:{
    textAlign: 'center',
    margin: '5% 0 5% 0'
  },

  btnSubmit:{
    backgroundColor: '#02a1ed !important',
    padding: '8px 25px',
    borderColor: '#02a1ed',
    borderRadius: '7px !important' ,
    color: 'white',
    "&hover": {
      backgroundColor: 'red !important'
    }
  },


  /*css answer*/
  legend:{
    color: 'black',
    textAlign: 'left',
    backgroundColor: 'lightgray',
    width: 'auto',
    padding: '5px 20px !important',
    fontSize: 'medium'
  },
  fieldset:{
    border: '2px solid lightgray !important',
    padding: '0 2% 2% 2% !important',
    textAlign: 'left !important',
    color: 'gray'
  },
  questionName:{
    paddingLeft: '0px !important',
    marginBottom: '2%',
    color: 'black'
  },
  legendDescription:{
    color: 'white',
    textAlign: 'left',
    backgroundColor: 'green',
    width: 'auto !important',
    padding: '3px 15px !important ',
    fontSize: 'medium'
  },
  fieldsetDescription:{
    border: '2px solid green !important',
    padding: '0 2% 2% 2% !important',
    textAlign: 'left !important',
    color: 'green !important'
  },

  btnNextQuestion:{
    textAlign: 'center',
    padding: '0px'
  },

  btnRepeat:{
    backgroundColor: 'white',
    border: '2px solid orange',
    color: 'orange',
    fontSize: '80%',
    borderRadius: '5px !important',
    display: 'flex',
    height: '20px !important',
    width: '75px'
  },
  iconPlay:{
    marginTop: 'auto',
    marginBottom: 'auto',
    /* margin-left: auto; */
    marginLeft: '12%'
  },

  /*css total result*/
  colTable:{
    margin: '5% 20% !important',
    textAlign: 'center'
  },
  tableTotal:{
    border: '2px solid gray',
    backgroundColor: 'white',
    marginTop: '3%'
  },
  th:{
    fontSize: '120%',
    color: 'black',
    border: '1px solid gray'
  },
  td: {
    border: '1px solid gray'
  },

  footerTotalResult: {
    textAlign: 'center',
    color: 'black',
    fontSize: '150%',
    fontWeight: '600'
  },

  btnDoTheSameExercise:{
    backgroundColor: '#02a1ed !important',
    padding: '8px 25px',
    borderColor: '#02a1ed',
    borderRadius: '7px !important' ,
    marginTop: '3% !important',
    color: 'white',
    "&hover": {
      backgroundColor: 'red !important'
    }
  },

  buttonListenAgain:{
    backgroundColor: 'white !important',
    border: '2px solid orange !important',
    color: 'orange !important',
    fontSize: '80%',
    borderRadius: '5px !important',
    display: 'flex',
    height: '20px !important',
    width: '75px',
    margin: '0'
  }

})

