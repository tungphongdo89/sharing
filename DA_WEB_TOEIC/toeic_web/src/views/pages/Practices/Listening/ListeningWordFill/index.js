import React, {Component} from 'react';
import * as Styles from "./style"
import Question from "./question"
import {connect} from "react-redux";
import {bindActionCreators, compose} from "redux";
import * as styles from "../../PracticeMulti/style";
import {withStyles} from "@material-ui/styles";
import practiceListenMultiActions from "../../../../../redux/actions/practice/listening";
import readingActions from "../../../../../redux/actions/practice/reading";
import {FormattedMessage} from "react-intl";
import {CODE_PART1_LF, CODE_PART2_LF, CODE_PART3_LF, CODE_PART4_LF} from "../../../../../commons/constants/CONSTANTS";
import Spinner from "reactstrap/es/Spinner";
import { Empty } from 'antd';
import 'antd/dist/antd.css';
class Index extends Component {

  constructor(props) {
    super(props);
    this.state = {
      listAnswerEntered: {},
      stt: this.props.indexListen
    }
  }

  componentDidMount() {
    const {listeningActions, readingActions} = this.props;
    const {updateCheckListeningFalse} = listeningActions
    const {cleanListQuestionAndAnswerReading} = readingActions;
    cleanListQuestionAndAnswerReading();
    updateCheckListeningFalse();
  }

  onSubmit = (listAnswerEnteredOne, indexQuestion) => {
    let newList = this.state.listAnswerEntered
    newList[indexQuestion] = listAnswerEnteredOne
    this.setState({
      isSubmit: true,
      listAnswerEntered: newList
    })
  }

  loadingComponent = (
    <div style={{
      position: 'absolute',
      zIndex: 110,
      top: '110%',
      left: 0,
      width: '100%',
      height: '100%',
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      background: 'rgba(255,255,255,0.8)'
    }}>
      <Spinner color="primary" className="reload-spinner"/>
    </div>
  );


  next = () => {
    const {updateSubmitFill} = this.props.listeningActions;
    updateSubmitFill();
    this.setState({
      stt: this.state.stt + 1
    })
  }

  render() {
    const {listQuestionWordFill, viewHistory} = this.props;

    let listInQuestion = viewHistory ?
      listQuestionWordFill ? listQuestionWordFill.map((question, index) => {
        return <Question key={index}
                         question={question}
                         isSubmit={this.props.submitFill}
                         index={index}
                         stt={this.state.stt}
                         next={this.next}
        ></Question>
      }) : null
      :
      listQuestionWordFill ? listQuestionWordFill.data ? listQuestionWordFill.data.map((question, index) => {
          return <Question key={index}
                           question={question}
                           isSubmit={this.props.submitFill}
                           index={index}
                           stt={this.state.stt}
                           next={this.next}
          ></Question>
        }) : null
        : null

    let renderTitle = () => {
      let title = null;
      if (this.props.part === CODE_PART1_LF) {
        title = (<FormattedMessage id="practices.listening.photographs" tagName="data"/>)
      } else if (this.props.part === CODE_PART2_LF) {
        title = (<FormattedMessage id="practices.listening.questionResponse" tagName="data"/>)
      } else if (this.props.part === CODE_PART3_LF) {
        title = (<FormattedMessage id="practices.listening.shortConversation" tagName="data"/>)
      } else if (this.props.part === CODE_PART4_LF) {
        title = (<FormattedMessage id="practices.listening.shortTalk" tagName="data"/>)
      }
      return title;
    }
    return (
      <Styles.ContainerStyle style={{width: '90% !important'}}>
        <h3 style={{textAlign: 'center', marginBottom: '2%'}}>
          {renderTitle()}
        </h3>

        {listInQuestion ? listInQuestion.length > 0 ? listInQuestion[this.state.stt] :<Empty />:''}
        {this.props.isLoadFill === true ? this.loadingComponent : null}
      </Styles.ContainerStyle>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  listeningActions: bindActionCreators(practiceListenMultiActions, dispatch),
  readingActions: bindActionCreators(readingActions, dispatch)
})

const mapStateToProps = (state) => {
  return {
    listQuestionWordFill: state.multiListeningReducer.listQuestionWordFill,
    resultQuestionWordFill: state.multiListeningReducer.resultQuestionWordFill,
    numberCorrectAnswer: state.multiListeningReducer.numberCorrectAnswer,
    partId: state.multiListeningReducer.partId,
    indexListen: state.multiListeningReducer.indexListen,
    part: state.multiListeningReducer.part,
    submitFill: state.multiListeningReducer.submitFill,
    isLoadFill: state.multiListeningReducer.isLoadFill,
    viewHistory: state.multiListeningReducer.viewHistory
  }
}
const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
)

export default compose(withStyles(styles.style), withConnect)(Index);
