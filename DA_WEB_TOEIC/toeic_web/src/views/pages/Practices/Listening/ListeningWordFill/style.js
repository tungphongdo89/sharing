import styled from "styled-components"
import { Card , CardBody , CardTitle , CardHeader , Row , Col , Button , Container , Label , Input } from "reactstrap"
import ReactPlayer from "react-player"

export const ContainerStyle = styled(Container)`
  width : 90%;
`

export const CartStyle = styled(Card)`
  margin-bottom: 20px;
  border : 0;
`

export const CardHeaderStyle = styled(CardHeader)`
  background: rgba(22, 155, 213, 1);
  height: 3rem;
  &&&{
    border-radius: 10px;
    background: rgba(22, 155, 213, 1);
  }
`

export const CardTitleStyle = styled(CardTitle)`
  margin-top:-1.5rem;
  width : 100%;
`

export const ColTitle = styled(Col)`
  text-align : right;
`

export const CardBodyStyle = styled(CardBody)`
  margin : 0 1.5%;
  padding: 45px 0 0;
  border: 1px solid rgba(22, 155, 213, 1);
  border-top : 0;
  box-shadow: none;
  
`
export const ReactPlayerReading = styled(ReactPlayer)`
  margin : 0 auto 30px;

  & audio{
    outline : none;
  }
`

export const RowQuestion = styled(Row)`
  padding : 0 10%;
  text-align : center;
`

export const InputEnter = styled(Input)`
  border: none;
  width: ${props => props.maxlength * 1.85}ch;
  height: 32px;
  font: 1.7ch consolas, monospace;
  letter-spacing: .2ch;
  display: inline;
  color: orange;
  padding : 0;
  
  &:focus{
    outline: none;
  }
`

export const RowContent = styled(Row)`
  padding : 0 15%;
`

export const LabelAnswer = styled(Label)`
  color : ${props => props.checkAnswer ? "green" : "red"};
  font-weight: bold;
`

export const RowDescription = styled(Row)`
  position : relative;
  // width : 95%;
  padding : 10px 2%;
  border : 1px solid green;
  left: 4%;
`

export const LabelDescription = styled(Label)`
  position: absolute;
  top: -14px;
  background: green;
  color : white;
  padding: 0 2%;
  height: 23px;
  text-align: center;
`

export const ColItemStyle = styled(Col)`
  text-align: center;
`;

export const ButtonQuestionStyle = styled(Button)`
  width: 80%;
  padding: 20px 0;
  margin: 0 auto 30px;
  border-radius: 20px;
  text-align: center;
  background : ${props => props.selected ? "orange" : null};
`

export const ButtonAnswerStyle = styled(Button)`
  width: 80%;
  padding: 20px 0;
  margin: 0 auto 30px;
  border: 2px solid ${props => props.exactly === 1 ? "green" : (props.exactly === -1 ? "red" : null)};
  border-radius: 20px;
  text-align: center;
  background : ${props => props.exactly === 1 ? "green" : (props.exactly === -1 ? "red" : null)};
`

export const RowButton = styled(Row)`
  margin-bottom : 20px;
`

export const ButtonSubmit = styled(Button)`
  width : 15%;
  border-radius : 15px;
  margin : 0 auto;
  padding : 10px 0;
  
  
  &:after{
    content: none;
  }
`

export const ButtonRepeat = styled(Button)`
    background-color: white !important;
    border: 2px solid orange !important;
    color: orange !important;
    font-size: 80%;
    border-radius: 5px !important;
    display: flex;
    height: 20px !important;
    width: 75px;
    margin: 0;
`


