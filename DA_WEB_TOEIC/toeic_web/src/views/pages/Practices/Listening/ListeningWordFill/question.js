import React, {Component} from 'react';
import {Col, PopoverBody, Row, UncontrolledPopover} from "reactstrap"
import * as Styles from "./style"
import * as Icon from "react-feather";
import {ChevronsRight} from "react-feather";
import {bindActionCreators, compose} from "redux";
import {connect} from "react-redux";
import * as styles from "../../PracticeMulti/style";
import {withStyles} from "@material-ui/styles";
import practiceListenMultiActions from "../../../../../redux/actions/practice/listening";
import Spinner from "reactstrap/es/Spinner";
import {history} from "../../../../../history";
import {FormattedMessage} from "react-intl";
import {CODE_PART1_LF, CODE_PART2_LF, CODE_PART3_LF, CODE_PART4_LF} from "../../../../../commons/constants/CONSTANTS";
import {toast} from "react-toastify";
import {showMessage} from "../../../../../commons/utils/convertDataForToast";
import "./test.scss"

class Question extends Component {

  constructor(props) {
    super(props);
    this.state = {
      listAnswerEnteredOne: [],
      listItemCorrectFormat: [],
      checkReview: this.props.checkedReview,
      countNumber: 0,
      play0: false,
      play2: false,
      play3: false,
      play4: false,
      play1: false,
      play: false,
      playing: false,
      played: false,
      playAgain: false,
      indexListenAgain: -1,
      pathFile: "",
      audio: new Audio(""),
      playAfterSubmit: false
    }
  }

  componentDidMount() {
    let listAnswerEnteredTemp = []
    let listItemCorrectFormatTemp = []
    this.setState({
      listAnswerEnteredOne: listAnswerEnteredTemp.fill(""),
      listItemCorrectFormat: listItemCorrectFormatTemp.fill(false)
    })
  }

  onSubmitQuestion = (arrNumberInput) => {
    this.onPausing();
    if (!this.state.played) {
      toast.error(showMessage("audio.not.played"));
    } else {
      const {listeningActions} = this.props;
      const {getResultQuestionOfListeningWordFill} = listeningActions;
      let listAnswerEnteredOneTemp = this.state.listAnswerEnteredOne;

      let dem = 0;
      for (let i = 0; i < arrNumberInput.length; i++) {
        if (listAnswerEnteredOneTemp[i]) {
          for (let j = 0; j < listAnswerEnteredOneTemp[i].length; j++) {
            if (listAnswerEnteredOneTemp[i][j] === "" || listAnswerEnteredOneTemp[i][j] === undefined) {
              toast.error(showMessage("not.complete.answers"))
              dem++;
              break;
            }
          }
        }
        if (arrNumberInput[i] !== 0 && (listAnswerEnteredOneTemp[i] && listAnswerEnteredOneTemp[i].length !== arrNumberInput[i]
          || listAnswerEnteredOneTemp[i] === undefined)) {
          toast.error(showMessage("not.complete.answers"))
          dem++;
          break;
        }
      }
      if (dem === 0) {
        this.setState({
          listAnswerEnteredOne: listAnswerEnteredOneTemp,
          playing: false
        })
        getResultQuestionOfListeningWordFill(this.props.question.id, this.state.listAnswerEnteredOne);
      }
    }
  }


  ref = player => {
    this.player = player
  }


  next = () => {
    if (this.props.viewHistory) {
      history.push("/pages/aggregatedResults");
    } else if (this.props.index + 1 === this.props.listQuestionWordFill.data.length || this.state.checkReview) {
      history.push("/pages/aggregatedResults");
    } else {
      this.props.next();
    }
  }

  cutStartTime(startTime) {
    let st = [];
    startTime.split("|").map((a, index) => {
      if (a.trim() !== "") {
        st.push(a)
      }
    })
    return st;
  }

  onPlaying = () => {
    // 
    // if(this.state.pathFile !== ""){
    //   this.state.audio.pause();
    //   this.setState({
    //     pathFile: "",
    //     audio: new Audio(""),
    //     indexListenAgain: -1
    //   })
    // }
    this.setState({
      play: true,
      played: true,
      play0: false,
      play1: false,
      play2: false,
      play3: false,
    })
  }

  onPlayAfterSubmit = () => {
    if (this.state.pathFile !== "") {
      this.state.audio.pause();
      this.setState({
        pathFile: "",
        audio: new Audio(""),
        indexListenAgain: -1
      })
    }
    this.setState({
      playAfterSubmit: true
    })
  }

  onPauseAfterSubmit = () => {
    this.setState({
      playAfterSubmit: false
    })
  }

  onPausing = () => {
    this.setState({
      play: false
    })
  }

  loadingComponent = (
    <div style={{
      position: 'absolute',
      zIndex: 110,
      top: 0,
      left: 0,
      width: '100%',
      height: '100%',
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      background: 'rgba(255,255,255,0.8)'
    }}>
      <Spinner color="primary" className="reload-spinner"/>
    </div>
  );


  playingListening = (question, indexQuestion) => {
    if (this.state.pathFile !== "") {
      this.state.audio.pause();
      this.setState({
        pathFile: "",
        audio: null
      })
    }
    this.setState({
      playAfterSubmit: false
    })
    if (this.props.part === CODE_PART1_LF) {
      if (question.pathFileQuesA || question.pathFileQuesB || question.pathFileQuesC || question.pathFileQuesD) {
        if (indexQuestion === 0) {
          this.setState({
            indexListenAgain: indexQuestion,
            play0: true,
            play1: false,
            play2: false,
            play3: false,
            pathFile: question.pathFileQuesA
          }, () => {
            this.state.play0 ? this.InitAudio(question.pathFileQuesA).play() : this.InitAudio(question.pathFileQuesA).pause();
          })
        } else if (indexQuestion === 1) {
          this.setState({
            indexListenAgain: indexQuestion,
            play0: false,
            play1: true,
            play2: false,
            play3: false,
            pathFile: question.pathFileQuesB
          }, () => {
            this.state.play1 ? this.InitAudio(question.pathFileQuesB).play() : this.InitAudio(question.pathFileQuesB).pause();
          })
        } else if (indexQuestion === 2) {
          this.setState({
            indexListenAgain: indexQuestion,
            play0: false,
            play1: false,
            play2: true,
            play3: false,
            pathFile: question.pathFileQuesC
          }, () => {
            this.state.play2 ? this.InitAudio(question.pathFileQuesC).play() : this.InitAudio(question.pathFileQuesC).pause();
          })
        } else if (indexQuestion === 3) {
          this.setState({
            indexListenAgain: indexQuestion,
            play0: false,
            play1: false,
            play2: false,
            play3: true,
            pathFile: question.pathFileQuesD
          }, () => {
            this.state.play3 ? this.InitAudio(question.pathFileQuesD).play() : this.InitAudio(question.pathFileQuesD).pause();
          })
        }
      }
    } else if (this.props.part === CODE_PART2_LF) {
      if (question.pathFileQuesA || question.pathFileQuesB || question.pathFileQuesC || question.pathFileQuesD || question.pathFileQues) {
        if (indexQuestion === 0) {
          this.setState({
            indexListenAgain: indexQuestion,
            play0: true,
            play1: false,
            play2: false,
            play3: false,
            pathFile: question.pathFileQues
          }, () => {
            this.state.play0 ? this.InitAudio(question.pathFileQues).play() : this.InitAudio(question.pathFileQues).pause();
          })
        } else if (indexQuestion === 1) {
          this.setState({
            indexListenAgain: indexQuestion,
            play0: false,
            play1: true,
            play2: false,
            play3: false,
            pathFile: question.pathFileQuesA
          }, () => {
            this.state.play1 ? this.InitAudio(question.pathFileQuesA).play() : this.InitAudio(question.pathFileQuesA).pause();
          })
        } else if (indexQuestion === 2) {
          this.setState({
            indexListenAgain: indexQuestion,
            play0: false,
            play1: false,
            play2: true,
            play3: false,
            pathFile: question.pathFileQuesB
          }, () => {
            this.state.play2 ? this.InitAudio(question.pathFileQuesB).play() : this.InitAudio(question.pathFileQuesB).pause();
          })
        } else if (indexQuestion === 3) {
          this.setState({
            indexListenAgain: indexQuestion,
            play0: false,
            play1: false,
            play2: false,
            play3: true,
            pathFile: question.pathFileQuesC
          }, () => {
            this.state.play3 ? this.InitAudio(question.pathFileQuesC).play() : this.InitAudio(question.pathFileQuesC).pause();
          })
        }
      }
    }
  }

  InitAudio = (url) => {
    let audio = new Audio(url)
    this.setState({
      audio: audio
    })
    if (this.state.pathFile === url) {
      audio.onended = () => {
        this.setState({
          indexListenAgain: -1
        })
      }
    }
    return audio;
  }

  renderUnderline = length => {
    let xhtml = "";
    for (var i = 0; i < length; i++) {
      xhtml += "_"
    }
    return xhtml;
  }

  render() {
    const {resultQuestionWordFill, listQuestionAnswer} = this.props;
    let questionProps = this.props.question

    let arrQuestion = [];
    let arrTemp = [];
    let arrOfCharacters = [];
    let arrItemQuestion = [];
    let arrNumberInput = [];


    let arrQues = questionProps.name.split('\n');
    const filteredItems = arrQues.filter(item => item.trim() !== "")


    filteredItems.map((value, i) => {
      if (value.trim() !== "") {
        arrQuestion = value.split("<");
        arrTemp[i] = [];
        arrOfCharacters[i] = [];
        arrItemQuestion[i] = [];
        let itemHead = arrQuestion.shift();
        
        arrQuestion.map((item, index) => {
          // console.log(item.split(">"))
          let s = item.split(">")[0].trim();
          let dem = 0;
          if (/\s/.test(s) && dem === 0) {
            let s1 = s.split(" ")[0];
            let s2 = s.split(" ")[1];
            arrOfCharacters[i].push(s1.length)
            arrOfCharacters[i].push(s2.length)
            arrItemQuestion[i].push(" ")
            arrTemp[i].push([s1, ""])
            arrTemp[i].push([s2, item.split(">")[1]])
            arrItemQuestion[i].push(item.split(">")[1])
          } else {
            arrTemp[i].push(item.split(">"))
            arrOfCharacters[i].push(arrTemp[i][arrTemp[i].length - 1][0].length)
            arrItemQuestion[i].push(arrTemp[i][arrTemp[i].length - 1][1])
          }
        })
        if (this.props.part === CODE_PART1_LF) {
          if (i === 0) {
            itemHead = ("(A) ") + itemHead
          } else if (i === 1) {
            itemHead = ("(B) ") + itemHead
          } else if (i === 2) {
            itemHead = ("(C) ") + itemHead
          } else {
            itemHead = ("(D) ") + itemHead
          }
        } else if (this.props.part === CODE_PART2_LF) {
          if (i === 1) {
            itemHead = ("(A) ") + itemHead
          } else if (i === 2) {
            itemHead = ("(B) ") + itemHead
          } else if (i === 3) {
            itemHead = ("(C) ") + itemHead
          }
        }
        arrItemQuestion[i].unshift(itemHead)

        arrNumberInput.push(arrItemQuestion[i].length - 1)
      }
    })


    let formatInput = (len) => {
      let result = " ";
      for (let i = 0; i < len; i++) {
        result += "_";
      }
      return result;
    }

    let formatInputWidth = (len) => {
      let result = 0;
      for (let i = 0; i < len; i++) {
        result = result + 0.5;
      }
      let l = parseInt(len) + result;
      let result2 = String(l);
      return result2;
    }

    let onEnterAnswer = (answer, indexAnswer, i, number) => {
      if (answer.length < number) {
        toast.error(showMessage("enter.enough.characters"))
      } else {
        let newlistAnswerEnteredOne = this.state.listAnswerEnteredOne
        if (newlistAnswerEnteredOne.length !== 0 && newlistAnswerEnteredOne[i]) {
          newlistAnswerEnteredOne[i][indexAnswer] = answer;
        } else {
          newlistAnswerEnteredOne[i] = [];
          newlistAnswerEnteredOne[i][indexAnswer] = answer;
        }
        this.setState({
          listAnswerEnteredOne: newlistAnswerEnteredOne,
        })
      }
    }

    let onChangeInput = (e) => {
      if (/\s/.test(e.target.value)) {
        toast.warn(showMessage("not.enter.spaces"));
        e.target.value = e.target.value.replace(/\s/g, '')
      }
    }

    let question = arrItemQuestion.map((itemQues, i) => {
      let smallItem = itemQues.map((item, index) => {
        return (
          <>
            {/*<style>{`*/}
            {/*#inputCharacter {*/}
            {/*border: none !important;*/}
            {/*padding: 0 !important;*/}
            {/*// width: auto;*/}
            {/*background: repeating-linear-gradient(90deg, dimgrey 0, dimgrey 1ch, transparent 0, transparent 1.5ch) 0 100%/100% 2px no-repeat !important;*/}
            {/*font: 2.5ch droid sans mono, consolas, monospace !important;*/}
            {/*letter-spacing: 0.5ch;*/}
            {/*}*/}
            {/*#inputCharacter:focus {*/}
            {/*outline: none !important;*/}
            {/*color: dodgerblue !important;*/}
            {/*}*/}
            {/*#inputCharacter:hover{*/}
            {/*color:red !important;*/}
            {/*border: red !important;*/}
            {/*}*/}

            {/*`}</style>*/}
            <span key={index}>{item}
              {/*String(parseInt(arrOfCharacters[i][index]) + 1) */}
              <input type="text"
                     maxLength={arrOfCharacters[i][index] ? arrOfCharacters[i][index] : 0}
                     hidden={index < arrOfCharacters[i].length ? false : true}
                     className={i + index}
                     id="inputCharacter"
                     style={{
                       width: "" + (arrOfCharacters[i][index]) * 1.5 + "ch",
                     }}
                     onChange={onChangeInput}
                     onBlur={(e) => onEnterAnswer(e.target.value, index, i, arrOfCharacters[i][index])}
                // size={arrOfCharacters[i][index] ? arrOfCharacters[i][index] : 0}
              >
                      </input>
              {/*{index > arrOfCharacters.length ? item : null}*/}
              </span>
          </>)
      })
      return (
        <div style={{marginTop: this.props.part === CODE_PART1_LF || this.props.part === CODE_PART2_LF ? '2%' : ''}}>
          {smallItem}<br/>
        </div>);
    })

    let cutAnswerResult = (data) => {
      
      const {usFill} = this.props;
      let answerResult = data.toLowerCase().split("|");
      let answer = [];
      
      for (let i = 0; i < answerResult.length; i++) {
        if (answerResult[i].indexOf(" ") != -1) {
          let arrTemp = answerResult[i].split(" ");
          answer.push(arrTemp[0])
          answer.push(arrTemp[1])
        } else {
          answer.push(answerResult[i])
        }
      }
      let result = [], lst;
      if (usFill[this.props.index] && usFill[this.props.index].fill) {
        for (let i = 0; i < usFill[this.props.index].fill.length; i++) {
          if (usFill[this.props.index].fill[i] === undefined || usFill[this.props.index].fill[i] === null) {
            lst = [];
          }
          if (usFill[this.props.index].fill[i] !== [] && usFill[this.props.index].fill[i]) {
            lst = answer.splice(0, usFill[this.props.index].fill[i].length)
          }
          result.push(lst);
        }
      }
      return result;
    }

    let setClassNameAnswer = (i, index) => {
      
      if (this.props.usFill && this.props.usFill[this.props.index] && this.props.usFill[this.props.index].fill
        && this.props.usFill[this.props.index].fill[i] && this.props.usFill[this.props.index].fill[i][index] &&
        this.props.listQuestionAnswer && this.props.listQuestionAnswer[this.props.index]
        && this.props.listQuestionAnswer[this.props.index].lstAnswerCut && this.props.listQuestionAnswer[this.props.index].lstAnswerCut[i][index] &&
        this.props.usFill[this.props.index].fill[i][index].toUpperCase() === this.props.listQuestionAnswer[this.props.index].lstAnswerCut[i][index].toUpperCase()) {
        return 'success';
      } else {
        return 'danger';
      }
    }

    let renderIcon = (i) => {
      let xhtml = null;
      if (this.props.lstIndex && this.props.lstIndex[this.props.index] && this.props.lstIndex[this.props.index].correct &&
        this.props.lstIndex[this.props.index].correct.length > 0
        && this.props.lstIndex[this.props.index].correct.indexOf(i) !== -1) {
        xhtml = <Icon.Check size={25} style={{color: 'green'}}/>
      } else if (this.props.lstIndex && this.props.lstIndex[this.props.index] && this.props.lstIndex[this.props.index].incorrect &&
        this.props.lstIndex[this.props.index].incorrect.length > 0 &&
        this.props.lstIndex[this.props.index].incorrect.indexOf(i) !== -1) {
        xhtml = <Icon.X size={25} style={{color: '#b21717'}}/>
      }
      return xhtml;
    }

    let answer = arrItemQuestion.map((itemQues, i) => {
      let smallItem = itemQues.map((item, index) => {
        return <span key={index} style={{fontSize: '15px'}}>{item}
          <label
            className={setClassNameAnswer(i, index)}
            style={{fontWeight: 'bold', fontSize: '16px'}}>
                    {this.props.usFill && this.props.usFill[this.props.index] && this.props.usFill[this.props.index].fill
                    && this.props.usFill[this.props.index].fill[i] ?
                      this.props.usFill[this.props.index].fill[i][index] : ''}
                  </label></span>
      })
      return (<div className="mt-1">
        <Row>
          <Col xs={1} style={{marginRight: '0px'}}>
            {
              renderIcon(i)
            }
          </Col>
          <Col xs={11}>
            <div style={{float: 'left', width: '100%'}}>{smallItem}

              <Styles.ButtonRepeat outline
                                   style={{padding: '0 1%', paddingRight: '2%', display: 'inline-flex', width: 'auto'}}
                                   id='btnHighlightRepeat'
                                   onClick={() => this.playingListening(listQuestionAnswer ? listQuestionAnswer[this.props.index] : {}, i)}>
                {this.state.indexListenAgain === i
                  ?
                  <Icon.Volume1 size={12} style={{marginTop: '2.5px', marginLeft: '5px', marginRight: '5px'}}/>
                  :
                  <Icon.Play size={11} style={{marginTop: '2.5px', marginLeft: '5px', marginRight: '5px'}}/>
                }
                <p style={{marginTop: '-2px'}}><FormattedMessage id="listenAgain" tagName="data"/></p>

              </Styles.ButtonRepeat>

              <br/>
            </div>
          </Col>
        </Row>
      </div>);
    })

    let transcript = arrItemQuestion.map((itemQues, i) => {
      let smallItem = itemQues.map((item, index) => {
        return <span key={index} style={{fontSize: '15px'}}>{item}
          <label style={{fontWeight: 'bold', color: 'green', fontSize: '15px'}}>
                    {listQuestionAnswer && listQuestionAnswer[this.props.index] ?
                      cutAnswerResult(listQuestionAnswer[this.props.index].answer)[i] ?
                        cutAnswerResult(listQuestionAnswer[this.props.index].answer)[i][index] : '' : ''}
                  </label></span>
      })
      return (<div className="mt-1">
        {smallItem}
        {
          this.props.part === CODE_PART2_LF ?
            <>
              <button style={{border: 'none', backgroundColor: 'white', padding: '0'}}
                      id={"a" + (listQuestionAnswer && listQuestionAnswer[this.props.index] ? listQuestionAnswer[this.props.index].id : '') + i}
                      className="mr-1 mb-1">
                <Icon.AlertCircle size={20}/>
              </button>
              <UncontrolledPopover trigger="focus" placement="bottom"
                                   target={"a" + (listQuestionAnswer && listQuestionAnswer[this.props.index] ? listQuestionAnswer[this.props.index].id : '') + i}>
                <PopoverBody style={{borderColor: 'green !important', color: 'white', backgroundColor: 'green'}}>
                  <FormattedMessage
                    id={"translate"}/>: {i === 0 ? (listQuestionAnswer && listQuestionAnswer[this.props.index] ? listQuestionAnswer[this.props.index].translatingQuestion : '') :
                  (i === 1 ? (listQuestionAnswer && listQuestionAnswer[this.props.index] ? listQuestionAnswer[this.props.index].translatingQuesA : '') :
                    (i === 2 ? (listQuestionAnswer && listQuestionAnswer[this.props.index] ? listQuestionAnswer[this.props.index].translatingQuesB : '') :
                      (listQuestionAnswer && listQuestionAnswer[this.props.index] ? listQuestionAnswer[this.props.index].translatingQuesC : '')))}
                </PopoverBody>
              </UncontrolledPopover>
            </>
            :
            <>
              <button style={{border: 'none', backgroundColor: 'white', padding: '0'}}
                      id={"a" + (listQuestionAnswer && listQuestionAnswer[this.props.index] ? listQuestionAnswer[this.props.index].id : '') + i}
                      className="mr-1 mb-1">
                <Icon.AlertCircle size={20}/>
              </button>
              <UncontrolledPopover trigger="focus" placement="bottom"
                                   target={"a" + (listQuestionAnswer && listQuestionAnswer[this.props.index] ? listQuestionAnswer[this.props.index].id : '') + i}>
                <PopoverBody style={{borderColor: 'green !important', color: 'white', backgroundColor: 'green'}}>
                  <FormattedMessage
                    id={"translate"}/>: {i === 0 ? (listQuestionAnswer && listQuestionAnswer[this.props.index] ? listQuestionAnswer[this.props.index].translatingQuesA : '') :
                  (i === 1 ? (listQuestionAnswer && listQuestionAnswer[this.props.index] ? listQuestionAnswer[this.props.index].translatingQuesB : '') :
                    (i === 2 ? (listQuestionAnswer && listQuestionAnswer[this.props.index] ? listQuestionAnswer[this.props.index].translatingQuesC : '') :
                      (listQuestionAnswer && listQuestionAnswer[this.props.index] ? listQuestionAnswer[this.props.index].translatingQuesD : '')))}
                </PopoverBody>
              </UncontrolledPopover>
            </>

        }
        <br/>
      </div>);
    })

    let getUrlSound = (url1, url2, urlCategory1, urlCategory2) => {
      if ((urlCategory1 !== null || urlCategory2 !== null) && url1 === null && url2 === null) {
        if (urlCategory1.indexOf('.mp3') !== -1) {
          return urlCategory1;
        } else if (urlCategory2.indexOf('.mp3') !== -1) {
          return urlCategory2;
        }
      } else if ((url1 !== null || url2 !== null) && urlCategory1 === null && urlCategory2 === null) {
        if (url1.indexOf('.mp3') !== -1) {
          return url1;
        } else if (url2.indexOf('.mp3') !== -1) {
          return url2;
        }
      }
    }

    let getUrlImage = (url1, url2) => {
      if (url1.indexOf('.jpg') !== -1) {
        return url1;
      } else if (url2.indexOf('.jpg') !== -1) {
        return url2;
      }
    }

    return (
      <>
        <style>{`

              #btnHighLight:focus-within {
                border: 2px solid black !important;
                text-shadow: 2px 2px 5px gray;
              }

               #btnHighlightRepeat:focus-within {
                border: 2px solid black !important;
              }

              #btnHighLight{
                background-color: #02a1ed !important;
              }
            `}</style>
        <Styles.CartStyle>
          <Styles.CardHeaderStyle>
            <Styles.CardTitleStyle>
              <Row>
                <Col xs="6" sm="6" md="6" lg="6"
                     xl="6" style={{color: 'white', fontWeight: '700', fontSize: '1.8rem'}}><FormattedMessage
                  id={"question"}/> {this.props.index + 1}/
                  {
                    this.props.viewHistory ? this.props.listQuestionWordFill.length :
                      this.props.listQuestionWordFill.data.length
                  }</Col>
              </Row>
            </Styles.CardTitleStyle>
          </Styles.CardHeaderStyle>
          <Styles.CardBodyStyle>
            {this.props.isSubmitWordFillTemp === true ? this.loadingComponent : null}
            <Row>
              {
                this.props.isSubmit ?
                  <Styles.ReactPlayerReading
                    ref={this.ref}
                    // url={ questionProps.pathFile1 ?  questionProps.pathFile2 ? getUrlSound(questionProps.pathFile1,questionProps.pathFile2)
                    //   : getUrlSound(questionProps.pathFile1,null) : ""}
                    url={questionProps.pathFileCategory1 || questionProps.pathFileCategory2 ? getUrlSound(null, null, questionProps.pathFileCategory1, questionProps.pathFileCategory2) :
                      questionProps.pathFile1 || questionProps.pathFile2 ?
                        getUrlSound(questionProps.pathFile1, questionProps.pathFile2, null, null) : ''}
                    playing={this.state.playAfterSubmit}
                    controls={this.props.isSubmit ? true : false}
                    onPlay={this.onPlayAfterSubmit}
                    onPause={this.onPauseAfterSubmit}
                    config={{
                      file: {
                        attributes: {
                          controlsList: 'nodownload',
                        }
                      }
                    }}
                    width="300px"
                    height="40px"
                    style={{marginLeft: '37%'}}
                  ></Styles.ReactPlayerReading> : null
              }
              <Styles.ReactPlayerReading
                ref={this.ref}
                // url={ questionProps.pathFile1 ?  questionProps.pathFile2 ? getUrlSound(questionProps.pathFile1,questionProps.pathFile2)
                //   : getUrlSound(questionProps.pathFile1,null) : ""}
                url={questionProps.pathFileCategory1 || questionProps.pathFileCategory2 ? getUrlSound(null, null, questionProps.pathFileCategory1, questionProps.pathFileCategory2) :
                  questionProps.pathFile1 || questionProps.pathFile2 ?
                    getUrlSound(questionProps.pathFile1, questionProps.pathFile2, null, null) : ''}
                playing={this.state.play}
                controls={this.props.isSubmit ? false : true}
                onPlay={this.onPlaying}
                onPause={this.onPausing}
                config={{
                  file: {
                    attributes: {
                      controlsList: 'nodownload',
                    }
                  }
                }}
                width="300px"
                height="40px"
              ></Styles.ReactPlayerReading>
            </Row>
            {
              this.props.isSubmit || this.state.checkReview
                // && this.props.index+1 < this.props.listQuestionWordFill.data.length
                ?
                <ChevronsRight size={50}
                               style={{marginTop: this.props.part === CODE_PART4_LF ? '10%' : '15%', cursor: 'pointer'}}
                               className="float-right"
                               onClick={() => this.next()}/> : null
            }
            <Styles.RowQuestion
              style={{padding: '0', height: this.props.part === CODE_PART1_LF ? '600px !important' : ''}}>
              {this.props.part === CODE_PART1_LF ?
                <Row style={{width: '100%', height: '100%'}}>
                  <Col xs={6} style={{paddingLeft: '4%'}}>
                    <img
                      src={questionProps.pathFile1 && questionProps.pathFile2 ? getUrlImage(questionProps.pathFile1, questionProps.pathFile2) : ""}
                      style={{width: '100%', height: '70%'}}/>
                  </Col>
                  <Col xs={6} className="text-left ml-0">
                    {
                      this.props.isSubmit || this.state.checkReview ? answer : question
                    }
                  </Col>
                </Row>
                :
                <Col className="text-left"
                     style={{
                       margin: this.props.part === CODE_PART3_LF || this.props.part === CODE_PART4_LF ? '0 5%' : '0 30%',
                       marginRight: this.props.part === CODE_PART4_LF || this.props.part === CODE_PART3_LF ? '0 5%' : ' ',
                       lineHeight: this.props.part === CODE_PART3_LF || this.props.part === CODE_PART4_LF ? '2rem' : ''
                     }}>
                  {
                    this.props.isSubmit || this.state.checkReview ? answer : question
                  }
                </Col>
              }
            </Styles.RowQuestion>
            <div className="container">
              {

                this.props.isSubmit || this.state.checkReview ?
                  <Styles.RowDescription
                    style={{
                      marginTop: this.props.part === CODE_PART2_LF || this.props.part === CODE_PART3_LF || this.props.part === CODE_PART4_LF ? '5%' : '',
                      left: "0%"
                    }}>
                    <Styles.LabelDescription>Transcript</Styles.LabelDescription>
                    <p style={{fontSize: '15px', whiteSpace: 'pre-line'}}>{transcript}</p>
                  </Styles.RowDescription>
                  : null
              }
            </div>
            <Styles.RowButton>
              {
                !this.props.isSubmit ?
                  (
                    <button className="btn btn-info"
                            onClick={() => this.onSubmitQuestion(arrNumberInput)}
                            style={{
                              marginLeft: '45%', marginRight: '40%',
                              marginTop: this.props.part === CODE_PART3_LF || this.props.part === CODE_PART2_LF || this.props.part === CODE_PART4_LF ? '8%' : ''
                            }}
                            id="btnHighLight"
                    >
                      <FormattedMessage id="submit" tagName="data"/>
                      {/*nộp bài*/}
                    </button>
                    // <Styles.ButtonSubmit color="primary" onClick={()=>this.onSubmitQuestion(arrNumberInput)}
                    //                      style={{marginLeft: '45%', marginRight: '40%', marginTop:this.props.part === CODE_PART3_LF || this.props.part === CODE_PART2_LF ? '8%' : ''}} id="btnHighLight">
                    //   <FormattedMessage id="submit" tagName="data" /></Styles.ButtonSubmit>
                  ) : null
              }
            </Styles.RowButton>
          </Styles.CardBodyStyle>
        </Styles.CartStyle>
      </>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  listeningActions: bindActionCreators(practiceListenMultiActions, dispatch)
})

const mapStateToProps = (state) => {
  return {
    listQuestionWordFill: state.multiListeningReducer.listQuestionWordFill,
    resultQuestionWordFill: state.multiListeningReducer.resultQuestionWordFill,
    numberCorrectAnswer: state.multiListeningReducer.numberCorrectAnswer,
    isSubmitWordFillTemp: state.multiListeningReducer.isSubmitWordFill,
    partId: state.multiListeningReducer.partId,
    usFill: state.multiListeningReducer.userFill,
    checkedReview: state.multiListeningReducer.checkedReview,
    listQuestionAnswer: state.multiListeningReducer.listQuestionAnswer,
    lstIndex: state.multiListeningReducer.lstIndex,
    lstAnswerCut: state.multiListeningReducer.lstAnswerCut,
    part: state.multiListeningReducer.part,
    viewHistory: state.multiListeningReducer.viewHistory

  }
}
const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
)

export default compose(withStyles(styles.style), withConnect)(Question);

