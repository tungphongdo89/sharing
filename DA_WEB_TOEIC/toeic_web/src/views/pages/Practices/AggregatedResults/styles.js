export const styles = () => ({
    table:
      {
        borderCollapse: "collapse",
        width: "100%",
        "&&>tr, td, th": {
          border: "1px solid #999",
          padding: "0.5rem",
        }
      },

  }
)

