import React from "react"
import {bindActionCreators, compose} from 'redux';
import {connect} from "react-redux"
import {Card, Col, CardTitle, Row, Spinner} from 'reactstrap';
import CardHeader from "reactstrap/es/CardHeader";
import CardBody from "reactstrap/es/CardBody";
import { Button } from 'reactstrap';
import {styles} from "./styles";
import {withStyles} from "@material-ui/core";
import {history} from "../../../../history";
import readingActions from "../../../../redux/actions/practice/reading";
import practiceListenMultiActions from "../../../../redux/actions/practice/listening";
import historyPracticesAction from "../../../../redux/actions/history-practices"
import {CODE_PART1,CODE_PART2, CODE_PART3, CODE_PART4, CODE_PART5, CODE_PART6,CODE_PART8,
  CODE_PART7, CODE_PART1_LF,CODE_PART2_LF, CODE_PART3_LF, CODE_PART4_LF} from "../../../../commons/constants/CONSTANTS";
import historyPracticseReducer from "../../../../redux/reducers/historyPractices/historyPracticesReducer";
import {FormattedMessage} from "react-intl";
import {showMessage} from "../../../../commons/utils/convertDataForToast";
import {ArrowLeft} from "react-feather";
import historyTestAction from "../../../../redux/actions/history-test";

class AggregatedResults extends React.Component {
  state = {
    currentScore: 0,
    totalScore: 0
  }

  componentDidMount() {
    const element = document.getElementById("topGridResult");
    element.scrollIntoView()
    const {listQuestionAnswerReduce,listQuestionAndAnswerReduce, readingActions } = this.props;
    let sum =0;
    let total=0;
    let totalScore = 0
    if(listQuestionAnswerReduce.length > 0){
      for(let i=0; i<listQuestionAnswerReduce.length; i++){
        if(listQuestionAnswerReduce[i].correct === true){
          sum += listQuestionAnswerReduce[i].score;
        }
        total += listQuestionAnswerReduce[i].score;
      }
    } else if(listQuestionAndAnswerReduce.length > 0){
      for(let i=0; i<listQuestionAndAnswerReduce.length; i++){
        total += listQuestionAndAnswerReduce[i].totalSubCorrect;
        totalScore = listQuestionAndAnswerReduce[i].totalScore;
      }
    }

    this.setState({
      currentScore: total,
      totalScore: totalScore
    })

  }

  onView = (index) =>{
    debugger
    const {readingActions,listQuestionAndAnswerReduce,listeningActions,listQuestionAnswerReduce} = this.props;
    const {getIndex, updateCheckReview} = readingActions;
    const {updateCheckListening, getIndexListen, setTotalCorrectAnswerWhenReview, showAudioWhenReviewPart12} = listeningActions;

    let totalCorrectAnswer = 0;
    for(let i=0;i<listQuestionAnswerReduce.length;i++){
      if(listQuestionAnswerReduce[i].correct){
        totalCorrectAnswer ++;
      }
    }

    updateCheckReview();
    getIndex(index);
    updateCheckListening();
    getIndexListen(index);
    const {saveCurrentSumScore} = readingActions;
    saveCurrentSumScore(this.state.currentScore, this.state.totalScore);
    if(this.props.part === CODE_PART6){
      history.push("/pages/practice-reading-completed-passage");
    } else if(this.props.part === CODE_PART5){
      history.push("/pages/readingWordFill");
    } else if(this.props.part === CODE_PART7){
      readingActions.setTempAnsweredReadingSingleAndDualQuest(listQuestionAndAnswerReduce[index]);
      history.push("/pages/practice-reading-single");
    } else if(this.props.part === CODE_PART8){
      readingActions.setTempAnsweredReadingSingleAndDualQuest(listQuestionAndAnswerReduce[index]);
      history.push("/pages/practice-reading-dual");
    } else if(this.props.part === CODE_PART1_LF || this.props.part === CODE_PART2_LF || this.props.part === CODE_PART3_LF || this.props.part === CODE_PART4_LF ||
      this.props.partListening === CODE_PART1_LF || this.props.partListening === CODE_PART2_LF || this.props.partListening === CODE_PART3_LF || this.props.partListening === CODE_PART4_LF){
      history.push("/pages/listeningWordFill")
    }

    else if(this.props.part === CODE_PART1 || this.props.partListening === "PART1"){
      debugger
      setTotalCorrectAnswerWhenReview(totalCorrectAnswer);
      showAudioWhenReviewPart12();
      listeningActions.setQuestionAnsweredPracticeListening(listQuestionAnswerReduce[index],index);
      history.push("/pages/practice-listening-painting");
    }

    else if(this.props.part === CODE_PART2 || this.props.partListening === "PART2"){
      debugger
      setTotalCorrectAnswerWhenReview(totalCorrectAnswer);
      showAudioWhenReviewPart12();
      listeningActions.setQuestionAnsweredPracticeListening(listQuestionAnswerReduce[index],index);
      history.push("/pages/practice-listening-painting");
    }
  }

  loadingComponent = (
    <div style={{
      position: 'absolute',
      zIndex: 110,
      top: 0,
      left: 0,
      width: '100%',
      height: '100%',
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      background: 'rgba(255,255,255,0.8)',
    }}>
      <Spinner color="primary" className="reload-spinner"/>
    </div>
  );

  saveResult =()=>{
    if(this.props.part === CODE_PART1_LF || this.props.part === CODE_PART2_LF ||
      this.props.part === CODE_PART3_LF || this.props.part === CODE_PART4_LF){
      const {multiListenQuestionAction,listSize,lstNumberCorrect, listQuestionAnswerReduce,historyPracticesAction,readingActions } = this.props;
      let numberCorrect = 0;
      for(let i=0;i<lstNumberCorrect.length;i++){
        if(lstNumberCorrect[i] === listSize[i]){
          numberCorrect++;
        }
      }
      let number = numberCorrect + '/' + listSize.length;
      historyPracticesAction.updateCheckHiddenButtonSaveP3P4();
      readingActions.updateCheckButtonSaveReading();

      const historyListenFill = {};
      historyListenFill.lstQuestionAnswer = this.props.listQuestionAnswerReduce
      historyListenFill.userFill = this.props.userFill
      historyListenFill.topicId = this.props.topicId
      historyListenFill.levelCode = this.props.levelCode
      historyListenFill.part = this.props.part
      historyListenFill.typeCode = this.props.typeCode;
      historyListenFill.userId = this.props.login.userInfoLogin.userId;
      historyListenFill.numberCorrect = number;
      multiListenQuestionAction.createHistoryListenFill(historyListenFill)
    }
    if(this.props.part === CODE_PART5   ){
      const {readingActions,historyPracticesAction,multiListenQuestionAction } = this.props;
      historyPracticesAction.updateCheckHiddenButtonSaveP3P4();
      multiListenQuestionAction.updateCheckButtonSaveListening();

      const historyWordFill = {};
      let number = this.props.totalTrue + '/' + this.props.listQuestionAndAnswerReduce.length
      historyWordFill.lstQuestionAnswer = this.props.listQuestionAndAnswerReduce
      historyWordFill.topicId = this.props.topicIdRead
      historyWordFill.levelCode = this.props.levelCodeRead
      historyWordFill.part = this.props.part
      historyWordFill.typeCode = this.props.typeCodeReading
      historyWordFill.userId = this.props.login.userInfoLogin.userId
      historyWordFill.numberCorrect = number;
      readingActions.createdHistoryReadingWordFill(historyWordFill)
    }
    if(this.props.part === CODE_PART6){
      const {readingActions,historyPracticesAction,multiListenQuestionAction } = this.props;
      historyPracticesAction.updateCheckHiddenButtonSaveP3P4();
      multiListenQuestionAction.updateCheckButtonSaveListening();

      let historyCompletedPassage = {
        lstCategoryReadingChoose: []
      };
      let listQuestionAnswerTemp = [];
      let total = 0;
      if(this.props.listQuestionAndAnswerReduce){
        for(let i = 0; i< this.props.listQuestionAndAnswerReduce.length; i++ ){
          historyCompletedPassage.lstCategoryReadingChoose.length = i + 1;
          listQuestionAnswerTemp = this.props.listQuestionAndAnswerReduce[i];
          historyCompletedPassage.lstCategoryReadingChoose[historyCompletedPassage.lstCategoryReadingChoose.length - 1] = {};
          historyCompletedPassage.lstCategoryReadingChoose[historyCompletedPassage.lstCategoryReadingChoose.length - 1].listQuestionAnswer = listQuestionAnswerTemp;
          total = total + this.props.listQuestionAndAnswerReduce[i].length;
        }
      }
      let number = this.props.numberCorrectAnswer + '/' + total
      historyCompletedPassage.topicId = this.props.topicIdRead
      historyCompletedPassage.levelCode = this.props.levelCodeRead
      historyCompletedPassage.part = this.props.part
      historyCompletedPassage.typeCode = this.props.typeCodeReading
      historyCompletedPassage.userId = this.props.login.userInfoLogin.userId
      historyCompletedPassage.numberCorrect = number;
      readingActions.createHistoryReadingCompletedPassage(historyCompletedPassage)
    }
    if( this.props.part === CODE_PART7  || this.props.part === CODE_PART8){
      const {readingActions,historyPracticesAction,multiListenQuestionAction } = this.props;
      historyPracticesAction.updateCheckHiddenButtonSaveP3P4();
      multiListenQuestionAction.updateCheckButtonSaveListening();
      readingActions.updateCheckButtonSaveReading();
      let historySingleDual = {
        lstCategoryReadingChoose: []
      };
      let listQuestionAnswerTemp = [];
      let totalQues = 0;
      if(this.props.listQuestionAndAnswerReduce){
        historySingleDual.numberCorrect = 0
        for(let i = 0; i< this.props.listQuestionAndAnswerReduce.length; i++ ){
          historySingleDual.lstCategoryReadingChoose.length = i + 1;
          listQuestionAnswerTemp = this.props.listQuestionAndAnswerReduce[i].listQuestion;
          totalQues += listQuestionAnswerTemp.length;
          for(let j =0;j<listQuestionAnswerTemp.length;j++){
            if(listQuestionAnswerTemp[j].correct === true ) 
                historySingleDual.numberCorrect++;
          }
          historySingleDual.lstCategoryReadingChoose[historySingleDual.lstCategoryReadingChoose.length - 1] = {};
          historySingleDual.lstCategoryReadingChoose[historySingleDual.lstCategoryReadingChoose.length - 1].listQuestionAnswerCategory = listQuestionAnswerTemp;
        } 
      } 
      historySingleDual.numberCorrect = historySingleDual.numberCorrect + "/" + totalQues
      historySingleDual.topicId = this.props.topicIdRead
      historySingleDual.levelCode = this.props.levelCodeRead
      historySingleDual.part = this.props.part
      historySingleDual.typeCode = this.props.typeCodeReading
      historySingleDual.userId = this.props.login.userInfoLogin.userId
      readingActions.createHistoryReadingSingleDual(historySingleDual)
    }
    if(this.props.part === "PART1"){

      let historyPractices = {};
      historyPractices.levelCode = this.props.levelCode;
      historyPractices.numberCorrect = 0;
      historyPractices.part = this.props.part;
      historyPractices.topicId = this.props.topicId;
      historyPractices.typeCode = this.props.typeCode;
      historyPractices.userId = this.props.login.userInfoLogin.userId;

      let numberCorrect = 0;
      let listDetailHistoryLisSingle = [];
      for(let i=0;i<this.props.listQuestionAnswerReduce.length;i++){
        if(this.props.listQuestionAnswerReduce[i].correct === true){
          numberCorrect += 1;
        }
        let detailHistoryLisSingle = {};
        detailHistoryLisSingle.questionId = this.props.listQuestionAnswerReduce[i].id;
        detailHistoryLisSingle.parentId = null;
        detailHistoryLisSingle.correct = this.props.listQuestionAnswerReduce[i].correct.toString();
        detailHistoryLisSingle.topicName = this.props.listQuestionAnswerReduce[i].nameTopic;
        detailHistoryLisSingle.userChoose = this.props.listQuestionAnswerReduce[i].userChoose;
        listDetailHistoryLisSingle.push(detailHistoryLisSingle);
      }

      historyPractices.numberCorrect = numberCorrect + "/" + this.props.listQuestionAnswerReduce.length;

      historyPractices.listDetailHistoryLisSingle = listDetailHistoryLisSingle;
      console.log("historyPractices :", historyPractices)

      const {historyPracticesAction, multiListenQuestionAction, readingActions} = this.props;
      readingActions.updateCheckButtonSaveReading();
      multiListenQuestionAction.updateCheckButtonSaveListening();
      const {saveResultHistoryLisSingle} = historyPracticesAction;
      saveResultHistoryLisSingle(historyPractices);
    }

    if(this.props.part === "PART2"){
      let historyPractices = {};
      historyPractices.levelCode = this.props.levelCode;
      historyPractices.numberCorrect = 0;
      historyPractices.part = this.props.part;
      historyPractices.topicId = this.props.topicId;
      historyPractices.typeCode = this.props.typeCode;
      historyPractices.userId = this.props.login.userInfoLogin.userId;

      let numberCorrect = 0;
      let listDetailHistoryLisSingle = [];
      for(let i=0;i<this.props.listQuestionAnswerReduce.length;i++){
        if(this.props.listQuestionAnswerReduce[i].correct === true){
          numberCorrect += 1;
        }
        let detailHistoryLisSingle = {};
        detailHistoryLisSingle.questionId = this.props.listQuestionAnswerReduce[i].id;
        detailHistoryLisSingle.parentId = null;
        detailHistoryLisSingle.correct = this.props.listQuestionAnswerReduce[i].correct.toString();
        detailHistoryLisSingle.topicName = this.props.listQuestionAnswerReduce[i].nameTopic;
        detailHistoryLisSingle.userChoose = this.props.listQuestionAnswerReduce[i].userChoose;
        listDetailHistoryLisSingle.push(detailHistoryLisSingle);
      }

      historyPractices.numberCorrect = numberCorrect + "/" + this.props.listQuestionAnswerReduce.length;

      historyPractices.listDetailHistoryLisSingle = listDetailHistoryLisSingle;
      console.log("historyPractices :", historyPractices)

      const {historyPracticesAction, multiListenQuestionAction, readingActions} = this.props;
      readingActions.updateCheckButtonSaveReading();
      multiListenQuestionAction.updateCheckButtonSaveListening();
      const {saveResultHistoryLisSingle} = historyPracticesAction;
      saveResultHistoryLisSingle(historyPractices);
    }

  }

  backToHistory =()=> {
    let {historyPracticesAction,historyTestAction} = this.props;
    let {changeActiveTab} = historyPracticesAction;
    changeActiveTab("4");
    historyTestAction.updateActiveTabWhenFail("4");
    history.push("/pages/profiles");
  }

  render() {
    const {listQuestionAnswerReduce, listQuestionAndAnswerReduce, lstNumberCorrect, listSize, nameTopicListen,classes} = this.props;

    const loadingComponent = (
      <div style={{
        position: 'absolute',
        zIndex: 110,
        top: 0,
        left: 0,
        width: '100%',
        height: '100%',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        background: 'rgba(255,255,255,0.8)',
      }}>
        <Spinner color="primary" className="reload-spinner"/>
      </div>
    );


    let resultsListenMap = listQuestionAnswerReduce ? listQuestionAnswerReduce.map((result, index) => {
      return (
        <tr>
          <td style={{textAlign:'center'}}>{index+1}</td>
          {this.props.partListening === "PART1" || this.props.partListening === "PART2" ?
            <td style={{textAlign:'center'}}>{result.nameTopic}</td>
            :
            <td style={{textAlign:'center'}}>{nameTopicListen}</td>
          }

          {
             this.props.partListening === "PART1" || this.props.partListening === "PART2" ?
              <td style={{textAlign: 'center', fontWeight: 'bold', color: result.correct === true? 'lightgreen' : 'red'}}>
                {
                  result.correct === true?
                    <>
                      {/*"Đúng"*/}
                      <FormattedMessage id="correct" />
                    </>
                    :
                    <>
                      {/*"Sai"*/}
                      <FormattedMessage id="incorrect" />
                    </>
                }
              </td>
              :
              <td style={{textAlign: 'center', fontWeight: 'bold',
                color: lstNumberCorrect && listSize && lstNumberCorrect[index] === listSize[index] ? 'green' : 'red'}}>
                {/*{result.correct === true ? result.score : 0}*/}
                {lstNumberCorrect && listSize && lstNumberCorrect[index] === listSize[index] ? <FormattedMessage id="true" tagName="true"/> : <FormattedMessage id="false" tagName="false"/>}
              </td>
          }
          <td style={{textAlign: 'center'}}><button id="btnHighLightLink" color="link" onClick={()=>this.onView(index)}><FormattedMessage id="viewAgain" tagName="viewAgain"/></button></td>
        </tr>
      )
    }): null

    const setStyleColor=(result,index)=>{
      let color='';
      if(this.props.part === CODE_PART6){
        if(this.props.hiddenButtonWhenViewHistory){
          let numberCorrect = 0;
          result.map((ques,i)=>{
            if(ques.indexInCorrect === null){
              numberCorrect++;
            }
          })
          if(numberCorrect === result.length){
            color='green';
          } else color='red';
        } else {
          if(result[result.length - 1] && result[result.length - 1].numberCorrect &&
            result[result.length - 1].numberCorrect === result.length ){
            color='green';
          } else {
            color='red';
          }
        }
      } else if(this.props.part === CODE_PART5){
        if(result.indexInCorrect === null && result.numberSelected <= 3){
          color = "green"
        } else {
          color = "red"
        }
      }
      else {
        if(result.correct === result.numberQuest){
          color='green';
        } else {
          color='red';
        }
      }
      return color;
    }

    let renderKQ =(result, index)=>{
      let xhtml = null;
      if(this.props.part === CODE_PART6  ){
        if(this.props.hiddenButtonWhenViewHistory){
          let numberCorrect = 0;
          result.map((ques,i)=>{
            if(ques.indexInCorrect === null){
              numberCorrect++;
            }
          })
          xhtml = numberCorrect + "/" + result.length
        } else {
          if( result[result.length - 1]){
            xhtml = result[result.length - 1].numberCorrect +"/"+result.length
          }
        }

      } else if (this.props.part === CODE_PART5){
        if(result.indexInCorrect === null && result.numberSelected <= 3){
          xhtml = <FormattedMessage id="true" tagName="true"/>
        } else {
          xhtml = <FormattedMessage id="false" tagName="false"/>
        }
      }
      else if(this.props.part === CODE_PART7 || this.props.part === CODE_PART8) {
        var totalCorrect = 0
        result.listQuestion.map((ques)=>{
          if(ques.correct === true) 
            totalCorrect = totalCorrect+1
        }) 
        xhtml =  totalCorrect   + "/" + result.listQuestion.length 
      }
      return xhtml;
    }

    let resultsReadMap =listQuestionAndAnswerReduce ? listQuestionAndAnswerReduce.map((result, index) => {
      return (
        <tr>
          <td style={{textAlign:'center'}}>{index+1}</td>
          <td style={{textAlign:'center'}}>{this.props.nameTopic ? this.props.nameTopic : ''}</td>
          <td style={{textAlign: 'center',
            fontWeight: 'bold',
            color:setStyleColor(result,index) }}>
            {renderKQ(result,index)} 
            
          {/* { this.props.part === CODE_PART6 ?  this.props.lstCorrect ? this.props.lstCorrect[index].numberCorrect : 0 : result.correct }  */}
             
          {/* {this.props.part === CODE_PART6 ? this.props.lstCorrect ? this.props.lstCorrect[index].numberQues : 0 : result.numberQuest}   */}
          </td>
          <td style={{textAlign: 'center'}}><button id="btnHighLightLink" color="link" onClick={()=>this.onView(index)}><FormattedMessage id="viewAgain" tagName="viewAgain"/></button></td>
        </tr>
      )
    }): null



    return (
      <>
        <style>{`
          #btnHighLight {
            width: 150px;
            border-radius: 5px !important;
            background-color: #00cfe8 !important;
            border-color: #00cfe8 !important;
            padding: 10px 0;
            color: white;
            text-align: center !important;
          }
          #btnHighLight:focus-within {
            border: 1px solid black !important;
            border-radius: 10px;
            text-shadow: 2px 2px 5px #00cfe8 !important;
          }
          #btnHighLightWarning {
            width: 100px;
            border-radius: 5px!important;
            background-color: #ff9f43 !important;
            border-color: #ff9f43 !important;
            padding: 10px 0;
            color: white;
          }
          #btnHighLightWarning:focus-within {
            border: 1px solid black !important;
            border-radius: 10px;
            text-shadow: 2px 2px 5px #ff9f43 !important;
          }
          #btnHighLightLink {
            background-color: white;
            border: none;
            color: #7367f0;
            padding: 0 2px;
          }
          #btnHighLightLink:focus-within {
            border: 1px solid black !important;
          }
          #btnBackHistory{
            margin-left: 2%;
            border-color: 1px solid blue;
            background-color: #7367f0;
            color: white;
            padding: 8.5px 10px;
            border-radius: 5px;
            border: none;
          }
          #btnBackHistory:focus-within {
            border: 1px solid black !important;
          }
        `}</style>
        <div>
          {this.props.hiddenBtnWhenReviewResult === true ?
            <>
              {/*<button id="btnBackHistory" onClick={this.backToHistory}>*/}
                {/*<FormattedMessage id="back" tagName="back"/>*/}
              {/*</button>*/}
              <button id="btnBackHistory" style={{marginLeft: '2%'}} left
                      onClick={this.backToHistory} title={showMessage("back")}>
                <ArrowLeft width="16" height="16"/>
              </button>
            </>

            : null
          }
          <Row>
            <Col sm="1"/>
            <Col sm="10">
              <Card style={{backgroundColor: '#f8f8f8', boxShadow: 'none', margin: '0 12% 4% 12%'}}>
                { this.props.loadingSaveReading || this.props.loadingSaveListening || this.props.loadingGetHistoryListenFill
                  || this.props.loadingGetHistoryReadFill || this.props.loadingGetHistoryReadSingleDual
                  ? this.loadingComponent : null}
                <CardHeader>
                  <CardTitle style={{color: 'black', fontSize: '200%', fontWeight: '600'}}>
                    <FormattedMessage id="aggregated.results" tagName="aggregated.results"/>
                  </CardTitle>
                </CardHeader>
                <CardBody >
                  <table className={classes.table} style={{border: '2px solid gray'}}>
                    <thead style={{backgroundColor: 'white'}}>
                      <tr>
                        <th style={{width:'10%', textAlign:'center'}} ><FormattedMessage id="stt" tagName="stt"/></th>
                        <th style={{textAlign: 'center', width:'20%'}}><FormattedMessage id="topic" tagName="topic"/></th>
                        <th style={{width:'12%', textAlign:'center'}}><FormattedMessage id="result" tagName="result"/></th>
                        <th style={{width:'8%'}}></th>
                      </tr>
                    </thead>
                    <tbody id="topGridResult" style={{backgroundColor: 'white'}}>
                    {
                      this.props.typeCode === '2' || this.props.typeCode === '5' ? resultsListenMap : resultsReadMap
                    }
                    </tbody>
                  </table>
                  {this.props.hiddenBtnWhenReviewResult === false || this.props.hiddenButtonWhenViewHistory === false ?
                    <div className="text-center mt-2">
                      {this.props.loadingSaveResultSingle ? <>{loadingComponent}</> : null}
                      <p style={{fontSize:'1.3rem', fontWeight: 'bold'}}><FormattedMessage id="congratulations.completing.exercise" tagName="congratulations.completing.exercise"/></p>
                      <p style={{fontSize:'1.3rem', fontWeight: 'bold'}}><FormattedMessage id="continue.practice.similar.exercises" tagName="continue.practice.similar.exercises"/></p>
                      <div>
                        {this.props.hiddenBtnSaveResultSingle === false || this.props.checkHiddenButtonListen === false || this.props.checkHiddenButtonReading === false ?
                          <button id="btnHighLightWarning" className="mt-2 mr-2" style={{fontWeight:'700'}}
                                  onClick={this.saveResult}
                          ><FormattedMessage id="save" tagName="save"/></button> : null
                        }
                        <button id="btnHighLight" className="mt-2" style={{fontWeight:'700'}} onClick={()=>{
                          const {readingActions,listeningActions,dataRepracticeReading,
                            multiListenQuestionAction,multiListeningReducer, historyPracticesAction} = this.props;
                          const {topicId,levelCode, nameTopic} = multiListeningReducer
                          const data ={};
                          switch (this.props.part){
                            case CODE_PART1:{
                              if(this.props.typeCode=="2"){
                                const {cleanHiddenBtnBeforeGetLisSingle}= historyPracticesAction;
                                cleanHiddenBtnBeforeGetLisSingle();

                                const {multiListenQuestionAction} = this.props;
                                const {resetCountCheckListeningPractices} = multiListenQuestionAction;
                                resetCountCheckListeningPractices();

                                multiListenQuestionAction.clearTempQuestionAnswer();
                                multiListenQuestionAction.clearTotalCorrect();
                                multiListenQuestionAction.clearAnsweredMultiListenQuest();
                                multiListenQuestionAction.getListListeningQuestionOfMulti(topicId, levelCode)
                                history.push("/pages/practice-listening-painting")
                                break;
                              }
                            }
                            case CODE_PART2:{
                              if(this.props.typeCode=="2"){
                                const {cleanHiddenBtnBeforeGetLisSingle}= historyPracticesAction;
                                cleanHiddenBtnBeforeGetLisSingle();

                                const {multiListenQuestionAction} = this.props;
                                const {resetCountCheckListeningPractices} = multiListenQuestionAction;
                                resetCountCheckListeningPractices();

                                multiListenQuestionAction.getPartPracticeListening(CODE_PART2)
                                multiListenQuestionAction.clearTempQuestionAnswer();
                                multiListenQuestionAction.clearTotalCorrect();
                                multiListenQuestionAction.clearAnsweredMultiListenQuest();
                                multiListenQuestionAction.getListListeningQuestionOfMulti(topicId, levelCode)
                                history.push("/pages/practice-listening-question-answer")
                                break;
                              }
                            }
                            case CODE_PART1_LF:{
                              const {multiListenQuestionAction} = this.props;
                              const {getListQuestionOfListeningWordFill} = multiListenQuestionAction;
                              multiListenQuestionAction.cleanListQuestionOfListeningWordFill();
                              multiListenQuestionAction.cleanResultQuestionOfListeningWordFill();
                              multiListenQuestionAction.cleanNumberCorrectAnswerWordFill();
                              multiListenQuestionAction.cleanListQuestionAndAnswerListening();
                              const lstPart1 ={}
                              lstPart1.topicName = nameTopic;
                              lstPart1.levelCode =levelCode;
                              lstPart1.part = this.props.part
                              getListQuestionOfListeningWordFill(lstPart1);
                              history.push("/pages/listeningWordFill");
                              break;
                            }
                            case CODE_PART2_LF:{
                              const {multiListenQuestionAction} = this.props;
                              const {getListQuestionOfListeningWordFill} = multiListenQuestionAction;
                              multiListenQuestionAction.cleanListQuestionOfListeningWordFill();
                              multiListenQuestionAction.cleanResultQuestionOfListeningWordFill();
                              multiListenQuestionAction.cleanNumberCorrectAnswerWordFill();
                              multiListenQuestionAction.cleanListQuestionAndAnswerListening();
                              const lst ={}
                              lst.topicName = nameTopic;
                              lst.levelCode =levelCode;
                              lst.part = this.props.part
                              getListQuestionOfListeningWordFill(lst);
                              history.push("/pages/listeningWordFill");
                              break;
                            }
                            case CODE_PART3_LF:{
                              const {multiListenQuestionAction} = this.props;
                              const {getListQuestionOfListeningWordFill} = multiListenQuestionAction;
                              multiListenQuestionAction.cleanListQuestionOfListeningWordFill();
                              multiListenQuestionAction.cleanResultQuestionOfListeningWordFill();
                              multiListenQuestionAction.cleanNumberCorrectAnswerWordFill();
                              multiListenQuestionAction.cleanListQuestionAndAnswerListening();
                              const lst ={}
                              lst.topicName = nameTopic;
                              lst.levelCode =levelCode;
                              lst.part = this.props.part
                              getListQuestionOfListeningWordFill(lst);
                              history.push("/pages/listeningWordFill");
                              break;
                            }
                            case CODE_PART4_LF:{
                              const {multiListenQuestionAction} = this.props;
                              const {getListQuestionOfListeningWordFill, getNameTopic} = multiListenQuestionAction;
                              multiListenQuestionAction.cleanListQuestionOfListeningWordFill();
                              multiListenQuestionAction.cleanResultQuestionOfListeningWordFill();
                              multiListenQuestionAction.cleanNumberCorrectAnswerWordFill();
                              multiListenQuestionAction.cleanListQuestionAndAnswerListening();
                              const lst ={}
                              lst.topicName = nameTopic;
                              lst.levelCode =levelCode;
                              lst.part = this.props.part
                              getListQuestionOfListeningWordFill(lst);
                              history.push("/pages/listeningWordFill");
                              break;
                            }
                            case CODE_PART5:{
                              readingActions.cleanListQuestionOfReadingCompletedPassage();
                              readingActions.cleanResultQuestionOfReadingCompletedPassage();
                              readingActions.cleaningListQuestionAndAnswerReadingCompletedPassage();
                              readingActions.cleanNumberCorrectAnswer();
                              readingActions.cleanNumberSelected();
                              readingActions.setValueIndex();
                              readingActions.getPart(CODE_PART5);
                              readingActions.getNameTopicPassage(this.props.nameTopicRead);
                              let quest = {};
                              quest.topicName = this.props.nameTopicRead;
                              quest.levelCode = this.props.levelCodeRead;
                              readingActions.getListQuestionOfReadingWordFill(quest);
                              history.push("/pages/readingWordFill");
                              break
                            }
                            case CODE_PART6:{
                              debugger
                              readingActions.cleanListQuestionOfReadingCompletedPassage();
                              readingActions.cleanResultQuestionOfReadingCompletedPassage();
                              readingActions.cleaningListQuestionAndAnswerReadingCompletedPassage();
                              readingActions.cleanNumberCorrectAnswer();
                              readingActions.setValueIndex();
                              readingActions.getPart(CODE_PART6);
                              readingActions.getNameTopicPassage(this.props.nameTopicRead);
                              let quest = {};
                              quest.topicName = this.props.nameTopicRead;
                              quest.levelCode = this.props.levelCodeRead;
                              readingActions.getListQuestionOfReadingCompletedPassage(quest);
                              history.push("/pages/practice-reading-completed-passage")
                              break
                            }
                            case CODE_PART7:{
                              let quest = {};
                              quest.topicName = this.props.nameTopicRead;
                              readingActions.clearTempReadingSingleAndDualQuestion();
                              readingActions.clearTotalTrueReadingSingleAndDual();
                              readingActions.cleaningListQuestionAndAnswerReadingCompletedPassage();
                              readingActions.getPart(CODE_PART7);
                              readingActions.getNameTopicPassage(quest.topicName);
                              readingActions.getListSingleAndDualReadingQuestion(dataRepracticeReading)
                              history.push("/pages/practice-reading-single")
                              break;
                            }
                            case CODE_PART8:{
                              let quest = {};
                              quest.topicName = this.props.nameTopicRead;
                              readingActions.clearTempReadingSingleAndDualQuestion();
                              readingActions.clearTotalTrueReadingSingleAndDual();
                              readingActions.cleaningListQuestionAndAnswerReadingCompletedPassage();
                              readingActions.getPart(CODE_PART8);
                              readingActions.getNameTopicPassage(quest.topicName);
                              readingActions.getListSingleAndDualReadingQuestion(dataRepracticeReading)
                              history.push("/pages/practice-reading-dual")
                              break;
                            }
                            default:

                          }
                        }}><FormattedMessage id="do.the.same" tagName="do.the.same"/></button>

                      </div>
                    </div> : null
                  }

                </CardBody>
              </Card>
            </Col>
            <Col sm="1" />
          </Row>
        </div>
      </>

    )
  }
}

const mapStateToProps = state => {
  return {
    multiListeningReducer: state.multiListeningReducer,
    listQuestionAnswerReduce: state.multiListeningReducer.listQuestionAnswer,
    typeCode: state.multiListeningReducer.paramValue,
    lstNumberCorrect: state.multiListeningReducer.lstNumberCorrect,
    listSize: state.multiListeningReducer.listSize,
    nameTopicListen: state.multiListeningReducer.nameTopic,
    userFill: state.multiListeningReducer.userFill,
    topicId: state.multiListeningReducer.topicId,
    levelCode: state.multiListeningReducer.levelCode,
    loadingSaveListening: state.multiListeningReducer.loadingSave,
    partListening: state.multiListeningReducer.part,
    hiddenBtnWhenReviewResult: state.multiListeningReducer.hiddenBtnWhenReviewResult,
    checkHiddenButtonListen:state.multiListeningReducer.checkHiddenButtonListen,
    loadingGetHistoryListenFill: state.multiListeningReducer.loadingGetHistoryListenFill,


    listQuestionAndAnswerReduce: state.readingReducer.listQuestionAndAnswerTemp,
    nameTopic: state.readingReducer.nameTopic,
    part: state.readingReducer.part,
    checkedReview: state.readingReducer.checkReview,
    lstCorrect: state.readingReducer.lstCorrect,
    current: state.readingReducer.currentScore,
    sumScore: state.readingReducer.totalScore,
    dataRepracticeReading:state.readingReducer.storeDataRepracticeReading,
    nameTopicRead: state.readingReducer.nameTopic,
    levelCodeRead: state.readingReducer.levelCode,
    topicIdRead: state.readingReducer.topicId,
    typeCodeReading: state.readingReducer.typeCode,
    loadingSaveReading: state.readingReducer.loadingSave,
    totalTrue: state.readingReducer.totalTrue,
    hiddenButtonWhenViewHistory: state.readingReducer.hiddenButtonWhenViewHistory,
    loadingGetHistoryReadFill: state.readingReducer.loadingGetHistoryReadFill,
    numberCorrectAnswer: state.readingReducer.numberCorrectAnswer,
    loadingGetHistoryReadSingleDual : state.readingReducer.loadingGetHistoryReadSingleDual,
    login: state.auth.login,
    loadingSaveResultSingle: state.historyPracticseReducer.loadingSaveResultSingle,
    hiddenBtnSaveResultSingle: state.historyPracticseReducer.hiddenBtnSaveResultSingle,
    checkHiddenButtonReading: state.readingReducer.checkHiddenButtonReading
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    multiListenQuestionAction: bindActionCreators(practiceListenMultiActions,dispatch),
    readingActions : bindActionCreators(readingActions, dispatch),
    listeningActions : bindActionCreators(practiceListenMultiActions, dispatch),
    historyPracticesAction: bindActionCreators(historyPracticesAction, dispatch),
    historyTestAction: bindActionCreators(historyTestAction,dispatch)

  }

}
const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
)

export default compose(withStyles(styles), withConnect)(AggregatedResults);

