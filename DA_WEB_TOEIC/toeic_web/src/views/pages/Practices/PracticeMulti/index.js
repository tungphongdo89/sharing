import React, {Component} from 'react';
import {
  Badge,
  Card, CardBody,
  Row, Col,
  CardTitle,
} from 'reactstrap';
import {withStyles} from "@material-ui/styles";
import {connect} from "react-redux";
import {bindActionCreators, compose} from "redux";
import * as styles from "./style";
import PropTypes from 'prop-types';
import Question from "./question";
import practiceListenMultiActions from "../../../../redux/actions/practice/listening";
import Spinner from "reactstrap/es/Spinner";
import {FormattedMessage} from "react-intl";


class PracticeMulti extends Component {

  constructor(props) {
    super(props);
    this.state = {
      indexQuestion: this.props.indexQuestion,
    }
  }

  componentDidMount() {
    const element = document.getElementById("questionPart");
    element.scrollIntoView()
  }

  // componentDidUpdate(prevProps, prevState, snapshot) {
  //   const {listQuestionPart1} = this.props;
  //   if(prevProps.listQuestionPart1.length === 0){
  //     const element = document.getElementById("questionPart");
  //     element.scrollIntoView()
  //   }
  // }

  nextQuestion = () => {
    debugger
    let index = this.state.indexQuestion;
    const {multiListenQuestionAction} = this.props;
    multiListenQuestionAction.clearTempQuestionAnswer()
    this.setState({indexQuestion: index + 1})
  }
  preQuestion = () => {
    let index = this.state.indexQuestion;
    this.setState({indexQuestion: index - 1})
  }


  render() {
    debugger
    let {classes, listQuestionPart1, multiListeningReducer} = this.props;
    const {questionAnswer, listQuestionAnswer} = multiListeningReducer;
    if(!listQuestionPart1){
      listQuestionPart1 = listQuestionAnswer;
    }

    let questionItemMap = listQuestionPart1 ? listQuestionPart1.map((quest, index) => {
      return <div key={index} style={{position: "relative"}}>
        <Badge style={{backgroundColor: "rgba(22, 155, 213, 1)"}} className="badge-xl block">
          <Row>
            <Col className="text-left" xs={6}>
              <div style={{position: 'relative'}}>
                <div className="titaleModal">
                  {
                    questionAnswer && multiListeningReducer.reviewPractice === true?
                      <strong>
                        {/*Câu */}
                        <FormattedMessage id="question" tagName="data" />{' '}
                        {multiListeningReducer.indexQuestion + 1}/{listQuestionPart1.length}
                      </strong>
                      :
                      <strong>
                        {/*Câu */}
                        <FormattedMessage id="question" tagName="data" />{' '}
                        {index + 1}/{listQuestionPart1.length}
                      </strong>
                  }
                </div>
              </div>
            </Col>
            <Col className="text-right" xs={6} style={{fontSize: "75%", textAlign: "center"}}>
              <div style={{position: 'relative'}}>
                <div className="titaleModal">
                  <strong><i>
                    {/*Số câu đúng*/}
                    <FormattedMessage id="total.correct.answer" tagName="data" />
                    : {multiListeningReducer.totalCorrect}/{listQuestionPart1.length}
                  </i></strong>
                </div>
              </div>
            </Col>
          </Row>
        </Badge>
        <div style={{backgroundColor: 'white'}} className={classes.borderdiv} >
          {
            questionAnswer ?
              <Question
                preQuestion={this.preQuestion}
                nextQuestion={this.nextQuestion}
                quest={questionAnswer}
                index={index}
                totalQuest={listQuestionPart1.length}
              /> :
              <Question
                preQuestion={this.preQuestion}
                nextQuestion={this.nextQuestion}
                quest={quest}
                index={index}
                totalQuest={listQuestionPart1.length}
              />
          }
        </div>
      </div>
    }) : ""

    return (
      <>
        {listQuestionPart1 !== undefined ?
          <div style={{marginLeft: "auto",marginRight:"auto", width: "1000px"}} id="questionPart">
            {
              multiListeningReducer.part === "PART1"?
                <h3 className="text-center" style={{marginBottom: '5%'}}><b>
                  Part I:{' '}
                  {/*Bài nghe mô tả hình ảnh*/}
                  <FormattedMessage id="image.description.listening.part" tagName="data" />
                </b></h3>:""
            }
            {
              multiListeningReducer.part === "PART2"?
                <h3 className="text-center" style={{marginBottom: '5%'}}><b>
                  Part II:{' '}
                  {/*Bài nghe hỏi đáp*/}
                  <FormattedMessage id="question.and.answer.listening.part" tagName="data" />
                </b></h3>:""
            }
            <Card style={{backgroundColor: '#f8f8f8', boxShadow: '0 0 0 0'}}>
              <CardBody>
                {questionItemMap[this.state.indexQuestion]}
                {
                  multiListeningReducer.loading == true ?
                    <div style={{
                      position: 'absolute',
                      zIndex: 110,
                      top: 0,
                      left: 0,
                      width: '100%',
                      height: '100%',
                      display: 'flex',
                      justifyContent: 'center',
                      alignItems: 'center',
                      background: 'rgba(255,255,255,0.8)'
                    }}>
                      <Spinner color="primary" className="reload-spinner"/>
                    </div> : null
                }
              </CardBody>
            </Card>
          </div>
          :
          <div style={{marginLeft: "auto",marginRight:"auto", width: "1000px", height: '800px'}} id="questionPart">
            {
              multiListeningReducer.part === "PART1"?
                <h3 className="text-center" style={{marginBottom: '5%'}}><b>
                  Part I:{' '}
                  {/*Bài nghe mô tả hình ảnh*/}
                  <FormattedMessage id="image.description.listening.part" tagName="data" />
                </b></h3>:""
            }
            {
              multiListeningReducer.part === "PART2"?
                <h3 className="text-center" style={{marginBottom: '5%'}}><b>
                  Part II:{' '}
                  {/*Bài nghe hỏi đáp*/}
                  <FormattedMessage id="question.and.answer.listening.part" tagName="data" />
                </b></h3>:""
            }
            <Card style={{backgroundColor: '#f8f8f8', boxShadow: '0 0 0 0'}}>
              <CardBody>
                {questionItemMap[this.state.indexQuestion]}
                {
                  multiListeningReducer.loading == true ?
                    <div style={{
                      position: 'absolute',
                      zIndex: 110,
                      top: 0,
                      left: 0,
                      width: '100%',
                      height: '100%',
                      display: 'flex',
                      justifyContent: 'center',
                      alignItems: 'center',
                      background: 'rgba(255,255,255,0.8)'
                    }}>
                      <Spinner color="primary" className="reload-spinner"/>
                    </div> : null
                }
              </CardBody>
            </Card>
          </div>
        }
      </>

    );
  }
}

PracticeMulti.propTypes = {
  classes: PropTypes.object
}

const mapDispatchToProps = dispatch => {
  return {
    multiListenQuestionAction: bindActionCreators(practiceListenMultiActions, dispatch)
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    multiListeningReducer: state.multiListeningReducer,
    listQuestionPart1: state.multiListeningReducer.listQuestionPart1,
    indexQuestion: state.multiListeningReducer.indexQuestion
  }
}
const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
)
export default compose(withStyles(styles.style), withConnect)(PracticeMulti);
