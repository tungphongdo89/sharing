import React, {Component} from 'react';
import {Col, Row, Button, Badge} from "reactstrap";
import * as styles from "./style";
import PropTypes from "prop-types";
import {connect} from "react-redux";
import {bindActionCreators, compose} from "redux";
import {withStyles} from "@material-ui/styles";
import {ChevronsRight, ChevronsLeft, Check, X} from 'react-feather';
import practiceListenMultiActions from "../../../../redux/actions/practice/listening";
import Spinner from "reactstrap/es/Spinner";
import {history} from "../../../../history";
import readingReducer from "../../../../redux/reducers/practice/reading/readingReducer";
import {ReactPlayerReading} from "../ListeningToeicConversationPractice/style";
import {FormattedMessage} from "react-intl";
import {showMessage} from "../../../../commons/utils/convertDataForToast";

class Question extends Component {

  constructor() {
    super();
    this.state = {
      playing: false,
      timeAgain: 0,

      showPathFile: true,
      playPathFile: false,
      playQuesA: false,
      playQuesB: false,
      playQuesC: false,
      playQuesD: false
    }
  }

  componentDidMount() {
    console.log(this.props.location)
  }

  ref = player => {
    this.player = player
  }

  playingAgain =(index)=>{
    const {doPracticeAction} = this.props;
    const {hidePathFileWhenListenAgain} = doPracticeAction;
    hidePathFileWhenListenAgain(true)
    debugger
    if(index === -1){
      this.setState({
        playPathFile: false,
        playQuesA: false,
        playQuesB: false,
        playQuesC: false,
        playQuesD: false
      })
    }
    if(index === 0){
      this.setState({
        playPathFile: false,
        playQuesA: true,
        playQuesB: false,
        playQuesC: false,
        playQuesD: false
      })
    }
    if(index === 1){
      this.setState({
        playPathFile: false,
        playQuesA: false,
        playQuesB: true,
        playQuesC: false,
        playQuesD: false
      })
    }
    if(index === 2){
      this.setState({
        playPathFile: false,
        playQuesA: false,
        playQuesB: false,
        playQuesC: true,
        playQuesD: false
      })
    }
    if(index === 3){
      this.setState({
        playPathFile: false,
        playQuesA: false,
        playQuesB: false,
        playQuesC: false,
        playQuesD: true
      })
    }
  }

  onPlaying =()=> {
    debugger
    // const {doPracticeAction} = this.props;
    // const {hidePathFileWhenListenAgain} = doPracticeAction;
    // hidePathFileWhenListenAgain(true)

  }

  onEnding =()=>{
    debugger
    const {doPracticeAction} = this.props;
    const {hidePathFileWhenListenAgain} = doPracticeAction;
    hidePathFileWhenListenAgain(false)
    this.setState({
      playQuesA: false,
      playQuesB: false,
      playQuesC: false,
      playQuesD: false
    })
  }

  render() {
    debugger
    const {quest, classes, totalQuest, index, multiListeningReducer,readingReducer} = this.props;

    return (
      <>
        <style>{`
          #vertical-center {
            margin: 0;
            position: absolute;
            top: 50%;
            -ms-transform: translateY(-50%);
            transform: translateY(-50%);
          }

          #btnHighLight {
            width: 90%;
            border-radius: 5px !important;
            background-color: #00cfe8 !important;
            border-color: #00cfe8 !important;
            padding: 10px 0;
            color: white;
            text-align: center !important;
          }
          #btnHighLightSuccess {
            width: 90%;
            border-radius: 5px!important;
            background-color: #1f9d57 !important;
            border-color: #1f9d57 !important;
            padding: 10px 0;
            color: white;
            padding-left: 74px;
          }
          #btnHighLightDanger {
            width: 90%;
            border-radius: 5px!important;
            background-color: #ea5455 !important;
            border-color: #ea5455 !important;
            padding: 10px 0;
            color: white;
            padding-left: 74px;
          }
          #btnHighLightSuccess2 {
            width: 90%;
            border-radius: 5px!important;
            background-color: #1f9d57 !important;
            border-color: #1f9d57 !important;
            padding: 10px 0;
            color: white;
            padding-left: 80px;
          }
          #btnHighLightDanger2 {
            width: 90%;
            border-radius: 5px!important;
            background-color: #ea5455 !important;
            border-color: #ea5455 !important;
            padding: 10px 0;
            color: white;
            padding-left: 80px;
          }
          #btnHighLightLight {
            width: 90%;
            border-radius: 5px!important;
            background-color: #a5abb6 !important;
            border-color: #a5abb6 !important;
            padding: 10px 0;
            color: white;
            text-align: center !important;
          }

          #btnHighLight:focus-within {
            border: 1px solid black !important;
            border-radius: 10px;
            text-shadow: 2px 2px 5px #00cfe8 !important;
          }

          #btnNextQuestion {
            margin-top: 55%;
            width: 50%;
            border: none;
            background-color: white;
          }

          #btnNextQuestion:focus-within {
            color: green;
            cursor: pointer;
            border-color: black;
          }
        `}</style>

        <div style={{backgroundColor: 'white'}}>
          <br/>
          <Row>
            <Col xs={12}>

              {this.props.hidePathFile === false?
                <ReactPlayerReading
                  ref={this.ref}
                  url={quest.pathFile1 ? quest.pathFile1 : ""}
                  playing={this.props.playPathFile}
                  controls={true}
                  // onPlay={this.onPlaying}
                  // onPause={this.onPausing}
                  config={{
                    file: {
                      attributes: {
                        controlsList: 'nodownload'
                      }
                    }
                  }}
                  width="40%"
                  height="80%"
                >
                </ReactPlayerReading> : null
              }




              {this.state.playQuesA === true ?
                <ReactPlayerReading
                  ref={this.ref}
                  url={quest.pathFileQuesA ? quest.pathFileQuesA : ""}
                  playing={this.state.playQuesA}
                  controls={this.state.playQuesA}
                  onPlay={this.onPlaying}
                  onEnded={this.onEnding}
                  // onPlay={this.onPlaying}
                  // onPause={this.onPausing}
                  config={{
                    file: {
                      attributes: {
                        controlsList: 'nodownload'
                      }
                    }
                  }}
                  width="40%"
                  height="80%"
                >
                </ReactPlayerReading> : null
              }

              {this.state.playQuesB === true ?
                <ReactPlayerReading
                  ref={this.ref}
                  url={quest.pathFileQuesB ? quest.pathFileQuesB : ""}
                  playing={this.state.playQuesB}
                  controls={this.state.playQuesB}
                  onPlay={this.onPlaying}
                  onEnded={this.onEnding}
                  // onPlay={this.onPlaying}
                  // onPause={this.onPausing}
                  config={{
                    file: {
                      attributes: {
                        controlsList: 'nodownload'
                      }
                    }
                  }}
                  width="40%"
                  height="80%"
                >
                </ReactPlayerReading> : null
              }

              {this.state.playQuesC === true ?
                <ReactPlayerReading
                  ref={this.ref}
                  url={quest.pathFileQuesC ? quest.pathFileQuesC : ""}
                  playing={this.state.playQuesC}
                  controls={this.state.playQuesC}
                  onPlay={this.onPlaying}
                  onEnded={this.onEnding}
                  // onPlay={this.onPlaying}
                  // onPause={this.onPausing}
                  config={{
                    file: {
                      attributes: {
                        controlsList: 'nodownload'
                      }
                    }
                  }}
                  width="40%"
                  height="80%"
                >
                </ReactPlayerReading> : null
              }

              {this.state.playQuesD === true ?
                <ReactPlayerReading
                  ref={this.ref}
                  url={quest.pathFileQuesD ? quest.pathFileQuesD : ""}
                  playing={this.state.playQuesD}
                  controls={this.state.playQuesD}
                  onPlay={this.onPlaying}
                  onEnded={this.onEnding}
                  // onPlay={this.onPlaying}
                  // onPause={this.onPausing}
                  config={{
                    file: {
                      attributes: {
                        controlsList: 'nodownload'
                      }
                    }
                  }}
                  width="40%"
                  height="80%"
                >
                </ReactPlayerReading> : null
              }




            </Col>
          </Row>
          <Row className="text-center" style={{marginTop: '5%', marginBottom: '5%'}}>
            <Col xs={1}>
              {
                this.props.index === 0 || multiListeningReducer.reviewPractice === false ?
                  <Button onClick={() => {
                    this.props.preQuestion()
                  }} color="white" className="btn-icon"
                          style={{width: "100%", display: "none"}}>
                    <ChevronsLeft size={50}/>
                  </Button>
                  :
                  <Button onClick={() => {
                    this.props.preQuestion()
                  }} color="white" className="btn-icon"
                          style={{width: "100%", height: "80%", display: "none"}}>
                    <ChevronsLeft size={50}/>
                  </Button>
              }
            </Col>

            <Col xs={10}>
              <Row>
                {
                  quest.pathFile2 && quest.pathFile2.trim() !== "" ?
                    <Col className="text-center" xs={6}>
                      <div style={{width: "100%", height: "100%", marginBottom: "5%"}}>
                        <img src={quest.pathFile2 ? `${quest.pathFile2}` : ""} style={{
                          border: "1px black solid",
                          width: "100%",
                          height: "100%"
                        }}/>
                      </div>
                    </Col>
                    : ""
                }
                <Col className={quest.pathFile2 && quest.pathFile2.trim() !== "" ? "text-right" : "text-center"}
                     xs={quest.pathFile2 && quest.pathFile2.trim() !== "" ? 6 : 12}>
                  {quest.pathFile2 && quest.pathFile2.trim() !== "" ?
                    <div>
                      { multiListeningReducer.questionAnswer !== undefined && this.props.countCheck >= 3 ?
                          quest.listAnswerToChoose.map((answerToChoose, index) => {
                            let xhtml = ""
                            if (answerToChoose.answer === quest.userChoose) {
                              xhtml =
                                <div style={{marginBottom: '2%'}}>
                                  <button id="btnHighLightDanger" className="text-left"
                                          style={{width: "90%", borderRadius: "10px"}} color="danger"
                                          onClick={() =>
                                            // if (answerToChoose.startTime != null && answerToChoose.startTime.trim() != "") {
                                            //   let second = 0;
                                            //   let listItemTime = answerToChoose.startTime.split(":");
                                            //   second += parseFloat(listItemTime[0] * 3600);
                                            //   second += parseFloat(listItemTime[1] * 60);
                                            //   second += parseFloat(listItemTime[2]);
                                            //   if (this.player.getDuration() >= second) {
                                            //     this.player.seekTo(second, 'seconds');
                                            //     this.setState({...this.state, playing: true})
                                            //   }
                                            // }

                                            this.playingAgain(index)
                                          }>
                                    <X size={17} style={{paddingBottom: '2px', marginRight: '5px'}}/>
                                    <span>{'   '}</span>
                                    <strong>
                                      {/*Nghe lại phương án*/}
                                      <FormattedMessage id="listen.again.option" tagName="data" />{' '}
                                      {answerToChoose.answer}</strong>
                                  </button>
                                  <br/><br/>
                                </div>
                            }
                            if (answerToChoose.answer === quest.answer) {
                              xhtml =
                                <div style={{marginBottom: '2%'}}>
                                  <button id="btnHighLightSuccess" className="text-left"
                                          style={{width: "90%", borderRadius: "10px"}} color="success"
                                          onClick={() =>
                                            // if (answerToChoose.startTime != null && answerToChoose.startTime.trim() != "") {
                                            //   let second = 0;
                                            //   let listItemTime = answerToChoose.startTime.split(":");
                                            //   second += parseFloat(listItemTime[0] * 3600);
                                            //   second += parseFloat(listItemTime[1] * 60);
                                            //   second += parseFloat(listItemTime[2]);
                                            //   if (this.player.getDuration() >= second) {
                                            //     this.player.seekTo(second, 'seconds');
                                            //     this.setState({...this.state, playing: true})
                                            //   }
                                            // }

                                            this.playingAgain(index)
                                          }>
                                    <Check size={17} style={{paddingBottom: '2px', marginRight: '5px'}}/>
                                    <span>{'   '}</span>
                                    <strong>
                                      {/*Nghe lại phương án*/}
                                      <FormattedMessage id="listen.again.option" tagName="data" />{' '}
                                      {answerToChoose.answer}</strong>
                                  </button>
                                  <br/><br/>
                                </div>
                            }
                            if (answerToChoose.answer !== quest.answer
                              && answerToChoose.answer !== quest.userChoose) {
                              xhtml =
                                <div style={{marginBottom: '2%'}}>
                                  <button id="btnHighLightLight" className="text-left"
                                          style={{width: "90%", borderRadius: "10px"}} color="light"
                                          onClick={() =>
                                            // if (answerToChoose.startTime != null && answerToChoose.startTime.trim() != "") {
                                            //   let second = 0;
                                            //   let listItemTime = answerToChoose.startTime.split(":");
                                            //   second += parseFloat(listItemTime[0] * 3600);
                                            //   second += parseFloat(listItemTime[1] * 60);
                                            //   second += parseFloat(listItemTime[2]);
                                            //   if (this.player.getDuration() >= second) {
                                            //     this.player.seekTo(second, 'seconds');
                                            //     this.setState({...this.state, playing: true})
                                            //   }
                                            // }

                                            this.playingAgain(index)
                                          }>
                                    <strong>
                                      {/*Nghe lại phương án*/}
                                      <FormattedMessage id="listen.again.option" tagName="data" />{' '}
                                      {answerToChoose.answer}</strong>
                                  </button>
                                  <br/><br/>
                                </div>
                            }
                            return xhtml;
                          })
                          :
                          quest.listAnswerToChoose.map((answerToChoose, index) => (
                            <div style={{marginBottom: '2%'}}>
                              <button id="btnHighLight" style={{width: "90%", borderRadius: "10px"}} color="info"
                                      onClick={() => {
                                        debugger
                                        // if(this.props.countCheck >= 2)
                                        // this.setState({
                                        //   showPathFile: false
                                        // });
                                        this.playingAgain(-1)
                                        const {checkAnswerMultiListeningQuestion} = this.props.doPracticeAction
                                        checkAnswerMultiListeningQuestion(multiListeningReducer.topicId, quest.id, answerToChoose.answer)
                                      }}>
                                <strong>{answerToChoose.answer}</strong>
                              </button>
                              <br/><br/>
                            </div>
                          ))
                      }
                    </div>
                    :
                    <Row>
                      <Col xs={3}></Col>
                      <Col xs={6}>
                        <div>
                          { multiListeningReducer.questionAnswer !== undefined && this.props.countCheck >= 2 ?
                              quest.listAnswerToChoose.map((answerToChoose, index) => {
                                let xhtml = ""
                                if (answerToChoose.answer === quest.userChoose) {
                                  xhtml =
                                    <div style={{marginBottom: '2%'}}>
                                      <button id="btnHighLightDanger2" className="text-left"
                                              style={{width: "90%", borderRadius: "10px"}} color="danger"
                                              onClick={() => {

                                                this.playingAgain(index)
                                              }}>
                                        <X size={17} style={{paddingBottom: '2px'}}/>
                                        <span>{'   '}</span>
                                        <strong>
                                          {/*Nghe lại phương án*/}
                                          {/*Nghe lại phương án*/}
                                          <FormattedMessage id="listen.again.option" tagName="data" />{' '}
                                          {answerToChoose.answer}
                                        </strong>
                                      </button>
                                      <br/><br/>
                                    </div>
                                }
                                if (answerToChoose.answer === quest.answer) {
                                  xhtml =
                                    <div style={{marginBottom: '2%'}}>
                                      <button id="btnHighLightSuccess2" className="text-left"
                                              style={{width: "90%", borderRadius: "10px"}} color="success"
                                              onClick={() => {
                                                this.playingAgain(index)
                                              }}>
                                        <Check size={17} style={{paddingBottom: '2px'}}/>
                                        <span>{'   '}</span>
                                        <strong>
                                          {/*Nghe lại phương án */}
                                          {/*Nghe lại phương án*/}
                                          <FormattedMessage id="listen.again.option" tagName="data" />{' '}
                                          {answerToChoose.answer}</strong>
                                      </button>
                                      <br/><br/>
                                    </div>
                                }
                                if (answerToChoose.answer !== quest.answer
                                  && answerToChoose.answer !== quest.userChoose) {
                                  xhtml =
                                    <div style={{marginBottom: '2%'}}>
                                      <button id="btnHighLightLight" className="text-left"
                                              style={{width: "90%", borderRadius: "10px"}} color="light"
                                              onClick={() => {

                                                this.playingAgain(index)
                                              }}>
                                        <strong>
                                          {/*Nghe lại phương án*/}
                                          <FormattedMessage id="listen.again.option" tagName="data" />{' '}
                                          {answerToChoose.answer}</strong>
                                      </button>
                                      <br/><br/>
                                    </div>
                                }
                                return xhtml;
                              })
                              :
                              quest.listAnswerToChoose.map((answerToChoose, index) => (
                                <div style={{marginBottom: '2%'}}>
                                  <button id="btnHighLight" style={{width: "90%", borderRadius: "10px"}} color="info"
                                          onClick={() => {
                                            debugger
                                            // if(this.props.countCheck >= 1)
                                            //   this.setState({
                                            //     showPathFile: false
                                            //   });
                                            this.playingAgain(-1)
                                            const {checkAnswerMultiListeningQuestion} = this.props.doPracticeAction
                                            checkAnswerMultiListeningQuestion(multiListeningReducer.topicId, quest.id, answerToChoose.answer)
                                          }}>
                                    <strong>{answerToChoose.answer}</strong>
                                  </button>
                                  <br/><br/>
                                </div>
                              ))
                          }
                        </div>
                      </Col>
                      <Col xs={3}></Col>
                    </Row>
                  }
                </Col>
              </Row>

            </Col>


            <Col xs={1}>
              {
                !multiListeningReducer.questionAnswer?
                 null
                  :
                  <>
                    {quest.pathFile2 && quest.pathFile2.trim() !== "" ?
                      <>
                        {/*bài nghe part 1*/}
                        {this.props.countCheck >= 3 ?
                          (index + 1) == totalQuest || multiListeningReducer.reviewPractice === true ?
                            <button id="btnNextQuestion" onClick={() => {

                              this.setState({
                                playPathFile: false,
                                playQuesA: false,
                                playQuesB: false,
                                playQuesC: false,
                                playQuesD: false
                              })
                              history.push("/pages/aggregatedResults")
                            }}>
                              <ChevronsRight size={35}/>
                            </button>

                            :
                            <button id="btnNextQuestion" onClick={() => {
                              const {doPracticeAction} = this.props;
                              const {resetCountCheckListeningPractices} = doPracticeAction;
                              resetCountCheckListeningPractices();

                              this.setState({
                                playPathFile: true,
                                playQuesA: false,
                                playQuesB: false,
                                playQuesC: false,
                                playQuesD: false
                              })
                              this.props.nextQuestion()
                            }}>
                              <ChevronsRight size={35}/>
                            </button>

                          : null
                        }
                      </>
                      :
                      <>
                        {/*bài nghe part 2*/}
                        {this.props.countCheck >= 2 ?
                          (index + 1) == totalQuest || multiListeningReducer.reviewPractice === true ?
                            <button id="btnNextQuestion" onClick={() => {

                              this.setState({
                                playPathFile: false,
                                playQuesA: false,
                                playQuesB: false,
                                playQuesC: false,
                                playQuesD: false
                              })
                              history.push("/pages/aggregatedResults")
                            }}>
                              <ChevronsRight size={35}/>
                            </button>

                            :
                            <button id="btnNextQuestion" onClick={() => {
                              const {doPracticeAction} = this.props;
                              const {resetCountCheckListeningPractices} = doPracticeAction;
                              resetCountCheckListeningPractices();

                              this.setState({
                                playPathFile: true,
                                playQuesA: false,
                                playQuesB: false,
                                playQuesC: false,
                                playQuesD: false
                              })
                              this.props.nextQuestion()
                            }}>
                              <ChevronsRight size={35}/>
                            </button>

                          : null
                        }
                      </>
                    }
                  </>

              }
            </Col>
          </Row>

          {quest.pathFile2 && quest.pathFile2.trim() !== "" ?
            <>
              {/*bài nghe part 1*/}
              {this.props.countCheck >= 3 ?
                <>
                  {
                    quest.description || quest.transcript ?
                      <div
                      >
                        <Row>
                          <Col xs="12" className={classes.title}>
                            <Badge color="success" className="mr-1 mb-1 badge-square">
                              <span>
                                {/*Giải thích*/}
                                <FormattedMessage id="explanation"/>
                              </span>
                            </Badge>
                          </Col>
                        </Row>
                        <Row>
                          <div style={{border: "#28c76f 2px solid", padding: "1% 1% 3% 1%", fontStyle: 'italic'}}
                               className={classes.content + " text-success"}>
                            {
                              quest.description ? quest.description.split("\n").map((des, i) =>
                                <p>
                                  {i === 0 ? <>(A) {des}</> : null}
                                  {i === 1 ? <>(B) {des}</> : null}
                                  {i === 2 ? <>(C) {des}</> : null}
                                  {i === 3 ? <>(D) {des}</> : null}
                                </p>
                              ) : <>This question have no description</>
                            }
                          </div>
                        </Row>
                        <Row>
                          <Col xs="12" className={classes.title}>
                            <Badge color="light" className="mr-1 mb-1 badge-square">
                              <span>Transcript</span>
                            </Badge>
                          </Col>
                        </Row>
                        <Row>
                          <div style={{border: "#babfc7 2px solid", padding: "1% 1% 3% 1%"}}
                               className={classes.content}>
                            {
                              quest.transcript ? quest.transcript.split("\n").map((trans, i) =>
                                <p>
                                  {i === 0 ? <>(A) {trans}</> : null}
                                  {i === 1 ? <>(B) {trans}</> : null}
                                  {i === 2 ? <>(C) {trans}</> : null}
                                  {i === 3 ? <>(D) {trans}</> : null}
                                </p>
                              ) : <>This question have no transcript</>
                            }
                          </div>
                        </Row>
                      </div> : ""
                  }
                </> : null
              }
            </>
            :
            <>
              {/*bài nghe part 2*/}
              {this.props.countCheck >= 2 ?
                <>
                  {
                    quest.description || quest.transcript ?
                      <div
                      >
                        <Row>
                          <Col xs="12" className={classes.title}>
                            <Badge color="success" className="mr-1 mb-1 badge-square">
                              <span>
                                {/*Giải thích*/}
                                <FormattedMessage id="explanation"/>
                              </span>
                            </Badge>
                          </Col>
                        </Row>
                        <Row>
                          <div style={{border: "#28c76f 2px solid", padding: "1% 1% 3% 1%", fontStyle: 'italic'}}
                               className={classes.content + " text-success"}>
                            {
                              quest.description ? quest.description.split("\n").map((des, i) =>
                                <p>
                                  {i === 0 ? <>(A) {des}</> : null}
                                  {i === 1 ? <>(B) {des}</> : null}
                                  {i === 2 ? <>(C) {des}</> : null}
                                  {i === 3 ? <>(D) {des}</> : null}
                                </p>
                              ) : <>This question have no description</>
                            }
                          </div>
                        </Row>
                        <Row>
                          <Col xs="12" className={classes.title}>
                            <Badge color="light" className="mr-1 mb-1 badge-square">
                              <span>Transcript</span>
                            </Badge>
                          </Col>
                        </Row>
                        <Row>
                          <div style={{border: "#babfc7 2px solid", padding: "1% 1% 3% 1%"}}
                               className={classes.content}>
                            {
                              quest.transcript ? quest.transcript.split("\n").map((trans, i) =>
                                <p>
                                  {i === 0 ? <>(A) {trans}</> : null}
                                  {i === 1 ? <>(B) {trans}</> : null}
                                  {i === 2 ? <>(C) {trans}</> : null}
                                  {i === 3 ? <>(D) {trans}</> : null}
                                </p>
                              ) : <>This question have no transcript</>
                            }
                          </div>
                        </Row>
                      </div> : ""
                  }
                </> : null
              }
            </>
          }

        </div>
      </>

    );
  }
}

Question.propTypes = {
  classes: PropTypes.object
}

const mapDispatchToProps = dispatch => {
  return {
    doPracticeAction: bindActionCreators(practiceListenMultiActions, dispatch)
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    readingReducer: state.readingReducer,
    multiListeningReducer: state.multiListeningReducer,
    countCheck: state.multiListeningReducer.countCheck,
    hidePathFile: state.multiListeningReducer.hidePathFile
  }
}
const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
)

export default compose(withStyles(styles.style), withConnect)(Question);
