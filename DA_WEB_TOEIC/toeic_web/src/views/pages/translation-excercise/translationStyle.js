import styled from 'styled-components';

export const styles = () => ({
  topicName:{
    fontSize: '200%'
  },
  result:{
    fontSize: '130%',
    fontWeight: '600',
    margin: '5%'
  },
  titleModal:{
    backgroundColor: '#02a1ed',
    margin: '0 12%',
    height: '50px',
    borderRadius: '8px',
    color: 'white',
    position: 'relative'
  },
  titleModalChild:{
    marginLeft: '2%',
    position: 'absolute',
    top: '50%',
    // -msTransform: 'translateY(-50%)',
    transform: 'translateY(-50%)',
    fontSize: '180%'
  },
  bodySnp:{
    paddingLeft: '0px',
    paddingRight: '0px',
  },
  bodyModal:{
    backgroundColor: 'white',
    margin: '0 6px',
    border: '1px solid #02a1ed'
  },
  paragraphEng:{
    backgroundColor: '#e5e3e3',
    padding: '3%',
    margin: '3% 10% 3% 10%',
    height: '350px',
  },
  textareaEng:{
    width: '100%',
    border: 'none',
    height: '300px',
    backgroundColor: '#e5e3e3',
    color: 'black'
  },
  paragraphViet:{
    margin: '3% 10% 3% 10%'
  },
  divSubmit:{
    textAlign: 'center',
    margin: '5% 0 5% 0'
  },
  btnSubmit:{
    backgroundColor: '#02a1ed !important',
    // backgroundColor: 'red'
    padding: '8px 25px',
    borderColor: '#02a1ed',
    borderRadius: '7px !important' ,
    color: 'white',
    "&hover": {
      backgroundColor: 'red !important'
    }
  },
  inputParagraphViet:{
    height: '350px !important',
    borderColor: 'gray',
    borderRadius: '0px'
  },
  /*
  @media (max-width: 6144px) {
    .inputParagraphViet{
      height: 272px !important;
      border: 0.5px solid gray;
      border-radius: 0px;
    }
  }
  @media (max-width: 1187px) {
    .inputParagraphViet{
      height: 292px !important;
      border-color: gray;
      border-radius: 0px;
    }
  }
  @media (max-width: 980px) {
    .inputParagraphViet{
      height: 407px !important;
      border-color: gray;
      border-radius: 0px;
    }
  }
  @media (max-width: 750px) {
    .inputParagraphViet{
      height: 272px !important;
      border-color: gray;
      border-radius: 0px;
    }
  }*/


  divAnswer:{
    textAlign: 'center',
    margin: '0 5% 5% 0'
  },
  legendAnswer:{
    color: 'white',
    textAlign: 'left',
    backgroundColor: 'green',
    width: 'auto',
    padding: '5px 20px !important',
    fontSize: 'medium'
  },
  fieldSet:{
    border: '2px solid green !important',
    padding: '0 2% 2% 2% !important',
    margin: '0 5% !important',
    textAlign: 'left !important',
    color: 'green',
  },
  translationNextQuestion:{
    textAlign: 'right',
    padding: '0px',
    marginTop: '2%'
  },
  iconNextQuestion:{
    margin: '0px !important',
    cursor: 'pointer',
    color: '#b2aeae'
  },
  iconNextQuestionHover :{
    color: 'green'
  }

})


// export default styles;
