import React, {PureComponent} from 'react';
import {connect} from "react-redux";
import * as Icon from "react-feather";
import {bindActionCreators, compose} from "redux";
import translationAction from "../../../../redux/actions/translation-exercise/translationAction";

import {
  Card,
  CardBody,
  CardImg,
  Container,
  Row,
  Col,
  Button,
  Progress,
  Input,
  Form  ,
  Spinner
} from "reactstrap"

import {withStyles} from "@material-ui/core";
import {styles} from '../translationStyle.js';
import {FormattedMessage} from "react-intl";
import {showMessage} from "../../../../commons/utils/convertDataForToast";

class TranslationVA extends PureComponent {
  /*{id: 1, name: "Hello", parentId: 342, status: 1, description: "đây là câu 1" ,
                    answersToChoose: "A.dapansai|B.dapansai|C.dapansai|D.dapand", answer: "Xin chào", score: 5 },
                {id: 2, name: "Thank you", parentId: 342, status: 1, description: "đây là câu 1" ,
                    answersToChoose: "A.dapansai|B.dapansai|C.dapansai|D.dapand", answer: "Cảm ơn", score: 5 },
                {id: 3, name: "Goodbye", parentId: 342, status: 1, description: "đây là câu 1" ,
                    answersToChoose: "A.dapansai|B.dapansai|C.dapansai|D.dapand", answer: "Tạm biệt", score: 5 }*/
  constructor(props){
    super(props);
    this.state = {
      data: true,
      errorMessage: "",
      answer: "",
      listQuestions: [],
      question: {},
      index: 0   ,
      load: false
    }
  }
  componentDidUpdate(prevProps){
    if(this.props.listQaTransVa !== prevProps.listQaTransVa){
      this.setState({
        load: true,
        index: 0 ,
        data: true,
        answer: "",
        result: 0
      })
      setTimeout(()=>{
        this.setState({
          load: false
        })
      }, 500)
      const element = document.getElementById("titleQuestion");
      element.scrollIntoView()
    }
  }

  componentDidMount() {
    const element = document.getElementById("titleQuestion");
    element.scrollIntoView()
  }

  changeInput=(e)=>{
    let answer = e.target.value;
    if(answer==""){
      this.setState({
        errorMessage: showMessage("the.answer.is.required"),
        answer: ""
      })
    }
    else{
      this.setState({
        errorMessage: "",
        answer: answer
      })
    }

  }

  showAnswer=()=>{
    debugger
    let {translationAction} = this.props;
    let {disableAnswerWhenSubmit} = translationAction;

    const {index} = this.state;
    const {listQaTransVa} = this.props;
    let question = listQaTransVa[index];
    let answer = this.state.answer;
    if(answer.trim() === ""){
      this.setState({
        errorMessage: showMessage("the.answer.is.required"),
      })
    }
    else{
      disableAnswerWhenSubmit(true);

      this.setState({
        load: true,
        data: false,
        errorMessage: ""
      })
      setTimeout(()=>{
        this.setState({
          load: false
        })
      }, 500)
      this.cutAnswer(question.answer).map((subAnswer)=>
        answer.trim().toLowerCase() === subAnswer.trim().toLowerCase() ?
          this.setState({
            result: 1
          }) :
          this.setState({
            result: 2
          })
      )
    }
  }
  nextQuestion =()=>{

    let {translationAction} = this.props;
    let {disableAnswerWhenSubmit} = translationAction;
    disableAnswerWhenSubmit(false);

    let index = this.state.index;
    if(index<=1){
      this.setState({
        load: true,
        index: index + 1,
        data: true,
        answer: "",
        result: 0
      })
      setTimeout(()=>{
        this.setState({
          load: false
        })
      }, 500)

    }

  }

  cutQuestionName(name){
    debugger
    let index = name.indexOf("|");
    let title = name.substring(0, index);
    let content = name.substring(index+1);

    let array = [];
    array.push(title);
    array.push(content);
    console.log("title: ",array[0]);
    console.log("content: ",array[1]);
    return array;
  }

  cutAnswer(answer){
    let char = 0;
    let answerArray = [];
    let subAnswer = "";

    let count=0,index=0; index = 0;
    for( count=-1,index=-2; index != -1; count++,index=answer.indexOf("|",index+1) );

    for(let i=1;i<=count;i++){
      char = answer.indexOf("|");
      subAnswer = answer.substring(0, char);
      answerArray.push(subAnswer);
      answer = answer.substring(char + 1);
    }

    return answerArray;
  }
  render() {
    const {classes, listQaTransVa} = this.props;
    const checkReadOnly = true;
    const {data, index, answer} = this.state;
    let total = 0;
    let question = {};
    if(listQaTransVa !== null){
      total = listQaTransVa.length;
      question = listQaTransVa[index];
    }
    const loadingComponent = (
      <div style={{
        position: 'absolute',
        zIndex: 110,
        top: 0,
        left: 0,
        width: '74%',
        height: '100%',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        background: 'rgba(255,255,255,0.8)',
        margin: '0 13%'
      }}>
        <Spinner color="primary" className="reload-spinner"/>
      </div>
    );

    return (
      <>
        <style>{`
          #iconNextQuestion:hover{
            color: green !important;
          }
        `}</style>
        <h2 id="titleQuestion" style={{textAlign: 'center'}} className={classes.topicName}>
          <b>
            {/*Bài dịch Việt-Anh*/}
            <FormattedMessage id="trans.eng.to.viet" tagName="data" />{' '}
            -{' '}
            {/*Trình độ*/}
            <FormattedMessage id="level" tagName="data" />{' '}
            {/*{this.props.levelName}*/}
            <FormattedMessage id={this.props.levelName} tagName="data" />
          </b>
        </h2>
        <Container style={{marginTop: '2%'}}>
          <div className={classes.titleModal}>
            <div className={classes.titleModalChild}>
              {/*Câu*/}
              <FormattedMessage id="question" tagName="data" />{' '}
              {this.state.index + 1}{' '}
              {/*đến*/}
              <FormattedMessage id="to.for.translation" tagName="data" />{' '}
              {total}
            </div>
          </div>
          <Col sm="12" className={classes.bodySnp}>
            <div style={{position: 'relative'}}>
              {this.state.load === true ? <>{loadingComponent}</> : ""}
              <Row className={classes.bodyModal} style={{margin: '0 13% 0 13%', padding: '0 0%'}}>
                <Col xs="12" md="12" sm="12" lg="12">
                  {this.state.result === 1 ?
                    <Row>
                      <Col xs={9}>
                        <div className={classes.result} style={{ color: 'green'}}>
                          {/*Chúc mừng bạn đã làm đúng bài dịch này*/}
                          <FormattedMessage id="congratulations.you.did.this.translation.exercise.correctly" tagName="data" />
                        </div>
                      </Col>
                      {this.state.index + 1 === total ? "" :
                        <Col xs={3} className={classes.translationNextQuestion}>
                          <Icon.ChevronsRight
                            size={48}
                            className="mr-4 fonticon-wrap"
                            id = "iconNextQuestion"
                            style = {{
                              margin: '0px !important',
                              cursor: 'pointer',
                              color: '#b2aeae'
                            }}
                            onClick={this.nextQuestion}
                          />
                        </Col>
                      }
                    </Row>
                    : null

                  }
                  {this.state.result === 2 ?
                    <Row>
                      <Col xs={8}>
                        <div className={classes.result} style={{ color: 'red',}}>
                          {/*Bạn chưa làm đúng bài dịch này*/}
                          <FormattedMessage id="you.did.this.translation.exercise.incorrectly" tagName="data" />
                        </div>
                      </Col>
                      {this.state.index + 1 === total ? "" :
                        <Col xs={4} className={classes.translationNextQuestion}>
                          <Icon.ChevronsRight
                            size={48}
                            className="mr-4 fonticon-wrap"
                            id = "iconNextQuestion"
                            style = {{
                              margin: '0px !important',
                              cursor: 'pointer',
                              color: '#b2aeae'
                            }}
                            onClick={this.nextQuestion}
                          />
                        </Col>
                      }
                    </Row>
                    : null
                  }

                </Col>
                <Col xs="12" md="12" sm="12" lg="6">
                  {/*{question ?*/}
                    {/*<div className={classes.paragraphEng}>*/}
                      {/*<h3 style={{textAlign: 'center'}}><b>{this.cutQuestionName(question.name)[0]}</b></h3>*/}
                      {/*<textarea className={classes.textareaEng} disabled>*/}
                        {/*{this.cutQuestionName(question.name)[1]}*/}
                      {/*</textarea>*/}
                    {/*</div>*/}
                  {/*: ""}*/}

                  {question ?
                    <div className={classes.paragraphEng}>
                      <h3 style={{textAlign: 'center'}}><b>{this.cutQuestionName(question.name)[0]}</b></h3>
                      <div className={classes.textareaEng} disabled>
                        {this.cutQuestionName(question.name)[1]}
                      </div>
                    </div>
                    : ""}
                </Col>
                <Col xs="12" md="12" sm="12" lg="6">
                  <div className={classes.paragraphViet}>
                    <Input
                      readOnly={this.props.checkReadOnly}
                      className={classes.inputParagraphViet}
                      type="textarea"
                      name="answer"
                      value={answer}
                      onChange = {this.changeInput}
                    />
                    <p style={{color: "red", textAlign:'center'}}><i><strong> {this.state.errorMessage} </strong></i></p>
                  </div>
                </Col>

                {data == true ?
                  (<Col xs="12" md="12" sm="12" lg="12" className={classes.divSubmit}>
                    <button className={classes.btnSubmit}
                            onClick={this.showAnswer}
                    >Nộp bài</button>
                  </Col>):
                  (
                    <>
                      {/*{this.state.index + 1 === total ? "" :*/}
                        {/*<Col xs="12" md="12" sm="12" lg="12" className={classes.translationNextQuestion}>*/}
                          {/*<Icon.ChevronsRight*/}
                            {/*size={64}*/}
                            {/*className="mr-4 fonticon-wrap"*/}
                            {/*id = "iconNextQuestion"*/}
                            {/*style = {{*/}
                              {/*margin: '0px !important',*/}
                              {/*cursor: 'pointer',*/}
                              {/*color: '#b2aeae'*/}
                            {/*}}*/}
                            {/*onClick={this.nextQuestion}*/}
                          {/*/>*/}
                        {/*</Col>*/}
                      {/*}*/}
                      {question ?
                        <>
                          {this.cutAnswer(question.answer).map((subAnswer, index)=>
                            <Col xs="12" md="12" sm="12" lg="12" className={classes.divAnswer}>
                              <fieldset className={classes.fieldSet}>
                                <legend className={classes.legendAnswer}>
                                  {/*Đáp án*/}
                                  <FormattedMessage id="answer" tagName="data" />{' '}
                                  {index+1}</legend>
                                <h3 style={{textAlign: 'center', color: 'green'}}><i></i></h3>
                                <i>{subAnswer}</i>
                              </fieldset>
                            </Col>
                          )}
                        </>
                        : ""
                      }
                    </>)
                  }

              </Row>
            </div>
          </Col>
        </Container>
      </>
    );
  }
}

const mapStateToProps = state => {
  return {
    userInfo: state.auth.login,
    listQaTransVa: state.translationReducer.listQaTransVa,
    isLoading: state.translationReducer.isLoading,
    checkReadOnly: state.translationReducer.checkReadOnly,
    levelName: state.translationReducer.levelName
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    translationAction : bindActionCreators(translationAction , dispatch)
  }
}
const withConnect = connect(mapStateToProps, mapDispatchToProps)
export default compose(withStyles(styles), withConnect)(TranslationVA);
