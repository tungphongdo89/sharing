const styles = theme => ({

  title: {
    padding: '25px',
    paddingRight: '15px',
    marginTop: '20px',
    fontFamily: 'Arial-BoldMT, Arial Bold, Arial, sans - serif',
    fontStyle: 'normal',
    color: '#000',
    textAlign: 'center',
    fontWeight: '700',
  },
  icon: {
    marginTop: '20px',
    textAlign: 'right',
    fontWeight: '800',
    fontStyle: 'normal',
    color: 'black',
    fontSize: '16px',
  },
  hasAccount: {
    color: '#169BD5',
    fontSize: '16px',
    padding: '20px',
    textAlign:'right',
    border: 'none',
    background:'none',
    outline: 'none',
    '&:focus': {
      color: '#7367f0',
      border: '1px lightgray solid',
    }
  },
  hasAccountLink: {
    '&:focus': {
      color: '#169BD9 !important',
    }
  },
  buttonRegister: {
    background: '#169BD5',
    color: 'white',
    width: '130px',
    height: '45px',
    borderRadius: '10px ! important',
    border: '#1ab7ea',
    fontSize: '16px',
    paddingRight:'1em',
    // paddingLeft: '15px',
    // marginLeft: '64px ! important',
    '&:focus': {
      background: '#169BD6 !important',
    }

  },
  error:{
    color:'red',

  }

})


export default styles;
