import React from "react"
import {Card, CardBody, Col, Row, TabContent, TabPane} from "reactstrap"
import RegisterJWT from "./RegisterJWT";
import {withStyles} from "@material-ui/core";
import "../../../../assets/scss/pages/authentication.scss"
import styles from './styles';
import {showMessage} from "../../../../commons/utils/convertDataForToast";
import {FormattedMessage} from "react-intl";


class Register extends React.Component {
  state = {
    activeTab: "1"
  }
  myRef = React.createRef();
  toggle = tab => {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab
      })
    }
  }

  executeScroll = () => this.myRef.current.scrollIntoView()
  componentDidMount() {
    if(this.myRef.current){
      this.executeScroll();
    }
  }
  render() {
    const {classes} = this.props;
    return (
      <div ref={this.myRef}>
        <Row className="m-0 justify-content-center" >
          <Col
            sm="12"
            xl="12"
            lg="12"
            md="12"
          >
            <Card
              className="rounded-0 mb-0 w-100">
              <Row className="m-0">
                <Col lg="12" md="12" className="p-0">
                  <Card>
                    <Row className="" sm={12}>
                      <Col sm={12} className="text-right">
                        <h2 className={classes.title}>
                          {/*Đăng ký*/}
                          <FormattedMessage id="register" tagName="data" />
                        </h2>
                      </Col>
                    </Row>
                    <CardBody className="pt-1 pb-50">
                      <TabContent activeTab={this.state.activeTab}>
                        <TabPane tabId="1">
                          <RegisterJWT/>
                        </TabPane>
                      </TabContent>
                    </CardBody>
                  </Card>
                </Col>
              </Row>
            </Card>
          </Col>
        </Row>
      </div>

    )
  }
}

export default withStyles(styles)(Register);
