import styled from "styled-components"
import { Col , Row , Label , Button , CardTitle } from "reactstrap"
import { Link } from "react-router-dom"

export const ColLoginForm = styled(Col)`
  width: 80%;
  margin: 0 auto;
  padding: 10px 60px;
  border: 2px solid aliceblue;
  border-radius: 10px;
  box-shadow: 5px 8px 8px 8px #888888;
`

export const RowTitle = styled(Row)`
    margin-bottom: 20px;
`

export const StyleCardTitle = styled(CardTitle)`
  width : 100%;
  text-align: center;
  margin-bottom: 0;
  font-weight: bold;
  font-size : 20px;
`

export const RowLink = styled(Row)`
  margin: 20px 0;
   width : 80%;
  display: inline-flex;
`

export const StyleRow = styled(Row)`
  margin-bottom: 20px;
  padding : 0 14px;
  width : 80%;
  display: inline-flex;
`

export const ColLable = styled(Col)`
  padding : 0;
`

export const ColLink = styled(Col)`
  width: 40%;
  float: left;
  padding: 0;

`

export const StyleLink = styled(Link)`
  text-decoration: none;
  padding-left : 14px;
  &:hover{
    color: red;
    font-weight: bold;
  }
  &:focus{
    color: #10b3bb;
    border: 1px lightgray solid;
  }
`

export const StyleLinkRight = styled(Link)`
  width: 40%;
  text-align: right;
  padding-right : 14px;
  float: right;
  width : 100%;
  &:hover{
    color: red;
    font-weight: bold;
  }
  &:focus{
    color: #10b3bb;
    border: 1px lightgray solid;
  }
`

export const ColSpinner = styled(Col)`
  width: 20%;
  float: left;
  text-align: center;
`

export const StyleLable = styled(Label)`
  height: 38px;
  line-height: 38px;
`

export const ButtonInfo = styled(Button)`
  width: 40%;
  
  &:hover{
     background: #7067db;
  }
  &:focus{
    border: 2px black solid;
    opacity: 0.8;
  }
`

export const ButtonPrimary = styled(Button)`
  width: 100%;
  margin-bottom: 10px;
  color: white;
  
  &:hover{
    background: #7067db;
    color: white;
  }
`
