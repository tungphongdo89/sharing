import * as Yup from "yup";

export const formSchema = Yup.object().shape({
  email: Yup.string()
            .required('email.is.required')
            .min(6, 'email.more.6.char')
            .max(45, 'email.longer.45.char')
            .matches(/^(([^<>()\[\]\\.,^%$#&*='\/?;:\s@!"ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựýỳỵỷỹ]+(\.[^<>!()\[\]\\.,^%$#&*='\/?;:\s@"ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳýỵỷỹ]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z0-9/-_.^-]+\.)+[a-zA-Z]{2,}))$/,
            //.matches(/[a-zA-Z\d]+@[a-zA-Z\d]+\.[a-zA-Z\d]+/,
            'incorrect.email.format'),
  password: Yup.string().required('password.is.required')
    .min(6,'password.must.contain.at.least.6.characters')
    .max(45,'password.cant.be.longer.than.45.characters')
    .matches(/^[^ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựýỳỵỷỹ]+$/,'password.cannot.contain.accented.vietnamese.characters')
    .matches(/^(?=.*[a-zA-Z])(?=.*\d)[a-zA-Z\d\s<>()\[\]\\.,^%$#&*=+-_'"~`!{}|\/?;:@]{6,}$/,'password.must.contain.number.alphabet.and.cant.contain.space')
    .matches(/^[^\s]+$/,'password.can.not.enter.space.characters'),
  passwordConfirm: Yup.string()
    .required("password.is.required")
    .min(6,'password.must.contain.at.least.6.characters')
    .max(45,'password.cant.be.longer.than.45.characters')
    .matches(/^(?=.*[a-zA-Z])(?=.*\d)[a-zA-Z\d<>()\[\]\\.,^%$#&*=+-_'"~`!{}|\/?;:@]{6,}$/,'password.must.contain.number.alphabet.and.cant.contain.space')
    //.matches(/(?=^.\S{5,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Za-z])(?=.*[A-Za-z]).*$/,'Mật khẩu chứa cả chữ và số và không chứa kí tự space')
    .when("password", {
      is: password => (password && password.length > 0  ? true : false),
      then: Yup.string().oneOf(
        [Yup.ref("password")],
        "password.is.not.match"
      )
    }),
});
