import React from "react"
import {Button, Col, FormGroup, Label} from "reactstrap"
import {bindActionCreators, compose} from "redux";
import {connect} from "react-redux"
import {Link} from 'react-router-dom';
// import {signupWithJWT} from "../../../../redux/actions/auth/registerActions"
import {withStyles} from "@material-ui/core";
// import PropTypes from 'prop-types';
import styles from "./styles";
import registerAction from "../../../../redux/actions/auth/registerActions";
import Spinner from "reactstrap/es/Spinner";
import {Field, Form, Formik} from "formik";
import {history} from '../../../../history';
import {formSchema} from './validate';
import {FormattedMessage} from "react-intl";

class RegisterJWT extends React.Component {
  state = {
    email: "",
    password: "",
    confirmPass: "",
    isValidateEmail: false,
    isValidatePassword: true,
    isValidateConfirmPassword: true,
    loading: false,
    errorDisable: false
  }

  componentWillReceiveProps(nextProps, nextContext) {
    const {values} = nextProps;
    if (!values.isLogging) {
      this.setState({
        loading: false
      });
    }
  }
  
  render() {
    const {classes} = this.props;
    const {loading} = this.state;
    return (
      <Formik
        initialValues={{
          email: "",
          password: "",
          passwordConfirm: ""
        }}
        onSubmit={(values, actions) => {
          this.setState({
            loading: true
          })
          setTimeout(() => {
            actions.setSubmitting(false);
            const {registerAction} = this.props;
            const {registerWithEmail} = registerAction;
            const user = {};
            user.username = values.email;
            user.email = values.email;
            user.password = values.password;
            registerWithEmail(user);
          }, 1000);
        }}
        validationSchema={formSchema}

      >
        {({errors , touched,isValid,isSubmitting}) => (
          <Form>
            <FormGroup className="form-group d-flex">
              <Label className="text-right" for="required" sm={6}>Email (<span style={{color: 'red'}}>*</span>) </Label>
              <Col sm={3}>
                <div className="w-100">
                  <Field
                    autoFocus={true}
                    type="email" 
                    name="email"  maxLength={45}
                    className={`form-control ${errors.email &&
                    touched.email &&
                    "is-invalid"}`}
                  />
         
                  {errors.email && touched.email ? (
                    <div className={classes.error}>
                      {/*{errors.email}*/}
                      <FormattedMessage id={errors.email}/>
                    </div>
                  ) : null}
                </div>
              </Col>
            </FormGroup>
            <FormGroup className="form-group d-flex">
              <Label className="text-right" for="required" sm={6}>
                {/*Mật khẩu */}{' '}
                <FormattedMessage id="password" tagName="data" />{' '}
                (<span style={{color: 'red'}}>*</span>) </Label>
              <Col sm={3}>
                <div className="w-100">
                  <Field
                    type="password"
                    name="password"  maxLength={45} 
                    className={`form-control ${errors.password &&
                    touched.password &&
                    "is-invalid"}`}
                  />
                  {errors.password && touched.password ? (
                    <div className={classes.error}>
                      {/*{errors.password}*/}
                      <FormattedMessage id={errors.password}/>
                    </div>
                  ) : null}
                </div>
              </Col>
            </FormGroup>
            <FormGroup className="form-group d-flex">
              <Label className="text-right" for="required" sm={6}>
                {/*Nhập lại mật khẩu*/}{' '}
                <FormattedMessage id="confirm.password" tagName="data" />{' '}
                (<span style={{color: 'red'}}>*</span>) </Label>
              <Col sm={3}>
                <div className="w-100">
                  <Field
                    type="password" maxLength={45}
                    name="passwordConfirm"
                    className={`form-control ${errors.passwordConfirm &&
                    touched.passwordConfirm &&
                    "is-invalid"}`}
                  />
                  {errors.passwordConfirm && touched.passwordConfirm ? (
                    <div className={classes.error}>
                      {/*{errors.passwordConfirm}*/}
                      <FormattedMessage id={errors.passwordConfirm}/>
                    </div>
                  ) : null}
                </div>
              </Col>
            </FormGroup>
            <FormGroup className="form-group d-flex">
              <Col sm={6} className="text-right">
                <Link to={"/pages/login"} onClick={() => {
                  history.push("/pages/login")
                }}     className={classes.hasAccount} 
                >
                  {/*Đã có tài khoản? */}
                  <FormattedMessage id="have.an.account" tagName="data" />
                </Link>
              </Col>
                <Col sm={1} xs={1}>
                  {
                    loading && <Spinner color="primary"/>
                  }
                </Col>
                <Col sm={2} xs={2} className="text-right">
                  <Button color="primary"  disabled={!isValid || isSubmitting}  className={classes.buttonRegister} type="submit">OK</Button>
                </Col>
            </FormGroup>
          </Form>
        )}
      </Formik>
    )
  }
}


const mapStateToProps = state => {
  return {
    values: state.auth.register
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    registerAction: bindActionCreators(registerAction, dispatch)
  }
}
export default compose(withStyles(styles), connect(mapStateToProps, mapDispatchToProps))(RegisterJWT)
