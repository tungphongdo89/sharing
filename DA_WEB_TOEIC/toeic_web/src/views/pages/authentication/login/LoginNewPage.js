import React, {Component} from 'react';
import {Field, Form, Formik} from "formik"
import * as Yup from "yup"
import {ROLE_ADMIN, ROLE_PREVIEW, ROLE_TEACHER} from "../../../../../src/commons/constants/CONSTANTS";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import loginActions from "../../../../redux/actions/auth/loginActions";
import Spinner from "reactstrap/es/Spinner";
import {history} from "../../../../history"
import * as Styles from "./style-login"
import {Col, Row} from "reactstrap"
import {getSessionCookie} from "../../../../commons/configCookie";
import {showMessage} from "../../../../commons/utils/convertDataForToast";
import {FormattedMessage} from "react-intl";
import Multilangue from "../../../../layouts/components/navbar/Multilangue";

class LoginPage extends Component {
  state = {
    loading: false,
    isDisableButtonLogin: true,
  }

  myRef = React.createRef();

  handleLogin = async (values) => {
    this.setState({
      loading: true
    });
    const {loginActions} = this.props;
    const {loginWithJWT} = loginActions
    const user = {};
    user.username = values.username;
    user.password = values.password;
    const res = await loginWithJWT(user)
  }

  executeScroll = () => this.myRef.current.scrollIntoView()

  componentWillMount() {
    this.checkAuth(this.props.values);
  }

  componentDidMount() {
    this.executeScroll();
  }


  componentWillReceiveProps(nextProps, nextContext) {

    const {values} = nextProps;

    if (!values.isLogging) {
      this.setState({
        loading: false
      });
    }
  }

  checkAuth(userInfo) {
    debugger
    console.log(userInfo)
    const {location} = this.props.history;
    console.log(location);
    if (getSessionCookie() !== null) {
      if (userInfo.isAuthenticated) {
        if (userInfo.userRole === ROLE_ADMIN || userInfo.userRole === ROLE_PREVIEW) {
          history.push("/private/admin/")
        } else if (userInfo.userRole === ROLE_TEACHER) {
          history.push("/private/teacher")
        } else
          history.push("/")
      }
    }

  }

  autoLogin = (id) => {
    // e.preventDefault()
    console.log("------------------debug autologin");
    this.setState({
      loading: true
    });
    const {loginActions} = this.props;
    const {loginWithJWT} = loginActions
    const user = {}
    switch (id) {
      case 1:
        user.username = "usertest@gmail.com";
        user.password = "matroi1230";
        break;
      case 2:
        user.username = "huynv@gmail.com";
        user.password = "matroi1230";
        break;
      case 3:
        user.username = "preview@gmail.com";
        user.password = "matroi1230";
        break
      default:
        break
    }
    loginWithJWT(user)
  }

  render() {

    const {loading} = this.state

    return (
      <div className="container-fluid" ref={this.myRef}>
        <Formik
          initialValues={{
            username: "",
            password: ""
          }}
          onSubmit={(values) => {
            this.handleLogin(values)
          }}
          validationSchema={Yup.object().shape({
            username: Yup.string()
              .required(
                // 'Email không được bỏ trống!'
                showMessage("email.is.required")
              )
              .min(6,
                // 'Email phải chứa ít nhất 6 ký tự!'
                showMessage("email.must.contain.at.least.6.characters")
              )
              .max(45,
                // 'Email không được dài quá 45 ký tự!'
                showMessage("email.cant.be.longer.than.45.characters")
              )
              .matches(/^(([^<>()\[\]\\.,^%$#&*='\/?;:\s@!"ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựýỳỵỷỹ]+(\.[^<>!()\[\]\\.,^%$#&*='\/?;:\s@"ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳýỵỷỹ]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z0-9/-_.^-]+\.)+[a-zA-Z]{2,}))$/,
                // 'Email không đúng định dạng!'
                showMessage("incorrect.email.format")
              ),
            password: Yup.string().required(
              // 'Mật khẩu không được bỏ trống!'
              showMessage("password.is.required")
            )
              .min(6,
                // 'Mật khẩu phải chứa ít nhất 6 ký tự!'
                showMessage("password.must.contain.at.least.6.characters")
              )
              .max(45,
                // 'Mật khẩu không được dài quá 45 ký tự!'
                showMessage("password.cant.be.longer.than.45.characters")
              )
              .matches(/^(?=.*[a-zA-Z])(?=.*\d)[a-zA-Z\d<>()\[\]\\.,^%$#&*=+-_'"~`!{}|\/?;:@]{6,}$/,
                // 'Mật khẩu phải chứa cả chữ, số và không chứa khoảng trắng!'
                showMessage("password.must.contain.number.alphabet.and.cant.contain.space")
              ),
          })}
        >
          {({errors}) => (
            <Row>
              <Styles.ColLoginForm xs="12" sm="12" md="8" lg="6">
                <Styles.RowTitle><Styles.StyleCardTitle>
                  {/*Đăng nhập*/}
                  <FormattedMessage id="login" tagName="data"/>
                </Styles.StyleCardTitle></Styles.RowTitle>
                <Form>
                  <Styles.StyleRow>
                    <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12 ">
                      <div className="float-right ">
                        <Multilangue checkDisabled={false}/>
                      </div>
                    </div>
                  </Styles.StyleRow>
                  <Styles.StyleRow>
                    <Styles.ColLable xs="12" sm="12" md="4" lg="4">
                      <Styles.StyleLable>
                        {/*Email đăng nhập*/}
                        <FormattedMessage id="email.for.login" tagName="data"/>{' '}
                        (<span style={{color: "red"}}>*</span>)</Styles.StyleLable>
                    </Styles.ColLable>
                    <Col xs="12" sm="12" md="8" lg="8">
                      <Field autoFocus={true} type="email" className="form-control" name="username" maxLength={45}
                             max="5" min="1"></Field>
                      {errors.username ?
                        <strong><label style={{color: "red"}}>{errors.username}</label></strong> : null}
                    </Col>
                  </Styles.StyleRow>

                  <Styles.StyleRow>
                    <Styles.ColLable xs="12" sm="12" md="4" lg="4">
                      <Styles.StyleLable>
                        {/*Mật khẩu */}
                        <FormattedMessage id="password" tagName="data"/>{' '}
                        (<span style={{color: "red"}}>*</span>)</Styles.StyleLable>
                    </Styles.ColLable>
                    <div className="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                      <Field type="password" className="form-control" name="password" maxLength={45}></Field>
                      {errors.password ?
                        <strong><label style={{color: "red"}}>{errors.password}</label></strong> : null}
                    </div>
                  </Styles.StyleRow>

                  <Styles.RowLink>
                    <Styles.ColLink xs="5" sm="5" md="5" lg="5">
                      <Styles.StyleLink to="/pages/register">
                        {/*Chưa có tài khoản ?*/}
                        <FormattedMessage id="have.no.account.yet" tagName="data"/>
                      </Styles.StyleLink>
                    </Styles.ColLink>
                    <Styles.ColSpinner xs="2" sm="2" md="2" lg="2">
                      {loading && <Spinner color="primary"/>}
                    </Styles.ColSpinner>
                    <Styles.ColLink xs="5" sm="5" md="5" lg="5">
                      <Styles.StyleLinkRight to="/pages/forgot-password">
                        {/*Quên mật khẩu ?*/}
                        <FormattedMessage id="forgot.password" tagName="data"/>
                      </Styles.StyleLinkRight>
                    </Styles.ColLink>
                  </Styles.RowLink>

                  <Styles.StyleRow>
                    <Col xs="4" sm="4" md="4" lg="4"></Col>
                    <Col xs="4" sm="4" md="4" lg="4">
                      <Styles.ButtonInfo type="submit" color="info"
                                         disabled={errors.username || errors.password ? true : false} autofocus>
                        {/*Đăng nhập*/}
                        <FormattedMessage id="login" tagName="data"/>
                      </Styles.ButtonInfo>
                    </Col>
                    <Col xs="4" sm="4" md="4" lg="4"></Col>
                  </Styles.StyleRow>

                  <Styles.StyleRow>
                    <Col xs="4" sm="4" md="4" lg="4">
                      <Styles.ButtonPrimary type="button" color="primary" onClick={() => this.autoLogin(1)}>Login with
                        Student</Styles.ButtonPrimary>
                    </Col>
                    <Col xs="4" sm="4" md="4" lg="4">
                      <Styles.ButtonPrimary type="button" color="primary" onClick={() => this.autoLogin(2)}>Login with
                        Admin</Styles.ButtonPrimary>
                    </Col>
                    <Col xs="4" sm="4" md="4" lg="4">
                      <Styles.ButtonPrimary type="button" color="primary" onClick={() => this.autoLogin(3)}>Login with
                        Preview</Styles.ButtonPrimary>
                    </Col>
                  </Styles.StyleRow>
                </Form>
              </Styles.ColLoginForm>
            </Row>
          )}
        </Formik>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    values: state.auth.login
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    loginActions: bindActionCreators(loginActions, dispatch)
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(LoginPage)
