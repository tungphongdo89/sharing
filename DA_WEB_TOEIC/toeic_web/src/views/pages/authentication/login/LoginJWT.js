import React, {Suspense} from "react"
import {Link} from "react-router-dom"
import {Button, CardBody, Form, FormGroup, Input, Label} from "reactstrap"
import Checkbox from "../../../../components/@vuexy/checkbox/CheckboxesVuexy"
import {Check, Lock, Mail} from "react-feather"
import {connect} from "react-redux"
import {history} from "../../../../history"
import {bindActionCreators} from "redux";
import loginActions from "../../../../redux/actions/auth/loginActions";
// import Spinner from "reactstrap/es/Spinner";
import Spinner from "../../../../components/@vuexy/spinner/Fallback-spinner"

class LoginJWT extends React.Component {
  state = {
    username: "",
    password: "",
    remember: false,
    loading: false
  }

  handleLogin = async (e) => {
    e.preventDefault()
    this.setState({
      loading: true
    });
    const {loginActions} = this.props;
    const {loginWithJWT} = loginActions
    const user = {};
    user.username = this.state.username;
    user.password = this.state.password;
    user.remember = this.state.remember;
    const res = await loginWithJWT(user)
  }

  componentWillReceiveProps(nextProps, nextContext) {
    const {values} = nextProps;
    if (!values.isLogging) {
      this.setState({
        loading: false
      });
    }
  }

  autoLogin = (id) => {
    // e.preventDefault()
    this.setState({
      loading: true
    });

    const {loginActions} = this.props;
    const {loginWithJWT} = loginActions
    const user = {}
    switch (id) {
      case 1:
        user.username = "usertest@gmail.com";
        user.password = "matroi1230";
        break;
      case 2:
        user.username = "huynv@gmail.com";
        user.password = "matroi1230";
        break;
      case 3:
        user.username = "preview@gmail.com";
        user.password = "matroi1230";
        break
      default:
        break
    }
    loginWithJWT(user)
  }

  render() {
    const {loading} = this.state
    return (
      <React.Fragment>
        <CardBody className="pt-1">
          <Form action="/" onSubmit={this.handleLogin}>
            <FormGroup className="form-label-group position-relative has-icon-left">

              <Input
                type="email"
                placeholder="Email"
                // value={this.state.user.username}
                onChange={e => this.setState({username: e.target.value})}
                required
              />
              <div className="form-control-position">
                <Mail size={15}/>
              </div>
              <Label>Email</Label>
            </FormGroup>
            <FormGroup className="form-label-group position-relative has-icon-left">
              <Input
                type="password"
                placeholder="Password"
                // value={this.state.user.password}
                onChange={e => this.setState({password: e.target.value})}
                required
              />
              <div className="form-control-position">
                <Lock size={15}/>
              </div>
              <Label>Password</Label>
            </FormGroup>
            <FormGroup className="d-flex justify-content-between align-items-center">
              <Checkbox
                color="primary"
                icon={<Check className="vx-icon" size={16}/>}
                label="Remember me"
                defaultChecked={false}
                onChange={this.handleRemember}
              />
              <div className="float-right">
                <Link to="/pages/forgot-password">Forgot Password?</Link>
              </div>
            </FormGroup>
            <div className="d-flex justify-content-between">
              <Button.Ripple
                color="primary"
                outline
                onClick={() => {
                  history.push("/pages/register")
                }}
              >
                Register
              </Button.Ripple>
              {
                loading && <Spinner color="primary"/>
              }
              <Button.Ripple color="primary" type="submit">
                Login
              </Button.Ripple>


            </div>
            <br/>
            {/*<div className="d-flex justify-content-between ">*/}
              {/*<Suspense fallback={<Spinner/>}>*/}
                {/*<Button className="mr-2" outline color="primary" onClick={() => this.autoLogin(1)}>*/}
                  {/*Login with student*/}
                {/*</Button>*/}
              {/*</Suspense>*/}
              {/*<Button className="mr-2" outline color="primary" onClick={() => this.autoLogin(2)}>*/}
                {/*Login with admin*/}
              {/*</Button>*/}
              {/*<Button className="mr-2" outline color="primary" onClick={() => this.autoLogin(3)}>*/}
                {/*Login with preview*/}
              {/*</Button>*/}
            {/*</div>*/}
          </Form>
        </CardBody>
      </React.Fragment>
    )
  }
}

const mapStateToProps = state => {
  return {
    values: state.auth.login
  }
}
const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    loginActions: bindActionCreators(loginActions, dispatch)
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(LoginJWT)
