import * as Yup from "yup";

export const formSchema = Yup.object().shape({
    passwordOld: Yup.string()
      .required("current.pass.not.empty")
      .min(6,"current.pass.more.than.6.character")
      .max(45,"current.pass.longer.45.character")
      .matches(/^[^ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựýỳỵỷỹ]+$/,'current.pw.cannot.vietnam.char')
      .matches(/^(?=.*[a-zA-Z])(?=.*\d)[a-zA-Z\d\s<>()\[\]\\.,^%$#&*=+-_'"~`!{}|\/?;:@]{6,}$/,'current.pw.must.validate')
      .matches(/^[^\s]+$/,'current.pw.cannot.space.char'),
    passwordNew: Yup.string()
        .required("new.pass.not.empty")
        .min(6,"new.pass.more.than.6.character")
        .max(45,"new.pass.longer.45.character")
        .matches(/^[^ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựýỳỵỷỹ]+$/,'new.pw.cannot.vietnam.char')
        .matches(/^(?=.*[a-zA-Z])(?=.*\d)[a-zA-Z\d\s<>()\[\]\\.,^%$#&*=+-_'"~`!{}|\/?;:@]{6,}$/,'new.pw.must.validate')
        .matches(/^[^\s]+$/,'new.pw.cannot.space.char'),
    passwordConfirm: Yup.string()
        .required("re.password.is.required")
        .min(6,"re.pass.more.than.6.character")
        .max(45,"re.pass.longer.45.character")
        .matches(/^[^ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựýỳỵỷỹ]+$/,'new.pw.cannot.vietnam.char')
        .matches(/^(?=.*[a-zA-Z])(?=.*\d)[a-zA-Z\d\s<>()\[\]\\.,^%$#&*=+-_'"~`!{}|\/?;:@]{6,}$/,'new.pw.must.validate')
        .matches(/^[^\s]+$/,'new.pw.cannot.space.char')
        .when("passwordNew", {
            is: passwordNew => (passwordNew && passwordNew.length > 0 ? true : false),
            then: Yup.string().oneOf(
              [Yup.ref("passwordNew")],
              "re.password.same.new.pass"
            )
          }),
  });
