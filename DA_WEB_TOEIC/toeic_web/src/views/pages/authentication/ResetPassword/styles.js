
const styles = ({
    button:{
        width: '150px',
        height: '45px',
        borderRadius: '10px ! important',
        border: '#1ab7ea',
        fontSize: '16px',
         float:'right',
      textAlign: 'center'
    },
    icon:{
        marginLeft: '20px',
        textAlign: 'right',
        fontWeight: '800',
        fontStyle: 'normal',
        color: 'black',
        fontSize: '16px',
    },
    error: {
        color: 'red',
        fontSize: 12
    }
});

export default styles;
