import React, {Component} from 'react';
import {
  FormGroup,
  Button,
  Label,
  Col
} from "reactstrap"
import {withStyles} from "@material-ui/core"
import styles from "./styles"
import PropTypes from 'prop-types'
import {compose, bindActionCreators} from 'redux';
import changePasswordAction from "../../../../redux/actions/auth/changePasswordActions";
import {connect} from 'react-redux';
import {Formik, Field, Form} from "formik"
import {formSchema} from "./validate";
import Spinner from "reactstrap/es/Spinner";
import {history} from '../../../../history';
import {toast} from "react-toastify";
import {showMessage} from "../../../../commons/utils/convertDataForToast";
import {FormattedMessage} from "react-intl";

class ResetPasswordForm extends Component {
  state = {
    isLoading: false
  }

  componentWillReceiveProps(nextProps, nextContext) {
    const {loading} = nextProps;
    if (!loading) {
      this.setState({
        isLoading: false
      });
    }
  }

  render() {
    const {classes} = this.props;
    return (
      <>
        <style>{`
          #btnHighLight:focus-within {
            border: none !important;
            box-shadow: 0 0 1pt 1pt black !important;
          }

        `}

        </style>
        <Formik
          initialValues={{
            passwordOld: "",
            passwordNew: "",
            passwordConfirm: ""
          }}
          onSubmit={(values, actions) => {
            const {changePasswordActions} = this.props;
            const {changePassword} = changePasswordActions;
            const user = {};
            user.password = values.passwordNew;
            user.passwordOld = values.passwordOld;
            user.userRole = this.props.userRole;
            changePassword(user);
            setTimeout(() => {
              actions.setSubmitting(false);
            }, 1000);
          }}
          validationSchema={formSchema}
        >
          {({errors, touched, isValid, isSubmitting, values}) => (
            <Form>
              <FormGroup className="form-group d-flex">
                <Col xs={2}/>
                <Label className="text-right" for="required" xs={3}><FormattedMessage id="password.old"/> (<span
                  style={{color: "red"}}>*</span>) </Label>
                <Col xs={4}>
                  <div className="w-100">
                    <Field
                      type="password"
                      name="passwordOld"
                      className={`form-control ${errors.passwordOld &&
                      touched.passwordOld &&
                      "is-invalid "}`}
                    />
                    {errors.passwordOld && touched.passwordOld ? (
                      <div className={classes.error}><FormattedMessage id={errors.passwordOld}/></div>
                    ) : null}
                  </div>
                </Col>
              </FormGroup>
              <FormGroup className="form-group d-flex">
                <Col xs={2}/>
                <Label className="text-right" for="passwordNew" sm={3}><FormattedMessage id="password.new"/> (<span
                  style={{color: "red"}}>*</span>)</Label>
                <Col sm={4}>
                  <div className="w-100 ">
                    <Field
                      type="password"
                      name="passwordNew"
                      className={`form-control ${errors.passwordNew &&
                      touched.passwordNew &&
                      "is-invalid"}`}
                    />
                    {errors.passwordNew && touched.passwordNew ? (
                      <div className={classes.error}><FormattedMessage id={errors.passwordNew}/></div>
                    ) : null}
                  </div>
                </Col>

              </FormGroup>

              <FormGroup className="form-group d-flex">
                <Col xs={2}/>
                <Label className="text-right" for="passwordConfirm" sm={3}><FormattedMessage id="password.new.confirm"/> (<span
                  style={{color: "red"}}>*</span>)</Label>
                <Col sm={4}>
                  <div className="w-100 ">
                    <Field
                      type="password"
                      name="passwordConfirm"
                      className={`form-control ${errors.passwordConfirm &&
                      touched.passwordConfirm &&
                      "is-invalid"}`}
                    />
                    {errors.passwordConfirm && touched.passwordConfirm ? (
                      <div className={classes.error}><FormattedMessage id={errors.passwordConfirm}/></div>
                    ) : null}
                  </div>
                </Col>
              </FormGroup>
              <FormGroup className="form-group d-flex">
                <Col xs={2}/>
                <Col xs={3}>
                  <Button
                    id="btnHighLight"
                    className={classes.button}
                    onClick={() => {
                      if(this.props.userRole === "STUDENT"){
                        history.push("/")
                      } else {
                        history.push("/private/admin/")
                      }
                    }}
                  >
                    <FormattedMessage id="cancel"/>
                  </Button>
                </Col>
                <Col xs={1}>
                  {this.props.isLoading ? <Spinner color="primary" style={{marginLeft:'90%'}}/> : null}
                </Col>
                <Col xs={3}>
                  <Button
                    id="btnHighLight"
                    className={classes.button}
                    color="primary"
                    type="submit"
                    disabled={!isValid || isSubmitting}
                  >
                    OK
                  </Button>
                </Col>
              </FormGroup>
            </Form>

          )}
        </Formik>
      </>
    );
  }
}

ResetPasswordForm.propTypes = {
  classes: PropTypes.object,
  changePasswordActions: PropTypes.shape({
    changePassword: PropTypes.func
  })
}

const mapStateToProps = state => {
  return {
    isLoading: state.auth.changePassword.loading,
    userRole: state.auth.login.userRole,
  }
};

const mapDispatchToProps = dispatch => ({
  changePasswordActions: bindActionCreators(changePasswordAction, dispatch)
});

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
);

export default compose(
  withStyles(styles),
  withConnect
)(ResetPasswordForm);
