import React from "react"
import {
    Card,
    CardHeader,
    CardTitle,
    CardBody,
    Row,
    Col,
} from "reactstrap"
import { withStyles, Typography } from "@material-ui/core"
import styles from "./styles"
import ResetPasswordForm from "./ResetPasswordForm"
import {FormattedMessage} from "react-intl";

class ResetPassword extends React.Component {

    render() {
        return (
            <Row className="m-0 justify-content-center">
                <Col
                    className=" justify-content-center"
                >
                    <Card className="bg-authentication rounded-0 mb-0 w-100">
                        <Row className="m-0">
                            <Col sm="12" md="12" >
                                <Card className="rounded-0 mb-0 px-2 py-50">
                                    <CardHeader className="w-100 mb-2 text-center">
                                        <CardTitle className="w-100">
                                            <Row className="">
                                                <Col className="text-center">
                                                    <Typography variant="h4" gutterBottom><FormattedMessage id="change.pass"/></Typography>
                                                </Col>
                                            </Row>
                                        </CardTitle>
                                    </CardHeader>
                                    <CardBody className="pt-1">
                                        <ResetPasswordForm />
                                    </CardBody>
                                </Card>
                            </Col>
                        </Row>
                    </Card>
                </Col>
            </Row>
        )
    }
}
export default withStyles(styles)(ResetPassword);
