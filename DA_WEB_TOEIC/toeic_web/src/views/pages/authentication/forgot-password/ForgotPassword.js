import React from 'react';
import {Field, Form, Formik} from "formik"
import Spinner from "reactstrap/es/Spinner";
import "../../../../assets/css/style-forgot-password.css"
import * as Yup from "yup";
import * as emailManagement from "../../../../redux/actions/email/index";
import {connect} from "react-redux";
import {Col, Row} from "reactstrap";
import * as Styles from "./style-forgot";
import {showMessage} from "../../../../commons/utils/convertDataForToast";
import {FormattedMessage} from "react-intl";

class ForgotComponent extends React.Component {

  state = {
    isLoading: false
  }

  showLoading = () => {
    this.setState({
      isLoading: true
    })
  }

  componentWillReceiveProps(nextProps, nextContext) {
    this.setState({
      isLoading: false
    })
  }

  render() {
    const {isLoading} = this.state

    return (
      <>
        <style>{`
          #btnHighLight:focus-within {
            border: 1px solid black !important;
            padding: 3px;
            text-shadow: 2px 2px 5px gray;
          }

          #btnSubmit:focus-within {
            border: 1px solid black !important;
            text-shadow: 2px 2px 5px gray;
          }
        `}</style>

        <div className="container-fluid">
          <Formik
            initialValues={{
              username: ""
            }}
            onSubmit={(values) => {
              debugger
              this.showLoading()
              this.props.sendEmail(values.username.toLowerCase().trim());
            }}
            validationSchema={Yup.object().shape({
              username: Yup.string()
                .required(
                  // 'Email không được bỏ trống!'
                  "email.is.required"
                )
                .min(6,
                  // 'Email phải chứa ít nhất 6 ký tự!'
                 "email.must.contain.at.least.6.characters"
                )
                .max(45,
                  // 'Email không được dài quá 45 ký tự!'
                 "email.cant.be.longer.than.45.characters"
                )
                .matches(/^(([^<>()\[\]\\.,^%$#&*='\/?;:\s@!"ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựýỳỵỷỹ]+(\.[^<>!()\[\]\\.,^%$#&*='\/?;:\s@"ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳýỵỷỹ]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z0-9/-_.^-]+\.)+[a-zA-Z]{2,}))$/,
                  // 'Email không đúng định dạng!'
                 "incorrect.email.format"
                ),
            })}
          >
            {({errors}) => (
              <Row>
                <Styles.ColLoginForm xs="12" sm="12" md="8" lg="6" m="2" p="2">
                  <Styles.RowTitle><Styles.StyleCardTitle>
                    {/*Lấy lại mật khẩu*/}
                    <FormattedMessage id="recover.password" tagName="data" />
                  </Styles.StyleCardTitle></Styles.RowTitle>
                  <Form>
                    <div>
                      <div className="m-auto text-center">
                        <Styles.StyleRow>

                          <Styles.ColLable xs="12" sm="12" md="4" lg="4">
                            <Styles.StyleLable>
                              {/*Email đăng nhập */}
                              <FormattedMessage id="email.for.login" tagName="data" />{' '}
                              (<span style={{color: "red"}}>*</span>)</Styles.StyleLable>
                          </Styles.ColLable>
                          <Col xs="12" sm="12" md="8" lg="8">
                            <Field autoFocus={true} type="email" className="form-control" name="username" maxLength={45}
                                   max="5" min="1"></Field>
                            {errors.username ?
                              <strong><label
                                style={{color: "red", float: "left"}}><FormattedMessage id={errors.username}/></label></strong> : null}
                          </Col>
                        </Styles.StyleRow>
                      </div>
                      <div className="m-auto text-center">
                        <Styles.RowLink>
                          <Styles.ColLink xs="5" sm="5" md="5" lg="5">
                            <Styles.StyleLink id="btnHighLight" to="/pages/login">
                              {/*Đăng nhập*/}
                              <FormattedMessage id="login" tagName="data" />
                            </Styles.StyleLink>
                          </Styles.ColLink>
                          <Styles.ColSpinner xs="2" sm="2" md="2" lg="2">
                            {isLoading && <Spinner color="primary"/>}
                          </Styles.ColSpinner>
                          <Styles.ColLink xs="5" sm="5" md="5" lg="5" p="0 4">
                            <button id="btnSubmit" className="btn btn-info" type="submit" right
                                                  disabled={errors.username ? true : false}
                                                  autofocus>OK</button>
                          </Styles.ColLink>
                        </Styles.RowLink>
                      </div>
                    </div>
                  </Form>
                </Styles.ColLoginForm>
              </Row>
            )}
          </Formik>
        </div>
      </>

    );
  }

  //   return (
  //     <>
  //       <style>{`
  //         #btnHighLight:focus-within {
  //           border: 1px solid black !important;
  //           padding: 3px;
  //           text-shadow: 2px 2px 5px gray;
  //         }
  //
  //         #btnSubmit:focus-within {
  //           border: 1px solid black !important;
  //           text-shadow: 2px 2px 5px gray;
  //         }
  //       `}</style>
  //       <div className="container-fluid ">
  //         <div className="row m-0">
  //           <div className="col-xs-0 col-sm-1 col-md-2 col-lg-2 col-xl-3"></div>
  //           <div className="col-xs-12 col-sm-10 col-md-8 col-lg-8 col-xl-6 content">
  //             <div className="row title">
  //               <h3><strong>Lấy lại mật khẩu</strong></h3>
  //             </div>
  //             <div className="row formContent">
  //               <Formik
  //                 initialValues={{
  //                   userName: ""
  //                 }}
  //
  //                 validationSchema={Yup.object().shape({
  //                   userName: Yup.string().trim()
  //                     .email("Email ko đúng định dạng")
  //                     .required("Không được phép để trống")
  //                     .max(44, "Nhập tối đa 45 ký tự")
  //                 })}
  //
  //                 onSubmit={(values) => {
  //                   this.props.sendEmail(values.userName.toLowerCase().trim())
  //                 }}
  //               >
  //                 {({errors}) => (
  //                   <Form>
  //                     <div style={{paddingLeft: '10%'}} className="row">
  //                       <div className="col-xs-12 col-sm-12 col-md-4 col-lg-4">
  //                         <label>Email đăng nhập (<span style={{color: "red"}}>*</span>)</label>
  //                       </div>
  //                       <div style={{paddingRight: '10%'}} className="col-xs-12 col-sm-12 col-md-8 col-lg-8">
  //                         <Field autoFocus={true} type="text" class="form-control" name="userName"
  //                                placeholder="Email đăng nhập..."></Field>
  //                       </div>
  //                     </div>
  //
  //                     <div style={{paddingLeft: '42%'}} className="row">
  //                       {errors.userName ? <label
  //                         style={{color: "red", width: "100%"}}><strong>{errors.userName}</strong></label> : null}
  //                     </div>
  //
  //                     <div className="row" style={{paddingLeft: '10%'}}>
  //                       <div className="col-xs-4 col-sm-4 col-md-4 col-lg-4" style={{width: "40%"}}>
  //                         <Link id="btnHighLight" to="/pages/login">Đăng nhập</Link>
  //                       </div>
  //                       <div className="col-xs-4 col-sm-4 col-md-4 col-lg-4 spin" style={{width: "20%"}}>
  //                         {this.state.isLoading && <Spinner color="primary"/>}
  //                       </div>
  //                       <div className="col-xs-4 col-sm-4 col-md-4 col-lg-4"
  //                            style={{width: "40%", paddingRight: '10%'}}>
  //                         {
  //                           errors.userName ?
  //                             <button id="btnSubmit" type="submit" className="btn btn-info" disabled="disabled">OK</button> :
  //                             <button id="btnSubmit" type="submit" className="btn btn-info" onClick={this.showLoading}>OK</button>
  //                         }
  //                       </div>
  //                     </div>
  //                   </Form>
  //                 )}
  //               </Formik>
  //             </div>
  //           </div>
  //           <div className="col-xs-0 col-sm-1 col-md-2 col-lg-2 col-xl-3"></div>
  //         </div>
  //       </div>
  //     </>
  //
  //   );
  // }
}

const mapStateToProps = (state) => {
  return {
    forgotPassReducer: state.forgotPasswordReducer
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    sendEmail: (email) => {
      dispatch(emailManagement.sendEmail(email));
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ForgotComponent)
