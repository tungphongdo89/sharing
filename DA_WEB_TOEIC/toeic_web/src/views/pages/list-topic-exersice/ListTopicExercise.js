import {Button, Card, CardBody, CardHeader, CardTitle, Col, Collapse, Container, Row, Spinner} from "reactstrap"
import React, {Component} from 'react';
import {BrowserRouter as Router, Link} from "react-router-dom";
import * as Icon from "react-bootstrap-icons"
import styled from "styled-components"
import topicActions from "../../../redux/actions/topic/topicAction";
import {bindActionCreators} from 'redux';
import {connect} from "react-redux";
import {history} from "../../../history";
import practiceListenMultiActions from "../../../redux/actions/practice/listening";
import readingActions from "../../../redux/actions/practice/reading";
import historyPracticesAction from "../../../redux/actions/history-practices"
import {CODE_PART1_LF, CODE_PART2_LF, CODE_PART3_LF, CODE_PART4_LF, CODE_PART1,CODE_PART2,CODE_PART3,CODE_PART4,CODE_PART5,CODE_PART6,CODE_PART7,CODE_PART8} from "../../../commons/constants/CONSTANTS";
import {FormattedMessage} from "react-intl";


const CartStyle = styled(Card)`
  &&&{
    margin-bottom: 20px;
    opacity : ${props => props.opac ? 0.4 : 1};
  }
`

const CardHeaderStyle = styled(CardHeader)`
 &&&{
    background: #169bd5;
    border-radius: 10px;
  }
`;

const CardTitleStyle = styled(CardTitle)`
 &&&{
    margin: 0;
    position: relative;
    color: white;
    font-weight: bold;
    width : 100%;
 }
`;

const IconCaretUpStyle = styled(Icon.CaretUp)`
  &&&{
    position: absolute;
    right: 0;
    top: -10%;
    font-size: 25px;
    border-color: white;
  }
`;

const IconCaretDownStyle = styled(Icon.CaretDown)`
  &&&{
    position: absolute;
    right: 0;
    top: -10%;
    font-size: 25px;
    border-color: white;
  }
`;

const CardBodyStyle = styled(CardBody)`
  &&&{
    padding: 45px 20px 0;
    background: #ceeaf6;
  }
`;

const ColItemStyle = styled(Col)`
  &&&{
    text-align: center;
  }
`;

const ButtonStyle = styled(Button)`
  &&&{
    width: 80%;
    padding: 20px 0;
    margin: 0 auto 50px;
    border: 2px dashed #169bd5;
    border-radius: 20px;
    text-align: center;
    background: white;
  }
`;

const LinkStyle = styled(Link)`
  &&&{
    text-decoration: none;
    color: black;
  }
`;

const TitleMain = styled.h2`
  &&&{
    margin-bottom : 30px;
  }
`;

const SpinnerStyle = styled(Spinner)`
  position: absolute;
    z-index: 1;
    top: 50%;
    left: 50%
`;

class ListTopicExcercise extends Component {

  constructor(props) {
    super(props);
    this.focusItem = React.createRef()
    this.state = {
      isOpenEasy : true,
      isOpenMedium : false,
      isOpenDifficult : false,
      isLoading : true
    }
  }

  getPartExerciseByURL = () => {
    let pathURL = this.props.location.pathname
    let arr = pathURL.split("/").reverse()
    let stringPartExercise = ""
    switch (arr[0]) {
      // Bài đọc
      case "complete-sentence" :
        stringPartExercise = CODE_PART5
        break
      case "complete-passage" :
        stringPartExercise = CODE_PART6
        break
      case "single-paragraph" :
        stringPartExercise = CODE_PART7
        break
      case "dual-paragraph" :
        stringPartExercise = CODE_PART8
        break

      // Bài nghe
      case "painting" :
        stringPartExercise = CODE_PART1
        break
      case "question-answer" :
        stringPartExercise = CODE_PART2
        break
      case "conversation" :
        stringPartExercise = CODE_PART3
        break
      case "short-story" :
        stringPartExercise = CODE_PART4
        break

      //bài nghe điền từ
      case "photographs":
        stringPartExercise = CODE_PART1_LF
        break
      case "question-response":
        stringPartExercise = CODE_PART2_LF
        break
      case "short-conversations":
        stringPartExercise = CODE_PART3_LF
        break
      case "short-talks":
        stringPartExercise = CODE_PART4_LF
        break
    }
    return stringPartExercise
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if(prevProps.location.pathname !== this.props.location.pathname){
      this.setState({
        isOpenEasy : true,
        isLoading : true
      })
      let paramCode = this.getPartExerciseByURL()
      this.props.topicAction.fetchTopicByPartAndLevel(paramCode,this.props.paramValue)
    }
  }


  componentDidMount() {
    let paramCode = this.getPartExerciseByURL()
    this.props.topicAction.fetchTopicByPartAndLevel(paramCode,this.props.paramValue)
    this.focusItem.current.scrollIntoView({behavior:"smooth"})
  }

  componentWillReceiveProps(nextProps, nextContext) {
    // Lần vào component đầu tiên , các lần thay đổi loại bài sẽ ko lm thay đổi props.isLoadingGetListTopic
    // Thay đổi isLoading từ true về false để tắt loading
    if(nextProps.isLoadingGetListTopic !== this.props.isLoadingGetListTopic || nextProps.topicsByPart !== this.props.topicsByPart){
      this.setState({
        isLoading : false
      })
    }
    if(this.props.location.pathname !== nextProps.location.pathname){
      this.focusItem.current.scrollIntoView({behavior:"smooth"})
    }
  }

  toggleCollapseEasy = () => {
    this.setState({
      isOpenEasy : !this.state.isOpenEasy,
      isOpenMedium: false,
      isOpenDifficult: false,
    })
  }

  toggleCollapseMedium = () => {
    this.setState({
      isOpenMedium: !this.state.isOpenMedium,
      isOpenEasy : false,
      isOpenDifficult: false,
    })
  }

  toggleCollapseDifficult = () => {
    this.setState({
      isOpenDifficult: !this.state.isOpenDifficult,
      isOpenMedium: false,
      isOpenEasy : false,
    })
  }

  render() {
    let pathURL = this.props.location.pathname
    let arr = pathURL.split("/").reverse()
    let stringTypeExercise = arr[0]
    let stringSpeciesExercise = arr[1]

    switch (stringTypeExercise) {
      // Bài đọc
      case "complete-sentence" :
        stringTypeExercise = <FormattedMessage id="part5"/>
        break
      case "complete-passage" :
        stringTypeExercise = <FormattedMessage id="part6"/>
        break
      case "single-paragraph" :
        stringTypeExercise = <FormattedMessage id="part7"/>
        break
      case "dual-paragraph" :
        stringTypeExercise = <FormattedMessage id="part8"/>
        break

      // Bài nghe
      case "painting" :
        stringTypeExercise = <FormattedMessage id="part1"/>
        break
      case "question-answer" :
        stringTypeExercise = <FormattedMessage id="part2"/>
        break
      case "conversation" :
        stringTypeExercise = <FormattedMessage id="part3"/>
        break
      case "short-story" :
        stringTypeExercise = <FormattedMessage id="part4"/>
        break

      //Bài nghe điền từ
      case "photographs":
        stringTypeExercise = <FormattedMessage id="part1"/>
        break
      case "question-response":
        stringTypeExercise = <FormattedMessage id="part2"/>
        break
      case "short-conversations":
        stringTypeExercise = <FormattedMessage id="part3"/>
        break
      case "short-talks":
        stringTypeExercise = <FormattedMessage id="part4"/>
        break
    }

    switch (stringSpeciesExercise) {
      case "read" :
        stringSpeciesExercise = <FormattedMessage id="reading.unit"/>
        break
      case "listen" :
        stringSpeciesExercise = <FormattedMessage id="listening.unit"/>
        break
      case "listen-fill" :
        stringSpeciesExercise = <FormattedMessage id="listening.fill.unit"/>
        break
      case "translationAV" :
        stringSpeciesExercise =<FormattedMessage id="trans.eng.to.viet"/>
        break
      case "translationVA" :
        stringSpeciesExercise =<FormattedMessage id="trans.viet.to.eng"/>
        break
    }

    let renderCollapseEasy = this.props.topicsByPart[0] ? (this.props.topicsByPart[0].length <= 0 ? <h5 style={{color : "grey" , width : "100%" , textAlign : "center"}}><FormattedMessage id={"not.found.data"}/></h5> : this.props.topicsByPart[0].map((collapseItem , index) => {
      const {multiListenQuestionAction, readingAction} = this.props
      return (
        <ColItemStyle xs="12" sm="6" md="4" lg="3" xl="3" key={index}>
          <ButtonStyle onClick={() => {
            const {readingAction} = this.props;
            let Part = this.getPartExerciseByURL();
            const {getListSingleAndDualReadingQuestion, getPart} = readingAction
            getPart(Part)
            const data ={}
            switch(Part){
              case CODE_PART1_LF: {
                const {multiListenQuestionAction} = this.props;
                const {getListQuestionOfListeningWordFill} = multiListenQuestionAction;
                multiListenQuestionAction.cleanListQuestionOfListeningWordFill();
                multiListenQuestionAction.cleanResultQuestionOfListeningWordFill();
                multiListenQuestionAction.cleanNumberCorrectAnswerWordFill();
                multiListenQuestionAction.cleanListQuestionAndAnswerListening();
                const lstPart1 ={}
                lstPart1.topicName = collapseItem.topicName;
                lstPart1.levelCode ="EASY";
                lstPart1.part =CODE_PART1_LF;
                getListQuestionOfListeningWordFill(lstPart1);
                this.props.practiceListen.getTopicId(collapseItem.topicId,"EASY")
                multiListenQuestionAction.getNameTopic(collapseItem.topicName);
                multiListenQuestionAction.getPartPracticeListening(CODE_PART1_LF)
                history.push("/pages/listeningWordFill");
                break
              }

              case CODE_PART1:{
                const {multiListenQuestionAction,multiListeningReducer, historyPracticesAction} = this.props;
                const {cleanHiddenBtnBeforeGetLisSingle}= historyPracticesAction;
                cleanHiddenBtnBeforeGetLisSingle();

                const {resetCountCheckListeningPractices} = multiListenQuestionAction;
                resetCountCheckListeningPractices();


                const {getListListeningQuestionOfMulti}= multiListenQuestionAction;
                const {topicId, levelCode} = multiListeningReducer
                multiListenQuestionAction.getNameTopic(collapseItem.topicName);
                this.props.practiceListen.getTopicId(collapseItem.topicId,"EASY")
                multiListenQuestionAction.getPartPracticeListening(CODE_PART1)
                multiListenQuestionAction.clearTempQuestionAnswer();
                multiListenQuestionAction.clearTotalCorrect();
                multiListenQuestionAction.clearAnsweredMultiListenQuest();
                getListListeningQuestionOfMulti(collapseItem.topicId,"EASY");
                history.push("/pages/practice-listening-painting")
                break
              }
              case CODE_PART2_LF:{
                const {multiListenQuestionAction} = this.props;
                const {getListQuestionOfListeningWordFill, getNameTopic} = multiListenQuestionAction;
                multiListenQuestionAction.cleanListQuestionOfListeningWordFill();
                multiListenQuestionAction.cleanResultQuestionOfListeningWordFill();
                multiListenQuestionAction.cleanNumberCorrectAnswerWordFill();
                multiListenQuestionAction.cleanListQuestionAndAnswerListening();
                const lst ={}
                lst.topicName = collapseItem.topicName;
                lst.levelCode ="EASY";
                lst.part =CODE_PART2_LF;
                getListQuestionOfListeningWordFill(lst);
                getNameTopic(collapseItem.topicName);
                multiListenQuestionAction.getPartPracticeListening(CODE_PART2_LF)
                this.props.practiceListen.getTopicId(collapseItem.topicId,"EASY")
                history.push("/pages/listeningWordFill");
                break
              }
              case CODE_PART2:{
                const {multiListenQuestionAction,multiListeningReducer, historyPracticesAction} = this.props;
                const {cleanHiddenBtnBeforeGetLisSingle}= historyPracticesAction;
                cleanHiddenBtnBeforeGetLisSingle();

                const {resetCountCheckListeningPractices} = multiListenQuestionAction;
                resetCountCheckListeningPractices();

                const {getNameTopic,getListListeningQuestionOfMulti}= multiListenQuestionAction;
                const {topicId, levelCode} = multiListeningReducer
                getNameTopic(collapseItem.topicName);
                this.props.practiceListen.getTopicId(collapseItem.topicId, "EASY")
                multiListenQuestionAction.getPartPracticeListening(CODE_PART2)
                multiListenQuestionAction.clearTempQuestionAnswer();
                multiListenQuestionAction.clearTotalCorrect();
                multiListenQuestionAction.clearAnsweredMultiListenQuest();
                getListListeningQuestionOfMulti(collapseItem.topicId,"EASY");
                history.push("/pages/practice-listening-question-answer")
                break
              }
              case CODE_PART3_LF:{
                const {multiListenQuestionAction} = this.props;
                const {getListQuestionOfListeningWordFill, getNameTopic} = multiListenQuestionAction;
                multiListenQuestionAction.cleanListQuestionOfListeningWordFill();
                multiListenQuestionAction.cleanResultQuestionOfListeningWordFill();
                multiListenQuestionAction.cleanNumberCorrectAnswerWordFill();
                multiListenQuestionAction.cleanListQuestionAndAnswerListening();
                const lst ={}
                lst.topicName = collapseItem.topicName;
                lst.levelCode ="EASY";
                lst.part =CODE_PART3_LF;
                getListQuestionOfListeningWordFill(lst);
                getNameTopic(collapseItem.topicName);
                multiListenQuestionAction.getPartPracticeListening(CODE_PART3_LF)
                this.props.practiceListen.getTopicId(collapseItem.topicId,"EASY")
                history.push("/pages/listeningWordFill");
                break
              }
              case "PART3": {
                debugger
                const {multiListenQuestionAction, historyPracticesAction} = this.props;
                const {fetchListeningToeicConverstation, cleanBeforeGetDetailHL} = multiListenQuestionAction;
                cleanBeforeGetDetailHL();
                const {showButtonSaveResultListening} = historyPracticesAction;
                showButtonSaveResultListening();

                const lst ={}
                lst.topicId = collapseItem.topicId;
                lst.part = "PART3";
                lst.levelCode ="EASY";
                fetchListeningToeicConverstation(lst);
                // getNameTopic(collapseItem.topicName);
                history.push("/pages/practice-listening-toeic-conversation");
                break
              }
              case CODE_PART4_LF:{
                const {multiListenQuestionAction} = this.props;
                const {getListQuestionOfListeningWordFill, getNameTopic} = multiListenQuestionAction;
                multiListenQuestionAction.cleanListQuestionOfListeningWordFill();
                multiListenQuestionAction.cleanResultQuestionOfListeningWordFill();
                multiListenQuestionAction.cleanNumberCorrectAnswerWordFill();
                multiListenQuestionAction.cleanListQuestionAndAnswerListening();
                const lst ={}
                lst.topicName = collapseItem.topicName;
                lst.levelCode ="EASY";
                lst.part =CODE_PART4_LF;
                getListQuestionOfListeningWordFill(lst);
                getNameTopic(collapseItem.topicName);
                multiListenQuestionAction.getPartPracticeListening(CODE_PART4_LF)
                this.props.practiceListen.getTopicId(collapseItem.topicId,"EASY")
                history.push("/pages/listeningWordFill");
                break
              }
              case "PART4": {
                debugger
                if(this.props.paramValue === "2"){
                  const {multiListenQuestionAction, historyPracticesAction} = this.props;
                  const {fetchListeningToeicConverstation, cleanBeforeGetDetailHL} = multiListenQuestionAction;
                  cleanBeforeGetDetailHL();

                  const {showButtonSaveResultListening} = historyPracticesAction;
                  showButtonSaveResultListening();

                  const lst ={}
                  lst.topicId = collapseItem.topicId;
                  lst.part = "PART4";
                  lst.levelCode ="EASY";
                  fetchListeningToeicConverstation(lst);
                  // getNameTopic(collapseItem.topicName);
                  history.push("/pages/practice-listening-toeic-short-talking");
                  break
                }
                else {}
                }
              case CODE_PART5:
                const {getListQuestionOfReadingWordFill, getNameTopic} = readingAction;
                const lst ={};
                readingAction.cleanListQuestionOfReadingCompletedPassage();
                readingAction.cleanResultQuestionOfReadingCompletedPassage();
                readingAction.cleaningListQuestionAndAnswerReadingCompletedPassage();
                readingAction.cleanNumberCorrectAnswer();
                readingAction.setValueIndex();
                lst.topicName = collapseItem.topicName;
                lst.levelCode ="EASY";
                getListQuestionOfReadingWordFill(lst);
                getNameTopic(collapseItem.topicName);
                getPart(CODE_PART5);
                readingAction.getTopicId(collapseItem.topicId)
                readingAction.getLevelCode('EASY');
                history.push("/pages/readingWordFill");
                break
              case CODE_PART6:
                // const {readingAction} = this.props;
                const { getListQuestionOfReadingCompletedPassage, getNameTopicPassage, getLevelCode} = readingAction;
                let quest = {};
                readingAction.cleanListQuestionOfReadingCompletedPassage();
                readingAction.cleanResultQuestionOfReadingCompletedPassage();
                readingAction.cleaningListQuestionAndAnswerReadingCompletedPassage();
                readingAction.cleanNumberCorrectAnswer();
                readingAction.setValueIndex();
                quest.topicName = collapseItem.topicName;
                quest.levelCode = "EASY";
                getListQuestionOfReadingCompletedPassage(quest);
                getPart(CODE_PART6);
                getNameTopicPassage(collapseItem.topicName);
                readingAction.getTopicId(collapseItem.topicId)
                getLevelCode('EASY');
                history.push("/pages/practice-reading-completed-passage")
                break
              case CODE_PART7: 
                data.topicId = collapseItem.topicId
                data.part = "PART7"
                data.levelCode = "EASY"
                readingAction.storeDataToRepracticeReading(data);
                readingAction.clearTempReadingSingleAndDualQuestion();
                readingAction.clearTotalTrueReadingSingleAndDual();
                readingAction.cleaningListQuestionAndAnswerReadingCompletedPassage();
                getListSingleAndDualReadingQuestion(data)
                readingAction.setPartPracticeReading(CODE_PART7);
                readingAction.getLevelCode('EASY');
                readingAction.getNameTopic(collapseItem.topicName);
                readingAction.getTopicId(collapseItem.topicId)
                history.push("/pages/practice-reading-single")
                break
              case CODE_PART8:
                data.topicId = collapseItem.topicId
                data.part = CODE_PART8
                data.levelCode = "EASY"
                readingAction.storeDataToRepracticeReading(data);
                readingAction.clearTempReadingSingleAndDualQuestion();
                readingAction.clearTotalTrueReadingSingleAndDual();
                readingAction.cleaningListQuestionAndAnswerReadingCompletedPassage();
                getListSingleAndDualReadingQuestion(data)
                readingAction.getLevelCode('EASY');
                readingAction.getNameTopic(collapseItem.topicName);
                readingAction.getTopicId(collapseItem.topicId)
                readingAction.setPartPracticeReading(CODE_PART8);
                history.push("/pages/practice-reading-dual")
                break
              default:
            }
          }} color="default">
            <LinkStyle to="">{collapseItem.topicName}</LinkStyle>
          </ButtonStyle>
        </ColItemStyle>
      )
    })) : null

    let renderCollapseMedium = this.props.topicsByPart[1] ? (this.props.topicsByPart[1].length <= 0 ? <h5 style={{color : "grey" , width : "100%" , textAlign : "center"}}><FormattedMessage id={'not.found.data'}/></h5> : this.props.topicsByPart[1].map((collapseItem , index) => {
      const {multiListenQuestionAction, readingAction} = this.props
      return (
        <ColItemStyle xs="12" sm="6" md="4" lg="3" xl="3" key={index}>
          <ButtonStyle onClick={() => {
            let Part = this.getPartExerciseByURL();
            const {getListSingleAndDualReadingQuestion, getPart} = readingAction
            getPart(Part)
            const data ={}
            debugger
            switch(Part){
              case CODE_PART1_LF:{
                const {multiListenQuestionAction} = this.props;
                const {getListQuestionOfListeningWordFill, getNameTopic} = multiListenQuestionAction;
                multiListenQuestionAction.cleanListQuestionOfListeningWordFill();
                multiListenQuestionAction.cleanResultQuestionOfListeningWordFill();
                multiListenQuestionAction.cleanNumberCorrectAnswerWordFill();
                multiListenQuestionAction.cleanListQuestionAndAnswerListening();
                const lst ={}
                lst.topicName = collapseItem.topicName;
                lst.levelCode ="MEDIUM";
                lst.part =CODE_PART1_LF;
                getListQuestionOfListeningWordFill(lst);
                getNameTopic(collapseItem.topicName);
                multiListenQuestionAction.getPartPracticeListening(CODE_PART1_LF)
                this.props.practiceListen.getTopicId(collapseItem.topicId,"MEDIUM")
                history.push("/pages/listeningWordFill");
                break
              }
              case CODE_PART1:
              {
                debugger
                  const {multiListenQuestionAction,multiListeningReducer, historyPracticesAction} = this.props;
                  const {cleanHiddenBtnBeforeGetLisSingle}= historyPracticesAction;
                  cleanHiddenBtnBeforeGetLisSingle();

                const {resetCountCheckListeningPractices} = multiListenQuestionAction;
                resetCountCheckListeningPractices();

                  const {getNameTopic,getListListeningQuestionOfMulti}= multiListenQuestionAction;
                  const {topicId, levelCode} = multiListeningReducer
                  getNameTopic(collapseItem.topicName);
                  this.props.practiceListen.getTopicId(collapseItem.topicId, "MEDIUM")
                  multiListenQuestionAction.getPartPracticeListening(CODE_PART1)
                  multiListenQuestionAction.clearTempQuestionAnswer();
                  multiListenQuestionAction.clearTotalCorrect();
                  multiListenQuestionAction.clearAnsweredMultiListenQuest();
                  // getListListeningQuestionOfMulti(topicId,levelCode);
                  getListListeningQuestionOfMulti(collapseItem.topicId,"MEDIUM");
                  history.push("/pages/practice-listening-painting")
                break
                }
              case CODE_PART2_LF:{
                const {multiListenQuestionAction} = this.props;
                const {getListQuestionOfListeningWordFill, getNameTopic} = multiListenQuestionAction;
                multiListenQuestionAction.cleanListQuestionOfListeningWordFill();
                multiListenQuestionAction.cleanResultQuestionOfListeningWordFill();
                multiListenQuestionAction.cleanNumberCorrectAnswerWordFill();
                multiListenQuestionAction.cleanListQuestionAndAnswerListening();
                const lst ={}
                lst.topicName = collapseItem.topicName;
                lst.levelCode ="MEDIUM";
                lst.part =CODE_PART2_LF;
                getListQuestionOfListeningWordFill(lst);
                getNameTopic(collapseItem.topicName);
                multiListenQuestionAction.getPartPracticeListening(CODE_PART2_LF)
                this.props.practiceListen.getTopicId(collapseItem.topicId,"MEDIUM")
                history.push("/pages/listeningWordFill");
                break
              }
              case CODE_PART2:
              {
                debugger
                  const {multiListenQuestionAction,multiListeningReducer, historyPracticesAction} = this.props;
                  const {cleanHiddenBtnBeforeGetLisSingle}= historyPracticesAction;
                  cleanHiddenBtnBeforeGetLisSingle();

                const {resetCountCheckListeningPractices} = multiListenQuestionAction;
                resetCountCheckListeningPractices();

                  const {getNameTopic,getListListeningQuestionOfMulti}= multiListenQuestionAction;
                  const {topicId, levelCode} = multiListeningReducer
                  getNameTopic(collapseItem.topicName);
                  this.props.practiceListen.getTopicId(collapseItem.topicId, "MEDIUM")
                  multiListenQuestionAction.getPartPracticeListening(CODE_PART2)
                  multiListenQuestionAction.clearTempQuestionAnswer();
                  multiListenQuestionAction.clearTotalCorrect();
                  multiListenQuestionAction.clearAnsweredMultiListenQuest();
                  // getListListeningQuestionOfMulti(topicId,levelCode);
                  getListListeningQuestionOfMulti(collapseItem.topicId,"MEDIUM");
                  history.push("/pages/practice-listening-question-answer")
                  break
                }
              case CODE_PART3_LF:{
                const {multiListenQuestionAction} = this.props;
                const {getListQuestionOfListeningWordFill, getNameTopic} = multiListenQuestionAction;
                multiListenQuestionAction.cleanListQuestionOfListeningWordFill();
                multiListenQuestionAction.cleanResultQuestionOfListeningWordFill();
                multiListenQuestionAction.cleanNumberCorrectAnswerWordFill();
                multiListenQuestionAction.cleanListQuestionAndAnswerListening();
                const lst ={}
                lst.topicName = collapseItem.topicName;
                lst.levelCode ="MEDIUM";
                lst.part =CODE_PART3_LF;
                getListQuestionOfListeningWordFill(lst);
                getNameTopic(collapseItem.topicName);
                multiListenQuestionAction.getPartPracticeListening(CODE_PART3_LF)
                this.props.practiceListen.getTopicId(collapseItem.topicId,"MEDIUM")
                history.push("/pages/listeningWordFill");
                break
              }
              case "PART3":
              {
                  if(this.props.paramValue === "2"){
                    const {multiListenQuestionAction, historyPracticesAction} = this.props;
                    const {fetchListeningToeicConverstation, cleanBeforeGetDetailHL} = multiListenQuestionAction;
                    cleanBeforeGetDetailHL();

                    const {showButtonSaveResultListening} = historyPracticesAction;
                    showButtonSaveResultListening();

                    const lst ={}
                    lst.topicId = collapseItem.topicId;
                    lst.part = "PART3";
                    lst.levelCode ="MEDIUM";
                    fetchListeningToeicConverstation(lst);
                    // getNameTopic(collapseItem.topicName);
                    history.push("/pages/practice-listening-toeic-conversation");
                    break
                  }
                  else {}
                }
              case CODE_PART4_LF:{
                const {multiListenQuestionAction} = this.props;
                const {getListQuestionOfListeningWordFill, getNameTopic} = multiListenQuestionAction;
                multiListenQuestionAction.cleanListQuestionOfListeningWordFill();
                multiListenQuestionAction.cleanResultQuestionOfListeningWordFill();
                multiListenQuestionAction.cleanNumberCorrectAnswerWordFill();
                multiListenQuestionAction.cleanListQuestionAndAnswerListening();
                const lst ={}
                lst.topicName = collapseItem.topicName;
                lst.levelCode ="MEDIUM";
                lst.part =CODE_PART4_LF;
                getListQuestionOfListeningWordFill(lst);
                getNameTopic(collapseItem.topicName);
                multiListenQuestionAction.getPartPracticeListening(CODE_PART4_LF)
                this.props.practiceListen.getTopicId(collapseItem.topicId,"MEDIUM")
                history.push("/pages/listeningWordFill");
                break
              }
              case "PART4":
              {
                if(this.props.paramValue === "2"){
                  const {multiListenQuestionAction, historyPracticesAction} = this.props;
                  const {fetchListeningToeicConverstation, cleanBeforeGetDetailHL} = multiListenQuestionAction;
                  cleanBeforeGetDetailHL();

                  const {showButtonSaveResultListening} = historyPracticesAction;
                  showButtonSaveResultListening();

                  const lst ={}
                  lst.topicId = collapseItem.topicId;
                  lst.part = "PART4";
                  lst.levelCode ="MEDIUM";
                  fetchListeningToeicConverstation(lst);
                  // getNameTopic(collapseItem.topicName);
                  history.push("/pages/practice-listening-toeic-short-talking");
                  break
                }
                else {}
              }
              case CODE_PART5:
                // const {readingAction} = this.props;
                const {getListQuestionOfReadingWordFill, getNameTopic} = readingAction;
                const lst ={};
                readingAction.cleanListQuestionOfReadingCompletedPassage();
                readingAction.cleanResultQuestionOfReadingCompletedPassage();
                readingAction.cleaningListQuestionAndAnswerReadingCompletedPassage();
                readingAction.cleanNumberCorrectAnswer();
                readingAction.setValueIndex();
                lst.topicName = collapseItem.topicName;
                lst.levelCode ="MEDIUM";
                getListQuestionOfReadingWordFill(lst);
                getNameTopic(collapseItem.topicName);
                readingAction.getPart(CODE_PART5);
                readingAction.getLevelCode("MEDIUM");
                readingAction.getTopicId(collapseItem.topicId)
                history.push("/pages/readingWordFill");
                break
              case CODE_PART6:
                // const {readingActions} = this.props;
                const { getListQuestionOfReadingCompletedPassage, getNameTopicPassage, getLevelCode} = readingAction;
                let quest = {};
                readingAction.cleanListQuestionOfReadingCompletedPassage();
                readingAction.cleanResultQuestionOfReadingCompletedPassage();
                readingAction.cleaningListQuestionAndAnswerReadingCompletedPassage();
                readingAction.cleanNumberCorrectAnswer();
                readingAction.setValueIndex();
                quest.topicName = collapseItem.topicName;
                quest.levelCode = "MEDIUM";
                getListQuestionOfReadingCompletedPassage(quest);
                getNameTopicPassage(collapseItem.topicName);
                readingAction.getPart(CODE_PART6);
                getLevelCode("MEDIUM");
                readingAction.getTopicId(collapseItem.topicId)
                history.push("/pages/practice-reading-completed-passage")
                break
              case CODE_PART7:
                data.topicId = collapseItem.topicId
                data.part = CODE_PART7
                data.levelCode = "MEDIUM"
                readingAction.storeDataToRepracticeReading(data);
                readingAction.clearTempReadingSingleAndDualQuestion();
                readingAction.clearTotalTrueReadingSingleAndDual();
                readingAction.cleaningListQuestionAndAnswerReadingCompletedPassage();
                getListSingleAndDualReadingQuestion(data)
                readingAction.getNameTopic(collapseItem.topicName);
                readingAction.getLevelCode("MEDIUM");
                readingAction.getTopicId(collapseItem.topicId)
                readingAction.setPartPracticeReading(CODE_PART7);
                history.push("/pages/practice-reading-single")
                break
              case CODE_PART8:
                data.topicId = collapseItem.topicId
                data.part = CODE_PART8
                data.levelCode = "MEDIUM"
                readingAction.storeDataToRepracticeReading(data);
                readingAction.clearTempReadingSingleAndDualQuestion();
                readingAction.clearTotalTrueReadingSingleAndDual();
                readingAction.cleaningListQuestionAndAnswerReadingCompletedPassage();
                getListSingleAndDualReadingQuestion(data)
                readingAction.getLevelCode("MEDIUM");
                readingAction.getNameTopic(collapseItem.topicName);
                readingAction.getTopicId(collapseItem.topicId)
                readingAction.setPartPracticeReading(CODE_PART8);
                history.push("/pages/practice-reading-dual")
                break
              default:
            }
          }} color="default">
            <LinkStyle to="">{collapseItem.topicName}</LinkStyle>
          </ButtonStyle>
        </ColItemStyle>
      )
    })) : null

    let renderCollapseDifficult = this.props.topicsByPart[2] ? (this.props.topicsByPart[2].length <= 0 ? <h5 style={{color : "grey" , width : "100%" , textAlign : "center"}}><FormattedMessage id={'not.found.data'}/></h5> : this.props.topicsByPart[2].map((collapseItem , index) => {
      const {multiListenQuestionAction, readingAction} = this.props
      return (
        <ColItemStyle xs="12" sm="6" md="4" lg="3" xl="3" key={index}>
          <ButtonStyle onClick={() => {
            let Part = this.getPartExerciseByURL();
            const {getListSingleAndDualReadingQuestion, getPart} = readingAction
            getPart(Part);
            const data ={}
            debugger
            switch(Part){
              case CODE_PART1_LF:{
                const {multiListenQuestionAction} = this.props;
                const {getListQuestionOfListeningWordFill, getNameTopic} = multiListenQuestionAction;
                multiListenQuestionAction.cleanListQuestionOfListeningWordFill();
                multiListenQuestionAction.cleanResultQuestionOfListeningWordFill();
                multiListenQuestionAction.cleanNumberCorrectAnswerWordFill();
                multiListenQuestionAction.cleanListQuestionAndAnswerListening();
                const lst ={}
                lst.topicName = collapseItem.topicName;
                lst.levelCode ="DIFFICULT";
                lst.part =CODE_PART1_LF;
                getListQuestionOfListeningWordFill(lst);
                getNameTopic(collapseItem.topicName);
                multiListenQuestionAction.getPartPracticeListening(CODE_PART1_LF)
                this.props.practiceListen.getTopicId(collapseItem.topicId,"DIFFICULT")
                history.push("/pages/listeningWordFill");
                break
              }
              case CODE_PART1:
              {
                debugger
                  const {multiListenQuestionAction,multiListeningReducer, historyPracticesAction} = this.props;
                  const {cleanHiddenBtnBeforeGetLisSingle}= historyPracticesAction;
                  cleanHiddenBtnBeforeGetLisSingle();

                const {resetCountCheckListeningPractices} = multiListenQuestionAction;
                resetCountCheckListeningPractices();

                  const {getNameTopic,getListListeningQuestionOfMulti}= multiListenQuestionAction;
                  getNameTopic(collapseItem.topicName);
                  this.props.practiceListen.getTopicId(collapseItem.topicId, "DIFFICULT")
                  multiListenQuestionAction.getPartPracticeListening(CODE_PART1)
                  const {topicId, levelCode} = multiListeningReducer
                  multiListenQuestionAction.clearTempQuestionAnswer();
                  multiListenQuestionAction.clearTotalCorrect();
                  multiListenQuestionAction.clearAnsweredMultiListenQuest();
                  getListListeningQuestionOfMulti(collapseItem.topicId, "DIFFICULT")
                  history.push("/pages/practice-listening-painting")
                  break
                }
              case CODE_PART2_LF:{
                const {multiListenQuestionAction} = this.props;
                const {getListQuestionOfListeningWordFill, getNameTopic} = multiListenQuestionAction;
                multiListenQuestionAction.cleanListQuestionOfListeningWordFill();
                multiListenQuestionAction.cleanResultQuestionOfListeningWordFill();
                multiListenQuestionAction.cleanNumberCorrectAnswerWordFill();
                multiListenQuestionAction.cleanListQuestionAndAnswerListening();
                const lst ={}
                lst.topicName = collapseItem.topicName;
                lst.levelCode ="DIFFICULT";
                lst.part =CODE_PART2_LF;
                getListQuestionOfListeningWordFill(lst);
                getNameTopic(collapseItem.topicName);
                multiListenQuestionAction.getPartPracticeListening(CODE_PART2_LF)
                this.props.practiceListen.getTopicId(collapseItem.topicId,"DIFFICULT")
                history.push("/pages/listeningWordFill");
                break
              }
              case CODE_PART2:
              {
                  debugger
                  const {multiListenQuestionAction,multiListeningReducer, historyPracticesAction} = this.props;
                  const {cleanHiddenBtnBeforeGetLisSingle}= historyPracticesAction;
                  cleanHiddenBtnBeforeGetLisSingle();

                  const {resetCountCheckListeningPractices} = multiListenQuestionAction;
                  resetCountCheckListeningPractices();

                  const {getNameTopic,getListListeningQuestionOfMulti}= multiListenQuestionAction;
                  const {topicId, levelCode} = multiListeningReducer
                  getNameTopic(collapseItem.topicName);
                  this.props.practiceListen.getTopicId(collapseItem.topicId, "DIFFICULT")
                  multiListenQuestionAction.getPartPracticeListening(CODE_PART2)
                  multiListenQuestionAction.clearTempQuestionAnswer();
                  multiListenQuestionAction.clearTotalCorrect();
                  multiListenQuestionAction.clearAnsweredMultiListenQuest();
                  getListListeningQuestionOfMulti(collapseItem.topicId,"DIFFICULT");
                  history.push("/pages/practice-listening-question-answer")
                  break
                }
              case CODE_PART3_LF:{
                const {multiListenQuestionAction} = this.props;
                const {getListQuestionOfListeningWordFill, getNameTopic} = multiListenQuestionAction;
                multiListenQuestionAction.cleanListQuestionOfListeningWordFill();
                multiListenQuestionAction.cleanResultQuestionOfListeningWordFill();
                multiListenQuestionAction.cleanNumberCorrectAnswerWordFill();
                multiListenQuestionAction.cleanListQuestionAndAnswerListening();
                const lst ={}
                lst.topicName = collapseItem.topicName;
                lst.levelCode ="DIFFICULT";
                lst.part =CODE_PART3_LF;
                getListQuestionOfListeningWordFill(lst);
                getNameTopic(collapseItem.topicName);
                multiListenQuestionAction.getPartPracticeListening(CODE_PART3_LF)
                this.props.practiceListen.getTopicId(collapseItem.topicId,"DIFFICULT")
                history.push("/pages/listeningWordFill");
                break
              }
              case "PART3":
              {
                  if(this.props.paramValue === "2"){
                    const {multiListenQuestionAction, historyPracticesAction} = this.props;
                    const {fetchListeningToeicConverstation, cleanBeforeGetDetailHL} = multiListenQuestionAction;
                    cleanBeforeGetDetailHL();

                    const {showButtonSaveResultListening} = historyPracticesAction;
                    showButtonSaveResultListening();

                    const lst ={}
                    lst.topicId = collapseItem.topicId;
                    lst.part = "PART3";
                    lst.levelCode ="DIFFICULT";
                    fetchListeningToeicConverstation(lst);
                    // getNameTopic(collapseItem.topicName);
                    history.push("/pages/practice-listening-toeic-conversation");
                    break
                  }
                  else {}
              }
              case CODE_PART4_LF:{
                const {multiListenQuestionAction} = this.props;
                const {getListQuestionOfListeningWordFill, getNameTopic} = multiListenQuestionAction;
                multiListenQuestionAction.cleanListQuestionOfListeningWordFill();
                multiListenQuestionAction.cleanResultQuestionOfListeningWordFill();
                multiListenQuestionAction.cleanNumberCorrectAnswerWordFill();
                multiListenQuestionAction.cleanListQuestionAndAnswerListening();
                const lst ={}
                lst.topicName = collapseItem.topicName;
                lst.levelCode ="DIFFICULT";
                lst.part =CODE_PART4_LF;
                getListQuestionOfListeningWordFill(lst);
                getNameTopic(collapseItem.topicName);
                multiListenQuestionAction.getPartPracticeListening(CODE_PART4_LF)
                this.props.practiceListen.getTopicId(collapseItem.topicId,"DIFFICULT")
                history.push("/pages/listeningWordFill");
                break
              }
              case "PART4":
              {
                if(this.props.paramValue === "2"){
                  const {multiListenQuestionAction, historyPracticesAction} = this.props;
                  const {fetchListeningToeicConverstation, cleanBeforeGetDetailHL} = multiListenQuestionAction;
                  cleanBeforeGetDetailHL();

                  const {showButtonSaveResultListening} = historyPracticesAction;
                  showButtonSaveResultListening();

                  const lst ={}
                  lst.topicId = collapseItem.topicId;
                  lst.part = "PART4";
                  lst.levelCode ="DIFFICULT";
                  fetchListeningToeicConverstation(lst);
                  // getNameTopic(collapseItem.topicName);
                  history.push("/pages/practice-listening-toeic-short-talking");
                  break
                }
                else {}
              }
              case CODE_PART5:
                // const {readingAction} = this.props;
                const {getListQuestionOfReadingWordFill, getNameTopic} = readingAction;
                const lst ={};
                readingAction.cleanListQuestionOfReadingCompletedPassage();
                readingAction.cleanResultQuestionOfReadingCompletedPassage();
                readingAction.cleaningListQuestionAndAnswerReadingCompletedPassage();
                readingAction.cleanNumberCorrectAnswer();
                readingAction.setValueIndex();
                lst.topicName = collapseItem.topicName;
                lst.levelCode ="DIFFICULT";
                getListQuestionOfReadingWordFill(lst);
                getNameTopic(collapseItem.topicName);
                getPart(CODE_PART5);
                readingAction.getLevelCode("DIFFICULT");
                readingAction.getTopicId(collapseItem.topicId)
                history.push("/pages/readingWordFill");
                break
              case CODE_PART6:
                // const {readingActions} = this.props;
                const { getListQuestionOfReadingCompletedPassage, getNameTopicPassage, getLevelCode} = readingAction;
                let quest = {};
                readingAction.cleanListQuestionOfReadingCompletedPassage();
                readingAction.cleanResultQuestionOfReadingCompletedPassage();
                readingAction.cleaningListQuestionAndAnswerReadingCompletedPassage();
                readingAction.cleanNumberCorrectAnswer();
                readingAction.setValueIndex();
                quest.topicName = collapseItem.topicName;
                quest.levelCode = "DIFFICULT";
                getListQuestionOfReadingCompletedPassage(quest);
                getNameTopicPassage(collapseItem.topicName);
                readingAction.getPart(CODE_PART6);
                getLevelCode("DIFFICULT");
                readingAction.getTopicId(collapseItem.topicId)
                history.push("/pages/practice-reading-completed-passage")
                break
              case CODE_PART7:
                data.topicId = collapseItem.topicId
                data.part = CODE_PART7
                data.levelCode = "DIFFICULT"
                readingAction.storeDataToRepracticeReading(data);
                readingAction.clearTempReadingSingleAndDualQuestion();
                readingAction.clearTotalTrueReadingSingleAndDual();
                readingAction.cleaningListQuestionAndAnswerReadingCompletedPassage();
                getListSingleAndDualReadingQuestion(data)
                readingAction.setPartPracticeReading(CODE_PART7);
                readingAction.getTopicId(collapseItem.topicId)
                readingAction.getNameTopic(collapseItem.topicName);
                readingAction.getLevelCode("DIFFICULT"); 
                history.push("/pages/practice-reading-single")
                break
              case CODE_PART8:
                data.topicId = collapseItem.topicId
                data.part = CODE_PART8
                data.levelCode = "DIFFICULT"
                readingAction.storeDataToRepracticeReading(data);
                readingAction.clearTempReadingSingleAndDualQuestion();
                readingAction.clearTotalTrueReadingSingleAndDual();
                readingAction.cleaningListQuestionAndAnswerReadingCompletedPassage();
                getListSingleAndDualReadingQuestion(data)
                readingAction.getLevelCode("DIFFICULT");
                readingAction.setPartPracticeReading(CODE_PART8);
                readingAction.getNameTopic(collapseItem.topicName);
                readingAction.getTopicId(collapseItem.topicId)
                history.push("/pages/practice-reading-dual")
                break
              default:
            }
          }} color="default">
            <LinkStyle to="">{collapseItem.topicName}</LinkStyle>
          </ButtonStyle>
        </ColItemStyle>
      )
    })) : null

    return (
      <Router>
        <Container>
          <TitleMain><strong><FormattedMessage id="practices"/> - {stringSpeciesExercise} - {stringTypeExercise}</strong></TitleMain>
          <div ref={this.focusItem} className="vx-collapse collapse-bordered accordion-icon-rotate" style={{position : "relative"}}>
            {this.state.isLoading ? <SpinnerStyle color="primary" className="reload-spinner" /> : null}

            <CartStyle opac={this.state.isLoading ? true : false}>
              <CardHeaderStyle onClick={this.toggleCollapseEasy}>
                <CardTitleStyle>
                  <FormattedMessage id="level"/> <FormattedMessage id="easy"/>
                  {this.state.isOpenEasy ? <IconCaretUpStyle></IconCaretUpStyle> : <IconCaretDownStyle></IconCaretDownStyle>}
                </CardTitleStyle>
              </CardHeaderStyle>
              <Collapse
                isOpen={this.state.isOpenEasy}
                onEntering={this.onEntering}
                onEntered={this.onEntered}
                onExiting={this.onExiting}
                onExited={this.onExited}
              >
                <CardBodyStyle>
                  <Row>
                    {renderCollapseEasy}
                  </Row>
                </CardBodyStyle>
              </Collapse>
            </CartStyle>

            <CartStyle opac={this.state.isLoading ? true : false}>
              <CardHeaderStyle onClick={this.toggleCollapseMedium}>
                <CardTitleStyle>
                  <FormattedMessage id="level"/> <FormattedMessage id="medium"/>
                  {this.state.isOpenMedium ? <IconCaretUpStyle></IconCaretUpStyle> : <IconCaretDownStyle></IconCaretDownStyle>}
                </CardTitleStyle>
              </CardHeaderStyle>
              <Collapse
                isOpen={this.state.isOpenMedium}
                onEntering={this.onEntering}
                onEntered={this.onEntered}
                onExiting={this.onExiting}
                onExited={this.onExited}
              >
                <CardBodyStyle>
                  <Row>
                    {renderCollapseMedium}
                  </Row>
                </CardBodyStyle>
              </Collapse>
            </CartStyle>

            <CartStyle opac={this.state.isLoading ? true : false}>
              <CardHeaderStyle onClick={this.toggleCollapseDifficult}>
                <CardTitleStyle>
                  <FormattedMessage id="level"/> <FormattedMessage id="difficult"/>
                  {this.state.isOpenDifficult ? <IconCaretUpStyle></IconCaretUpStyle> : <IconCaretDownStyle></IconCaretDownStyle>}
                </CardTitleStyle>
              </CardHeaderStyle>
              <Collapse
                isOpen={this.state.isOpenDifficult}
                onEntering={this.onEntering}
                onEntered={this.onEntered}
                onExiting={this.onExiting}
                onExited={this.onExited}
              >
                <CardBodyStyle>
                  <Row>
                    {renderCollapseDifficult}
                  </Row>
                </CardBodyStyle>
              </Collapse>
            </CartStyle>
          </div>
        </Container>
      </Router>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    topicsByPart : state.topicReducer.listTopicByPart,
    isLoadingGetListTopic : state.topicReducer.isLoadingGetListTopic,
    paramValue: state.multiListeningReducer.paramValue,
    multiListeningReducer: state.multiListeningReducer,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    multiListenQuestionAction: bindActionCreators(practiceListenMultiActions,dispatch),
    topicAction: bindActionCreators(topicActions, dispatch),
    practiceListen: bindActionCreators(practiceListenMultiActions,dispatch),
    readingAction : bindActionCreators(readingActions, dispatch),
    historyPracticesAction: bindActionCreators(historyPracticesAction, dispatch),
  }
}

export default connect(mapStateToProps , mapDispatchToProps)(ListTopicExcercise)
