import React, {PureComponent} from 'react';
import {connect} from "react-redux";
import * as Icon from "react-feather";
import {bindActionCreators, compose} from "redux";

import {
  Card,
  CardBody,
  CardImg,
  Container,
  Row,
  Col,
  Button,
  Progress,
  Input,
  Form  ,
  Spinner
} from "reactstrap"

import {withStyles} from "@material-ui/core";
import {styles} from "./style";
import {history} from "../../../history";
import testAction from "../../../redux/actions/test";
import doTestAction from "../../../redux/actions/do-test-management";
import doTestReducer from "../../../redux/reducers/do-test-management/doTestReducer";
import {FormattedMessage} from "react-intl";

class ListTests extends PureComponent {
  constructor(props){
    super(props);
    this.state = {
    }
  }

  onClick =(element)=>{
    debugger
    let fullTest = {};
    const {doTestActions} = this.props;
    const {getDetailFullTest,saveFullTest,cleanTest} = doTestActions;
    // fullTest.id = element.id;
    // fullTest.name = element.name;
    // getDetailFullTest(fullTest);
    cleanTest();
    saveFullTest(element);
    history.push("/pages/fulltest")
  }

  componentDidMount() {
    debugger
    let test = {};
    const {doTestActions} = this.props;
    const {getListNameTest} = doTestActions;
    getListNameTest();
  }

  render() {
    const {classes} = this.props;
    const loadingComponent = (
      <div style={{
        position: 'absolute',
        zIndex: 110,
        top: 0,
        left: 0,
        width: '100%',
        height: '100%',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        background: 'rgba(255,255,255,0.8)'
      }}>
        <Spinner color="primary" className="reload-spinner"/>
      </div>
    );
    return (
      <>

        <style>{`
          #buttonTest:hover{
            background-color: #91d6f3 !important;
          }
        `}</style>
        <Container fluid={true} style={{marginTop: '2%', backgroundColor: 'white', padding: '2% 10%'}}>
          <h2 className={classes.title}>
            {/*Bài thi*/}
            <FormattedMessage id="list.of.the.test"/>
          </h2>
          {this.props.loadingLstName ? loadingComponent : null}
          <Col xs="12" md="12" sm="12" lg="12" className={classes.colParents}>
            {
              this.props.listNameTest && this.props.listNameTest.map((element, index) =>{
                return ( <button id="buttonTest" className={classes.buttonTest} onClick={()=>this.onClick(element)}>{element.name}</button>)
              }
            )}
          </Col>
        </Container>
      </>
    );
  }
}

const mapStateToProps = state => {
  return {
    userInfo: state.auth.login,
    listNameTest: state.doTestReducer.listNameTest,
    loadingLstName: state.doTestReducer.loadingLstName
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    testActions: bindActionCreators(testAction, dispatch),
    doTestActions: bindActionCreators(doTestAction, dispatch),
  }
}
const withConnect = connect(mapStateToProps,mapDispatchToProps)
export default compose(withStyles(styles), withConnect)(ListTests);
