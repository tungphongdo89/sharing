import styled from "styled-components"
import {Button, Card, CardImg, CardTitle, Col, Label, Row} from "reactstrap";
import * as Icon from "react-bootstrap-icons";
import {Field} from "formik";
import Spinner from "reactstrap/es/Spinner";

export const CardStyle = styled(Card)`
  width : 60%;
  margin : 0 auto; 
`

export const CardTitleStyle = styled(CardTitle)`
  width: 100%;
  text-align: center;
  font-weight: bold;
`

export const ColAvatar = styled(Col)`
  height: 196px;
  width: 33.33%;
  float: left;
  border: 1px solid white;
  border-radius: 100%;
  box-sizing: border-box;
  padding: 40px 0 0 0;  
`

export const RowImg = styled(Row)`
  position: relative;
  margin: 0;
`

export const IconPersonFill = styled(Icon.PersonFill)`
  width: 200px;
  height: 200px;
  margin: 0 auto;
  background: black;
  color: white;
  border: 1px solid white;
  border-radius: 50%;
`

export const FieldUploadFile = styled(Field)`
  width: 0.1px;
  height: 0.1px;
  opacity: 0;
  overflow: hidden;
  position: absolute;
  z-index: -1;
  
  &:focus{
    background-color: #1ab7ea;
    border-color: #1ab7ea;
  }
`

export const LabelUploadFile = styled(Label)`
  width: 36%;
  text-align: center;
  position: absolute;
  bottom: 5px;
  left: 32%;
  background: #1ab7ea;
  border: 1px solid #7367f0;
  border-radius: 50%;
  padding: 3px 10px;
  font-weight : bold;
  cursor: pointer;
  
  &:hover{
    background: #7367f0;
    color : white;
  }
`

export const CardImgStyle = styled(CardImg)`
   width: 200px;
  height: 200px;
  margin: 0 auto;
  background: black;
  color: white;
  border: 1px solid white;
  border-radius: 50%;
  
`

export const ColInfo = styled(Col)`
  width: 66.67%;
  float: right;
  padding: 15px 0 0 50px !important;
`

export const RowInput = styled(Row)`
  margin: 0 0 15px 0;
`

export const BtnSubmitStyle = styled(Button)`
  width : 30%;
  &::after{
    content: none;
  }
`

export const SpinnerStyle = styled(Spinner)`
  margin: 5px 30% 0;
`
