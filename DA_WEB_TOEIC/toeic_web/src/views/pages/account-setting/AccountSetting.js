import React, {Component} from 'react';
import {CardBody, CardHeader, Col, Row} from "reactstrap"
import {Field, Form, Formik} from "formik"
import {connect} from "react-redux"
import * as Yup from "yup"
import * as accountActions from "../../../redux/actions/account/accountActions";
import * as qs from "query-string"
import * as Style from "./style"
import {FormattedMessage} from "react-intl";

// const FILE_SIZE = 5.001 * 1024 * 1024;
const FILE_SIZE = 5242880;
// const FILE_SIZE = 1000;
class AccountSettings extends Component {

  state = {
    urlImage: "",
    fileUpload: null,
    messTypeFileError : "",
    isUpdating : false,
    disableButtonOk: true,
    password:'',
  }

  componentWillReceiveProps() {
    this.setState({
      isUpdating : false
    })
  }

  handleChangeFile = (e) => {
    let reader = new FileReader();
    let file = e.target.files[0]

    if (file !== undefined && (file.type.indexOf("image/jpg") !== -1 || file.type.indexOf("image/jpeg") !== -1 || file.type.indexOf("image/png") !== -1)) {
      //5242880
      if(file !== undefined && file.size > 5242880){
        this.setState({
          urlImage: "",
          fileUpload: null,
          messTypeFileError: "Chỉ được phép upload file có dung lượng tối đa 5MB"
        })
      } else {
        reader.onloadend = () => {
          this.setState({
            urlImage: reader.result,
            fileUpload: file,
            messTypeFileError: ""
          });
        }
        reader.readAsDataURL(file)
      }
    }
    else if (file === undefined) {
      this.setState({
        urlImage: "",
        fileUpload: null
      })
    }
    else {
      this.setState({
        urlImage: "",
        fileUpload: null,
        messTypeFileError: "File upload phải có định dạng .JPG hoặc .PNG"
      })
    }
  }

  showUpdating = () => {
    this.setState({
      isUpdating : true
    })
  }

  render() {

    const parsed = qs.parse(this.props.location.search)
    let emailUserInvited = parsed.email

    return (
      <>
        <style>{`
            #btnHighLight:focus-within {
              border: none !important;
              outline: 1px solid gray !important;
              border-radius: 3px !important;
            }
            #userName:focus-within {
              text-shadow: 1px 1px #1890ff;
            }
          `}

        </style>
        <React.Fragment>
          <Style.CardStyle>
            <CardHeader>
              <Style.CardTitleStyle><strong><FormattedMessage id="account.setting"/></strong></Style.CardTitleStyle>
            </CardHeader>
            <CardBody>
              <Row>
                <Col xs="0" sm="0" md="0" lg="1" xl="1"></Col>
                <Col xs="12" sm="12" md="12" lg="10" xl="10">
                  <Formik
                    initialValues={{
                      userShowName: "",
                      password: "",
                      rePassword: ""
                    }}

                    validationSchema={Yup.object().shape({
                      userShowName: Yup.string()
                        .required("Tên tài khoản không được phép để trống")
                        .min(1, "Tên tài khoản phải lớn hơn hoặc bằng 1 ký tự")
                        .max(45, "Tên tài khoản phải nhỏ hơn hoặc bằng 45 ký tự"),
                      password: Yup.string()
                        .required("Mật khẩu không được phép để trống")
                        .min(6, "Mật khẩu phải lớn hơn hoặc bằng 6 ký tự")
                        .max(45, "Mật khẩu phải nhỏ hơn hoặc bằng 45 ký tự")
                        .matches(/^[^ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựýỳỵỷỹ]+$/,'Mật khẩu không được chứa kí tự Tiếng Việt có dấu!')
                        .matches(/^(?=.*[a-zA-Z])(?=.*\d)[a-zA-Z\d\s<>()\[\]\\.,^%$#&*=+-_'"~`!{}|\/?;:@]{6,}$/,'Mật khẩu phải chứa cả chữ, số và không chứa khoảng trắng!')
                        .matches(/^[^\s]+$/,'Mật khẩu không được nhập kí tự khoảng trắng!'),

                      rePassword: Yup.string()
                        .required("Mật khẩu nhập lại không được phép để trống")
                        .min(6, "Mật khẩu phải lớn hơn hoặc bằng 6 ký tự")
                        .max(45, "Mật khẩu phải nhỏ hơn hoặc bằng 45 ký tự")
                        .matches(/^[^ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựýỳỵỷỹ]+$/,'Mật khẩu nhập lại không được chứa kí tự Tiếng Việt có dấu!')
                        .matches(/^(?=.*[a-zA-Z])(?=.*\d)[a-zA-Z\d\s<>()\[\]\\.,^%$#&*=+-_'"~`!{}|\/?;:@]{6,}$/,'Mật khẩu nhập lại phải chứa cả chữ, số và không chứa khoảng trắng!')
                        .matches(/^[^\s]+$/,'Mật khẩu nhập lại không được nhập kí tự khoảng trắng!')
                        .oneOf([Yup.ref('password'), null], 'Mật khẩu chưa trùng khớp')
                    })}

                    onSubmit={(values) => {
                      values.userName = parsed.email
                      let formData = new FormData()
                      if (this.state.fileUpload !== null) {
                        formData.append(
                          "fileUpload",
                          this.state.fileUpload,
                        )

                      }
                      formData.append("userName", emailUserInvited)
                      formData.append("userShowName", values.userShowName)
                      formData.append("password", values.password)
                      this.props.settingAccount(formData)
                    }}
                  >
                    {({errors, touched, values, handleChange, setFieldValue, isValid, isSubmitting}) => (
                      <Form>
                        <Style.ColAvatar xs="5" sm="4" md="4" lg="4">
                          <Style.RowImg>
                            {
                              this.state.urlImage === "" ?
                                <Style.IconPersonFill className="avatar" ></Style.IconPersonFill> :
                                <Style.CardImgStyle className="avatar"
                                                    src={this.state.urlImage ? this.state.urlImage : null} alt=""
                                                    height="200px" width="200px"></Style.CardImgStyle>
                            }
                            <Style.FieldUploadFile type="file" id="file" name="file" className="input-file"
                                                   onChange={(e) => {
                                                     this.handleChangeFile(e)
                                                   }}></Style.FieldUploadFile>
                            <Style.LabelUploadFile htmlFor="file" className="lable-for-file"><span
                              className="glyphicon glyphicon-open" ></span><FormattedMessage id="upload"/></Style.LabelUploadFile>

                          </Style.RowImg>
                          <Row>
                            {this.state.messTypeFileError !== "" ? <label style={{
                              color: "red",
                              width: "100%",
                              textAlign: "center"
                            }}><strong>{this.state.messTypeFileError}</strong></label> : null}
                          </Row>
                        </Style.ColAvatar>

                        <Style.ColInfo xs="7" sm="8" md="8" lg="8">
                          <Style.RowInput>
                            <label><FormattedMessage id="account.name"/> (<span style={{color: "red"}}>*</span>)</label>
                            <Field type="text" name="userShowName" className="form-control" placeholder=""
                                   autoFocus={true}/>
                            {errors.userShowName ?
                              <strong><label style={{color: "red"}}>{errors.userShowName}</label></strong> : null}
                          </Style.RowInput>

                          <Style.RowInput>
                            <label><FormattedMessage id="password"/> (<span style={{color: "red"}}>*</span>)</label>
                            <Field type="password" name="password" className="form-control" placeholder=""
                            />
                            {errors.password ?
                              <strong><label style={{color: "red"}}>{errors.password}</label></strong> : null}
                          </Style.RowInput>

                          <Style.RowInput>
                            <label><FormattedMessage id="enter.the.password"/> (<span style={{color: "red"}}>*</span>)</label>
                            <Field type="password" name="rePassword" className="form-control" placeholder=""/>
                            {errors.rePassword ?
                              <strong><label style={{color: "red"}}>{errors.rePassword}</label></strong> : null}
                          </Style.RowInput>

                          <Style.RowInput>

                            <Style.BtnSubmitStyle type="submit" color="info" onClick={this.showUpdating}
                                                  id="btnHighLight"
                                                  disabled={!isValid || isSubmitting || this.state.messTypeFileError !== ""}>OK</Style.BtnSubmitStyle>

                            {this.state.isUpdating && <Style.SpinnerStyle color="primary"/>}
                          </Style.RowInput>
                        </Style.ColInfo>
                      </Form>
                    )}
                  </Formik>
                </Col>
                <Col xs="0" sm="0" md="0" lg="1" xl="1"></Col>
              </Row>
            </CardBody>
          </Style.CardStyle>
        </React.Fragment>
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    account_Reducer : state.accountReducer
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    settingAccount : (data) => {
      dispatch(accountActions.settingAccount(data));
    }
  }
}

export default connect(mapStateToProps , mapDispatchToProps)(AccountSettings)
