import React from "react"
import {Button, Card, CardBody, CardHeader, CardTitle, Col, FormGroup, Input, Label, Row, Spinner,} from "reactstrap"
import {ArrowLeft, Eye} from "react-feather"
import "../../../assets/css/style-modal-account.css"
import {bindActionCreators, compose} from "redux";
import {connect} from "react-redux";
import {withStyles} from "@material-ui/styles";
import styles from "./styles";
import studentAccountAction from "../../../redux/actions/studentAccount"
import {Field, Form, Formik} from "formik";
import {Table} from "antd";
import PaginationIconsAndTextForMinitest from './pagingtionHistoryMinitest';
import 'antd/dist/antd.css';
import {getUTCTimeSeconds, getUTCTime} from "../../../commons/utils/DateUtils";
import Tooltip from "react-bootstrap/Tooltip";
import OverlayTrigger from "react-bootstrap/OverlayTrigger";
import {history} from "../../../history";
import testAction from "../../../redux/actions/do-test-management";
import * as Yup from "yup";

import DatePicker,{registerLocale} from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import {FormattedMessage} from "react-intl";
import {showMessage} from "../../../commons/utils/convertDataForToast";
import vi from 'date-fns/locale/vi';
registerLocale('vi', vi)

const formSchema = Yup.object().shape({
  createDateFrom: Yup.string()
    .matches(/^((2000|2400|2800|(19|2[0-9](0[48]|[2468][048]|[13579][26])))-02-29)$|^(((19|2[0-9])[0-9]{2})-02-(0[1-9]|1[0-9]|2[0-8]))$|^(((19|2[0-9])[0-9]{2})-(0[13578]|10|12)-(0[1-9]|[12][0-9]|3[01]))$|^(((19|2[0-9])[0-9]{2})-(0[469]|11)-(0[1-9]|[12][0-9]|30))$/, "Ngày tháng không đúng định dạng"),
  createDateTo: Yup.string()
    .matches(/^((2000|2400|2800|(19|2[0-9](0[48]|[2468][048]|[13579][26])))-02-29)$|^(((19|2[0-9])[0-9]{2})-02-(0[1-9]|1[0-9]|2[0-8]))$|^(((19|2[0-9])[0-9]{2})-(0[13578]|10|12)-(0[1-9]|[12][0-9]|3[01]))$|^(((19|2[0-9])[0-9]{2})-(0[469]|11)-(0[1-9]|[12][0-9]|30))$/, "Ngày tháng không đúng định dạng")

});

let errDate1 = "";
let errDate2 = "";

function validateDateFrom(date,value) {
  
  if (value.createDateFrom === "") {
    if (document.getElementById("createDateFromMnitest") !== null && document.getElementById("createDateFromMnitest").validationMessage !== "") {
      errDate1 = showMessage("date.not.correct.format");
    } else {
      errDate1 = ""
    }
  } else {
    if (!/^((2000|2400|2800|(19|2[0-9](0[48]|[2468][048]|[13579][26])))-02-29)$|^(((19|2[0-9])[0-9]{2})-02-(0[1-9]|1[0-9]|2[0-8]))$|^(((19|2[0-9])[0-9]{2})-(0[13578]|10|12)-(0[1-9]|[12][0-9]|3[01]))$|^(((19|2[0-9])[0-9]{2})-(0[469]|11)-(0[1-9]|[12][0-9]|30))$/i.test(value.createDateFrom)) {
      errDate1 = showMessage("date.not.correct.format");
    } else if (value.createDateFrom && value.createDateTo) {
      if (Date.parse(value.createDateFrom) > Date.parse(value.createDateTo)) {
        errDate1 = showMessage("compare.time");
      } else {
        errDate1 = ""
      }
    } else {
      errDate1 = ""
    }
  }
}

function validateDateTo(value) {
  if (value.createDateTo === "") {
    if (document.getElementById("createDateToMnitest") !== null && document.getElementById("createDateToMnitest").validationMessage !== "") {
      errDate2 = showMessage("date.not.correct.format");
    } else {
      errDate2 = ""
    }
  } else {
    if (!/^((2000|2400|2800|(19|2[0-9](0[48]|[2468][048]|[13579][26])))-02-29)$|^(((19|2[0-9])[0-9]{2})-02-(0[1-9]|1[0-9]|2[0-8]))$|^(((19|2[0-9])[0-9]{2})-(0[13578]|10|12)-(0[1-9]|[12][0-9]|3[01]))$|^(((19|2[0-9])[0-9]{2})-(0[469]|11)-(0[1-9]|[12][0-9]|30))$/i.test(value.createDateTo)) {
      errDate2 = showMessage("date.not.correct.format");
    } else if (value.createDateFrom && value.createDateTo) {
      if (Date.parse(value.createDateFrom) > Date.parse(value.createDateTo)) {
        errDate1 = showMessage("compare.time");;
        errDate2 = ""
      } else {
        errDate1 = ""
      }
    } else {
      errDate2 = ""
    }
  }
}

class HistoryMinitest extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      activeTab: "1",
      isOpen: false,
      createFrom: '',
      createTo: '',
      searchValue: '',
      page: 1,
      pageSize: 10,
      count: 1,
      modalDetailTest: false,
      testId: 0,
      testName: '',
      modalRankTest: false,
      rankOfUser: '',
      isSearch: null,
      message: '',
      errDate1: '',
      errDate2: '',
    }
    this.inputFocus = React.createRef();

  }

  validateDateFrom =(value)=> {
    if (value.createDateFrom === "") {
      if (document.getElementById("createDateFromHp") !== null && document.getElementById("createDateFromHp").validationMessage !== "") {
        // errDate1 = showMessage("date.not.correct.format");
        this.setState({
          errDate1: showMessage("date.not.correct.format")
        })
      } else {
        // errDate1 = ""
        this.setState({
          errDate1: ''
        })
      }
    } else {
      if (!/^((2000|2400|2800|(19|2[0-9](0[48]|[2468][048]|[13579][26])))-02-29)$|^(((19|2[0-9])[0-9]{2})-02-(0[1-9]|1[0-9]|2[0-8]))$|^(((19|2[0-9])[0-9]{2})-(0[13578]|10|12)-(0[1-9]|[12][0-9]|3[01]))$|^(((19|2[0-9])[0-9]{2})-(0[469]|11)-(0[1-9]|[12][0-9]|30))$/i.test(value.createDateFrom)) {
        // errDate1 = showMessage("date.not.correct.format");
        this.setState({
          errDate1: showMessage("date.not.correct.format")
        })
      } else if (value.createDateFrom && value.createDateTo) {
        if (Date.parse(value.createDateFrom) > Date.parse(value.createDateTo)) {
          // errDate1 = showMessage("compare.time");
          this.setState({
            errDate1: showMessage("compare.time")
          })
        } else {
          // errDate1 = ""
          this.setState({
            errDate1: ''
          })
        }
      } else {
        // errDate1 = ""
        this.setState({
          errDate1: ''
        })
      }
    }
  }

  validateDateTo =(value)=> {
    if (value.createDateTo === "") {
      if (document.getElementById("createDateToHp") !== null && document.getElementById("createDateToHp").validationMessage !== "") {
        // errDate2 = showMessage("date.not.correct.format");
        this.setState({
          errDate2: showMessage("date.not.correct.format")
        })
      } else {
        // errDate2 = ""
        this.setState({
          errDate2: ''
        })
      }
    } else {
      if (!/^((2000|2400|2800|(19|2[0-9](0[48]|[2468][048]|[13579][26])))-02-29)$|^(((19|2[0-9])[0-9]{2})-02-(0[1-9]|1[0-9]|2[0-8]))$|^(((19|2[0-9])[0-9]{2})-(0[13578]|10|12)-(0[1-9]|[12][0-9]|3[01]))$|^(((19|2[0-9])[0-9]{2})-(0[469]|11)-(0[1-9]|[12][0-9]|30))$/i.test(value.createDateTo)) {
        // errDate2 = showMessage("date.not.correct.format");
        this.setState({
          errDate2: showMessage("date.not.correct.format")
        })
      } else if (value.createDateFrom && value.createDateTo) {
        if (Date.parse(value.createDateFrom) > Date.parse(value.createDateTo)) {
          // errDate1 = showMessage("compare.time");
          // errDate2 = ""
          this.setState({
            errDate1: showMessage("compare.time"),
            errDate2: ''
          })
        } else {
          // errDate1 = ""
          this.setState({
            errDate1: ''
          })
        }
      } else {
        // errDate2 = ""
        this.setState({
          errDate2: ''
        })
      }
    }
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if(prevProps.active !== this.props.active){
      this.setState({
        activeTab: "1",
        isOpen: false,
        createFrom: '',
        createTo: '',
        searchValue: '',
        page: 1,
        pageSize: 10,
        count: 1,
        modalDetailTest: false,
        testId: 0,
        testName: '',
        modalRankTest: false,
        rankOfUser: '',
        isSearch: null,
        message: '',
        errDate1: '',
        errDate2: ''
      })
    }
  }


  toggle = tab => {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab,
        isOpen: !this.state.isOpen
      })
    }
  }

  resetIsSearch = () => {
    this.setState({
      isSearch: null
    })
  }

  onChangPageReceive = (pageNumber) => {
    this.setState({
      page: pageNumber
    })
    if (pageNumber === 1) {
      this.setState({
        isSearch: 1,
      })
    }
    const searchPropertiesBE = {};
    searchPropertiesBE.keySearch = this.state.searchValue.toString().trim()
    searchPropertiesBE.createFrom = this.state.createFrom
    searchPropertiesBE.createTo = this.state.createTo
    searchPropertiesBE.pageSize = this.state.pageSize
    searchPropertiesBE.page = pageNumber
    searchPropertiesBE.userId = this.props.studentAccountReducer.userInfoLogin.userId;
    this.props.testAction.getListHistoryMinitest(searchPropertiesBE)
  }

  viewDetail = (record) => {
    this.toggleModalDetailTest(record.testId, record.testName);
  }

  toggleModalDetailTest = (testId, testName) => {
    this.setState(prevState => ({
      testId: testId,
      testName: testName,
      modalDetailTest: !prevState.modalDetailTest
    }))
  }

  render() {

    const {classes, listHistoryMinitest, total, backTab} = this.props;
    const count = total;

    if (this.props.active === '2') {
      if (this.inputFocus.current) {
        this.inputFocus.current.focus()
      }
    }
    const columns = [
      {
        dataIndex: 'sl',
        title: (
          <div style={{fontWeight: '700'}}>
            {/*STT*/}
            <FormattedMessage id="index" tagName="data" />
          </div>
        ),
        className: 'drag-visible',
        width: window.screen.width * 0.02,
        render: data => {
          return (
            <>
              <Row>
                <Col xs={6} style={{paddingRight: '0px'}}>
                  {data}
                </Col>
              </Row>
            </>
          )
        }
      },
      {
        dataIndex: "doTestDate",
        title: (
          <div style={{fontWeight: '700'}}>
            {/*Thời điểm làm bài*/}
            <FormattedMessage id="time.of.doing.test" tagName="data" />
          </div>
        ),
        className: 'drag-visible',
        width: window.screen.width * 0.05,
        render: data => (<div>{getUTCTime(new Date(data))}</div>),
      },
      {
        dataIndex: "totalCorrectAnswerListening",
        title: (
          <div style={{fontWeight: '700'}}>
            {/*Số câu đúng phần nghe*/}
            <FormattedMessage id="total.of.correct.answer.of.listening.part" tagName="data" />
          </div>
        ),
        className: 'drag-visible',
        width: window.screen.width * 0.05,
        render: data => (<div title={data}>{data}</div>)
      },
      {
        dataIndex: "totalCorrectAnswerReading",
        title: (
          <div style={{fontWeight: '700'}}>
            {/*Số câu đúng phần đọc*/}
            <FormattedMessage id="total.of.correct.answer.of.reading.part" tagName="data" />
          </div>
        ),
        className: 'drag-visible',
        width: window.screen.width * 0.05,
        render: data => (<div title={data}>{data}</div>)
      },
      {
        dataIndex: "part1",
        title: (
          <div style={{fontWeight: '700'}}>
            {/*Số câu đúng Part 1*/}
            <FormattedMessage id="total.of.correct.answer.of.part1" tagName="data" />
          </div>
        ),
        className: 'drag-visible',
        width: window.screen.width * 0.05,
        render: data => (<div title={data}>{data}</div>)
      },
      {
        dataIndex: "part2",
        title: (
          <div style={{fontWeight: '700'}}>
            {/*Số câu đúng Part 2*/}
            <FormattedMessage id="total.of.correct.answer.of.part2" tagName="data" />
          </div>
        ),
        className: 'drag-visible',
        width: window.screen.width * 0.05,
        render: data => (<div title={data}>{data}</div>)
      },
      {
        dataIndex: "part3",
        title: (
          <div style={{fontWeight: '700'}}>
            {/*Số câu đúng Part 3*/}
            <FormattedMessage id="total.of.correct.answer.of.part3" tagName="data" />
          </div>
        ),
        className: 'drag-visible',
        width: window.screen.width * 0.05,
        render: data => (<div title={data}>{data}</div>)
      },
      {
        dataIndex: "part4",
        title: (
          <div style={{fontWeight: '700'}}>
            {/*Số câu đúng Part 4*/}
            <FormattedMessage id="total.of.correct.answer.of.part4" tagName="data" />
          </div>
        ),
        className: 'drag-visible',
        width: window.screen.width * 0.05,
        render: data => (<div title={data}>{data}</div>)
      },
      {
        dataIndex: "part5",
        title: (
          <div style={{fontWeight: '700'}}>
            {/*Số câu đúng Part 5*/}
            <FormattedMessage id="total.of.correct.answer.of.part5" tagName="data" />
          </div>
        ),
        className: 'drag-visible',
        width: window.screen.width * 0.05,
        render: data => (<div title={data}>{data}</div>)
      },
      {
        dataIndex: "part6",
        title: (
          <div style={{fontWeight: '700'}}>
            {/*Số câu đúng Part 6*/}
            <FormattedMessage id="total.of.correct.answer.of.part6" tagName="data" />
          </div>
        ),
        className: 'drag-visible',
        width: window.screen.width * 0.05,
        render: data => (<div title={data}>{data}</div>)
      },
      {
        dataIndex: "part7",
        title: (
          <div style={{fontWeight: '700'}}>
            {/*Số câu đúng Part 7*/}
            <FormattedMessage id="total.of.correct.answer.of.part7" tagName="data" />
          </div>
        ),
        className: 'drag-visible',
        width: window.screen.width * 0.05,
        render: data => (<div title={data}>{data}</div>)
      },
      {
        dataIndex: "part8",
        title: (
          <div style={{fontWeight: '700'}}>
            {/*Số câu đúng Part 8*/}
            <FormattedMessage id="total.of.correct.answer.of.part8" tagName="data" />
          </div>
        ),
        className: 'drag-visible',
        width: window.screen.width * 0.05,
        render: data => (<div title={data}>{data}</div>)
      },
      // {
      //   dataIndex: "totalTime",
      //   title: "Tổng thời gian làm bài",
      //   className: 'drag-visible',
      //   width: window.screen.width*0.05,
      //   render: data => (<div style={{
      //     whiteSpace: 'nowrap',
      //     width: '50px',
      //     overflow: 'hidden',
      //     textOverflow: 'ellipsis'}} title={data} >{data}</div>)
      // },
      {
        dataIndex: "",
        title: (
          <div style={{fontWeight: '700', textAlign: 'center'}}>
            {/*Xem chi tiết*/}
            <FormattedMessage id="Detail" tagName="data" />
          </div>
        ),
        className: 'drag-visible center',
        width: window.screen.width * 0.05,
        render: (data, record) => (
          <div style={{textAlign: 'center'}}>
            <OverlayTrigger placement="top" overlay={props => (<Tooltip {...props}>
              {/*Xem chi tiết*/}
              <FormattedMessage id="Detail" tagName="data" />
            </Tooltip>)}>
              <button id="btnHighLight" style={{border: 'none', backgroundColor: 'rgb(0 0 0 / 0%)'}}
                      onClick={() => {

                        const {testAction} = this.props;
                        const {
                          submitAnswerMinitest, cleanTest, checkGetDetailOrSubmitMinitest,
                          showHideButtonBackHistoryMinitest
                        } = testAction;
                        cleanTest();

                        checkGetDetailOrSubmitMinitest();
                        // showHideButtonBackHistoryMinitest(true);

                        let detailHistoryMinitest = {};
                        detailHistoryMinitest.minitestId = record.testId;
                        detailHistoryMinitest.checkDetail = 0;
                        submitAnswerMinitest(detailHistoryMinitest);

                        history.push("/pages/minitest");

                      }}
              >
                <Eye size={15} className=" mr-1 fonticon-wrap" id="btnHighLight" style={{cursor: "pointer", marginLeft: '25%'}}
                />
              </button>
            </OverlayTrigger>
          </div>
          )
      }
    ];
    let newData = [];

    if (listHistoryMinitest && listHistoryMinitest.data) {
      listHistoryMinitest.data.map((item, index) => {
        newData.push({sl: (this.state.pageSize) * (this.state.page - 1) + index + 1, ...item});
      });
    }

    const loadingComponent = (
      <div style={{
        position: 'absolute',
        zIndex: 110,
        top: 0,
        left: 0,
        width: '100%',
        height: '100%',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        background: 'rgba(255,255,255,0.8)'
      }}>
        <Spinner color="primary" className="reload-spinner"/>
      </div>
    );
    return (
      <>
        <style>{`
          #btnHighLight:focus-within {
            border: none !important;
            outline: 1px solid gray !important;
          }
          #postLink:focus-within {
            text-shadow: 1px 1px #1890ff;
          }
          #countDoTest:focus-within {
             border: none !important;
             outline: 1px solid gray !important;
          }
          #rank:focus-within {
             border: none !important;
             outline: 1px solid gray !important;
          }

          #btnSubmit {
            margin-top: 0% !important;
            padding: 0.85rem 2rem !important;
          }

          #btnSubmit:focus-within {
            border: none !important;
            outline: 1px solid gray !important;
          }

        `}

        </style>
        <Row>
          <Col sm="12">
            <Card>
              <CardHeader>
                <CardTitle><h1>
                  {/*Lịch sử làm bài Minitest*/}
                  <FormattedMessage id="history.of.doing.mini.test" tagName="data" />
                </h1></CardTitle>
                <Button color="primary" className="btn-icon right" color="primary" left onClick={backTab} title={showMessage("back")}>
                  <ArrowLeft/>
                </Button>
              </CardHeader>
              <CardBody>
                <Formik
                  initialValues={{
                    createDateFrom: '',
                    createDateTo: '',
                    searchValue: ''
                  }}
                  onSubmit={(values, actions) => {
                    this.setState({
                      loading: true,
                      page: 1,
                      createFrom: values.createDateFrom,
                      createTo: values.createDateTo,
                      searchValue: values.searchValue
                    })
                    this.onChangPageReceive(1)
                  }}
                  // validationSchema={formSchema}
                >
                  {({errors, touched, values,resetForm}) => (
                    <Form>
                      <FormGroup className="form-group d-flex">
                        <>
                          <Col sm={3} md={3} lg={3} xs={3}>
                            <FormGroup className="mb-0">
                              <div className="w-100">
                                <Label className={classes.fieldLabel} for="createDateFrom">
                                  {/*Thời điểm từ*/}
                                  <FormattedMessage id="time.from" tagName="data" />
                                </Label>

                                <Field
                                  id="createDateFromMnitest"
                                  type="date"
                                  name="createDateFrom"

                                  className={`form-control ${errors.createDateFrom &&
                                  touched.createDateFrom &&
                                  "is-invalid"}`}
                                  validate={() => this.validateDateFrom(values)}

                                />
                                {this.state.errDate1 ? (
                                  <div className={classes.error}>{this.state.errDate1}</div>
                                ) : null}

                                {/*<DatePicker*/}
                                  {/*autoFocus={true}*/}
                                  {/*type="date"*/}
                                  {/*locale="vi"*/}
                                  {/*name="createDateFrom"*/}
                                  {/*style={{height: '50px !important'}}*/}
                                  {/*className={`form-control ${errors.createDateFrom &&*/}
                                  {/*touched.createDateFrom &&*/}
                                  {/*"is-invalid"}`}*/}
                                  {/*onChangeRaw={this.handleDateChangeRaw}*/}
                                  {/*isClearable*/}
                                  {/*selected={this.state.createFrom}*/}
                                  {/*onChange={date => this.setState({createFrom: date})}*/}
                                  {/*dateFormat="dd/MM/yyyy"*/}
                                  {/*placeholderText="dd/mm/yyyy"*/}
                                  {/*onCalendarClose ={()=>{*/}
                                    {/*if(this.state.createFrom && this.state.createTo){*/}
                                      {/*if(this.state.createFrom > this.state.createTo){*/}
                                        {/*this.setState({*/}
                                          {/*message : "Thời điểm từ phải nhỏ hơn thời điểm đến"*/}
                                        {/*})*/}
                                      {/*} else {*/}
                                        {/*this.setState({*/}
                                          {/*message: ''*/}
                                        {/*})*/}
                                      {/*}*/}
                                    {/*}*/}
                                  {/*}}*/}
                                {/*/>*/}

                                {/*{this.state.message ? (*/}
                                  {/*<div className={classes.error}>{this.state.message}</div>*/}
                                {/*) : null}*/}

                              </div>
                            </FormGroup>
                          </Col>
                          <Col sm={3} md={3} lg={3} xs={3}>
                            <FormGroup className="mb-0">
                              <div className="w-100">
                                <Label className={classes.fieldLabel} for="createDateTo">
                                  {/*Thời điểm đến*/}
                                  <FormattedMessage id="time.to" tagName="data" />
                                </Label>

                                <Field
                                  type="date"
                                  id="createDateToMnitest"
                                  name="createDateTo"

                                  minDate='1900-01-01'
                                  className={`form-control ${errors.createDateTo &&
                                  touched.createDateTo &&
                                  "is-invalid"}`}
                                  validate={() => this.validateDateTo(values)}

                                />
                                {this.state.errDate2 ? (
                                  <div className={classes.error}>{this.state.errDate2}</div>
                                ) : null}

                                {/*<DatePicker*/}
                                  {/*// ref="createDateTo"*/}
                                  {/*type="date"*/}
                                  {/*locale="vi"*/}
                                  {/*name="createDateTo"*/}
                                  {/*style={{height: '38px'}}*/}
                                  {/*className={`form-control ${errors.createDateTo &&*/}
                                  {/*touched.createDateTo &&*/}
                                  {/*"is-invalid"}`}*/}
                                  {/*onChangeRaw={this.handleDateChangeRaw}*/}
                                  {/*isClearable*/}
                                  {/*selected={this.state.createTo}*/}
                                  {/*onChange={date => this.setState({createTo: date})}*/}
                                  {/*dateFormat="dd/MM/yyyy"*/}
                                  {/*placeholderText="dd/mm/yyyy"*/}
                                  {/*onCalendarClose ={()=>{*/}
                                    {/*if(this.state.createFrom && this.state.createTo){*/}
                                      {/*if(this.state.createFrom > this.state.createTo){*/}
                                        {/*this.setState({*/}
                                          {/*message : "Thời điểm từ phải nhỏ hơn thời điểm đến"*/}
                                        {/*})*/}
                                      {/*} else {*/}
                                        {/*this.setState({*/}
                                          {/*message: ''*/}
                                        {/*})*/}
                                      {/*}*/}
                                    {/*}*/}
                                  {/*}}*/}
                                {/*/>*/}

                                {/*{errors.createDateTo && touched.createDateTo ? (*/}
                                  {/*<div className={classes.error}>{errors.createDateTo}</div>*/}
                                {/*) : null}*/}
                              </div>
                            </FormGroup>
                          </Col>
                          <Col sm={1} md={1} lg={1} xs={1}>
                            <FormGroup className="mb-0" style={{display: 'table-caption'}}>
                              <div className="w-100">
                                <Label className={classes.fieldLabel}></Label>
                                <Button disabled={(this.state.errDate1 ? true : false) || (this.state.errDate2 ? true : false)} type="submit" className={classes.buttonSearch} color="primary"
                                        id="btnSubmit"
                                >
                                  {/*Tìm*/}
                                  <FormattedMessage id="search" tagName="data" />
                                </Button>
                                <Button type="reset" hidden={true} id="resetFormMiniTest" onClick={resetForm}>
                                </Button>
                              </div>
                            </FormGroup>
                          </Col>
                        </>

                      </FormGroup>
                    </Form>
                  )}
                </Formik>
              </CardBody>
            </Card>
          </Col>
          <Col sm="12">
            <Card>
              <CardBody>
                <div style={{position: 'relative'}}>
                  {this.props.loadingGetListHistoryMinitest ? loadingComponent : null}
                  <Table
                    columns={columns}
                    dataSource={newData}
                    pagination={false}
                    onChange={this.onChangPageReceive}
                    locale={{
                      emptyText: showMessage("data.not.found")
                    }}
                  />
                  <div style={{marginLeft: '40%', marginTop: '5%'}}>
                    <PaginationIconsAndTextForMinitest
                      totalItem={count}
                      onChangPage={this.onChangPageReceive}
                      checkSearch={this.state.isSearch}
                      resetIsSearch={this.resetIsSearch}
                    />
                  </div>

                </div>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </>
    )
  }
}

const mapDispatchToProps = dispatch => {
  return {
    studentAccountAction: bindActionCreators(studentAccountAction, dispatch),
    testAction: bindActionCreators(testAction, dispatch)
  }
}

const mapStateToProps = (state) => {
  return {
    studentAccountReducer: state.auth.login,
    listHistoryMinitest: state.doTestReducer.listHistoryMinitest,
    loadingGetListHistoryMinitest: state.doTestReducer.loadingGetListHistoryMinitest,
    total: state.doTestReducer.total,
    isLoading: state.historyTestReducer.isLoading,
    values: state.historyTestReducer
  }
}
const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
)

export default compose(withStyles(styles), withConnect)(HistoryMinitest);
