import React from "react"
import {Button, Card, CardBody, CardHeader, CardTitle, Col, FormGroup, Input, Label, Row, Spinner,} from "reactstrap"
import {ArrowLeft, Eye} from "react-feather"
import "../../../assets/css/style-modal-account.css"
import {bindActionCreators, compose} from "redux";
import {connect} from "react-redux";
import {withStyles} from "@material-ui/styles";
import styles from "./styles";
import studentAccountAction from "../../../redux/actions/studentAccount"
import {Field, Form, Formik} from "formik";
import {Table} from "antd";
import PaginationIconsAndTextForMinitest from './pagingtionHistoryMinitest';
import 'antd/dist/antd.css';
import {getUTCTimeSeconds, getUTCTimeSecondsForHistoryPractices, getUTCTime} from "../../../commons/utils/DateUtils";
import Tooltip from "react-bootstrap/Tooltip";
import OverlayTrigger from "react-bootstrap/OverlayTrigger";
import {history} from "../../../history";
import historyPracticesAction from '../../../redux/actions/history-practices'
import * as Yup from "yup";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";


import topicAction from "../../../redux/actions/topic/topicAction";
import practiceListenMultiActions from "../../../redux/actions/practice/listening"
import {
  CODE_PART1_LF,
  CODE_PART2_LF,
  CODE_PART3_LF,
  CODE_PART4_LF,
  CODE_PART5,
  CODE_PART6,
  CODE_PART7,
  CODE_PART8
} from "../../../commons/constants/CONSTANTS";

import readingActions from "../../../redux/actions/practice/reading";
import {FormattedMessage} from "react-intl";
import {showMessage} from "../../../commons/utils/convertDataForToast";


const formSchema = Yup.object().shape({
  createDateFrom: Yup.string()
    .matches(/^((2000|2400|2800|(19|2[0-9](0[48]|[2468][048]|[13579][26])))-02-29)$|^(((19|2[0-9])[0-9]{2})-02-(0[1-9]|1[0-9]|2[0-8]))$|^(((19|2[0-9])[0-9]{2})-(0[13578]|10|12)-(0[1-9]|[12][0-9]|3[01]))$|^(((19|2[0-9])[0-9]{2})-(0[469]|11)-(0[1-9]|[12][0-9]|30))$/, "Ngày tháng không đúng định dạng"),
  createDateTo: Yup.string()
    .matches(/^((2000|2400|2800|(19|2[0-9](0[48]|[2468][048]|[13579][26])))-02-29)$|^(((19|2[0-9])[0-9]{2})-02-(0[1-9]|1[0-9]|2[0-8]))$|^(((19|2[0-9])[0-9]{2})-(0[13578]|10|12)-(0[1-9]|[12][0-9]|3[01]))$|^(((19|2[0-9])[0-9]{2})-(0[469]|11)-(0[1-9]|[12][0-9]|30))$/, "Ngày tháng không đúng định dạng")

});

let listOptionsTypeForHistoryPractices;
let listOptionsPartForHistoryPractices;
let listOptionsTopicForHistoryPractices;

// moment.tz.setDefault("Asia/Ho_Chi_Minh");


let errDate1 = "";
let errDate2 = "";

function validateDateFrom(date,value) {
  if (value.createDateFrom === "") {
    if (document.getElementById("createDateFromHp") !== null && document.getElementById("createDateFromHp").validationMessage !== "") {
      errDate1 = showMessage("date.not.correct.format");
    } else {
      errDate1 = ""
    }
  } else {
    if (!/^((2000|2400|2800|(19|2[0-9](0[48]|[2468][048]|[13579][26])))-02-29)$|^(((19|2[0-9])[0-9]{2})-02-(0[1-9]|1[0-9]|2[0-8]))$|^(((19|2[0-9])[0-9]{2})-(0[13578]|10|12)-(0[1-9]|[12][0-9]|3[01]))$|^(((19|2[0-9])[0-9]{2})-(0[469]|11)-(0[1-9]|[12][0-9]|30))$/i.test(value.createDateFrom)) {
      errDate1 = showMessage("date.not.correct.format");
    } else if (value.createDateFrom && value.createDateTo) {
      if (Date.parse(value.createDateFrom) > Date.parse(value.createDateTo)) {
        errDate1 = showMessage("compare.time");
      } else {
        errDate1 = ""
      }
    } else {
      errDate1 = ""
    }
  }
}

function validateDateTo(value) {
  if (value.createDateTo === "") {
    if (document.getElementById("createDateToHp") !== null && document.getElementById("createDateToHp").validationMessage !== "") {
      errDate2 = showMessage("date.not.correct.format");
    } else {
      errDate2 = ""
    }
  } else {
    if (!/^((2000|2400|2800|(19|2[0-9](0[48]|[2468][048]|[13579][26])))-02-29)$|^(((19|2[0-9])[0-9]{2})-02-(0[1-9]|1[0-9]|2[0-8]))$|^(((19|2[0-9])[0-9]{2})-(0[13578]|10|12)-(0[1-9]|[12][0-9]|3[01]))$|^(((19|2[0-9])[0-9]{2})-(0[469]|11)-(0[1-9]|[12][0-9]|30))$/i.test(value.createDateTo)) {
      errDate2 = showMessage("date.not.correct.format");
    } else if (value.createDateFrom && value.createDateTo) {
      if (Date.parse(value.createDateFrom) > Date.parse(value.createDateTo)) {
        errDate1 = showMessage("compare.time");;
        errDate2 = ""
      } else {
        errDate1 = ""
      }
    } else {
      errDate2 = ""
    }
  }
}

class HistoryPractices extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      activeTab: "1",
      isOpen: false,
      createFrom:'',
      createTo:'',
      searchValue:'',
      page: 1,
      pageSize: 10,
      count: 1,
      modalDetailTest: false,
      testId: 0,
      testName: '',
      modalRankTest: false,
      rankOfUser:'',
      isSearch: null,
      message:'',
      typeName:'',
      typeCode: '',
      partName: '',
      part: '',
      levelCode: '',
      topicName: '',
      topicId: 0,
      addDescriptionForPart: true,

      errDate1: '',
      errDate2: '',

      defaultCreateFrom: ''

    }
    this.inputFocus = React.createRef();

  }

  onChangeDateFrom =(value)=>{{
    this.setState({
      defaultCreateFrom: value.createDateFrom
    })
  }}

  validateDateFrom =(value)=> {
    if (value.createDateFrom === "") {
      if (document.getElementById("createDateFromHp") !== null && document.getElementById("createDateFromHp").validationMessage !== "") {
        // errDate1 = showMessage("date.not.correct.format");
        this.setState({
          errDate1: showMessage("date.not.correct.format")
        })
      } else {
        // errDate1 = ""
        this.setState({
          errDate1: ''
        })
      }
    } else {
      if (!/^((2000|2400|2800|(19|2[0-9](0[48]|[2468][048]|[13579][26])))-02-29)$|^(((19|2[0-9])[0-9]{2})-02-(0[1-9]|1[0-9]|2[0-8]))$|^(((19|2[0-9])[0-9]{2})-(0[13578]|10|12)-(0[1-9]|[12][0-9]|3[01]))$|^(((19|2[0-9])[0-9]{2})-(0[469]|11)-(0[1-9]|[12][0-9]|30))$/i.test(value.createDateFrom)) {
        // errDate1 = showMessage("date.not.correct.format");
        this.setState({
          errDate1: showMessage("date.not.correct.format")
        })
      } else if (value.createDateFrom && value.createDateTo) {
        if (Date.parse(value.createDateFrom) > Date.parse(value.createDateTo)) {
          // errDate1 = showMessage("compare.time");
          this.setState({
            errDate1: showMessage("compare.time")
          })
        } else {
          // errDate1 = ""
          this.setState({
            errDate1: ''
          })
        }
      } else {
        // errDate1 = ""
        this.setState({
          errDate1: ''
        })
      }
    }
  }

  validateDateTo =(value)=> {
    if (value.createDateTo === "") {
      if (document.getElementById("createDateToHp") !== null && document.getElementById("createDateToHp").validationMessage !== "") {
        // errDate2 = showMessage("date.not.correct.format");
        this.setState({
          errDate2: showMessage("date.not.correct.format")
        })
      } else {
        // errDate2 = ""
        this.setState({
          errDate2: ''
        })
      }
    } else {
      if (!/^((2000|2400|2800|(19|2[0-9](0[48]|[2468][048]|[13579][26])))-02-29)$|^(((19|2[0-9])[0-9]{2})-02-(0[1-9]|1[0-9]|2[0-8]))$|^(((19|2[0-9])[0-9]{2})-(0[13578]|10|12)-(0[1-9]|[12][0-9]|3[01]))$|^(((19|2[0-9])[0-9]{2})-(0[469]|11)-(0[1-9]|[12][0-9]|30))$/i.test(value.createDateTo)) {
        // errDate2 = showMessage("date.not.correct.format");
        this.setState({
          errDate2: showMessage("date.not.correct.format")
        })
      } else if (value.createDateFrom && value.createDateTo) {
        if (Date.parse(value.createDateFrom) > Date.parse(value.createDateTo)) {
          // errDate1 = showMessage("compare.time");
          // errDate2 = ""
          this.setState({
            errDate1: showMessage("compare.time"),
            errDate2: ''
          })
        } else {
          // errDate1 = ""
          this.setState({
            errDate1: ''
          })
        }
      } else {
        // errDate2 = ""
        this.setState({
          errDate2: ''
        })
      }
    }
  }


  toggle = tab => {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab,
        isOpen: !this.state.isOpen
      })
    }
  }

  resetIsSearch = () => {
    this.setState({
      isSearch: null
    })
  }

  onChangPageReceive = (pageNumber) => {
    this.setState({
      page: pageNumber
    })
    if (pageNumber === 1) {
      this.setState({
        isSearch: 1,
      })
    }
    const searchPropertiesBE = {};
    searchPropertiesBE.keySearch = this.state.searchValue.toString().trim()
    searchPropertiesBE.typeCode = this.state.typeCode
    searchPropertiesBE.part = this.state.part
    searchPropertiesBE.topicId = this.state.topicId
    searchPropertiesBE.levelCode = this.state.levelCode

    let {createFrom, createTo} = this.state;

    searchPropertiesBE.createFrom = createFrom;

    // searchPropertiesBE.createFrom.setDate(searchPropertiesBE.createFrom.getDate() + 1)

    searchPropertiesBE.createTo = createTo;
    // searchPropertiesBE.createTo.setDate(searchPropertiesBE.createTo.getDate() + 1)

    searchPropertiesBE.pageSize = this.state.pageSize
    searchPropertiesBE.page = pageNumber
    searchPropertiesBE.userId = this.props.studentAccountReducer.userInfoLogin.userId;
    this.props.historyPracticesAction.getListHistoryPractices(searchPropertiesBE)

    // this.setState({
    //   createFrom: searchPropertiesBE.createFrom.setDate(searchPropertiesBE.createFrom.getDate() - 1),
    //   createTo: searchPropertiesBE.createTo.setDate(searchPropertiesBE.createTo.getDate() - 1)
    // })
  }

  viewDetail = (record)=>{
    this.toggleModalDetailTest(record.testId, record.testName);
  }

  toggleModalDetailTest = (testId, testName) => {
    this.setState(prevState => ({
      testId: testId,
      testName: testName,
      modalDetailTest: !prevState.modalDetailTest
    }))
  }

  handleDateChangeRaw = (e) => {
    e.preventDefault();
  }

  componentDidMount() {
    const {historyPracticesAction} = this.props;
    const {getTypeForHistoryPractices, getPartForHistoryPractices, getTopicForHistoryPractices} = historyPracticesAction;
    getTypeForHistoryPractices();
    getPartForHistoryPractices({});
    getTopicForHistoryPractices({});
  }


  handleViewDetailPractices =(id, part, levelCode, topicId, userId,createDate, topicName, typeCode)=>{
    if(part === "PART1" || part === "PART2"){
      debugger
      const {practiceListenMultiActions, readingActions} = this.props;
      const {getDetailHistoryLisSingle} = practiceListenMultiActions;
      readingActions.clearTempReadingSingleAndDualQuestion();
      const historyPractices = {};
      historyPractices.id = id;
      historyPractices.part = part;
      getDetailHistoryLisSingle(historyPractices);
      readingActions.updateViewBtnReading();
      history.push("/pages/aggregatedResults")
    }
    if(part === "PART3" || part === "PART4"){
      const {practiceListenMultiActions, historyPracticesAction, readingActions} = this.props;
      const {getDetailHistoryListening, cleanBeforeGetDetailHL} = practiceListenMultiActions;
      cleanBeforeGetDetailHL();
      readingActions.clearTempReadingSingleAndDualQuestion();
      const {showButtonSaveResultListening} = historyPracticesAction;
      showButtonSaveResultListening();

      const historyPractices = {};
      historyPractices.id = id;
      getDetailHistoryListening(historyPractices);
      readingActions.updateViewBtnReading();
      if(part === "PART3"){
        history.push("/pages/practice-listening-toeic-conversation")
      }
      else {
        history.push("/pages/practice-listening-toeic-short-talking")
      }
    }

    if(part === CODE_PART1_LF || part === CODE_PART2_LF || part === CODE_PART3_LF || part === CODE_PART4_LF){
      const {practiceListenMultiActions,readingActions} = this.props;
      const {getHistoryListenFill, getValueTypeCode,getPartPracticeListening,getNameTopic} = practiceListenMultiActions;
      practiceListenMultiActions.cleanListQuestionOfListeningWordFill();
      practiceListenMultiActions.cleanResultQuestionOfListeningWordFill();
      practiceListenMultiActions.cleanNumberCorrectAnswerWordFill();
      practiceListenMultiActions.cleanListQuestionAndAnswerListening();
      readingActions.clearTempReadingSingleAndDualQuestion();
      readingActions.cleanListQuestionOfReadingWordFill();
      readingActions.cleanResultQuestionOfReadingWordFill();
      readingActions.cleanNumberCorrectAnswer();

      readingActions.updateViewBtnReading();

      getValueTypeCode(typeCode);
      getPartPracticeListening(part);
      getNameTopic(topicName);
      let historyLF = {};
      historyLF.id = id;
      historyLF.createDate = createDate;
      historyLF.part = part;
      getHistoryListenFill(historyLF);

      history.push("/pages/aggregatedResults");
    }
    if(part === CODE_PART5){
      const {readingActions,practiceListenMultiActions} = this.props;
      const {getHistoryInCompleteSentences, getTypeCodeReading,getPart,getNameTopic } = readingActions;
      // readingActions.cleanListQuestionOfReadingWordFill();
      // readingActions.cleanResultQuestionOfReadingWordFill();
      practiceListenMultiActions.cleanListQuestionOfListeningWordFill();
      practiceListenMultiActions.cleanResultQuestionOfListeningWordFill();
      practiceListenMultiActions.cleanNumberCorrectAnswerWordFill();
      practiceListenMultiActions.cleanListQuestionAndAnswerListening();
      practiceListenMultiActions.cleanTypeNameLevel();
      readingActions.clearTempReadingSingleAndDualQuestion();
      readingActions.cleanListQuestionOfReadingWordFill();
      readingActions.cleanResultQuestionOfReadingWordFill();
      readingActions.cleanNumberCorrectAnswer();

      practiceListenMultiActions.updateViewBtnListening();
      let historyRF = {};
      getTypeCodeReading(typeCode);
      getPart(part);
      getNameTopic(topicName);
      historyRF.id = id;
      historyRF.createDate = createDate;
      getHistoryInCompleteSentences(historyRF);
      history.push("/pages/aggregatedResults");
    }
    if(part === CODE_PART6){
      const {readingActions, practiceListenMultiActions} = this.props;
      const {getDetailHistoryReadingCompletedPassage, getTypeCodeReading,getPart,getNameTopic } = readingActions;
      practiceListenMultiActions.cleanListQuestionOfListeningWordFill();
      practiceListenMultiActions.cleanResultQuestionOfListeningWordFill();
      practiceListenMultiActions.cleanNumberCorrectAnswerWordFill();
      practiceListenMultiActions.cleanListQuestionAndAnswerListening();
      practiceListenMultiActions.cleanTypeNameLevel();
      readingActions.cleanListQuestionOfReadingWordFill();
      readingActions.cleanResultQuestionOfReadingWordFill();
      readingActions.cleanNumberCorrectAnswer();
      readingActions.clearTempReadingSingleAndDualQuestion();
      practiceListenMultiActions.updateViewBtnListening();

      let historyRC = {};
      getTypeCodeReading(typeCode);
      getPart(part);
      getNameTopic(topicName);
      historyRC.id = id;
      historyRC.createDate = createDate;
      getDetailHistoryReadingCompletedPassage(historyRC);
      history.push("/pages/aggregatedResults");
    }
    if(part === CODE_PART7 || part === CODE_PART8){
      const {readingActions , practiceListenMultiActions} = this.props;
      const {getDetailHistoryReadingSingle,getTypeCodeReading,getPart,getNameTopic} = readingActions;
      readingActions.clearTempReadingSingleAndDualQuestion();
      readingActions.clearTotalTrueReadingSingleAndDual();
      practiceListenMultiActions.cleanListQuestionOfListeningWordFill();
      practiceListenMultiActions.cleanResultQuestionOfListeningWordFill();
      practiceListenMultiActions.cleanNumberCorrectAnswerWordFill();
      practiceListenMultiActions.cleanListQuestionAndAnswerListening();
      practiceListenMultiActions.cleanTypeNameLevel();
      readingActions.cleanListQuestionOfReadingWordFill();
      readingActions.cleanResultQuestionOfReadingWordFill();
      readingActions.cleanNumberCorrectAnswer();
      practiceListenMultiActions.updateViewBtnListening();
      const historyPractices = {};
      historyPractices.id = id;
      getTypeCodeReading(typeCode);
      getPart(part);
      getNameTopic(topicName);
      getDetailHistoryReadingSingle(historyPractices);
      practiceListenMultiActions.updateViewBtnListening();
      readingActions.updateViewBtnReading();
      history.push("/pages/aggregatedResults")
    }
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    debugger
    if(prevProps.active !== this.props.active){
      // errDate1 = null;
      // errDate2 = null;

      document.getElementById("createDateFromHp").value = null ;

      this.setState({
        activeTab: "1",
        isOpen: false,
        createFrom:'',
        createTo:'',
        searchValue:'',
        page: 1,
        pageSize: 10,
        count: 1,
        modalDetailTest: false,
        testId: 0,
        testName: '',
        modalRankTest: false,
        rankOfUser:'',
        isSearch: null,
        message:'',
        typeName:'',
        typeCode: '',
        partName: '',
        part: '',
        levelCode: '',
        topicName: '',
        topicId: 0,
        addDescriptionForPart: true,
        errDate1: '',
        errDate2: '',
        defaultCreateFrom: '1999-08-31'
      })
    }
  }

  render() {
    debugger
    const {classes, listHistoryPractices, total,backTab} = this.props;
    const {part, topicId} = this.state;
    const count = total;

    const {listTypeForHistoryPractices, listPartForHistoryPractices, listTopicForHistoryPractices} = this.props;
    if(undefined !== listTypeForHistoryPractices){
      listOptionsTypeForHistoryPractices = listTypeForHistoryPractices.map((type) => {
        return (
          <option title={type.name.length > 20 ? type.name : ""} value={type.value}>
            {/*{type.name.length > 20 ? type.name.substring(0, 20) + "..." : type.name}*/}
            { showMessage(type.code.toLowerCase().replaceAll("_",".")) }
          </option>
        );
      })
    }

    if(undefined !== listPartForHistoryPractices){
      listOptionsPartForHistoryPractices = listPartForHistoryPractices.map((part) => {
        // return (
        //   <option title={part.name.length > 20 ? part.name : ""} value={part.code}>
        //     {part.name.length > 20 ? part.name.substring(0, 20) + "..." : part.name}
        //   </option>
        // );

        if (part.parentCode === "LISTENING_FILL_UNIT" && this.state.addDescriptionForPart === true) {
          return <option value={part.code}>
            {/*{part.name + " (" + showMessage("listening.fill.unit") + ")"}*/}
            { showMessage(part.code.toLowerCase().replaceAll("_",".")) + " (" + showMessage("listening.fill.unit") + ")"}
          </option>
        } else if (part.parentCode === "LISTENING_UNIT" && this.state.addDescriptionForPart === true) {
          return <option value={part.code}>
            {/*{part.name + " (" + showMessage("listening.unit") + ")"}*/}
            { showMessage(part.code.toLowerCase().replaceAll("_",".")) + " (" + showMessage("listening.unit") + ")"}
          </option>
        } else {
          return <option value={part.code}>
            {/*{part.name}*/}
            { showMessage(part.code.toLowerCase().replaceAll("_",".")) }
          </option>
        }
      })
    }

    if(undefined !== listTopicForHistoryPractices){
      listOptionsTopicForHistoryPractices = listTopicForHistoryPractices.map((topic) => {
        return (
          <option title={topic.topicName.length > 30 ? topic.topicName : ""} value={topic.topicId}>
            {topic.topicName.length > 30 ? topic.topicName.substring(0, 30) + "..." : topic.topicName}
          </option>
        );
      })
    }

    if(this.props.active === '2'){
      if(this.inputFocus.current){
        this.inputFocus.current.focus()
      }
    }
    const columns = [
      {
        dataIndex: 'sl',
        title: (
          <div style={{fontWeight: '700'}}>
            {/*STT*/}
            <FormattedMessage id="index" tagName="data" />
          </div>
        ),
        className: 'drag-visible',
        width: window.screen.width*0.05,
        render: data =>{
          return (
            <>
              <Row>
                <Col xs={6} style={{paddingRight: '0px'}}>
                  {data}
                </Col>
              </Row>
            </>
          )
        }
      },
      {
        dataIndex: "createDate",
        title: (
          <div style={{fontWeight: '700'}}>
            {/*Thời điểm làm bài*/}
            <FormattedMessage id="time.of.doing.test" tagName="data" />
          </div>
        ),
        className: 'drag-visible',
        width:window.screen.width*0.15,
        render: data =>(<div>{getUTCTime(new Date(data))}</div>),
      },
      {
        dataIndex: "typeCode",
        title: (
          <div style={{fontWeight: '700'}}>
            {/*Loại bài*/}
            <FormattedMessage id="type.of.category" tagName="data" />
          </div>
        ),
        className: 'drag-visible',
        width: window.screen.width*0.12,
        render: data => (
          <div title={data==="1" ? showMessage("reading.unit") :
            ( data==="2" ? showMessage("listening.unit") : showMessage("listening.fill.unit") )} >
            {data==="1" ? showMessage("reading.unit") :
              ( data==="2" ? showMessage("listening.unit") : showMessage("listening.fill.unit") )}
          </div>
        )
      },
      {
        dataIndex: "part",
        title: (
          <div style={{fontWeight: '700'}}>
            {/*Phần*/}
            <FormattedMessage id="part" tagName="data" />
          </div>
        ),
        className: 'drag-visible',
        width: window.screen.width*0.12,
        render: data => (<div title={showMessage(data.toLowerCase().replaceAll("_","."))} >
          {/*{data}*/}
          {showMessage(data.toLowerCase().replaceAll("_","."))}
        </div>)
      },
      {
        dataIndex: "levelCode",
        title: (
          <div style={{fontWeight: '700'}}>
            {/*Trình độ*/}
            <FormattedMessage id="level" tagName="data" />
          </div>
        ),
        className: 'drag-visible',
        width: window.screen.width*0.09,
        render: data => (<div  title={data} >
          {data === "EASY" ? <>{showMessage("easy")}</> : data === "MEDIUM" ?
            <>{showMessage("medium")}</> : <>{showMessage("difficult")}</>}
        </div>)
      },
      {
        dataIndex: "topicName",
        title: (
          <div style={{fontWeight: '700'}}>
            {/*Tên chủ đề*/}
            <FormattedMessage id="category.name" tagName="data" />
          </div>
        ),
        className: 'drag-visible',
        width: window.screen.width*0.1,
        render: data => (<div title={data} >{data}</div>)
      },
      {
        dataIndex: "numberCorrect",
        title: (
          <div style={{fontWeight: '700'}}>
            {/*Số câu đúng*/}
            <FormattedMessage id="total.of.correct.answer" tagName="data" />
          </div>
        ),
        className: 'drag-visible',
        width: window.screen.width*0.08,
        render: data => (<div title={data} >{data}</div>)
      },
      {
        dataIndex: "",
        title: (
          <div style={{fontWeight: '700', textAlign: 'center'}}>
            {/*Xem chi tiết*/}
            <FormattedMessage id="Detail" tagName="data" />
          </div>
        ),
        className: 'drag-visible',
        width:window.screen.width*0.08,
        render: (data,record) => (
          <div style={{textAlign: 'center'}}>
            <OverlayTrigger placement="top" overlay={props => (<Tooltip {...props}>
              <FormattedMessage id="Detail" tagName="data" />
            </Tooltip>)}>
              <button id="btnHighLight" style={{border:'none', backgroundColor: 'rgb(0 0 0 / 0%)'}}
                      onClick={()=>{
                        this.handleViewDetailPractices(record.id, record.part, record.levelCode, record.topicId, record.userId, record.createDate, record.topicName, record.typeCode);

                      }}
              >
                <Eye size={15} className=" mr-1 fonticon-wrap" id="btnHighLight" style={{cursor: "pointer", marginLeft:'25%'}}
                />
              </button>
            </OverlayTrigger>
          </div>
          )
      }
    ];
    let newData = [];

    if(listHistoryPractices && listHistoryPractices.data){
      listHistoryPractices.data.map((item, index) => {
        newData.push({sl: (this.state.pageSize) * (this.state.page - 1) + index + 1, ...item});
      });
    }

    const loadingComponent = (
      <div style={{
        position: 'absolute',
        zIndex: 110,
        top: 0,
        left: 0,
        width: '100%',
        height: '100%',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        background: 'rgba(255,255,255,0.8)'
      }}>
        <Spinner color="primary" className="reload-spinner"/>
      </div>
    );
    return (
      <>
        <style>{`
          #btnHighLight:focus-within {
            border: none !important;
            outline: 1px solid gray !important;
          }
          #btnSubmit {
            margin-top: 0% !important;
            padding: 0.85rem 2rem !important;
          }

          #btnSubmit:focus-within {
            border: none !important;
            outline: 1px solid gray !important;
          }

          #postLink:focus-within {
            text-shadow: 1px 1px #1890ff;
          }
          #countDoTest:focus-within {
             border: none !important;
             outline: 1px solid gray !important;
          }
          #rank:focus-within {
             border: none !important;
             outline: 1px solid gray !important;
          }
        `}

        </style>
        <Row>
          <Col sm="12">
            <Card>
              <CardHeader>
                <CardTitle>
                  <h1>
                    {/*Lịch sử làm bài Luyện tập*/}
                    <FormattedMessage id="history.of.doing.practices" tagName="data" />
                  </h1>
                </CardTitle>
                <Button color="primary" className="btn-icon right" color="primary" left onClick={backTab} title={showMessage("back")}>
                  <ArrowLeft/>
                </Button>
              </CardHeader>
              <CardBody>
                <Formik
                  initialValues={{
                    typeCode: '',
                    part: '',
                    topicId: 0,
                    levelCode: '',
                    createDateFrom: '',
                    createDateTo: '',
                    searchValue: ''
                  }}
                  onSubmit={(values, actions) => {
                    this.setState({
                      loading: true,
                      page: 1,
                      createFrom: values.createDateFrom,
                      createTo: values.createDateTo,
                      searchValue: values.searchValue
                    })
                    this.onChangPageReceive(1)
                  }}
                  // validationSchema={formSchema}
                >
                  {({errors, touched, values,resetForm}) => (
                    <Form>
                      <FormGroup className="form-group">
                        <>
                          <Row>
                            <Col sm={3} md={3} lg={3} xs={3}>
                              <FormGroup className="mb-0">
                                <Label>
                                  {/*Loại đề bài*/}
                                  <FormattedMessage id="type.of.category" tagName="data" />
                                </Label>
                                <Input
                                  type="select"
                                  name="typeCode"
                                  value={this.state.typeCode}
                                  onChange={e => {
                                    this.setState(
                                      {
                                        typeCode: e.target.value,
                                        part: "",
                                        topicId: 0,
                                      }
                                    )

                                    if(e.target.value === ""){
                                      this.setState(
                                        {
                                          addDescriptionForPart: true
                                        }
                                      )
                                    }
                                    else {
                                      this.setState(
                                        {
                                          addDescriptionForPart: false
                                        }
                                      )
                                    }

                                    const {historyPracticesAction} = this.props;
                                    const {getTypeForHistoryPractices, getPartForHistoryPractices, getTopicForHistoryPractices} = historyPracticesAction;
                                    let apParam = {};
                                    apParam.value = e.target.value;
                                    getPartForHistoryPractices(apParam);

                                    let apParamForTopic = {};
                                    apParamForTopic.type = e.target.value;
                                    apParamForTopic.part = this.state.part;
                                    getTopicForHistoryPractices(apParamForTopic);
                                  }}
                                >
                                  <option defaultChecked value="">
                                    {/*Tất cả*/}
                                    {showMessage("all")}
                                  </option>
                                  {listOptionsTypeForHistoryPractices}
                                  {/*<option value="">Chủ đề A</option>*/}
                                  {/*<option value="">Chủ đề B</option>*/}
                                  {/*<option value="">Chủ đề C</option>*/}
                                </Input>
                              </FormGroup>
                            </Col>
                            <Col sm={3} md={3} lg={3} xs={3}>
                              <FormGroup className="mb-0">
                                <Label>
                                  {/*Phần*/}
                                  <FormattedMessage id="part" tagName="data" />
                                </Label>
                                <Input
                                  type="select"
                                  name="part"
                                  value={this.state.part}
                                  onChange={e => {
                                    this.setState(
                                      {
                                        part: e.target.value,
                                        topicId: 0
                                      }
                                    )


                                    const {historyPracticesAction} = this.props;
                                    const {getPartForHistoryPractices, getTopicForHistoryPractices} = historyPracticesAction;

                                    let apParamForTopic = {};
                                    apParamForTopic.type = this.state.typeCode;
                                    apParamForTopic.part = e.target.value;
                                    getTopicForHistoryPractices(apParamForTopic);

                                  }}
                                >
                                  <option defaultChecked value="">
                                    {/*Tất cả*/}
                                    {showMessage("all")}
                                  </option>
                                  {listOptionsPartForHistoryPractices}
                                </Input>
                              </FormGroup>
                            </Col>
                            <Col sm={3} md={3} lg={3} xs={3}>
                              <FormGroup className="mb-0">
                                <Label>
                                  {/*Chủ đề */}
                                  <FormattedMessage id="topic" tagName="data" />
                                </Label>
                                <Input
                                  type="select"
                                  name="topicId"
                                  value={this.state.topicId}
                                  onChange={e => {
                                    this.setState(
                                      {
                                        topicId: e.target.value
                                      }
                                    )
                                  }}
                                >
                                  <option defaultChecked value="">
                                    {/*Tất cả*/}
                                    {showMessage("all")}
                                  </option>
                                  {listOptionsTopicForHistoryPractices}
                                </Input>
                              </FormGroup>
                            </Col>
                          </Row>

                          <Row>
                            <Col sm={3} md={3} lg={3} xs={3}>
                              <FormGroup className="mb-0">
                                <Label for="department">
                                  {/*Trình độ*/}
                                  <FormattedMessage id="level" tagName="data" />
                                </Label>
                                <Input
                                  type="select"
                                  name="levelCode"
                                  onChange={e => {
                                    // let apParamDTO = {};
                                    // const {topicAction} = this.props;
                                    // const {fetchPartByTopic} = topicAction;
                                    this.setState(
                                      {
                                        levelCode: e.target.value
                                      }
                                    )
                                    // apParamDTO.parentCode = e.target.value;
                                    // fetchPartByTopic(apParamDTO);
                                    this.setState({reload: true})
                                    setTimeout(() => {
                                      this.setState({
                                        reload: false,
                                      })
                                    }, 1000)
                                  }}
                                >
                                  <option defaultChecked value="">
                                    {/*Tất cả*/}
                                    {showMessage("all")}
                                  </option>
                                  {/*{optionTypeTopic}*/}
                                  <option value="EASY">
                                    {/*Dễ*/}
                                    {showMessage("easy")}
                                  </option>
                                  <option value="MEDIUM">
                                    {/*Trung bình*/}
                                    {showMessage("medium")}
                                  </option>
                                  <option value="DIFFICULT">
                                    {/*Khó*/}
                                    {showMessage("difficult")}
                                  </option>
                                </Input>
                              </FormGroup>
                            </Col>
                            <Col sm={3} md={3} lg={3} xs={3}>
                              <FormGroup className="mb-0">
                                <div className="w-100">
                                  <Label className={classes.fieldLabel} for="createDateFrom">
                                    {/*Thời điểm từ*/}
                                    <FormattedMessage id="time.from" tagName="data" />
                                  </Label>

                                  <Field
                                    id="createDateFromHp"
                                    type="date"
                                    name="createDateFrom"
                                    // value={this.state.defaultCreateFrom}
                                    // onChange={() => this.onChangeDateFrom(values)}
                                    className={`form-control ${errors.createDateFrom &&
                                    touched.createDateFrom &&
                                    "is-invalid"}`}
                                    validate={() => this.validateDateFrom(values)}

                                  />
                                  {this.state.errDate1 ? (
                                    <div className={classes.error}>{this.state.errDate1}</div>
                                  ) : null}

                                  {/*<DatePicker*/}
                                  {/*type="date"*/}
                                  {/*name="createDateFrom"*/}
                                  {/*style={{height: '50px !important'}}*/}
                                  {/*className={`form-control ${errors.createDateFrom &&*/}
                                  {/*touched.createDateFrom &&*/}
                                  {/*"is-invalid"}`}*/}
                                  {/*onChangeRaw={this.handleDateChangeRaw}*/}
                                  {/*isClearable*/}
                                  {/*selected={this.state.createFrom}*/}
                                  {/*onChange={date => this.setState({createFrom: date})}*/}
                                  {/*dateFormat="dd/MM/yyyy"*/}
                                  {/*placeholderText="dd/mm/yyyy"*/}
                                  {/*onCalendarClose ={()=>{*/}
                                  {/**/}
                                  {/*if(this.state.createFrom && this.state.createTo){*/}
                                  {/*if(this.state.createFrom > this.state.createTo){*/}
                                  {/*this.setState({*/}
                                  {/*message : "Thời điểm từ phải nhỏ hơn thời điểm đến"*/}
                                  {/*})*/}
                                  {/*} else {*/}
                                  {/*this.setState({*/}
                                  {/*message: ''*/}
                                  {/*})*/}
                                  {/*}*/}
                                  {/*}*/}
                                  {/*}}*/}
                                  {/*/>*/}

                                  {/*{this.state.message ? (*/}
                                  {/*<div className={classes.error}>{this.state.message}</div>*/}
                                  {/*) : null}*/}

                                </div>
                              </FormGroup>
                            </Col>
                            <Col sm={3} md={3} lg={3} xs={3}>
                              <FormGroup className="mb-0">
                                <div className="w-100">
                                  <Label className={classes.fieldLabel} for="createDateTo">
                                    {/*Thời điểm đến*/}
                                    <FormattedMessage id="time.to" tagName="data" />
                                  </Label>

                                  <Field
                                    type="date"
                                    id="createDateToHp"
                                    name="createDateTo"

                                    minDate='1900-01-01'
                                    className={`form-control ${errors.createDateTo &&
                                    touched.createDateTo &&
                                    "is-invalid"}`}
                                    validate={() => this.validateDateTo(values)}

                                  />
                                  {this.state.errDate2 ? (
                                    <div className={classes.error}>{this.state.errDate2}</div>
                                  ) : null}

                                  {/*<DatePicker*/}
                                  {/*// ref="createDateTo"*/}
                                  {/*type="date"*/}
                                  {/*name="createDateTo"*/}
                                  {/*style={{height: '38px'}}*/}
                                  {/*className={`form-control ${errors.createDateTo &&*/}
                                  {/*touched.createDateTo &&*/}
                                  {/*"is-invalid"}`}*/}
                                  {/*onChangeRaw={this.handleDateChangeRaw}*/}
                                  {/*isClearable*/}
                                  {/*selected={this.state.createTo}*/}
                                  {/*onChange={date => this.setState({createTo: date})}*/}
                                  {/*dateFormat="dd/MM/yyyy"*/}
                                  {/*placeholderText="dd/mm/yyyy"*/}
                                  {/*onCalendarClose ={()=>{*/}
                                  {/*if(this.state.createFrom && this.state.createTo){*/}
                                  {/*if(this.state.createFrom > this.state.createTo){*/}
                                  {/*this.setState({*/}
                                  {/*message : "Thời điểm từ phải nhỏ hơn thời điểm đến"*/}
                                  {/*})*/}
                                  {/*} else {*/}
                                  {/*this.setState({*/}
                                  {/*message: ''*/}
                                  {/*})*/}
                                  {/*}*/}
                                  {/*}*/}
                                  {/*}}*/}
                                  {/*/>*/}

                                  {/*{errors.createDateTo && touched.createDateTo ? (*/}
                                  {/*<div className={classes.error}>{errors.createDateTo}</div>*/}
                                  {/*) : null}*/}
                                </div>
                              </FormGroup>
                            </Col>
                            <Col sm={1} md={1} lg={1} xs={1}>
                              <FormGroup className="mb-0" style={{display: 'table-caption'}}>
                                <div className="w-100">
                                  <Label className={classes.fieldLabel}></Label>
                                  <Button disabled={(this.state.errDate1 ? true : false) || (this.state.errDate2 ? true : false)} type="submit"
                                          className={classes.buttonSearch} color="primary"
                                          id="btnSubmit"
                                  >
                                    {/*Tìm*/}
                                    <FormattedMessage id="search" tagName="data" />
                                  </Button>
                                  <Button type="reset" hidden={true} id="resetFormMiniTestPrac" onClick={resetForm}>
                                  </Button>
                                </div>
                              </FormGroup>
                            </Col>
                          </Row>

                        </>

                      </FormGroup>
                    </Form>
                  )}
                </Formik>
              </CardBody>
            </Card>
          </Col>
          <Col sm="12">
            <Card>
              <CardBody>
                <div style={{position: 'relative'}}>
                  {this.props.isLoadingDosearch ? loadingComponent : null}
                  <Table
                    columns={columns}
                    dataSource={newData}
                    pagination={false}
                    onChange={this.onChangPageReceive}
                    locale={{
                      emptyText: 'Không tìm thấy dữ liệu'
                    }}
                  />
                  <div style={{marginLeft:'40%', marginTop:'5%'}}>
                    <PaginationIconsAndTextForMinitest
                      totalItem={count}
                      onChangPage={this.onChangPageReceive}
                      checkSearch={this.state.isSearch}
                      resetIsSearch={this.resetIsSearch}
                    />
                  </div>

                </div>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </>
    )
  }
}

const mapDispatchToProps = dispatch => {
  return {
    studentAccountAction: bindActionCreators(studentAccountAction, dispatch),
    historyPracticesAction: bindActionCreators(historyPracticesAction, dispatch),
    topicAction: bindActionCreators(topicAction, dispatch),
    practiceListenMultiActions: bindActionCreators(practiceListenMultiActions, dispatch),
    readingActions: bindActionCreators(readingActions, dispatch)
  }
}

const mapStateToProps = (state) => {
  return {
    studentAccountReducer: state.auth.login,
    listHistoryPractices: state.historyPracticseReducer.listHistoryPractices,
    total: state.historyPracticseReducer.total,
    values: state.historyPracticseReducer,
    listTypeForHistoryPractices: state.historyPracticseReducer.listTypeForHistoryPractices,
    listPartForHistoryPractices: state.historyPracticseReducer.listPartForHistoryPractices,
    listTopicForHistoryPractices: state.historyPracticseReducer.listTopicForHistoryPractices,
    listCategories: state.multiListeningReducer.listCategories,
    isLoadingDosearch: state.historyPracticseReducer.isLoadingDosearch

  }
}
const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
)

export default compose(withStyles(styles), withConnect)(HistoryPractices);
