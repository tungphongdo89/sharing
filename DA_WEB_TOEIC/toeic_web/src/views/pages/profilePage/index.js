import React from "react"
import {Nav, NavItem, NavLink, TabContent, TabPane} from "reactstrap"
import "../../../assets/css/style-modal-account.css"
import {bindActionCreators, compose} from "redux";
import {connect} from "react-redux";
import {withStyles} from "@material-ui/styles";
import styles from "./styles";
import studentAccountAction from "../../../redux/actions/studentAccount"
import ProfileContent from "./profile";
import HistoryTest from "./history";
import historyTestAction from "../../../redux/actions/history-test";
import HistoryMinitest from "./historyMinitest"
import testAction from "../../../redux/actions/do-test-management";

import HistoryPractices from "./historyPractices"
import historyPracticesAction from '../../../redux/actions/history-practices'
import {FormattedMessage} from "react-intl";

class ProfileManagement extends React.Component {

  state = {
    // active:  "1"
    active: this.props.active ? this.props.active : "1",
    lstOldActive: ["1"]
  }

  toggle = tab => {
    if (this.state.active !== tab) {
      this.setState({active: tab})
    }
  }


  componentWillReceiveProps(nextProps, nextContext) {
    if (nextProps.active === "1") {
      this.setState({
        active: nextProps.active
      })
    }
    if (nextProps.activeTab === "2") {
      this.setState({active: this.props.activeTab})
    }
  }

  backTab = () => {
    const length = this.state.lstOldActive.length;
    if (length >= 2) {
      this.setState({
        active: this.state.lstOldActive[length - 2],
        lstOldActive: this.state.lstOldActive.slice(0, -1)
      })
    }
  }

  render() {
    return (
      <div className="nav-vertical">
        <Nav tabs className="nav-left" style={{width: "16%", textAlign: "left", marginLeft: '1%'}}>
          <NavItem style={{marginBottom: '2%'}}>
            <NavLink
              className={({
                active: this.state.active === "1"
              })}
              onClick={() => {
                this.toggle("1")
                let arr = [...this.state.lstOldActive]
                arr.push("1")
                this.setState({
                  lstOldActive: arr
                })
                if (document.getElementById("resetFormMiniTestProfile")) {
                  document.getElementById("resetFormMiniTestProfile").click()
                }
                if (document.getElementById("btnHighLightCancel")) {
                  document.getElementById("btnHighLightCancel").click()
                }
              }}
              style={{fontSize: '1.1rem'}}
            >
              <FormattedMessage id="account.information" tagName="data"/>
            </NavLink>
          </NavItem>
          <NavItem style={{marginBottom: '2%'}}>
            <NavLink style={{fontSize: '1.1rem'}}
                     className={({
                       active: this.state.active === "2"
                     })}
                     onClick={() => {
                       this.toggle("2")
                       const {historyTestAction, studentAccountReducer} = this.props;
                       const {fetchHistoryTest} = historyTestAction;
                       let history = {};
                       history.userId = studentAccountReducer.userInfoLogin.userId;
                       history.page = 1;
                       history.pageSize = 10;
                       fetchHistoryTest(history);
                       let arr = [...this.state.lstOldActive]
                       arr.push("2")
                       document.getElementById("resetFormMiniTestFull").click()
                       this.setState({
                         lstOldActive: arr
                       })
                     }}
            >
              <FormattedMessage id="history.of.doing.full.test" tagName="data"/>
            </NavLink>
          </NavItem>
          <NavItem style={{marginBottom: '2%'}}>
            <NavLink style={{fontSize: '1.1rem'}}
                     className={({
                       active: this.state.active === "3"
                     })}
                     onClick={() => {
                       this.toggle("3")
                       const {testAction, studentAccountReducer} = this.props;
                       const {getListHistoryMinitest} = testAction;
                       let historyMinitest = {};
                       historyMinitest.userId = studentAccountReducer.userInfoLogin.userId;
                       historyMinitest.page = 1;
                       historyMinitest.pageSize = 10;
                       getListHistoryMinitest(historyMinitest);
                       let arr = [...this.state.lstOldActive]
                       arr.push("3")
                       document.getElementById("resetFormMiniTest").click()
                       this.setState({
                         lstOldActive: arr
                       })
                     }}
            >

              <FormattedMessage id="history.of.doing.mini.test" tagName="data"/>
            </NavLink>
          </NavItem>
          <NavItem style={{marginBottom: '2%'}}>
            <NavLink style={{fontSize: '1.1rem'}}
                     className={({
                       active: this.state.active === "4"
                     })}
                     onClick={() => {
                       this.toggle("4")
                       const {historyPracticesAction, studentAccountReducer} = this.props;
                       const {getListHistoryPractices} = historyPracticesAction;
                       let historyPractices = {};
                       historyPractices.userId = studentAccountReducer.userInfoLogin.userId;
                       historyPractices.page = 1;
                       historyPractices.pageSize = 10;
                       getListHistoryPractices(historyPractices);
                       let arr = [...this.state.lstOldActive]
                       arr.push("4")
                       document.getElementById("resetFormMiniTestPrac").click()
                       this.setState({
                         lstOldActive: arr
                       })
                     }}
            >
              <FormattedMessage id="history.of.doing.practices"/>
            </NavLink>
          </NavItem>
          <NavItem style={{marginBottom: '2%'}}>
            <NavLink style={{fontSize: '1.1rem'}}
                     className={({
                       active: this.state.active === "5"
                     })}
                     onClick={() => {
                       this.toggle("5")
                     }}
            >
              <FormattedMessage id="activity.statistics"/>
            </NavLink>
          </NavItem>
        </Nav>
        <TabContent activeTab={this.state.active}>
          <TabPane tabId="1">
            <ProfileContent backTab={this.backTab} isEdit={false}/>
          </TabPane>
          <TabPane tabId="2">
            <HistoryTest active={this.state.active} backTab={this.backTab}/>
          </TabPane>
          <TabPane tabId="3">
            <HistoryMinitest active={this.state.active} backTab={this.backTab}/>
          </TabPane>
          <TabPane tabId="4">
            <HistoryPractices active={this.state.active} backTab={this.backTab}/>
          </TabPane>
          <TabPane tabId="5">

          </TabPane>
        </TabContent>
      </div>
    )
  }
}

const mapDispatchToProps = dispatch => {
  return {
    studentAccountAction: bindActionCreators(studentAccountAction, dispatch),
    historyTestAction: bindActionCreators(historyTestAction, dispatch),
    testAction: bindActionCreators(testAction, dispatch),
    historyPracticesAction: bindActionCreators(historyPracticesAction, dispatch)
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    studentAccountReducer: state.auth.login,
    activeTab: state.historyTestReducer.active,
    active: state.historyPracticseReducer.activeTab
  }
}
const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
)

export default compose(withStyles(styles), withConnect)(ProfileManagement);
