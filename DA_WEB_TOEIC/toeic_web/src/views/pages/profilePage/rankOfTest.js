import React, {Component} from 'react';
import MUIDataTable from "mui-datatables";
import {TableCell, TableFooter, TableRow, withStyles} from "@material-ui/core";
import PaginationComponent from "./pagingtion";
import styled from "styled-components";
import {bindActionCreators, compose} from "redux";
import styles from "./styles";
import {connect} from "react-redux";
import Spinner from "reactstrap/es/Spinner";
import historyTestAction from "../../../redux/actions/history-test";
import {getUTCTimeSeconds} from "../../../commons/utils/DateUtils";
import PaginationIconsAndText from "../../apps/user/list/List";
import {FormattedMessage} from "react-intl";
import {showMessage} from "../../../commons/utils/convertDataForToast";

const TableCellStyle = styled(TableCell)`
  &&&{
    display: grid;
    justify-content: center;
   }
`

class RankOfTest extends Component {

  constructor(props) {
    super(props);
    this.state = {
      page: 1,
      pageSize: 10,
      isVisible: true,
      reload: false,
      collapse: true,
      collapseListStudent:true,
      status: "Opened",
      categoryType: "2",
      typeTopic: "",
      part: "",
      codeTopic: "",
      levelCode: "",
      statusTest: "2",
      createFrom: null,
      createTo: null,
      search: "Nhập thông tin tìm kiếm",
      rowsPerPageOptions: [10],
      testId: 0,
      typeCode: 0,
      isSearch: null
    }
  }

  resetIsSearch = () => {
    this.setState({
      isSearch: null
    })
  }
  componentDidMount() {
    this.props.historyTestAction.getRankOfTest({
      testId:this.props.testId
    })
  }
  cutStringName = (data)=>{
    let str= data;
    if(data !== null){
       str = data.slice(0,3) + '...';
    }
    return str;
  }

  render() {
    const data = this.props.lstRankOfTest ?this.props.lstRankOfTest :[]
    if (this.state.page === 0) {
      this.state.page = 1;
    }
    let newData = [];
    data.map((item, index) => {
      newData.push({sl: (this.state.pageSize) * (this.state.page - 1) + index + 1, ...item});
    });

    const columns = [
      {
        name: 'sl',
        label: showMessage("index"),
        options: {
          filter: false,
          sort: false,
          customHeadLabelRender: () => {
            return <div style={{fontWeight: 'bold'}}>
              {/*STT*/}
              <FormattedMessage id="index" tagName="data" />
            </div>;
          },
          customBodyRender: (index) => {
            return (<div>
              {index}
            </div>)
          }
        }
      },
      {
        name: 'userShowName',
        label: "userShowName",
        options: {
          filter: false,
          sort: false,
          customHeadLabelRender: () => {
            return <div style={{fontWeight: 'bold', textAlign:'center'}}>
              {/*Tài khoản*/}
              <FormattedMessage id="account" tagName="data" />
            </div>;
          },
          customBodyRender: (data) => {
            return (<div style={{textAlign:'center'}}>
              {this.cutStringName(data)}
            </div>)
          }
        }
      },
      {
        name: 'totalScore',
        label: "totalScore",
        options: {
          filter: false,
          sort: false,
          customHeadLabelRender: () => {
            return <div style={{fontWeight: 'bold',textAlign:'center'}}>
              {/*Tổng điểm*/}
              <FormattedMessage id="total.score" tagName="data" />
            </div>;
          },
          customBodyRender: (data) => {
            return (<div style={{textAlign:'center'}}>
              {data}
            </div>)
          }
        }
      },
      {
        name: 'listeningScore',
        label: "listeningScore",
        options: {
          filter: false,
          sort: false,
          customHeadLabelRender: () => {
            return <div style={{fontWeight: 'bold', textAlign:'center'}}>
              {/*Điểm phần nghe*/}
              <FormattedMessage id="score.of.listening.part" tagName="data" />
            </div>;
          },
          customBodyRender: (data) => {
            return (<div style={{textAlign:'center'}}>
              {data ? data : 0}
            </div>)
          }
        }
      },
      {
        name: 'readingScore',
        label: "readingScore",
        options: {
          filter: false,
          sort: false,
          customHeadLabelRender: () => {
            return <div style={{fontWeight: 'bold', textAlign:'center'}}>
              {/*Điểm phần đọc*/}
              <FormattedMessage id="score.of.reading.part" tagName="data" />
            </div>;
          },
          customBodyRender: (data) => {
            return (<div style={{textAlign:'center'}}>
              {data ? data : 0}
            </div>)
          }
        }
      },
      {
        name: 'latestHomeworkTime',
        label: "latestHomeworkTime",
        options: {
          filter: false,
          sort: false,
          customHeadLabelRender: () => {
            return <div style={{fontWeight: 'bold', textAlign:'center'}}>
              {/*Thời gian làm bài*/}
              <FormattedMessage id="time" tagName="data" />
            </div>;
          },
          customBodyRender: (data) => {
            return (<div style={{textAlign:'center'}}>
              {getUTCTimeSeconds(new Date(data))}
            </div>)
          }
        }
      },
      {
        name: '',
        label: "Xếp hạng",
        options: {
          filter: false,
          sort: false,
          customHeadLabelRender: () => {
            return <div style={{fontWeight: 'bold', textAlign:'center'}}>
              {/*Xếp hạng*/}
              <FormattedMessage id="rating" tagName="data" />
            </div>;
          },
          customBodyRender: (data, record) => {
            return (<div style={{textAlign:'center'}}>
              {record.rowIndex + 1}/{this.props.numberOfUser}
            </div>)
          }
        }
      }
    ];

    const options = {
      filter: false,
      filterType: 'checkbox',
      selectableRowsHideCheckboxes: false,
      responsive: "standard",
      selectableRows: "none",
      print: false,
      download: false,
      search: false,
      viewColumns: false,
      serverSide: true,
      textLabels: {
        body: {
          noMatch: this.props.isLoading ?
            <p/> :
            // 'Không tìm thấy dữ liệu',
            showMessage("data.not.found")
        },
      },
      customFooter: () => {
        return (
          <TableFooter>
            <TableRow>
              <TableCell colSpan={6}>
                {/*<PaginationIconsAndText count={count} getCurrentPage ={ (currentPage)=>{this.getCurrentPage(currentPage)}}/>*/}
              </TableCell>
            </TableRow>
          </TableFooter>
        );
      }
    };


    return (
      <div>
        {
          this.props.loadingRank == true ?
            <div style={{
              position: 'absolute',
              zIndex: 110,
              top: 0,
              left: 0,
              width: '100%',
              height: '100%',
              display: 'flex',
              justifyContent: 'center',
              alignItems: 'center',
              background: 'rgba(255,255,255,0.8)'
            }}>
              <Spinner color="primary" className="reload-spinner"/>
            </div> : null
        }
        <MUIDataTable
          columns={columns}
          data={newData}
          options={options}
        />
      </div>
    );
  }
}

const mapStateToProps = (state) => {

  return {
    lstRankOfTest: state.historyTestReducer.lstRankOfTest,
    studentAccountReducer: state.auth.login,
    loadingRank: state.historyTestReducer.loadingRank,
    values: state.historyTestReducer,
    numberOfUser: state.historyTestReducer.numberOfUser
  }

}
const mapDispatchToProps = (dispatch) => {
  return {
    historyTestAction: bindActionCreators(historyTestAction, dispatch),
  }
}

export default compose(withStyles(styles()), connect(mapStateToProps, mapDispatchToProps))(RankOfTest)
