const styles = theme => ({
  labelForFile: {
    fontSize: "10px",
    fontWeight: "700",
    padding: "5px",
    color: "white",
    backgroundColor: "grey",
    display: "inline-block",
    cursor: "pointer",
    border: "1px solid grey",
    borderRadius: "60%",
    width: "26.5%",
    textAlign: "center",
    position: "absolute",
    bottom: "12%",
    left: "36.5%",
  },
  error: {
    color: 'red',
    fontSize: 12
  },
  buttonAdd:{
    paddingTop: '1.2em ! important',
    marginTop: '1em ! important',
    paddingLeft:'2em ! important',
  },
  buttonSearch:{
    paddingTop: '1.2em ! important',
    marginTop: '1em ! important',

  },
  search:{
    paddingLeft:'4em'
  }
});

export default styles;
