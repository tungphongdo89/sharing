import React from "react"
import {
  Button,
  Card,
  CardBody,
  CardHeader,
  CardTitle,
  Col,
  FormGroup,
  Label,
  Modal,
  ModalHeader,
  Row,
  Spinner,
} from "reactstrap"
import {ArrowLeft, Eye, Search} from "react-feather"
import "../../../assets/css/style-modal-account.css"
import {bindActionCreators, compose} from "redux";
import {connect} from "react-redux";
import {withStyles} from "@material-ui/styles";
import styles from "./styles";
import studentAccountAction from "../../../redux/actions/studentAccount"
import {Field, Form, Formik} from "formik";
import {Table} from "antd";
import PaginationComponent from './pagingtionHistory';
import historyTestAction from "../../../redux/actions/history-test"
import 'antd/dist/antd.css';
import {getUTCTimeSeconds, getUTCTime} from "../../../commons/utils/DateUtils";
import DetailOfTest from "./detailOfTest";
import RankOfTest from "./rankOfTest";
import {IconButton} from "@material-ui/core";
import ClearIcon from '@material-ui/icons/Clear';
import Tooltip from "react-bootstrap/Tooltip";
import OverlayTrigger from "react-bootstrap/OverlayTrigger";
import {history} from "../../../history";
import testAction from "../../../redux/actions/do-test-management";
import * as Yup from "yup";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import {FormattedMessage} from "react-intl";
import {showMessage} from "../../../commons/utils/convertDataForToast";

const formSchema = Yup.object().shape({
  createDateFrom: Yup.string()
    .matches(/^((2000|2400|2800|(19|2[0-9](0[48]|[2468][048]|[13579][26])))-02-29)$|^(((19|2[0-9])[0-9]{2})-02-(0[1-9]|1[0-9]|2[0-8]))$|^(((19|2[0-9])[0-9]{2})-(0[13578]|10|12)-(0[1-9]|[12][0-9]|3[01]))$|^(((19|2[0-9])[0-9]{2})-(0[469]|11)-(0[1-9]|[12][0-9]|30))$/, "Ngày tháng không đúng định dạng"),
  createDateTo: Yup.string()
    .matches(/^((2000|2400|2800|(19|2[0-9](0[48]|[2468][048]|[13579][26])))-02-29)$|^(((19|2[0-9])[0-9]{2})-02-(0[1-9]|1[0-9]|2[0-8]))$|^(((19|2[0-9])[0-9]{2})-(0[13578]|10|12)-(0[1-9]|[12][0-9]|3[01]))$|^(((19|2[0-9])[0-9]{2})-(0[469]|11)-(0[1-9]|[12][0-9]|30))$/, "Ngày tháng không đúng định dạng")

});
let errDate1 = "";
let errDate2 = "";

function validateDateFrom(date, value) {
  
  if (value.createDateFrom === "") {
    if (document.getElementById("createDateFromHistory") !== null && document.getElementById("createDateFromHistory").validationMessage !== "") {
      errDate1 = showMessage("date.not.correct.format");
    } else {
      errDate1 = ""
    }
  } else {
    if (!/^((2000|2400|2800|(19|2[0-9](0[48]|[2468][048]|[13579][26])))-02-29)$|^(((19|2[0-9])[0-9]{2})-02-(0[1-9]|1[0-9]|2[0-8]))$|^(((19|2[0-9])[0-9]{2})-(0[13578]|10|12)-(0[1-9]|[12][0-9]|3[01]))$|^(((19|2[0-9])[0-9]{2})-(0[469]|11)-(0[1-9]|[12][0-9]|30))$/i.test(value.createDateFrom)) {
      errDate1 = showMessage("date.not.correct.format");
    } else if (value.createDateFrom && value.createDateTo) {
      if (Date.parse(value.createDateFrom) > Date.parse(value.createDateTo)) {
        errDate1 = showMessage("compare.time");
      } else {
        errDate1 = ""
      }
    } else {
      errDate1 = ""
    }
  }
}

function validateDateTo(value) {
  if (value.createDateTo === "") {
    if (document.getElementById("createDateToHistory") !== null && document.getElementById("createDateToHistory").validationMessage !== "") {
      errDate2 = showMessage("date.not.correct.format");
    } else {
      errDate2 = ""
    }
  } else {
    if (!/^((2000|2400|2800|(19|2[0-9](0[48]|[2468][048]|[13579][26])))-02-29)$|^(((19|2[0-9])[0-9]{2})-02-(0[1-9]|1[0-9]|2[0-8]))$|^(((19|2[0-9])[0-9]{2})-(0[13578]|10|12)-(0[1-9]|[12][0-9]|3[01]))$|^(((19|2[0-9])[0-9]{2})-(0[469]|11)-(0[1-9]|[12][0-9]|30))$/i.test(value.createDateTo)) {
      errDate2 = showMessage("date.not.correct.format");
    } else if (value.createDateFrom && value.createDateTo) {
      if (Date.parse(value.createDateFrom) > Date.parse(value.createDateTo)) {
        errDate1 = showMessage("compare.time");
        ;
        errDate2 = ""
      } else {
        errDate1 = ""
      }
    } else {
      errDate2 = ""
    }
  }
}

class HistoryTest extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      activeTab: "1",
      isOpen: false,
      createFrom: '',
      createTo: '',
      searchValue: '',
      page: 1,
      pageSize: 10,
      count: 1,
      modalDetailTest: false,
      testId: 0,
      testName: '',
      modalRankTest: false,
      rankOfUser: '',
      isSearch: null,
      message: '',
      errDate1: '',
      errDate2: ''
    }
    this.inputFocus = React.createRef();

  }

  validateDateFrom =(value)=> {
    if (value.createDateFrom === "") {
      if (document.getElementById("createDateFromHp") !== null && document.getElementById("createDateFromHp").validationMessage !== "") {
        // errDate1 = showMessage("date.not.correct.format");
        this.setState({
          errDate1: showMessage("date.not.correct.format")
        })
      } else {
        // errDate1 = ""
        this.setState({
          errDate1: ''
        })
      }
    } else {
      if (!/^((2000|2400|2800|(19|2[0-9](0[48]|[2468][048]|[13579][26])))-02-29)$|^(((19|2[0-9])[0-9]{2})-02-(0[1-9]|1[0-9]|2[0-8]))$|^(((19|2[0-9])[0-9]{2})-(0[13578]|10|12)-(0[1-9]|[12][0-9]|3[01]))$|^(((19|2[0-9])[0-9]{2})-(0[469]|11)-(0[1-9]|[12][0-9]|30))$/i.test(value.createDateFrom)) {
        // errDate1 = showMessage("date.not.correct.format");
        this.setState({
          errDate1: showMessage("date.not.correct.format")
        })
      } else if (value.createDateFrom && value.createDateTo) {
        if (Date.parse(value.createDateFrom) > Date.parse(value.createDateTo)) {
          // errDate1 = showMessage("compare.time");
          this.setState({
            errDate1: showMessage("compare.time")
          })
        } else {
          // errDate1 = ""
          this.setState({
            errDate1: ''
          })
        }
      } else {
        // errDate1 = ""
        this.setState({
          errDate1: ''
        })
      }
    }
  }

  validateDateTo =(value)=> {
    if (value.createDateTo === "") {
      if (document.getElementById("createDateToHp") !== null && document.getElementById("createDateToHp").validationMessage !== "") {
        // errDate2 = showMessage("date.not.correct.format");
        this.setState({
          errDate2: showMessage("date.not.correct.format")
        })
      } else {
        // errDate2 = ""
        this.setState({
          errDate2: ''
        })
      }
    } else {
      if (!/^((2000|2400|2800|(19|2[0-9](0[48]|[2468][048]|[13579][26])))-02-29)$|^(((19|2[0-9])[0-9]{2})-02-(0[1-9]|1[0-9]|2[0-8]))$|^(((19|2[0-9])[0-9]{2})-(0[13578]|10|12)-(0[1-9]|[12][0-9]|3[01]))$|^(((19|2[0-9])[0-9]{2})-(0[469]|11)-(0[1-9]|[12][0-9]|30))$/i.test(value.createDateTo)) {
        // errDate2 = showMessage("date.not.correct.format");
        this.setState({
          errDate2: showMessage("date.not.correct.format")
        })
      } else if (value.createDateFrom && value.createDateTo) {
        if (Date.parse(value.createDateFrom) > Date.parse(value.createDateTo)) {
          // errDate1 = showMessage("compare.time");
          // errDate2 = ""
          this.setState({
            errDate1: showMessage("compare.time"),
            errDate2: ''
          })
        } else {
          // errDate1 = ""
          this.setState({
            errDate1: ''
          })
        }
      } else {
        // errDate2 = ""
        this.setState({
          errDate2: ''
        })
      }
    }
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if(prevProps.active !== this.props.active){
      this.setState({
        activeTab: "1",
        isOpen: false,
        createFrom: '',
        createTo: '',
        searchValue: '',
        page: 1,
        pageSize: 10,
        count: 1,
        modalDetailTest: false,
        testId: 0,
        testName: '',
        modalRankTest: false,
        rankOfUser: '',
        isSearch: null,
        message: '',
        errDate1: '',
        errDate2: ''
      })
    }
  }


  toggle = tab => {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab,
        isOpen: !this.state.isOpen
      })
    }
  }

  resetIsSearch = () => {
    this.setState({
      isSearch: null
    })
  }

  onChangPageReceive = (pageNumber,values) => {
    this.setState({
      page: pageNumber
    })
    if (pageNumber === 1) {
      this.setState({
        isSearch: 1,
      })
    }
    const searchPropertiesBE = {};
    searchPropertiesBE.keySearch = this.state.searchValue.toString().trim()
    searchPropertiesBE.createFrom = values.createDateFrom
    searchPropertiesBE.createTo = values.createDateTo
    searchPropertiesBE.pageSize = this.state.pageSize
    searchPropertiesBE.page = pageNumber
    searchPropertiesBE.userId = this.props.studentAccountReducer.userInfoLogin.userId;
    this.props.historyTestAction.fetchHistoryTest(searchPropertiesBE)
  }

  viewDetail = (record) => {
    this.toggleModalDetailTest(record.testId, record.testName);
  }
  viewRank = (record) => {
    this.toggleModalRankTest(record.testId, record.testName, record.rankOfUser);
  }

  toggleModalDetailTest = (testId, testName) => {
    this.setState(prevState => ({
      testId: testId,
      testName: testName,
      modalDetailTest: !prevState.modalDetailTest
    }))
  }

  toggleModalRankTest = (testId, testName, rankOfUser) => {
    this.setState(prevState => ({
      testId: testId,
      testName: testName,
      rankOfUser: rankOfUser,
      modalRankTest: !prevState.modalRankTest
    }))
  }

  handleDateChangeRaw = (e) => {
    e.preventDefault();
  }

  render() {
    const {classes, lstHistoryTest, backTab} = this.props;
    const count = this.props.values.total;

    if (this.props.active === '2') {
      if (this.inputFocus.current) {
        this.inputFocus.current.focus()
      }
    }
    const columns = [
      {
        dataIndex: 'sl',
        title: (
          <div style={{fontWeight: '700'}}>
            {/*STT*/}
            <FormattedMessage id="index" />
          </div>
        )
        ,
        className: 'drag-visible',
        width: window.screen.width * 0.05,
        render: data => {
          return (
            <>
              <Row>
                <Col xs={6}>
                  {data}
                </Col>
              </Row>
            </>
          )
        }
      },
      {
        dataIndex: "testName",
        title:  (
          <div style={{fontWeight: '700'}}>
            {/*Tên bài thi*/}
            <FormattedMessage id="test.name" tagName="data" />
          </div>
        ),
        className: 'drag-visible',
		    width: window.screen.width * 0.15,
        render: data => (<div style={{
          whiteSpace: 'nowrap',
          width: '150px',
          overflow: 'hidden',
          textOverflow: 'ellipsis'
        }} title={data}>{data}</div>)
      },
      {
        dataIndex: "countCategory",
        title:  (
          <div style={{fontWeight: '700'}}>
            {/*Số lượt làm bài*/}
            <FormattedMessage id="number.doing.test" tagName="data" />
          </div>
        ),
		width: window.screen.width * 0.12,
        className: 'drag-visible',
        render: (data, record) => (
          <button style={{backgroundColor: 'rgb(0 0 0 / 0%)', border: 'none'}}
                  onClick={() => this.viewDetail(record)} id="countDoTest">{data}</button>
        )
      },
      {
        dataIndex: "totalScore",
        title:  (
          <div style={{fontWeight: '700'}}>
            {/*Tổng điểm*/}
            <FormattedMessage id="total.score" tagName="data" />
          </div>
        ),
        className: 'drag-visible',
        width: window.screen.width * 0.09,
        render: data => (<div >{data}</div>)
      },
      {
        dataIndex: "latestHomeworkTime",
        title:  (
          <div style={{fontWeight: '700'}}>
            {/*Thời gian làm bài gần nhất*/}
            <FormattedMessage id="the.nearest.time.of.doing.test" tagName="data" />
          </div>
        ),
        className: 'drag-visible',
        width: window.screen.width * 0.15,
        render: data => (<div>{getUTCTime(new Date(data))}</div>),
      },
      {
        dataIndex: "rankOfUser",
        title:  (
          <div style={{fontWeight: '700'}}>
            {/*Xếp hạng*/}
            <FormattedMessage id="rating" tagName="data" />
          </div>
        ),
        className: 'drag-visible',
        width: window.screen.width * 0.07,
        render: (data, record) => (
          <button style={{ backgroundColor: 'rgb(0 0 0 / 0%)', border: 'none'}}
                  onClick={() => this.viewRank(record)} id="rank">{data}</button>)
      },
      {
        dataIndex: "",
        title:  (
          <div style={{fontWeight: '700', textAlign: 'center'}}>
            {/*Xem chi tiết*/}
            <FormattedMessage id="Detail" tagName="data" />
          </div>
        ),
        className: 'drag-visible',
        // width: window.screen.width * 0.1,
        render: (data, record) => (
          <div style={{textAlign: 'center'}}>
            <OverlayTrigger textAlign="center" placement="top" overlay={props => (<Tooltip {...props}>
              {/*Xem chi tiết*/}
              <FormattedMessage id="Detail" tagName="data" />
            </Tooltip>)}>
              <button id="btnHighLight" style={{border: 'none', backgroundColor: 'rgb(0 0 0 / 0%)'}}
                      onClick={() => {
                        const {historyTestAction, studentAccountReducer, testAction} = this.props;
                        testAction.cleanTest();
                        historyTestAction.getDetailHistoryFullTest(record);
                        let historyFT = {};
                        historyFT.id = record.id;
                        historyFT.createDate = record.latestHomeworkTime
                        historyFT.testId = record.testId
                        historyFT.userId = studentAccountReducer.userInfoLogin.userId
                        historyTestAction.getHistoryFullTest(historyFT);
                        history.push("/pages/fulltest");

                      }}
              >
                <Eye size={15} className=" mr-1 fonticon-wrap" id="btnHighLight"
                     style={{cursor: "pointer", marginLeft: '25%'}}
                />
              </button>
            </OverlayTrigger>
          </div>
          )
      }
    ];
    let newData = [];

    if (lstHistoryTest && lstHistoryTest.data) {
      lstHistoryTest.data.map((item, index) => {
        newData.push({sl: (this.state.pageSize) * (this.state.page - 1) + index + 1, ...item});
      });
    }

    const loadingComponent = (
      <div style={{
        position: 'absolute',
        zIndex: 110,
        top: 0,
        left: 0,
        width: '100%',
        height: '100%',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        background: 'rgba(255,255,255,0.8)'
      }}>
        <Spinner color="primary" className="reload-spinner"/>
      </div>
    );
    return (
      <>
        <style>{`
          #btnHighLight:focus-within {
            border: none !important;
            outline: 1px solid gray !important;
          }
          #postLink:focus-within {
            text-shadow: 1px 1px #1890ff;
          }
          #countDoTest:focus-within {
             border: none !important;
             outline: 1px solid gray !important;
          }
          #rank:focus-within {
             border: none !important;
             outline: 1px solid gray !important;
          }
          #btnSubmit {
            margin-top: 0% !important;
            padding: 0.85rem 2rem !important;
          }

          #btnSubmit:focus-within {
            border: none !important;
            outline: 1px solid gray !important;
          }
        `}

        </style>
        <Row>
          <Col sm="12">
            <Card>
              <CardHeader>
                <CardTitle><h1>
                  {/*Lịch sử làm bài FullTest*/}
                  <FormattedMessage id="history.of.doing.full.test" tagName="data" />
                </h1></CardTitle>

                <Button color="primary" className="btn-icon right" color="primary" left onClick={backTab} title={showMessage("back")}>
                  <ArrowLeft/>
                </Button>
              </CardHeader>
              <CardBody>
                <Formik
                  initialValues={{
                    createDateFrom: '',
                    createDateTo: '',
                  }}
                  onSubmit={(values, actions) => {
                    this.setState({
                      loading: true,
                    })
                    this.onChangPageReceive(1, values)
                  }}
                  // validationSchema={formSchema}
                >
                  {({errors, touched, values,resetForm}) => (
                    <Form>
                      <FormGroup className="form-group d-flex">
                        <>
                          <Modal
                            isOpen={this.state.modalDetailTest}
                            toggle={this.toggleModalDetailTest}
                            className="modal-dialog-centered modal-lg"
                            style={{maxWidth: '1000px', width: '90%'}}
                          >
                            <Row className="flex flex-space-between flex-middle  bg-primary "
                                 style={{width: '100%', marginLeft: '0.2px'}}>
                              <Col xs={11}>
                                <ModalHeader style={{background: '#7367f0', textAlign: 'left', float: 'left'}}>
                                  <h2 style={{color: 'rgba(0, 0, 0, 0.87)'}}><strong>
                                    {/*Chi tiết*/}
                                    <FormattedMessage id="detail.for.history" tagName="data" />
                                    : <span
                                    style={{color: 'white'}}>{this.state.testName}</span></strong></h2>
                                </ModalHeader>
                              </Col>
                              <Col xs={1} style={{paddingLeft: '38px', paddingRight: '0px'}}>
                                <IconButton onClick={this.toggleModalDetailTest}>
                                  <ClearIcon/>
                                </IconButton>
                              </Col>
                            </Row>
                            <DetailOfTest testId={this.state.testId}/>
                          </Modal>
                          <Modal
                            isOpen={this.state.modalRankTest}
                            toggle={this.toggleModalRankTest}
                            className="modal-dialog-centered modal-lg"
                            style={{maxWidth: '1000px', width: '90%'}}
                          >
                            <Row className="flex flex-space-between flex-middle  bg-primary "
                                 style={{width: '100%', marginLeft: '0.2px'}}>
                              <Col xs={11}>
                                <ModalHeader style={{background: '#7367f0', textAlign: 'left', float: 'left'}}>
                                  <h2 style={{color: 'rgba(0, 0, 0, 0.87)'}}><strong>
                                    {/*Bảng xếp hạng điểm thi*/}
                                    <FormattedMessage id="Table.of.rating" tagName="data" />
                                    : <span
                                    style={{color: 'white'}}>{this.state.testName}</span></strong></h2>
                                </ModalHeader>
                              </Col>
                              <Col xs={1} style={{paddingLeft: '38px', paddingRight: '0px'}}>
                                <IconButton onClick={this.toggleModalRankTest}>
                                  <ClearIcon/>
                                </IconButton>
                              </Col>
                            </Row>
                            <>
                              <h5 style={{color: 'red', margin: '1.5%'}}>
                                {/*Xếp hạng của bạn là*/}
                                <FormattedMessage id="your.rating.is" tagName="data" />
                                : {this.state.rankOfUser} </h5>
                              <RankOfTest testId={this.state.testId}/>
                            </>
                          </Modal>
                          <Col sm={3} md={3} lg={3} xs={3}>
                            <span>&nbsp;</span>
                            <FormGroup className="position-relative has-icon-left">
                              <div className="w-100">
                                <input
                                  autoFocus={true}
                                  type="text"
                                  name="searchValue"
                                  // style={{height: '45px'}}
                                  placeholder= {showMessage("enter.test.name.that.need.to.search")}
                                  className="form-control"
                                  ref={this.inputFocus}
                                  onChange={(e) => {
                                    this.setState({
                                      searchValue: e.target.value
                                    })
                                  }}
                                />
                                <div className="form-control-position px-1">
                                  <Search size={15}/>
                                </div>
                              </div>
                            </FormGroup>
                          </Col>
                          <Col sm={3} md={3} lg={3} xs={3}>
                            <FormGroup className="mb-0">
                              <div className="w-100">
                                <Label className={classes.fieldLabel} for="createDateFrom">
                                  {/*Thời điểm từ*/}
                                  <FormattedMessage id="time.from" tagName="data" />
                                </Label>
                                <Field
                                id="createDateFromHistory"
                                type="date"
                                name="createDateFrom"
                                // style={{height: '45px'}}
                                className={`form-control ${errors.createDateFrom &&
                                touched.createDateFrom &&
                                "is-invalid"}`}
                                // validate={() => this.validateDateFrom(values)}
                                validate={validateDateFrom(this, values)}

                              />
                                {errDate1 ? (
                                  <div className={classes.error}>{errDate1}</div>
                                ) : null}

                              </div>
                            </FormGroup>
                          </Col>
                          <Col sm={3} md={3} lg={3} xs={3}>
                            <FormGroup className="mb-0">
                              <div className="w-100">
                                <Label className={classes.fieldLabel} for="createDateTo">
                                  {/*Thời điểm đến*/}
                                  <FormattedMessage id="time.to" tagName="data" />
                                </Label>
                                <Field
                                  type="date"
                                  id="createDateToHistory"
                                  name="createDateTo"
                                  // style={{height: '45px'}}
                                  minDate='1900-01-01'
                                  className={`form-control ${errors.createDateTo &&
                                  touched.createDateTo &&
                                  "is-invalid"}`}
                                  validate={validateDateTo(values)}

                                />
                                {errDate2 ? (
                                  <div className={classes.error}>{errDate2}</div>
                                ) : null}

                              </div>
                            </FormGroup>
                          </Col>
                          <Col sm={1} md={1} lg={1} xs={1}>
                            <FormGroup className="mb-0">
                              <div className="w-100" style={{display: 'table-caption'}}>
                                <Label className={classes.fieldLabel}></Label>
                                <Button disabled={(errDate1 ? true : false) || (errDate2 ? true : false)} type="submit" className={classes.buttonSearch} color="primary"
                                        id="btnSubmit"
                                >
                                  {/*Tìm*/}
                                  <FormattedMessage id="search" tagName="data" />
                                </Button>
                                <Button type="reset" hidden={true} id="resetFormMiniTestFull" onClick={resetForm}>
                                </Button>
                              </div>
                            </FormGroup>
                          </Col>
                        </>
                      </FormGroup>
                    </Form>
                  )}
                </Formik>
              </CardBody>
            </Card>
          </Col>
          <Col sm="12">
            <Card>
              <CardBody>
                <div style={{position: 'relative'}}>
                  {this.props.isLoading && loadingComponent}
                  <Table
                    columns={columns}
                    dataSource={newData}
                    pagination={false}
                    onChange={this.onChangPageReceive}
                    // scroll={{ x: 'fit-content' }}
                    locale={{
                      emptyText: showMessage("data.not.found")
                    }}
                  />
                  <div style={{marginLeft: '40%', marginTop: '5%'}}>
                    <PaginationComponent
                      totalItem={count}
                      onChangPage={this.onChangPageReceive}
                      checkSearch={this.state.isSearch}
                      resetIsSearch={this.resetIsSearch}
                    />
                  </div>

                </div>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </>
    )
  }
}

const mapDispatchToProps = dispatch => {
  return {
    studentAccountAction: bindActionCreators(studentAccountAction, dispatch),
    historyTestAction: bindActionCreators(historyTestAction, dispatch),
    testAction: bindActionCreators(testAction, dispatch)
  }
}

const mapStateToProps = (state) => {
  return {
    studentAccountReducer: state.auth.login,
    lstHistoryTest: state.historyTestReducer.lstHistoryTest,
    isLoading: state.historyTestReducer.isLoading,
    values: state.historyTestReducer
  }
}
const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
)

export default compose(withStyles(styles), withConnect)(HistoryTest);
