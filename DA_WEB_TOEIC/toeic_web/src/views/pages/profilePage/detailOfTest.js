import React, {Component} from 'react';
import MUIDataTable from "mui-datatables";
import {TableCell, TableFooter, TableRow, withStyles} from "@material-ui/core";
import PaginationComponent from "./pagingtion";
import styled from "styled-components";
import {bindActionCreators, compose} from "redux";
import styles from "./styles";
import {connect} from "react-redux";
import Spinner from "reactstrap/es/Spinner";
import historyTestAction from "../../../redux/actions/history-test";
import {getUTCTimeSeconds} from "../../../commons/utils/DateUtils";
import {FormattedMessage} from "react-intl";
import {showMessage} from "../../../commons/utils/convertDataForToast";

const TableCellStyle = styled(TableCell)`
  &&&{
    display: grid;
    justify-content: center;
   }
`

class DetailOfTest extends Component {

  constructor(props) {
    super(props);
    this.state = {
      page: 1,
      count: 1,
      searchValue: "",
      currentPage: 0,
      pageSize: 10,
      isVisible: true,
      reload: false,
      collapse: true,
      collapseListStudent:true,
      status: "Opened",
      categoryType: "2",
      typeTopic: "",
      part: "",
      codeTopic: "",
      levelCode: "",
      statusTest: "2",
      createFrom: null,
      createTo: null,
      search: "Nhập thông tin tìm kiếm",
      rowsPerPageOptions: [10],
      testId: 0,
      typeCode: 0,
      isSearch: null
    }
  }

  resetIsSearch = () => {
    this.setState({
      isSearch: null
    })
  }
  componentDidMount() {
    this.props.historyTestAction.getDetailOfTest({
      testId:this.props.testId,
      page:1,
      pageSize:this.state.pageSize,
      userId:this.props.studentAccountReducer.userInfoLogin.userId
    })
  }

  onChangPageReceive = (pageNumber) => {
    const {studentAccountReducer, historyTestAction} = this.props;
    this.setState({
      page: pageNumber
    })
    if (pageNumber === 1) {
      this.setState({
        isSearch: 1,
      })
    }
    const searchPropertiesBE = {};
    searchPropertiesBE.testId = this.props.testId;
    searchPropertiesBE.userId = studentAccountReducer.userInfoLogin.userId;
    searchPropertiesBE.pageSize = this.state.pageSize
    searchPropertiesBE.page = pageNumber
    historyTestAction.getDetailOfTest(searchPropertiesBE);
  }

  render() {

    const columns = [
      {
        name: 'sl',
        label: showMessage("index"),
        options: {
          filter: false,
          sort: false,
          customHeadLabelRender: () => {
            return <div style={{fontWeight: 'bold'}}>
              {/*STT*/}
              <FormattedMessage id="index" tagName="data" />
            </div>;
          },
          customBodyRender: (index) => {
            return (<div>
              {index}
            </div>)
          }
        }
      },
      {
        name: 'createDate',
        label: "createDate",
        options: {
          filter: false,
          sort: false,
          customHeadLabelRender: () => {
            return <div style={{fontWeight: 'bold'}}>
              {/*Thời điểm làm bài*/}
              <FormattedMessage id="time" tagName="data" />
            </div>;
          },
          customBodyRender: (data) => {
            return (<div style={{width:'120px'}}>
              {getUTCTimeSeconds(new Date(data))}
            </div>)
          }
        }
      },
      {
        name: 'totalScore',
        label: "totalScore",
        options: {
          filter: false,
          sort: false,
          customHeadLabelRender: () => {
            return <div style={{fontWeight: 'bold'}}>
              {/*Tổng điểm*/}
              <FormattedMessage id="total.score" tagName="data" />
            </div>;
          },
          customBodyRender: (data) => {
            return (<div style={{color:'red'}}>
              {data}
            </div>)
          }
        }
      },
      {
        name: 'listeningScore',
        label: "listeningScore",
        options: {
          filter: false,
          sort: false,
          customHeadLabelRender: () => {
            return <div style={{fontWeight: 'bold'}}>
              {/*Điểm phần nghe*/}
              <FormattedMessage id="score.of.listening.part" tagName="data" />
            </div>;
          },
          customBodyRender: (data) => {
            return (<div>
              {data ? data : 0}
            </div>)
          }
        }
      },
      {
        name: 'readingScore',
        label: "readingScore",
        options: {
          filter: false,
          sort: false,
          customHeadLabelRender: () => {
            return <div style={{fontWeight: 'bold'}}>
              {/*Điểm phần đọc*/}
              <FormattedMessage id="score.of.reading.part" tagName="data" />
            </div>;
          },
          customBodyRender: (data) => {
            return (<div>
              {data ? data : 0}
            </div>)
          }
        }
      },
      {
        name: 'part1',
        label: "part1",
        options: {
          filter: false,
          sort: false,
          customHeadLabelRender: () => {
            return <div style={{fontWeight: 'bold'}}>
              {/*Số câu đúng Part 1*/}
              <FormattedMessage id="total.of.correct.answer.of.part1" tagName="data" />
            </div>;
          },
          customBodyRender: (data) => {
            return (<div>
              {data}
            </div>)
          }
        }
      },
      {
        name: 'part2',
        label: "part2",
        options: {
          filter: false,
          sort: false,
          customHeadLabelRender: () => {
            return <div style={{fontWeight: 'bold'}}>
              {/*Số câu đúng Part 2*/}
              <FormattedMessage id="total.of.correct.answer.of.part2" tagName="data" />
            </div>;
          },
          customBodyRender: (data) => {
            return (<div>
              {data}
            </div>)
          }
        }
      },
      {
        name: 'part3',
        label: "part3",
        options: {
          filter: false,
          sort: false,
          customHeadLabelRender: () => {
            return <div style={{fontWeight: 'bold'}}>
              {/*Số câu đúng Part 3*/}
              <FormattedMessage id="total.of.correct.answer.of.part3" tagName="data" />
            </div>;
          },
          customBodyRender: (data) => {
            return (<div>
              {data}
            </div>)
          }
        }
      },
      {
        name: 'part4',
        label: "part4",
        options: {
          filter: true,
          sort: false,
          customHeadLabelRender: () => {
            return <div style={{fontWeight: 'bold'}}>
              {/*Số câu đúng Part 4*/}
              <FormattedMessage id="total.of.correct.answer.of.part4" tagName="data" />
            </div>;
          },
          customBodyRender: (data) => {
            return (<div>
              {data}
            </div>)
          }
        }
      },
      {
        name: 'part5',
        label: "part5",
        options: {
          filter: false,
          sort: false,
          customHeadLabelRender: () => {
            return <div style={{fontWeight: 'bold'}}>
              {/*Số câu đúng Part 5*/}
              <FormattedMessage id="total.of.correct.answer.of.part5" tagName="data" />
            </div>;
          },
          customBodyRender: (data) => {
            return (<div>
              {data}
            </div>)
          }
        }
      },
      {
        name: 'part6',
        label: "part6",
        options: {
          filter: false,
          sort: false,
          customHeadLabelRender: () => {
            return <div style={{fontWeight: 'bold'}}>
              {/*Số câu đúng Part 6*/}
              <FormattedMessage id="total.of.correct.answer.of.part6" tagName="data" />
            </div>;
          },
          customBodyRender: (data) => {
            return (<div>
              {data}
            </div>)
          }
        }
      },
      {
        name: 'part7',
        label: "part7",
        options: {
          filter: false,
          sort: false,
          customHeadLabelRender: () => {
            return <div style={{fontWeight: 'bold'}}>
              {/*Số câu đúng Part 7*/}
              <FormattedMessage id="total.of.correct.answer.of.part7" tagName="data" />
            </div>;
          },
          customBodyRender: (data) => {
            return (<div>
              {data}
            </div>)
          }
        }
      }
    ];
    const data = this.props.lstDetailOfTest && this.props.lstDetailOfTest.data ?this.props.lstDetailOfTest.data :[]
    if (this.state.page === 0) {
      this.state.page = 1;
    }
    let newData = [];
    data.map((item, index) => {
      newData.push({sl: (this.state.pageSize) * (this.state.page - 1) + index + 1, ...item});
    });

    const options = {
      filter: false,
      filterType: 'checkbox',
      selectableRowsHideCheckboxes: true,
      responsive: "standard",
      selectableRows: "none",
      print: false,
      download: false,
      search: false,
      viewColumns: false,
      serverSide: true,
      textLabels: {
        body: {
          noMatch: this.props.isLoading ?
            <p/> :
            'Không tìm thấy dữ liệu',
        },
      },
      customFooter: () => {
        return (
          <TableFooter>
            <TableRow>
              <TableCellStyle colSpan={6}>

              </TableCellStyle>
            </TableRow>
          </TableFooter>
        );
      }
    };


    return (
      <div>
        {
          this.props.loading == true ?
            <div style={{
              position: 'absolute',
              zIndex: 110,
              top: 0,
              left: 0,
              width: '100%',
              height: '100%',
              display: 'flex',
              justifyContent: 'center',
              alignItems: 'center',
              background: 'rgba(255,255,255,0.8)'
            }}>
              <Spinner color="primary" className="reload-spinner"/>
            </div> : null
        }
        <MUIDataTable
          columns={columns}
          data={newData}
          options={options}
        />
      </div>
    );
  }
}

const mapStateToProps = (state) => {

  return {
    lstDetailOfTest: state.historyTestReducer.lstDetailOfTest,
    studentAccountReducer: state.auth.login,
    loading: state.historyTestReducer.loading,
    values: state.historyTestReducer
  }

}
const mapDispatchToProps = (dispatch) => {
  return {
    historyTestAction: bindActionCreators(historyTestAction, dispatch),
  }
}

export default compose(withStyles(styles()), connect(mapStateToProps, mapDispatchToProps))(DetailOfTest)
