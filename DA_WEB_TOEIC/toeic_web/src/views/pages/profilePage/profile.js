import React from "react"
import {Badge, Button, Card, CardBody, CardHeader, CardTitle, Col, FormGroup, Row, Spinner} from "reactstrap"
import {ArrowLeft, Edit2, Save, XCircle} from "react-feather"
import "../../../assets/css/style-modal-account.css"
import {bindActionCreators, compose} from "redux";
import {connect} from "react-redux";
import {withStyles} from "@material-ui/styles";
import styles from "./styles";
import studentAccountAction from "../../../redux/actions/studentAccount"
import * as Icon from "react-bootstrap-icons"
import {Field, Form, Formik} from "formik";
import * as Yup from "yup";
import {toast} from "react-toastify"
import PropTypes from "prop-types";
import {showMessage} from "../../../commons/utils/convertDataForToast";
import {FormattedMessage} from "react-intl";
import {history} from "../../../history"

// const FILE_SIZE = 5 * 1024;
const FILE_SIZE = 5.001 * 1024 * 1024;
const SUPPORTED_FORMATS = [
  "image/jpg",
  "image/jpeg",
  "image/png"
];

const formSchema = Yup.object().shape({
  userShowName: Yup.string()
    .required("account.name.is.required")
    .min(1, "account.name.must.be.longer.than.1.character")
    .max(45, "account.name.is.no.longer.than.45.character"),
  phone: Yup.string().required("phone.number.is.required")
    .matches(/^(0|\84)(\s|\.)?((3[2-9])|(5[689])|(7[06-9])|(8[1-689])|(9[0-46-9]))(\d)(\s|\.)?(\d{3})(\s|\.)?(\d{3})$/, "invalid.phone.number")
    .min(1, "phone.number.must.be.longer.than.1.character")
    .max(10, "phone.number.is.no.longer.than.10.character")
    .nullable(),
  target: Yup.string()
    .matches(/^[0-9]+$/, "target.must.be.positive.number.from.1.to.990")
    .min(1, "target.must.be.longer.than.1.character")
    .max(3, "target.is.no.longer.than.3.character")
    .test("target", "target.must.be.positive.number.from.1.to.990", value => value ? value <= 990 && value > 0 ? true : false : true)
});

class ProfileContent extends React.Component {
  state = {
    activeTab: "1",
    isOpen: false,
    isEdit: false,
    fileUpload: null,
    urlImage: "",
    messTypeFileError: "",
    isUpdating: false,
  }

  constructor(props) {
    super(props);
    this.fileUpload = React.createRef();
  }

  componentWillMount() {
    debugger
    this.setState({
      isEdit: this.props.isEdit
    });
  }

  toggle = tab => {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab,
        isOpen: !this.state.isOpen
      })
    }
  }

  componentDidMount() {
    if (history.location.state && history.location.state.transaction) {
      let state = {...history.location.state};
      delete state.transaction;
      history.replace({...history.location, state});
    }
    const {studentAccountAction} = this.props;
    const {getStudentProfile} = studentAccountAction;
    getStudentProfile();
    this.setState({
      isEdit: false
    })
  }

  componentDidCatch(error, errorInfo) {
    toast.error("error", error);
    toast.error("errorInfo", errorInfo);
  }

  openEditForm = () => {
    document.getElementById("resetFormMiniTestProfile").click()
    this.setState({
      isEdit: true
    })
  }

  closeEditForm = (values, errors) => {
    const {studentDetail} = this.props.studentAccountReducer;
    values.userShowName = studentDetail && studentDetail.userShowName ? studentDetail.userShowName : ""
    values.phone = studentDetail && studentDetail.phone ? studentDetail.phone : ""
    values.target = studentDetail && studentDetail.target ? studentDetail.target : ""
    values.fileUpload = studentDetail && studentDetail.userAvatar ? studentDetail.userAvatar : null
    errors = {}
    this.setState({
      isEdit: false,
      messTypeFileError: '',
      fileUpload: null,
      urlImage: ""
    })
  }


  componentWillReceiveProps(nextProps, nextContext) {
    debugger
    const {studentAccountReducer} = nextProps;
    if (!studentAccountReducer.isEdit) {
      this.setState({
        isUpdating: false,
        isEdit: false
      });
    }
  }

  cutStringName = (data) => {
    let str = data;
    if (data !== null) {
      if (data.toString().length <= 20) {
        str = data;
      } else {
        str = data.slice(0, 20) + '...';
      }
    }
    return str;
  }

  fileHandle = (props) => {
    const {studentAccountReducer} = this.props;
    const {studentDetail} = studentAccountReducer;
    const handleChangeFile = (e) => {
      let reader = new FileReader();
      let file = e.target.files[0]
      if (file !== undefined && (file.type.indexOf("image/jpg") !== -1 || file.type.indexOf("image/jpeg") !== -1 || file.type.indexOf("image/png") !== -1)) {
        //5242880
        if (file !== undefined && file.size > 5242880) {
          this.setState({
            urlImage: "",
            fileUpload: null,
            messTypeFileError: showMessage("max.size.file.upload")
          })
        } else {
          reader.onloadend = () => {
            this.setState({
              urlImage: reader.result,
              fileUpload: file,
              messTypeFileError: ""
            });
          }
          reader.readAsDataURL(file)

        }
      }
      // else if (file === undefined) {
      //   this.setState({
      //     urlImage: "",
      //     fileUpload: null
      //   })
      // }
      else {
        this.setState({
          urlImage: "",
          fileUpload: null,
          messTypeFileError: showMessage("format.file.upload")
        })
      }
      props.form.setFieldValue(props.field.name, file);
      console.log(this.state)
    }

    return (
      <div className="text-center account-avatar w-100">
        <div className="row">
          {
            this.state.urlImage ?
              <img className="avatar" src={this.state.urlImage} alt="" height="200px" width="200px"/> :
              studentDetail.userAvatar ? studentDetail.userAvatar === null ?
                <Icon.PersonFill/> :
                <img className="avatar" src={studentDetail.userAvatar} alt="" height="200px" width="200px"/>
                : <Icon.PersonFill/>
          }
          <input type="file" id="file" name={props.field.name} className="input-file" onChange={(e) => {
            handleChangeFile(e)
          }}/>

          <label htmlFor="file" className="lable-for-file"><span className="glyphicon glyphicon-open"></span>
            {/*Tải lên*/}
            <FormattedMessage id="upload"/>
          </label>

        </div>

      </div>
    );
  }

  resetFormData = () => {
    this.setState({
      isEdit: false
    });
  }

  render() {
    debugger
    const {studentAccountReducer, backTab} = this.props;
    const {studentDetail, isLoading} = studentAccountReducer;
    const loadingComponent = (
      <div style={{
        position: 'absolute',
        zIndex: 110,
        top: 0,
        left: 0,
        width: '100%',
        height: '100%',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        background: 'rgba(255,255,255,0.8)'
      }}>
        <Spinner color="primary" className="reload-spinner"/>
      </div>
    );
    return (
      <>
        <style>{`
          #btnHighLight:focus-within {
            border: none !important;
            outline: 1px solid gray !important;
          }
           #btnSubmitHighLight:focus-within {
            border: none !important;
            outline: 1px solid gray !important;
          }
        `}

        </style>
        <Card>
          {isLoading && loadingComponent}
          <CardHeader>
            <CardTitle><h1><FormattedMessage id="management.account"/></h1></CardTitle>
            <Button color="primary" className="btn-icon right" color="primary" left onClick={backTab}
                    title={showMessage("back")}>
              <ArrowLeft/>
            </Button>
          </CardHeader>
          <CardBody>
            <Formik
              enableReinitialize
              initialValues={{
                userShowName: studentDetail && studentDetail.userShowName ? studentDetail.userShowName : "",
                phone: studentDetail && studentDetail.phone ? studentDetail.phone : "",
                target: studentDetail && studentDetail.target ? studentDetail.target : "",
                fileUpload: studentDetail && studentDetail.fileUpload ? studentDetail.fileUpload : null
              }}
              onSubmit={(values, actions) => {
                debugger
                if (values.userShowName.toString().indexOf('\s')) {
                  values.userShowName = values.userShowName.trim();
                }
                if (values.userShowName.toString().trim() === '') {
                  values.userShowName = ''
                } else {
                  setTimeout(() => {
                    actions.setSubmitting(false);
                    const {studentAccountAction} = this.props;
                    const {updateStudentProfile} = studentAccountAction;
                    values.fileUpload = this.state.fileUpload;
                    let formData = new FormData()
                    if (values.fileUpload !== null) {
                      formData.append(
                        "fileUpload",
                        values.fileUpload,
                      )
                    }
                    formData.append("userShowName", values.userShowName.toString().trim())
                    formData.append("phone", values.phone)
                    formData.append("target", values.target)
                    updateStudentProfile(formData)
                  }, 500);

                }

              }}

              validationSchema={formSchema}
            >
              {({values, errors, touched, resetForm}) => (
                <Form>
                  <Row className="profile-img-container d-flex align-items-center justify-content-between ">
                    <Col xs="9" sm="9" lg="3" className="text-center">
                      {
                        this.state.isEdit ?
                          <div>
                            <Field
                              name="fileUpload"
                              className="form-control"
                              component={this.fileHandle}
                              setFieldValue={this.props.setFieldValue}
                              errorMessage={errors[`fileUpload`] ? errors[`fileUpload`] : undefined}
                              touched={touched[`fileUpload`]}
                              style={{display: "flex"}}
                              onBlur={this.props.handleBlur}
                            />

                            {/*{ this.state.messTypeFileError !== "" ? <label style={{color : "red" , width : "100%" , textAlign : "center"}}><strong>{this.state.messTypeFileError}</strong></label> : null }*/}

                            {this.state.messTypeFileError !== "" ? <label style={{
                              color: "red",
                              width: "100%",
                              textAlign: "center",
                              marginTop: '3%'
                            }}><strong>{this.state.messTypeFileError}</strong></label> : null}

                          </div>
                          :
                          <div className="text-center account-avatar w-100">
                            <div className="row">
                              {studentDetail && studentDetail.userAvatar === null ?
                                <Icon.PersonFill
                                  className="avatar"
                                  style={{height: "200px", width: "200px"}}
                                >
                                </Icon.PersonFill>
                                :
                                <img
                                  src={studentDetail ? studentDetail.userAvatar : ""}
                                  alt=""
                                  height="200px"
                                  width="200px"
                                  className="img-border rounded-circle box-shadow-1 avatar"
                                />
                              }
                            </div>
                          </div>
                      }
                    </Col>
                    <Col xs="12" sm="12" lg="6" className="mt-2">
                      <div className="w-100">
                        <FormGroup>
                          <Row>
                            <Col xs="6" className="text-right">
                              <FormattedMessage id="email.login"/>:
                            </Col>
                            <Col xs="6">
                              {studentDetail && studentDetail.userName ? studentDetail.userName : ""}
                            </Col>
                          </Row>
                        </FormGroup>
                        <FormGroup>
                          <Row>
                            <Col xs="6" className="text-right">
                              <FormattedMessage id="account.name"/>{' '}{this.state.isEdit ?
                              <span>(<span style={{color: "red"}}>*</span>)</span> : null}:
                            </Col>
                            <Col xs="6">
                              {this.state.isEdit ?
                                <Field
                                  autoFocus={true}
                                  name="userShowName"
                                  className={`form-control ${errors.userShowName &&
                                  touched.userShowName &&
                                  "is-invalid"}`}
                                  setFieldValue={this.props.setFieldValue}
                                  defaultValue={values.userShowName.toString().trim()}
                                  onMouseLeave={(e) => {
                                    if (e.target.value.toString().indexOf('\s')) {
                                      values.userShowName = e.target.value.trim();
                                      e.target.value = e.target.value.trim();
                                    }
                                    if (e.target.value.toString().trim() === '') {
                                      values.userShowName = ''
                                      e.target.value = ""
                                    }
                                  }}
                                />
                                :
                                <div
                                  title={studentDetail && studentDetail.userShowName ? studentDetail.userShowName : ""}>{studentDetail && studentDetail.userShowName ? this.cutStringName(studentDetail.userShowName) : ""}</div>
                              }
                              {errors.userShowName && touched.userShowName ? (
                                <div className="invalid-feedback mt-25">
                                  {/*{errors.userShowName}*/}
                                  <FormattedMessage id={errors.userShowName}/>
                                </div>
                              ) : null}
                            </Col>
                          </Row>
                        </FormGroup>
                        <FormGroup>
                          <Row>
                            <Col xs="6" className="text-right">
                              <FormattedMessage id="phone.number"/>{' '}{this.state.isEdit ?
                              <span>(<span style={{color: "red"}}>*</span>)</span> : null}:
                            </Col>
                            <Col xs="6">
                              {this.state.isEdit ?
                                <Field
                                  name="phone"
                                  className={`form-control ${errors.phone &&
                                  touched.phone &&
                                  "is-invalid"}`}
                                  defaultValue={values.phone}
                                />
                                : (studentDetail && studentDetail.phone ? studentDetail.phone : "")}
                              {errors.phone && touched.phone ? (
                                <div className="invalid-feedback mt-25">
                                  {/*{errors.phone}*/}
                                  <FormattedMessage id={errors.phone}/>
                                </div>
                              ) : null}
                            </Col>
                          </Row>
                        </FormGroup>
                        <FormGroup>
                          <Row>
                            <Col xs="6" className="text-right">
                              <FormattedMessage id="current.score"/>:
                            </Col>
                            <Col xs="6">
                              {studentDetail && studentDetail.currentScore ? studentDetail.currentScore : ""}
                            </Col>
                          </Row>
                        </FormGroup>
                        <FormGroup>
                          <Row>
                            <Col xs="6" className="text-right">
                              <FormattedMessage id="target"/>:
                            </Col>
                            <Col xs="6">
                              {this.state.isEdit ?
                                <Field
                                  name="target"
                                  className={`form-control ${errors.target &&
                                  touched.target &&
                                  "is-invalid"}`}
                                  defaultValue={values.target}
                                />
                                : (studentDetail && studentDetail.target ? studentDetail.target : "")}
                              {errors.target && touched.target ? (
                                <div className="invalid-feedback mt-25">
                                  {/*{errors.target}*/}
                                  <FormattedMessage id={errors.target}/>
                                </div>
                              ) : null}
                            </Col>
                          </Row>
                        </FormGroup>
                        <FormGroup>
                          <Row>
                            <Col xs="6" className="text-right">
                              <FormattedMessage id="payment.status"/>:
                            </Col>
                            <Col xs="6">
                              {studentDetail && studentDetail.paymentStatus === 1
                                ? <Badge color="success" className="badge-md"><FormattedMessage id="paid"/></Badge>
                                : <Badge color="warning" className="badge-md"><FormattedMessage
                                  id="wait.for.pay"/></Badge>}
                            </Col>
                          </Row>
                        </FormGroup>
                      </div>

                    </Col>
                    <Col lg="3"></Col>

                  </Row>
                  <Row className="profile-img-container d-flex align-items-center justify-content-between mt-2">
                    <Col lg="3"/>
                    <Col lg="6">
                      <div className="w-100 text-center">
                        {
                          this.state.isEdit ?
                            <div>
                              <Button type="submit" color="info" className="btn-icon mr-2" style={{marginTop: '5px'}}
                                      id="btnSubmitHighLight">
                                <Save size={17}/> <FormattedMessage id="save"/>
                              </Button>
                              <Button onClick={() => this.closeEditForm(values, errors)} color="light"
                                      className="btn-icon" id="btnHighLightCancel"
                                      style={{marginTop: '5px'}}>
                                <XCircle size={17}/> <FormattedMessage id="cancel"/>
                              </Button>
                              {this.props.loading && <Spinner color="primary" className="ml-1"/>}
                            </div> :
                            <div>
                              <Button type="submit" style={{
                                visibility: "hidden"
                              }}>
                              </Button>
                              <Button type="button" onClick={this.openEditForm} color="info" className="btn-icon"
                                      style={{marginLeft: "-11%"}} id="btnHighLight">
                                <Edit2 size={15}/> <FormattedMessage id="edit"/>
                              </Button>
                              <Button type="reset" hidden={true} id="resetFormMiniTestProfile" onClick={resetForm}>
                                reset
                              </Button>
                            </div>
                        }
                      </div>
                    </Col>
                    <Col lg="3"/>
                  </Row>
                </Form>
              )}
            </Formik>
          </CardBody>
        </Card>
      </>
    )
  }
}

ProfileContent.propTypes = {
  classes: PropTypes.object,
  studentAccountAction: PropTypes.shape({
    updateStudentProfile: PropTypes.func
  })
}

const mapDispatchToProps = dispatch => {
  return {
    studentAccountAction: bindActionCreators(studentAccountAction, dispatch),
  }
}

const mapStateToProps = (state) => {
  return {
    studentAccountReducer: state.auth.login,
    loading: state.auth.login.isEdit
  }
}
const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
)

export default compose(withStyles(styles), withConnect)(ProfileContent);
