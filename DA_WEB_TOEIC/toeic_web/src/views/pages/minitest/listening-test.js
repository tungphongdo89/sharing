import React, { Component } from 'react';
import {connect} from "react-redux";
import {bindActionCreators, compose} from "redux";
import {Badge, Col, Container, Form, FormGroup, Input, Label, Row, Spinner, Button,
  UncontrolledPopover,
  PopoverHeader,
  PopoverBody,} from "reactstrap"
import styles from "./style";
import {withStyles} from "@material-ui/core";
import {ReactPlayerReading} from "../Practices/ListeningToeicShortTalkingPractice/style";

import testAction from "../../../redux/actions/do-test-management";
import * as Icon from "react-feather";
import {FormattedMessage} from "react-intl";

class ListeningTest extends Component {

  constructor(props) {
    super(props);
    this.state = {
      load: false,
      listQuesListening: [],
      listQuesReading: [],
      listSttQuesListeningChoosen: this.props.listSttQuestionListeningChoosen,
      listSttQuesReadingChoosen: this.props.listSttQuestionReadingChoosen,
      listQuestionListeningPrimitive: [],
      listPlaying: [
        {playing: true},  {playing: false}, {playing: false}, {playing: false}, {playing: false}, {playing: false}, {playing: false},
        {playing: false}, {playing: false}, {playing: false}, {playing: false}, {playing: false}, {playing: false}, {playing: false},{playing: false}, {playing: false}, {playing: false}, {playing: false},
        {playing: false}, {playing: false}, {playing: false}, {playing: false}, {playing: false}, {playing: false},
        {playing: false}, {playing: false}, {playing: false}, {playing: false}, {playing: false}, {playing: false}
      ],
      listPlayingAnswer: [
        {playing: false}, {playing: false}, {playing: false}, {playing: false}, {playing: false}, {playing: false},
        {playing: false}, {playing: false}, {playing: false}, {playing: false}, {playing: false}, {playing: false},{playing: false}, {playing: false}, {playing: false}, {playing: false},
        {playing: false}, {playing: false}, {playing: false}, {playing: false}, {playing: false},
        {playing: false}, {playing: false}, {playing: false}, {playing: false}, {playing: false}
      ],

      listPlayingAgainPart1: [
        {playing: false},  {playing: false}, {playing: false}, {playing: false},
        {playing: false},  {playing: false}, {playing: false}, {playing: false},
        {playing: false},  {playing: false}, {playing: false}, {playing: false},
        {playing: false},  {playing: false}, {playing: false}, {playing: false},
        {playing: false},  {playing: false}, {playing: false}, {playing: false},
        {playing: false},  {playing: false}, {playing: false}, {playing: false}
      ],

      checkPlayingPart1: -1,

      listPlayingAgainPart2: [
        {playing: false},  {playing: false}, {playing: false},
        {playing: false},  {playing: false}, {playing: false},
        {playing: false},  {playing: false}, {playing: false},
        {playing: false},  {playing: false}, {playing: false},
        {playing: false},  {playing: false}, {playing: false},
        {playing: false},  {playing: false}, {playing: false},
        {playing: false},  {playing: false}, {playing: false},
        {playing: false},  {playing: false}, {playing: false},
        {playing: false},  {playing: false}, {playing: false},
        {playing: false},  {playing: false}, {playing: false}
      ],

      checkPlayingPart2: -1,

      listPlayingAgainPart3: [
        {playing: false},  {playing: false}, {playing: false},
        {playing: false},  {playing: false}, {playing: false},
        {playing: false},  {playing: false}, {playing: false},
        {playing: false},  {playing: false}, {playing: false},
        {playing: false},  {playing: false}, {playing: false}
      ],

      checkPlayingPart3: -1,

      listPlayingAgainPart4: [
        {playing: false},  {playing: false}, {playing: false},
        {playing: false},  {playing: false}, {playing: false},
        {playing: false},  {playing: false}, {playing: false},
        {playing: false},  {playing: false}, {playing: false},
        {playing: false},  {playing: false}, {playing: false}
      ],

      checkPlayingPart4: -1,

      secondListenAgain: 0,
      currentPlaying: true,
      showTranslation: false
    }
  }

  componentDidMount() {
    this.setState({
      load: true
    })
    setTimeout(()=>{
      this.setState({
        load: false
      })
    }, 2000)
  }

  cutAnswerToChoose(answerToChoose){
    let char = 0;
    let answerArray = [];
    let subAnswer = "";

    let count=0,index=0; index = 0;
    for( count=-1,index=-2; index != -1; count++,index=answerToChoose.indexOf("|",index+1) );

    for(let i=1;i<=count;i++){
      char = answerToChoose.indexOf("|");
      subAnswer = answerToChoose.substring(0, char);
      answerArray.push(subAnswer);
      answerToChoose = answerToChoose.substring(char + 1);
    }
    return answerArray;
  }

  lowerCaseAll =(string)=>{
    return string.toLowerCase();
  }
  upperCaseFirstLetter =(string)=>{
    string = this.lowerCaseAll(string);
    return string.trim().charAt(0).toUpperCase() + string.trim().slice(1);
  }

  cutSubAnswer(subAnswer){
    let index = subAnswer.indexOf(".");
    let title = subAnswer.substring(0, index);
    let content = subAnswer.substring(index+1);

    let array = [];
    array.push(title);
    array.push(content);
    return array;
  }

  cutInputValue(value){
    debugger
    let index = value.indexOf("|");
    let title = value.substring(0, index);
    let content = value.substring(index+1);

    let array = [];
    array.push(title);
    array.push(content);
    return array;
  }

  compare =(a, b)=>{
    if ( a.stt < b.stt ){
      return -1;
    }
    if ( a.stt > b.stt ){
      return 1;
    }
    return 0;
  }

  getlistQuesListeningPrimitive =(question2)=> {
    debugger
    const list = this.props.listQuestionMinitest;
    let {listQuestionListeningPrimitive} = this.state;

    //chọn lần 1
    if(listQuestionListeningPrimitive.length === 0){
      let listQuesListeningPrimitive = [];

      for(let i=0;i<list[0].listCategoryMinitestDTOS[0].listQuestionMinitestDTOS.length;i++){
        let question = {};
        question.id = Number(list[0].listCategoryMinitestDTOS[0].listQuestionMinitestDTOS[i].id);
        question.stt = Number(list[0].listCategoryMinitestDTOS[0].listQuestionMinitestDTOS[i].stt);
        question.answerChoosen = "null";
        question.indexSubAnswer = null;
        if(list[0].listCategoryMinitestDTOS[0].categoryId === null){
          question.categoryId = 0;
        }
        else{
          question.categoryId = Number(list[0].listCategoryMinitestDTOS[0].categoryId);
        }

        question.partName = list[0].partName;
        if(question2.id === question.id){
          listQuesListeningPrimitive.push(question2)
        }
        else{
          listQuesListeningPrimitive.push(question)
        }

      }

      for(let i=0;i<list[1].listCategoryMinitestDTOS[0].listQuestionMinitestDTOS.length;i++){
        let question = {};
        question.id = Number(list[1].listCategoryMinitestDTOS[0].listQuestionMinitestDTOS[i].id);
        question.stt = Number(list[1].listCategoryMinitestDTOS[0].listQuestionMinitestDTOS[i].stt);
        question.answerChoosen = "null";
        question.indexSubAnswer = null;
        if(list[1].listCategoryMinitestDTOS[0].categoryId === null){
          question.categoryId = 0;
        }
        else{
          question.categoryId = Number(list[1].listCategoryMinitestDTOS[0].categoryId);
        }

        question.partName = list[1].partName;
        if(question2.id === question.id){
          listQuesListeningPrimitive.push(question2)
        }
        else{
          listQuesListeningPrimitive.push(question)
        }
      }

      for(let i=0;i<list[2].listCategoryMinitestDTOS.length;i++){
        for(let j=0;j<list[2].listCategoryMinitestDTOS[i].listQuestionMinitestDTOS.length;j++){
          let question = {};
          question.id = Number(list[2].listCategoryMinitestDTOS[i].listQuestionMinitestDTOS[j].id);
          question.stt = Number(list[2].listCategoryMinitestDTOS[i].listQuestionMinitestDTOS[j].stt);
          question.answerChoosen = "null";
          question.indexSubAnswer = null;
          question.categoryId = Number(list[2].listCategoryMinitestDTOS[i].categoryId);
          question.partName = list[2].partName;
          if(question2.id === question.id){
            listQuesListeningPrimitive.push(question2)
          }
          else{
            listQuesListeningPrimitive.push(question)
          }
        }
      }

      for(let i=0;i<list[3].listCategoryMinitestDTOS.length;i++){
        for(let j=0;j<list[3].listCategoryMinitestDTOS[i].listQuestionMinitestDTOS.length;j++){
          let question = {};
          question.id = Number(list[3].listCategoryMinitestDTOS[i].listQuestionMinitestDTOS[j].id);
          question.stt = Number(list[3].listCategoryMinitestDTOS[i].listQuestionMinitestDTOS[j].stt);
          question.answerChoosen = "null";
          question.indexSubAnswer = null;
          question.categoryId = Number(list[3].listCategoryMinitestDTOS[i].categoryId);
          question.partName = list[3].partName;
          if(question2.id === question.id){
            listQuesListeningPrimitive.push(question2)
          }
          else{
            listQuesListeningPrimitive.push(question)
          }
        }
      }
      this.setState({
        listQuestionListeningPrimitive: listQuesListeningPrimitive
      })
      return listQuesListeningPrimitive;
    }
    // chọn đáp án từ lần thứ 2 trở đi
    else{
      for(let i=0; i<listQuestionListeningPrimitive.length;i++){
        if(question2.id === listQuestionListeningPrimitive[i].id){
          listQuestionListeningPrimitive[i].id = question2.id;
          listQuestionListeningPrimitive[i].stt = question2.stt;
          listQuestionListeningPrimitive[i].answerChoosen = question2.answerChoosen;
          listQuestionListeningPrimitive[i].indexSubAnswer = question2.indexSubAnswer;
          listQuestionListeningPrimitive[i].categoryId = question2.categoryId;
          listQuestionListeningPrimitive[i].partName = question2.partName;
        }
      }
      this.setState({
        listQuestionListeningPrimitive: listQuestionListeningPrimitive
      })
      return listQuestionListeningPrimitive;
    }


  }

  onChangeValue =(e)=> {
    debugger
    const {testAction, listQuestionMinitest} = this.props;

    const {listQuesListening} = this.state;
    console.log(e.target.value);
    // let listQuestionListening = listQuesListening;
    let question = {};

    let answerChoosen = this.cutInputValue(e.target.value)[0].trim();
    let id = this.cutSubAnswer(this.cutInputValue(e.target.value)[1])[0];
    let indexSubAnswerAndCategoryIdAndSttAndPartName = this.cutSubAnswer(this.cutInputValue(e.target.value)[1])[1];
    let indexSubAnswer = this.cutInputValue(indexSubAnswerAndCategoryIdAndSttAndPartName)[0];
    let categoryIdAndSttAndPartName = this.cutInputValue(indexSubAnswerAndCategoryIdAndSttAndPartName)[1];
    let categoryId = this.cutSubAnswer(categoryIdAndSttAndPartName)[0];
    let sttAndPartName = this.cutSubAnswer(categoryIdAndSttAndPartName)[1];
    let stt = this.cutInputValue(sttAndPartName)[0];
    let partName = this.cutInputValue(sttAndPartName)[1];

    question.id = Number(id);
    question.stt = Number(stt);
    question.answerChoosen = answerChoosen;
    question.indexSubAnswer = Number(indexSubAnswer);
    if(categoryId === "null"){
      question.categoryId = 0;
    }
    else{
      question.categoryId = Number(categoryId);
    }
    question.partName = partName;

    //list mới
    let list = this.getlistQuesListeningPrimitive(question);
    //list mới

    const {getListQuesListeningChoosen} = testAction;
    getListQuesListeningChoosen(list);

  }

  checkingAnswerChoosen =(indexIncorrectAnswer, indexCorrectAnswer, indexSubAnswer)=>{
    // th câu đúng
    if(indexIncorrectAnswer !== -1){
      if(indexSubAnswer === indexIncorrectAnswer){
        return true;
      }
      else{
        return false;
      }
    }
    //th câu sai
    else{
      if(indexSubAnswer === indexCorrectAnswer){
        return true;
      }
      else {
        return false;
      }
    }
  }

  onEnding =(index)=>{
    const listPlaying2 = this.state.listPlaying;

    if(index < 29){
      listPlaying2[index].playing = false;
      listPlaying2[index+1].playing = true;

      this.setState({
        listPlaying: listPlaying2
      })
    }
    if(index === 29){
      debugger
      const {testAction} = this.props;
      const {doReadingAfterListening} = testAction;
      listPlaying2[index].playing = false;
      this.setState({
        listPlaying: listPlaying2
      })
      doReadingAfterListening(true)
    }

  }

  onEndingListeningAgainPart =()=>{
    debugger
    this.setState({
      listPlayingAgainPart1: [
        {playing: false},  {playing: false}, {playing: false}, {playing: false},
        {playing: false},  {playing: false}, {playing: false}, {playing: false},
        {playing: false},  {playing: false}, {playing: false}, {playing: false},
        {playing: false},  {playing: false}, {playing: false}, {playing: false},
        {playing: false},  {playing: false}, {playing: false}, {playing: false},
        {playing: false},  {playing: false}, {playing: false}, {playing: false}
      ],

      checkPlayingPart1: -1,

      listPlayingAgainPart2: [
        {playing: false},  {playing: false}, {playing: false},
        {playing: false},  {playing: false}, {playing: false},
        {playing: false},  {playing: false}, {playing: false},
        {playing: false},  {playing: false}, {playing: false},
        {playing: false},  {playing: false}, {playing: false},
        {playing: false},  {playing: false}, {playing: false},
        {playing: false},  {playing: false}, {playing: false},
        {playing: false},  {playing: false}, {playing: false},
        {playing: false},  {playing: false}, {playing: false},
        {playing: false},  {playing: false}, {playing: false}
      ],

      checkPlayingPart2: -1,

      listPlayingAgainPart3: [
        {playing: false},  {playing: false}, {playing: false},
        {playing: false},  {playing: false}, {playing: false},
        {playing: false},  {playing: false}, {playing: false},
        {playing: false},  {playing: false}, {playing: false},
        {playing: false},  {playing: false}, {playing: false}
      ],

      checkPlayingPart3: -1,

      listPlayingAgainPart4: [
        {playing: false},  {playing: false}, {playing: false},
        {playing: false},  {playing: false}, {playing: false},
        {playing: false},  {playing: false}, {playing: false},
        {playing: false},  {playing: false}, {playing: false},
        {playing: false},  {playing: false}, {playing: false}
      ],

      checkPlayingPart4: -1
    })

  }

  playingListeningAgainForPart1 =(question, indexQuestion, indexSubAnswer)=>{
    debugger
    this.setState({
      listPlayingAgainPart1: [
        {playing: false},  {playing: false}, {playing: false}, {playing: false},
        {playing: false},  {playing: false}, {playing: false}, {playing: false},
        {playing: false},  {playing: false}, {playing: false}, {playing: false},
        {playing: false},  {playing: false}, {playing: false}, {playing: false},
        {playing: false},  {playing: false}, {playing: false}, {playing: false},
        {playing: false},  {playing: false}, {playing: false}, {playing: false}
      ]
    })
    const listPlayingAgainPart = this.state.listPlayingAgainPart1;
    let indexPlaying = indexQuestion * 4 + indexSubAnswer;
    for(let i=0; i<listPlayingAgainPart.length;i++){
      if(i === indexPlaying){
        listPlayingAgainPart[i].playing = true;
      }
      else {
        listPlayingAgainPart[i].playing = false;
      }
    }

    this.setState({
      listPlayingAgainPart1: listPlayingAgainPart,
      checkPlayingPart1: indexPlaying
    })
  }


  playingListeningAgainForPart2 =(question, indexQuestion, indexSubAnswer)=>{
    debugger
    this.setState({
      listPlayingAgainPart2: [
        {playing: false},  {playing: false}, {playing: false},
        {playing: false},  {playing: false}, {playing: false},
        {playing: false},  {playing: false}, {playing: false},
        {playing: false},  {playing: false}, {playing: false},
        {playing: false},  {playing: false}, {playing: false},
        {playing: false},  {playing: false}, {playing: false},
        {playing: false},  {playing: false}, {playing: false},
        {playing: false},  {playing: false}, {playing: false},
        {playing: false},  {playing: false}, {playing: false},
        {playing: false},  {playing: false}, {playing: false}
      ],
    })
    const listPlayingAgainPart = this.state.listPlayingAgainPart2;
    let indexPlaying = indexQuestion * 3 + indexSubAnswer;
    for(let i=0; i<listPlayingAgainPart.length;i++){
      if(i === indexPlaying){
        listPlayingAgainPart[i].playing = true;
      }
      else {
        listPlayingAgainPart[i].playing = false;
      }
    }

    this.setState({
      listPlayingAgainPart2: listPlayingAgainPart,
      checkPlayingPart2: indexPlaying
    })
  }

  playingListeningAgainForPart3 =(indexCategory, indexQuestion)=>{
    debugger
    this.setState({
      listPlayingAgainPart3: [
        {playing: false},  {playing: false}, {playing: false},
        {playing: false},  {playing: false}, {playing: false},
        {playing: false},  {playing: false}, {playing: false},
        {playing: false},  {playing: false}, {playing: false},
        {playing: false},  {playing: false}, {playing: false}
      ],
    })
    const listPlayingAgainPart = this.state.listPlayingAgainPart3;
    let indexPlaying = indexCategory * 3 + indexQuestion;
    for(let i=0; i<listPlayingAgainPart.length;i++){
      if(i === indexPlaying){
        listPlayingAgainPart[i].playing = true;
      }
      else {
        listPlayingAgainPart[i].playing = false;
      }
    }

    this.setState({
      listPlayingAgainPart3: listPlayingAgainPart,
      checkPlayingPart3: indexPlaying
    })
  }

  playingListeningAgainForPart4 =(indexCategory, indexQuestion)=>{
    debugger
    this.setState({
      listPlayingAgainPart4: [
        {playing: false},  {playing: false}, {playing: false},
        {playing: false},  {playing: false}, {playing: false},
        {playing: false},  {playing: false}, {playing: false},
        {playing: false},  {playing: false}, {playing: false},
        {playing: false},  {playing: false}, {playing: false}
      ],
    })
    const listPlayingAgainPart = this.state.listPlayingAgainPart4;
    let indexPlaying = indexCategory * 3 + indexQuestion;
    for(let i=0; i<listPlayingAgainPart.length;i++){
      if(i === indexPlaying){
        listPlayingAgainPart[i].playing = true;
      }
      else {
        listPlayingAgainPart[i].playing = false;
      }
    }

    this.setState({
      listPlayingAgainPart4: listPlayingAgainPart,
      checkPlayingPart4: indexPlaying
    })
  }

  onEndingForAnswer =(index)=>{
    const listPlayingAnswer2 = this.state.listPlayingAnswer;

    listPlayingAnswer2[index].playing = false;

    this.setState({
      listPlayingAnswer: listPlayingAnswer2,
      currentPlaying: false
    })

  }

  ref = player => {
    this.player = player;
    // if(null !== this.player && this.props.showAnswer === true && this.state.currentPlaying === true){
    //   this.player.seekTo(this.state.secondListenAgain, 'seconds')
    // }

  }

  cutStartTime(startTime, indexSubAnswer){
    let st = "";
    this.cutAnswerToChoose(startTime).map((a, i) => {
      if(i === indexSubAnswer) {
        st = this.cutAnswerToChoose(startTime)[i].trim();
      }
    })
    console.log(st+"cc");
    return st;
  }

  playingListeningAgain =(question, indexQuestion, indexSubAnswer)=>{

    const listPlayingAnswer2 = this.state.listPlayingAnswer;
    let checkPlaying = 0;

    for(let i=0;i<this.state.listPlayingAnswer.length;i++){
      if(this.state.listPlayingAnswer[indexQuestion].playing !== true){
        checkPlaying ++;
      }
    }

    if(checkPlaying !== 0){
      //th1 : click vào file nghe khác
      for(let i=0;i<listPlayingAnswer2.length;i++){
        if(i === indexQuestion){
          listPlayingAnswer2[i].playing = true;
        }
        else {
          listPlayingAnswer2[i].playing = false;
        }
      }

      if (this.cutStartTime(question.startTime, indexSubAnswer) !== null
        && this.cutStartTime(question.startTime, indexSubAnswer).trim() !== "") {
        let second = 0;
        let listItemTime = this.cutStartTime(question.startTime, indexSubAnswer).split(":");
        second += parseFloat(listItemTime[0] * 3600);
        second += parseFloat(listItemTime[1] * 60);
        second += parseFloat(listItemTime[2]);

        this.setState({
          listPlayingAnswer: listPlayingAnswer2,
          secondListenAgain: second,
          currentPlaying: true
        })
      }
      //------------------------
    }
    else{
      //th2 : click vào file nghe khác
      for(let i=0;i<listPlayingAnswer2.length;i++){
        listPlayingAnswer2[i].playing = false;
      }

      this.setState({
        listPlayingAnswer: listPlayingAnswer2,
      })
      //------------------------
    }

  }



  render() {
    debugger
    const ttText = "abcdeee";
    const {classes, listQuestionMinitest, listQuestionAndAnswerMinitest, showAnswer, loadingQuestion,
      loadingSubmitAnswer} = this.props;
    const {listQuestionListeningPrimitive, checkPlayingPart1, checkPlayingPart2, checkPlayingPart3, checkPlayingPart4} = this.state;
    let checkPlayingP1 = checkPlayingPart1;
    let checkPlayingP2 = checkPlayingPart2;
    let checkPlayingP3 = checkPlayingPart3;
    let checkPlayingP4 = checkPlayingPart4;

    const loadingComponent = (
      <div style={{
        position: 'absolute',
        zIndex: 110,
        top: 0,
        left: 0,
        width: '100%',
        height: '100%',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        background: 'rgba(255,255,255,0.8)',
      }}>
        <Spinner color="primary" className="reload-spinner"/>
      </div>
    );
    return (
      <>
        {this.props.checkGetListQuestionMinitest === false ?
          <div className="mt-2">
            {loadingQuestion !== undefined ?
              <>{loadingQuestion === false ? <>{this.loadingComponent}</> : ""}</> : null
            }

            <Badge style={{backgroundColor: "rgba(32 171 231)", width: "100%", height: "60%", marginLeft: "0%",
              padding: "10px 0 0 0", borderRadius:'10px'}} className="badge-xl block">
              <div>
                <div className="text-left" style={{marginLeft: '0px'}}>
                  <div className={classes.titleModal}>
                    <h3 style={{color:'white', marginLeft: '2%', top: '40%', transform: 'translateY(-50%)', position: 'absolute'}}>
                      Listening Test
                    </h3>
                  </div>
                </div>
              </div>
            </Badge>
            <div style={{position: 'relative', margin: '0 1%'}}>
              {/*{this.state.load === true ? <>{loadingComponent}</> : ""}*/}
              <Row className={classes.bodyModal} style={{margin: '0 0% 0 0%', padding: '0 0%', width: "100%" }}>
                <div style={{margin:'3% 3%'}}>
                  {
                    showAnswer === true ? null :
                      <h5>In the Listening test, you will be asked to demonstrate how well you understand spoken English. The entire Listening test

                        will last approximately 45 minutes. There are four parts, and directions are given for each part. You must mark your

                        answers on the separate answer sheet. Do not write your answer in your test book.</h5>
                  }
                  <div>
                    {showAnswer === false && this.props.stopListening === true && this.props.loadingQuestion === true ?
                      <>
                        {listQuestionMinitest.length > 0 ?
                          <div >
                            {this.state.listPlaying[0].playing === true ?
                              <ReactPlayerReading
                                ref={this.ref}
                                url = "https://s3-ap-southeast-1.amazonaws.com/cnd.migitek.storage/audio/Part1.mp3"
                                // url = "https://s3-ap-southeast-1.amazonaws.com/cnd.migitek.storage/storage/assest/filesOfCategory\De_bai_ID_304\audio\audio3.mp3"
                                playing={this.state.listPlaying[0].playing}
                                controls={this.state.listPlaying[0].playing}
                                muted={!this.state.listPlaying[0].playing}
                                // onPlay={this.onPlaying}
                                // onPause={this.onPausing}
                                onEnded={() => this.onEnding(0)}
                                config={{
                                  file: {
                                    attributes: {
                                      controlsList: 'nodownload'
                                    }
                                  }
                                }}
                                width="320px"
                                height="100px"
                              >
                              </ReactPlayerReading> : null
                            }
                            {listQuestionMinitest[0].listCategoryMinitestDTOS[0].listQuestionMinitestDTOS.map((question, indexQuestion) =>
                              <>
                                 {/*file nghe câu hỏi */}
                                {this.state.listPlaying[indexQuestion + 1].playing === true ?
                                  <>
                                    <ReactPlayerReading
                                      ref={this.ref}
                                      url = {question ? question.pathFile1 : ""}
                                      playing={this.state.listPlaying[indexQuestion + 1].playing}
                                      controls={this.state.listPlaying[indexQuestion + 1].playing}
                                      muted={!this.state.listPlaying[indexQuestion + 1].playing}
                                      // onPlay={this.onPlaying}
                                      // onPause={this.onPausing}
                                      onEnded={() => this.onEnding(indexQuestion + 1)}
                                      config={{
                                        file: {
                                          attributes: {
                                            controlsList: 'nodownload'
                                          }
                                        }
                                      }}
                                      width="320px"
                                      height="100px"
                                    >
                                    </ReactPlayerReading>
                                  </> : null
                                }

                              </>
                            )}
                            {this.state.listPlaying[7].playing === true ?
                              <ReactPlayerReading
                                ref={this.ref}
                                url = "https://s3-ap-southeast-1.amazonaws.com/cnd.migitek.storage/audio/Part2.mp3"
                                playing={this.state.listPlaying[7].playing}
                                controls={this.state.listPlaying[7].playing}
                                muted={!this.state.listPlaying[7].playing}
                                // onPlay={this.onPlaying}
                                // onPause={this.onPausing}
                                onEnded={() => this.onEnding(7)}
                                config={{
                                  file: {
                                    attributes: {
                                      controlsList: 'nodownload'
                                    }
                                  }
                                }}
                                width="320px"
                                height="100px"
                              >
                              </ReactPlayerReading> : null
                            }
                            {listQuestionMinitest[1].listCategoryMinitestDTOS[0].listQuestionMinitestDTOS.map((question, indexQuestion) =>
                              <>
                                {this.state.listPlaying[indexQuestion+8].playing === true ?
                                  <ReactPlayerReading
                                    ref={this.ref}
                                    url = {question ? question.pathFile1 : ""}
                                    // url="https://s3-ap-southeast-1.amazonaws.com/cnd.migitek.storage/storage/assest/filesOfCategory\De_bai_ID_304\audio\audio3.mp3"
                                    playing={this.state.listPlaying[indexQuestion+8].playing}
                                    controls={this.state.listPlaying[indexQuestion+8].playing}
                                    muted={!this.state.listPlaying[indexQuestion+8].playing}
                                    // onPlay={this.onPlaying}
                                    // onPause={this.onPausing}
                                    onEnded={() => this.onEnding(indexQuestion+8)}
                                    config={{
                                      file: {
                                        attributes: {
                                          controlsList: 'nodownload'
                                        }
                                      }
                                    }}
                                    width="320px"
                                    height="100px"
                                  >
                                  </ReactPlayerReading> : null
                                }
                              </>

                            )}
                            {this.state.listPlaying[18].playing === true ?
                              <ReactPlayerReading
                                ref={this.ref}
                                url = "https://s3-ap-southeast-1.amazonaws.com/cnd.migitek.storage/audio/Part3.mp3"                            // url = "https://s3-ap-southeast-1.amazonaws.com/cnd.migitek.storage/storage/assest/filesOfCategory\De_bai_ID_304\audio\audio3.mp3"
                                playing={this.state.listPlaying[18].playing}
                                controls={this.state.listPlaying[18].playing}
                                muted={!this.state.listPlaying[18].playing}
                                onEnded={() => this.onEnding(18)}
                                config={{
                                  file: {
                                    attributes: {
                                      controlsList: 'nodownload'
                                    }
                                  }
                                }}
                                width="320px"
                                height="100px"
                              >
                              </ReactPlayerReading> : null
                            }
                            {listQuestionMinitest[2].listCategoryMinitestDTOS.map((category, indexCategory) =>
                              <>
                                {this.state.listPlaying[indexCategory+19].playing === true ?
                                  <ReactPlayerReading
                                    ref={this.ref}
                                    url = {category ? category.pathFile1 : ""}
                                    // url="https://s3-ap-southeast-1.amazonaws.com/cnd.migitek.storage/storage/assest/filesOfCategory\De_bai_ID_304\audio\audio3.mp3"
                                    playing={this.state.listPlaying[indexCategory+19].playing}
                                    controls={this.state.listPlaying[indexCategory+19].playing}
                                    muted={!this.state.listPlaying[indexCategory+19].playing}
                                    // onPlay={this.onPlaying}
                                    // onPause={this.onPausing}
                                    onEnded={() => this.onEnding(indexCategory+19)}
                                    config={{
                                      file: {
                                        attributes: {
                                          controlsList: 'nodownload'
                                        }
                                      }
                                    }}
                                    width="320px"
                                    height="100px"
                                  >
                                  </ReactPlayerReading> : null
                                }
                              </>
                            )}
                            {this.state.listPlaying[24].playing === true ?
                              <ReactPlayerReading
                                ref={this.ref}
                                url = "https://s3-ap-southeast-1.amazonaws.com/cnd.migitek.storage/audio/Part4.mp3"                            // url = "https://s3-ap-southeast-1.amazonaws.com/cnd.migitek.storage/storage/assest/filesOfCategory\De_bai_ID_304\audio\audio3.mp3"
                                playing={this.state.listPlaying[24].playing}
                                controls={this.state.listPlaying[24].playing}
                                muted={!this.state.listPlaying[24].playing}
                                // onPlay={this.onPlaying}
                                // onPause={this.onPausing}
                                onEnded={() => this.onEnding(24)}
                                config={{
                                  file: {
                                    attributes: {
                                      controlsList: 'nodownload'
                                    }
                                  }
                                }}
                                width="320px"
                                height="100px"
                              >
                              </ReactPlayerReading> : null
                            }
                            {listQuestionMinitest[3].listCategoryMinitestDTOS.map((category, indexCategory) =>
                              <>
                                {this.state.listPlaying[indexCategory+25].playing === true ?
                                  <ReactPlayerReading
                                    ref={this.ref}
                                    url = {category ? category.pathFile1 : ""}
                                    // url="https://s3-ap-southeast-1.amazonaws.com/cnd.migitek.storage/storage/assest/filesOfCategory\De_bai_ID_304\audio\audio3.mp3"
                                    playing={this.state.listPlaying[indexCategory+25].playing}
                                    controls={this.state.listPlaying[indexCategory+25].playing}
                                    muted={!this.state.listPlaying[indexCategory+25].playing}
                                    // onPlay={this.onPlaying}
                                    // onPause={this.onPausing}
                                    onEnded={() => this.onEnding(indexCategory+25)}
                                    config={{
                                      file: {
                                        attributes: {
                                          controlsList: 'nodownload'
                                        }
                                      }
                                    }}
                                    width="320px"
                                    height="100px"
                                  >
                                  </ReactPlayerReading> : null
                                }
                              </>
                            )}

                          </div>
                          : null
                        }
                      </>
                      :
                      <>
                        {listQuestionAndAnswerMinitest.length > 0 ?
                          <>
                            {listQuestionAndAnswerMinitest[0].listCategoryMinitestDTOS[0].listQuestionMinitestDTOS.map((question, indexQuestion) =>
                              <>
                                {this.state.listPlayingAnswer[indexQuestion].playing === true ?
                                  <ReactPlayerReading
                                    ref={this.ref}
                                    url = {question ? question.pathFile1 : ""}
                                    playing={this.state.listPlayingAnswer[indexQuestion].playing}
                                    controls={this.state.listPlayingAnswer[indexQuestion].playing}
                                    muted={!this.state.listPlayingAnswer[indexQuestion].playing}
                                    onEnded={() => this.onEndingForAnswer(indexQuestion)}
                                    // onPlay={this.onPlaying}
                                    // onPause={this.onPausing}
                                    config={{
                                      file: {
                                        attributes: {
                                          controlsList: 'nodownload'
                                        }
                                      }
                                    }}
                                    width="320px"
                                    height="100px"
                                  >
                                  </ReactPlayerReading> : null
                                }

                                {/*file nghe lại câu A*/}
                                {this.state.listPlayingAgainPart1[indexQuestion * 4].playing === true ?
                                  <>
                                    <ReactPlayerReading
                                      ref={this.ref}
                                      url = {question ? question.pathFileQuesA : ""}
                                      playing={this.state.listPlayingAgainPart1[indexQuestion * 4].playing}
                                      controls={true}
                                      onEnded={() => this.onEndingListeningAgainPart()}
                                      config={{
                                        file: {
                                          attributes: {
                                            controlsList: 'nodownload'
                                          }
                                        }
                                      }}
                                      width="320px"
                                      height="100px"
                                    >
                                    </ReactPlayerReading>
                                  </> : null
                                }

                                {/*file nghe lại câu B*/}
                                {this.state.listPlayingAgainPart1[indexQuestion * 4 + 1].playing === true ?
                                  <>
                                    <ReactPlayerReading
                                      ref={this.ref}
                                      url = {question ? question.pathFileQuesB : ""}
                                      playing={this.state.listPlayingAgainPart1[indexQuestion * 4 + 1].playing}
                                      controls={true}
                                      onEnded={() => this.onEndingListeningAgainPart()}
                                      config={{
                                        file: {
                                          attributes: {
                                            controlsList: 'nodownload'
                                          }
                                        }
                                      }}
                                      width="320px"
                                      height="100px"
                                    >
                                    </ReactPlayerReading>
                                  </> : null
                                }

                                {/*file nghe lại câu C*/}
                                {this.state.listPlayingAgainPart1[indexQuestion * 4 + 2].playing === true ?
                                  <>
                                    <ReactPlayerReading
                                      ref={this.ref}
                                      url = {question ? question.pathFileQuesC : ""}
                                      playing={this.state.listPlayingAgainPart1[indexQuestion * 4 + 2].playing}
                                      controls={true}
                                      onEnded={() => this.onEndingListeningAgainPart()}
                                      config={{
                                        file: {
                                          attributes: {
                                            controlsList: 'nodownload'
                                          }
                                        }
                                      }}
                                      width="320px"
                                      height="100px"
                                    >
                                    </ReactPlayerReading>
                                  </> : null
                                }

                                {/*file nghe lại câu D*/}
                                {this.state.listPlayingAgainPart1[indexQuestion * 4 + 3].playing === true ?
                                  <>
                                    <ReactPlayerReading
                                      ref={this.ref}
                                      url = {question ? question.pathFileQuesD : ""}
                                      playing={this.state.listPlayingAgainPart1[indexQuestion * 4 + 3].playing}
                                      controls={true}
                                      onEnded={() => this.onEndingListeningAgainPart()}
                                      config={{
                                        file: {
                                          attributes: {
                                            controlsList: 'nodownload'
                                          }
                                        }
                                      }}
                                      width="320px"
                                      height="100px"
                                    >
                                    </ReactPlayerReading>
                                  </> : null
                                }

                              </>
                            )}
                            {listQuestionAndAnswerMinitest[1].listCategoryMinitestDTOS[0].listQuestionMinitestDTOS.map((question, indexQuestion) =>
                              <>
                                {this.state.listPlayingAnswer[indexQuestion+6].playing === true ?
                                  <ReactPlayerReading
                                    ref={this.ref}
                                    url = {question ? question.pathFile1 : ""}
                                    // url="https://s3-ap-southeast-1.amazonaws.com/cnd.migitek.storage/storage/assest/filesOfCategory\De_bai_ID_304\audio\audio3.mp3"
                                    playing={this.state.listPlayingAnswer[indexQuestion+6].playing}
                                    controls={this.state.listPlayingAnswer[indexQuestion+6].playing}
                                    muted={!this.state.listPlayingAnswer[indexQuestion+6].playing}
                                    onEnded={() => this.onEndingForAnswer(indexQuestion+6)}
                                    // onPlay={this.onPlaying}
                                    // onPause={this.onPausing}
                                    config={{
                                      file: {
                                        attributes: {
                                          controlsList: 'nodownload'
                                        }
                                      }
                                    }}
                                    width="320px"
                                    height="100px"
                                  >
                                  </ReactPlayerReading> : null
                                }

                                {/*file nghe lại câu A part2*/}
                                {this.state.listPlayingAgainPart2[indexQuestion * 3].playing === true ?
                                  <>
                                    <ReactPlayerReading
                                      ref={this.ref}
                                      url = {question ? question.pathFileQuesA : ""}
                                      playing={this.state.listPlayingAgainPart2[indexQuestion * 3].playing}
                                      controls={true}
                                      onEnded={() => this.onEndingListeningAgainPart()}
                                      config={{
                                        file: {
                                          attributes: {
                                            controlsList: 'nodownload'
                                          }
                                        }
                                      }}
                                      width="320px"
                                      height="100px"
                                    >
                                    </ReactPlayerReading>
                                  </> : null
                                }

                                {/*file nghe lại câu B part2*/}
                                {this.state.listPlayingAgainPart2[indexQuestion * 3 + 1].playing === true ?
                                  <>
                                    <ReactPlayerReading
                                      ref={this.ref}
                                      url = {question ? question.pathFileQuesB : ""}
                                      playing={this.state.listPlayingAgainPart2[indexQuestion * 3 + 1].playing}
                                      controls={true}
                                      onEnded={() => this.onEndingListeningAgainPart()}
                                      config={{
                                        file: {
                                          attributes: {
                                            controlsList: 'nodownload'
                                          }
                                        }
                                      }}
                                      width="320px"
                                      height="100px"
                                    >
                                    </ReactPlayerReading>
                                  </> : null
                                }

                                {/*file nghe lại câu C part2*/}
                                {this.state.listPlayingAgainPart2[indexQuestion * 3 + 2].playing === true ?
                                  <>
                                    <ReactPlayerReading
                                      ref={this.ref}
                                      url = {question ? question.pathFileQuesC : ""}
                                      playing={this.state.listPlayingAgainPart2[indexQuestion * 3 + 2].playing}
                                      controls={true}
                                      onEnded={() => this.onEndingListeningAgainPart()}
                                      config={{
                                        file: {
                                          attributes: {
                                            controlsList: 'nodownload'
                                          }
                                        }
                                      }}
                                      width="320px"
                                      height="100px"
                                    >
                                    </ReactPlayerReading>
                                  </> : null
                                }


                              </>

                            )}
                            {listQuestionAndAnswerMinitest[2].listCategoryMinitestDTOS.map((category, indexCategory) =>
                              <>
                                {this.state.listPlayingAnswer[indexCategory+16].playing === true ?
                                  <ReactPlayerReading
                                    ref={this.ref}
                                    url = {category ? category.pathFile1 : ""}
                                    // url="https://s3-ap-southeast-1.amazonaws.com/cnd.migitek.storage/storage/assest/filesOfCategory\De_bai_ID_304\audio\audio3.mp3"
                                    playing={this.state.listPlayingAnswer[indexCategory+16].playing}
                                    controls={this.state.listPlayingAnswer[indexCategory+16].playing}
                                    muted={!this.state.listPlayingAnswer[indexCategory+16].playing}
                                    onEnded={() => this.onEndingForAnswer(indexCategory+16)}
                                    // onPlay={this.onPlaying}
                                    // onPause={this.onPausing}
                                    config={{
                                      file: {
                                        attributes: {
                                          controlsList: 'nodownload'
                                        }
                                      }
                                    }}
                                    width="320px"
                                    height="100px"
                                  >
                                  </ReactPlayerReading> : null
                                }

                                {category.listQuestionMinitestDTOS.map((question, indexQuestion) =>
                                  <>
                                    {/*nghe lại câu hỏi part3*/}
                                    {this.state.listPlayingAgainPart3[indexCategory * 3 + indexQuestion].playing === true ?
                                      <>
                                        <ReactPlayerReading
                                          ref={this.ref}
                                          url={question ? question.pathFileQues : ""}
                                          playing={this.state.listPlayingAgainPart3[indexCategory * 3 + indexQuestion].playing}
                                          controls={true}
                                          onEnded={() => this.onEndingListeningAgainPart(indexCategory * 3 + indexQuestion)}
                                          config={{
                                            file: {
                                              attributes: {
                                                controlsList: 'nodownload'
                                              }
                                            }
                                          }}
                                          width="320px"
                                          height="100px"
                                        >
                                        </ReactPlayerReading>
                                      </> : null
                                    }
                                  </>
                                )}
                              </>
                            )}
                            {listQuestionAndAnswerMinitest[3].listCategoryMinitestDTOS.map((category, indexCategory) =>
                              <>
                                {this.state.listPlayingAnswer[indexCategory+21].playing === true ?
                                  <ReactPlayerReading
                                    ref={this.ref}
                                    url = {category ? category.pathFile1 : ""}
                                    // url="https://s3-ap-southeast-1.amazonaws.com/cnd.migitek.storage/storage/assest/filesOfCategory\De_bai_ID_304\audio\audio3.mp3"
                                    playing={this.state.listPlayingAnswer[indexCategory+21].playing}
                                    controls={this.state.listPlayingAnswer[indexCategory+21].playing}
                                    muted={!this.state.listPlayingAnswer[indexCategory+21].playing}
                                    onSeek={this.state.secondListenAgain}
                                    onEnded={() => this.onEndingForAnswer(indexCategory+21)}
                                    // onPlay={this.onPlaying}
                                    // onPause={this.onPausing}
                                    config={{
                                      file: {
                                        attributes: {
                                          controlsList: 'nodownload'
                                        }
                                      }
                                    }}
                                    width="320px"
                                    height="100px"
                                  >
                                  </ReactPlayerReading> : null
                                }

                                {category.listQuestionMinitestDTOS.map((question, indexQuestion) =>
                                  <>
                                    {/*nghe lại câu hỏi part4*/}
                                    {this.state.listPlayingAgainPart4[indexCategory * 3 + indexQuestion].playing === true ?
                                      <>
                                        <ReactPlayerReading
                                          ref={this.ref}
                                          url={question ? question.pathFileQues : ""}
                                          playing={this.state.listPlayingAgainPart4[indexCategory * 3 + indexQuestion].playing}
                                          controls={true}
                                          onEnded={() => this.onEndingListeningAgainPart(indexCategory * 3 + indexQuestion)}
                                          config={{
                                            file: {
                                              attributes: {
                                                controlsList: 'nodownload'
                                              }
                                            }
                                          }}
                                          width="320px"
                                          height="100px"
                                        >
                                        </ReactPlayerReading>
                                      </> : null
                                    }
                                  </>
                                )}
                              </>
                            )}

                          </>
                          : null
                        }
                      </>
                    }
                  </div>

                  {showAnswer === false ?
                    <>
                      {listQuestionMinitest.length > 0 ?
                        <>
                          {/*--------------------------------------PART1----------------------------------------*/}
                          <div>
                            <h1 id="PART1" style={{fontWeight: '700'}}>PART 1</h1>
                            <h5>Directions: For each question in this part, you will hear four statements about a picture in your test book. When you hear the

                              statements, you must select the one statement that best describes what you see in the picture. Then find the number of the

                              question on your answer sheet and mark your answer. The statements will not be printed in your test book and will be

                              spoken only one time.</h5>
                            <>
                              {listQuestionMinitest[0].listCategoryMinitestDTOS[0].listQuestionMinitestDTOS.map((question, index) =>
                                <Row id={String(question.stt)} style={{marginTop: '4%'}}>
                                  <Col xs={1}><h3>{question.stt}.</h3></Col>
                                  <Col xs="5">
                                    {question ? this.cutAnswerToChoose(question.answersToChoose).map((subAnswer, indexSubAnswer) =>
                                      <FormGroup check style={{marginLeft:'0', marginBottom:'3%'}}>
                                        <Label  check className={classes.labelRadioButton} style={{cursor: 'pointer'}}>
                                          <Input type="radio" name={question.stt}
                                                 onChange={this.onChangeValue}
                                                 value={subAnswer+"|"+question.id+"."+indexSubAnswer
                                                 +"|"+listQuestionMinitest[0].listCategoryMinitestDTOS[0].categoryId+"."+question.stt
                                                 +"|"+listQuestionMinitest[0].partName}
                                          />
                                          {/*{this.upperCaseFirstLetter(subAnswer)}*/}
                                          <h4>({ indexSubAnswer===0 ? "A" : (indexSubAnswer===1 ? "B" : (indexSubAnswer===2 ? "C" : "D")) }) </h4>
                                        </Label>
                                      </FormGroup>
                                    ) : null }
                                  </Col>
                                  <Col xs={6}>
                                    <img className={classes.imgClass} style={{width: '100%', border: '5px solid lightgray'}}
                                         src={question.pathFile2 ? question.pathFile2 : ""}/>
                                  </Col>
                                </Row>
                              )}
                            </>
                          </div>
                          {/*--------------------------------------end PART1----------------------------------------*/}



                          {/*--------------------------------------PART2----------------------------------------*/}
                          <div className="mt-3" style={{marginTop: '5% !important'}}>
                            <h1 id="PART2" style={{fontWeight: '700'}}>PART 2</h1>
                            <h5>Directions: You will hear a question or statement and three responses spoken in English. They will not be printed in your

                              test book and will be spoken only one time. Select the best response to the question or statement and mark the letter (A),

                              (B), or (C) on your answer sheet. </h5>

                            <Row style={{marginTop: '4%'}}>
                              {listQuestionMinitest[1].listCategoryMinitestDTOS[0].listQuestionMinitestDTOS.map((question, index) =>
                                <Col xs="6" md="6" sm="6" lg="6" style={{marginBottom:'3%'}}>
                                  <Row id={String(question.stt)}>
                                    <Col xs="1" md="1" sm="1" lg="1"><h3>{question.stt}.</h3></Col>
                                    <Col xs="11" md="11" sm="11" lg="11">
                                      {question ? this.cutAnswerToChoose(question.answersToChoose).map((subAnswer, indexSubAnswer) =>
                                        <FormGroup check style={{marginLeft:'8%', marginBottom:'3%'}}>
                                          <Label  check className={classes.labelRadioButton} style={{cursor: 'pointer'}}>
                                            <Input type="radio" name={question.stt}
                                                   onChange={this.onChangeValue}
                                                   value={subAnswer+"|"+question.id+"."+indexSubAnswer
                                                   +"|"+listQuestionMinitest[1].listCategoryMinitestDTOS[0].categoryId+"."+question.stt
                                                   +"|"+listQuestionMinitest[1].partName}
                                            />
                                            <h4>({ indexSubAnswer===0 ? "A" : (indexSubAnswer===1 ? "B" : (indexSubAnswer===2 ? "C" : "D")) })</h4>
                                          </Label>
                                        </FormGroup>
                                      ) : null }
                                    </Col>
                                  </Row>
                                </Col>
                              )}
                            </Row>
                          </div>
                          {/*--------------------------------------end PART2----------------------------------------*/}



                          {/*-----------------------------------------PART3------------------------------------------*/}
                          <div className="mt-3">
                            <h1 id="PART3" style={{fontWeight: '700'}}>PART 3</h1>
                            <h5>Directions: You will hear some conversations between two people. You will be asked to answer three questions about what

                              the speakers say in each conversation. Select the best response for each question and mark the letter (A), (B), (C), or (D)

                              on your answer sheet. The conversations will not be printed in your test book and will be spoken only one time.</h5>

                            {listQuestionMinitest[2].listCategoryMinitestDTOS.map((category, indexCategory) =>
                              <>
                                {null === category.pathFile2 ?
                                  <>
                                    <h4 style={{fontWeight: '700', marginTop: '5%'}}>3.{indexCategory+1}.</h4>

                                    {category.listQuestionMinitestDTOS.map((question, indexQuestion) =>
                                      <Row id={String(question.stt)}  style={{marginTop:'3%', marginLeft: '10%'}}>
                                        <Col xs="6" md="6" sm="6" lg="6">
                                          <h4>{question.stt}. {question.name}</h4>
                                        </Col>
                                        <Col xs="6" md="6" sm="6" lg="6">
                                          {this.cutAnswerToChoose(question.answersToChoose).map((subAnswer, indexSubAnswer) =>
                                            <FormGroup check style={{marginLeft:'8%', marginBottom:'5%'}}>
                                              <Label  check className={classes.labelRadioButton} style={{cursor: 'pointer'}}>
                                                <Input type="radio" name={question.stt}
                                                       onChange={this.onChangeValue}
                                                       value={subAnswer+"|"+question.id+"."+indexSubAnswer
                                                       +"|"+category.categoryId +"."+question.stt
                                                       +"|"+listQuestionMinitest[2].partName}
                                                />
                                                ({ indexSubAnswer===0 ? "A" : (indexSubAnswer===1 ? "B" : (indexSubAnswer===2 ? "C" : "D")) }) {this.upperCaseFirstLetter(subAnswer)}
                                              </Label>
                                            </FormGroup>
                                          )}
                                        </Col>
                                      </Row>
                                    )}

                                  </>
                                  :
                                  <>
                                    <h4 style={{fontWeight: '700', marginTop: '5%'}}>3.{indexCategory+1}.</h4>
                                    <Row style={{marginTop:'3%', marginLeft: '10%'}}>
                                      <Col xs="6" md="6" sm="6" lg="6">
                                        <img className={classes.imgClass} style={{width: '100%', border: '5px solid lightgray'}}
                                             src={category.pathFile2}/>
                                      </Col>
                                      <Col xs="6" md="6" sm="6" lg="6">
                                        {category.listQuestionMinitestDTOS.map((question, indexQuestion) =>
                                          <Col id={String(question.stt)}  xs="12" md="12" sm="12" lg="12">
                                            <h4>{question.stt}. {question.name}</h4>
                                            {this.cutAnswerToChoose(question.answersToChoose).map((subAnswer, indexSubAnswer) =>
                                              <FormGroup check style={{marginLeft:'8%', marginBottom:'5%'}}>
                                                <Label  check className={classes.labelRadioButton} style={{cursor: 'pointer'}}>
                                                  <Input type="radio" name={question.stt}
                                                         onChange={this.onChangeValue}
                                                         value={subAnswer+"|"+question.id+"."+indexSubAnswer
                                                         +"|"+category.categoryId+"."+question.stt
                                                         +"|"+listQuestionMinitest[2].partName}
                                                  />
                                                  ({ indexSubAnswer===0 ? "A" : (indexSubAnswer===1 ? "B" : (indexSubAnswer===2 ? "C" : "D")) }) {this.upperCaseFirstLetter(subAnswer)}
                                                </Label>
                                              </FormGroup>
                                            )}
                                          </Col>
                                        )}
                                      </Col>
                                    </Row>
                                  </>
                                }
                              </>
                            )}
                          </div>
                          {/*-------------------------------------------end PART3---------------------------------------*/}


                          {/*--------------------------------------------PART4--------------------------------------------*/}
                          <div className="mt-3">
                            <h1 id="PART4" style={{fontWeight: '700'}}>PART 4</h1>
                            <h5>Directions: You will hear some talks given by a single speaker. You will be asked to answer three questions about what the

                              speaker say in each talk. Select the best response for each question and mark the letter (A), (B), (C), or (D) on your answer

                              sheet. The talks will not be printed in your test book and will be spoken only one time.</h5>

                            {listQuestionMinitest[3].listCategoryMinitestDTOS.map((category, indexCategory) =>
                              <>
                                {null === category.pathFile2 ?
                                  <>
                                    <h4 style={{fontWeight: '700', marginTop: '5%'}}>4.{indexCategory + 1}.</h4>

                                    {category.listQuestionMinitestDTOS.map((question, indexQuestion) =>
                                      <Row id={String(question.stt)}  style={{marginTop:'3%', marginLeft: '10%'}}>
                                        <Col xs="6" md="6" sm="6" lg="6">
                                          <h4>{question.stt}. {question.name}</h4>
                                        </Col>
                                        <Col xs="6" md="6" sm="6" lg="6">
                                          {this.cutAnswerToChoose(question.answersToChoose).map((subAnswer, indexSubAnswer) =>
                                            <FormGroup check style={{marginLeft:'8%', marginBottom:'5%'}}>
                                              <Label  check className={classes.labelRadioButton} style={{cursor: 'pointer'}}>
                                                <Input type="radio" name={question.stt}
                                                       onChange={this.onChangeValue}
                                                       value={subAnswer+"|"+question.id+"."+indexSubAnswer
                                                       +"|"+category.categoryId+"."+question.stt
                                                       +"|"+listQuestionMinitest[3].partName}
                                                />
                                                ({ indexSubAnswer===0 ? "A" : (indexSubAnswer===1 ? "B" : (indexSubAnswer===2 ? "C" : "D")) }) {this.upperCaseFirstLetter(subAnswer)}
                                              </Label>
                                            </FormGroup>
                                          )}
                                        </Col>
                                      </Row>
                                    )}

                                  </>
                                  :
                                  <>
                                    <h4 style={{fontWeight: '700', marginTop: '5%'}}>4.{indexCategory+1}.</h4>
                                    <Row style={{marginTop:'3%', marginLeft: '10%'}}>
                                      <Col xs="6" md="6" sm="6" lg="6">
                                        <img className={classes.imgClass} style={{width: '100%', border: '5px solid lightgray'}}
                                             src={category.pathFile2}/>
                                      </Col>
                                      <Col xs="6" md="6" sm="6" lg="6">
                                        {category.listQuestionMinitestDTOS.map((question, indexQuestion) =>
                                          <Col id={String(question.stt)}  xs="12" md="12" sm="12" lg="12">
                                            <h4>{question.stt}. {question.name}</h4>
                                            {this.cutAnswerToChoose(question.answersToChoose).map((subAnswer, indexSubAnswer) =>
                                              <FormGroup check style={{marginLeft:'8%', marginBottom:'5%'}}>
                                                <Label  check className={classes.labelRadioButton} style={{cursor: 'pointer'}}>
                                                  <Input type="radio" name={question.stt}
                                                         onChange={this.onChangeValue}
                                                         value={subAnswer+"|"+question.id+"."+indexSubAnswer
                                                         +"|"+category.categoryId+"."+question.stt
                                                         +"|"+listQuestionMinitest[3].partName}
                                                  />
                                                  ({ indexSubAnswer===0 ? "A" : (indexSubAnswer===1 ? "B" : (indexSubAnswer===2 ? "C" : "D")) }) {this.upperCaseFirstLetter(subAnswer)}
                                                </Label>
                                              </FormGroup>
                                            )}
                                          </Col>
                                        )}
                                      </Col>
                                    </Row>
                                  </>
                                }
                              </>
                            )}
                          </div>
                          {/*-------------------------------------------- end PART4--------------------------------------------*/}
                        </> : null
                      }
                    </>
                    :
                    <>
                      {listQuestionAndAnswerMinitest.length > 0 ?
                        <>
                          {/*--------------------------------------PART1----------------------------------------*/}
                          <div>
                            <h1 id="PART1" style={{fontWeight: '700'}}>PART 1</h1>
                            <>
                              {listQuestionAndAnswerMinitest[0].listCategoryMinitestDTOS[0].listQuestionMinitestDTOS.map((question, index) =>
                                <>
                                  <Row id={String(question.stt)}  style={{marginTop: '4%'}}>
                                    <Col xs={1}><h4>{question.stt}.</h4></Col>
                                    <Col xs="5">
                                      {question ? this.cutAnswerToChoose(question.answersToChoose).map((subAnswer, indexSubAnswer) =>
                                        <>
                                          <Col xs="12" md="12" sm="12" lg="12" style={{display: 'flex', padding: '0%'}}>
                                            <Col xs="1" md="2" sm="1" lg="1" style={{padding: '0%'}}>
                                              {indexSubAnswer === question.indexCorrectAnswer ?
                                                <Icon.Check size={25} style={{color: 'green'}}/> : null
                                              }
                                              {indexSubAnswer === question.indexIncorrectAnswer ?
                                                <Icon.X size={25} style={{color: '#b21717'}} /> : null
                                              }
                                              {/*<Icon.Check size={25} style={{color: 'green'}}/>*/}
                                            </Col>
                                            <Col xs="11" md="10" sm="11" lg="11" style={{padding: '0%', display: 'flex'}}>

                                              <FormGroup check style={{marginBottom:'3%', display: 'flex'}}>
                                                <Label  check className={classes.labelRadioButton} style={{cursor: 'pointer'}}>
                                                  <Input type="radio" name={question.stt}
                                                         checked={this.checkingAnswerChoosen(question.indexIncorrectAnswer,
                                                           question.indexCorrectAnswer, indexSubAnswer)}
                                                  />
                                                  <h4>({ indexSubAnswer===0 ? "A" : (indexSubAnswer===1 ? "B" : (indexSubAnswer===2 ? "C" : "D")) })</h4>
                                                </Label>

                                                <Button outline className={classes.buttonListenAgain}
                                                        style={{padding: '0', display:'inline-flex', marginLeft:'10px', width:'auto'}} id='btnHighlightRepeat'
                                                        onClick={()=>this.playingListeningAgainForPart1(question, index, indexSubAnswer)}>
                                                  <div style={{padding: '0 5px', display:'inline-flex'}}>
                                                    {checkPlayingP1 === (index * 4 + indexSubAnswer) ?

                                                      <Icon.Volume1 size={12} style={{marginTop:'2.5px', marginRight:'2px'}} />
                                                      :
                                                      <Icon.Play size={11} style={{marginTop:'2.5px', marginRight:'2px'}}/>
                                                    }
                                                    <p style={{marginTop:'-2px'}}><FormattedMessage id="listenAgain" tagName="data" /></p>

                                                  </div>

                                                </Button>
                                              </FormGroup>

                                              {/*/!*phần dịch câu đáp án*!/*/}
                                              {/*<button style={{border: 'none', backgroundColor: 'white', padding: '0', marginLeft: '2%'}}*/}
                                                      {/*id={"a" + question.stt + indexSubAnswer} className="mr-1 mb-1">*/}
                                                {/*<Icon.AlertCircle style={{verticalAlign: 'unset'}}  size={20} />*/}
                                              {/*</button>*/}
                                              {/*<UncontrolledPopover trigger="focus" placement="bottom" target={"a" + question.stt + indexSubAnswer}>*/}
                                                {/*<PopoverBody style={{borderColor: 'green !important',*/}
                                                  {/*color: 'white', backgroundColor: 'green', borderRadius: '5px'}}>*/}
                                                  {/*<FormattedMessage id="translate" />: {indexSubAnswer===0 ? question.translatingQuesA : (indexSubAnswer===1 ? question.translatingQuesB : (indexSubAnswer===2 ? question.translatingQuesC : question.translatingQuesD))}*/}
                                                {/*</PopoverBody>*/}
                                              {/*</UncontrolledPopover>*/}
                                              {/*/!*phần dịch câu đáp án*!/*/}


                                              {/*<div style={{marginLeft: '3%'}}>*/}
                                                {/*<button className={classes.btnRepeat}*/}
                                                        {/*onClick={()=>this.playingListeningAgainForPart1(question, index, indexSubAnswer)}>*/}
                                                  {/*<div style={{display: 'flex'}}>*/}
                                                    {/*{*/}
                                                      {/*checkPlayingP1 === (index * 4 + indexSubAnswer) ?*/}
                                                        {/*<Icon.Volume1 size={12} className={classes.iconPlay} />*/}
                                                        {/*:*/}
                                                        {/*<Icon.Play className={classes.iconPlay} size={11}/>*/}
                                                    {/*}*/}
                                                    {/*<FormattedMessage id="listenAgain" tagName="data" />*/}
                                                  {/*</div>*/}

                                                {/*</button>*/}
                                              {/*</div>*/}

                                            </Col>
                                          </Col>
                                        </>
                                      ) : null }
                                    </Col>
                                    <Col xs={6}>
                                      <img className={classes.imgClass} style={{width: '100%', border: '5px solid lightgray'}}
                                           src={question.pathFile2 ? question.pathFile2 : ""}/>
                                    </Col>
                                  </Row>
                                  <Row>
                                    <Col xs="12" md="12" sm="12" lg="12" style={{marginBottom: '10%'}}>
                                      <fieldset className={classes.fieldsetDescription}>
                                        <legend className={classes.legendDescription}><FormattedMessage id="description" /></legend>
                                        <i>
                                            {
                                              null === question.description ?
                                                <><FormattedMessage id={"no.explanation"}/></>
                                                :
                                                 question.description.split("\n").map((des, i) =>
                                                   <p>
                                                     {i === 0 ? <>(A) {des}</> : null}
                                                     {i === 1 ? <>(B) {des}</> : null}
                                                     {i === 2 ? <>(C) {des}</> : null}
                                                     {i === 3 ? <>(D) {des}</> : null}
                                                   </p>
                                                 )
                                            }
                                        </i>
                                      </fieldset>
                                    </Col>
                                  </Row>
                                </>
                              )}
                            </>
                          </div>
                          {/*--------------------------------------end PART1----------------------------------------*/}



                          {/*--------------------------------------PART2----------------------------------------*/}
                          <div className="mt-3" style={{marginTop: '5% !important'}}>
                            <h1 id="PART2" style={{fontWeight: '700'}}>PART 2</h1>

                            <>
                              {listQuestionAndAnswerMinitest[1].listCategoryMinitestDTOS[0].listQuestionMinitestDTOS.map((question, index) =>
                                <>
                                  <Row id={String(question.stt)}  style={{marginTop: '4%'}}>
                                    <Col xs={1}><h4>{question.stt}.</h4></Col>
                                    <Col xs="5">
                                      {question ? this.cutAnswerToChoose(question.answersToChoose).map((subAnswer, indexSubAnswer) =>
                                        <>
                                          <Col xs="12" md="12" sm="12" lg="12" style={{display: 'flex', padding: '0%'}}>
                                            <Col xs="1" md="2" sm="1" lg="1" style={{padding: '0%'}}>
                                              {indexSubAnswer === question.indexCorrectAnswer ?
                                                <Icon.Check size={25} style={{color: 'green'}}/> : null
                                              }
                                              {indexSubAnswer === question.indexIncorrectAnswer ?
                                                <Icon.X size={25} style={{color: '#b21717'}} /> : null
                                              }
                                              {/*<Icon.Check size={25} style={{color: 'green'}}/>*/}
                                            </Col>
                                            <Col xs="11" md="10" sm="11" lg="11" style={{padding: '0%', display: 'flex'}}>

                                              <FormGroup check style={{marginBottom:'3%', display: 'flex'}}>
                                                <Label  check className={classes.labelRadioButton} style={{cursor: 'pointer'}}>
                                                  <Input type="radio" name={question.stt}
                                                         checked={this.checkingAnswerChoosen(question.indexIncorrectAnswer,
                                                           question.indexCorrectAnswer, indexSubAnswer)}
                                                  />
                                                  <h4>({ indexSubAnswer===0 ? "A" : (indexSubAnswer===1 ? "B" : (indexSubAnswer===2 ? "C" : "D")) })</h4>
                                                </Label>

                                                <Button outline className={classes.buttonListenAgain}
                                                        style={{padding: '0', display:'inline-flex', marginLeft:'10px', width:'auto'}} id='btnHighlightRepeat'
                                                        onClick={()=>this.playingListeningAgainForPart2(question, index, indexSubAnswer)}>
                                                  <div style={{padding: '0 5px', display:'inline-flex'}}>
                                                    {checkPlayingP2 === (index * 3 + indexSubAnswer) ?

                                                      <Icon.Volume1 size={12} style={{marginTop:'2.5px', marginRight:'2px'}} />
                                                      :
                                                      <Icon.Play size={11} style={{marginTop:'2.5px', marginRight:'2px'}}/>
                                                    }
                                                    <p style={{marginTop:'-2px'}}><FormattedMessage id="listenAgain" tagName="data" /></p>

                                                  </div>

                                                </Button>
                                              </FormGroup>

                                              {/*/!*phần dịch câu đáp án*!/*/}
                                              {/*<button style={{border: 'none', backgroundColor: 'white', padding: '0', marginLeft: '2%'}}*/}
                                                      {/*id={"a" + question.stt + indexSubAnswer} className="mr-1 mb-1">*/}
                                                {/*<Icon.AlertCircle style={{verticalAlign: 'unset'}}  size={20} />*/}
                                              {/*</button>*/}
                                              {/*<UncontrolledPopover trigger="focus" placement="bottom" target={"a" + question.stt + indexSubAnswer}>*/}
                                                {/*<PopoverBody style={{borderColor: 'green !important',*/}
                                                  {/*color: 'white', backgroundColor: 'green', borderRadius: '5px'}}>*/}
                                                  {/*<FormattedMessage id="translate" />: {indexSubAnswer===0 ? question.translatingQuesA : (indexSubAnswer===1 ? question.translatingQuesB : (indexSubAnswer===2 ? question.translatingQuesC : question.translatingQuesD))}*/}
                                                {/*</PopoverBody>*/}
                                              {/*</UncontrolledPopover>*/}
                                              {/*/!*phần dịch câu đáp án*!/*/}


                                              {/*<div style={{marginLeft: '3%'}}>*/}
                                                {/*<button className={classes.btnRepeat}*/}
                                                        {/*onClick={()=>this.playingListeningAgainForPart2(question, index, indexSubAnswer)}>*/}
                                                  {/*<div style={{display: 'flex'}}>*/}
                                                    {/*{*/}
                                                      {/*checkPlayingP2 === (index * 3 + indexSubAnswer) ?*/}
                                                        {/*<Icon.Volume1 size={12} className={classes.iconPlay} />*/}
                                                        {/*:*/}
                                                        {/*<Icon.Play className={classes.iconPlay} size={11}/>*/}
                                                    {/*}*/}
                                                    {/*<FormattedMessage id="listenAgain" tagName="data" />*/}
                                                  {/*</div>*/}

                                                {/*</button>*/}

                                              {/*</div>*/}

                                            </Col>
                                          </Col>
                                        </>
                                      ) : null }
                                    </Col>
                                    <Col xs={6}>

                                    </Col>
                                  </Row>
                                  <Row>
                                    <Col xs="12" md="12" sm="12" lg="12" style={{marginBottom: '10%'}}>
                                      <fieldset className={classes.fieldsetDescription}>
                                        <legend className={classes.legendDescription}><FormattedMessage id="description" /></legend>
                                        <i>
                                          {
                                            null === question.description ?
                                              <><FormattedMessage id={"no.explanation"}/></>
                                              :
                                              question.description.split("\n").map((des, i) =>
                                                <p>
                                                  {i === 0 ? <>(A) {des}</> : null}
                                                  {i === 1 ? <>(B) {des}</> : null}
                                                  {i === 2 ? <>(C) {des}</> : null}
                                                  {i === 3 ? <>(D) {des}</> : null}
                                                </p>
                                              )
                                          }
                                        </i>
                                      </fieldset>
                                    </Col>
                                  </Row>
                                </>
                              )}
                            </>
                          </div>
                          {/*--------------------------------------end PART2----------------------------------------*/}



                          {/*-----------------------------------------PART3------------------------------------------*/}
                          <div className="mt-3">
                            <h1 id="PART3" style={{fontWeight: '700'}}>PART 3</h1>

                            {listQuestionAndAnswerMinitest[2].listCategoryMinitestDTOS.map((category, indexCategory) =>
                              <>
                                {null === category.pathFile2 ?
                                  <>
                                    <h4 style={{fontWeight: '700', marginTop: '5%'}}>3.{indexCategory+1}.</h4>

                                    {category.listQuestionMinitestDTOS.map((question, indexQuestion) =>
                                      <>
                                        <Row id={String(question.stt)}  style={{marginTop:'3%', marginLeft: '10%'}}>
                                          <Col xs="6" md="6" sm="6" lg="6">
                                            {/*<h4 style={{marginBottom: '0'}}>*/}
                                              {/*{question.stt}. {question.name}*/}

                                              {/*/!*phần dịch câu hỏi*!/*/}
                                              {/*<button style={{border: 'none', backgroundColor: 'white', padding: '0', marginLeft: '3px'}}*/}
                                                      {/*id={"q" + question.stt} className="mr-1 mb-1">*/}
                                                {/*<Icon.AlertCircle  size={20} />*/}
                                              {/*</button>*/}
                                              {/*<UncontrolledPopover trigger="focus" placement="bottom" target={"q" + question.stt}*/}

                                              {/*>*/}
                                                {/*<PopoverBody style={{borderColor: 'green !important',*/}
                                                  {/*color: 'white', backgroundColor: 'green', borderRadius: '5px'}}>*/}
                                                  {/*<FormattedMessage id="translate" />: {question.translatingQuestion !== null ? question.translatingQuestion : "chưa có phần dịch"}*/}
                                                {/*</PopoverBody>*/}
                                              {/*</UncontrolledPopover>*/}
                                              {/*/!*end phần dịch câu hỏi*!/*/}
                                            {/*</h4>*/}
                                            {/*<button className={classes.btnRepeat}*/}
                                                    {/*onClick={()=>this.playingListeningAgainForPart3(indexCategory, indexQuestion)}>*/}
                                              {/*{*/}
                                                {/*checkPlayingP3 === (indexCategory * 3 + indexQuestion) ?*/}
                                                  {/*<Icon.Volume1 size={12} className={classes.iconPlay} />*/}
                                                  {/*:*/}
                                                  {/*<Icon.Play className={classes.iconPlay} size={11}/>*/}
                                              {/*}*/}
                                              {/*<FormattedMessage id="listenAgain" tagName="data" />*/}
                                            {/*</button>*/}

                                            <h5 style={{fontWeight:'700'}}>{question.stt}. {question.name}
                                              {/*phần dịch câu hỏi*/}
                                              <button style={{border: 'none', backgroundColor: 'white', padding: '0', marginLeft: '2%'}}
                                                      id={"q" + question.stt} className="mr-1 mb-1">
                                                <Icon.AlertCircle  size={20} />
                                              </button>
                                              <UncontrolledPopover trigger="focus" placement="bottom" target={"q" + question.stt}
                                              >
                                                <PopoverBody style={{borderColor: 'green !important',
                                                  color: 'white', backgroundColor: 'green', borderRadius: '5px'}}>
                                                  <FormattedMessage id="translate" />: {question.translatingQuestion !== null ? question.translatingQuestion : "chưa có phần dịch"}
                                                </PopoverBody>
                                              </UncontrolledPopover>
                                              {/*end phần dịch câu hỏi*/}

                                              <Button outline className={classes.buttonListenAgain}
                                                      style={{padding: '0', display:'inline-flex', width:'auto'}} id='btnHighlightRepeat'
                                                      onClick={()=>this.playingListeningAgainForPart3(indexCategory, indexQuestion)}>
                                                <div style={{padding: '0 5px', display:'inline-flex'}}>
                                                  {checkPlayingP3 === (indexCategory * 3 + indexQuestion) ?

                                                    <Icon.Volume1 size={12} style={{marginTop:'2.5px', marginRight:'2px'}} />
                                                    :
                                                    <Icon.Play size={11} style={{marginTop:'2.5px', marginRight:'2px'}}/>
                                                  }
                                                  <p style={{marginTop:'-2px'}}><FormattedMessage id="listenAgain" tagName="data" /></p>

                                                </div>

                                              </Button>

                                              {/*<Button outline className={classes.buttonListenAgain} style={{padding: '0', display:'inline-flex'}} id='btnHighlightRepeat'*/}
                                                      {/*onClick={()=>this.playingListeningAgainForPart3(indexCategory, indexQuestion)}>*/}
                                                {/*{*/}
                                                  {/*checkPlayingP3 === (indexCategory * 3 + indexQuestion) ?*/}
                                                    {/*<Icon.Volume1 size={12} className={classes.iconPlay} />*/}
                                                    {/*:*/}
                                                    {/*<Icon.Play className={classes.iconPlay} size={11}/>*/}
                                                {/*}*/}
                                                {/*<p style={{marginTop:'-2px'}}><FormattedMessage id="listenAgain" tagName="data" /></p>*/}

                                              {/*</Button>*/}
                                            </h5>

                                          </Col>
                                          <Col xs="6" md="6" sm="6" lg="6">
                                            {this.cutAnswerToChoose(question.answersToChoose).map((subAnswer, indexSubAnswer) =>
                                              <>
                                                <Col xs="12" md="12" sm="12" lg="12" style={{display: 'flex', padding: '0%'}}>
                                                  <Col xs="1" md="2" sm="1" lg="1" style={{padding: '0%'}}>
                                                    {indexSubAnswer === question.indexCorrectAnswer ?
                                                      <Icon.Check size={25} style={{color: 'green'}}/> : null
                                                    }
                                                    {indexSubAnswer === question.indexIncorrectAnswer ?
                                                      <Icon.X size={25} style={{color: '#b21717'}} /> : null
                                                    }
                                                    {/*<Icon.Check size={25} style={{color: 'green'}}/>*/}
                                                  </Col>
                                                  <Col xs="11" md="10" sm="11" lg="11" style={{padding: '0%', display: 'flex'}}>
                                                    <FormGroup check style={{marginBottom:'3%'}}>
                                                      <Label  check className={classes.labelRadioButton} style={{cursor: 'pointer',
                                                        fontSize: '120%', fontWeight: '500'}}>
                                                        <Input type="radio" name={question.stt}
                                                               checked={this.checkingAnswerChoosen(question.indexIncorrectAnswer,
                                                                 question.indexCorrectAnswer, indexSubAnswer)}
                                                        />
                                                        ({ indexSubAnswer===0 ? "A" : (indexSubAnswer===1 ? "B" : (indexSubAnswer===2 ? "C" : "D")) }) {this.upperCaseFirstLetter(subAnswer)}
                                                      </Label>
                                                      {/*phần dịch câu đáp án*/}
                                                      <button style={{border: 'none', backgroundColor: 'white', padding: '0', marginLeft: '3px'}}
                                                              id={"a" + question.stt + indexSubAnswer} className="mr-1 mb-1">
                                                        <Icon.AlertCircle  size={20} />
                                                      </button>
                                                      <UncontrolledPopover trigger="focus" placement="bottom" target={"a" + question.stt + indexSubAnswer}

                                                      >
                                                        <PopoverBody style={{borderColor: 'green !important',
                                                          color: 'white', backgroundColor: 'green', borderRadius: '5px'}}>
                                                          <FormattedMessage id="translate" />: {indexSubAnswer===0 ? question.translatingQuesA : (indexSubAnswer===1 ? question.translatingQuesB : (indexSubAnswer===2 ? question.translatingQuesC : question.translatingQuesD))}
                                                        </PopoverBody>
                                                      </UncontrolledPopover>
                                                      {/*end phần dịch câu đáp án*/}
                                                    </FormGroup>


                                                  </Col>
                                                </Col>
                                              </>
                                            )}
                                          </Col>
                                        </Row>
                                        <Row style={{marginLeft: '10%', marginRight: '10%'}}>
                                          <Col xs="12" md="12" sm="12" lg="12" style={{marginBottom: '10%'}}>
                                            <fieldset className={classes.fieldsetDescription}>
                                              <legend className={classes.legendDescription}><FormattedMessage id="description" /></legend>
                                              <i>{null === question.description ? <><FormattedMessage id={"no.explanation"}/> GÌ HẾT :))</> : question.description}</i>
                                            </fieldset>
                                          </Col>
                                        </Row>
                                      </>
                                    )}

                                  </>
                                  :
                                  <>
                                    <h4 style={{fontWeight: '700', marginTop: '5%'}}>3.{indexCategory+1}.</h4>
                                    <Row style={{marginTop:'3%', marginLeft: '10%'}}>
                                      <Col xs="6" md="6" sm="6" lg="6">
                                        <img className={classes.imgClass} style={{width: '100%', border: '5px solid lightgray'}}
                                             src={category.pathFile2}/>
                                      </Col>
                                      <Col xs="6" md="6" sm="6" lg="6">
                                        {category.listQuestionMinitestDTOS.map((question, indexQuestion) =>
                                          <>
                                            <Col id={String(question.stt)}  xs="12" md="12" sm="12" lg="12">

                                              <h5 style={{fontWeight:'700'}}>{question.stt}. {question.name}
                                                {/*phần dịch câu hỏi*/}
                                                <button style={{border: 'none', backgroundColor: 'white', padding: '0', marginLeft: '2%'}}
                                                        id={"q" + question.stt} className="mr-1 mb-1">
                                                  <Icon.AlertCircle  size={20} />
                                                </button>
                                                <UncontrolledPopover trigger="focus" placement="bottom" target={"q" + question.stt}
                                                >
                                                  <PopoverBody style={{borderColor: 'green !important',
                                                    color: 'white', backgroundColor: 'green', borderRadius: '5px'}}>
                                                    <FormattedMessage id="translate" />: {question.translatingQuestion !== null ? question.translatingQuestion : "chưa có phần dịch"}
                                                  </PopoverBody>
                                                </UncontrolledPopover>
                                                {/*end phần dịch câu hỏi*/}

                                                <Button outline className={classes.buttonListenAgain}
                                                        style={{padding: '0', display:'inline-flex',  width:'auto'}} id='btnHighlightRepeat'
                                                        onClick={()=>this.playingListeningAgainForPart3(indexCategory, indexQuestion)}>
                                                  <div style={{padding: '0 5px', display:'inline-flex'}}>
                                                    {checkPlayingP3 === (indexCategory * 3 + indexQuestion) ?

                                                      <Icon.Volume1 size={12} style={{marginTop:'2.5px', marginRight:'2px'}} />
                                                      :
                                                      <Icon.Play size={11} style={{marginTop:'2.5px', marginRight:'2px'}}/>
                                                    }
                                                    <p style={{marginTop:'-2px'}}><FormattedMessage id="listenAgain" tagName="data" /></p>

                                                  </div>

                                                </Button>

                                                {/*<Button outline className={classes.buttonListenAgain} style={{padding: '0', display:'inline-flex'}} id='btnHighlightRepeat'*/}
                                                        {/*onClick={()=>this.playingListeningAgainForPart3(indexCategory, indexQuestion)}>*/}
                                                  {/*{*/}
                                                    {/*checkPlayingP3 === (indexCategory * 3 + indexQuestion) ?*/}
                                                      {/*<Icon.Volume1 size={12} className={classes.iconPlay} />*/}
                                                      {/*:*/}
                                                      {/*<Icon.Play className={classes.iconPlay} size={11}/>*/}
                                                  {/*}*/}
                                                  {/*<p style={{marginTop:'-2px'}}><FormattedMessage id="listenAgain" tagName="data" /></p>*/}

                                                {/*</Button>*/}
                                              </h5>


                                              {this.cutAnswerToChoose(question.answersToChoose).map((subAnswer, indexSubAnswer) =>
                                                <>
                                                  <Col xs="12" md="12" sm="12" lg="12" style={{display: 'flex', padding: '0%'}}>
                                                    <Col xs="1" md="2" sm="1" lg="1" style={{padding: '0%'}}>
                                                      {indexSubAnswer === question.indexCorrectAnswer ?
                                                        <Icon.Check size={25} style={{color: 'green'}}/> : null
                                                      }
                                                      {indexSubAnswer === question.indexIncorrectAnswer ?
                                                        <Icon.X size={25} style={{color: '#b21717'}} /> : null
                                                      }
                                                      {/*<Icon.Check size={25} style={{color: 'green'}}/>*/}
                                                    </Col>
                                                    <Col xs="11" md="10" sm="11" lg="11" style={{padding: '0%', display: 'flex'}}>
                                                      <FormGroup check style={{marginBottom:'3%'}}>
                                                        <Label  check className={classes.labelRadioButton} style={{cursor: 'pointer',
                                                          fontSize: '120%', fontWeight: '500'}}>
                                                          <Input type="radio" name={question.stt}
                                                                 checked={this.checkingAnswerChoosen(question.indexIncorrectAnswer,
                                                                   question.indexCorrectAnswer, indexSubAnswer)}
                                                          />
                                                          ({ indexSubAnswer===0 ? "A" : (indexSubAnswer===1 ? "B" : (indexSubAnswer===2 ? "C" : "D")) }) {this.upperCaseFirstLetter(subAnswer)}
                                                        </Label>
                                                        {/*phần dịch câu đáp án*/}
                                                        <button style={{border: 'none', backgroundColor: 'white', padding: '0', marginLeft: '3px'}}
                                                                id={"a" + question.stt + indexSubAnswer} className="mr-1 mb-1">
                                                          <Icon.AlertCircle  size={20} />
                                                        </button>
                                                        <UncontrolledPopover trigger="focus" placement="bottom" target={"a" + question.stt + indexSubAnswer}

                                                        >
                                                          <PopoverBody style={{borderColor: 'green !important',
                                                            color: 'white', backgroundColor: 'green', borderRadius: '5px'}}>
                                                            <FormattedMessage id="translate" />: {indexSubAnswer===0 ? question.translatingQuesA : (indexSubAnswer===1 ? question.translatingQuesB : (indexSubAnswer===2 ? question.translatingQuesC : question.translatingQuesD))}
                                                          </PopoverBody>
                                                        </UncontrolledPopover>
                                                        {/*phần dịch câu đáp án*/}
                                                      </FormGroup>

                                                      {/*<div style={{marginLeft: '3%'}}>*/}
                                                        {/*{indexSubAnswer === question.indexCorrectAnswer ?*/}
                                                          {/*<button className={classes.btnRepeat}*/}
                                                                  {/*onClick={()=>this.playingListeningAgain(question, indexCategory+16, indexSubAnswer)}>*/}
                                                            {/*<Icon.Play className={classes.iconPlay} size={11}/>*/}
                                                            {/*Nghe lại*/}
                                                          {/*</button> : null*/}
                                                        {/*}*/}
                                                      {/*</div>*/}
                                                    </Col>
                                                  </Col>
                                                </>
                                              )}
                                            </Col>
                                            <Col xs="12" md="12" sm="12" lg="12" style={{marginBottom: '10%'}}>
                                              <fieldset className={classes.fieldsetDescription}>
                                                <legend className={classes.legendDescription}><FormattedMessage id="description" /></legend>
                                                <i>{null === question.description ? <><FormattedMessage id={"no.explanation"}/> GÌ HẾT :))</> : question.description}</i>
                                              </fieldset>
                                            </Col>
                                          </>
                                        )}
                                      </Col>
                                    </Row>
                                  </>
                                }
                              </>
                            )}
                          </div>
                          {/*-------------------------------------------end PART3---------------------------------------*/}


                          {/*--------------------------------------------PART4--------------------------------------------*/}
                          <div className="mt-3">
                            <h1 id="PART4" style={{fontWeight: '700'}}>PART 4</h1>

                            {listQuestionAndAnswerMinitest[3].listCategoryMinitestDTOS.map((category, indexCategory) =>
                              <>
                                {null === category.pathFile2 ?
                                  <>
                                    <h4 style={{fontWeight: '700', marginTop: '5%'}}>4.{indexCategory + 1}.</h4>

                                    {category.listQuestionMinitestDTOS.map((question, indexQuestion) =>
                                      <>
                                        <Row id={String(question.stt)}  style={{marginTop:'3%', marginLeft: '10%'}}>
                                          <Col xs="6" md="6" sm="6" lg="6">

                                            <h5 style={{fontWeight:'700'}}>{question.stt}. {question.name}
                                              {/*phần dịch câu hỏi*/}
                                              <button style={{border: 'none', backgroundColor: 'white', padding: '0', marginLeft: '2%'}}
                                                      id={"q" + question.stt} className="mr-1 mb-1">
                                                <Icon.AlertCircle  size={20} />
                                              </button>
                                              <UncontrolledPopover trigger="focus" placement="bottom" target={"q" + question.stt}
                                              >
                                                <PopoverBody style={{borderColor: 'green !important',
                                                  color: 'white', backgroundColor: 'green', borderRadius: '5px'}}>
                                                  <FormattedMessage id="translate" />: {question.translatingQuestion !== null ? question.translatingQuestion : "chưa có phần dịch"}
                                                </PopoverBody>
                                              </UncontrolledPopover>
                                              {/*end phần dịch câu hỏi*/}

                                              <Button outline className={classes.buttonListenAgain}
                                                      style={{padding: '0', display:'inline-flex',  width:'auto'}} id='btnHighlightRepeat'
                                                      onClick={()=>this.playingListeningAgainForPart4(indexCategory, indexQuestion)}>
                                                <div style={{padding: '0 5px', display:'inline-flex'}}>
                                                  {checkPlayingP4 === (indexCategory * 3 + indexQuestion) ?

                                                    <Icon.Volume1 size={12} style={{marginTop:'2.5px', marginRight:'2px'}} />
                                                    :
                                                    <Icon.Play size={11} style={{marginTop:'2.5px', marginRight:'2px'}}/>
                                                  }
                                                  <p style={{marginTop:'-2px'}}><FormattedMessage id="listenAgain" tagName="data" /></p>

                                                </div>
                                              </Button>

                                              {/*<Button outline className={classes.buttonListenAgain} style={{padding: '0', display:'inline-flex'}} id='btnHighlightRepeat'*/}
                                                      {/*onClick={()=>this.playingListeningAgainForPart4(indexCategory, indexQuestion)}>*/}
                                                {/*{*/}
                                                  {/*checkPlayingP4 === (indexCategory * 3 + indexQuestion) ?*/}
                                                    {/*<Icon.Volume1 size={12} className={classes.iconPlay} />*/}
                                                    {/*:*/}
                                                    {/*<Icon.Play className={classes.iconPlay} size={11}/>*/}
                                                {/*}*/}
                                                {/*<p style={{marginTop:'-2px'}}><FormattedMessage id="listenAgain" tagName="data" /></p>*/}

                                              {/*</Button>*/}
                                            </h5>




                                          </Col>
                                          <Col xs="6" md="6" sm="6" lg="6">
                                            {this.cutAnswerToChoose(question.answersToChoose).map((subAnswer, indexSubAnswer) =>
                                              <>
                                                <Col xs="12" md="12" sm="12" lg="12" style={{display: 'flex', padding: '0%'}}>
                                                  <Col xs="1" md="2" sm="1" lg="1" style={{padding: '0%'}}>
                                                    {indexSubAnswer === question.indexCorrectAnswer ?
                                                      <Icon.Check size={25} style={{color: 'green'}}/> : null
                                                    }
                                                    {indexSubAnswer === question.indexIncorrectAnswer ?
                                                      <Icon.X size={25} style={{color: '#b21717'}} /> : null
                                                    }
                                                    {/*<Icon.Check size={25} style={{color: 'green'}}/>*/}
                                                  </Col>
                                                  <Col xs="11" md="10" sm="11" lg="11" style={{padding: '0%', display: 'flex'}}>
                                                    <FormGroup check style={{marginBottom:'3%'}}>
                                                      <Label  check className={classes.labelRadioButton} style={{cursor: 'pointer',
                                                        fontSize: '120%', fontWeight: '500'}}>
                                                        <Input type="radio" name={question.stt}
                                                               checked={this.checkingAnswerChoosen(question.indexIncorrectAnswer,
                                                                 question.indexCorrectAnswer, indexSubAnswer)}
                                                        />
                                                        ({ indexSubAnswer===0 ? "A" : (indexSubAnswer===1 ? "B" : (indexSubAnswer===2 ? "C" : "D")) }) {this.upperCaseFirstLetter(subAnswer)}
                                                      </Label>
                                                      {/*phần dịch câu đáp án*/}
                                                      <button style={{border: 'none', backgroundColor: 'white', padding: '0', marginLeft: '3px'}}
                                                              id={"a" + question.stt + indexSubAnswer} className="mr-1 mb-1">
                                                        <Icon.AlertCircle  size={20} />
                                                      </button>
                                                      <UncontrolledPopover trigger="focus" placement="bottom" target={"a" + question.stt + indexSubAnswer}

                                                      >
                                                        <PopoverBody style={{borderColor: 'green !important',
                                                          color: 'white', backgroundColor: 'green', borderRadius: '5px'}}>
                                                          <FormattedMessage id="translate" />: {indexSubAnswer===0 ? question.translatingQuesA : (indexSubAnswer===1 ? question.translatingQuesB : (indexSubAnswer===2 ? question.translatingQuesC : question.translatingQuesD))}
                                                        </PopoverBody>
                                                      </UncontrolledPopover>
                                                      {/*phần dịch câu đáp án*/}
                                                    </FormGroup>

                                                    {/*<div style={{marginLeft: '3%'}}>*/}
                                                      {/*{indexSubAnswer === question.indexCorrectAnswer ?*/}
                                                        {/*<button className={classes.btnRepeat}*/}
                                                                {/*onClick={()=>this.playingListeningAgain(question, indexCategory+21, indexSubAnswer)}>*/}
                                                          {/*<Icon.Play className={classes.iconPlay} size={11}/>*/}
                                                          {/*Nghe lại*/}
                                                        {/*</button> : null*/}
                                                      {/*}*/}
                                                    {/*</div>*/}
                                                  </Col>
                                                </Col>
                                              </>
                                            )}
                                          </Col>
                                        </Row>
                                        <Row style={{marginLeft: '10%', marginRight: '10%'}}>
                                          <Col xs="12" md="12" sm="12" lg="12" style={{marginBottom: '10%'}}>
                                            <fieldset className={classes.fieldsetDescription}>
                                              <legend className={classes.legendDescription}><FormattedMessage id="description" /></legend>
                                              <i>{null === question.description ? <><FormattedMessage id={"no.explanation"}/> GÌ HẾT :))</> : question.description}</i>
                                            </fieldset>
                                          </Col>
                                        </Row>
                                      </>
                                    )}

                                  </>
                                  :
                                  <>
                                    <h4 style={{fontWeight: '700', marginTop: '5%'}}>4.{indexCategory+1}.</h4>
                                    <Row style={{marginTop:'3%', marginLeft: '10%'}}>
                                      <Col xs="6" md="6" sm="6" lg="6">
                                        <img className={classes.imgClass} style={{width: '100%', border: '5px solid lightgray'}}
                                             src={category.pathFile2}/>
                                      </Col>
                                      <Col xs="6" md="6" sm="6" lg="6">
                                        {category.listQuestionMinitestDTOS.map((question, indexQuestion) =>
                                          <>
                                            <Col id={String(question.stt)}  xs="12" md="12" sm="12" lg="12">

                                              <h5 style={{fontWeight:'700'}}>{question.stt}. {question.name}
                                                {/*phần dịch câu hỏi*/}
                                                <button style={{border: 'none', backgroundColor: 'white', padding: '0', marginLeft: '2%'}}
                                                        id={"q" + question.stt} className="mr-1 mb-1">
                                                  <Icon.AlertCircle  size={20} />
                                                </button>
                                                <UncontrolledPopover trigger="focus" placement="bottom" target={"q" + question.stt}
                                                >
                                                  <PopoverBody style={{borderColor: 'green !important',
                                                    color: 'white', backgroundColor: 'green', borderRadius: '5px'}}>
                                                    <FormattedMessage id="translate" />: {question.translatingQuestion !== null ? question.translatingQuestion : "chưa có phần dịch"}
                                                  </PopoverBody>
                                                </UncontrolledPopover>
                                                {/*end phần dịch câu hỏi*/}

                                                <Button outline className={classes.buttonListenAgain}
                                                        style={{padding: '0', display:'inline-flex',  width:'auto'}} id='btnHighlightRepeat'
                                                        onClick={()=>this.playingListeningAgainForPart4(indexCategory, indexQuestion)}>
                                                  <div style={{padding: '0 5px', display:'inline-flex'}}>
                                                    {checkPlayingP4 === (indexCategory * 3 + indexQuestion) ?

                                                      <Icon.Volume1 size={12} style={{marginTop:'2.5px', marginRight:'2px'}} />
                                                      :
                                                      <Icon.Play size={11} style={{marginTop:'2.5px', marginRight:'2px'}}/>
                                                    }
                                                    <p style={{marginTop:'-2px'}}><FormattedMessage id="listenAgain" tagName="data" /></p>

                                                  </div>
                                                </Button>


                                                {/*<Button outline className={classes.buttonListenAgain} style={{padding: '0', display:'inline-flex'}} id='btnHighlightRepeat'*/}
                                                        {/*onClick={()=>this.playingListeningAgainForPart4(indexCategory, indexQuestion)}>*/}
                                                  {/*{*/}
                                                    {/*checkPlayingP4 === (indexCategory * 3 + indexQuestion) ?*/}
                                                      {/*<Icon.Volume1 size={12} className={classes.iconPlay} />*/}
                                                      {/*:*/}
                                                      {/*<Icon.Play className={classes.iconPlay} size={11}/>*/}
                                                  {/*}*/}
                                                  {/*<p style={{marginTop:'-2px'}}><FormattedMessage id="listenAgain" tagName="data" /></p>*/}

                                                {/*</Button>*/}
                                              </h5>

                                              {this.cutAnswerToChoose(question.answersToChoose).map((subAnswer, indexSubAnswer) =>
                                                <>
                                                  <Col xs="12" md="12" sm="12" lg="12" style={{display: 'flex', padding: '0%'}}>
                                                    <Col xs="1" md="2" sm="1" lg="1" style={{padding: '0%'}}>
                                                      {indexSubAnswer === question.indexCorrectAnswer ?
                                                        <Icon.Check size={25} style={{color: 'green'}}/> : null
                                                      }
                                                      {indexSubAnswer === question.indexIncorrectAnswer ?
                                                        <Icon.X size={25} style={{color: '#b21717'}} /> : null
                                                      }
                                                      {/*<Icon.Check size={25} style={{color: 'green'}}/>*/}
                                                    </Col>
                                                    <Col xs="11" md="10" sm="11" lg="11" style={{padding: '0%', display: 'flex'}}>
                                                      <FormGroup check style={{marginBottom:'3%'}}>
                                                        <Label  check className={classes.labelRadioButton} style={{cursor: 'pointer',
                                                          fontSize: '120%', fontWeight: '500'}}>
                                                          <Input type="radio" name={question.stt}
                                                                 checked={this.checkingAnswerChoosen(question.indexIncorrectAnswer,
                                                                   question.indexCorrectAnswer, indexSubAnswer)}
                                                          />
                                                          ({ indexSubAnswer===0 ? "A" : (indexSubAnswer===1 ? "B" : (indexSubAnswer===2 ? "C" : "D")) }) {this.upperCaseFirstLetter(subAnswer)}
                                                        </Label>
                                                        {/*phần dịch câu đáp án*/}
                                                        <button style={{border: 'none', backgroundColor: 'white', padding: '0', marginLeft: '3px'}}
                                                                id={"a" + question.stt + indexSubAnswer} className="mr-1 mb-1">
                                                          <Icon.AlertCircle  size={20} />
                                                        </button>
                                                        <UncontrolledPopover trigger="focus" placement="bottom" target={"a" + question.stt + indexSubAnswer}

                                                        >
                                                          <PopoverBody style={{borderColor: 'green !important',
                                                            color: 'white', backgroundColor: 'green', borderRadius: '5px'}}>
                                                            <FormattedMessage id="translate" />: {indexSubAnswer===0 ? question.translatingQuesA : (indexSubAnswer===1 ? question.translatingQuesB : (indexSubAnswer===2 ? question.translatingQuesC : question.translatingQuesD))}
                                                          </PopoverBody>
                                                        </UncontrolledPopover>
                                                        {/*phần dịch câu đáp án*/}
                                                      </FormGroup>

                                                    </Col>
                                                  </Col>
                                                </>
                                              )}
                                            </Col>
                                            <Col xs="12" md="12" sm="12" lg="12" style={{marginBottom: '10%'}}>
                                              <fieldset className={classes.fieldsetDescription}>
                                                <legend className={classes.legendDescription}><FormattedMessage id="description" /></legend>
                                                <i>{null === question.description ? <><FormattedMessage id={"no.explanation"}/> GÌ HẾT :))</> : question.description}</i>
                                              </fieldset>
                                            </Col>
                                          </>
                                        )}
                                      </Col>
                                    </Row>
                                  </>
                                }
                              </>
                            )}
                          </div>
                          {/*-------------------------------------------- end PART4--------------------------------------------*/}
                        </> : null
                      }
                    </>
                  }
                </div>
              </Row>
            </div>
          </div>
          :
          <div className="mt-2">
            {loadingQuestion !== undefined ?
              <>{loadingQuestion === false ? <>{this.loadingComponent}</> : ""}</> : null
            }
            <Badge style={{backgroundColor: "rgba(32 171 231)", width: "100%", height: "60%", marginLeft: "0%",
              padding: "10px 0 0 0", borderRadius:'10px'}} className="badge-xl block">
              <div>
                <div className="text-left" style={{marginLeft: '0px'}}>
                  <div className={classes.titleModal}>
                    <h3 style={{color:'white', marginLeft: '2%', top: '40%', transform: 'translateY(-50%)', position: 'absolute'}}>
                      Listening Test
                    </h3>
                  </div>
                </div>
              </div>
            </Badge>
            <div style={{position: 'relative', margin: '0 1%'}}>
              {/*{this.state.load === true ? <>{loadingComponent}</> : ""}*/}
              <Row className={classes.bodyModal} style={{margin: '0 0% 0 0%', padding: '0 0%', width: "100%" }}>
                <div style={{margin:'3% 3%'}}>
                  {
                    listQuestionAndAnswerMinitest.length > 0 ? null :
                      <h5>In the Listening test, you will be asked to demonstrate how well you understand spoken English. The entire Listening test

                        will last approximately 45 minutes. There are four parts, and directions are given for each part. You must mark your

                        answers on the separate answer sheet. Do not write your answer in your test book.</h5>
                  }
                  <div>
                    <>
                      {listQuestionAndAnswerMinitest.length > 0 ?
                        <>
                          {listQuestionAndAnswerMinitest[0].listCategoryMinitestDTOS[0].listQuestionMinitestDTOS.map((question, indexQuestion) =>
                            <>
                              {/*file nghe câu hỏi*/}
                              {this.state.listPlayingAnswer[indexQuestion].playing === true ?
                                <ReactPlayerReading
                                  ref={this.ref}
                                  url = {question ? question.pathFile1 : ""}
                                  playing={this.state.listPlayingAnswer[indexQuestion].playing}
                                  controls={this.state.listPlayingAnswer[indexQuestion].playing}
                                  muted={!this.state.listPlayingAnswer[indexQuestion].playing}
                                  onEnded={() => this.onEndingForAnswer(indexQuestion)}
                                  // onPlay={this.onPlaying}
                                  // onPause={this.onPausing}
                                  config={{
                                    file: {
                                      attributes: {
                                        controlsList: 'nodownload'
                                      }
                                    }
                                  }}
                                  width="320px"
                                  height="100px"
                                >
                                </ReactPlayerReading> : null
                              }

                              {/*file nghe lại câu A*/}
                              {this.state.listPlayingAgainPart1[indexQuestion * 4].playing === true ?
                                <>
                                  <ReactPlayerReading
                                    ref={this.ref}
                                    url = {question ? question.pathFileQuesA : ""}
                                    playing={this.state.listPlayingAgainPart1[indexQuestion * 4].playing}
                                    controls={true}
                                    onEnded={() => this.onEndingListeningAgainPart()}
                                    config={{
                                      file: {
                                        attributes: {
                                          controlsList: 'nodownload'
                                        }
                                      }
                                    }}
                                    width="320px"
                                    height="100px"
                                  >
                                  </ReactPlayerReading>
                                </> : null
                              }

                              {/*file nghe lại câu B*/}
                              {this.state.listPlayingAgainPart1[indexQuestion * 4 + 1].playing === true ?
                                <>
                                  <ReactPlayerReading
                                    ref={this.ref}
                                    url = {question ? question.pathFileQuesB : ""}
                                    playing={this.state.listPlayingAgainPart1[indexQuestion * 4 + 1].playing}
                                    controls={true}
                                    onEnded={() => this.onEndingListeningAgainPart()}
                                    config={{
                                      file: {
                                        attributes: {
                                          controlsList: 'nodownload'
                                        }
                                      }
                                    }}
                                    width="320px"
                                    height="100px"
                                  >
                                  </ReactPlayerReading>
                                </> : null
                              }

                              {/*file nghe lại câu C*/}
                              {this.state.listPlayingAgainPart1[indexQuestion * 4 + 2].playing === true ?
                                <>
                                  <ReactPlayerReading
                                    ref={this.ref}
                                    url = {question ? question.pathFileQuesC : ""}
                                    playing={this.state.listPlayingAgainPart1[indexQuestion * 4 + 2].playing}
                                    controls={true}
                                    onEnded={() => this.onEndingListeningAgainPart()}
                                    config={{
                                      file: {
                                        attributes: {
                                          controlsList: 'nodownload'
                                        }
                                      }
                                    }}
                                    width="320px"
                                    height="100px"
                                  >
                                  </ReactPlayerReading>
                                </> : null
                              }

                              {/*file nghe lại câu D*/}
                              {this.state.listPlayingAgainPart1[indexQuestion * 4 + 3].playing === true ?
                                <>
                                  <ReactPlayerReading
                                    ref={this.ref}
                                    url = {question ? question.pathFileQuesD : ""}
                                    playing={this.state.listPlayingAgainPart1[indexQuestion * 4 + 3].playing}
                                    controls={true}
                                    onEnded={() => this.onEndingListeningAgainPart()}
                                    config={{
                                      file: {
                                        attributes: {
                                          controlsList: 'nodownload'
                                        }
                                      }
                                    }}
                                    width="320px"
                                    height="100px"
                                  >
                                  </ReactPlayerReading>
                                </> : null
                              }

                            </>
                          )}
                          {listQuestionAndAnswerMinitest[1].listCategoryMinitestDTOS[0].listQuestionMinitestDTOS.map((question, indexQuestion) =>
                            <>
                              {this.state.listPlayingAnswer[indexQuestion+6].playing === true ?
                                <ReactPlayerReading
                                  ref={this.ref}
                                  url = {question ? question.pathFile1 : ""}
                                  // url="https://s3-ap-southeast-1.amazonaws.com/cnd.migitek.storage/storage/assest/filesOfCategory\De_bai_ID_304\audio\audio3.mp3"
                                  playing={this.state.listPlayingAnswer[indexQuestion+6].playing}
                                  controls={this.state.listPlayingAnswer[indexQuestion+6].playing}
                                  muted={!this.state.listPlayingAnswer[indexQuestion+6].playing}
                                  onEnded={() => this.onEndingForAnswer(indexQuestion+6)}
                                  // onPlay={this.onPlaying}
                                  // onPause={this.onPausing}
                                  config={{
                                    file: {
                                      attributes: {
                                        controlsList: 'nodownload'
                                      }
                                    }
                                  }}
                                  width="320px"
                                  height="100px"
                                >
                                </ReactPlayerReading> : null
                              }

                              {/*file nghe lại câu A part2*/}
                              {this.state.listPlayingAgainPart2[indexQuestion * 3].playing === true ?
                                <>
                                  <ReactPlayerReading
                                    ref={this.ref}
                                    url = {question ? question.pathFileQuesA : ""}
                                    playing={this.state.listPlayingAgainPart2[indexQuestion * 3].playing}
                                    controls={true}
                                    onEnded={() => this.onEndingListeningAgainPart()}
                                    config={{
                                      file: {
                                        attributes: {
                                          controlsList: 'nodownload'
                                        }
                                      }
                                    }}
                                    width="320px"
                                    height="100px"
                                  >
                                  </ReactPlayerReading>
                                </> : null
                              }

                              {/*file nghe lại câu B part2*/}
                              {this.state.listPlayingAgainPart2[indexQuestion * 3 + 1].playing === true ?
                                <>
                                  <ReactPlayerReading
                                    ref={this.ref}
                                    url = {question ? question.pathFileQuesB : ""}
                                    playing={this.state.listPlayingAgainPart2[indexQuestion * 3 + 1].playing}
                                    controls={true}
                                    onEnded={() => this.onEndingListeningAgainPart()}
                                    config={{
                                      file: {
                                        attributes: {
                                          controlsList: 'nodownload'
                                        }
                                      }
                                    }}
                                    width="320px"
                                    height="100px"
                                  >
                                  </ReactPlayerReading>
                                </> : null
                              }

                              {/*file nghe lại câu C part2*/}
                              {this.state.listPlayingAgainPart2[indexQuestion * 3 + 2].playing === true ?
                                <>
                                  <ReactPlayerReading
                                    ref={this.ref}
                                    url = {question ? question.pathFileQuesC : ""}
                                    playing={this.state.listPlayingAgainPart2[indexQuestion * 3 + 2].playing}
                                    controls={true}
                                    onEnded={() => this.onEndingListeningAgainPart()}
                                    config={{
                                      file: {
                                        attributes: {
                                          controlsList: 'nodownload'
                                        }
                                      }
                                    }}
                                    width="320px"
                                    height="100px"
                                  >
                                  </ReactPlayerReading>
                                </> : null
                              }

                            </>

                          )}
                          {listQuestionAndAnswerMinitest[2].listCategoryMinitestDTOS.map((category, indexCategory) =>
                            <>
                              {this.state.listPlayingAnswer[indexCategory+16].playing === true ?
                                <ReactPlayerReading
                                  ref={this.ref}
                                  url = {category ? category.pathFile1 : ""}
                                  // url="https://s3-ap-southeast-1.amazonaws.com/cnd.migitek.storage/storage/assest/filesOfCategory\De_bai_ID_304\audio\audio3.mp3"
                                  playing={this.state.listPlayingAnswer[indexCategory+16].playing}
                                  controls={this.state.listPlayingAnswer[indexCategory+16].playing}
                                  muted={!this.state.listPlayingAnswer[indexCategory+16].playing}
                                  onEnded={() => this.onEndingForAnswer(indexCategory+16)}
                                  // onPlay={this.onPlaying}
                                  // onPause={this.onPausing}
                                  config={{
                                    file: {
                                      attributes: {
                                        controlsList: 'nodownload'
                                      }
                                    }
                                  }}
                                  width="320px"
                                  height="100px"
                                >
                                </ReactPlayerReading> : null
                              }

                              {category.listQuestionMinitestDTOS.map((question, indexQuestion) =>
                                <>
                                  {/*nghe lại câu hỏi part3*/}
                                  {this.state.listPlayingAgainPart3[indexCategory * 3 + indexQuestion].playing === true ?
                                    <>
                                      <ReactPlayerReading
                                        ref={this.ref}
                                        url={question ? question.pathFileQues : ""}
                                        playing={this.state.listPlayingAgainPart3[indexCategory * 3 + indexQuestion].playing}
                                        controls={true}
                                        onEnded={() => this.onEndingListeningAgainPart(indexCategory * 3 + indexQuestion)}
                                        config={{
                                          file: {
                                            attributes: {
                                              controlsList: 'nodownload'
                                            }
                                          }
                                        }}
                                        width="320px"
                                        height="100px"
                                      >
                                      </ReactPlayerReading>
                                    </> : null
                                  }
                                </>
                              )}

                            </>
                          )}
                          {listQuestionAndAnswerMinitest[3].listCategoryMinitestDTOS.map((category, indexCategory) =>
                            <>
                              {this.state.listPlayingAnswer[indexCategory+21].playing === true ?
                                <ReactPlayerReading
                                  ref={this.ref}
                                  url = {category ? category.pathFile1 : ""}
                                  // url="https://s3-ap-southeast-1.amazonaws.com/cnd.migitek.storage/storage/assest/filesOfCategory\De_bai_ID_304\audio\audio3.mp3"
                                  playing={this.state.listPlayingAnswer[indexCategory+21].playing}
                                  controls={this.state.listPlayingAnswer[indexCategory+21].playing}
                                  muted={!this.state.listPlayingAnswer[indexCategory+21].playing}
                                  onSeek={this.state.secondListenAgain}
                                  onEnded={() => this.onEndingForAnswer(indexCategory+21)}
                                  // onPlay={this.onPlaying}
                                  // onPause={this.onPausing}
                                  config={{
                                    file: {
                                      attributes: {
                                        controlsList: 'nodownload'
                                      }
                                    }
                                  }}
                                  width="320px"
                                  height="100px"
                                >
                                </ReactPlayerReading> : null
                              }

                              {category.listQuestionMinitestDTOS.map((question, indexQuestion) =>
                                <>
                                  {/*nghe lại câu hỏi part4*/}
                                  {this.state.listPlayingAgainPart4[indexCategory * 3 + indexQuestion].playing === true ?
                                    <>
                                      <ReactPlayerReading
                                        ref={this.ref}
                                        url={question ? question.pathFileQues : ""}
                                        playing={this.state.listPlayingAgainPart4[indexCategory * 3 + indexQuestion].playing}
                                        controls={true}
                                        onEnded={() => this.onEndingListeningAgainPart(indexCategory * 3 + indexQuestion)}
                                        config={{
                                          file: {
                                            attributes: {
                                              controlsList: 'nodownload'
                                            }
                                          }
                                        }}
                                        width="320px"
                                        height="100px"
                                      >
                                      </ReactPlayerReading>
                                    </> : null
                                  }
                                </>
                              )}

                            </>
                          )}

                        </>
                        : null
                      }
                    </>
                  </div>

                  <>
                    {listQuestionAndAnswerMinitest.length > 0 ?
                      <>
                        {/*--------------------------------------PART1----------------------------------------*/}
                        <div>
                          <h1 id="PART1" style={{fontWeight: '700'}}>PART 1</h1>
                          <>
                            {listQuestionAndAnswerMinitest[0].listCategoryMinitestDTOS[0].listQuestionMinitestDTOS.map((question, index) =>
                              <>
                                <Row id={String(question.stt)}  style={{marginTop: '4%'}}>
                                  <Col xs={1}><h4>{question.stt}.</h4></Col>
                                  <Col xs="5">
                                    {question ? this.cutAnswerToChoose(question.answersToChoose).map((subAnswer, indexSubAnswer) =>
                                      <>
                                        <Col xs="12" md="12" sm="12" lg="12" style={{display: 'flex', padding: '0%'}}>
                                          <Col xs="1" md="2" sm="1" lg="1" style={{padding: '0%'}}>
                                            {indexSubAnswer === question.indexCorrectAnswer ?
                                              <Icon.Check size={25} style={{color: 'green'}}/> : null
                                            }
                                            {indexSubAnswer === question.indexIncorrectAnswer ?
                                              <Icon.X size={25} style={{color: '#b21717'}} /> : null
                                            }
                                            {/*<Icon.Check size={25} style={{color: 'green'}}/>*/}
                                          </Col>
                                          <Col xs="11" md="10" sm="11" lg="11" style={{padding: '0%', display: 'flex'}}>

                                            <FormGroup check style={{marginBottom:'3%', display: 'flex'}}>
                                              <Label  check className={classes.labelRadioButton} style={{cursor: 'pointer'}}>
                                                <Input type="radio" name={question.stt}
                                                       checked={this.checkingAnswerChoosen(question.indexIncorrectAnswer,
                                                         question.indexCorrectAnswer, indexSubAnswer)}
                                                />
                                                <h4>({ indexSubAnswer===0 ? "A" : (indexSubAnswer===1 ? "B" : (indexSubAnswer===2 ? "C" : "D")) })</h4>
                                              </Label>

                                              <Button outline className={classes.buttonListenAgain}
                                                      style={{padding: '0', display:'inline-flex', marginLeft:'10px', width:'auto'}} id='btnHighlightRepeat'
                                                      onClick={()=>this.playingListeningAgainForPart1(question, index, indexSubAnswer)}>
                                                <div style={{padding: '0 5px', display:'inline-flex'}}>
                                                  {checkPlayingP1 === (index * 4 + indexSubAnswer) ?

                                                    <Icon.Volume1 size={12} style={{marginTop:'2.5px', marginRight:'2px'}} />
                                                    :
                                                    <Icon.Play size={11} style={{marginTop:'2.5px', marginRight:'2px'}}/>
                                                  }
                                                  <p style={{marginTop:'-2px'}}><FormattedMessage id="listenAgain" tagName="data" /></p>

                                                </div>

                                              </Button>
                                            </FormGroup>

                                            {/*/!*phần dịch câu đáp án*!/*/}
                                            {/*<button style={{border: 'none', backgroundColor: 'white', padding: '0', marginLeft: '2%'}}*/}
                                            {/*id={"a" + question.stt + indexSubAnswer} className="mr-1 mb-1">*/}
                                            {/*<Icon.AlertCircle style={{verticalAlign: 'unset'}}  size={20} />*/}
                                            {/*</button>*/}
                                            {/*<UncontrolledPopover trigger="focus" placement="bottom" target={"a" + question.stt + indexSubAnswer}>*/}
                                            {/*<PopoverBody style={{borderColor: 'green !important',*/}
                                            {/*color: 'white', backgroundColor: 'green', borderRadius: '5px'}}>*/}
                                            {/*<FormattedMessage id="translate" />: {indexSubAnswer===0 ? question.translatingQuesA : (indexSubAnswer===1 ? question.translatingQuesB : (indexSubAnswer===2 ? question.translatingQuesC : question.translatingQuesD))}*/}
                                            {/*</PopoverBody>*/}
                                            {/*</UncontrolledPopover>*/}
                                            {/*/!*phần dịch câu đáp án*!/*/}


                                            {/*<div style={{marginLeft: '3%'}}>*/}
                                            {/*<button className={classes.btnRepeat}*/}
                                            {/*onClick={()=>this.playingListeningAgainForPart1(question, index, indexSubAnswer)}>*/}
                                            {/*<div style={{display: 'flex'}}>*/}
                                            {/*{*/}
                                            {/*checkPlayingP1 === (index * 4 + indexSubAnswer) ?*/}
                                            {/*<Icon.Volume1 size={12} className={classes.iconPlay} />*/}
                                            {/*:*/}
                                            {/*<Icon.Play className={classes.iconPlay} size={11}/>*/}
                                            {/*}*/}
                                            {/*<FormattedMessage id="listenAgain" tagName="data" />*/}
                                            {/*</div>*/}

                                            {/*</button>*/}
                                            {/*</div>*/}

                                          </Col>
                                        </Col>
                                      </>
                                    ) : null }
                                  </Col>
                                  <Col xs={6}>
                                    <img className={classes.imgClass} style={{width: '100%', border: '5px solid lightgray'}}
                                         src={question.pathFile2 ? question.pathFile2 : ""}/>
                                  </Col>
                                </Row>
                                <Row>
                                  <Col xs="12" md="12" sm="12" lg="12" style={{marginBottom: '10%'}}>
                                    <fieldset className={classes.fieldsetDescription}>
                                      <legend className={classes.legendDescription}><FormattedMessage id="description" /></legend>
                                      <i>
                                        {
                                          null === question.description ?
                                            <><FormattedMessage id={"no.explanation"}/> GÌ HẾT :))</>
                                            :
                                            question.description.split("\n").map((des, i) =>
                                              <p>
                                                {i === 0 ? <>(A) {des}</> : null}
                                                {i === 1 ? <>(B) {des}</> : null}
                                                {i === 2 ? <>(C) {des}</> : null}
                                                {i === 3 ? <>(D) {des}</> : null}
                                              </p>
                                            )
                                        }
                                      </i>
                                    </fieldset>
                                  </Col>
                                </Row>
                              </>
                            )}
                          </>
                        </div>
                        {/*--------------------------------------end PART1----------------------------------------*/}



                        {/*--------------------------------------PART2----------------------------------------*/}
                        <div className="mt-3" style={{marginTop: '5% !important'}}>
                          <h1 id="PART2" style={{fontWeight: '700'}}>PART 2</h1>

                          <>
                            {listQuestionAndAnswerMinitest[1].listCategoryMinitestDTOS[0].listQuestionMinitestDTOS.map((question, index) =>
                              <>
                                <Row id={String(question.stt)}  style={{marginTop: '4%'}}>
                                  <Col xs={1}><h4>{question.stt}.</h4></Col>
                                  <Col xs="5">
                                    {question ? this.cutAnswerToChoose(question.answersToChoose).map((subAnswer, indexSubAnswer) =>
                                      <>
                                        <Col xs="12" md="12" sm="12" lg="12" style={{display: 'flex', padding: '0%'}}>
                                          <Col xs="1" md="2" sm="1" lg="1" style={{padding: '0%'}}>
                                            {indexSubAnswer === question.indexCorrectAnswer ?
                                              <Icon.Check size={25} style={{color: 'green'}}/> : null
                                            }
                                            {indexSubAnswer === question.indexIncorrectAnswer ?
                                              <Icon.X size={25} style={{color: '#b21717'}} /> : null
                                            }
                                            {/*<Icon.Check size={25} style={{color: 'green'}}/>*/}
                                          </Col>
                                          <Col xs="11" md="10" sm="11" lg="11" style={{padding: '0%', display: 'flex'}}>

                                            <FormGroup check style={{marginBottom:'3%', display: 'flex'}}>
                                              <Label  check className={classes.labelRadioButton} style={{cursor: 'pointer'}}>
                                                <Input type="radio" name={question.stt}
                                                       checked={this.checkingAnswerChoosen(question.indexIncorrectAnswer,
                                                         question.indexCorrectAnswer, indexSubAnswer)}
                                                />
                                                <h4>({ indexSubAnswer===0 ? "A" : (indexSubAnswer===1 ? "B" : (indexSubAnswer===2 ? "C" : "D")) })</h4>
                                              </Label>

                                              <Button outline className={classes.buttonListenAgain}
                                                      style={{padding: '0', display:'inline-flex', marginLeft:'10px', width:'auto'}} id='btnHighlightRepeat'
                                                      onClick={()=>this.playingListeningAgainForPart2(question, index, indexSubAnswer)}>
                                                <div style={{padding: '0 5px', display:'inline-flex'}}>
                                                  {checkPlayingP2 === (index * 3 + indexSubAnswer) ?

                                                    <Icon.Volume1 size={12} style={{marginTop:'2.5px', marginRight:'2px'}} />
                                                    :
                                                    <Icon.Play size={11} style={{marginTop:'2.5px', marginRight:'2px'}}/>
                                                  }
                                                  <p style={{marginTop:'-2px'}}><FormattedMessage id="listenAgain" tagName="data" /></p>

                                                </div>

                                              </Button>
                                            </FormGroup>

                                            {/*/!*phần dịch câu đáp án*!/*/}
                                            {/*<button style={{border: 'none', backgroundColor: 'white', padding: '0', marginLeft: '2%'}}*/}
                                            {/*id={"a" + question.stt + indexSubAnswer} className="mr-1 mb-1">*/}
                                            {/*<Icon.AlertCircle style={{verticalAlign: 'unset'}}  size={20} />*/}
                                            {/*</button>*/}
                                            {/*<UncontrolledPopover trigger="focus" placement="bottom" target={"a" + question.stt + indexSubAnswer}>*/}
                                            {/*<PopoverBody style={{borderColor: 'green !important',*/}
                                            {/*color: 'white', backgroundColor: 'green', borderRadius: '5px'}}>*/}
                                            {/*<FormattedMessage id="translate" />: {indexSubAnswer===0 ? question.translatingQuesA : (indexSubAnswer===1 ? question.translatingQuesB : (indexSubAnswer===2 ? question.translatingQuesC : question.translatingQuesD))}*/}
                                            {/*</PopoverBody>*/}
                                            {/*</UncontrolledPopover>*/}
                                            {/*/!*phần dịch câu đáp án*!/*/}


                                            {/*<div style={{marginLeft: '3%'}}>*/}
                                            {/*<button className={classes.btnRepeat}*/}
                                            {/*onClick={()=>this.playingListeningAgainForPart2(question, index, indexSubAnswer)}>*/}
                                            {/*<div style={{display: 'flex'}}>*/}
                                            {/*{*/}
                                            {/*checkPlayingP2 === (index * 3 + indexSubAnswer) ?*/}
                                            {/*<Icon.Volume1 size={12} className={classes.iconPlay} />*/}
                                            {/*:*/}
                                            {/*<Icon.Play className={classes.iconPlay} size={11}/>*/}
                                            {/*}*/}
                                            {/*<FormattedMessage id="listenAgain" tagName="data" />*/}
                                            {/*</div>*/}

                                            {/*</button>*/}

                                            {/*</div>*/}

                                          </Col>
                                        </Col>
                                      </>
                                    ) : null }
                                  </Col>
                                  <Col xs={6}>

                                  </Col>
                                </Row>
                                <Row>
                                  <Col xs="12" md="12" sm="12" lg="12" style={{marginBottom: '10%'}}>
                                    <fieldset className={classes.fieldsetDescription}>
                                      <legend className={classes.legendDescription}><FormattedMessage id="description" /></legend>
                                      <i>
                                        {
                                          null === question.description ?
                                            <><FormattedMessage id={"no.explanation"}/> GÌ HẾT :))</>
                                            :
                                            question.description.split("\n").map((des, i) =>
                                              <p>
                                                {i === 0 ? <>(A) {des}</> : null}
                                                {i === 1 ? <>(B) {des}</> : null}
                                                {i === 2 ? <>(C) {des}</> : null}
                                                {i === 3 ? <>(D) {des}</> : null}
                                              </p>
                                            )
                                        }
                                      </i>
                                    </fieldset>
                                  </Col>
                                </Row>
                              </>
                            )}
                          </>
                        </div>
                        {/*--------------------------------------end PART2----------------------------------------*/}



                        {/*-----------------------------------------PART3------------------------------------------*/}
                        <div className="mt-3">
                          <h1 id="PART3" style={{fontWeight: '700'}}>PART 3</h1>

                          {listQuestionAndAnswerMinitest[2].listCategoryMinitestDTOS.map((category, indexCategory) =>
                            <>
                              {null === category.pathFile2 ?
                                <>
                                  <h4 style={{fontWeight: '700', marginTop: '5%'}}>3.{indexCategory+1}.</h4>

                                  {category.listQuestionMinitestDTOS.map((question, indexQuestion) =>
                                    <>
                                      <Row id={String(question.stt)}  style={{marginTop:'3%', marginLeft: '10%'}}>
                                        <Col xs="6" md="6" sm="6" lg="6">

                                          <h5 style={{fontWeight:'700'}}>{question.stt}. {question.name}
                                            {/*phần dịch câu hỏi*/}
                                            <button style={{border: 'none', backgroundColor: 'white', padding: '0', marginLeft: '2%'}}
                                                    id={"q" + question.stt} className="mr-1 mb-1">
                                              <Icon.AlertCircle  size={20} />
                                            </button>
                                            <UncontrolledPopover trigger="focus" placement="bottom" target={"q" + question.stt}
                                            >
                                              <PopoverBody style={{borderColor: 'green !important',
                                                color: 'white', backgroundColor: 'green', borderRadius: '5px'}}>
                                                <FormattedMessage id="translate" />: {question.translatingQuestion !== null ? question.translatingQuestion : "chưa có phần dịch"}
                                              </PopoverBody>
                                            </UncontrolledPopover>
                                            {/*end phần dịch câu hỏi*/}

                                            <Button outline className={classes.buttonListenAgain}
                                                    style={{padding: '0', display:'inline-flex',  width:'auto'}} id='btnHighlightRepeat'
                                                    onClick={()=>this.playingListeningAgainForPart3(indexCategory, indexQuestion)}>
                                              <div style={{padding: '0 5px', display:'inline-flex'}}>
                                                {checkPlayingP3 === (indexCategory * 3 + indexQuestion) ?

                                                  <Icon.Volume1 size={12} style={{marginTop:'2.5px', marginRight:'2px'}} />
                                                  :
                                                  <Icon.Play size={11} style={{marginTop:'2.5px', marginRight:'2px'}}/>
                                                }
                                                <p style={{marginTop:'-2px'}}><FormattedMessage id="listenAgain" tagName="data" /></p>

                                              </div>
                                            </Button>

                                            {/*<Button outline className={classes.buttonListenAgain} style={{padding: '0', display:'inline-flex'}} id='btnHighlightRepeat'*/}
                                                    {/*onClick={()=>this.playingListeningAgainForPart3(indexCategory, indexQuestion)}>*/}
                                              {/*{*/}
                                                {/*checkPlayingP3 === (indexCategory * 3 + indexQuestion) ?*/}
                                                  {/*<Icon.Volume1 size={12} className={classes.iconPlay} />*/}
                                                  {/*:*/}
                                                  {/*<Icon.Play className={classes.iconPlay} size={11}/>*/}
                                              {/*}*/}
                                              {/*<p style={{marginTop:'-2px'}}><FormattedMessage id="listenAgain" tagName="data" /></p>*/}

                                            {/*</Button>*/}
                                          </h5>

                                        </Col>
                                        <Col xs="6" md="6" sm="6" lg="6">
                                          {this.cutAnswerToChoose(question.answersToChoose).map((subAnswer, indexSubAnswer) =>
                                            <>
                                              <Col xs="12" md="12" sm="12" lg="12" style={{display: 'flex', padding: '0%'}}>
                                                <Col xs="1" md="2" sm="1" lg="1" style={{padding: '0%'}}>
                                                  {indexSubAnswer === question.indexCorrectAnswer ?
                                                    <Icon.Check size={25} style={{color: 'green'}}/> : null
                                                  }
                                                  {indexSubAnswer === question.indexIncorrectAnswer ?
                                                    <Icon.X size={25} style={{color: '#b21717'}} /> : null
                                                  }
                                                  {/*<Icon.Check size={25} style={{color: 'green'}}/>*/}
                                                </Col>
                                                <Col xs="11" md="10" sm="11" lg="11" style={{padding: '0%', display: 'flex'}}>
                                                  <FormGroup check style={{marginBottom:'3%'}}>
                                                    <Label  check className={classes.labelRadioButton} style={{cursor: 'pointer',
                                                      fontSize: '120%', fontWeight: '500'}}>
                                                      <Input style={{marginTop: '5px'}} type="radio" name={question.stt}
                                                             checked={this.checkingAnswerChoosen(question.indexIncorrectAnswer,
                                                               question.indexCorrectAnswer, indexSubAnswer)}
                                                      />
                                                      ({ indexSubAnswer===0 ? "A" : (indexSubAnswer===1 ? "B" : (indexSubAnswer===2 ? "C" : "D")) }) {this.upperCaseFirstLetter(subAnswer)}
                                                    </Label>
                                                    {/*phần dịch câu đáp án*/}
                                                    <button style={{border: 'none', backgroundColor: 'white', padding: '0', marginLeft: '3px'}}
                                                            id={"a" + question.stt + indexSubAnswer} className="mr-1 mb-1">
                                                      <Icon.AlertCircle  size={20} />
                                                    </button>
                                                    <UncontrolledPopover trigger="focus" placement="bottom" target={"a" + question.stt + indexSubAnswer}

                                                    >
                                                      <PopoverBody style={{borderColor: 'green !important',
                                                        color: 'white', backgroundColor: 'green', borderRadius: '5px'}}>
                                                        <FormattedMessage id="translate" />: {indexSubAnswer===0 ? question.translatingQuesA : (indexSubAnswer===1 ? question.translatingQuesB : (indexSubAnswer===2 ? question.translatingQuesC : question.translatingQuesD))}
                                                      </PopoverBody>
                                                    </UncontrolledPopover>
                                                    {/*end phần dịch câu đáp án*/}
                                                  </FormGroup>

                                                  {/*<div style={{marginLeft: '3%'}}>*/}
                                                    {/*{indexSubAnswer === question.indexCorrectAnswer ?*/}
                                                      {/*<button className={classes.btnRepeat}*/}
                                                              {/*onClick={()=>this.playingListeningAgain(question, indexCategory+16, indexSubAnswer)}>*/}
                                                        {/*<Icon.Play className={classes.iconPlay} size={11}/>*/}
                                                        {/*Nghe lại*/}
                                                      {/*</button> : null*/}
                                                    {/*}*/}
                                                  {/*</div>*/}
                                                </Col>
                                              </Col>
                                            </>
                                          )}
                                        </Col>
                                      </Row>
                                      <Row style={{marginLeft: '10%', marginRight: '10%'}}>
                                        <Col xs="12" md="12" sm="12" lg="12" style={{marginBottom: '10%'}}>
                                          <fieldset className={classes.fieldsetDescription}>
                                            <legend className={classes.legendDescription}><FormattedMessage id="description" /></legend>
                                            <i>{null === question.description ? <><FormattedMessage id={"no.explanation"}/> GÌ HẾT :))</> : question.description}</i>
                                          </fieldset>
                                        </Col>
                                      </Row>
                                    </>
                                  )}

                                </>
                                :
                                <>
                                  <h4 style={{fontWeight: '700', marginTop: '5%'}}>3.{indexCategory+1}.</h4>
                                  <Row style={{marginTop:'3%', marginLeft: '10%'}}>
                                    <Col xs="6" md="6" sm="6" lg="6">
                                      <img className={classes.imgClass} style={{width: '100%', border: '5px solid lightgray'}}
                                           src={category.pathFile2}/>
                                    </Col>
                                    <Col xs="6" md="6" sm="6" lg="6">
                                      {category.listQuestionMinitestDTOS.map((question, indexQuestion) =>
                                        <>
                                          <Col id={String(question.stt)}  xs="12" md="12" sm="12" lg="12">
                                            <h5 style={{fontWeight:'700'}}>{question.stt}. {question.name}
                                              {/*phần dịch câu hỏi*/}
                                              <button style={{border: 'none', backgroundColor: 'white', padding: '0', marginLeft: '2%'}}
                                                      id={"q" + question.stt} className="mr-1 mb-1">
                                                <Icon.AlertCircle  size={20} />
                                              </button>
                                              <UncontrolledPopover trigger="focus" placement="bottom" target={"q" + question.stt}
                                              >
                                                <PopoverBody style={{borderColor: 'green !important',
                                                  color: 'white', backgroundColor: 'green', borderRadius: '5px'}}>
                                                  <FormattedMessage id="translate" />: {question.translatingQuestion !== null ? question.translatingQuestion : "chưa có phần dịch"}
                                                </PopoverBody>
                                              </UncontrolledPopover>
                                              {/*end phần dịch câu hỏi*/}

                                              <Button outline className={classes.buttonListenAgain}
                                                      style={{padding: '0', display:'inline-flex',  width:'auto'}} id='btnHighlightRepeat'
                                                      onClick={()=>this.playingListeningAgainForPart3(indexCategory, indexQuestion)}>
                                                <div style={{padding: '0 5px', display:'inline-flex'}}>
                                                  {checkPlayingP3 === (indexCategory * 3 + indexQuestion) ?

                                                    <Icon.Volume1 size={12} style={{marginTop:'2.5px', marginRight:'2px'}} />
                                                    :
                                                    <Icon.Play size={11} style={{marginTop:'2.5px', marginRight:'2px'}}/>
                                                  }
                                                  <p style={{marginTop:'-2px'}}><FormattedMessage id="listenAgain" tagName="data" /></p>

                                                </div>
                                              </Button>

                                              {/*<Button outline className={classes.buttonListenAgain} style={{padding: '0', display:'inline-flex'}} id='btnHighlightRepeat'*/}
                                                      {/*onClick={()=>this.playingListeningAgainForPart3(indexCategory, indexQuestion)}>*/}
                                                {/*{*/}
                                                  {/*checkPlayingP3 === (indexCategory * 3 + indexQuestion) ?*/}
                                                    {/*<Icon.Volume1 size={12} className={classes.iconPlay} />*/}
                                                    {/*:*/}
                                                    {/*<Icon.Play className={classes.iconPlay} size={11}/>*/}
                                                {/*}*/}
                                                {/*<p style={{marginTop:'-2px'}}><FormattedMessage id="listenAgain" tagName="data" /></p>*/}

                                              {/*</Button>*/}
                                            </h5>


                                            {this.cutAnswerToChoose(question.answersToChoose).map((subAnswer, indexSubAnswer) =>
                                              <>
                                                <Col xs="12" md="12" sm="12" lg="12" style={{display: 'flex', padding: '0%'}}>
                                                  <Col xs="1" md="2" sm="1" lg="1" style={{padding: '0%'}}>
                                                    {indexSubAnswer === question.indexCorrectAnswer ?
                                                      <Icon.Check size={25} style={{color: 'green'}}/> : null
                                                    }
                                                    {indexSubAnswer === question.indexIncorrectAnswer ?
                                                      <Icon.X size={25} style={{color: '#b21717'}} /> : null
                                                    }
                                                    {/*<Icon.Check size={25} style={{color: 'green'}}/>*/}
                                                  </Col>
                                                  <Col xs="11" md="10" sm="11" lg="11" style={{padding: '0%', display: 'flex'}}>
                                                    <FormGroup check style={{marginBottom:'3%'}}>
                                                      <Label  check className={classes.labelRadioButton} style={{cursor: 'pointer',
                                                        fontSize: '120%', fontWeight: '500'}}>
                                                        <Input type="radio" name={question.stt}
                                                               checked={this.checkingAnswerChoosen(question.indexIncorrectAnswer,
                                                                 question.indexCorrectAnswer, indexSubAnswer)}
                                                        />
                                                        ({ indexSubAnswer===0 ? "A" : (indexSubAnswer===1 ? "B" : (indexSubAnswer===2 ? "C" : "D")) }) {this.upperCaseFirstLetter(subAnswer)}
                                                      </Label>
                                                      {/*phần dịch câu đáp án*/}
                                                      <button style={{border: 'none', backgroundColor: 'white', padding: '0', marginLeft: '3px'}}
                                                              id={"a" + question.stt + indexSubAnswer} className="mr-1 mb-1">
                                                        <Icon.AlertCircle  size={20} />
                                                      </button>
                                                      <UncontrolledPopover trigger="focus" placement="bottom" target={"a" + question.stt + indexSubAnswer}

                                                      >
                                                        <PopoverBody style={{borderColor: 'green !important',
                                                          color: 'white', backgroundColor: 'green', borderRadius: '5px'}}>
                                                          <FormattedMessage id="translate" />: {indexSubAnswer===0 ? question.translatingQuesA : (indexSubAnswer===1 ? question.translatingQuesB : (indexSubAnswer===2 ? question.translatingQuesC : question.translatingQuesD))}
                                                        </PopoverBody>
                                                      </UncontrolledPopover>
                                                      {/*phần dịch câu đáp án*/}
                                                    </FormGroup>

                                                    {/*<div style={{marginLeft: '3%'}}>*/}
                                                    {/*{indexSubAnswer === question.indexCorrectAnswer ?*/}
                                                    {/*<button className={classes.btnRepeat}*/}
                                                    {/*onClick={()=>this.playingListeningAgain(question, indexCategory+16, indexSubAnswer)}>*/}
                                                    {/*<Icon.Play className={classes.iconPlay} size={11}/>*/}
                                                    {/*Nghe lại*/}
                                                    {/*</button> : null*/}
                                                    {/*}*/}
                                                    {/*</div>*/}
                                                  </Col>
                                                </Col>
                                              </>
                                            )}
                                          </Col>
                                          <Col xs="12" md="12" sm="12" lg="12" style={{marginBottom: '10%'}}>
                                            <fieldset className={classes.fieldsetDescription}>
                                              <legend className={classes.legendDescription}><FormattedMessage id="description" /></legend>
                                              <i>{null === question.description ? <><FormattedMessage id={"no.explanation"}/> GÌ HẾT :))</> : question.description}</i>
                                            </fieldset>
                                          </Col>
                                        </>
                                      )}
                                    </Col>
                                  </Row>
                                </>
                              }
                            </>
                          )}
                        </div>
                        {/*-------------------------------------------end PART3---------------------------------------*/}


                        {/*--------------------------------------------PART4--------------------------------------------*/}
                        <div className="mt-3">
                          <h1 id="PART4" style={{fontWeight: '700'}}>PART 4</h1>

                          {listQuestionAndAnswerMinitest[3].listCategoryMinitestDTOS.map((category, indexCategory) =>
                            <>
                              {null === category.pathFile2 ?
                                <>
                                  <h4 style={{fontWeight: '700', marginTop: '5%'}}>4.{indexCategory + 1}.</h4>

                                  {category.listQuestionMinitestDTOS.map((question, indexQuestion) =>
                                    <>
                                      <Row id={String(question.stt)}  style={{marginTop:'3%', marginLeft: '10%'}}>
                                        <Col xs="6" md="6" sm="6" lg="6">
                                          <h5 style={{fontWeight:'700'}}>{question.stt}. {question.name}
                                            {/*phần dịch câu hỏi*/}
                                            <button style={{border: 'none', backgroundColor: 'white', padding: '0', marginLeft: '2%'}}
                                                    id={"q" + question.stt} className="mr-1 mb-1">
                                              <Icon.AlertCircle  size={20} />
                                            </button>
                                            <UncontrolledPopover trigger="focus" placement="bottom" target={"q" + question.stt}
                                            >
                                              <PopoverBody style={{borderColor: 'green !important',
                                                color: 'white', backgroundColor: 'green', borderRadius: '5px'}}>
                                                <FormattedMessage id="translate" />: {question.translatingQuestion !== null ? question.translatingQuestion : "chưa có phần dịch"}
                                              </PopoverBody>
                                            </UncontrolledPopover>
                                            {/*end phần dịch câu hỏi*/}

                                            <Button outline className={classes.buttonListenAgain}
                                                    style={{padding: '0', display:'inline-flex',  width:'auto'}} id='btnHighlightRepeat'
                                                    onClick={()=>this.playingListeningAgainForPart4(indexCategory, indexQuestion)}>
                                              <div style={{padding: '0 5px', display:'inline-flex'}}>
                                                {checkPlayingP4 === (indexCategory * 3 + indexQuestion) ?

                                                  <Icon.Volume1 size={12} style={{marginTop:'2.5px', marginRight:'2px'}} />
                                                  :
                                                  <Icon.Play size={11} style={{marginTop:'2.5px', marginRight:'2px'}}/>
                                                }
                                                <p style={{marginTop:'-2px'}}><FormattedMessage id="listenAgain" tagName="data" /></p>

                                              </div>
                                            </Button>

                                            {/*<Button outline className={classes.buttonListenAgain} style={{padding: '0', display:'inline-flex'}} id='btnHighlightRepeat'*/}
                                                    {/*onClick={()=>this.playingListeningAgainForPart4(indexCategory, indexQuestion)}>*/}
                                              {/*{*/}
                                                {/*checkPlayingP4 === (indexCategory * 3 + indexQuestion) ?*/}
                                                  {/*<Icon.Volume1 size={12} className={classes.iconPlay} />*/}
                                                  {/*:*/}
                                                  {/*<Icon.Play className={classes.iconPlay} size={11}/>*/}
                                              {/*}*/}
                                              {/*<p style={{marginTop:'-2px'}}><FormattedMessage id="listenAgain" tagName="data" /></p>*/}

                                            {/*</Button>*/}
                                          </h5>

                                        </Col>
                                        <Col xs="6" md="6" sm="6" lg="6">
                                          {this.cutAnswerToChoose(question.answersToChoose).map((subAnswer, indexSubAnswer) =>
                                            <>
                                              <Col xs="12" md="12" sm="12" lg="12" style={{display: 'flex', padding: '0%'}}>
                                                <Col xs="1" md="2" sm="1" lg="1" style={{padding: '0%'}}>
                                                  {indexSubAnswer === question.indexCorrectAnswer ?
                                                    <Icon.Check size={25} style={{color: 'green'}}/> : null
                                                  }
                                                  {indexSubAnswer === question.indexIncorrectAnswer ?
                                                    <Icon.X size={25} style={{color: '#b21717'}} /> : null
                                                  }
                                                  {/*<Icon.Check size={25} style={{color: 'green'}}/>*/}
                                                </Col>
                                                <Col xs="11" md="10" sm="11" lg="11" style={{padding: '0%', display: 'flex'}}>
                                                  <FormGroup check style={{marginBottom:'3%'}}>
                                                    <Label  check className={classes.labelRadioButton} style={{cursor: 'pointer',
                                                      fontSize: '120%', fontWeight: '500'}}>
                                                      <Input type="radio" name={question.stt}
                                                             checked={this.checkingAnswerChoosen(question.indexIncorrectAnswer,
                                                               question.indexCorrectAnswer, indexSubAnswer)}
                                                      />
                                                      ({ indexSubAnswer===0 ? "A" : (indexSubAnswer===1 ? "B" : (indexSubAnswer===2 ? "C" : "D")) }) {this.upperCaseFirstLetter(subAnswer)}
                                                    </Label>
                                                    {/*phần dịch câu đáp án*/}
                                                    <button style={{border: 'none', backgroundColor: 'white', padding: '0', marginLeft: '3px'}}
                                                            id={"a" + question.stt + indexSubAnswer} className="mr-1 mb-1">
                                                      <Icon.AlertCircle  size={20} />
                                                    </button>
                                                    <UncontrolledPopover trigger="focus" placement="bottom" target={"a" + question.stt + indexSubAnswer}

                                                    >
                                                      <PopoverBody style={{borderColor: 'green !important',
                                                        color: 'white', backgroundColor: 'green', borderRadius: '5px'}}>
                                                        <FormattedMessage id="translate" />: {indexSubAnswer===0 ? question.translatingQuesA : (indexSubAnswer===1 ? question.translatingQuesB : (indexSubAnswer===2 ? question.translatingQuesC : question.translatingQuesD))}
                                                      </PopoverBody>
                                                    </UncontrolledPopover>
                                                    {/*phần dịch câu đáp án*/}
                                                  </FormGroup>

                                                </Col>
                                              </Col>
                                            </>
                                          )}
                                        </Col>
                                      </Row>
                                      <Row style={{marginLeft: '10%', marginRight: '10%'}}>
                                        <Col xs="12" md="12" sm="12" lg="12" style={{marginBottom: '10%'}}>
                                          <fieldset className={classes.fieldsetDescription}>
                                            <legend className={classes.legendDescription}><FormattedMessage id="description" /></legend>
                                            <i>{null === question.description ? <><FormattedMessage id={"no.explanation"}/> GÌ HẾT :))</> : question.description}</i>
                                          </fieldset>
                                        </Col>
                                      </Row>
                                    </>
                                  )}

                                </>
                                :
                                <>
                                  <h4 style={{fontWeight: '700', marginTop: '5%'}}>4.{indexCategory+1}.</h4>
                                  <Row style={{marginTop:'3%', marginLeft: '10%'}}>
                                    <Col xs="6" md="6" sm="6" lg="6">
                                      <img className={classes.imgClass} style={{width: '100%', border: '5px solid lightgray'}}
                                           src={category.pathFile2}/>
                                    </Col>
                                    <Col xs="6" md="6" sm="6" lg="6">
                                      {category.listQuestionMinitestDTOS.map((question, indexQuestion) =>
                                        <>
                                          <Col id={String(question.stt)}  xs="12" md="12" sm="12" lg="12">
                                            <h5 style={{fontWeight:'700'}}>{question.stt}. {question.name}
                                              {/*phần dịch câu hỏi*/}
                                              <button style={{border: 'none', backgroundColor: 'white', padding: '0', marginLeft: '2%'}}
                                                      id={"q" + question.stt} className="mr-1 mb-1">
                                                <Icon.AlertCircle  size={20} />
                                              </button>
                                              <UncontrolledPopover trigger="focus" placement="bottom" target={"q" + question.stt}
                                              >
                                                <PopoverBody style={{borderColor: 'green !important',
                                                  color: 'white', backgroundColor: 'green', borderRadius: '5px'}}>
                                                  <FormattedMessage id="translate" />: {question.translatingQuestion !== null ? question.translatingQuestion : "chưa có phần dịch"}
                                                </PopoverBody>
                                              </UncontrolledPopover>
                                              {/*end phần dịch câu hỏi*/}

                                              <Button outline className={classes.buttonListenAgain}
                                                      style={{padding: '0', display:'inline-flex',  width:'auto'}} id='btnHighlightRepeat'
                                                      onClick={()=>this.playingListeningAgainForPart4(indexCategory, indexQuestion)}>
                                                <div style={{padding: '0 5px', display:'inline-flex'}}>
                                                  {checkPlayingP4 === (indexCategory * 3 + indexQuestion) ?

                                                    <Icon.Volume1 size={12} style={{marginTop:'2.5px', marginRight:'2px'}} />
                                                    :
                                                    <Icon.Play size={11} style={{marginTop:'2.5px', marginRight:'2px'}}/>
                                                  }
                                                  <p style={{marginTop:'-2px'}}><FormattedMessage id="listenAgain" tagName="data" /></p>

                                                </div>
                                              </Button>

                                              {/*<Button outline className={classes.buttonListenAgain} style={{padding: '0', display:'inline-flex'}} id='btnHighlightRepeat'*/}
                                                      {/*onClick={()=>this.playingListeningAgainForPart4(indexCategory, indexQuestion)}>*/}
                                                {/*{*/}
                                                  {/*checkPlayingP4 === (indexCategory * 3 + indexQuestion) ?*/}
                                                    {/*<Icon.Volume1 size={12} className={classes.iconPlay} />*/}
                                                    {/*:*/}
                                                    {/*<Icon.Play className={classes.iconPlay} size={11}/>*/}
                                                {/*}*/}
                                                {/*<p style={{marginTop:'-2px'}}><FormattedMessage id="listenAgain" tagName="data" /></p>*/}

                                              {/*</Button>*/}
                                            </h5>

                                            {this.cutAnswerToChoose(question.answersToChoose).map((subAnswer, indexSubAnswer) =>
                                              <>
                                                <Col xs="12" md="12" sm="12" lg="12" style={{display: 'flex', padding: '0%'}}>
                                                  <Col xs="1" md="2" sm="1" lg="1" style={{padding: '0%'}}>
                                                    {indexSubAnswer === question.indexCorrectAnswer ?
                                                      <Icon.Check size={25} style={{color: 'green'}}/> : null
                                                    }
                                                    {indexSubAnswer === question.indexIncorrectAnswer ?
                                                      <Icon.X size={25} style={{color: '#b21717'}} /> : null
                                                    }
                                                    {/*<Icon.Check size={25} style={{color: 'green'}}/>*/}
                                                  </Col>
                                                  <Col xs="11" md="10" sm="11" lg="11" style={{padding: '0%', display: 'flex'}}>
                                                    <FormGroup check style={{marginBottom:'3%'}}>
                                                      <Label  check className={classes.labelRadioButton} style={{cursor: 'pointer',
                                                        fontSize: '120%', fontWeight: '500'}}>
                                                        <Input type="radio" name={question.stt}
                                                               checked={this.checkingAnswerChoosen(question.indexIncorrectAnswer,
                                                                 question.indexCorrectAnswer, indexSubAnswer)}
                                                        />
                                                        ({ indexSubAnswer===0 ? "A" : (indexSubAnswer===1 ? "B" : (indexSubAnswer===2 ? "C" : "D")) }) {this.upperCaseFirstLetter(subAnswer)}
                                                      </Label>
                                                      {/*phần dịch câu đáp án*/}
                                                      <button style={{border: 'none', backgroundColor: 'white', padding: '0', marginLeft: '3px'}}
                                                              id={"a" + question.stt + indexSubAnswer} className="mr-1 mb-1">
                                                        <Icon.AlertCircle  size={20} />
                                                      </button>
                                                      <UncontrolledPopover trigger="focus" placement="bottom" target={"a" + question.stt + indexSubAnswer}

                                                      >
                                                        <PopoverBody style={{borderColor: 'green !important',
                                                          color: 'white', backgroundColor: 'green', borderRadius: '5px'}}>
                                                          <FormattedMessage id="translate" />: {indexSubAnswer===0 ? question.translatingQuesA : (indexSubAnswer===1 ? question.translatingQuesB : (indexSubAnswer===2 ? question.translatingQuesC : question.translatingQuesD))}
                                                        </PopoverBody>
                                                      </UncontrolledPopover>
                                                      {/*phần dịch câu đáp án*/}
                                                    </FormGroup>

                                                  </Col>
                                                </Col>
                                              </>
                                            )}
                                          </Col>
                                          <Col xs="12" md="12" sm="12" lg="12" style={{marginBottom: '10%'}}>
                                            <fieldset className={classes.fieldsetDescription}>
                                              <legend className={classes.legendDescription}><FormattedMessage id="description" /></legend>
                                              <i>{null === question.description ? <><FormattedMessage id={"no.explanation"}/> GÌ HẾT :))</> : question.description}</i>
                                            </fieldset>
                                          </Col>
                                        </>
                                      )}
                                    </Col>
                                  </Row>
                                </>
                              }
                            </>
                          )}
                        </div>
                        {/*-------------------------------------------- end PART4--------------------------------------------*/}
                      </> : null
                    }
                  </>
                </div>
              </Row>
            </div>
          </div>
        }

      </>

    );
  }
}

const mapStateToProps = (state) => {
  return {
    listQuestionMinitest : state.doTestReducer.listQuestionMinitest,
    listQuestionListeningChoosenDTOS: state.doTestReducer.listQuestionListeningChoosenDTOS,
    listQuestionReadingChoosenDTOS: state.doTestReducer.listQuestionReadingChoosenDTOS,
    listQuestionAndAnswerMinitest: state.doTestReducer.listQuestionAndAnswerMinitest,
    loadingQuestion: state.doTestReducer.loadingQuestion,
    loadingSubmitAnswer: state.doTestReducer.loadingSubmitAnswer,
    checkGetListQuestionMinitest: state.doTestReducer.checkGetListQuestionMinitest,
    showAnswer: state.doTestReducer.showAnswer
  }
}
const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    testAction : bindActionCreators(testAction , dispatch),
  }
}
const withConnect = connect(mapStateToProps, mapDispatchToProps)

export default compose(withStyles(styles), withConnect)(ListeningTest);

