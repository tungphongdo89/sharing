import React, { Component, useState } from 'react';
import {connect} from "react-redux";
import {bindActionCreators, compose} from "redux";
import {Badge, Col, Container, Form, FormGroup, Input, Label, Row, Spinner} from "reactstrap";
import {Link, NavLink} from "react-router-dom";
import styles from "./style";
import {withStyles} from "@material-ui/core";
import ListeningTest from './listening-test';
import ReadingTest from './reading-test';
import AccessTimeIcon from '@material-ui/icons/AccessTime';
import testAction from "../../../redux/actions/do-test-management";

import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import {history} from "../../../history";
import historyPracticesAction from "../../../redux/actions/history-practices/";
import {showMessage} from "../../../commons/utils/convertDataForToast";
import {ArrowLeft} from "react-feather";
import historyTestAction from "../../../redux/actions/history-test";
import {FormattedMessage} from "react-intl";

class Index extends Component {

  constructor(props) {
    super(props);
    this.state = {
      load: false,
      loadModal: false,
      data: false,
      listSttQuesListeningChoosen: [],
      listSttQuesReadingChoosen: [],
      modalConfirmSubmit: false,
      modalTimeIsUp: false,
      modalConfirmDoTest: true,
      second: this.props.secondForMinitest,
      minute: this.props.minuteForMinitest,
      keepDoTest: true,
      keepDoTest2: false,
      waitClosePopup: true
    }
  }

  getlistQuesListeningPrimitive =()=> {
    debugger
    const list = this.props.listQuestionMinitest;

    let listQuesListeningPrimitive = [];

    for(let i=0;i<list[0].listCategoryMinitestDTOS[0].listQuestionMinitestDTOS.length;i++){
      let question = {};
      question.id = Number(list[0].listCategoryMinitestDTOS[0].listQuestionMinitestDTOS[i].id);
      question.stt = Number(list[0].listCategoryMinitestDTOS[0].listQuestionMinitestDTOS[i].stt);
      question.answerChoosen = "null";
      question.indexSubAnswer = null;
      if(list[0].listCategoryMinitestDTOS[0].categoryId === null){
        question.categoryId = 0;
      }
      else{
        question.categoryId = Number(list[0].listCategoryMinitestDTOS[0].categoryId);
      }
      question.partName = list[0].partName;
      listQuesListeningPrimitive.push(question)

    }

    for(let i=0;i<list[1].listCategoryMinitestDTOS[0].listQuestionMinitestDTOS.length;i++){
      let question = {};
      question.id = Number(list[1].listCategoryMinitestDTOS[0].listQuestionMinitestDTOS[i].id);
      question.stt = Number(list[1].listCategoryMinitestDTOS[0].listQuestionMinitestDTOS[i].stt);
      question.answerChoosen = "null";
      question.indexSubAnswer = null;
      if(list[1].listCategoryMinitestDTOS[0].categoryId === null){
        question.categoryId = 0;
      }
      else{
        question.categoryId = Number(list[1].listCategoryMinitestDTOS[0].categoryId);
      }

      question.partName = list[1].partName;
      listQuesListeningPrimitive.push(question)
    }

    for(let i=0;i<list[2].listCategoryMinitestDTOS.length;i++){
      for(let j=0;j<list[2].listCategoryMinitestDTOS[i].listQuestionMinitestDTOS.length;j++){
        let question = {};
        question.id = Number(list[2].listCategoryMinitestDTOS[i].listQuestionMinitestDTOS[j].id);
        question.stt = Number(list[2].listCategoryMinitestDTOS[i].listQuestionMinitestDTOS[j].stt);
        question.answerChoosen = "null";
        question.indexSubAnswer = null;
        question.categoryId = Number(list[2].listCategoryMinitestDTOS[i].categoryId);
        question.partName = list[2].partName;
        listQuesListeningPrimitive.push(question)
      }
    }

    for(let i=0;i<list[3].listCategoryMinitestDTOS.length;i++){
      for(let j=0;j<list[3].listCategoryMinitestDTOS[i].listQuestionMinitestDTOS.length;j++){
        let question = {};
        question.id = Number(list[3].listCategoryMinitestDTOS[i].listQuestionMinitestDTOS[j].id);
        question.stt = Number(list[3].listCategoryMinitestDTOS[i].listQuestionMinitestDTOS[j].stt);
        question.answerChoosen = "null";
        question.indexSubAnswer = null;
        question.categoryId = Number(list[3].listCategoryMinitestDTOS[i].categoryId);
        question.partName = list[3].partName;
        listQuesListeningPrimitive.push(question)
      }
    }

    debugger
    return listQuesListeningPrimitive;
  }

  getlistQuesReadingPrimitive =()=> {
    debugger
    const list = this.props.listQuestionMinitest;

    let listQuesReadingPrimitive = [];

    for(let i=0;i<list[4].listCategoryMinitestDTOS[0].listQuestionMinitestDTOS.length;i++){
      let question = {};
      question.id = Number(list[4].listCategoryMinitestDTOS[0].listQuestionMinitestDTOS[i].id);
      question.stt = Number(list[4].listCategoryMinitestDTOS[0].listQuestionMinitestDTOS[i].stt);
      question.answerChoosen = "null";
      question.indexSubAnswer = null;
      if(list[4].listCategoryMinitestDTOS[0].categoryId === null){
        question.categoryId = 0;
      }
      else{
        question.categoryId = Number(list[4].listCategoryMinitestDTOS[0].categoryId);
      }

      question.partName = list[4].partName;
      listQuesReadingPrimitive.push(question)

    }


    for(let i=0;i<list[5].listCategoryMinitestDTOS.length;i++){
      for(let j=0;j<list[5].listCategoryMinitestDTOS[i].listQuestionMinitestDTOS.length;j++){
        let question = {};
        question.id = Number(list[5].listCategoryMinitestDTOS[i].listQuestionMinitestDTOS[j].id);
        question.stt = Number(list[5].listCategoryMinitestDTOS[i].listQuestionMinitestDTOS[j].stt);
        question.answerChoosen = "null";
        question.indexSubAnswer = null;
        question.categoryId = Number(list[5].listCategoryMinitestDTOS[i].categoryId);
        question.partName = list[5].partName;
        listQuesReadingPrimitive.push(question)
      }
    }

    for(let i=0;i<list[6].listCategoryMinitestDTOS.length;i++){
      for(let j=0;j<list[6].listCategoryMinitestDTOS[i].listQuestionMinitestDTOS.length;j++){
        let question = {};
        question.id = Number(list[6].listCategoryMinitestDTOS[i].listQuestionMinitestDTOS[j].id);
        question.stt = Number(list[6].listCategoryMinitestDTOS[i].listQuestionMinitestDTOS[j].stt);
        question.answerChoosen = "null";
        question.indexSubAnswer = null;
        question.categoryId = Number(list[6].listCategoryMinitestDTOS[i].categoryId);
        question.partName = list[6].partName;
        listQuesReadingPrimitive.push(question)
      }
    }

    for(let i=0;i<list[7].listCategoryMinitestDTOS.length;i++){
      for(let j=0;j<list[7].listCategoryMinitestDTOS[i].listQuestionMinitestDTOS.length;j++){
        let question = {};
        question.id = Number(list[7].listCategoryMinitestDTOS[i].listQuestionMinitestDTOS[j].id);
        question.stt = Number(list[7].listCategoryMinitestDTOS[i].listQuestionMinitestDTOS[j].stt);
        question.answerChoosen = "null";
        question.indexSubAnswer = null;
        question.categoryId = Number(list[7].listCategoryMinitestDTOS[i].categoryId);
        question.partName = list[7].partName;
        listQuesReadingPrimitive.push(question)
      }
    }

    debugger
    return listQuesReadingPrimitive;

  }

  submitAnswer =(selfOrAuto)=>{
    debugger
    const {listQuestionListeningChoosenDTOS, listQuestionReadingChoosenDTOS , testAction} = this.props;
    const { modalConfirmSubmit, modalTimeIsUp} = this.state;
    let MinitestSubmitAnswer = {};

    if(listQuestionListeningChoosenDTOS.length > 0){
      MinitestSubmitAnswer.listQuestionListeningChoosenDTOS = listQuestionListeningChoosenDTOS;
    }
    else {
      MinitestSubmitAnswer.listQuestionListeningChoosenDTOS = this.getlistQuesListeningPrimitive();
    }

    if(listQuestionReadingChoosenDTOS.length > 0){
      MinitestSubmitAnswer.listQuestionReadingChoosenDTOS = listQuestionReadingChoosenDTOS;
    }
    else {
      MinitestSubmitAnswer.listQuestionReadingChoosenDTOS = this.getlistQuesReadingPrimitive();
    }

    this.setState({
      load: true,
      loadModal: true
    })
    setTimeout(()=>{
      this.setState({
        load: false,
        modalConfirmSubmit: !modalConfirmSubmit,
        modalTimeIsUp: !modalTimeIsUp,
        loadModal: false

      })
    }, 20000)

    this.modalTimeIsUp(false)

    const {submitAnswerMinitest} = testAction;
    MinitestSubmitAnswer.checkDetail = 1;
    submitAnswerMinitest(MinitestSubmitAnswer);

    const {getListQuesListeningChoosen, getListQuesReadingChoosen, doReadingAfterListening} = testAction;
    // let listListening = [], listReading = [];
    // getListQuesListeningChoosen(listListening);
    // getListQuesReadingChoosen(listReading);
    doReadingAfterListening(true);

  }


  saveResultThisMinitest =()=>{
    debugger
    const {testAction, listQuestionListeningChoosenDTOS, listQuestionReadingChoosenDTOS} = this.props;
    const {saveResultMinitest} = testAction;
    let minutes =  parseInt(((this.props.minuteForMinitest * 60 + this.props.secondForMinitest) - (this.state.minute * 60 + this.state.second)) / 60);
    let seconds = ((this.props.minuteForMinitest * 60 + this.props.secondForMinitest) - (this.state.minute * 60 + this.state.second)) % 60;
    let totalTime = minutes + ":" + seconds;
    let userId = this.props.login.userInfoLogin.userId;
    let minitestSubmitAnswerDTO = {};
    // minitestSubmitAnswerDTO.listQuestionListeningChoosenDTOS = listQuestionListeningChoosenDTOS;
    // minitestSubmitAnswerDTO.listQuestionReadingChoosenDTOS = listQuestionReadingChoosenDTOS;

    if(listQuestionListeningChoosenDTOS.length > 0){
      minitestSubmitAnswerDTO.listQuestionListeningChoosenDTOS = listQuestionListeningChoosenDTOS;
    }
    else {
      minitestSubmitAnswerDTO.listQuestionListeningChoosenDTOS = this.getlistQuesListeningPrimitive();
    }

    if(listQuestionReadingChoosenDTOS.length > 0){
      minitestSubmitAnswerDTO.listQuestionReadingChoosenDTOS = listQuestionReadingChoosenDTOS;
    }
    else {
      minitestSubmitAnswerDTO.listQuestionReadingChoosenDTOS = this.getlistQuesReadingPrimitive();
    }

    let minitestResultHistoryDTO = {};
    minitestResultHistoryDTO.totalTime = totalTime;
    minitestResultHistoryDTO.check = 0;
    minitestResultHistoryDTO.userId = userId;
    minitestResultHistoryDTO.minitestSubmitAnswerDTO = minitestSubmitAnswerDTO;

    saveResultMinitest(minitestResultHistoryDTO);
    const {getListQuesListeningChoosen, getListQuesReadingChoosen} = testAction;
    let listListening = [], listReading = [];
    getListQuesListeningChoosen(listListening);
    getListQuesReadingChoosen(listReading);
  }

  doTestAgain =()=>{
    debugger
    // const {testAction} = this.props;
    // const { modalConfirmSubmit, modalTimeIsUp} = this.state;
    //
    // this.setState({
    //   load: false,
    //   modalConfirmSubmit: !modalConfirmSubmit,
    //   modalTimeIsUp: !modalTimeIsUp,
    //   loadModal: false
    //
    // })
    //
    // // this.modalTimeIsUp(false);
    //
    //
    // const {cleanTest} = testAction;
    // cleanTest();
    //
    // const {getListQuestionMinitest, getListQuesListeningChoosen, getListQuesReadingChoosen} = testAction;
    // getListQuestionMinitest();
    //
    // let listListening = [], listReading = [];
    // getListQuesListeningChoosen(listListening);
    // getListQuesReadingChoosen(listReading);
    //
    // this.linkToElement("topPage")



    window.location.replace("/pages/minitest");




  }

  doCurrentTestAgain =()=> {
    debugger
    const {testAction} = this.props;
    const {getListQuesReadingChoosen, getListQuesListeningChoosen, doCurrentTestAgain} = testAction;
    const list = [];
    getListQuesReadingChoosen(list);
    getListQuesListeningChoosen(list);
    doCurrentTestAgain(true);
    window.location.replace("/pages/minitest")
  }

  backToHome =()=>{
    debugger
    history.push("/")
  }

  //------------------modal Xác nhận bắt đầu làm bài----------------
  toggleConfirmDoTest = () => {
    debugger
    const {modalConfirmDoTest} = this.state;
    this.setState({
      modalConfirmDoTest: !modalConfirmDoTest,
      keepDoTest2: true,
      waitClosePopup: false
    })
  }



  modalConfirmDoTest = () => {
    const {modalConfirmDoTest} = this.state;
    return (
      <div>
        <Modal isOpen={modalConfirmDoTest} toggle={() => this.toggleConfirmDoTest()}>
          <div style={{padding: '2%', fontSize: '160%',
            fontWeight: '600', borderBottom: '1px solid gray', color: '#959494'}}>
            <Row>
              <Col xs={11} style={{textAlign:'center', paddingLeft: '11%'}}>
                {/*Xác nhận làm bài*/}
                <FormattedMessage id="confirm.doing.this.test" tagName="data" />
              </Col>
              <Col xs={1} style={{textAlign:'center'}}>
                <span id="sttQues" style={{fontWeight: '800'}}>
                  <a onClick={() => this.backToHome()} style={{color: 'rgb(149, 148, 148)'}}>x</a>
                </span>
              </Col>
            </Row>

          </div>
          <ModalBody>
            <div style={{textAlign: 'center'}}>
              <h3>
                {/*Bạn có chắc muốn làm bài thi này ?*/}
                <FormattedMessage id="are.you.sure.you.want.to.do.this.test" tagName="data" />
              </h3>
            </div>
          </ModalBody>
          <Row style={{padding: '2%', borderTop: '1px solid gray', margin: '0'}}>
            <Col xs="6" md="6" sm="6" lg="6">
              <a onClick={() => this.backToHome()}><Button id="btnHighLight" color="secondary">
                {/*Hủy*/}
                <FormattedMessage id="cancel" tagName="data" />
              </Button></a>{' '}
            </Col>
            <Col xs="6" md="6" sm="6" lg="6" style={{textAlign: 'right'}}>
              <button id="btnSubmit" className="btn btn-primary" onClick={() => this.toggleConfirmDoTest()}>
                {/*Làm bài*/}
                <FormattedMessage id="do.test" tagName="data" />
              </button>
            </Col>
          </Row>
        </Modal>
      </div>
    );
  }
  // ------------------end modal xác nhận bắt đầu làm bài----------------

  //------------------modal xác nhận nộp bài----------------
  toggleConfirmSubmit = () => {
    debugger
    const {modalConfirmSubmit} = this.state;
    // const {testAction} = this.props;
    // const {noTranslationWhenDoingTest} = testAction;
    // noTranslationWhenDoingTest(false);
    this.setState({
      modalConfirmSubmit: !modalConfirmSubmit
    })
    // this.saveHistoryMinitest();
  }

  modalConfirmSubmit = () => {
    const {modalConfirmSubmit} = this.state;
    return (
      <div>
        {/*<Button color="danger" >Test Modal</Button>*/}
        <Modal isOpen={modalConfirmSubmit}>

          <div style={{padding: '2%', fontSize: '160%',
            fontWeight: '600', borderBottom: '1px solid gray', color: '#959494'}}>

            <Row>
              <Col xs={11} style={{textAlign:'center', paddingLeft: '10%'}}>
                {/*Xác nhận nộp bài*/}
                <FormattedMessage id="confirm.submitting.this.test" tagName="data" />
              </Col>
              <Col xs={1} style={{textAlign:'center'}}>
                <span id="sttQues" style={{marginLeft: '26%', fontWeight: '800'}} onClick={() => this.toggleConfirmSubmit()}>x</span>
              </Col>
            </Row>

            {/*<span style={{marginLeft: '28%'}}>*/}
              {/*/!*Xác nhận nộp bài*!/*/}
              {/*<FormattedMessage id="confirm.submitting.this.test" tagName="data" />*/}
            {/*</span>*/}
            {/*<span id="sttQues" style={{marginLeft: '26%', fontWeight: '800'}} onClick={() => this.toggleConfirmSubmit()}>x</span>*/}
          </div>
          <ModalBody>
            <div style={{textAlign: 'center'}}>
              <h5>
                {/*Vẫn chưa hết thời gian làm bài !*/}
                <FormattedMessage id="there.s.still.time" tagName="data" />
              </h5>
              <h5>
                {/*Bạn có chắc muốn nộp bài ?*/}
                <FormattedMessage id="are.you.sure.to.submit" tagName="data" />
              </h5>
            </div>
          </ModalBody>
          <Row style={{padding: '2%', borderTop: '1px solid gray', margin: '0'}}>
            <Col xs="6" md="6" sm="6" lg="6">
              <Button id="btnHighLight" color="danger"
                      onClick={ () => this.toggleTimeIsUp()}>
                {/*Nộp bài*/}
                <FormattedMessage id="submit" tagName="data" />
              </Button>{' '}
            </Col>
            <Col xs="6" md="6" sm="6" lg="6" style={{textAlign: 'right'}}>
              <button id="btnSubmit" className="btn btn-primary" onClick={() => this.toggleConfirmSubmit()}>
                {/*Tiếp tục làm bài*/}
                <FormattedMessage id="continue.doing" tagName="data" />
              </button>
            </Col>
          </Row>
        </Modal>
      </div>
    );
  }
  // ------------------end modal xác nhận nộp bài----------------

  //---modal nhắc còn 5p---------
  toggleRemainder = () => {
    const {keepDoTest} = this.state;
    this.setState({
      keepDoTest: !keepDoTest
    })
  }

  modalRemainder = () => {
    const {keepDoTest} = this.state;
    return (
      <div>
        <Modal isOpen={keepDoTest}>
          <div style={{padding: '2%', fontSize: '160%',
            fontWeight: '600', borderBottom: '1px solid gray', color: '#959494'}}>
            <span style={{marginLeft: '36%'}}>Thông báo</span>
            <span id="sttQues" style={{marginLeft: '34%', fontWeight: '800'}} onClick={() => this.toggleRemainder()}>x</span>
          </div>
          <ModalBody>
            <div style={{textAlign: 'center'}}>
              <h5>
                {/*Sắp hết thời gian làm bài!*/}
                <FormattedMessage id="time.is.running.out" tagName="data" />
              </h5>
              <h5>
                {/*Còn 5 phút làm bài*/}
                <FormattedMessage id="there.are.5.minutes.left" tagName="data" />
              </h5>
            </div>
          </ModalBody>
          <Row style={{padding: '2%', borderTop: '1px solid gray', margin: '0'}}>
            <Col xs="6" md="6" sm="6" lg="6">
              <Button id="btnHighLight" color="danger"
                      onClick={ () => this.toggleConfirmSubmit(true)}>
                {/*Nộp bài*/}
                <FormattedMessage id="submit" tagName="data" />
              </Button>{' '}
            </Col>
            <Col xs="6" md="6" sm="6" lg="6" style={{textAlign: 'right'}}>
              <button id="btnSubmit" className="btn btn-primary" onClick={() => this.toggleRemainder()}>
                {/*Tiếp tục làm bài*/}
                <FormattedMessage id="continue.doing" tagName="data" />
              </button>
            </Col>
          </Row>
        </Modal>
      </div>
    );
  }
  //---end modal nhắc còn 5p-----------

  //--- modal time is up ------------
  toggleTimeIsUp = () => {
    debugger
    const {testAction, listQuestionListeningChoosenDTOS, listQuestionReadingChoosenDTOS} = this.props;
    const {saveResultMinitest} = testAction;
    let minutes =  parseInt(((this.props.minuteForMinitest * 60 + this.props.secondForMinitest) - (this.state.minute * 60 + this.state.second)) / 60);
    let seconds = ((this.props.minuteForMinitest * 60 + this.props.secondForMinitest) - (this.state.minute * 60 + this.state.second)) % 60;
    let totalTime = minutes + ":" + seconds;
    let userId = this.props.login.userInfoLogin.userId;
    let minitestSubmitAnswerDTO = {};
    // minitestSubmitAnswerDTO.listQuestionListeningChoosenDTOS = listQuestionListeningChoosenDTOS;
    // minitestSubmitAnswerDTO.listQuestionReadingChoosenDTOS = listQuestionReadingChoosenDTOS;

    if(listQuestionListeningChoosenDTOS.length > 0){
      minitestSubmitAnswerDTO.listQuestionListeningChoosenDTOS = listQuestionListeningChoosenDTOS;
    }
    else {
      minitestSubmitAnswerDTO.listQuestionListeningChoosenDTOS = this.getlistQuesListeningPrimitive();
    }

    if(listQuestionReadingChoosenDTOS.length > 0){
      minitestSubmitAnswerDTO.listQuestionReadingChoosenDTOS = listQuestionReadingChoosenDTOS;
    }
    else {
      minitestSubmitAnswerDTO.listQuestionReadingChoosenDTOS = this.getlistQuesReadingPrimitive();
    }

    let minitestResultHistoryDTO = {};
    minitestResultHistoryDTO.totalTime = totalTime;
    minitestResultHistoryDTO.check = 0;
    minitestResultHistoryDTO.userId = userId;
    minitestResultHistoryDTO.minitestSubmitAnswerDTO = minitestSubmitAnswerDTO;

    // saveResultMinitest(minitestResultHistoryDTO);

    const {modalTimeIsUp} = this.state;
    this.setState({
      modalTimeIsUp: !modalTimeIsUp,
      keepDoTest2: false
    })
  }

  modalTimeIsUp = () => {
    const {modalTimeIsUp} = this.state;
    return (
      <div>
        {/*<Button color="danger" >Test Modal</Button>*/}
        <Modal style={{maxWidth: '600px'}} isOpen={modalTimeIsUp}>
          {this.props.loadingSaveResultMinitest ? <>{this.loadingComponent}</> : null}
          {this.props.loadingSubmitAnswer === true ? <>{this.loadingComponent}</> : ""}
          <div style={{padding: '2%', fontSize: '160%',
            fontWeight: '600', borderBottom: '1px solid gray', color: '#959494'}}>
            <span style={{marginLeft: '39%'}}>
              {/*Thông báo*/}
              <FormattedMessage id="notification" tagName="data" />
            </span>
            <span id="sttQues" style={{marginLeft: '33%', fontWeight: '800'}}>
              <a onClick={() => this.backToHome()} style={{color: 'rgb(149, 148, 148)'}}>x</a>
            </span>
          </div>
          <ModalBody>
            <div style={{textAlign: 'center'}}>
              <h5>
                {/*Chúc mừng bạn đã hoàn thành bài Minitest*/}
                <FormattedMessage id="congratulations.you.finished.this.minitest" tagName="data" />
              </h5>
              <h5>
                {/*Thời gian làm bài*/}
                <FormattedMessage id="time" tagName="data" />
                :
                <span style={{color: 'red', fontWeight: '600'}}>
                {/*{(this.props.minuteForMinitest * 60 + this.props.secondForMinitest) - (this.state.minute * 60 + this.state.second)}/*/}
                {/*{this.props.minuteForMinitest * 60 + this.props.secondForMinitest} giây*/}
                {/*{this.props.minuteForMinitest - this.state.minute - 1}*/}
                {/*:*/}
                {/*{60 - this.state.second}*/}

                {
                  parseInt(((this.props.minuteForMinitest * 60 + this.props.secondForMinitest) - (this.state.minute * 60 + this.state.second)) / 60)
                }
                :
                {
                  ((this.props.minuteForMinitest * 60 + this.props.secondForMinitest) - (this.state.minute * 60 + this.state.second)) % 60
                }
              </span></h5>
            </div>
          </ModalBody>
          <Row style={{padding: '2% 0', borderTop: '1px solid gray', margin: '0'}}>
            <Col style={{textAlign: 'center'}} xs="12" md="12" sm="12" lg="12">
              <a onClick={() => this.backToHome()}>
                <Button style={{margin: '1%'}} id="btnHighLight" color="secondary">
                  {/*Hủy*/}
                  <FormattedMessage id="cancel" tagName="data" />
                </Button>
              </a>

              <Button style={{margin: '1%'}} id="btnHighLight" color="danger"
                      onClick={ () => this.doCurrentTestAgain()}>
                {/*Làm lại*/}
                <FormattedMessage id="do.this.gain" tagName="data" />
              </Button>

              <Button style={{margin: '1%'}} id="btnHighLight" color="success"
                      onClick={ () => this.doTestAgain()}>
                {/*Làm bài mới*/}
                <FormattedMessage id="do.a.new.test" tagName="data" />
              </Button>

              <button style={{margin: '1%'}} id="btnSubmit" className="btn btn-primary" onClick={() => this.submitAnswer(true)}>
                {/*Xem đáp án*/}
                <FormattedMessage id="show.answer" tagName="data" />
              </button>
            </Col>
          </Row>
        </Modal>
      </div>
    );
  }
  //--- modal time is up ------------

  //--- modal auto time is up ------------

  modalAutoTimeIsUp = (trueOrFalse) => {
    return (
      <div>
        <Modal style={{maxWidth: '600px'}} isOpen={trueOrFalse}>
          {this.props.loadingSubmitAnswer === true ? <>{this.loadingComponent}</> : ""}
          <div style={{padding: '2%', fontSize: '160%',
            fontWeight: '600', borderBottom: '1px solid gray', color: '#959494'}}>
            <span style={{marginLeft: '39%'}}>
              {/*Thông báo*/}
              <FormattedMessage id="notification" tagName="data" />
            </span>
            <span id="sttQues" style={{marginLeft: '33%', fontWeight: '800'}}>
              <a onClick={() => this.backToHome()} style={{color: 'rgb(149, 148, 148)'}}>x</a>
            </span>
          </div>
          <ModalBody>
            <div style={{textAlign: 'center'}}>
              <h5>
                {/*Chúc mừng bạn đã hoàn thành bài Minitest*/}
                <FormattedMessage id="congratulations.you.finished.this.minitest" tagName="data" />
              </h5>
              <h5>
                {/*Thời gian làm bài*/}
                <FormattedMessage id="time" tagName="data" />
                : <span style={{color: 'red', fontWeight: '600'}}>
                {/*{(this.props.minuteForMinitest * 60 + this.props.secondForMinitest) - (this.state.minute * 60 + this.state.second)}/*/}
                {/*{this.props.minuteForMinitest * 60 + this.props.secondForMinitest} giây*/}
                {/*{this.props.minuteForMinitest - this.state.minute - 1}*/}
                {/*:*/}
                {/*{60 - this.state.second}*/}

                {
                  parseInt(((this.props.minuteForMinitest * 60 + this.props.secondForMinitest) - (this.state.minute * 60 + this.state.second)) / 60)
                }
                :
                {
                  ((this.props.minuteForMinitest * 60 + this.props.secondForMinitest) - (this.state.minute * 60 + this.state.second)) % 60
                }
              </span></h5>
            </div>
          </ModalBody>
          <Row style={{padding: '2%', borderTop: '1px solid gray', margin: '0'}}>
            <Col style={{textAlign: 'center'}} xs="12"  md="12" sm="12" lg="12">
              <a onClick={() => this.backToHome()}>
                <Button style={{margin: '1%'}} id="btnHighLight" color="secondary">
                  {/*Hủy*/}
                  <FormattedMessage id="cancel" tagName="data" />
                </Button>
              </a>

              <Button style={{margin: '1%'}} id="btnHighLight" color="danger"
                      onClick={ () => this.doCurrentTestAgain()}>
                {/*Làm lại*/}
                <FormattedMessage id="do.this.gain" tagName="data" />
              </Button>

              <Button style={{margin: '1%'}} id="btnHighLight" color="success"
                      onClick={ () => this.doTestAgain()}>
                {/*Làm bài mới*/}
                <FormattedMessage id="do.a.new.test" tagName="data" />
              </Button>

              <button style={{margin: '1%'}} id="btnSubmit" className="btn btn-primary" onClick={() => this.submitAnswer(true)}>
                {/*Xem đáp án*/}
                <FormattedMessage id="show.answer" tagName="data" />
              </button>
            </Col>
          </Row>
        </Modal>
      </div>
    );
  }
  //--- modal auto time is up------------

  componentDidMount() {
    debugger
    const {testAction, checkGetListQuestionMinitest} = this.props;
    const {getListQuesListeningChoosen, getListQuesReadingChoosen, doCurrentTestAgain} = testAction;

    if(checkGetListQuestionMinitest === false){

      if(this.props.doCurrentTest === false){
        const {getListQuestionMinitest} = testAction;
        getListQuestionMinitest();
        this.setState({
          load: true
        })
        setTimeout(()=>{
          this.setState({
            load: false
          })
        }, 20000)

        let listListening = [], listReading = [];
        getListQuesListeningChoosen(listListening);
        getListQuesReadingChoosen(listReading);
      }
      else{
        doCurrentTestAgain(false);
      }

    }
    else{

    }


  }

  setCountdown =()=>{
    const {second, minute, load, keepDoTest2} = this.state;
    if(keepDoTest2 === true){
      if(minute === 0){
        if(second > 0){
          setTimeout(()=>{
            this.setState({
              second: second - 1
            })
          }, 1000)
        }
      }
      else{
        if(second > 0){
          setTimeout(()=>{
            this.setState({
              second: second - 1
            })
          }, 1000)
        }
        if(second === 0){
          setTimeout(()=>{
            this.setState({
              minute: minute - 1,
              second: 59
            })
          }, 1000)
        }
      }
    }

    return(
      <>{this.state.minute < 10 ? 0 : null}{this.state.minute}:{this.state.second < 10 ? 0 : null}{this.state.second}</>
    )
  }

  linkToElement =(string)=>{
    const element = document.getElementById(string);
    element.scrollIntoView()
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if(this.props.showAnswer !== prevProps.showAnswer){
      const element = document.getElementById("topPage");
      element.scrollIntoView()
    }
  }

  backToHistory =()=> {
    let {historyPracticesAction,historyTestAction} = this.props;
    let {changeActiveTab} = historyPracticesAction;
    changeActiveTab("3");
    historyTestAction.updateActiveTabWhenFail("3");
    history.push("/pages/profiles");
  }



  loadingComponent = (
    <div style={{
      position: 'absolute',
      zIndex: 110,
      top: 0,
      left: 0,
      width: '100%',
      height: '100%',
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      background: 'rgba(255,255,255,0.8)',
    }}>
      <Spinner color="primary" className="reload-spinner"/>
    </div>
  );

  render() {
    debugger
    const {classes, listQuestionMinitest, listQuestionAndAnswerMinitest, showAnswer,
      listQuestionListeningChoosenDTOS, listQuestionReadingChoosenDTOS, loadingQuestion, loadingSubmitAnswer} = this.props;
    const {load} = this.state;
    let totalQuestionListening = 0;
    let totalCorrectAnswerListening = 0;
    let totalQuestionReading = 0;
    let totalCorrectAnswerReading = 0;
    if(showAnswer === true){
      for(let i=0; i<4;i++){
        totalQuestionListening += listQuestionAndAnswerMinitest[i].totalQuestion;
        totalCorrectAnswerListening += listQuestionAndAnswerMinitest[i].totalCorectAnswer;
      }
      for(let i=4; i<listQuestionAndAnswerMinitest.length;i++){
        totalQuestionReading += listQuestionAndAnswerMinitest[i].totalQuestion;
        totalCorrectAnswerReading += listQuestionAndAnswerMinitest[i].totalCorectAnswer;
      }
    }

    //for render stt
    let sizeOfListeningQues = 0;
    if(listQuestionMinitest.length > 0){
      sizeOfListeningQues = this.getlistQuesListeningPrimitive().length;
      console.log("sizeOfListeningQues", sizeOfListeningQues)
    }


    return (
      <>
        <style>{`
          #buttonTest:hover{
            background-color: #91d6f3 !important;
          }
          #btnSubmit{
            background-color: #02a1ed !important;
          }

          #btnSubmit:focus-within {
            border: 1px solid black !important;
            text-shadow: 2px 2px 5px gray;
          }

          #btnSubmit2{
            background-color: #02a1ed !important;
            font-size: 150%;
            font-weight: 700;
          }
          #btnSubmit2:focus-within{
            border: 1px solid black !important;
            text-shadow: 2px 2px 5px gray;
          }

          th{
            font-size: 120% !important;
            color: black !important;
            border: 1px solid gray !important;
          }
          td{
            border: 1px solid gray !important;
          }

          #scrollQuestion {
            height: 90vh;
            overflow: scroll;
            padding-bottom: 7px;
          }

          #sttQues:hover {
            cursor: pointer;
            background-color: #ede9e9;
          }

          #btnHighLight:focus-within {
            border: 1px solid black !important;
            text-shadow: 2px 2px 5px gray;
          }

          .translation {
            border: none;
          }
          #btnBackHistory{
            margin-left: 2%;
            border-color: 1px solid blue;
            background-color: #7367f0;
            color: white;
            padding: 8.5px 10px;
            border-radius: 5px;
            border: none;
          }
          #btnBackHistory:focus-within {
            border: 1px solid black !important;
          }

          #btnHighlightRepeat:focus-within {
            border: 1px solid black !important;
          }

        `}</style>

        {this.props.checkGetListQuestionMinitest === false ?
          <Container fluid={true} style={{ backgroundColor: 'white', padding: '2%'}}>
            {loadingQuestion !== undefined ?
              <>{loadingQuestion === false ? <>{this.loadingComponent}</> : ""}</> : null
            }
            {showAnswer === false ?
              <Col id="topPage" xs="12" md="12" sm="12" lg="12">
                <h1 style={{fontWeight: '700' }}>
                  {/*Bài thi số 1*/}
                  <FormattedMessage id="test1" tagName="data" />
                </h1>
              </Col>
              :
              <Col id="topPage" xs="12" md="12" sm="12" lg="12">
                <Row>
                  <Col xs="4" md="4" sm="4" lg="4">
                    <h1 style={{fontWeight: '700' }}>
                      {/*Bài thi số 1*/}
                      <FormattedMessage id="test1" tagName="data" />
                    </h1>
                  </Col>
                  <Col xs="4" md="4" sm="4" lg="4" style={{textAlign: 'center'}}>
                    <h4 style={{color: 'gray', fontWeight: '700'}}>
                      {/*Tổng thời gian làm bài*/}
                      <FormattedMessage id="total.time.doing.this.test" tagName="data" />
                    </h4>
                    <h3 style={{color: 'orange', fontWeight: '700'}}>
                      <>
                        {
                          parseInt(((this.props.minuteForMinitest * 60 + this.props.secondForMinitest) - (this.state.minute * 60 + this.state.second)) / 60)
                        }
                        :
                        {
                          ((this.props.minuteForMinitest * 60 + this.props.secondForMinitest) - (this.state.minute * 60 + this.state.second)) % 60
                        }
                      </>
                    </h3>
                  </Col>
                  {this.props.hideBtnSavHistoryMinitest ?
                    null :
                    <Col xs="4" md="4" sm="4" lg="4" style={{textAlign: 'right'}}>
                      <button id="btnBackHistory" onClick={this.saveResultThisMinitest}>
                        {/*Lưu lại*/}
                        <FormattedMessage id="save" tagName="data" />
                      </button>
                    </Col>
                  }
                </Row>
                <Row>
                  <Col xs={12}>
                    {loadingQuestion !== undefined ?
                      <>{loadingQuestion === false ? <>{this.loadingComponent}</> : ""}</> : null
                    }
                    {
                      this.props.loadingSaveResultMinitest ? <>{this.loadingComponent}</> : ""
                    }
                    <Row className={classes.textAlign} style={{padding: '1%'}}>
                      <table className="table table-bordered"
                             style={{border: '2px solid gray',
                               backgroundColor: 'white',
                               margin: '3% 10%', color: 'black', textAlign: 'center'}}>
                        <thead>
                        <tr>
                          <th>
                            {/*Dạng bài*/}
                            <FormattedMessage id="format.test" tagName="data" />
                          </th>
                          <th>
                            {/*Phần*/}
                            <FormattedMessage id="part" tagName="data" />
                          </th>
                          <th>
                            {/*Số câu đúng*/}
                            <FormattedMessage id="number.of.correct.answer" tagName="data" />
                          </th>
                          <th>
                            {/*Tổng số câu đúng*/}
                            <FormattedMessage id="total.of.correct.answer" tagName="data" />
                          </th>
                          {/*<th>Số điểm</th>*/}
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                          <td rowSpan={4}><b>
                            {/*Bài nghe*/}
                            <FormattedMessage id="listening.part" tagName="data" />
                          </b></td>
                          <td style={{textAlign: 'left'}}>Part 1:{' '}
                            {/*Nghe tranh*/}
                            <FormattedMessage id="image.description" tagName="data" />
                          </td>
                          <td style={{textAlign: 'right'}}>{listQuestionAndAnswerMinitest[0].totalCorectAnswer}/{listQuestionAndAnswerMinitest[0].totalQuestion}</td>
                          <td rowSpan={4}><b>{totalCorrectAnswerListening}/{totalQuestionListening}</b></td>
                          {/*<td rowSpan={4}><b>{totalCorrectAnswerListening*10}</b></td>*/}
                        </tr>
                        <tr>
                          <td style={{textAlign: 'left'}}>Part 2:{' '}
                            {/*Hỏi đáp*/}
                            <FormattedMessage id="question.and.answer" tagName="data" />
                          </td>
                          <td style={{textAlign: 'right'}}>{listQuestionAndAnswerMinitest[1].totalCorectAnswer}/{listQuestionAndAnswerMinitest[1].totalQuestion}</td>
                        </tr>
                        <tr>
                          <td style={{textAlign: 'left'}}>Part 3:{' '}
                            {/*Hội thoại*/}
                            <FormattedMessage id="conversation" tagName="data" />
                          </td>
                          <td style={{textAlign: 'right'}}>{listQuestionAndAnswerMinitest[2].totalCorectAnswer}/{listQuestionAndAnswerMinitest[2].totalQuestion}</td>
                        </tr>
                        <tr>
                          <td style={{textAlign: 'left'}}>Part 4:{' '}
                            {/*Bài nói ngắn*/}
                            <FormattedMessage id="short.conversation" tagName="data" />
                          </td>
                          <td style={{textAlign: 'right'}}>{listQuestionAndAnswerMinitest[3].totalCorectAnswer}/{listQuestionAndAnswerMinitest[3].totalQuestion}</td>
                        </tr>
                        <tr>
                          <td rowSpan={4}><b>
                            {/*Bài đọc*/}
                            <FormattedMessage id="reading.part" tagName="data" />
                          </b></td>
                          <td style={{textAlign: 'left'}}>Part 5:{' '}
                            {/*Hoàn thành câu*/}
                            <FormattedMessage id="complete.sentence" tagName="data" />
                          </td>
                          <td style={{textAlign: 'right'}}>{listQuestionAndAnswerMinitest[4].totalCorectAnswer}/{listQuestionAndAnswerMinitest[4].totalQuestion}</td>
                          <td rowSpan={4}><b>{totalCorrectAnswerReading}/{totalQuestionReading}</b></td>
                          {/*<td rowSpan={4}><b>{totalCorrectAnswerReading*10}</b></td>*/}
                        </tr>
                        <tr>
                          <td style={{textAlign: 'left'}}>Part 6:{' '}
                            {/*Hoàn thành đoạn văn*/}
                            <FormattedMessage id="complete.paragraph" tagName="data" />
                          </td>
                          <td style={{textAlign: 'right'}}>{listQuestionAndAnswerMinitest[5].totalCorectAnswer}/{listQuestionAndAnswerMinitest[5].totalQuestion}</td>
                        </tr>
                        <tr>
                          <td style={{textAlign: 'left'}}>Part 7:{' '}
                            {/*Đoạn đơn*/}
                            <FormattedMessage id="single.paragraph" tagName="data" />
                          </td>
                          <td style={{textAlign: 'right'}}>{listQuestionAndAnswerMinitest[6].totalCorectAnswer}/{listQuestionAndAnswerMinitest[6].totalQuestion}</td>
                        </tr>
                        <tr>
                          <td style={{textAlign: 'left'}}>Part 8:{' '}
                            {/*Đoạn đôi*/}
                            <FormattedMessage id="double.paragraph" tagName="data" />
                          </td>
                          <td style={{textAlign: 'right'}}>{listQuestionAndAnswerMinitest[7].totalCorectAnswer}/{listQuestionAndAnswerMinitest[7].totalQuestion}</td>
                        </tr>

                        </tbody>
                      </table>
                    </Row>
                  </Col>
                </Row>
              </Col>
            }
            {this.state.waitClosePopup === true ?
              <>
                {showAnswer === false ? <>{this.modalConfirmDoTest()}</> : null}
              </>
              :
              <>
                <Row>
                  <Col xs="9" md="9" sm="9" lg="9" className="overflow">
                    <div>
                      <ListeningTest stopListening={this.state.keepDoTest2}/>
                      {this.props.doReading ?
                        <ReadingTest/>
                        :
                        null
                      }
                      {/*---------------------------modal-------------------------*/}
                      {showAnswer === false ? <>{this.modalConfirmSubmit()}</> : null}
                      {this.state.minute === 4 ?
                        <>{this.modalRemainder()}</> : null}

                      {showAnswer === false ?
                        <>
                          {this.state.minute === 0 && this.state.second === 0 ?
                            <>{this.modalAutoTimeIsUp(true)}</>
                            : null
                          }
                        </> : null
                      }

                      {showAnswer === false ? <>{this.modalTimeIsUp()}</> : null}


                      {/*---------------------------end modal-------------------------*/}

                      {showAnswer === false ?
                        <Col xs="12" md="12" sm="12" lg="12" className={classes.divSubmit}>
                          <button id="btnSubmit2" className="btn btn-primary"
                            // onClick={this.submitAnswer}
                                  onClick={() => this.toggleConfirmSubmit()}
                          >
                            {/*Nộp bài*/}
                            <FormattedMessage id="submit" tagName="data" />
                          </button>
                        </Col> : null
                      }
                    </div>
                  </Col>

                  <Col xs="3" md="3" sm="3" lg="3">
                    <div className="sticky-top" style={{zIndex: '0'}}>

                      {showAnswer === false ?
                        <div style={{display: 'flex', justifyContent: 'center'}}>
                          {/*{this.state.load === true ? <>{this.loadingComponent}</> : ""}*/}
                          <AccessTimeIcon style={{fontSize: 60, color: 'gray'}}/>
                          <div style={{padding: '6px 0 0 0px', textAlign: 'center'}}>
                            <h4 style={{color: 'gray', fontWeight: '700'}}>
                              {/*Thời gian còn lại*/}
                              <FormattedMessage id="time.remaining" tagName="data" />
                            </h4>
                            <h3 style={{color: 'orange', fontWeight: '700'}}>
                              {loadingQuestion === false ?
                                <>{this.state.minute < 10 ? 0 : null}{this.state.minute}:{this.state.second < 10 ? 0 : null}{this.state.second}</>
                                :
                                <>{this.setCountdown()}</>
                              }
                            </h3>
                          </div>
                        </div> : null
                      }

                      {showAnswer === false ?
                        <div id="scrollQuestion">
                          {/*{loadingQuestion !== undefined ?*/}
                          {/*<>{loadingQuestion === false ? <>{this.loadingComponent}</> : ""}</> : null*/}
                          {/*}*/}
                          {listQuestionMinitest.length > 0 ?
                            <>
                              {/*----------------------PART 1 ------------------------*/}
                              <Col xs="12" md="12" sm="12" lg="12" style={{marginTop: '5%'}}>
                                <h6 style={{color: '#169BD5', fontWeight: '700'}}
                                    onClick={() => this.linkToElement(String(listQuestionMinitest[0].partName))}>
                                  <u>Part 1:</u>
                                </h6>
                                <Row style={{textAlign: 'center', border: '1px solid #1da9ed', margin: '0', paddingLeft: '9%'}}>
                                  {listQuestionMinitest[0].listCategoryMinitestDTOS.map((category, indexCategory) =>
                                    <>
                                      {category.listQuestionMinitestDTOS.map((question, indexQuestion) =>
                                        <>
                                          {listQuestionListeningChoosenDTOS.length > 0 ?
                                            <>
                                              {undefined !== listQuestionListeningChoosenDTOS[question.stt - 1] ?
                                                listQuestionListeningChoosenDTOS[question.stt - 1].answerChoosen !== "null" ?
                                                  <Col xs="12" md="6" sm="12" lg="4" >
                                                    <div id="sttQues" onClick={() => this.linkToElement(String(question.stt))}
                                                         style={{
                                                           border: '1px solid black',
                                                           width: '30px',
                                                           height: '30px',
                                                           borderRadius: '50%',
                                                           marginBottom: '5px',
                                                           marginTop: '5px',
                                                           position: 'relative',
                                                           backgroundColor: '#72716a',
                                                           color: 'white'
                                                         }}>
                                                      <div style={{
                                                        position: 'absolute',
                                                        transform: 'translate(-50%, -50%)',
                                                        top: '50%',
                                                        marginLeft: '50%'
                                                      }}>
                                                        {question.stt}
                                                      </div>
                                                    </div>
                                                  </Col>
                                                  :
                                                  <Col xs="12" md="6" sm="12" lg="4">
                                                    <div id="sttQues" onClick={() => this.linkToElement(String(question.stt))}
                                                         style={{
                                                           border: '1px solid black',
                                                           width: '30px',
                                                           height: '30px',
                                                           borderRadius: '50%',
                                                           marginBottom: '5px',
                                                           marginTop: '5px',
                                                           position: 'relative'
                                                         }}>
                                                      <div style={{
                                                        position: 'absolute',
                                                        transform: 'translate(-50%, -50%)',
                                                        top: '50%',
                                                        marginLeft: '50%'
                                                      }}>
                                                        {question.stt}
                                                      </div>
                                                    </div>
                                                  </Col> : null
                                              }
                                            </>
                                            : <Col xs="12" md="6" sm="12" lg="4">
                                              <div id="sttQues" onClick={() => this.linkToElement(String(question.stt))}
                                                   style={{
                                                     border: '1px solid black',
                                                     width: '30px',
                                                     height: '30px',
                                                     borderRadius: '50%',
                                                     marginBottom: '5px',
                                                     marginTop: '5px',
                                                     position: 'relative'
                                                   }}>
                                                <div style={{
                                                  position: 'absolute',
                                                  transform: 'translate(-50%, -50%)',
                                                  top: '50%',
                                                  marginLeft: '50%'
                                                }}>
                                                  {question.stt}
                                                </div>
                                              </div>
                                            </Col>
                                          }

                                        </>
                                      )}
                                    </>
                                  )}
                                </Row>
                              </Col>
                              {/*----------------------End PART 1 ------------------------*/}

                              {/*----------------------PART 2 ------------------------*/}
                              <Col xs="12" md="12" sm="12" lg="12" style={{marginTop: '5%'}}>
                                <h6 style={{color: '#169BD5', fontWeight: '700'}}
                                    onClick={() => this.linkToElement(String(listQuestionMinitest[1].partName))}>
                                  <u>Part 2:</u>
                                </h6>
                                <Row style={{textAlign: 'center', border: '1px solid #1da9ed', margin: '0', paddingLeft: '9%'}}>
                                  {listQuestionMinitest[1].listCategoryMinitestDTOS.map((category, indexCategory) =>
                                    <>
                                      {category.listQuestionMinitestDTOS.map((question, indexQuestion) =>
                                        <>
                                          {listQuestionListeningChoosenDTOS.length > 0 ?
                                            <>
                                              {undefined !== listQuestionListeningChoosenDTOS[question.stt - 1] ?
                                                listQuestionListeningChoosenDTOS[question.stt - 1].answerChoosen !== "null" ?
                                                  <Col xs="12" md="6" sm="12" lg="4" >
                                                    <div id="sttQues" onClick={() => this.linkToElement(String(question.stt))}
                                                         style={{
                                                           border: '1px solid black',
                                                           width: '30px',
                                                           height: '30px',
                                                           borderRadius: '50%',
                                                           marginBottom: '5px',
                                                           marginTop: '5px',
                                                           position: 'relative',
                                                           backgroundColor: '#72716a',
                                                           color: 'white'
                                                         }}>
                                                      <div style={{
                                                        position: 'absolute',
                                                        transform: 'translate(-50%, -50%)',
                                                        top: '50%',
                                                        marginLeft: '50%'
                                                      }}>
                                                        {question.stt}
                                                      </div>
                                                    </div>
                                                  </Col>
                                                  :
                                                  <Col xs="12" md="6" sm="12" lg="4">
                                                    <div id="sttQues" onClick={() => this.linkToElement(String(question.stt))}
                                                         style={{
                                                           border: '1px solid black',
                                                           width: '30px',
                                                           height: '30px',
                                                           borderRadius: '50%',
                                                           marginBottom: '5px',
                                                           marginTop: '5px',
                                                           position: 'relative'
                                                         }}>
                                                      <div style={{
                                                        position: 'absolute',
                                                        transform: 'translate(-50%, -50%)',
                                                        top: '50%',
                                                        marginLeft: '50%'
                                                      }}>
                                                        {question.stt}
                                                      </div>
                                                    </div>
                                                  </Col> : null
                                              }
                                            </>
                                            : <Col xs="12" md="6" sm="12" lg="4">
                                              <div id="sttQues" onClick={() => this.linkToElement(String(question.stt))}
                                                   style={{
                                                     border: '1px solid black',
                                                     width: '30px',
                                                     height: '30px',
                                                     borderRadius: '50%',
                                                     marginBottom: '5px',
                                                     marginTop: '5px',
                                                     position: 'relative'
                                                   }}>
                                                <div style={{
                                                  position: 'absolute',
                                                  transform: 'translate(-50%, -50%)',
                                                  top: '50%',
                                                  marginLeft: '50%'
                                                }}>
                                                  {question.stt}
                                                </div>
                                              </div>
                                            </Col>
                                          }

                                        </>
                                      )}
                                    </>
                                  )}
                                </Row>
                              </Col>
                              {/*----------------------End PART 2 ------------------------*/}

                              {/*----------------------PART 3 ------------------------*/}
                              <Col xs="12" md="12" sm="12" lg="12" style={{marginTop: '5%'}}>
                                <h6 style={{color: '#169BD5', fontWeight: '700'}}
                                    onClick={() => this.linkToElement(String(listQuestionMinitest[2].partName))}>
                                  <u>Part 3:</u>
                                </h6>
                                <Row style={{textAlign: 'center', border: '1px solid #1da9ed', margin: '0', paddingLeft: '9%'}}>
                                  {listQuestionMinitest[2].listCategoryMinitestDTOS.map((category, indexCategory) =>
                                    <>
                                      {category.listQuestionMinitestDTOS.map((question, indexQuestion) =>
                                        <>
                                          {listQuestionListeningChoosenDTOS.length > 0 ?
                                            <>
                                              {undefined !== listQuestionListeningChoosenDTOS[question.stt - 1] ?
                                                listQuestionListeningChoosenDTOS[question.stt - 1].answerChoosen !== "null" ?
                                                  <Col xs="12" md="6" sm="12" lg="4" >
                                                    <div id="sttQues" onClick={() => this.linkToElement(String(question.stt))}
                                                         style={{
                                                           border: '1px solid black',
                                                           width: '30px',
                                                           height: '30px',
                                                           borderRadius: '50%',
                                                           marginBottom: '5px',
                                                           marginTop: '5px',
                                                           position: 'relative',
                                                           backgroundColor: '#72716a',
                                                           color: 'white'
                                                         }}>
                                                      <div style={{
                                                        position: 'absolute',
                                                        transform: 'translate(-50%, -50%)',
                                                        top: '50%',
                                                        marginLeft: '50%'
                                                      }}>
                                                        {question.stt}
                                                      </div>
                                                    </div>
                                                  </Col>
                                                  :
                                                  <Col xs="12" md="6" sm="12" lg="4">
                                                    <div id="sttQues" onClick={() => this.linkToElement(String(question.stt))}
                                                         style={{
                                                           border: '1px solid black',
                                                           width: '30px',
                                                           height: '30px',
                                                           borderRadius: '50%',
                                                           marginBottom: '5px',
                                                           marginTop: '5px',
                                                           position: 'relative'
                                                         }}>
                                                      <div style={{
                                                        position: 'absolute',
                                                        transform: 'translate(-50%, -50%)',
                                                        top: '50%',
                                                        marginLeft: '50%'
                                                      }}>
                                                        {question.stt}
                                                      </div>
                                                    </div>
                                                  </Col> : null
                                              }
                                            </>
                                            : <Col xs="12" md="6" sm="12" lg="4">
                                              <div id="sttQues" onClick={() => this.linkToElement(String(question.stt))}
                                                   style={{
                                                     border: '1px solid black',
                                                     width: '30px',
                                                     height: '30px',
                                                     borderRadius: '50%',
                                                     marginBottom: '5px',
                                                     marginTop: '5px',
                                                     position: 'relative'
                                                   }}>
                                                <div style={{
                                                  position: 'absolute',
                                                  transform: 'translate(-50%, -50%)',
                                                  top: '50%',
                                                  marginLeft: '50%'
                                                }}>
                                                  {question.stt}
                                                </div>
                                              </div>
                                            </Col>
                                          }

                                        </>
                                      )}
                                    </>
                                  )}
                                </Row>
                              </Col>
                              {/*----------------------End PART 3 ------------------------*/}

                              {/*----------------------PART 4 ------------------------*/}
                              <Col xs="12" md="12" sm="12" lg="12" style={{marginTop: '5%'}}>
                                <h6 style={{color: '#169BD5', fontWeight: '700'}}
                                    onClick={() => this.linkToElement(String(listQuestionMinitest[3].partName))}>
                                  <u>Part 4:</u>
                                </h6>
                                <Row style={{textAlign: 'center', border: '1px solid #1da9ed', margin: '0', paddingLeft: '9%'}}>
                                  {listQuestionMinitest[3].listCategoryMinitestDTOS.map((category, indexCategory) =>
                                    <>
                                      {category.listQuestionMinitestDTOS.map((question, indexQuestion) =>
                                        <>
                                          {listQuestionListeningChoosenDTOS.length > 0 ?
                                            <>
                                              {undefined !== listQuestionListeningChoosenDTOS[question.stt - 1] ?
                                                listQuestionListeningChoosenDTOS[question.stt - 1].answerChoosen !== "null" ?
                                                  <Col xs="12" md="6" sm="12" lg="4" >
                                                    <div id="sttQues" onClick={() => this.linkToElement(String(question.stt))}
                                                         style={{
                                                           border: '1px solid black',
                                                           width: '30px',
                                                           height: '30px',
                                                           borderRadius: '50%',
                                                           marginBottom: '5px',
                                                           marginTop: '5px',
                                                           position: 'relative',
                                                           backgroundColor: '#72716a',
                                                           color: 'white'
                                                         }}>
                                                      <div style={{
                                                        position: 'absolute',
                                                        transform: 'translate(-50%, -50%)',
                                                        top: '50%',
                                                        marginLeft: '50%'
                                                      }}>
                                                        {question.stt}
                                                      </div>
                                                    </div>
                                                  </Col>
                                                  :
                                                  <Col xs="12" md="6" sm="12" lg="4">
                                                    <div id="sttQues" onClick={() => this.linkToElement(String(question.stt))}
                                                         style={{
                                                           border: '1px solid black',
                                                           width: '30px',
                                                           height: '30px',
                                                           borderRadius: '50%',
                                                           marginBottom: '5px',
                                                           marginTop: '5px',
                                                           position: 'relative'
                                                         }}>
                                                      <div style={{
                                                        position: 'absolute',
                                                        transform: 'translate(-50%, -50%)',
                                                        top: '50%',
                                                        marginLeft: '50%'
                                                      }}>
                                                        {question.stt}
                                                      </div>
                                                    </div>
                                                  </Col> : null
                                              }
                                            </>
                                            : <Col xs="12" md="6" sm="12" lg="4">
                                              <div id="sttQues" onClick={() => this.linkToElement(String(question.stt))}
                                                   style={{
                                                     border: '1px solid black',
                                                     width: '30px',
                                                     height: '30px',
                                                     borderRadius: '50%',
                                                     marginBottom: '5px',
                                                     marginTop: '5px',
                                                     position: 'relative'
                                                   }}>
                                                <div style={{
                                                  position: 'absolute',
                                                  transform: 'translate(-50%, -50%)',
                                                  top: '50%',
                                                  marginLeft: '50%'
                                                }}>
                                                  {question.stt}
                                                </div>
                                              </div>
                                            </Col>
                                          }

                                        </>
                                      )}
                                    </>
                                  )}
                                </Row>
                              </Col>
                              {/*----------------------End PART 4 ------------------------*/}

                              {this.props.doReading ?
                                <>
                                  {/*----------------------PART 5 ------------------------*/}
                                  <Col xs="12" md="12" sm="12" lg="12" style={{marginTop: '5%'}}>
                                    <h6 style={{color: '#169BD5', fontWeight: '700'}}
                                        onClick={() => this.linkToElement(String(listQuestionMinitest[4].partName))}>
                                      <u>Part 5:</u>
                                    </h6>
                                    <Row style={{textAlign: 'center', border: '1px solid #1da9ed', margin: '0', paddingLeft: '9%'}}>
                                      {listQuestionMinitest[4].listCategoryMinitestDTOS.map((category, indexCategory) =>
                                        <>
                                          {category.listQuestionMinitestDTOS.map((question, indexQuestion) =>
                                            <>
                                              {listQuestionReadingChoosenDTOS.length > 0 ?
                                                <>
                                                  {undefined !== listQuestionReadingChoosenDTOS[question.stt - sizeOfListeningQues - 1] ?
                                                    listQuestionReadingChoosenDTOS[question.stt - sizeOfListeningQues - 1].answerChoosen !== "null" ?
                                                      <Col xs="12" md="6" sm="12" lg="4" >
                                                        <div id="sttQues" onClick={() => this.linkToElement(String(question.stt))}
                                                             style={{
                                                               border: '1px solid black',
                                                               width: '30px',
                                                               height: '30px',
                                                               borderRadius: '50%',
                                                               marginBottom: '5px',
                                                               marginTop: '5px',
                                                               position: 'relative',
                                                               backgroundColor: '#72716a',
                                                               color: 'white'
                                                             }}>
                                                          <div style={{
                                                            position: 'absolute',
                                                            transform: 'translate(-50%, -50%)',
                                                            top: '50%',
                                                            marginLeft: '50%'
                                                          }}>
                                                            {question.stt}
                                                          </div>
                                                        </div>
                                                      </Col>
                                                      :
                                                      <Col xs="12" md="6" sm="12" lg="4">
                                                        <div id="sttQues" onClick={() => this.linkToElement(String(question.stt))}
                                                             style={{
                                                               border: '1px solid black',
                                                               width: '30px',
                                                               height: '30px',
                                                               borderRadius: '50%',
                                                               marginBottom: '5px',
                                                               marginTop: '5px',
                                                               position: 'relative'
                                                             }}>
                                                          <div style={{
                                                            position: 'absolute',
                                                            transform: 'translate(-50%, -50%)',
                                                            top: '50%',
                                                            marginLeft: '50%'
                                                          }}>
                                                            {question.stt}
                                                          </div>
                                                        </div>
                                                      </Col> : <>không có gì</>
                                                  }
                                                </>
                                                : <Col xs="12" md="6" sm="12" lg="4">
                                                  <div id="sttQues" onClick={() => this.linkToElement(String(question.stt))}
                                                       style={{
                                                         border: '1px solid black',
                                                         width: '30px',
                                                         height: '30px',
                                                         borderRadius: '50%',
                                                         marginBottom: '5px',
                                                         marginTop: '5px',
                                                         position: 'relative'
                                                       }}>
                                                    <div style={{
                                                      position: 'absolute',
                                                      transform: 'translate(-50%, -50%)',
                                                      top: '50%',
                                                      marginLeft: '50%'
                                                    }}>
                                                      {question.stt}
                                                    </div>
                                                  </div>
                                                </Col>
                                              }

                                            </>
                                          )}
                                        </>
                                      )}
                                    </Row>
                                  </Col>
                                  {/*----------------------End PART 5 ------------------------*/}

                                  {/*----------------------PART 6 ------------------------*/}
                                  <Col xs="12" md="12" sm="12" lg="12" style={{marginTop: '5%'}}>
                                    <h6 style={{color: '#169BD5', fontWeight: '700'}}
                                        onClick={() => this.linkToElement(String(listQuestionMinitest[5].partName))}>
                                      <u>Part 6:</u>
                                    </h6>
                                    <Row style={{textAlign: 'center', border: '1px solid #1da9ed', margin: '0', paddingLeft: '9%'}}>
                                      {listQuestionMinitest[5].listCategoryMinitestDTOS.map((category, indexCategory) =>
                                        <>
                                          {category.listQuestionMinitestDTOS.map((question, indexQuestion) =>
                                            <>
                                              {listQuestionReadingChoosenDTOS.length > 0 ?
                                                <>
                                                  {undefined !== listQuestionReadingChoosenDTOS[question.stt-1-sizeOfListeningQues] ?
                                                    listQuestionReadingChoosenDTOS[question.stt-1-sizeOfListeningQues].answerChoosen !== "null" ?
                                                      <Col xs="12" md="6" sm="12" lg="4" >
                                                        <div id="sttQues" onClick={() => this.linkToElement(String(question.stt))}
                                                             style={{
                                                               border: '1px solid black',
                                                               width: '30px',
                                                               height: '30px',
                                                               borderRadius: '50%',
                                                               marginBottom: '5px',
                                                               marginTop: '5px',
                                                               position: 'relative',
                                                               backgroundColor: '#72716a',
                                                               color: 'white'
                                                             }}>
                                                          <div style={{
                                                            position: 'absolute',
                                                            transform: 'translate(-50%, -50%)',
                                                            top: '50%',
                                                            marginLeft: '50%'
                                                          }}>
                                                            {question.stt}
                                                          </div>
                                                        </div>
                                                      </Col>
                                                      :
                                                      <Col xs="12" md="6" sm="12" lg="4">
                                                        <div id="sttQues" onClick={() => this.linkToElement(String(question.stt))}
                                                             style={{
                                                               border: '1px solid black',
                                                               width: '30px',
                                                               height: '30px',
                                                               borderRadius: '50%',
                                                               marginBottom: '5px',
                                                               marginTop: '5px',
                                                               position: 'relative'
                                                             }}>
                                                          <div style={{
                                                            position: 'absolute',
                                                            transform: 'translate(-50%, -50%)',
                                                            top: '50%',
                                                            marginLeft: '50%'
                                                          }}>
                                                            {question.stt}
                                                          </div>
                                                        </div>
                                                      </Col> : null
                                                  }
                                                </>
                                                : <Col xs="12" md="6" sm="12" lg="4">
                                                  <div id="sttQues" onClick={() => this.linkToElement(String(question.stt))}
                                                       style={{
                                                         border: '1px solid black',
                                                         width: '30px',
                                                         height: '30px',
                                                         borderRadius: '50%',
                                                         marginBottom: '5px',
                                                         marginTop: '5px',
                                                         position: 'relative'
                                                       }}>
                                                    <div style={{
                                                      position: 'absolute',
                                                      transform: 'translate(-50%, -50%)',
                                                      top: '50%',
                                                      marginLeft: '50%'
                                                    }}>
                                                      {question.stt}
                                                    </div>
                                                  </div>
                                                </Col>
                                              }

                                            </>
                                          )}
                                        </>
                                      )}
                                    </Row>
                                  </Col>
                                  {/*----------------------End PART 6 ------------------------*/}

                                  {/*----------------------PART 7 ------------------------*/}
                                  <Col xs="12" md="12" sm="12" lg="12" style={{marginTop: '5%'}}>
                                    <h6 style={{color: '#169BD5', fontWeight: '700'}}
                                        onClick={() => this.linkToElement(String(listQuestionMinitest[6].partName))}>
                                      <u>Part 7:</u>
                                    </h6>
                                    <Row style={{textAlign: 'center', border: '1px solid #1da9ed', margin: '0', paddingLeft: '9%'}}>
                                      {listQuestionMinitest[6].listCategoryMinitestDTOS.map((category, indexCategory) =>
                                        <>
                                          {category.listQuestionMinitestDTOS.map((question, indexQuestion) =>
                                            <>
                                              {listQuestionReadingChoosenDTOS.length > 0 ?
                                                <>
                                                  {undefined !== listQuestionReadingChoosenDTOS[question.stt - sizeOfListeningQues - 1] ?
                                                    listQuestionReadingChoosenDTOS[question.stt - sizeOfListeningQues - 1].answerChoosen !== "null" ?
                                                      <Col xs="12" md="6" sm="12" lg="4" >
                                                        <div id="sttQues" onClick={() => this.linkToElement(String(question.stt))}
                                                             style={{
                                                               border: '1px solid black',
                                                               width: '30px',
                                                               height: '30px',
                                                               borderRadius: '50%',
                                                               marginBottom: '5px',
                                                               marginTop: '5px',
                                                               position: 'relative',
                                                               backgroundColor: '#72716a',
                                                               color: 'white'
                                                             }}>
                                                          <div style={{
                                                            position: 'absolute',
                                                            transform: 'translate(-50%, -50%)',
                                                            top: '50%',
                                                            marginLeft: '50%'
                                                          }}>
                                                            {question.stt}
                                                          </div>
                                                        </div>
                                                      </Col>
                                                      :
                                                      <Col xs="12" md="6" sm="12" lg="4">
                                                        <div id="sttQues" onClick={() => this.linkToElement(String(question.stt))}
                                                             style={{
                                                               border: '1px solid black',
                                                               width: '30px',
                                                               height: '30px',
                                                               borderRadius: '50%',
                                                               marginBottom: '5px',
                                                               marginTop: '5px',
                                                               position: 'relative'
                                                             }}>
                                                          <div style={{
                                                            position: 'absolute',
                                                            transform: 'translate(-50%, -50%)',
                                                            top: '50%',
                                                            marginLeft: '50%'
                                                          }}>
                                                            {question.stt}
                                                          </div>
                                                        </div>
                                                      </Col> : null
                                                  }
                                                </>
                                                : <Col xs="12" md="6" sm="12" lg="4">
                                                  <div id="sttQues" onClick={() => this.linkToElement(String(question.stt))}
                                                       style={{
                                                         border: '1px solid black',
                                                         width: '30px',
                                                         height: '30px',
                                                         borderRadius: '50%',
                                                         marginBottom: '5px',
                                                         marginTop: '5px',
                                                         position: 'relative'
                                                       }}>
                                                    <div style={{
                                                      position: 'absolute',
                                                      transform: 'translate(-50%, -50%)',
                                                      top: '50%',
                                                      marginLeft: '50%'
                                                    }}>
                                                      {question.stt}
                                                    </div>
                                                  </div>
                                                </Col>
                                              }

                                            </>
                                          )}
                                        </>
                                      )}
                                    </Row>
                                  </Col>
                                  {/*----------------------End PART 7 ------------------------*/}

                                  {/*----------------------PART 8 ------------------------*/}
                                  <Col xs="12" md="12" sm="12" lg="12" style={{marginTop: '5%'}}>
                                    <h6 style={{color: '#169BD5', fontWeight: '700'}}
                                        onClick={() => this.linkToElement(String(listQuestionMinitest[7].partName))}>
                                      <u>Part 8:</u>
                                    </h6>
                                    <Row style={{textAlign: 'center', border: '1px solid #1da9ed', margin: '0', paddingLeft: '9%'}}>
                                      {listQuestionMinitest[7].listCategoryMinitestDTOS.map((category, indexCategory) =>
                                        <>
                                          {category.listQuestionMinitestDTOS.map((question, indexQuestion) =>
                                            <>
                                              {listQuestionReadingChoosenDTOS.length > 0 ?
                                                <>
                                                  {undefined !== listQuestionReadingChoosenDTOS[question.stt - sizeOfListeningQues - 1] ?
                                                    listQuestionReadingChoosenDTOS[question.stt - sizeOfListeningQues - 1].answerChoosen !== "null" ?
                                                      <Col xs="12" md="6" sm="12" lg="4" >
                                                        <div id="sttQues" onClick={() => this.linkToElement(String(question.stt))}
                                                             style={{
                                                               border: '1px solid black',
                                                               width: '30px',
                                                               height: '30px',
                                                               borderRadius: '50%',
                                                               marginBottom: '5px',
                                                               marginTop: '5px',
                                                               position: 'relative',
                                                               backgroundColor: '#72716a',
                                                               color: 'white'
                                                             }}>
                                                          <div style={{
                                                            position: 'absolute',
                                                            transform: 'translate(-50%, -50%)',
                                                            top: '50%',
                                                            marginLeft: '50%'
                                                          }}>
                                                            {question.stt}
                                                          </div>
                                                        </div>
                                                      </Col>
                                                      :
                                                      <Col xs="12" md="6" sm="12" lg="4">
                                                        <div id="sttQues" onClick={() => this.linkToElement(String(question.stt))}
                                                             style={{
                                                               border: '1px solid black',
                                                               width: '30px',
                                                               height: '30px',
                                                               borderRadius: '50%',
                                                               marginBottom: '5px',
                                                               marginTop: '5px',
                                                               position: 'relative'
                                                             }}>
                                                          <div style={{
                                                            position: 'absolute',
                                                            transform: 'translate(-50%, -50%)',
                                                            top: '50%',
                                                            marginLeft: '50%'
                                                          }}>
                                                            {question.stt}
                                                          </div>
                                                        </div>
                                                      </Col> : null
                                                  }
                                                </>
                                                : <Col xs="12" md="6" sm="12" lg="4">
                                                  <div id="sttQues" onClick={() => this.linkToElement(String(question.stt))}
                                                       style={{
                                                         border: '1px solid black',
                                                         width: '30px',
                                                         height: '30px',
                                                         borderRadius: '50%',
                                                         marginBottom: '5px',
                                                         marginTop: '5px',
                                                         position: 'relative'
                                                       }}>
                                                    <div style={{
                                                      position: 'absolute',
                                                      transform: 'translate(-50%, -50%)',
                                                      top: '50%',
                                                      marginLeft: '50%'
                                                    }}>
                                                      {question.stt}
                                                    </div>
                                                  </div>
                                                </Col>
                                              }

                                            </>
                                          )}
                                        </>
                                      )}
                                    </Row>
                                  </Col>
                                  {/*----------------------End PART 8 ------------------------*/}
                                </>
                                :
                                null
                              }

                            </> : null
                          }
                        </div>
                        :
                        <div id="scrollQuestion">
                          {loadingQuestion !== undefined ?
                            <>{loadingQuestion === false ? <>{this.loadingComponent}</> : ""}</> : null
                          }
                          {listQuestionAndAnswerMinitest.length > 0 ?
                            <>
                              {/*----------------------PART 1-8 ------------------------*/}
                              {listQuestionAndAnswerMinitest.map((quesMinitest, index) =>
                                <Col xs="12" md="12" sm="12" lg="12" style={{marginTop: '5%'}}>
                                  <h6  style={{color: '#169BD5', marginLeft: '5%', fontWeight: '700'}}
                                       onClick={() => this.linkToElement(String(quesMinitest.partName))}>
                                    <u>Part {index + 1}:</u>
                                  </h6>
                                  <Row style={{textAlign: 'center', border: '1px solid #1da9ed', margin: '0', paddingLeft: '9%'}}>
                                    {quesMinitest.listCategoryMinitestDTOS.map((category, indexCategory) =>
                                      <>
                                        {category.listQuestionMinitestDTOS.map((question, indexQuestion) =>
                                          <>
                                            {question.indexIncorrectAnswer === -1 ?
                                              <Col xs="12" md="6" sm="12" lg="4" style={{textAlign: 'center'}}>
                                                <div id="sttQues" onClick={() => this.linkToElement(String(question.stt))}
                                                     style={{
                                                       border: '1px solid gray',
                                                       width: '30px',
                                                       height: '30px',
                                                       borderRadius: '50%',
                                                       marginBottom: '5px',
                                                       marginTop: '5px',
                                                       position: 'relative',
                                                       backgroundColor: 'green',
                                                       color: 'white'
                                                     }}>
                                                  <div style={{
                                                    position: 'absolute',
                                                    transform: 'translate(-50%, -50%)',
                                                    top: '50%',
                                                    marginLeft: '50%'
                                                  }}>
                                                    {question.stt}
                                                  </div>
                                                </div>
                                              </Col>
                                              :
                                              <Col xs="12" md="6" sm="12" lg="4" style={{textAlign: 'center'}}>
                                                <div id="sttQues" onClick={() => this.linkToElement(String(question.stt))}
                                                     style={{
                                                       border: '1px solid gray',
                                                       width: '30px',
                                                       height: '30px',
                                                       borderRadius: '50%',
                                                       marginBottom: '5px',
                                                       marginTop: '5px',
                                                       position: 'relative',
                                                       backgroundColor: 'red',
                                                       color: 'white'
                                                     }}>
                                                  <div style={{
                                                    position: 'absolute',
                                                    transform: 'translate(-50%, -50%)',
                                                    top: '50%',
                                                    marginLeft: '50%'
                                                  }}>
                                                    {question.stt}
                                                  </div>
                                                </div>
                                              </Col>
                                            }
                                          </>
                                        )}
                                      </>
                                    )}
                                  </Row>
                                </Col>
                              )}
                              {/*----------------------End PART 1-8 ------------------------*/}
                            </>
                            : null
                          }
                        </div>
                      }
                    </div>

                  </Col>
                </Row>
              </>

            }
          </Container>
          :
          <Container fluid={true} style={{ backgroundColor: 'white', padding: '2%'}}>
            <button id="btnBackHistory" onClick={this.backToHistory} style={{marrginBottom: '2%'}} title={showMessage("back")}>
              <ArrowLeft width="16" height="16"/>
            </button>
            {/*{loadingQuestion !== undefined ?*/}
              {/*<>{loadingQuestion === false ? <>{this.loadingComponent}</> : ""}</> : null*/}
            {/*}*/}
            <Col id="topPage" xs="12" md="12" sm="12" lg="12">
              <Row>
                <Col xs="4" md="4" sm="4" lg="4">
                  <h1 style={{fontWeight: '700' }}>
                    {/*Bài thi số 1*/}
                    <FormattedMessage id="test1" tagName="data" />
                  </h1>
                </Col>
                <Col xs="4" md="4" sm="4" lg="4" style={{textAlign: 'center'}}>
                  <h4 style={{color: 'gray', fontWeight: '700'}}>
                    {/*Tổng thời gian làm bài*/}
                    <FormattedMessage id="total.time.doing.this.test" tagName="data" />
                  </h4>
                  <h3 style={{color: 'orange', fontWeight: '700'}}>
                    <>
                      {listQuestionAndAnswerMinitest.length > 0 ?
                        <>{listQuestionAndAnswerMinitest[0].totalTime}</>
                        :
                        null
                      }
                    </>
                  </h3>
                </Col>
              </Row>
              <Row>
                <Col xs={12}>
                  {loadingQuestion !== undefined ?
                    <>{loadingQuestion === false ? <>{this.loadingComponent}</> : ""}</> : null
                  }
                  {listQuestionAndAnswerMinitest.length > 0 ?
                    <Row className={classes.textAlign} style={{padding: '1%'}}>
                      <table className="table table-bordered"
                             style={{border: '2px solid gray',
                               backgroundColor: 'white',
                               margin: '3% 10%', color: 'black', textAlign: 'center'}}>
                        <thead>
                        <tr>
                          <th>
                            {/*Dạng bài*/}
                            <FormattedMessage id="format.test" tagName="data" />
                          </th>
                          <th>
                            {/*Phần*/}
                            <FormattedMessage id="part" tagName="data" />
                          </th>
                          <th>
                            {/*Số câu đúng*/}
                            <FormattedMessage id="number.of.correct.answer" tagName="data" />
                          </th>
                          <th>
                            {/*Tổng số câu đúng*/}
                            <FormattedMessage id="total.of.correct.answer" tagName="data" />
                          </th>
                          {/*<th>Số điểm</th>*/}
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                          <td rowSpan={4}><b>
                            {/*Bài nghe*/}
                            <FormattedMessage id="listening.part" tagName="data" />
                          </b></td>
                          <td style={{textAlign: 'left'}}>Part 1:{' '}
                            {/*Nghe tranh*/}
                            <FormattedMessage id="image.description" tagName="data" />
                          </td>
                          <td style={{textAlign: 'right'}}>{listQuestionAndAnswerMinitest[0].totalCorectAnswer}/{listQuestionAndAnswerMinitest[0].totalQuestion}</td>
                          <td rowSpan={4}><b>{totalCorrectAnswerListening}/{totalQuestionListening}</b></td>
                          {/*<td rowSpan={4}><b>{totalCorrectAnswerListening*10}</b></td>*/}
                        </tr>
                        <tr>
                          <td style={{textAlign: 'left'}}>Part 2:{' '}
                            {/*Hỏi đáp*/}
                            <FormattedMessage id="question.and.answer" tagName="data" />
                          </td>
                          <td style={{textAlign: 'right'}}>{listQuestionAndAnswerMinitest[1].totalCorectAnswer}/{listQuestionAndAnswerMinitest[1].totalQuestion}</td>
                        </tr>
                        <tr>
                          <td style={{textAlign: 'left'}}>Part 3:{' '}
                            {/*Hội thoại*/}
                            <FormattedMessage id="conversation" tagName="data" />
                          </td>
                          <td style={{textAlign: 'right'}}>{listQuestionAndAnswerMinitest[2].totalCorectAnswer}/{listQuestionAndAnswerMinitest[2].totalQuestion}</td>
                        </tr>
                        <tr>
                          <td style={{textAlign: 'left'}}>Part 4:{' '}
                            {/*Bài nói ngắn*/}
                            <FormattedMessage id="short.conversation" tagName="data" />
                          </td>
                          <td style={{textAlign: 'right'}}>{listQuestionAndAnswerMinitest[3].totalCorectAnswer}/{listQuestionAndAnswerMinitest[3].totalQuestion}</td>
                        </tr>
                        <tr>
                          <td rowSpan={4}><b>
                            {/*Bài đọc*/}
                            <FormattedMessage id="reading.part" tagName="data" />
                          </b></td>
                          <td style={{textAlign: 'left'}}>Part 5:{' '}
                            {/*Hoàn thành câu*/}
                            <FormattedMessage id="complete.sentence" tagName="data" />
                          </td>
                          <td style={{textAlign: 'right'}}>{listQuestionAndAnswerMinitest[4].totalCorectAnswer}/{listQuestionAndAnswerMinitest[4].totalQuestion}</td>
                          <td rowSpan={4}><b>{totalCorrectAnswerReading}/{totalQuestionReading}</b></td>
                          {/*<td rowSpan={4}><b>{totalCorrectAnswerReading*10}</b></td>*/}
                        </tr>
                        <tr>
                          <td style={{textAlign: 'left'}}>Part 6:{' '}
                            {/*Hoàn thành đoạn văn*/}
                            <FormattedMessage id="complete.paragraph" tagName="data" />
                          </td>
                          <td style={{textAlign: 'right'}}>{listQuestionAndAnswerMinitest[5].totalCorectAnswer}/{listQuestionAndAnswerMinitest[5].totalQuestion}</td>
                        </tr>
                        <tr>
                          <td style={{textAlign: 'left'}}>Part 7:{' '}
                            {/*Đoạn đơn*/}
                            <FormattedMessage id="single.paragraph" tagName="data" />
                          </td>
                          <td style={{textAlign: 'right'}}>{listQuestionAndAnswerMinitest[6].totalCorectAnswer}/{listQuestionAndAnswerMinitest[6].totalQuestion}</td>
                        </tr>
                        <tr>
                          <td style={{textAlign: 'left'}}>Part 8:{' '}
                            {/*Đoạn đôi*/}
                            <FormattedMessage id="double.paragraph" tagName="data" />
                          </td>
                          <td style={{textAlign: 'right'}}>{listQuestionAndAnswerMinitest[7].totalCorectAnswer}/{listQuestionAndAnswerMinitest[7].totalQuestion}</td>
                        </tr>

                        </tbody>
                      </table>
                    </Row>
                    :
                    null
                  }
                </Col>
              </Row>
            </Col>
            <>
              <Row>
                <Col xs="9" md="9" sm="9" lg="9" className="overflow">
                  <div>
                    <ListeningTest stopListening={this.state.keepDoTest2}/>
                    <ReadingTest/>
                  </div>
                </Col>

                <Col xs="3" md="3" sm="3" lg="3">
                  <div className="sticky-top" style={{zIndex: '0'}}>

                    <div id="scrollQuestion">
                      {loadingQuestion !== undefined ?
                      <>{loadingQuestion === false ? <>{this.loadingComponent}</> : ""}</> : null
                      }
                      {listQuestionAndAnswerMinitest.length > 0 ?
                        <>
                          {/*----------------------PART 1-8 ------------------------*/}
                          {listQuestionAndAnswerMinitest.map((quesMinitest, index) =>
                            <Col xs="12" md="12" sm="12" lg="12" style={{marginTop: '5%'}}>
                              <h6  style={{color: '#169BD5', marginLeft: '5%', fontWeight: '700'}}
                                   onClick={() => this.linkToElement(String(quesMinitest.partName))}>
                                <u>Part {index + 1}:</u>
                              </h6>
                              <Row style={{textAlign: 'center', border: '1px solid #1da9ed', margin: '0', paddingLeft: '9%'}}>
                                {quesMinitest.listCategoryMinitestDTOS.map((category, indexCategory) =>
                                  <>
                                    {category.listQuestionMinitestDTOS.map((question, indexQuestion) =>
                                      <>
                                        {question.indexIncorrectAnswer === -1 ?
                                          <Col xs="12" md="6" sm="12" lg="4" style={{textAlign: 'center'}}>
                                            <div id="sttQues" onClick={() => this.linkToElement(String(question.stt))}
                                                 style={{
                                                   border: '1px solid gray',
                                                   width: '30px',
                                                   height: '30px',
                                                   borderRadius: '50%',
                                                   marginBottom: '5px',
                                                   marginTop: '5px',
                                                   position: 'relative',
                                                   backgroundColor: 'green',
                                                   color: 'white'
                                                 }}>
                                              <div style={{
                                                position: 'absolute',
                                                transform: 'translate(-50%, -50%)',
                                                top: '50%',
                                                marginLeft: '50%'
                                              }}>
                                                {question.stt}
                                              </div>
                                            </div>
                                          </Col>
                                          :
                                          <Col xs="12" md="6" sm="12" lg="4" style={{textAlign: 'center'}}>
                                            <div id="sttQues" onClick={() => this.linkToElement(String(question.stt))}
                                                 style={{
                                                   border: '1px solid gray',
                                                   width: '30px',
                                                   height: '30px',
                                                   borderRadius: '50%',
                                                   marginBottom: '5px',
                                                   marginTop: '5px',
                                                   position: 'relative',
                                                   backgroundColor: 'red',
                                                   color: 'white'
                                                 }}>
                                              <div style={{
                                                position: 'absolute',
                                                transform: 'translate(-50%, -50%)',
                                                top: '50%',
                                                marginLeft: '50%'
                                              }}>
                                                {question.stt}
                                              </div>
                                            </div>
                                          </Col>
                                        }
                                      </>
                                    )}
                                  </>
                                )}
                              </Row>
                            </Col>
                          )}
                          {/*----------------------End PART 1-8 ------------------------*/}
                        </>
                        : null
                      }
                    </div>
                  </div>

                </Col>
              </Row>
            </>
          </Container>
        }
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    listQuestionMinitest : state.doTestReducer.listQuestionMinitest,
    listQuestionListeningChoosenDTOS: state.doTestReducer.listQuestionListeningChoosenDTOS,
    listQuestionReadingChoosenDTOS: state.doTestReducer.listQuestionReadingChoosenDTOS,
    listQuestionAndAnswerMinitest: state.doTestReducer.listQuestionAndAnswerMinitest,
    loadingQuestion: state.doTestReducer.loadingQuestion,
    loadingSubmitAnswer: state.doTestReducer.loadingSubmitAnswer,
    showAnswer: state.doTestReducer.showAnswer,
    doCurrentTest: state.doTestReducer.doCurrentTest,
    doReading: state.doTestReducer.doReading,
    minuteForMinitest: state.doTestReducer.minuteForMinitest,
    secondForMinitest: state.doTestReducer.secondForMinitest,
    noTranslating: state.doTestReducer.noTranslating,
    loadingSaveResultMinitest: state.doTestReducer.loadingSaveResultMinitest,
    checkGetListQuestionMinitest: state.doTestReducer.checkGetListQuestionMinitest,
    showBtnBackHistoryMinitest: state.doTestReducer.showBtnBackHistoryMinitest,
    hideBtnSavHistoryMinitest: state.doTestReducer.hideBtnSavHistoryMinitest,
    login: state.auth.login
  }
}
const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    testAction : bindActionCreators(testAction , dispatch),
    historyPracticesAction: bindActionCreators(historyPracticesAction,dispatch),
    historyTestAction: bindActionCreators(historyTestAction,dispatch)
  }
}
const withConnect = connect(mapStateToProps, mapDispatchToProps)


export default compose(withStyles(styles), withConnect)(Index);
