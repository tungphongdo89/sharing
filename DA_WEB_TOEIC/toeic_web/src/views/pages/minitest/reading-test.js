import React, { Component } from 'react';
import {connect} from "react-redux";
import {bindActionCreators, compose} from "redux";
import {
  Badge,
  Col,
  Container,
  Form,
  FormGroup,
  Input,
  Label,
  PopoverBody,
  Row,
  Spinner,
  UncontrolledPopover
} from "reactstrap"
import styles from "./style";
import {withStyles} from "@material-ui/core";
import {ReactPlayerReading} from "../Practices/ListeningToeicShortTalkingPractice/style";

import testAction from "../../../redux/actions/do-test-management";
import * as Icon from "react-feather";
import {FormattedMessage} from "react-intl";

class ReadingTest extends Component {

  constructor(props) {
    super(props);
    this.state = {
      load: false,
      listQuesReading: [],
      listSttQuesListeningChoosen: this.props.listSttQuestionListeningChoosen,
      listSttQuesReadingChoosen: this.props.listSttQuestionReadingChoosen,
      listQuestionReadingPrimitive: []
    }
  }


  cutAnswerToChoose(answerToChoose){
    let char = 0;
    let answerArray = [];
    let subAnswer = "";

    let count=0,index=0; index = 0;
    for( count=-1,index=-2; index != -1; count++,index=answerToChoose.indexOf("|",index+1) );

    for(let i=1;i<=count;i++){
      char = answerToChoose.indexOf("|");
      subAnswer = answerToChoose.substring(0, char);
      answerArray.push(subAnswer);
      answerToChoose = answerToChoose.substring(char + 1);
    }
    return answerArray;
  }

  lowerCaseAll =(string)=>{
    return string.toLowerCase();
  }
  upperCaseFirstLetter =(string)=>{
    string = this.lowerCaseAll(string);
    return string.trim().charAt(0).toUpperCase() + string.trim().slice(1);
  }

  cutSubAnswer(subAnswer){
    let index = subAnswer.indexOf(".");
    let title = subAnswer.substring(0, index);
    let content = subAnswer.substring(index+1);

    let array = [];
    array.push(title);
    array.push(content);
    return array;
  }

  cutInputValue(value){
    let index = value.indexOf("|");
    let title = value.substring(0, index);
    let content = value.substring(index+1);

    let array = [];
    array.push(title);
    array.push(content);
    return array;
  }

  compare =(a, b)=>{
    if ( a.stt < b.stt ){
      return -1;
    }
    if ( a.stt > b.stt ){
      return 1;
    }
    return 0;
  }

  getlistQuesReadingPrimitive =(question2)=> {
    const list = this.props.listQuestionMinitest;
    let {listQuestionReadingPrimitive} = this.state;

    //chọn lần 1
    if(listQuestionReadingPrimitive.length === 0){
      let listQuesReadingPrimitive = [];

      for(let i=0;i<list[4].listCategoryMinitestDTOS[0].listQuestionMinitestDTOS.length;i++){
        let question = {};
        question.id = Number(list[4].listCategoryMinitestDTOS[0].listQuestionMinitestDTOS[i].id);
        question.stt = Number(list[4].listCategoryMinitestDTOS[0].listQuestionMinitestDTOS[i].stt);
        question.answerChoosen = "null";
        question.indexSubAnswer = null;
        if(list[4].listCategoryMinitestDTOS[0].categoryId === null){
          question.categoryId = 0;
        }
        else{
          question.categoryId = Number(list[4].listCategoryMinitestDTOS[0].categoryId);
        }

        question.partName = list[4].partName;
        if(question2.id === question.id){
          listQuesReadingPrimitive.push(question2)
        }
        else{
          listQuesReadingPrimitive.push(question)
        }

      }


      for(let i=0;i<list[5].listCategoryMinitestDTOS.length;i++){
        for(let j=0;j<list[5].listCategoryMinitestDTOS[i].listQuestionMinitestDTOS.length;j++){
          let question = {};
          question.id = Number(list[5].listCategoryMinitestDTOS[i].listQuestionMinitestDTOS[j].id);
          question.stt = Number(list[5].listCategoryMinitestDTOS[i].listQuestionMinitestDTOS[j].stt);
          question.answerChoosen = "null";
          question.indexSubAnswer = null;
          question.categoryId = Number(list[5].listCategoryMinitestDTOS[i].categoryId);
          question.partName = list[5].partName;
          if(question2.id === question.id){
            listQuesReadingPrimitive.push(question2)
          }
          else{
            listQuesReadingPrimitive.push(question)
          }
        }
      }

      for(let i=0;i<list[6].listCategoryMinitestDTOS.length;i++){
        for(let j=0;j<list[6].listCategoryMinitestDTOS[i].listQuestionMinitestDTOS.length;j++){
          let question = {};
          question.id = Number(list[6].listCategoryMinitestDTOS[i].listQuestionMinitestDTOS[j].id);
          question.stt = Number(list[6].listCategoryMinitestDTOS[i].listQuestionMinitestDTOS[j].stt);
          question.answerChoosen = "null";
          question.indexSubAnswer = null;
          question.categoryId = Number(list[6].listCategoryMinitestDTOS[i].categoryId);
          question.partName = list[6].partName;
          if(question2.id === question.id){
            listQuesReadingPrimitive.push(question2)
          }
          else{
            listQuesReadingPrimitive.push(question)
          }
        }
      }

      for(let i=0;i<list[7].listCategoryMinitestDTOS.length;i++){
        for(let j=0;j<list[7].listCategoryMinitestDTOS[i].listQuestionMinitestDTOS.length;j++){
          let question = {};
          question.id = Number(list[7].listCategoryMinitestDTOS[i].listQuestionMinitestDTOS[j].id);
          question.stt = Number(list[7].listCategoryMinitestDTOS[i].listQuestionMinitestDTOS[j].stt);
          question.answerChoosen = "null";
          question.indexSubAnswer = null;
          question.categoryId = Number(list[7].listCategoryMinitestDTOS[i].categoryId);
          question.partName = list[7].partName;
          if(question2.id === question.id){
            listQuesReadingPrimitive.push(question2)
          }
          else{
            listQuesReadingPrimitive.push(question)
          }
        }
      }

      this.setState({
        listQuestionReadingPrimitive: listQuesReadingPrimitive
      })
      return listQuesReadingPrimitive;
    }
    // >= chọn lần 2
    else{
      for(let i=0; i<listQuestionReadingPrimitive.length;i++){
        if(question2.id === listQuestionReadingPrimitive[i].id){
          listQuestionReadingPrimitive[i].id = question2.id;
          listQuestionReadingPrimitive[i].stt = question2.stt;
          listQuestionReadingPrimitive[i].answerChoosen = question2.answerChoosen;
          listQuestionReadingPrimitive[i].indexSubAnswer = question2.indexSubAnswer;
          listQuestionReadingPrimitive[i].categoryId = question2.categoryId;
          listQuestionReadingPrimitive[i].partName = question2.partName;
        }
      }
      this.setState({
        listQuestionReadingPrimitive: listQuestionReadingPrimitive
      })
      return listQuestionReadingPrimitive;
    }


  }

  onChangeValue =(e)=> {
    const {testAction} = this.props;
    const {listQuesReading} = this.state;
    console.log(e.target.value);
    let listQuestionReading = listQuesReading;
    let question = {};

    let answerChoosen = this.cutInputValue(e.target.value)[0].trim();
    let id = this.cutSubAnswer(this.cutInputValue(e.target.value)[1])[0];
    let indexSubAnswerAndCategoryIdAndSttAndPartName = this.cutSubAnswer(this.cutInputValue(e.target.value)[1])[1];
    let indexSubAnswer = this.cutInputValue(indexSubAnswerAndCategoryIdAndSttAndPartName)[0];
    let categoryIdAndSttAndPartName = this.cutInputValue(indexSubAnswerAndCategoryIdAndSttAndPartName)[1];
    let categoryId = this.cutSubAnswer(categoryIdAndSttAndPartName)[0];
    let sttAndPartName = this.cutSubAnswer(categoryIdAndSttAndPartName)[1];
    let stt = this.cutInputValue(sttAndPartName)[0];
    let partName = this.cutInputValue(sttAndPartName)[1];

    question.id = Number(id);
    question.stt = Number(stt);
    question.answerChoosen = answerChoosen;
    question.indexSubAnswer = Number(indexSubAnswer);
    if(categoryId === "null"){
      question.categoryId = 0;
    }
    else{
      question.categoryId = Number(categoryId);
    }
    question.partName = partName;
    //

    //list mới
    let list = this.getlistQuesReadingPrimitive(question);
    //list mới

    const {getListQuesReadingChoosen} = testAction;
    getListQuesReadingChoosen(list);
  }

  checkingAnswerChoosen =(indexIncorrectAnswer, indexCorrectAnswer, indexSubAnswer)=>{
    // th câu đúng
    if(indexIncorrectAnswer !== -1){
      if(indexSubAnswer === indexIncorrectAnswer){
        return true;
      }
      else{
        return false;
      }
    }
    //th câu sai
    else{
      if(indexSubAnswer === indexCorrectAnswer){
        return true;
      }
      else {
        return false;
      }
    }
  }

  loadingComponent = (
    <div style={{
      position: 'absolute',
      zIndex: 110,
      top: 0,
      left: 0,
      width: '100%',
      height: '100%',
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      background: 'rgba(255,255,255,0.8)',
    }}>
      <Spinner color="primary" className="reload-spinner"/>
    </div>
  );


  render() {
    debugger
    const {classes, listQuestionMinitest , listQuestionAndAnswerMinitest, showAnswer, loadingQuestion,
      loadingSubmitAnswer} = this.props;
    // const {showAnswer} = this.state;

    return (
      <>
        {this.props.checkGetListQuestionMinitest === false ?
          <div className="mt-3">
            {loadingQuestion !== undefined ?
              <>{loadingQuestion === false ? <>{this.loadingComponent}</> : ""}</> : null
            }
            <Badge style={{backgroundColor: "rgba(32 171 231)", width: "100%", height: "60%", marginLeft: "0%",
              padding: "10px 0 0 0", borderRadius:'10px'}} className="badge-xl block">
              <div>
                <div className="text-left" style={{marginLeft: '0px'}}>
                  <div className={classes.titleModal}>
                    <h3 style={{color:'white', marginLeft: '2%', top: '40%', transform: 'translateY(-50%)', position: 'absolute'}}>
                      Reading Test
                    </h3>
                  </div>
                </div>
              </div>
            </Badge>
            <div style={{position: 'relative', margin: '0 1%'}}>

              <Row className={classes.bodyModal} style={{margin: '0 0% 0 0%', padding: '0 0%', width: "100%" }}>
                <div style={{margin:'3% 3%'}}>
                  {
                    listQuestionAndAnswerMinitest.length > 0 ? null :
                      <h5>In the Reading test, you will read a variety of texts and answer several different types of reading comprehensive questions.

                        The entire Reading test will last 75 minutes. There are three parts, and directions are given for each part. You are

                        encouraged to answer as many questions as possible within the time allowed. You must mark your answers on the

                        separate answer sheet. Do not write your answer in your test book.</h5>
                  }

                  {showAnswer === false ?
                    <>
                      {listQuestionMinitest.length > 0 ?
                        <>
                          {/*----------------------------------------PART5-----------------------------------------------*/}
                          <div className="mt-3">
                            <h1 id="PART5" style={{fontWeight: '700'}}>PART 5</h1>
                            <h5>Directions: A word or phrase is missing in each of the sentences below. Four answer choices
                              are given below each

                              sentence. Select the best answer to complete the sentence. Then mark the letter (A), (B), (C),
                              or (D) on your answer sheet</h5>

                            <Row style={{marginTop: '3%', marginLeft: '6%'}}>
                              {listQuestionMinitest[4].listCategoryMinitestDTOS[0].listQuestionMinitestDTOS.map((question, indexQuestion) =>
                                <Col id={String(question.stt)} xs={6}>
                                  <h4>{question.stt}. {question.name}</h4>
                                  <div className="w-100">
                                    {question ? this.cutAnswerToChoose(question.answersToChoose).map((subAnswer, indexSubAnswer) =>
                                      <FormGroup check style={{marginLeft: '10%', marginBottom: '3%'}}>
                                        <Label check className={classes.labelRadioButton} style={{cursor: 'pointer'}}>
                                          <Input type="radio" name={question.stt}
                                                 onChange={this.onChangeValue}
                                                 value={subAnswer + "|" + question.id + "." + indexSubAnswer
                                                 + "|" + listQuestionMinitest[4].listCategoryMinitestDTOS[0].categoryId + "." + question.stt
                                                 + "|" + listQuestionMinitest[4].partName}
                                          />
                                          ({indexSubAnswer === 0 ? "A" : (indexSubAnswer === 1 ? "B" : (indexSubAnswer === 2 ? "C" : "D"))}) {this.upperCaseFirstLetter(subAnswer)}
                                        </Label>
                                      </FormGroup>
                                    ) : null}
                                  </div>
                                </Col>
                              )}
                            </Row>

                          </div>
                          {/*----------------------------------------end PART5-----------------------------------------------*/}


                          {/*----------------------------------------PART6-----------------------------------------------*/}
                          <div className="mt-3">
                            <h1 id="PART6" style={{fontWeight: '700'}}>PART 6</h1>
                            <h5>Directions: Read the text that follow. A word or phrase is missing in some of the sentences.
                              Four answer choices are given

                              below each of the sentences. Select the best answer to complete the text. Then mark the letter
                              (A), (B), (C), or (D) on your

                              answer sheet</h5>

                            {listQuestionMinitest[5].listCategoryMinitestDTOS.map((category, indexCategory) =>
                              <>
                                <h4 style={{fontWeight: '700', marginTop: '5%'}}>6.{indexCategory + 1}.</h4>
                                <div style={{marginLeft: '7%'}}><h4>
                                  Question {category.listQuestionMinitestDTOS[0].name}-
                                  {category.listQuestionMinitestDTOS[category.listQuestionMinitestDTOS.length - 1].name}:
                                  refer to the following email
                                </h4></div>
                                <Row style={{marginTop: '3%', marginLeft: '3%'}}>
                                  <Col xs="12" md="5" sm="12" lg="5">
                                    <div style={{backgroundColor: '#d3d3d347', padding: '10px'}}>
                                      {category.pathFile1}
                                    </div>
                                  </Col>
                                  <Col xs="12" md="7" sm="12" lg="7">
                                    {category.listQuestionMinitestDTOS.map((question, indexQuestion) =>
                                      <Col id={String(question.stt)} xs="12" md="12" sm="12" lg="12">
                                        <h4>{question.stt}. {question.name}</h4>
                                        {this.cutAnswerToChoose(question.answersToChoose).map((subAnswer, indexSubAnswer) =>
                                          <FormGroup check style={{marginLeft: '8%', marginBottom: '2%'}}>
                                            <Label check className={classes.labelRadioButton} style={{cursor: 'pointer'}}>
                                              <Input type="radio" name={question.stt}
                                                     onChange={this.onChangeValue}
                                                     value={subAnswer + "|" + question.id + "." + indexSubAnswer
                                                     + "|" + category.categoryId + "." + question.stt
                                                     + "|" + listQuestionMinitest[5].partName}
                                              />
                                              ({indexSubAnswer === 0 ? "A" : (indexSubAnswer === 1 ? "B" : (indexSubAnswer === 2 ? "C" : "D"))}) {this.upperCaseFirstLetter(subAnswer)}
                                            </Label>
                                          </FormGroup>
                                        )}
                                      </Col>
                                    )}
                                  </Col>
                                </Row>
                              </>
                            )}
                          </div>
                          {/*---------------------------------------end PART6--------------------------------------------*/}


                          {/*---------------------------------------PART7--------------------------------------------*/}
                          <div className="mt-3">
                            <h3 id="PART7" style={{fontWeight: '700'}}>PART 7</h3>
                            <h5>Directions: In this part you will read a selection of texts, such as magazine and newspaper
                              articles, letters and

                              advertisements. Each text is followed by several questions. Select the best answer for each
                              question and mark the letter

                              (A), (B), (C), or (D) on your answer sheet</h5>
                            {/*<Row style={{marginTop: '4%'}}>*/}
                            {/*<Col xs={2}><h5>Example:</h5></Col>*/}
                            {/*<Col xs={4}><h5>Refer to the following article</h5></Col>*/}
                            {/*<Col xs={12} style={{marginLeft: '2%', marginTop: '2%'}}>*/}
                            {/*<fieldset className={classes.fieldset}>*/}
                            {/*<h3 style={{textAlign: 'center', color: 'green'}}><i></i></h3>*/}
                            {/*<p style={{lineHeight: '2rem', marginLeft: '1%'}}>These days, it’s easy to hear the music*/}
                            {/*you like, anytime you want. Almost any piece of music you can think of is available*/}
                            {/*online.</p>*/}
                            {/*</fieldset>*/}
                            {/*</Col>*/}
                            {/*<Row style={{marginTop: '2%', marginLeft: '10%', width: '100%'}}>*/}
                            {/*<Col xs={5}><h6 style={{fontWeight: '700'}}>What is the man looking for?</h6></Col>*/}
                            {/*<Col xs={7}>*/}
                            {/*<FormGroup check style={{marginLeft: '5%', marginBottom: '3%'}}>*/}
                            {/*<Label check className={classes.labelRadioButton} style={{cursor: 'pointer'}}>*/}
                            {/*<Input type="radio" name={"subAnswer"}*/}
                            {/*/>{'   '}*/}
                            {/*(A) A diverse range of applicants.*/}
                            {/*</Label>*/}
                            {/*</FormGroup>*/}
                            {/*<FormGroup check style={{marginLeft: '5%', marginBottom: '3%'}}>*/}
                            {/*<Label check className={classes.labelRadioButton} style={{cursor: 'pointer'}}>*/}
                            {/*<Input type="radio" name={"subAnswer"}*/}
                            {/*/>{'   '}*/}
                            {/*(B)A glassware merchant.*/}
                            {/*</Label>*/}
                            {/*</FormGroup>*/}
                            {/*<FormGroup check style={{marginLeft: '5%', marginBottom: '3%'}}>*/}
                            {/*<Label check className={classes.labelRadioButton} style={{cursor: 'pointer'}}>*/}
                            {/*<Input type="radio" name={"subAnswer"}*/}
                            {/*/>{'   '}*/}
                            {/*(C) A pair of eyeglasses.*/}
                            {/*</Label>*/}
                            {/*</FormGroup>*/}
                            {/*<FormGroup check style={{marginLeft: '5%', marginBottom: '3%'}}>*/}
                            {/*<Label check className={classes.labelRadioButton} style={{cursor: 'pointer'}}>*/}
                            {/*<Input type="radio" name={"subAnswer"}*/}
                            {/*/>{'   '}*/}
                            {/*(D) A warehouse.*/}
                            {/*</Label>*/}
                            {/*</FormGroup>*/}
                            {/*</Col>*/}
                            {/*</Row>*/}
                            {/*<h6 style={{marginLeft: '15%', fontStyle: 'italic'}}>The answer is B.</h6>*/}
                            {/*</Row>*/}

                            {listQuestionMinitest[6].listCategoryMinitestDTOS.map((category, indexCategory) =>
                              <>
                                <h4 style={{fontWeight: '700', marginTop: '5%'}}>7.{indexCategory + 1}.</h4>
                                <Row style={{marginTop: '3%'}}>
                                  {category.pathFile1.includes("https://") === true ?
                                    <Col xs="12" md="12" sm="12" lg="12">
                                      <img className={classes.imgClass}
                                           style={{width: '100%', border: '5px solid lightgray'}}
                                           src={category.pathFile1}/>
                                    </Col>
                                    :
                                    <Col xs="12" md="12" sm="12" lg="12">
                                      <div style={{backgroundColor: '#d3d3d347', padding: '10px'}}>
                                        {category.pathFile1}
                                      </div>
                                    </Col>
                                  }
                                  <Col xs="12" md="12" sm="12" lg="12" style={{marginTop: '5%'}}>
                                    {category.listQuestionMinitestDTOS.map((question, indexQuestion) =>
                                      <Row id={String(question.stt)} style={{marginTop: '3%', marginLeft: '10%'}}>
                                        <Col xs="6" md="6" sm="6" lg="6">
                                          <h4>{question.stt}. {question.name}</h4>
                                        </Col>
                                        <Col xs="6" md="6" sm="6" lg="6">
                                          {this.cutAnswerToChoose(question.answersToChoose).map((subAnswer, indexSubAnswer) =>
                                            <FormGroup check style={{marginLeft: '8%', marginBottom: '5%'}}>
                                              <Label check className={classes.labelRadioButton} style={{cursor: 'pointer'}}>
                                                <Input type="radio" name={question.stt}
                                                       onChange={this.onChangeValue}
                                                       value={subAnswer + "|" + question.id + "." + indexSubAnswer
                                                       + "|" + category.categoryId + "." + question.stt
                                                       + "|" + listQuestionMinitest[6].partName}
                                                />
                                                ({indexSubAnswer === 0 ? "A" : (indexSubAnswer === 1 ? "B" : (indexSubAnswer === 2 ? "C" : "D"))}) {this.upperCaseFirstLetter(subAnswer)}
                                              </Label>
                                            </FormGroup>
                                          )}
                                        </Col>
                                      </Row>
                                    )}
                                  </Col>
                                </Row>
                              </>
                            )}
                          </div>
                          {/*--------------------------------------- end PART7--------------------------------------------*/}


                          {/*---------------------------------------PART8--------------------------------------------*/}
                          <div className="mt-3">
                            <h1 id="PART8" style={{fontWeight: '700'}}>PART 8</h1>
                            <h5>Directions: In this part you will read a selection of texts, such as magazine and newspaper
                              articles, letters and

                              advertisements. Each text is followed by several questions. Select the best answer for each
                              question and mark the letter

                              (A), (B), (C), or (D) on your answer sheet</h5>
                            {/*<Row style={{marginTop: '4%'}}>*/}
                            {/*<Col xs={2}><h5>Example:</h5></Col>*/}
                            {/*<Col xs={4}><h5>Refer to the following article</h5></Col>*/}
                            {/*<Col xs={12} style={{marginLeft: '2%', marginTop: '2%'}}>*/}
                            {/*<fieldset className={classes.fieldset}>*/}
                            {/*<h3 style={{textAlign: 'center', color: 'green'}}><i></i></h3>*/}
                            {/*<p style={{lineHeight: '2rem', marginLeft: '1%'}}>These days, it’s easy to hear the music*/}
                            {/*you like, anytime you want. Almost any piece of music you can think of is available*/}
                            {/*online.</p>*/}
                            {/*</fieldset>*/}
                            {/*</Col>*/}
                            {/*<Row style={{marginTop: '2%', marginLeft: '10%', width: '100%'}}>*/}
                            {/*<Col xs={5}><h6 style={{fontWeight: '700'}}>What is the man looking for?</h6></Col>*/}
                            {/*<Col xs={7}>*/}
                            {/*<FormGroup check style={{marginLeft: '5%', marginBottom: '3%'}}>*/}
                            {/*<Label check className={classes.labelRadioButton} style={{cursor: 'pointer'}}>*/}
                            {/*<Input type="radio" name={"subAnswer"}*/}
                            {/*/>{'   '}*/}
                            {/*(A) A diverse range of applicants.*/}
                            {/*</Label>*/}
                            {/*</FormGroup>*/}
                            {/*<FormGroup check style={{marginLeft: '5%', marginBottom: '3%'}}>*/}
                            {/*<Label check className={classes.labelRadioButton} style={{cursor: 'pointer'}}>*/}
                            {/*<Input type="radio" name={"subAnswer"}*/}
                            {/*/>{'   '}*/}
                            {/*(B)A glassware merchant.*/}
                            {/*</Label>*/}
                            {/*</FormGroup>*/}
                            {/*<FormGroup check style={{marginLeft: '5%', marginBottom: '3%'}}>*/}
                            {/*<Label check className={classes.labelRadioButton} style={{cursor: 'pointer'}}>*/}
                            {/*<Input type="radio" name={"subAnswer"}*/}
                            {/*/>{'   '}*/}
                            {/*(C) A pair of eyeglasses.*/}
                            {/*</Label>*/}
                            {/*</FormGroup>*/}
                            {/*<FormGroup check style={{marginLeft: '5%', marginBottom: '3%'}}>*/}
                            {/*<Label check className={classes.labelRadioButton} style={{cursor: 'pointer'}}>*/}
                            {/*<Input type="radio" name={"subAnswer"}*/}
                            {/*/>{'   '}*/}
                            {/*(D) A warehouse.*/}
                            {/*</Label>*/}
                            {/*</FormGroup>*/}
                            {/*</Col>*/}
                            {/*</Row>*/}
                            {/*<h6 style={{marginLeft: '15%', fontStyle: 'italic'}}>The answer is B.</h6>*/}
                            {/*</Row>*/}

                            {listQuestionMinitest[7].listCategoryMinitestDTOS.map((category, indexCategory) =>
                              <>
                                <h4 style={{fontWeight: '700', marginTop: '5%'}}>8.{indexCategory + 1}.</h4>
                                <Row style={{marginTop: '3%'}}>
                                  {category.pathFile1.includes("https://") === true ?
                                    <Col xs="12" md="12" sm="12" lg="12">
                                      <img className={classes.imgClass}
                                           style={{width: '100%', border: '5px solid lightgray'}}
                                           src={category.pathFile1}/>
                                    </Col>
                                    :
                                    <Col xs="12" md="12" sm="12" lg="12">
                                      <div style={{backgroundColor: '#d3d3d347', padding: '10px'}}>
                                        {category.pathFile1}
                                      </div>
                                    </Col>
                                  }
                                  <Col xs="12" md="12" sm="12" lg="12" style={{marginTop: '5%'}}>
                                    {category.listQuestionMinitestDTOS.map((question, indexQuestion) =>
                                      <Row id={String(question.stt)} style={{marginTop: '3%', marginLeft: '10%'}}>
                                        <Col xs="6" md="6" sm="6" lg="6">
                                          <h4>{question.stt}. {question.name}</h4>
                                        </Col>
                                        <Col xs="6" md="6" sm="6" lg="6">
                                          {this.cutAnswerToChoose(question.answersToChoose).map((subAnswer, indexSubAnswer) =>
                                            <FormGroup check style={{marginLeft: '8%', marginBottom: '5%'}}>
                                              <Label check className={classes.labelRadioButton} style={{cursor: 'pointer'}}>
                                                <Input type="radio" name={question.stt}
                                                       onChange={this.onChangeValue}
                                                       value={subAnswer + "|" + question.id + "." + indexSubAnswer
                                                       + "|" + category.categoryId + "." + question.stt
                                                       + "|" + listQuestionMinitest[7].partName}
                                                />
                                                ({indexSubAnswer === 0 ? "A" : (indexSubAnswer === 1 ? "B" : (indexSubAnswer === 2 ? "C" : "D"))}) {this.upperCaseFirstLetter(subAnswer)}
                                              </Label>
                                            </FormGroup>
                                          )}
                                        </Col>
                                      </Row>
                                    )}
                                  </Col>
                                </Row>
                              </>
                            )}
                          </div>
                          {/*--------------------------------------- end PART8--------------------------------------------*/}
                        </>
                        : null
                      }
                    </>
                    :
                    <>
                      {listQuestionAndAnswerMinitest.length > 0 ?
                        <>
                          {/*----------------------------------------PART5-----------------------------------------------*/}
                          <div className="mt-3">
                            <h1 id="PART5" style={{fontWeight: '700'}}>PART 5</h1>

                            <Row style={{marginTop: '3%', marginLeft: '6%'}}>
                              {listQuestionAndAnswerMinitest[4].listCategoryMinitestDTOS[0].listQuestionMinitestDTOS.map((question, indexQuestion) =>
                                <>
                                  <Col id={String(question.stt)} xs={12}>
                                    <h4>
                                      {question.stt}. {question.name}
                                      {/*phần dịch câu hỏi*/}
                                      <button style={{border: 'none', backgroundColor: 'white', padding: '0', marginLeft: '3px'}}
                                              id={"q" + question.stt} className="mr-1 mb-1">
                                        <Icon.AlertCircle  size={20} />
                                      </button>
                                      <UncontrolledPopover trigger="focus" placement="bottom" target={"q" + question.stt}

                                      >
                                        <PopoverBody style={{borderColor: 'green !important',
                                          color: 'white', backgroundColor: 'green', borderRadius: '5px'}}>
                                          <FormattedMessage id="translate" />: {question.translatingQuestion !== null ? question.translatingQuestion : "chưa có phần dịch"}
                                        </PopoverBody>
                                      </UncontrolledPopover>
                                      {/*end phần dịch câu hỏi*/}
                                    </h4>
                                    <Col xs="5">
                                      {question ? this.cutAnswerToChoose(question.answersToChoose).map((subAnswer, indexSubAnswer) =>
                                        <>
                                          <Col xs="12" md="12" sm="12" lg="12" style={{display: 'flex', padding: '0%'}}>
                                            <Col xs="1" md="2" sm="1" lg="1" style={{padding: '0%'}}>
                                              {indexSubAnswer === question.indexCorrectAnswer ?
                                                <Icon.Check size={25} style={{color: 'green'}}/> : null
                                              }
                                              {indexSubAnswer === question.indexIncorrectAnswer ?
                                                <Icon.X size={25} style={{color: '#b21717'}} /> : null
                                              }
                                              {/*<Icon.Check size={25} style={{color: 'green'}}/>*/}
                                            </Col>
                                            <Col xs="11" md="10" sm="11" lg="11" style={{padding: '0%', display: 'flex'}}>

                                              <FormGroup check style={{marginBottom:'3%'}}>
                                                <Label  check className={classes.labelRadioButton} style={{cursor: 'pointer',
                                                  fontSize: '120%', fontWeight: '500'}}>
                                                  <Input type="radio" name={question.stt}
                                                         checked={this.checkingAnswerChoosen(question.indexIncorrectAnswer,
                                                           question.indexCorrectAnswer, indexSubAnswer)}
                                                  />
                                                  ({ indexSubAnswer===0 ? "A" : (indexSubAnswer===1 ? "B" : (indexSubAnswer===2 ? "C" : "D")) }) {this.upperCaseFirstLetter(subAnswer)}
                                                </Label>
                                                {/*phần dịch câu đáp án*/}
                                                <button style={{border: 'none', backgroundColor: 'white', padding: '0', marginLeft: '3px'}}
                                                        id={"a" + question.stt + indexSubAnswer} className="mr-1 mb-1">
                                                  <Icon.AlertCircle  size={20} />
                                                </button>
                                                <UncontrolledPopover trigger="focus" placement="bottom" target={"a" + question.stt + indexSubAnswer}>
                                                  <PopoverBody style={{borderColor: 'green !important',
                                                    color: 'white', backgroundColor: 'green', borderRadius: '5px'}}>
                                                    <FormattedMessage id="translate" />: {indexSubAnswer===0 ? question.translatingQuesA : (indexSubAnswer===1 ? question.translatingQuesB : (indexSubAnswer===2 ? question.translatingQuesC : question.translatingQuesD))}
                                                  </PopoverBody>
                                                </UncontrolledPopover>
                                                {/*phần dịch câu đáp án*/}
                                              </FormGroup>

                                              {/*<Col xs="10" md="4" sm="10" lg="4">*/}
                                              {/**/}
                                              {/*</Col>*/}

                                              {/*<Col xs="2" md="2" sm="2" lg="1">*/}
                                              {/**/}
                                              {/*</Col>*/}

                                            </Col>
                                          </Col>
                                        </>
                                      ) : null}
                                    </Col>
                                  </Col>
                                  <Col xs="12" md="12" sm="12" lg="12" style={{marginBottom: '10%', paddingRight: '10%'}}>
                                    <fieldset className={classes.fieldsetDescription}>
                                      <legend className={classes.legendDescription}>
                                        {/*Giải thích*/}
                                        <FormattedMessage id="description" />
                                      </legend>
                                      <i>{null === question.description ? <>Không có giải thích</> : question.description}</i>
                                    </fieldset>
                                  </Col>
                                </>
                              )}
                            </Row>

                          </div>
                          {/*----------------------------------------end PART5-----------------------------------------------*/}


                          {/*----------------------------------------PART6-----------------------------------------------*/}
                          <div className="mt-3">
                            <h1 id="PART6" style={{fontWeight: '700'}}>PART 6</h1>

                            {listQuestionAndAnswerMinitest[5].listCategoryMinitestDTOS.map((category, indexCategory) =>
                              <>
                                <h4 style={{fontWeight: '700', marginTop: '5%'}}>6.{indexCategory + 1}.</h4>
                                <div style={{marginLeft: '7%'}}><h4>
                                  {/*Question {category.listQuestionMinitestDTOS[0].name}-*/}
                                  {/*{category.listQuestionMinitestDTOS[category.listQuestionMinitestDTOS.length - 1].name}:*/}
                                  {/*refer to the following email*/}
                                </h4></div>
                                <Row style={{marginTop: '3%', marginLeft: '3%'}}>
                                  <Col xs="12" md="5" sm="12" lg="5">
                                    <div style={{backgroundColor: '#d3d3d347', padding: '10px'}}>
                                      {category.pathFile1}
                                    </div>
                                  </Col>
                                  <Col xs="12" md="7" sm="12" lg="7">
                                    {category.listQuestionMinitestDTOS.map((question, indexQuestion) =>
                                      <>
                                        <Col id={String(question.stt)} xs="12" md="12" sm="12" lg="12">
                                          <h4>
                                            {question.stt}. {question.name}

                                            {/*/!*phần dịch câu hỏi*!/*/}
                                            {/*<button style={{border: 'none', backgroundColor: 'white', padding: '0', marginLeft: '3px'}}*/}
                                                    {/*id={"q" + question.stt} className="mr-1 mb-1">*/}
                                              {/*<Icon.AlertCircle  size={20} />*/}
                                            {/*</button>*/}
                                            {/*<UncontrolledPopover trigger="focus" placement="bottom" target={"q" + question.stt}*/}

                                            {/*>*/}
                                              {/*<PopoverBody style={{borderColor: 'green !important',*/}
                                                {/*color: 'white', backgroundColor: 'green', borderRadius: '5px'}}>*/}
                                                {/*<FormattedMessage id="translate" />: {question.translatingQuestion !== null ? question.translatingQuestion : "chưa có phần dịch"}*/}
                                              {/*</PopoverBody>*/}
                                            {/*</UncontrolledPopover>*/}
                                            {/*/!*end phần dịch câu hỏi*!/*/}

                                          </h4>
                                          {this.cutAnswerToChoose(question.answersToChoose).map((subAnswer, indexSubAnswer) =>
                                            <>
                                              <Col xs="12" md="12" sm="12" lg="12" style={{display: 'flex', padding: '0%'}}>
                                                <Col xs="1" md="2" sm="1" lg="1" style={{padding: '0%'}}>
                                                  {indexSubAnswer === question.indexCorrectAnswer ?
                                                    <Icon.Check size={25} style={{color: 'green'}}/> : null
                                                  }
                                                  {indexSubAnswer === question.indexIncorrectAnswer ?
                                                    <Icon.X size={25} style={{color: '#b21717'}} /> : null
                                                  }
                                                </Col>
                                                <Col xs="11" md="10" sm="11" lg="11" style={{padding: '0%', display: 'flex'}}>
                                                  <FormGroup check style={{marginBottom:'3%'}}>
                                                    <Label  check className={classes.labelRadioButton} style={{cursor: 'pointer',
                                                      fontSize: '120%', fontWeight: '500'}}>
                                                      <Input type="radio" name={question.stt}
                                                             checked={this.checkingAnswerChoosen(question.indexIncorrectAnswer,
                                                               question.indexCorrectAnswer, indexSubAnswer)}
                                                      />
                                                      ({ indexSubAnswer===0 ? "A" : (indexSubAnswer===1 ? "B" : (indexSubAnswer===2 ? "C" : "D")) }) {this.upperCaseFirstLetter(subAnswer)}
                                                    </Label>
                                                    {/*phần dịch câu đáp án*/}
                                                    <button style={{border: 'none', backgroundColor: 'white', padding: '0', marginLeft: '3px'}}
                                                            id={"a" + question.stt + indexSubAnswer} className="mr-1 mb-1">
                                                      <Icon.AlertCircle  size={20} />
                                                    </button>
                                                    <UncontrolledPopover trigger="focus" placement="bottom" target={"a" + question.stt + indexSubAnswer}

                                                    >
                                                      <PopoverBody style={{borderColor: 'green !important',
                                                        color: 'white', backgroundColor: 'green', borderRadius: '5px'}}>
                                                        <FormattedMessage id="translate" />: {indexSubAnswer===0 ? question.translatingQuesA : (indexSubAnswer===1 ? question.translatingQuesB : (indexSubAnswer===2 ? question.translatingQuesC : question.translatingQuesD))}
                                                      </PopoverBody>
                                                    </UncontrolledPopover>
                                                    {/*phần dịch câu đáp án*/}
                                                  </FormGroup>
                                                </Col>
                                              </Col>
                                            </>
                                          )}
                                        </Col>
                                        <Col xs="12" md="12" sm="12" lg="12" style={{marginBottom: '10%'}}>
                                          <fieldset className={classes.fieldsetDescription}>
                                            <legend className={classes.legendDescription}>
                                              {/*Giải thích*/}
                                              <FormattedMessage id="description" />
                                            </legend>
                                            <i>{null === question.description ? <>KHÔNG CÓ GIẢI THÍCH GÌ HẾT :))</> : question.description}</i>
                                          </fieldset>
                                        </Col>
                                      </>
                                    )}
                                  </Col>
                                </Row>
                              </>
                            )}
                          </div>
                          {/*---------------------------------------end PART6--------------------------------------------*/}


                          {/*---------------------------------------PART7--------------------------------------------*/}
                          <div className="mt-3">
                            <h3 id="PART7" style={{fontWeight: '700'}}>PART 7</h3>

                            {listQuestionAndAnswerMinitest[6].listCategoryMinitestDTOS.map((category, indexCategory) =>
                              <>
                                <h4 style={{fontWeight: '700', marginTop: '5%'}}>7.{indexCategory + 1}.</h4>
                                <Row style={{marginTop: '3%'}}>
                                  { category.pathFile1 ? category.pathFile1.includes("https://") === true ?
                                    <Col xs="12" md="12" sm="12" lg="12">
                                      <img className={classes.imgClass}
                                           style={{width: '100%', border: '5px solid lightgray'}}
                                           src={category.pathFile1}/>
                                    </Col>
                                    :
                                    <Col xs="12" md="12" sm="12" lg="12">
                                      <div style={{backgroundColor: '#d3d3d347', padding: '10px'}}>
                                        {category.pathFile1}
                                      </div>
                                    </Col> : null
                                  }
                                  <Col xs="12" md="12" sm="12" lg="12" style={{marginTop: '5%'}}>
                                    {category.listQuestionMinitestDTOS.map((question, indexQuestion) =>
                                      <>
                                        <Row id={String(question.stt)} style={{marginTop: '3%', marginLeft: '5%'}}>
                                          <Col xs="6" md="6" sm="6" lg="6">
                                            <h4>
                                              {question.stt}. {question.name}

                                              {/*phần dịch câu hỏi*/}
                                              <button style={{border: 'none', backgroundColor: 'white', padding: '0', marginLeft: '3px'}}
                                                      id={"q" + question.stt} className="mr-1 mb-1">
                                                <Icon.AlertCircle  size={20} />
                                              </button>
                                              <UncontrolledPopover trigger="focus" placement="bottom" target={"q" + question.stt}

                                              >
                                                <PopoverBody style={{borderColor: 'green !important',
                                                  color: 'white', backgroundColor: 'green', borderRadius: '5px'}}>
                                                  <FormattedMessage id="translate" />: {question.translatingQuestion !== null ? question.translatingQuestion : "chưa có phần dịch"}
                                                </PopoverBody>
                                              </UncontrolledPopover>
                                              {/*end phần dịch câu hỏi*/}

                                            </h4>
                                          </Col>
                                          <Col xs="6" md="6" sm="6" lg="6">
                                            {this.cutAnswerToChoose(question.answersToChoose).map((subAnswer, indexSubAnswer) =>
                                              <>
                                                <Col xs="12" md="12" sm="12" lg="12" style={{display: 'flex', padding: '0%'}}>
                                                  <Col xs="1" md="2" sm="1" lg="1" style={{padding: '0%'}}>
                                                    {indexSubAnswer === question.indexCorrectAnswer ?
                                                      <Icon.Check size={25} style={{color: 'green'}}/> : null
                                                    }
                                                    {indexSubAnswer === question.indexIncorrectAnswer ?
                                                      <Icon.X size={25} style={{color: '#b21717'}} /> : null
                                                    }
                                                  </Col>
                                                  <Col xs="11" md="10" sm="11" lg="11" style={{padding: '0%', display: 'flex'}}>
                                                    <FormGroup check style={{marginBottom:'3%'}}>
                                                      <Label  check className={classes.labelRadioButton} style={{cursor: 'pointer',
                                                        fontSize: '120%', fontWeight: '500'}}>
                                                        <Input type="radio" name={question.stt}
                                                               checked={this.checkingAnswerChoosen(question.indexIncorrectAnswer,
                                                                 question.indexCorrectAnswer, indexSubAnswer)}
                                                        />
                                                        ({ indexSubAnswer===0 ? "A" : (indexSubAnswer===1 ? "B" : (indexSubAnswer===2 ? "C" : "D")) }) {this.upperCaseFirstLetter(subAnswer)}
                                                      </Label>
                                                      {/*phần dịch câu đáp án*/}
                                                      <button style={{border: 'none', backgroundColor: 'white', padding: '0', marginLeft: '3px'}}
                                                              id={"a" + question.stt + indexSubAnswer} className="mr-1 mb-1">
                                                        <Icon.AlertCircle  size={20} />
                                                      </button>
                                                      <UncontrolledPopover trigger="focus" placement="bottom" target={"a" + question.stt + indexSubAnswer}>
                                                        <PopoverBody style={{borderColor: 'green !important',
                                                          color: 'white', backgroundColor: 'green', borderRadius: '5px'}}>
                                                          <FormattedMessage id="translate" />: {indexSubAnswer===0 ? question.translatingQuesA : (indexSubAnswer===1 ? question.translatingQuesB : (indexSubAnswer===2 ? question.translatingQuesC : question.translatingQuesD))}
                                                        </PopoverBody>
                                                      </UncontrolledPopover>
                                                      {/*phần dịch câu đáp án*/}
                                                    </FormGroup>
                                                  </Col>
                                                </Col>
                                              </>
                                            )}
                                          </Col>
                                        </Row>
                                        <Row style={{marginLeft: '5%', marginRight: '5%'}}>
                                          <Col xs="12" md="12" sm="12" lg="12" style={{marginBottom: '10%'}}>
                                            <fieldset className={classes.fieldsetDescription}>
                                              <legend className={classes.legendDescription}>
                                                {/*Giải thích*/}
                                                <FormattedMessage id="description" />
                                              </legend>
                                              <i>{null === question.description ? <>KHÔNG CÓ GIẢI THÍCH GÌ HẾT :))</> : question.description}</i>
                                            </fieldset>
                                          </Col>
                                        </Row>
                                      </>
                                    )}
                                  </Col>
                                </Row>
                              </>
                            )}
                          </div>
                          {/*--------------------------------------- end PART7--------------------------------------------*/}


                          {/*---------------------------------------PART8--------------------------------------------*/}
                          <div className="mt-3">
                            <h1 id="PART8" style={{fontWeight: '700'}}>PART 8</h1>

                            {listQuestionAndAnswerMinitest[7].listCategoryMinitestDTOS.map((category, indexCategory) =>
                              <>
                                <h4 style={{fontWeight: '700', marginTop: '5%'}}>8.{indexCategory + 1}.</h4>
                                <Row style={{marginTop: '3%'}}>
                                  { category.pathFile1 ? category.pathFile1.includes("https://") === true ?
                                    <Col xs="12" md="12" sm="12" lg="12">
                                      <img className={classes.imgClass}
                                           style={{width: '100%', border: '5px solid lightgray'}}
                                           src={category.pathFile1}/>
                                    </Col>
                                    :
                                    <Col xs="12" md="12" sm="12" lg="12">
                                      <div style={{backgroundColor: '#d3d3d347', padding: '10px'}}>
                                        {category.pathFile1}
                                      </div>
                                    </Col> : null
                                  }
                                  <Col xs="12" md="12" sm="12" lg="12" style={{marginTop: '5%'}}>
                                    {category.listQuestionMinitestDTOS.map((question, indexQuestion) =>
                                      <>
                                        <Row id={String(question.stt)} style={{marginTop: '3%', marginLeft: '5%'}}>
                                          <Col xs="6" md="6" sm="6" lg="6">
                                            <h4>
                                              {question.stt}. {question.name}

                                              {/*phần dịch câu hỏi*/}
                                              <button style={{border: 'none', backgroundColor: 'white', padding: '0', marginLeft: '3px'}}
                                                      id={"q" + question.stt} className="mr-1 mb-1">
                                                <Icon.AlertCircle  size={20} />
                                              </button>
                                              <UncontrolledPopover trigger="focus" placement="bottom" target={"q" + question.stt}

                                              >
                                                <PopoverBody style={{borderColor: 'green !important',
                                                  color: 'white', backgroundColor: 'green', borderRadius: '5px'}}>
                                                  <FormattedMessage id="translate" />: {question.translatingQuestion !== null ? question.translatingQuestion : "chưa có phần dịch"}
                                                </PopoverBody>
                                              </UncontrolledPopover>
                                              {/*end phần dịch câu hỏi*/}

                                            </h4>
                                          </Col>
                                          <Col xs="6" md="6" sm="6" lg="6">
                                            {this.cutAnswerToChoose(question.answersToChoose).map((subAnswer, indexSubAnswer) =>
                                              <>
                                                <Col xs="12" md="12" sm="12" lg="12" style={{display: 'flex', padding: '0%'}}>
                                                  <Col xs="1" md="2" sm="1" lg="1" style={{padding: '0%'}}>
                                                    {indexSubAnswer === question.indexCorrectAnswer ?
                                                      <Icon.Check size={25} style={{color: 'green'}}/> : null
                                                    }
                                                    {indexSubAnswer === question.indexIncorrectAnswer ?
                                                      <Icon.X size={25} style={{color: '#b21717'}} /> : null
                                                    }
                                                  </Col>
                                                  <Col xs="11" md="10" sm="11" lg="11" style={{padding: '0%', display: 'flex'}}>
                                                    <FormGroup check style={{marginBottom:'3%'}}>
                                                      <Label  check className={classes.labelRadioButton} style={{cursor: 'pointer',
                                                        fontSize: '120%', fontWeight: '500'}}>
                                                        <Input type="radio" name={question.stt}
                                                               checked={this.checkingAnswerChoosen(question.indexIncorrectAnswer,
                                                                 question.indexCorrectAnswer, indexSubAnswer)}
                                                        />
                                                        ({ indexSubAnswer===0 ? "A" : (indexSubAnswer===1 ? "B" : (indexSubAnswer===2 ? "C" : "D")) }) {this.upperCaseFirstLetter(subAnswer)}
                                                      </Label>
                                                      {/*phần dịch câu đáp án*/}
                                                      <button style={{border: 'none', backgroundColor: 'white', padding: '0', marginLeft: '3px'}}
                                                              id={"a" + question.stt + indexSubAnswer} className="mr-1 mb-1">
                                                        <Icon.AlertCircle  size={20} />
                                                      </button>
                                                      <UncontrolledPopover trigger="focus" placement="bottom" target={"a" + question.stt + indexSubAnswer}

                                                      >
                                                        <PopoverBody style={{borderColor: 'green !important',
                                                          color: 'white', backgroundColor: 'green', borderRadius: '5px'}}>
                                                          <FormattedMessage id="translate" />: {indexSubAnswer===0 ? question.translatingQuesA : (indexSubAnswer===1 ? question.translatingQuesB : (indexSubAnswer===2 ? question.translatingQuesC : question.translatingQuesD))}
                                                        </PopoverBody>
                                                      </UncontrolledPopover>
                                                      {/*phần dịch câu đáp án*/}
                                                    </FormGroup>
                                                  </Col>
                                                </Col>
                                              </>
                                            )}
                                          </Col>
                                        </Row>
                                        <Row style={{marginLeft: '5%', marginRight: '5%'}}>
                                          <Col xs="12" md="12" sm="12" lg="12" style={{marginBottom: '10%'}}>
                                            <fieldset className={classes.fieldsetDescription}>
                                              <legend className={classes.legendDescription}>
                                                {/*Giải thích*/}
                                                <FormattedMessage id="description" />
                                              </legend>
                                              <i>{null === question.description ? <>KHÔNG CÓ GIẢI THÍCH GÌ HẾT :))</> : question.description}</i>
                                            </fieldset>
                                          </Col>
                                        </Row>
                                      </>
                                    )}
                                  </Col>
                                </Row>
                              </>
                            )}
                          </div>
                          {/*--------------------------------------- end PART8--------------------------------------------*/}
                        </>
                        : null
                      }
                    </>
                  }
                </div>
              </Row>
            </div>
          </div>
          :
          <div className="mt-3">
            {loadingQuestion !== undefined ?
              <>{loadingQuestion === false ? <>{this.loadingComponent}</> : ""}</> : null
            }
            <Badge style={{backgroundColor: "rgba(32 171 231)", width: "100%", height: "60%", marginLeft: "0%",
              padding: "10px 0 0 0", borderRadius:'10px'}} className="badge-xl block">
              <div>
                <div className="text-left" style={{marginLeft: '0px'}}>
                  <div className={classes.titleModal}>
                    <h3 style={{color:'white', marginLeft: '2%', top: '40%', transform: 'translateY(-50%)', position: 'absolute'}}>
                      Reading Test
                    </h3>
                  </div>
                </div>
              </div>
            </Badge>
            <div style={{position: 'relative', margin: '0 1%'}}>

              <Row className={classes.bodyModal} style={{margin: '0 0% 0 0%', padding: '0 0%', width: "100%" }}>
                <div style={{margin:'3% 3%'}}>
                  {
                    listQuestionAndAnswerMinitest.length > 0 ? null :
                      <h5>In the Reading test, you will read a variety of texts and answer several different types of reading comprehensive questions.

                        The entire Reading test will last 75 minutes. There are three parts, and directions are given for each part. You are

                        encouraged to answer as many questions as possible within the time allowed. You must mark your answers on the

                        separate answer sheet. Do not write your answer in your test book.</h5>
                  }

                  <>
                    {listQuestionAndAnswerMinitest.length > 0 ?
                      <>
                        {/*----------------------------------------PART5-----------------------------------------------*/}
                        <div className="mt-3">
                          <h1 id="PART5" style={{fontWeight: '700'}}>PART 5</h1>

                          <Row style={{marginTop: '3%', marginLeft: '6%'}}>
                            {listQuestionAndAnswerMinitest[4].listCategoryMinitestDTOS[0].listQuestionMinitestDTOS.map((question, indexQuestion) =>
                              <>
                                <Col id={String(question.stt)} xs={12}>
                                  <h4>
                                    {question.stt}. {question.name}
                                    {/*phần dịch câu hỏi*/}
                                    <button style={{border: 'none', backgroundColor: 'white', padding: '0', marginLeft: '3px'}}
                                            id={"q" + question.stt} className="mr-1 mb-1">
                                      <Icon.AlertCircle  size={20} />
                                    </button>
                                    <UncontrolledPopover trigger="focus" placement="bottom" target={"q" + question.stt}

                                    >
                                      <PopoverBody style={{borderColor: 'green !important',
                                        color: 'white', backgroundColor: 'green', borderRadius: '5px'}}>
                                        <FormattedMessage id="translate" />: {question.translatingQuestion !== null ? question.translatingQuestion : "chưa có phần dịch"}
                                      </PopoverBody>
                                    </UncontrolledPopover>
                                    {/*end phần dịch câu hỏi*/}
                                  </h4>
                                  <Col xs="5">
                                    {question ? this.cutAnswerToChoose(question.answersToChoose).map((subAnswer, indexSubAnswer) =>
                                      <>
                                        <Col xs="12" md="12" sm="12" lg="12" style={{display: 'flex', padding: '0%'}}>
                                          <Col xs="1" md="2" sm="1" lg="1" style={{padding: '0%'}}>
                                            {indexSubAnswer === question.indexCorrectAnswer ?
                                              <Icon.Check size={25} style={{color: 'green'}}/> : null
                                            }
                                            {indexSubAnswer === question.indexIncorrectAnswer ?
                                              <Icon.X size={25} style={{color: '#b21717'}} /> : null
                                            }
                                            {/*<Icon.Check size={25} style={{color: 'green'}}/>*/}
                                          </Col>
                                          <Col xs="11" md="10" sm="11" lg="11" style={{padding: '0%', display: 'flex'}}>

                                            <FormGroup check style={{marginBottom:'3%'}}>
                                              <Label  check className={classes.labelRadioButton} style={{cursor: 'pointer',
                                                fontSize: '120%', fontWeight: '500'}}>
                                                <Input type="radio" name={question.stt}
                                                       checked={this.checkingAnswerChoosen(question.indexIncorrectAnswer,
                                                         question.indexCorrectAnswer, indexSubAnswer)}
                                                />
                                                ({ indexSubAnswer===0 ? "A" : (indexSubAnswer===1 ? "B" : (indexSubAnswer===2 ? "C" : "D")) }) {this.upperCaseFirstLetter(subAnswer)}
                                              </Label>
                                              {/*phần dịch câu đáp án*/}
                                              <button style={{border: 'none', backgroundColor: 'white', padding: '0', marginLeft: '3px'}}
                                                      id={"a" + question.stt + indexSubAnswer} className="mr-1 mb-1">
                                                <Icon.AlertCircle  size={20} />
                                              </button>
                                              <UncontrolledPopover trigger="focus" placement="bottom" target={"a" + question.stt + indexSubAnswer}>
                                                <PopoverBody style={{borderColor: 'green !important',
                                                  color: 'white', backgroundColor: 'green', borderRadius: '5px'}}>
                                                  <FormattedMessage id="translate" />: {indexSubAnswer===0 ? question.translatingQuesA : (indexSubAnswer===1 ? question.translatingQuesB : (indexSubAnswer===2 ? question.translatingQuesC : question.translatingQuesD))}
                                                </PopoverBody>
                                              </UncontrolledPopover>
                                              {/*phần dịch câu đáp án*/}
                                            </FormGroup>


                                            {/*<Col xs="10" md="4" sm="10" lg="4">*/}
                                            {/**/}
                                            {/*</Col>*/}

                                            {/*<Col xs="2" md="2" sm="2" lg="1">*/}
                                            {/**/}
                                            {/*</Col>*/}

                                          </Col>
                                        </Col>
                                      </>
                                    ) : null}
                                  </Col>
                                </Col>
                                <Col xs="12" md="12" sm="12" lg="12" style={{marginBottom: '10%', paddingRight: '10%'}}>
                                  <fieldset className={classes.fieldsetDescription}>
                                    <legend className={classes.legendDescription}>
                                      {/*Giải thích*/}
                                      {/*Giải thích*/}
                                      <FormattedMessage id="description" />
                                    </legend>
                                    <i>{null === question.description ? <>KHÔNG CÓ GIẢI THÍCH GÌ HẾT :))</> : question.description}</i>
                                  </fieldset>
                                </Col>
                              </>
                            )}
                          </Row>

                        </div>
                        {/*----------------------------------------end PART5-----------------------------------------------*/}


                        {/*----------------------------------------PART6-----------------------------------------------*/}
                        <div className="mt-3">
                          <h1 id="PART6" style={{fontWeight: '700'}}>PART 6</h1>

                          {listQuestionAndAnswerMinitest[5].listCategoryMinitestDTOS.map((category, indexCategory) =>
                            <>
                              <h4 style={{fontWeight: '700', marginTop: '5%'}}>6.{indexCategory + 1}.</h4>
                              <div style={{marginLeft: '7%'}}><h4>
                                {/*Question {category.listQuestionMinitestDTOS[0].name}-*/}
                                {/*{category.listQuestionMinitestDTOS[category.listQuestionMinitestDTOS.length - 1].name}:*/}
                                {/*refer to the following email*/}
                              </h4></div>
                              <Row style={{marginTop: '3%', marginLeft: '3%'}}>
                                <Col xs="12" md="5" sm="12" lg="5">
                                  <div style={{backgroundColor: '#d3d3d347', padding: '10px'}}>
                                    {category.pathFile1}
                                  </div>
                                </Col>
                                <Col xs="12" md="7" sm="12" lg="7">
                                  {category.listQuestionMinitestDTOS.map((question, indexQuestion) =>
                                    <>
                                      <Col id={String(question.stt)} xs="12" md="12" sm="12" lg="12">
                                        <h4>
                                          {question.stt}. {question.name}

                                          {/*/!*phần dịch câu hỏi*!/*/}
                                          {/*<button style={{border: 'none', backgroundColor: 'white', padding: '0', marginLeft: '3px'}}*/}
                                                  {/*id={"q" + question.stt} className="mr-1 mb-1">*/}
                                            {/*<Icon.AlertCircle  size={20} />*/}
                                          {/*</button>*/}
                                          {/*<UncontrolledPopover trigger="focus" placement="bottom" target={"q" + question.stt}*/}

                                          {/*>*/}
                                            {/*<PopoverBody style={{borderColor: 'green !important',*/}
                                              {/*color: 'white', backgroundColor: 'green', borderRadius: '5px'}}>*/}
                                              {/*<FormattedMessage id="translate" />: {question.translatingQuestion !== null ? question.translatingQuestion : "chưa có phần dịch"}*/}
                                            {/*</PopoverBody>*/}
                                          {/*</UncontrolledPopover>*/}
                                          {/*/!*end phần dịch câu hỏi*!/*/}

                                        </h4>
                                        {this.cutAnswerToChoose(question.answersToChoose).map((subAnswer, indexSubAnswer) =>
                                          <>
                                            <Col xs="12" md="12" sm="12" lg="12" style={{display: 'flex', padding: '0%'}}>
                                              <Col xs="1" md="2" sm="1" lg="1" style={{padding: '0%'}}>
                                                {indexSubAnswer === question.indexCorrectAnswer ?
                                                  <Icon.Check size={25} style={{color: 'green'}}/> : null
                                                }
                                                {indexSubAnswer === question.indexIncorrectAnswer ?
                                                  <Icon.X size={25} style={{color: '#b21717'}} /> : null
                                                }
                                              </Col>
                                              <Col xs="11" md="10" sm="11" lg="11" style={{padding: '0%', display: 'flex'}}>
                                                <FormGroup check style={{marginBottom:'3%'}}>
                                                  <Label  check className={classes.labelRadioButton} style={{cursor: 'pointer',
                                                    fontSize: '120%', fontWeight: '500'}}>
                                                    <Input type="radio" name={question.stt}
                                                           checked={this.checkingAnswerChoosen(question.indexIncorrectAnswer,
                                                             question.indexCorrectAnswer, indexSubAnswer)}
                                                    />
                                                    ({ indexSubAnswer===0 ? "A" : (indexSubAnswer===1 ? "B" : (indexSubAnswer===2 ? "C" : "D")) }) {this.upperCaseFirstLetter(subAnswer)}
                                                  </Label>
                                                  {/*phần dịch câu đáp án*/}
                                                  <button style={{border: 'none', backgroundColor: 'white', padding: '0', marginLeft: '3px'}}
                                                          id={"a" + question.stt + indexSubAnswer} className="mr-1 mb-1">
                                                    <Icon.AlertCircle  size={20} />
                                                  </button>
                                                  <UncontrolledPopover trigger="focus" placement="bottom" target={"a" + question.stt + indexSubAnswer}

                                                  >
                                                    <PopoverBody style={{borderColor: 'green !important',
                                                      color: 'white', backgroundColor: 'green', borderRadius: '5px'}}>
                                                      <FormattedMessage id="translate" />: {indexSubAnswer===0 ? question.translatingQuesA : (indexSubAnswer===1 ? question.translatingQuesB : (indexSubAnswer===2 ? question.translatingQuesC : question.translatingQuesD))}
                                                    </PopoverBody>
                                                  </UncontrolledPopover>
                                                  {/*phần dịch câu đáp án*/}
                                                </FormGroup>
                                              </Col>
                                            </Col>
                                          </>
                                        )}
                                      </Col>
                                      <Col xs="12" md="12" sm="12" lg="12" style={{marginBottom: '10%'}}>
                                        <fieldset className={classes.fieldsetDescription}>
                                          <legend className={classes.legendDescription}>
                                            {/*Giải thích*/}
                                            <FormattedMessage id="description" />
                                          </legend>
                                          <i>{null === question.description ? <>KHÔNG CÓ GIẢI THÍCH GÌ HẾT :))</> : question.description}</i>
                                        </fieldset>
                                      </Col>
                                    </>
                                  )}
                                </Col>
                              </Row>
                            </>
                          )}
                        </div>
                        {/*---------------------------------------end PART6--------------------------------------------*/}


                        {/*---------------------------------------PART7--------------------------------------------*/}
                        <div className="mt-3">
                          <h3 id="PART7" style={{fontWeight: '700'}}>PART 7</h3>

                          {listQuestionAndAnswerMinitest[6].listCategoryMinitestDTOS.map((category, indexCategory) =>
                            <>
                              <h4 style={{fontWeight: '700', marginTop: '5%'}}>7.{indexCategory + 1}.</h4>
                              <Row style={{marginTop: '3%'}}>
                                { category.pathFile1 ? category.pathFile1.includes("https://") === true ?
                                  <Col xs="12" md="12" sm="12" lg="12">
                                    <img className={classes.imgClass}
                                         style={{width: '100%', border: '5px solid lightgray'}}
                                         src={category.pathFile1}/>
                                  </Col>
                                  :
                                  <Col xs="12" md="12" sm="12" lg="12">
                                    <div style={{backgroundColor: '#d3d3d347', padding: '10px'}}>
                                      {category.pathFile1}
                                    </div>
                                  </Col> : null
                                }
                                <Col xs="12" md="12" sm="12" lg="12" style={{marginTop: '5%'}}>
                                  {category.listQuestionMinitestDTOS.map((question, indexQuestion) =>
                                    <>
                                      <Row id={String(question.stt)} style={{marginTop: '3%', marginLeft: '5%'}}>
                                        <Col xs="6" md="6" sm="6" lg="6">
                                          <h4>
                                            {question.stt}. {question.name}

                                            {/*phần dịch câu hỏi*/}
                                            <button style={{border: 'none', backgroundColor: 'white', padding: '0', marginLeft: '3px'}}
                                                    id={"q" + question.stt} className="mr-1 mb-1">
                                              <Icon.AlertCircle  size={20} />
                                            </button>
                                            <UncontrolledPopover trigger="focus" placement="bottom" target={"q" + question.stt}

                                            >
                                              <PopoverBody style={{borderColor: 'green !important',
                                                color: 'white', backgroundColor: 'green', borderRadius: '5px'}}>
                                                <FormattedMessage id="translate" />: {question.translatingQuestion !== null ? question.translatingQuestion : "chưa có phần dịch"}
                                              </PopoverBody>
                                            </UncontrolledPopover>
                                            {/*end phần dịch câu hỏi*/}

                                          </h4>
                                        </Col>
                                        <Col xs="6" md="6" sm="6" lg="6">
                                          {this.cutAnswerToChoose(question.answersToChoose).map((subAnswer, indexSubAnswer) =>
                                            <>
                                              <Col xs="12" md="12" sm="12" lg="12" style={{display: 'flex', padding: '0%'}}>
                                                <Col xs="1" md="2" sm="1" lg="1" style={{padding: '0%'}}>
                                                  {indexSubAnswer === question.indexCorrectAnswer ?
                                                    <Icon.Check size={25} style={{color: 'green'}}/> : null
                                                  }
                                                  {indexSubAnswer === question.indexIncorrectAnswer ?
                                                    <Icon.X size={25} style={{color: '#b21717'}} /> : null
                                                  }
                                                </Col>
                                                <Col xs="11" md="10" sm="11" lg="11" style={{padding: '0%', display: 'flex'}}>
                                                  <FormGroup check style={{marginBottom:'3%'}}>
                                                    <Label  check className={classes.labelRadioButton} style={{cursor: 'pointer',
                                                      fontSize: '120%', fontWeight: '500'}}>
                                                      <Input type="radio" name={question.stt}
                                                             checked={this.checkingAnswerChoosen(question.indexIncorrectAnswer,
                                                               question.indexCorrectAnswer, indexSubAnswer)}
                                                      />
                                                      ({ indexSubAnswer===0 ? "A" : (indexSubAnswer===1 ? "B" : (indexSubAnswer===2 ? "C" : "D")) }) {this.upperCaseFirstLetter(subAnswer)}
                                                    </Label>
                                                    {/*phần dịch câu đáp án*/}
                                                    <button style={{border: 'none', backgroundColor: 'white', padding: '0', marginLeft: '3px'}}
                                                            id={"a" + question.stt + indexSubAnswer} className="mr-1 mb-1">
                                                      <Icon.AlertCircle  size={20} />
                                                    </button>
                                                    <UncontrolledPopover trigger="focus" placement="bottom" target={"a" + question.stt + indexSubAnswer}>
                                                      <PopoverBody style={{borderColor: 'green !important',
                                                        color: 'white', backgroundColor: 'green', borderRadius: '5px'}}>
                                                        <FormattedMessage id="translate" />: {indexSubAnswer===0 ? question.translatingQuesA : (indexSubAnswer===1 ? question.translatingQuesB : (indexSubAnswer===2 ? question.translatingQuesC : question.translatingQuesD))}
                                                      </PopoverBody>
                                                    </UncontrolledPopover>
                                                    {/*phần dịch câu đáp án*/}
                                                  </FormGroup>
                                                </Col>
                                              </Col>
                                            </>
                                          )}
                                        </Col>
                                      </Row>
                                      <Row style={{marginLeft: '5%', marginRight: '5%'}}>
                                        <Col xs="12" md="12" sm="12" lg="12" style={{marginBottom: '10%'}}>
                                          <fieldset className={classes.fieldsetDescription}>
                                            <legend className={classes.legendDescription}>
                                              {/*Giải thích*/}
                                              <FormattedMessage id="description" />
                                            </legend>
                                            <i>{null === question.description ? <>KHÔNG CÓ GIẢI THÍCH GÌ HẾT :))</> : question.description}</i>
                                          </fieldset>
                                        </Col>
                                      </Row>
                                    </>
                                  )}
                                </Col>
                              </Row>
                            </>
                          )}
                        </div>
                        {/*--------------------------------------- end PART7--------------------------------------------*/}


                        {/*---------------------------------------PART8--------------------------------------------*/}
                        <div className="mt-3">
                          <h1 id="PART8" style={{fontWeight: '700'}}>PART 8</h1>

                          {listQuestionAndAnswerMinitest[7].listCategoryMinitestDTOS.map((category, indexCategory) =>
                            <>
                              <h4 style={{fontWeight: '700', marginTop: '5%'}}>8.{indexCategory + 1}.</h4>
                              <Row style={{marginTop: '3%'}}>
                                { category.pathFile1 ? category.pathFile1.includes("https://") === true ?
                                  <Col xs="12" md="12" sm="12" lg="12">
                                    <img className={classes.imgClass}
                                         style={{width: '100%', border: '5px solid lightgray'}}
                                         src={category.pathFile1}/>
                                  </Col>
                                  :
                                  <Col xs="12" md="12" sm="12" lg="12">
                                    <div style={{backgroundColor: '#d3d3d347', padding: '10px'}}>
                                      {category.pathFile1}
                                    </div>
                                  </Col> : null
                                }
                                <Col xs="12" md="12" sm="12" lg="12" style={{marginTop: '5%'}}>
                                  {category.listQuestionMinitestDTOS.map((question, indexQuestion) =>
                                    <>
                                      <Row id={String(question.stt)} style={{marginTop: '3%', marginLeft: '5%'}}>
                                        <Col xs="6" md="6" sm="6" lg="6">
                                          <h4>
                                            {question.stt}. {question.name}

                                            {/*phần dịch câu hỏi*/}
                                            <button style={{border: 'none', backgroundColor: 'white', padding: '0', marginLeft: '3px'}}
                                                    id={"q" + question.stt} className="mr-1 mb-1">
                                              <Icon.AlertCircle  size={20} />
                                            </button>
                                            <UncontrolledPopover trigger="focus" placement="bottom" target={"q" + question.stt}

                                            >
                                              <PopoverBody style={{borderColor: 'green !important',
                                                color: 'white', backgroundColor: 'green', borderRadius: '5px'}}>
                                                <FormattedMessage id="translate" />: {question.translatingQuestion !== null ? question.translatingQuestion : "chưa có phần dịch"}
                                              </PopoverBody>
                                            </UncontrolledPopover>
                                            {/*end phần dịch câu hỏi*/}

                                          </h4>
                                        </Col>
                                        <Col xs="6" md="6" sm="6" lg="6">
                                          {this.cutAnswerToChoose(question.answersToChoose).map((subAnswer, indexSubAnswer) =>
                                            <>
                                              <Col xs="12" md="12" sm="12" lg="12" style={{display: 'flex', padding: '0%'}}>
                                                <Col xs="1" md="2" sm="1" lg="1" style={{padding: '0%'}}>
                                                  {indexSubAnswer === question.indexCorrectAnswer ?
                                                    <Icon.Check size={25} style={{color: 'green'}}/> : null
                                                  }
                                                  {indexSubAnswer === question.indexIncorrectAnswer ?
                                                    <Icon.X size={25} style={{color: '#b21717'}} /> : null
                                                  }
                                                </Col>
                                                <Col xs="11" md="10" sm="11" lg="11" style={{padding: '0%', display: 'flex'}}>
                                                  <FormGroup check style={{marginBottom:'3%'}}>
                                                    <Label  check className={classes.labelRadioButton} style={{cursor: 'pointer',
                                                      fontSize: '120%', fontWeight: '500'}}>
                                                      <Input type="radio" name={question.stt}
                                                             checked={this.checkingAnswerChoosen(question.indexIncorrectAnswer,
                                                               question.indexCorrectAnswer, indexSubAnswer)}
                                                      />
                                                      ({ indexSubAnswer===0 ? "A" : (indexSubAnswer===1 ? "B" : (indexSubAnswer===2 ? "C" : "D")) }) {this.upperCaseFirstLetter(subAnswer)}
                                                    </Label>
                                                    {/*phần dịch câu đáp án*/}
                                                    <button style={{border: 'none', backgroundColor: 'white', padding: '0', marginLeft: '3px'}}
                                                            id={"a" + question.stt + indexSubAnswer} className="mr-1 mb-1">
                                                      <Icon.AlertCircle  size={20} />
                                                    </button>
                                                    <UncontrolledPopover trigger="focus" placement="bottom" target={"a" + question.stt + indexSubAnswer}

                                                    >
                                                      <PopoverBody style={{borderColor: 'green !important',
                                                        color: 'white', backgroundColor: 'green', borderRadius: '5px'}}>
                                                        <FormattedMessage id="translate" />: {indexSubAnswer===0 ? question.translatingQuesA : (indexSubAnswer===1 ? question.translatingQuesB : (indexSubAnswer===2 ? question.translatingQuesC : question.translatingQuesD))}
                                                      </PopoverBody>
                                                    </UncontrolledPopover>
                                                    {/*phần dịch câu đáp án*/}
                                                  </FormGroup>
                                                </Col>
                                              </Col>
                                            </>
                                          )}
                                        </Col>
                                      </Row>
                                      <Row style={{marginLeft: '5%', marginRight: '5%'}}>
                                        <Col xs="12" md="12" sm="12" lg="12" style={{marginBottom: '10%'}}>
                                          <fieldset className={classes.fieldsetDescription}>
                                            <legend className={classes.legendDescription}>
                                              {/*Giải thích*/}
                                              <FormattedMessage id="description" />
                                            </legend>
                                            <i>{null === question.description ? <>KHÔNG CÓ GIẢI THÍCH GÌ HẾT :))</> : question.description}</i>
                                          </fieldset>
                                        </Col>
                                      </Row>
                                    </>
                                  )}
                                </Col>
                              </Row>
                            </>
                          )}
                        </div>
                        {/*--------------------------------------- end PART8--------------------------------------------*/}
                      </>
                      : null
                    }
                  </>
                </div>
              </Row>
            </div>
          </div>
        }
      </>

    );
  }
}

const mapStateToProps = (state) => {
  return {
    listQuestionMinitest : state.doTestReducer.listQuestionMinitest,
    listQuestionListeningChoosenDTOS: state.doTestReducer.listQuestionListeningChoosenDTOS,
    listQuestionReadingChoosenDTOS: state.doTestReducer.listQuestionReadingChoosenDTOS,
    listQuestionAndAnswerMinitest: state.doTestReducer.listQuestionAndAnswerMinitest,
    loadingQuestion: state.doTestReducer.loadingQuestion,
    loadingSubmitAnswer: state.doTestReducer.loadingSubmitAnswer,
    checkGetListQuestionMinitest: state.doTestReducer.checkGetListQuestionMinitest,
    showAnswer: state.doTestReducer.showAnswer
  }
}
const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    testAction : bindActionCreators(testAction , dispatch),
  }
}
const withConnect = connect(mapStateToProps, mapDispatchToProps)

export default compose(withStyles(styles), withConnect)(ReadingTest);
