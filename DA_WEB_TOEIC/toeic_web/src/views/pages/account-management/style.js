import styled from "styled-components"
import {Field} from "formik";
import {Button, CardImg, CardTitle, Col, Label, Row} from "reactstrap"
import * as Icon from "react-bootstrap-icons";
import Spinner from "reactstrap/es/Spinner";

export const CardTitleStyle = styled(CardTitle)`
  &&&{
    width: 100%;
    text-align: center;
    font-weight: bold;
  }
`

export const ColStyle = styled(Col)`
  &&&{
    margin: 0 auto;
  }
`

export const ColAvatarStyle = styled(Col)`
  &&&{
    height: 196px;
    width: 33.33%;
    float: left;
    border: 1px solid white;
    border-radius: 100%;
    box-sizing: border-box;
    padding: 0;
  }
`

export const RowAvatarStyle = styled(Row)`
  &&&{
    position: relative;
    margin: 0;
  }
`

export const FieldInputFileStyle = styled(Field)`
  &&&{
    width: 0.1px;
    height: 0.1px;
    opacity: 0;
    overflow: hidden;
    position: absolute;
    z-index: -1;
  
    font-size: 10px;
    font-weight: 700;
    padding: 5px;
    color: white;
    background-color: grey;
    display: inline-block;
    cursor: pointer;
    border: 1px solid grey;
    border-radius: 50%;
    
    &:focus{
      background-color: #1ab7ea;
      border-color: #1ab7ea;
    }
  }
`

export const LableInputFileStyle = styled(Label)`
  &&&{
    width: 40%;
    text-align: center;
    position: absolute;
    bottom: 1px;
    left: 30%;
  
    font-size: 10px;
    font-weight: 700;
    padding: 5px;
    color: white;
    background-color: grey;
    display: inline-block;
    cursor: pointer;
    border: 1px solid grey;
    border-radius: 50%;
    
    &:hover{
      background-color: #1ab7ea;
      border-color: #1ab7ea;
    }
  }
`

export const CardImageStyle = styled(CardImg)`
  &&&{
    max-width: 100%;
    max-height: 100%;
    border-radius: 100%;
    margin: 0 auto;
  }
`

export const IconPersonFillStyle = styled(Icon.PersonFill)`
  &&&{
    width: 200px;
    height: 200px;
    margin: 0 auto;
    background: black;
    color: white;
    border: 1px solid white;
    border-radius: 50%;
  }
`

/*---------------------------------------------------*/
export const ColInfoStyle = styled(Col)`
  &&&{
    width: 66.67%;
    float: right;
    padding: 15px 0 0 50px;
  }
`

export const RowInfoButtonStyle = styled(Col)`
  &&&{
    margin: 0 0 15px 0;
  }
`

export const ButtonInfoStyle = styled(Button)`
  &&&{
    margin-right: 14%;
  }
`

export const SpinnerInfoStyle = styled(Spinner)`
  &&&{
    margin-top: 5px;
  }
`



