import React from "react"
import {Button, Card, CardBody, CardHeader, CardTitle , Col} from "reactstrap"

import {Field, Form, Formik} from "formik"
import {connect} from "react-redux"
import * as Yup from "yup"
import * as accountActions from "../../../redux/actions/account/accountActions"
// import * as Style from "./style"
import "../../../assets/css/style-modal-account.css"
import * as Icon from "react-bootstrap-icons"
import Spinner from "reactstrap/es/Spinner";
import styled from "styled-components"
import {FormattedMessage} from "react-intl";
import {history} from "../../../history";
import {showMessage} from "../../../commons/utils/convertDataForToast";

const ButtonCancel = styled(Button)`
  width : 100%;
  color : white !important;
`

const ButtonOK = styled(Button)`
  width : 100%;
`

const ColSpinner = styled(Col)`
  text-align : center;
`

class AccountManagement extends React.Component {
  state = {
    activeTab: "1",
    urlImage: "",
    fileUpload: null,
    messTypeFileError: "",
    isUpdating: false
  }

  // nếu props reducer có thay đổi thì setState
  componentWillReceiveProps(newProps) {
    this.setState({
      isUpdating: false
    })
  }

  toggleTab = tab => {
    if (this.state.activeTab !== tab) {
      this.setState({activeTab: tab})
    }
  }

  handleChangeFile = (e) => {
    let reader = new FileReader();
    let file = e.target.files[0]
    if (file !== undefined && (file.type.indexOf("image/jpg") !== -1 || file.type.indexOf("image/jpeg") !== -1 || file.type.indexOf("image/png") !== -1)) {
      //5242880
      if(file !== undefined && file.size > 5242880){
        this.setState({
          urlImage: "",
          fileUpload: null,
          messTypeFileError: showMessage("max.size.file.upload")
        })
      } else {
        reader.onloadend = () => {
          this.setState({
            urlImage: reader.result,
            fileUpload: file,
            messTypeFileError: ""
          });
        }
        reader.readAsDataURL(file)
      }
    }
    else if (file === undefined) {
      this.setState({
        urlImage: "",
        fileUpload: null
      })
    }
    else {
      this.setState({
        urlImage: "",
        fileUpload: null,
        messTypeFileError: showMessage("format.file.upload")
      })
    }
  }

  showUpdating = () => {
    this.setState({
      isUpdating: true
    })
  }

  onRefresh=()=>{
    history.push("/private/admin/")
  }

  render() {
    return (
      <>
      <style>{`
          #btnHighLight:focus-within {
            border: none !important;
            box-shadow: 0 0 1pt 1pt black !important;
          }

        `}

      </style>
      <React.Fragment>
        <Card>
          <CardHeader>
            <CardTitle className="title-account" style={{fontSize:'2rem'}}><FormattedMessage id="management.account"/></CardTitle>
          </CardHeader>
          <CardBody>
            <div className="row" style={this.state.messTypeFileError !== "" ? {paddingBottom : "30px"} : null}>
              <div className="col-xs-0 col-sm-0 col-md-1 col-lg-1 col-xl-1"></div>
              <div className="col-xs-12 col-sm-12 col-md-10 col-lg-10 col-xl-10">
                <Formik
                  enableReinitialize
                  initialValues={{
                    userName: this.props.accountInfo ? this.props.accountInfo.userName : "",
                    userShowName: this.props.accountInfo ? this.props.accountInfo.userShowName : "",
                  }}

                  validationSchema={Yup.object().shape({
                    userShowName : Yup.string()
                      .required("account.name.is.required")
                      .max(45 , "account.name.is.no.longer.than.45.character").nullable()
                  })}

                  onSubmit={(values,errors) => {
                    if(values.userShowName.toString().trim() === "" ){
                      values.userShowName = ''
                    } else {
                      let formData = new FormData()
                      if(this.state.fileUpload !== null){
                        formData.append(
                          "fileUpload",
                          this.state.fileUpload,
                        )
                      }
                      formData.append("userName" , values.userName)
                      formData.append("userShowName" , values.userShowName.toString().trim())
                      this.props.updateProfileAdmin(formData)
                    }
                  }}
                >
                  {({errors,isSubmitting, values}) => (
                    <Form>
                      <div className="col-xs-5 col-sm-4 col-md-4 col-lg-4 account-avatar mb-3">
                        <div className="row">
                          {
                            this.state.urlImage ?
                              <img className="avatar" src={this.state.urlImage} alt="" height="200px" width="200px" /> :
                            this.props.accountInfo ? this.props.accountInfo.userAvatar === null ?
                              <Icon.PersonFill/> :
                              <img className="avatar" src={this.props.accountInfo.userAvatar} alt="" height="200px" width="200px" />
                              :  <Icon.PersonFill/>
                          }
                          <Field type="file" id="file" name="file" className="input-file" onChange={(e) => { this.handleChangeFile(e) }}></Field>
                          <label htmlFor="file" className="lable-for-file"><span className="glyphicon glyphicon-open"></span><FormattedMessage id="upload"/></label>

                        </div>
                        <div className="row">
                          { this.state.messTypeFileError !== "" ? <label style={{color : "red" , width : "100%" , textAlign : "center"}}><strong>{this.state.messTypeFileError}</strong></label> : null }
                        </div>
                      </div>

                      <div className="col-xs-7 col-sm-8 col-md-8 col-lg-8 account_info mb-1">
                        <div className="row">
                          <Col md={4} lg={4} xl={3}>
                            <label style={{"lineHeight": "37.9px"}}><FormattedMessage id="email.login"/></label>
                          </Col>
                          <Col md={8} lg={8} xl={9}>
                            <Field type="email" name="userName" className="form-control" placeholder="" disabled></Field>
                          </Col>
                        </div>

                        <div className="row">
                          <Col md={4} lg={4} xl={3}>
                            <label style={{"lineHeight": "37.9px"}}><FormattedMessage id="account.name"/> (<span style={{color : "red"}}>*</span>)</label>
                          </Col>
                          <Col md={8} lg={8} xl={9}>
                            <Field type="text" name="userShowName" className="form-control" placeholder=""
                                   onMouseLeave={(e) => {
                                     if(e.target.value.toString().trim()==='') {
                                       values.userShowName= ''
                                       e.target.value=""
                                     }
                                   }}
                            ></Field>
                            { errors.userShowName ?  <strong><label style={{ color : "red" }}>
                              {/*{errors.userShowName}*/}
                              <FormattedMessage id={errors.userShowName}/>
                            </label></strong> : null }
                          </Col>
                        </div>

                        <div className="row">
                          <Col md={4}>
                            <ButtonCancel type="button" color="secondary" id="btnHighLight" onClick={this.onRefresh}><FormattedMessage id="cancel"/></ButtonCancel>
                          </Col>
                          <ColSpinner md={4}>
                            {this.props.isLoading && <Spinner color="primary"/>}
                          </ColSpinner>
                          <Col md={4}>
                            {
                              this.state.messTypeFileError !== "" || errors.userShowName ?
                                <ButtonOK type="submit" color="primary" disabled="disabled" id="btnHighLight">OK</ButtonOK> :
                                <ButtonOK type="submit" color="primary" onClick={this.showUpdating} id="btnHighLight">OK</ButtonOK>
                            }
                          </Col>
                        </div>
                      </div>
                    </Form>
                  )}
                </Formik>
              </div>
              <div className="col-xs-0 col-sm-0 col-md-1 col-lg-1 col-xl-1"></div>
            </div>
          </CardBody>
        </Card>
      </React.Fragment>
        </>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    accountInfo: state.auth.login.userInfoLogin,
    isLoading: state.auth.login.isLoadingUpdateProfile
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    updateProfileAdmin: (data) => {
      dispatch(accountActions.updateProfileAdmin(data));
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(AccountManagement)
