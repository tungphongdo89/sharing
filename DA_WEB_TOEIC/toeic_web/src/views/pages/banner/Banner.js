import React, {Component} from 'react';
import {connect} from "react-redux";
import bannerAction from "../../../redux/actions/banner/bannerAction";
import {Card, CardBody , CarouselItem} from "reactstrap"
import Swiper from "react-id-swiper"
import { useState, useRef } from 'react';
import BannerSlider from "./BannerSlider";
import styles from "./styles.scss"

const params = {
  Swiper,
  // Add modules you need
  // modules: [Navigation, Pagination],

  spaceBetween: 30,
  loop: true,
  centeredSlides: true,
  autoplay: {
    delay: 250000,
    disableOnInteraction: false
  },
  pagination: {
    el: ".swiper-pagination",
    clickable: true,
    type: 'bullets'
  },
  navigation: {
    nextEl: ".swiper-button-next",
    prevEl: ".swiper-button-prev"
  }

}


class Banner extends Component {


  componentDidMount() {
    this.props.getBanner()
  }

  render() {
    // let bannerItemMap = this.props.banner_Reducer.lstBanner.data ? this.props.banner_Reducer.lstBanner.data.map((banner, index) => {
    //   return <div key={index} style={{position: "relative"}}>
    //     <img src={banner.pathImage} alt="banner" className="img-fluid" style={{width: "100%", height:'60vh'}}/>
    //     <a href={banner.postLink} className="btn btn-info btn-lg active" role="button" aria-pressed="true"
    //        target="_blank"
    //        style={{
    //          position: "absolute",
    //          right: "1%",
    //          bottom: "2%",
    //          width : "15%",
    //          padding: "10px 5px",
    //          color: "black",
    //          background: "darkturquoise !important"
    //        }}
    //     >Xem chi tiết</a>
    //   </div>
    // }) : ""

    return (
      <div>
        <Card>
          <CardBody>
            {/*<Swiper {...params}>*/}
            {/*  {bannerItemMap}*/}
            {/*</Swiper>*/}
            <BannerSlider listBanner={this.props.banner_Reducer.lstBanner.data}/>
          </CardBody>
        </Card>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    banner_Reducer: state.bannerReducer
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getBanner: () => {
      dispatch(bannerAction.getBanner());
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Banner);

