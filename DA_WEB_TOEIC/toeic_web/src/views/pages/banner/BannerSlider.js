import classNames from "classnames";
import React, {Component} from 'react';
import {FormattedMessage} from "react-intl";

class BannerSlider extends Component {
  constructor(props) {
    super(props);

    this.IMAGE_PARTS = 1;

    this.changeTO = null;
    this.AUTOCHANGE_TIME = 4000;

    this.state = { activeSlide: -1, prevSlide: -1, sliderReady: false };
  }

  componentWillUnmount() {
    window.clearTimeout(this.changeTO);
  }

  componentDidMount() {
    this.runAutochangeTO();
    setTimeout(() => {
      this.setState({ activeSlide: 0, sliderReady: true });
    }, 0);
  }

  runAutochangeTO() {
    this.changeTO = setTimeout(() => {
      this.changeSlides(1);
      this.runAutochangeTO();
    }, this.AUTOCHANGE_TIME);
  }

  changeSlides(change) {
    window.clearTimeout(this.changeTO);
    if(this.props.listBanner){
      const { length } = this.props.listBanner;
      const prevSlide = this.state.activeSlide;
      let activeSlide = prevSlide + change;
      if (activeSlide < 0) activeSlide = length - 1;
      if (activeSlide >= length) activeSlide = 0;
      this.setState({ activeSlide, prevSlide });
    }

  }

  render() {
    const { activeSlide, prevSlide, sliderReady } = this.state;
    return (
      <div className={classNames('slider', { 's--ready': sliderReady })}>
        <div className="slider__slides">
          {this.props.listBanner ? this.props.listBanner.map((slide, index) => (

            <div className={classNames('slider__slide', { 's--active': activeSlide === index, 's--prev': prevSlide === index  })} key={slide.bannerName}>
              <div className="slider__slide-content">
                <a href={slide.postLink}
                   // className="slider__slide-readmore"
                   className="btn btn-info btn-lg active" role="button" aria-pressed="true"
                   target="_blank"
                   style={{
                     position: "absolute",
                     right: "1%",
                     bottom: "2%",
                     width : "15%",
                     padding: "10px 5px",
                     color: "black",
                     background: "darkturquoise !important"
                   }}
                >
                  {/*Xem chi tiết*/}
                  <FormattedMessage id="Detail" tagName="data" />
                </a>
              </div>

              <div className="slider__slide-parts">
                {[...Array(this.IMAGE_PARTS).fill()].map((x, i) => (
                  <div className="slider__slide-part" key={i}>
                    <div className="slider__slide-part-inner" style={{ backgroundImage: `url(${slide.pathImage})` }} />
                  </div>
                ))}
              </div>
            </div>
          )) : null}
        </div>
        {
          this.props.listBanner ? this.props.listBanner.length>1&&
          <div className="slider__control" onClick={() => this.changeSlides(-1)} />:""
        }
        {
          this.props.listBanner ? this.props.listBanner.length>1&&
            <div className="slider__control slider__control--right" onClick={() => this.changeSlides(1)} />:""
        }

      </div>
    );
  }
}

export default BannerSlider
