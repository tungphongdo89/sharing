const styles = () => ({
    icon:{
        width: '200px',
        height: '200px',
        borderRadius: '50%',
      marginBottom :'5px'
    },
    title:{
        width:'46%',
        textAlign:'right'
    },
    content:{
        width:'65%',
        textAlign: 'left',
        display:'flex'
    },
    root:{
        textAlign: 'center'
    },
    option:{
      borderRadius: '10px ! important',
      backgroundColor: 'white',
      color: 'black',
      height: '50px'
    }
})

export default styles;
