import React from "react"
import {Button, Card, CardBody, CardHeader, CardTitle, Col, Input, Row} from "reactstrap"
import "../../../../assets/scss/pages/account-settings.scss"
import {Grid, Typography, withStyles} from "@material-ui/core"
import styles from "./styles";
import {bindActionCreators, compose} from "redux";
import {connect} from "react-redux";
import userAction from "../../../../redux/actions/users/userActions";
import resendEmailAction from "../../../../redux/actions/auth/resendEmailActions";
import Spinner from "reactstrap/es/Spinner";
import {getUTCTimeSeconds} from "../../../../commons/utils/DateUtils";
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import {FormattedMessage} from "react-intl";
import {showMessage, showMessageVn} from "../../../../commons/utils/convertDataForToast";

class UserDetail extends React.Component {
  state = {
    activeTab: "1",
    windowWidth: null,
    isReSend: false,
    isUpdating: false,
    isPaymentUpdating: false
  }

  toggle = tab => {
    this.setState({
      activeTab: tab,
      dropdownOpen: !this.state.dropdownOpen
    })
  }

  updateWidth = () => {
    this.setState({windowWidth: window.innerWidth})
  }

  componentWillReceiveProps(nextProps, nextContext) {
    this.setState({
      isUpdating: false,
      isPaymentUpdating: false
    })
    console.log("nextProps", nextProps);
    const {reSendEmail} = nextProps;
    if (!reSendEmail) {
      this.setState({
        isReSend: false
      });
    }
  }

  componentDidMount() {
    if (window !== undefined) {
      this.updateWidth()
      window.addEventListener("resize", this.updateWidth)
    }
  }

  onChangeUserStatus = (e) => {
    this.setState({
      isUpdating: true
    })
    const {userActions, userDetail} = this.props;
    const {updateUserDetail} = userActions;
    const user = {};
    user.userId = userDetail.userId;
    user.status = e.target.value ? parseInt(e.target.value) : userDetail.status;
    updateUserDetail(user);
  }

  onChangePaymentStatus = (e) => {
    this.setState({
      isPaymentUpdating: true
    })
    const {userActions, userDetail} = this.props;
    const {updateUserDetail} = userActions;
    const user = {};
    user.userId = userDetail.userId;
    user.paymentStatus = e.target.value ? parseInt(e.target.value) : userDetail.paymentStatus;
    updateUserDetail(user);
  }

  onReSend = () => {
    debugger
    this.setState({
      isReSend: true
    })
    const {resendEmailAction, userDetail} = this.props;
    const {resendEmail} = resendEmailAction;
    const user = {};
    user.username = userDetail.userName;
    user.email = userDetail.userName;
    user.role = userDetail.codeRole;
    resendEmail(user);

  }
  loadingComponent = (
    <div style={{
      position: 'absolute',
      zIndex: 110,
      top: 0,
      left: 0,
      width: '100%',
      height: '100%',
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      background: 'rgba(255,255,255,0.8)'
    }}>
      <Spinner color="primary" className="reload-spinner"/>
    </div>
  );

  renderUserStatus() {
    let xhtml = null
    const {classes, userDetail} = this.props;
    if (userDetail && userDetail.status === -1) {
      xhtml = (
        <div className={classes.content}>
          <Input type="select"
                 name="select"
                 id="exampleSelect"
                 style={{
                   backgroundColor: userDetail.status === -1 ? '#ff9f43' : '#babfc7',
                   color: userDetail.status === -1 ? '#fff' : 'black',
                   fontWeight: '500',
                   cursor: 'pointer'
                 }}
                 className="ml-4 w-50"
                 defaultValue={userDetail.status}
                 onChange={this.onChangeUserStatus}
          >
            <option value={-1} className={classes.option}>{showMessage("wait.for.confirmation")}</option>
            <option value={0} className={classes.option}>{showMessage("inactive")}</option>
          </Input>
          <Button color="link" style={{fontSize: '12px', padding: '0', marginLeft: '5px'}}
                  onClick={this.onReSend}
          > <FormattedMessage id={"resend.confirm.email"}/></Button>
          <div style={{marginLeft: '20px'}}>
            {this.state.isUpdating && this.loadingComponent}
          </div>
          <div>
            {this.state.isReSend && this.loadingComponent}
          </div>

        </div>
      )
    } else if (userDetail && userDetail.status === 1) {
      xhtml = (
        <div className={classes.content}>
          <Input type="select"
                 name="select"
                 id="exampleSelect"
                 style={{
                   backgroundColor: userDetail.status === 1 ? '#28c76f' : '#babfc7',
                   color: userDetail.status === 1 ? '#fff' : 'black',
                   fontWeight: '500',
                   cursor: 'pointer'
                 }}
                 className="ml-4 w-50"
                 defaultValue={userDetail.status}
                 onChange={this.onChangeUserStatus}
          >
            <option value={1} className={classes.option}>{showMessage("active")}</option>
            <option value={0} className={classes.option}>{showMessage("inactive")}</option>
          </Input>
          <div style={{marginLeft: '0px'}}>
            {this.state.isUpdating && this.loadingComponent}
          </div>
        </div>
      )
    } else if (userDetail && userDetail.status === 0 && userDetail.verified !== 1) {
      xhtml = (
        <div className={classes.content}>
          <Input type="select"
                 name="select"
                 id="exampleSelect"
                 style={{
                   backgroundColor: userDetail.status === -1 ? '#ff9f43' : '#babfc7',
                   color: userDetail.status === -1 ? '#fff' : 'black',
                   fontWeight: '500',
                   cursor: 'pointer'
                 }}
                 className="ml-4 w-50"
                 defaultValue={userDetail.status}
                 onChange={this.onChangeUserStatus}
          >
            <option value={-1} className={classes.option}>{showMessage("wait.for.confirmation")}</option>
            <option value={0} className={classes.option}>{showMessage("inactive")}</option>
          </Input>
          <div style={{marginLeft: '20px'}}>
            {this.state.isUpdating && this.loadingComponent}
          </div>
        </div>
      );
    } else if (userDetail && userDetail.status === 0 && userDetail.verified === 1) {
      xhtml = (
        <div className={classes.content}>
          <Input type="select"
                 name="select"
                 id="exampleSelect"
                 style={{
                   backgroundColor: userDetail.status === 1 ? '#28c76f' : '#babfc7',
                   color: userDetail.status === 1 ? '#fff' : 'black',
                   fontWeight: '500',
                   cursor: 'pointer'
                 }}
                 className="ml-4 w-50"
                 defaultValue={userDetail.status}
                 onChange={this.onChangeUserStatus}
          >
            <option value={1} className={classes.option}>{showMessage("active")}</option>
            <option value={0} className={classes.option}>{showMessage("inactive")}</option>
          </Input>
          <div style={{marginLeft: '20px'}}>
            {this.state.isUpdating && this.loadingComponent}
          </div>
        </div>
      );
    }

    return xhtml;
  }

  checkRole(data){
    debugger
    if(data === showMessageVn("admin")){
      return "admin";
    }else if(data === showMessageVn("student")){
        return "student";
    }else if(data === showMessageVn("teacher")){
      return "teacher";
    }else{
      return "preview"
    }
  }

  renderPaymentStatus() {
    let xhtml = null;
    const {classes, userDetail} = this.props;
    if (userDetail.userType === 'Học sinh') {
      xhtml = (
        <Grid className="d-flex mb-1">
          <div className={classes.title}>
            <Typography variant="h6" gutterBottom><FormattedMessage id={"payment.status"}/>:</Typography>
          </div>
          <div className={classes.content}>
            <Input type="select"
                   name="select"
                   id="selectPaymentStatus"
                   style={{
                     backgroundColor: userDetail.paymentStatus === 1 ? '#28c76f' : '#ff9f43',
                     color: '#fff',
                     fontWeight: '500',
                     cursor: 'pointer'
                   }}
                   className="ml-4 w-50"
                   defaultValue={userDetail.paymentStatus}
                   onChange={this.onChangePaymentStatus}
            >
              <option value={0} className={classes.option}>{showMessage("wait.for.pay")}</option>
              <option value={1} className={classes.option}>{showMessage("paid")}</option>
            </Input>
            <div style={{marginLeft: '20px'}}>
              {this.state.isPaymentUpdating && this.loadingComponent}
            </div>
          </div>
        </Grid>
      )
    } else xhtml = null;
    return xhtml;
  }


  render() {
    debugger
    const {classes, userDetail} = this.props
    return (
      <React.Fragment>
        <div className="">
          <Card className={classes.root}>
            <CardHeader>
              <CardTitle className="w-100">
                <Typography variant="h4" component="h2" gutterBottom className="float-left" style={{fontWeight: '600'}}>
                  <FormattedMessage id={"user.details"}/> - {userDetail.userCode}
                </Typography>
              </CardTitle>
            </CardHeader>
            <CardBody>
              <Row>
                <Col xs="12" sm="12" lg="3">
                  {
                    userDetail.userAvatar ?
                      <img src={userDetail.userAvatar ? userDetail.userAvatar : ""}
                           className={classes.icon}/>
                      :
                      <AccountCircleIcon style={{height: '200px', width: '200px'}}/>
                  }

                </Col>
                <Col lg="6" sm="12" lg="7">
                  <Grid className="d-flex mb-1">
                    <div className={classes.title}>
                      <Typography variant="h6" gutterBottom><FormattedMessage id={"email.login"}/>:</Typography>
                    </div>
                    <div className={classes.content}>
                      <Typography variant="h6" gutterBottom className="ml-4" title={userDetail.userName}
                                  style={{
                                    whiteSpace: 'nowrap',
                                    width: '300px',
                                    overflow: 'hidden',
                                    textOverflow: 'ellipsis'
                                  }}>{userDetail.userName}</Typography>
                    </div>
                  </Grid>
                  <Grid className="d-flex mb-1">
                    <div className={classes.title}>
                      <Typography variant="h6" gutterBottom><FormattedMessage id={"account.name"}/>:</Typography>
                    </div>
                    <div className={classes.content}>
                      <Typography variant="h6" gutterBottom className="ml-4" title={userDetail.userShowName}
                                  style={{
                                    whiteSpace: 'nowrap',
                                    width: '300px',
                                    overflow: 'hidden',
                                    textOverflow: 'ellipsis'
                                  }}
                      >{userDetail.userShowName}</Typography>
                    </div>
                  </Grid>
                  <Grid className="d-flex mb-1">
                    <div className={classes.title}>
                      <Typography variant="h6" gutterBottom><FormattedMessage id={"phone.number"}/>:</Typography>
                    </div>
                    <div className={classes.content}>
                      <Typography variant="h6" gutterBottom className="ml-4">{userDetail.phone}</Typography>
                    </div>
                  </Grid>
                  <Grid className="d-flex mb-1">
                    <div className={classes.title}>
                      <Typography variant="h6" gutterBottom><FormattedMessage id={"user.type"}/>:</Typography>
                    </div>
                    <div className={classes.content}>
                      <Typography variant="h6" gutterBottom className="ml-4"><FormattedMessage
                        id={userDetail.codeRole ? userDetail.codeRole.toLowerCase() : this.checkRole(userDetail.userType)}/></Typography>
                    </div>
                  </Grid>
                  <Grid className="d-flex mb-1">
                    <div className={classes.title}>
                      <Typography variant="h6" gutterBottom><FormattedMessage id={"account.status"}/>:</Typography>
                    </div>
                    {this.renderUserStatus()}
                  </Grid>
                  <Grid className="d-flex mb-1">
                    <div className={classes.title}>
                      <Typography variant="h6" gutterBottom><FormattedMessage id={"current.score"}/>:</Typography>
                    </div>
                    <div className={classes.content}>
                      <Typography variant="h6" gutterBottom
                                  className="ml-4">{userDetail.currentScore}</Typography>
                    </div>
                  </Grid>
                  <Grid className="d-flex mb-1">
                    <div className={classes.title}>
                      <Typography variant="h6" gutterBottom><FormattedMessage id={"target"}/>:</Typography>
                    </div>
                    <div className={classes.content}>
                      <Typography variant="h6" gutterBottom className="ml-4">{userDetail.target}</Typography>
                    </div>

                  </Grid>
                  {this.renderPaymentStatus()}
                  <Grid className="d-flex mb-1">
                    <div className={classes.title}>
                      <Typography variant="h6" gutterBottom><FormattedMessage id={"created.time"}/>:</Typography>
                    </div>
                    <div className={classes.content}>
                      <Typography variant="h6" gutterBottom
                                  className="ml-4">{getUTCTimeSeconds(new Date(userDetail.createDate))}</Typography>
                    </div>
                  </Grid>
                  <Grid className="d-flex mb-1">
                    <div className={classes.title}>
                      <Typography variant="h6" gutterBottom><FormattedMessage id={"last.login"}/>:</Typography>
                    </div>
                    <div className={classes.content}>
                      <Typography variant="h6" gutterBottom
                                  className="ml-4">{userDetail.nearestLoginDate ? getUTCTimeSeconds(new Date(userDetail.nearestLoginDate)) : ''}</Typography>
                    </div>
                  </Grid>
                  <Grid className="d-flex mb-1">
                    <div className={classes.title}>
                      <Typography variant="h6" gutterBottom><FormattedMessage id={"update.time"}/>:</Typography>
                    </div>
                    <div className={classes.content}>
                      <Typography variant="h6" gutterBottom
                                  className="ml-4">{userDetail.updateDate ? getUTCTimeSeconds(new Date(userDetail.updateDate)) : ''}</Typography>
                    </div>
                  </Grid>

                </Col>
              </Row>
            </CardBody>
          </Card>
        </div>
      </React.Fragment>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    userDetail: state.userReducer.userDetail,
    reSendEmail: state.resendEmailReducer
  }
};

const mapDispatchToProps = dispatch => ({
  userActions: bindActionCreators(userAction, dispatch),
  resendEmailAction: bindActionCreators(resendEmailAction, dispatch)
});

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
);

export default compose(
  withStyles(styles),
  withConnect
)(UserDetail);

