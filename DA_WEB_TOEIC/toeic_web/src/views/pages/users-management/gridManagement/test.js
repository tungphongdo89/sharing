import React, {Component} from 'react';
// import * as Api from "../../../apis/task";
import MUIDataTable from "mui-datatables";
import PropTypes from 'prop-types';

class TablePagingTable4 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      page: 1,
      pageSize: 10,
      count: 1,
      data: [["Loading Data..."]],
      isLoading: false,
      rowsPerPageOptions: [5, 10, 15]
    }
  }

    componentWillMount() {
      // const {data} = this.props
      // this.setState({
      //   data: data.data,
      //   page: 0,
      //   count: data.total
      // });
    }
    //
    // componentDidMount() {
    //   const {data} = this.props
    //   this.setState({
    //     data: data.data
    //   });
    // }

    componentWillReceiveProps(nextProps, nextContext) {
      const {data} = nextProps
      this.setState({
        data: data.data,
        page: 0,
        count: data.total
      });
    }


  // // get data
  // getData = async () => {
  //   const {url} = this.props;
  //   const {page, pageSize, keySearch} = this.state;
  //   const obj = {};
  //   obj.page = page
  //   obj.pageSize = pageSize
  //   obj.keySearch = keySearch
  //   const response = await Api.fecthUser(url, obj);
  //   if (response.data) {
  //     this.setState({data: response.data.data, page: page, count: response.data.total});
  //   }
  // }
  changePage = async (page) => {
    const {url} = this.props;
    const {pageSize, keySearch} = this.state;
    const obj = {};
    obj.page = page + 1
    obj.pageSize = pageSize
    obj.keySearch = keySearch
    // const response = await Api.fecthUser(url, obj);
    // if (response.data) {
    //   this.setState({data: response.data.data, page: page, count: response.data.total});
    // }
  };
  changeRowsPerPage = async (rowsPerPage) => {
    const {url} = this.props;
    const {page, keySearch} = this.state;
    const obj = {};
    obj.page = 1
    obj.pageSize = rowsPerPage
    // const response = await Api.fecthUser(url, obj);
    // if (response.data) {
    //   this.setState({
    //     data: response.data.data,
    //     page: 1,
    //     count: response.data.total,
    //     pageSize: rowsPerPage
    //   });
    // }
  };

  searchData = (data) => {
    const {url} = this.props;
    const parent = this;
    const {state} = this;
    const {page, pageSize} = state;
    const obj = {};
    obj.page = page
    obj.pageSize = pageSize
    obj.keySearch = data
    // const result = Api.fecthUser(url, obj).then(function (response) {
    //   console.log("state", state);
    //   parent.setState({data: response.data.data, page: response.data.page, count: response.data.total});
    //   // return response.data;
    // }).catch(function (error) {
    //   console.log("vcl error", error);
    // });
    // if (result) {
    //   this.setState({data: result.data, page: result.page, count: result.total});
    // }
  };

  render() {
    console.log('co vao lai ko');
    const {count, data, page, rowsPerPageOptions} = this.state;
    console.log('data', data);
    const {keySearch} = this.props;

    const options = {
      filter: false,
      filterType: "dropdown",
      responsive: "scroll",
      print: false,
      download: false,
      count: count,
      search: false,
      page: page,
      serverSide: true,
      rowsPerPageOptions: rowsPerPageOptions,
      onTableChange: (action, tableState) => {
        console.log(action, tableState);
        switch (action) {
          case 'changePage':
            this.changePage(tableState.page);
            break;
          case 'search':
            this.searchData(tableState.searchText);
            break;
          case 'changeRowsPerPage':
            this.changeRowsPerPage(tableState.rowsPerPage);
            break;

        }
      }
    };
    const columns = [
      {
        name: "userId",
        label: "ID",
        options: {
          filter: false,
        }
      }
      ,
      {
        name: "userName",
        label: "Tài khoản",
        options: {
          filter: true,
          sort: false,
        }
      },
      {
        name: "password",
        label: "Mật khẩu",
        options: {
          filter: true,
          sort: false,
        }
      },
      {
        name: "status",
        label: "Trạng thái",
        options: {
          filter: true,
          sort: false,
        }
      }
    ];

    return (
      <MUIDataTable
        title={"ACME Employee list"}
        // data={data}
        columns={columns}
        options={options}
      />
    );
  }
}

TablePagingTable4.propTypes = ({
  url: PropTypes.string
})

export default TablePagingTable4;
