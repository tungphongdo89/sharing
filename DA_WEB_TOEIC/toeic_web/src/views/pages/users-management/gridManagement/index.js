import React, {Component} from 'react';
import PropTypes from "prop-types";
import MUIDataTable from "mui-datatables";
import {useTheme, makeStyles} from "@material-ui/core/styles";
import {withStyles} from "@material-ui/core";
import styles from "./styles"

import {ChevronDown, Eye, RotateCw, X} from "react-feather";

import {bindActionCreators, compose} from "redux";
import userAction from "../../../../redux/actions/users/userActions";
import {connect} from "react-redux"

const rowsPerPageOptions = [5, 10, 15]

const defaultRowsPerPageOption = 5
const useStyles1 = makeStyles(theme => ({
  root: {
    flexShrink: 0,
    marginLeft: theme.spacing(2.5)
  }
}));


// function handleFirstPageButtonClick(event) {
//   onChangePage(event, 0);
// }
//
// function handleBackButtonClick(event) {
//   onChangePage(event, page - 1);
// }
//
// function handleNextButtonClick(event) {
//   onChangePage(event, page + 1);
// }

//   function handleLastPageButtonClick(event) {
//     onChangePage(event, Math.max(0, Math.ceil(count / rowsPerPage) - 1));
//
//
//   return (
//     <div className={classes.root}>
//       <IconButton
//         onClick={handleFirstPageButtonClick}
//         disabled={page === 0}
//         aria-label="first page"
//       >
//         {theme.direction === "rtl" ? <LastPageIcon/> : <FirstPageIcon/>}
//       </IconButton>
//       <IconButton
//         onClick={handleBackButtonClick}
//         disabled={page === 0}
//         aria-label="previous page"
//       >
//         {theme.direction === "rtl" ? (
//           <KeyboardArrowRight/>
//         ) : (
//           <KeyboardArrowLeft/>
//         )}
//       </IconButton>
//       <IconButton
//         onClick={handleNextButtonClick}
//         disabled={page >= Math.ceil(count / rowsPerPage) - 1}
//         aria-label="next page"
//       >
//         {theme.direction === "rtl" ? (
//           <KeyboardArrowLeft/>
//         ) : (
//           <KeyboardArrowRight/>
//         )}
//       </IconButton>
//       <IconButton
//         onClick={handleLastPageButtonClick}
//         disabled={page >= Math.ceil(count / rowsPerPage) - 1}
//         aria-label="last page"
//       >
//         {theme.direction === "rtl" ? <FirstPageIcon/> : <LastPageIcon/>}
//       </IconButton>
//     </div>
//   )
// }


class LazyTablePaging extends Component {
  constructor(props) {
    super(props);
    this.state = {
      collapse: true,
      pages: 0,
      columns: props.columns,
      search: "Nhập thông tin người dùng",
      keySearch: '',
      roleID: "0",
      loading: false,
      paymentStatus: "2",
      createDateFrom: "",
      createDateTo: "",
      status: "2",
      isLoading: false,
      page: 1,
      pageSize: 10,
      count: 1,
      data: [["Loading Data..."]],
      rowsPerPageOptions: [5, 10, 15]
    }
  }


  toggleCollapse = () => {
    this.setState(state => ({collapse: !this.state.collapse}))
  }
  onEntered = () => {
    this.setState({status: "Opened"})
  }
  onEntering = () => {
    this.setState({status: "Opening..."})
  }
  onEntered = () => {
    this.setState({status: "Opened"})
  }
  onExiting = () => {
    this.setState({status: "Closing..."})
  }
  onExited = () => {
    this.setState({status: "Closed"})
  }
  removeCard = () => {
    this.setState({isVisible: false})
  }

  componentWillMount() {
    // const {data} = this.props
    // this.setState({
    //   data: data.data,
    //   page: 0,
    //   count: data.total
    // });
  }

  componentDidMount() {
    let user = {};
    user.page = this.state.page;
    user.pageSize = this.state.pageSize;
    const {userAction} = this.props;
    const {fetchUser} = userAction;
    const resp = fetchUser(user);
    const {total} = this.props;
    this.setState({
      count: total
    })
    console.log("resp", resp)
  }

  componentWillReceiveProps(nextProps, nextContext) {
  }

  changePage = async (page) => {
    const {url} = this.props;
    const {pageSize, keySearch} = this.state;
    const user = {};
    user.page = page + 1
    user.pageSize = pageSize
    user.keySearch = keySearch;
    // user.keySearch = keySearch
    const {userAction} = this.props;
    const {fetchUser} = userAction;
    const resp = await fetchUser(user);
    // const response = await Api.fecthUser(url, obj);
    // if (response.data) {
    //   this.setState({data: response.data.data, page: page, count: response.data.total});
    // }
  };
  changeRowsPerPage = async (rowsPerPage) => {
    const {url} = this.props;
    const {page, keySearch} = this.state;
    const obj = {};
    obj.page = 1
    obj.pageSize = rowsPerPage
    const {userAction} = this.props;
    const {fetchUser} = userAction;
    const resp = await fetchUser(obj);
    // const response = await Api.fecthUser(url, obj);
    // if (response.data) {
    //   this.setState({
    //     data: response.data.data,
    //     page: 1,
    //     count: response.data.total,
    //     pageSize: rowsPerPage
    //   });
    // }
  };

  searchData = (data) => {
    const {url} = this.props;
    const parent = this;
    const {state} = this;
    const {page, pageSize} = state;
    const obj = {};
    obj.page = page
    obj.pageSize = pageSize
    obj.keySearch = data
    // const result = Api.fecthUser(url, obj).then(function (response) {
    //   console.log("state", state);
    //   parent.setState({data: response.data.data, page: response.data.page, count: response.data.total});
    //   // return response.data;
    // }).catch(function (error) {
    //   console.log("vcl error", error);
    // });
    // if (result) {
    //   this.setState({data: result.data, page: result.page, count: result.total});
    // }
  };

  render() {
    console.log('co vao lai ko');
    const {count, data, page, rowsPerPageOptions} = this.state;
    console.log('data', data);
    const {keySearch} = this.props;

    const options = {
      filter: false,
      filterType: "dropdown",
      responsive: "scroll",
      print: false,
      download: false,
      count: count,
      search: false,
      viewColumns: false,
      page: page,
      // checkbox: false,
      serverSide: true,
      rowsPerPageOptions: rowsPerPageOptions,
      onTableChange: (action, tableState) => {
        console.log(action, tableState);
        switch (action) {
          case 'changePage':
            this.changePage(tableState.page);
            break;
          case 'search':
            this.searchData(tableState.searchText);
            break;
          case 'changeRowsPerPage':
            this.changeRowsPerPage(tableState.rowsPerPage);
            break;

        }
      }
    };
    const columns = [
      {
        name: "userCode",
        label: "Mã người dùng",
        options: {
          filter: false,
        }
      },
      {
        name: "userName",
        label: "Email đăng nhập",
        options: {
          filter: true,
          sort: false,
        }
      },
      {
        name: "userShowName",
        label: "Tên tài khoản",
        options: {
          filter: true,
          sort: false,
        }
      },
      {
        name: "phone",
        label: "Số điện thoại",
        options: {
          filter: true,
          sort: false,
        }
      },
      {
        name: "userAvatar",
        label: "Avatar",
        options: {
          filter: true,
          sort: false,
          customBodyRender: (data) => {
            return ( <div  className="d-flex align-items-center cursor-pointer">
              <img
                className="rounded-circle mr-50"
                src={data}
                alt="user avatar"
                height="30"
                width="30"
              />
            </div>)
          }
        }
      },
      {
        name: "codeRole",
        label: "Loại người dùng",
        options: {
          filter: true,
          sort: false,
          customBodyRender: (data) => {
            return data === 'ADMIN' ?(<div className="text-center" >Quản trị viên</div>) :
                    data === 'STUDENT' ?(<div className="text-center">Học sinh</div>) :
                      data === 'PREVIEW' ? (<div className="text-center">Preview</div>) : null
          }

        }
      },
      {
        name: "status",
        label: "Trạng thái tài khoản",
        options: {
          filter: true,
          sort: false,
          customBodyRender: (data) => {
            return data === 1 ?(<div  className="badge badge-pill badge-success" style={{color: 'black ! important'}}>Đang hoạt động</div>) :
              data === 0 ?(<div className="badge badge-pill badge-light" style={{color: '#000'}} >Ngừng hoạt động</div>) :
                data === -1 ? (<div className="badge badge-pill badge-warning" style={{color: 'white'}} >Chờ xác nhận</div>) : null

          }
        }
      },
      {
        name: "paymentStatus",
        label: "Trạng thái thanh toán",
        options: {
          filter: true,
          sort: false,
          customBodyRender: (data) => {
            return data === 1 ?(<div   className="badge badge-pill badge-success">Đã thanh toán</div>) :
              data === 0 ?(<div className="badge badge-pill badge-warning" >Chưa thanh toán</div>) : null
          }
        }
      },
      {
        name: "status",
        label: "Action",
        options: {
          filter: true,
          sort: false,
          customBodyRender: (data) => {
            return (<div>
              <Eye
                className="mr-50"
                size={15}
                onClick={() => {

                }
                }
              />
            </div>)
          }
        }
      }
    ];
    const {dataList} = this.props;
    return (
      <div>
        <MUIDataTable
          title={"ACME Employee list dsfs"}
          data={dataList}
          columns={columns}
          options={options}
        />
      </div>
    );
  }

}

// LazyTablePaging.propTypes = {
//   count: PropTypes.number.isRequired,
//   onChangePage: PropTypes.func.isRequired,
//   page: PropTypes.number.isRequired,
//   rowsPerPage: PropTypes.number.isRequired
// };
const mapStateToProps = state => {
  return {
    dataList: state.userReducer.lstUser,
    total: state.userReducer.total
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    userAction: bindActionCreators(userAction, dispatch)

  }
}

export default compose(withStyles(styles), connect(mapStateToProps, mapDispatchToProps))(LazyTablePaging);
