import React, {Component} from 'react';
import {
  Card, CardText, CardBody,
  CardTitle, ButtonToggle, Col, Row, Input, FormGroup,
} from 'reactstrap';
import styles from './styles';
import {withStyles} from "@material-ui/styles";
import {connect} from "react-redux";
import {bindActionCreators, compose} from "redux";
import contactActions from "../../../../redux/actions/contact";
import {Field, Formik, Form} from "formik";
import * as Yup from "yup";
import Spinner from "reactstrap/es/Spinner";
import {FormattedMessage} from "react-intl";
import {showMessage} from "../../../../commons/utils/convertDataForToast";


class FormContact extends Component {
  myRef = React.createRef();
  executeScroll = () => this.myRef.current.scrollIntoView();

  componentDidMount() {
    this.executeScroll();
  }

  render() {
    const
      formSchema = Yup.object().shape({
        name: Yup.string().required("please.enter.your.contact.name").max(160, "contact.name.is.no.longer.than.160.character"),
        context: Yup.string().required("please.enter.content"),
        email: Yup.string()
          .email("invalid.email.contact.format")
          .max(45,
            // 'Email không được dài quá 45 ký tự!'
           "email.cant.be.longer.than.45.characters"
          )
          .matches(
            // /^[a-z0-9][\-_\.\+\!\#\$\%\&\'\*\/\=\?\^\`\{\|]{0,1}([a-z0-9][\-_\.\+\!\#\$\%\&\'\*\/\=\?\^\`\{\|]{0,1})*[a-z0-9]@[a-z0-9][-\.]{0,1}([a-z][-\.]{0,1})*[a-z0-9]\.[a-z0-9]{1,}([\.\-]{0,1}[a-z]){0,}[a-z0-9]{0,}$/,
            // /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/,
            // /^((http[s]?|ftp|https|HTTPS|HTTP|FTP):\/)?\/?((\/+)*\/)(.*)?(#[\w\-]+)?$/,
            /^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i,
            "invalid.email.contact"
          ),
      });
    const {classes, loading} = this.props;
    return (
      <>
        <style>{`
          #btnSendContact:focus{
            // background-color: #2EE59D;
            box-shadow: 0px 15px 20px rgba(9, 132, 255, 0.3) !important;
            // color: #fff;
            transform: translateY(-3px);
          }
          `}</style>
        <div ref={this.myRef}>
          <Card>
            <CardBody>
              <CardTitle><h1 style={{fontWeight: "bolder"}}>
                {/*Liên hệ*/}
                <FormattedMessage id="contact"/>
              </h1><br/></CardTitle>
              <CardText>
                <Formik
                  onSubmit={(values, actions) => {
                    setTimeout(() => {
                      actions.setSubmitting(false);
                      // const {contactAction} = this.props;
                      // const {sendContact} = contactAction;
                      // sendContact(values.email, values.name, values.context);
                    }, 1000);
                    const {contactAction} = this.props;
                    const {sendContact} = contactAction;
                    sendContact(values.email, values.name, values.context);
                    setTimeout(()=>{
                      actions.resetForm()
                    }, 3000)

                    // actions.resetForm()
                  }}
                  initialValues={{
                    name: this.props.accountDetail ?
                      this.props.accountDetail.userShowName ?
                        this.props.accountDetail.userShowName
                        : "" : "",
                    email: this.props.accountDetail ?
                      this.props.accountDetail.userName ?
                        this.props.accountDetail.userName
                        : "" : "",
                    context: "",
                  }}
                  validationSchema={formSchema}
                >
                  {({errors, touched, values}) => (
                    <Form>
                      <FormGroup className="my-3">
                        <Row>
                          <Col className="text-right" sm="2">Email (<span style={{color: "red"}}>*</span>)</Col>
                          <Col xs="10">
                            {
                              this.props.accountDetail ?
                                this.props.accountDetail.userName ?
                                  <Field
                                    autoFocus="true"
                                    disabled={true} type="email" name="email" id="email"
                                    className={`form-control ${errors.email &&
                                    touched.email &&
                                    "is-invalid"}`}
                                  /> :
                                  <Field
                                    autoFocus="true"
                                    type="email" name="email" id="email"
                                    className={`form-control ${errors.email &&
                                    touched.email &&
                                    "is-invalid"}`}
                                  /> :
                                <Field
                                  autoFocus="true"
                                  type="email" name="email" id="email"
                                  className={`form-control ${errors.email &&
                                  touched.email &&
                                  "is-invalid"}`}
                                />
                            }
                            {errors.email && touched.email ? (
                              <div className="invalid-feedback mt-25">
                                {/*{errors.email}*/}
                                <FormattedMessage id={errors.email}/>
                              </div>
                            ) : null}
                          </Col>
                        </Row>
                      </FormGroup>
                      <FormGroup className="my-3">
                        <Row>
                          <Col className="text-right" sm="2">
                            {/*Tên liên hệ*/}
                            <FormattedMessage id="contact.name"/>{' '}
                            (<span style={{color: "red"}}>*</span>)</Col>
                          <Col xs="10">
                            <Field name="name" id="name"
                                   className={`form-control ${errors.name &&
                                   touched.name &&
                                   "is-invalid"}`}
                                   onMouseLeave={(e) => {
                                     if (e.target.value.toString().trim() === '') {
                                       values.name = ''
                                       e.target.value = ""
                                     }
                                   }}
                                   EscapeOutside={(e) => {
                                     if (e.target.value.toString().trim() === '') {
                                       values.name = ''
                                       e.target.value = ""
                                     }
                                   }}
                            />
                            {errors.name && touched.name ? (
                              <div className="invalid-feedback mt-25">
                                {/*{errors.name}*/}
                                <FormattedMessage id={errors.name}/>
                              </div>
                            ) : null}
                          </Col>
                        </Row>
                      </FormGroup>
                      <FormGroup className="my-3">
                        <Row>
                          <Col className="text-right" sm="2">
                            {/*Nội dung*/}
                            <FormattedMessage id="content"/>{' '}
                            (<span style={{color: "red"}}>*</span>)</Col>
                          <Col xs="10">
                            <Field name="context" id="context"
                                   as="textarea"
                                   className={`form-control ${errors.context &&
                                   touched.context &&
                                   "is-invalid"}`}
                                   onMouseLeave={(e) => {
                                     if (e.target.value.toString().trim() === '') {
                                       values.context = ''
                                       e.target.value = ""
                                     }
                                   }}
                                   EscapeOutside={(e) => {
                                     if (e.target.value.toString().trim() === '') {
                                       values.context = ''
                                       e.target.value = ""
                                     }
                                   }}
                            />
                            {errors.context && touched.context ? (
                              <div className="invalid-feedback mt-25">
                                {/*{errors.context}*/}
                                <FormattedMessage id={errors.context}/>
                              </div>
                            ) : null}
                          </Col>
                        </Row>
                      </FormGroup>
                      <Row>
                        <Col className="text-right" sm="10" ml="80%">
                          {
                            loading && <Spinner color="primary"/>
                          }
                        </Col>
                        <Col className="text-right" sm="2" ml="80%">
                          {
                            (errors.name || errors.email || errors.context)
                            ||
                            (!touched.name && !touched.email && !touched.context) ?
                              <ButtonToggle disabled={true} type="submit" color="primary" id="btnSendContact">
                                {/*Gửi Liên Hệ*/}
                                <FormattedMessage id="send.the.contact"/>
                              </ButtonToggle>
                              :
                              <ButtonToggle type="submit" color="primary" id="btnSendContact">
                                {/*Gửi Liên Hệ*/}
                                <FormattedMessage id="send.the.contact"/>
                              </ButtonToggle>
                          }
                        </Col>
                      </Row>
                    </Form>
                  )}
                </Formik>
              </CardText>
            </CardBody>
          </Card>
        </div>
      </>
    );
  }
}


const mapDispatchToProps = dispatch => {
  return {
    contactAction: bindActionCreators(contactActions, dispatch)
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    loading: state.ContactReducer.loading,
    accountDetail: state.auth.login.userInfoLogin
  }
}
const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
)


export default compose(withStyles(styles), withConnect)(FormContact);
