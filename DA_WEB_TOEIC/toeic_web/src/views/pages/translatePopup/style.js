
const styles = ({
  textAlign:{
    textAlign : 'center',
  },
  titlePopover:{
    fontWeight: 700,
    fontStyle: 'normal',
    fontSize: '24px',
    textAlign: 'center',
    fontFamily:  "'Arial-BoldMT', 'Arial Bold', 'Arial', sans-serif",
    height: '28px',
  },
  popoverBody:{
    fontWeight: 400,
    fontStyle: 'normal',
    fontSize: '13px',
    letterSpacing: 'normal',
    color: '#333333',
    fontFamily: "'Arial' , sans-serif",
    maxHeight: '250px' ,
    overflowY: 'auto',
    padding: '5px 5px 5px 30px',
  },
  error: {
    color: 'red',
    fontSize: 12
  },
  showdetail: {
    readOnly: true
  },
  iconPlay:{
    // padding: '0px !important',
    color: '#1ee61b',   //rgba(22, 155, 213, 1)
    cursor:'pointer' 
  },
  iconPlayPlaying:{
    // padding: '0px !important',
    color: '#1ee99b',   //rgba(22, 155, 213, 1)
    cursor:'pointer' 
  },
  btnPlayMp3:{
    textAlign : 'center',
    background: 'none !important',
    border: 'none !important',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  // #1ee61b
});
export default styles;
