import { connect } from "react-redux";
import React from "react";
import { Button, PopoverHeader, PopoverBody, Popover } from "reactstrap"
import wordAction from "../../../redux/actions/word/wordAction";
import { bindActionCreators, compose } from "redux";
import { Fragment } from "react";
import styles from "./style";
import { withStyles } from "@material-ui/core";
import MultiPlayerMP3 from "./MultiPlayerMP3";
import { X } from "react-feather";
import { Headphones } from "react-bootstrap-icons";
import {FormattedMessage} from "react-intl";

class TranslatePopup extends React.Component {
  constructor(props) {
    super(props);
  }
  componentWillMount() {
    this.state = {
      stateCordinate: this.props.word.cordinate,
    };
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    //debugger;
    if (nextProps) {
      this.setState({
        stateCordinate: nextProps.word.cordinate,
      });
    }
  }
  toggleModal = () => {
    this.props.action.togglePopup(!this.props.word.status);
  }
  cancel = () => {
    this.props.action.fetchDetailVocabularyFailure({});
  }
  render() {
    const { stateCordinate } = this.state;
    const { classes, word } = this.props;
    const { description } = word;
    var description1 = word.description;
    if(description1){
      description1 = description1.replace(/Danh từ/ig, '<span style="color:#f77112;">Danh từ</span>');
      description1 = description1.replace(/Mạo từ/ig, '<span style="color:#f77112;">Mạo từ</span>');
      description1 = description1.replace(/Động từ/ig, '<span style="color:#f77112;">Động từ</span>');
      description1 = description1.replace(/Trạng từ/ig, '<span style="color:#f77112;">Trạng từ</span>');
      description1 = description1.replace(/Giới từ/ig, '<span style="color:#f77112;">Giới từ</span>');
      description1 = description1.replace(/Tính từ/ig, '<span style="color:#f77112;">Tính từ</span>');
      description1 = description1.replace(/Phó từ/ig, '<span style="color:#f77112;">Tính từ</span>');
      description1 = description1.replace(/Đại từ/ig, '<span style="color:#f77112;">Danh từ</span>');
    }
  
    console.log(description1);
    return (
      <div style={{ position: 'absolute', left: stateCordinate.x + 'px', top: stateCordinate.y + 'px', }}>
        {/* <OverlayTrigger trigger="click" rootClose  > */}
        <Button color="primary" outline id="controlledPopover"
          style={{
            visibility: 'hidden', //hidden
            // position: 'absolute', left: stateCordinate.x + 'px',top: stateCordinate.y + 'px' ,

          }}  //, marginLeft:cordinate.x+'px' 
        >
          Controlled
        </Button>
        {
          <div >
            <Popover
              placement="bottom" className="popoverModal"
              style={{ minWidth: '250px', width: '100%' }}
              target="controlledPopover"
              isOpen={word.status}
              toggle={this.toggleModal}
            >
              <PopoverHeader style={{ textAlign: 'center' }} >
                {
                   word.foundSuccess ?
                      <MultiPlayerMP3
                        urls={[word.mp3]}
                        data={word.title}
                      />
                      :
                      <div style={styles.btnPlayMp3} className="row">
                        <div style={styles.titlePopover}> {word.title} </div>
                        <span style={{
                          color: 'lightgray',
                          cursor: 'pointer',
                          position: 'absolute',
                          right: '0'
                        }}
                          onClick={() => {
                            this.cancel()
                          }}
                        ><X size={18} /></span>
                      </div>
                 }       
              </PopoverHeader>
              <PopoverBody style={styles.popoverBody} >
                {word.foundSuccess ?
                  <Fragment>
                   
                      {word.transcribe}  
                      <div dangerouslySetInnerHTML={{ __html: `${description1}` }} /> 
                      {word.synonymous && word.synonymous.trim().length > 0 ? <span>  <span  style={{color:'#f77112'}}><FormattedMessage id="synonym" tagName="data" /></span>: <i>{word.synonymous}</i> </span>: ''}
                  
                  </Fragment>
                  :
                  <div style={{ textAlign: 'center' }} > <FormattedMessage id="notFoundWord" tagName="data" /> "<b style={{ color: 'red' }}>{word.wordInHtml}</b>" <FormattedMessage id="notFoundWord2" tagName="data" /><br /><i> <FormattedMessage id="thanks" tagName="data" /></i></div>
                }
              </PopoverBody>
            </Popover> </div>}
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    word: state.wordReducer
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    action: bindActionCreators(wordAction, dispatch)
  }
}

export default compose(withStyles(styles), connect(mapStateToProps, mapDispatchToProps))(TranslatePopup)

