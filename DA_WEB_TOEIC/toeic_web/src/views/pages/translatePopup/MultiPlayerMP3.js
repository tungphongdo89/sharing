import React, { useState, useEffect } from 'react'
import styles from "./style";
import { Headphones } from "react-bootstrap-icons";
import { Fragment } from 'react';
import { X } from "react-feather"
import { connect } from "react-redux";
import wordAction from "../../../redux/actions/word/wordAction";
import { bindActionCreators } from "redux";

const useMultiAudio = (urls, word, action) => {
  const sources = useState(
    urls.map(url => {
      return {
        url,
        audio: new Audio(url),
      }
    }),
  )

  const [players, setPlayers] = useState(
    urls.map(url => {
      return {
        url,
        playing: false,
        audio: new Audio(url),
      }
    }),
  )

  let toggle = (i) => () => {
    const newPlayers = [...players]
    if (word.statusListening === false) {
      newPlayers[0].playing = true
      action.toggleStatusListening(true);
      setPlayers(newPlayers)
    } else {
      players.forEach((source, i) => {
        source.audio.load()
        source.audio.play()
      })

    }
  }

  useEffect(() => {
    players.forEach((source, i) => {
      word.statusListening   ? source.audio.play() : source.audio.load()
    })
  }, [sources, players])

  useEffect(() => {
    players.forEach((source, i) => {
      source.audio.addEventListener('ended', () => {
        const newPlayers = [...players]
        newPlayers[i].playing = false
        action.toggleStatusListening(false);
        setPlayers(newPlayers)
      })
    })
    return () => {
      players.forEach((source, i) => {
        source.audio.removeEventListener('ended', () => {
          const newPlayers = [...players]
          newPlayers[i].playing = false
          action.toggleStatusListening(false);
          setPlayers(newPlayers)
        })
      })
    }
  }, [])

  return [players, toggle]
}

let MultiPlayerMP3 = ({ urls, data, word, action }) => {
  const [players, toggle] = useMultiAudio(urls, word, action)
  return (
    <div>
      { players.map((player, i) => (
        <Player key={i} player={player} toggle={toggle(i)} data={data} />
      ))}
    </div>
  )
}

let Player = ({ player, toggle, data, word, action }) => {
  return (
    <div style={styles.btnPlayMp3} className="row">
        <div style={{
          fontWeight: 700,
          fontStyle: 'normal',
          fontSize: '24px',
          textAlign: 'center',
          fontFamily: "'Arial-BoldMT', 'Arial Bold', 'Arial', sans-serif",
          height: '28px',
          whiteSpace: 'nowrap',
          overflow: 'hidden',
          textOverflow: 'ellipsis',
          width: data.length > 18 && word.foundSuccess ? '170px' : ''
        }}
          title={data}> {data} </div>

        {word.foundSuccess ?
          <Fragment >
            {player.playing ?
              <Headphones onClick={toggle} size={25} style={styles.iconPlayPlaying} className="col-2 pl-0 pr-0" /> :
              <Headphones onClick={toggle} size={25} style={styles.iconPlay} className="col-2 pl-0 pr-0" />
            }

          </Fragment> : ''
        }
        <span style={{
          color: 'lightgray',
          cursor: 'pointer',
          position: 'absolute',
          right: '0'
        }}
          onClick={() => {
             action.toggleStatusListening(false)
             setTimeout(() => action.fetchDetailVocabularyFailure({}) , 100);
          }}
        ><X size={18} /></span>
    </div>
  )
}
const mapStateToProps = state => {
  return {
    word: state.wordReducer
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    action: bindActionCreators(wordAction, dispatch)
  }
}
MultiPlayerMP3 = connect(mapStateToProps, mapDispatchToProps)(MultiPlayerMP3)
Player = connect(mapStateToProps, mapDispatchToProps)(Player)

export default MultiPlayerMP3
