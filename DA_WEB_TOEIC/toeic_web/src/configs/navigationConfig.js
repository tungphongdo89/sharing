import React from "react"
import * as Icon from "react-feather"
import {ROLE_ADMIN, ROLE_TEACHER} from "../commons/constants/CONSTANTS";

const navigationConfig = [
  {
    id: "dashboard",
    title: "Dashboard",
    type: "collapse",
    icon: <Icon.Home size={20} />,
    badge: "warning",
    badgeText: "2",
    children: [
      {
        id: "analyticsDash",
        title: "Analytics",
        type: "item",
        icon: <Icon.Circle size={12} />,
        permissions: [ROLE_ADMIN, ROLE_TEACHER],
        navLink: "/private/admin"
      },
      {
        id: "eCommerceDash",
        title: "eCommerce",
        type: "item",
        icon: <Icon.Circle size={12} />,
        permissions: [ROLE_ADMIN],
        navLink: "/private/admin/ecommerce-dashboard"
      }
    ]
  },
  {
    id: "management_user",
    title: "Quản lý người dùng",
    type: "item",
    icon: <Icon.User size={20} />,
    permissions: [ROLE_ADMIN, ROLE_TEACHER],
    navLink: "/app/user/list",
  },
  {
    id: "management_topic",
    title: "Quản lý chủ dề",
    type: "item",
    icon: <Icon.Tag size={20} />,
    permissions: [ROLE_ADMIN, ROLE_TEACHER],
    navLink: "/app/topic/list"
  },
  {
    id: "management_catogory",
    title: "Quản lý đề bài",
    type: "item",
    icon: <Icon.Thermometer size={20} />,
    permissions: [ROLE_ADMIN, ROLE_TEACHER],
    navLink: "/app/category/list"
  },
  {
    id: "management_test",
    title: "Quản lý bài thi",
    type: "item",
    icon: <Icon.List size={20} />,
    permissions: [ROLE_ADMIN, ROLE_TEACHER],
    navLink: "/data-list/list-view"
  },
  {
    id: "management_banner",
    title: "Quản lý banner",
    type: "item",
    icon: <Icon.Droplet size={20} />,
    permissions: [ROLE_ADMIN, ROLE_TEACHER],
    navLink: "/colors/colors"
  },
  {
    id: "management_dictionnary",
    title: "Quản lý từ điển",
    type: "item",
    icon: <Icon.Eye size={20} />,
    permissions: [ROLE_ADMIN, ROLE_TEACHER],
    navLink: "/ui-elements/dictionary"
  }
]

export default navigationConfig
