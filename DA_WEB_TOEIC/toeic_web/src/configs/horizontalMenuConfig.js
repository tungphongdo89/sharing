import React from "react"
import * as Icon from "react-feather"
import {ROLE_ADMIN, ROLE_STUDENT, ROLE_TEACHER} from "../commons/constants/CONSTANTS";


const horizontalMenuConfig = [
  {
    id: "dashboard",
    title: "Dashboard",
    type: "dropdown",
    icon: <Icon.Home size={16} />,
    children: [
      {
        id: "analyticsDash",
        title: "Analytics",
        type: "item",
        icon: <Icon.Circle size={10} />,
        navLink: "/private/admin",
        permissions: [ROLE_ADMIN, ROLE_TEACHER]
      },
      {
        id: "eCommerceDash",
        title: "eCommerce",
        type: "item",
        icon: <Icon.Circle size={10} />,
        navLink: "/private/admin/ecommerce-dashboard",
        permissions: [ROLE_ADMIN]
      }
    ]
  },
  {
    id: "apps",
    title: "Apps",
    type: "dropdown",
    icon: <Icon.Grid size={16} />,
    children: [
      {
        id: "email",
        title: "Email",
        type: "item",
        icon: <Icon.Mail size={16} />,
        navLink: "/email/:filter",
        filterBase: "/email/inbox",
        permissions: [ROLE_ADMIN, ROLE_TEACHER]
      },
      {
        id: "chat",
        title: "Chat",
        type: "item",
        icon: <Icon.MessageSquare size={16} />,
        navLink: "/chat",
        permissions: [ROLE_ADMIN, ROLE_TEACHER]
      },
      {
        id: "todo",
        title: "Todo",
        type: "item",
        icon: <Icon.CheckSquare size={16} />,
        navLink: "/todo/:filter",
        filterBase: "/todo/all",
        permissions: [ROLE_ADMIN, ROLE_TEACHER]
      },
      {
        id: "calendar",
        title: "Calendar",
        type: "item",
        icon: <Icon.Calendar size={16} />,
        navLink: "/calendar",
        permissions: [ROLE_ADMIN, ROLE_TEACHER]
      },
      {
        id: "eCommerce",
        title: "Ecommerce",
        type: "dropdown",
        icon: <Icon.ShoppingCart size={16} />,
        children: [
          {
            id: "shop",
            title: "Shop",
            type: "item",
            icon: <Icon.Circle size={10} />,
            navLink: "/ecommerce/shop",
            permissions: [ROLE_ADMIN, ROLE_TEACHER]
          },
          {
            id: "detail",
            title: "Product Detail",
            type: "item",
            icon: <Icon.Circle size={10} />,
            navLink: "/ecommerce/product-detail",
            permissions: [ROLE_ADMIN, ROLE_TEACHER]
          },
          {
            id: "wishList",
            title: "Wish List",
            type: "item",
            icon: <Icon.Circle size={10} />,
            navLink: "/ecommerce/wishlist",
            permissions: [ROLE_ADMIN, ROLE_TEACHER]
          },
          {
            id: "checkout",
            title: "Checkout",
            type: "item",
            icon: <Icon.Circle size={10} />,
            navLink: "/ecommerce/checkout",
            permissions: [ROLE_ADMIN, ROLE_TEACHER]
          }
        ]
      },
      {
        id: "usersApp",
        title: "User",
        type: "dropdown",
        icon: <Icon.User size={16} />,
        children: [
          {
            id: "userList",
            title: "List",
            type: "item",
            icon: <Icon.Circle size={10} />,
            navLink: "/app/user/list",
            permissions: [ROLE_ADMIN, ROLE_TEACHER]
          },
          {
            id: "userView",
            title: "View",
            type: "item",
            icon: <Icon.Circle size={10} />,
            navLink: "/app/user/view",
            permissions: [ROLE_ADMIN, ROLE_TEACHER]
          },
          {
            id: "userEdit",
            title: "Edit",
            type: "item",
            icon: <Icon.Circle size={10} />,
            navLink: "/app/user/edit",
            permissions: [ROLE_ADMIN, ROLE_TEACHER]
          }
        ]
      }
    ]
  },
  {
    id: "uiElements",
    title: "UI Elements",
    type: "dropdown",
    icon: <Icon.Layers size={16} />,
    children: [
      {
        id: "dataView",
        title: "Data List",
        type: "dropdown",
        icon: <Icon.List size={16} />,
        children: [
          {
            id: "listView",
            title: "List View",
            type: "item",
            icon: <Icon.Circle size={10} />,
            permissions: [ROLE_ADMIN, ROLE_TEACHER],
            navLink: "/data-list/list-view"
          },
          {
            id: "thumbView",
            title: "Thumb View",
            type: "item",
            icon: <Icon.Circle size={10} />,
            permissions: [ROLE_ADMIN, ROLE_TEACHER],
            navLink: "/data-list/thumb-view"
          }
        ]
      },
      {
        id: "content",
        title: "Content",
        type: "dropdown",
        icon: <Icon.Layout size={16} />,
        children: [
          {
            id: "gird",
            title: "Grid",
            type: "item",
            icon: <Icon.Circle size={10} />,
            navLink: "/ui-element/grid",
            permissions: [ROLE_ADMIN, ROLE_TEACHER]
          },
          {
            id: "typography",
            title: "Typography",
            type: "item",
            icon: <Icon.Circle size={10} />,
            navLink: "/ui-element/typography",
            permissions: [ROLE_ADMIN, ROLE_TEACHER]
          },
          {
            id: "textUitlities",
            title: "Text Utilities",
            type: "item",
            icon: <Icon.Circle size={10} />,
            navLink: "/ui-element/textutilities",
            permissions: [ROLE_ADMIN, ROLE_TEACHER]
          },
          {
            id: "syntaxHighlighter",
            title: "Syntax Highlighter",
            type: "item",
            icon: <Icon.Circle size={10} />,
            navLink: "/ui-element/syntaxhighlighter",
            permissions: [ROLE_ADMIN, ROLE_TEACHER]
          }
        ]
      },
      {
        id: "colors",
        title: "Colors",
        type: "item",
        icon: <Icon.Droplet size={16} />,
        navLink: "/colors/colors",
        permissions: [ROLE_ADMIN, ROLE_TEACHER]
      },
      {
        id: "icons",
        title: "Icons",
        type: "item",
        icon: <Icon.Eye size={16} />,
        navLink: "/vocabulary/listlistVocabulary",
        permissions: [ROLE_ADMIN, ROLE_TEACHER]
      },
      {
        id: "cards",
        title: "Cards",
        type: "dropdown",
        icon: <Icon.CreditCard size={16} />,
        children: [
          {
            id: "basic",
            title: "Basic",
            type: "item",
            icon: <Icon.Circle size={10} />,
            navLink: "/cards/basic",
            permissions: [ROLE_ADMIN, ROLE_TEACHER]
          },
          {
            id: "statistics",
            title: "Statistics",
            type: "item",
            icon: <Icon.Circle size={10} />,
            navLink: "/cards/statistics",
            permissions: [ROLE_ADMIN, ROLE_TEACHER]
          },
          {
            id: "analytics",
            title: "Analytics",
            type: "item",
            icon: <Icon.Circle size={10} />,
            navLink: "/cards/analytics",
            permissions: [ROLE_ADMIN, ROLE_TEACHER]
          },
          {
            id: "cardActions",
            title: "Card Actions",
            type: "item",
            icon: <Icon.Circle size={10} />,
            navLink: "/cards/action",
            permissions: [ROLE_ADMIN, ROLE_TEACHER]
          }
        ]
      },
      {
        id: "components",
        title: "Components",
        type: "dropdown",
        icon: <Icon.Briefcase size={16} />,
        children: [
          {
            id: "alerts",
            title: "Alerts",
            type: "item",
            icon: <Icon.Circle size={10} />,
            navLink: "/components/alerts",
            permissions: [ROLE_ADMIN, ROLE_TEACHER]
          },
          {
            id: "buttons",
            title: "Buttons",
            type: "item",
            icon: <Icon.Circle size={10} />,
            navLink: "/components/buttons",
            permissions: [ROLE_ADMIN, ROLE_TEACHER]
          },
          {
            id: "breadCrumbs",
            title: "Breadcrumbs",
            type: "item",
            icon: <Icon.Circle size={10} />,
            navLink: "/components/breadcrumbs",
            permissions: [ROLE_ADMIN, ROLE_TEACHER]
          },
          {
            id: "carousel",
            title: "Carousel",
            type: "item",
            icon: <Icon.Circle size={10} />,
            navLink: "/components/carousel",
            permissions: [ROLE_ADMIN, ROLE_TEACHER]
          },
          {
            id: "dropDowns",
            title: "Dropdowns",
            type: "item",
            icon: <Icon.Circle size={10} />,
            navLink: "/components/dropdowns",
            permissions: [ROLE_ADMIN, ROLE_TEACHER]
          },
          {
            id: "listGroup",
            title: "List Group",
            type: "item",
            icon: <Icon.Circle size={10} />,
            navLink: "/components/list-group",
            permissions: [ROLE_ADMIN, ROLE_TEACHER]
          },
          {
            id: "modals",
            title: "Modals",
            type: "item",
            icon: <Icon.Circle size={10} />,
            navLink: "/components/modals",
            permissions: [ROLE_ADMIN, ROLE_TEACHER]
          },
          {
            id: "pagination",
            title: "Pagination",
            type: "item",
            icon: <Icon.Circle size={10} />,
            navLink: "/components/pagination",
            permissions: [ROLE_ADMIN, ROLE_TEACHER]
          },
          {
            id: "navsComponent",
            title: "Navs Component",
            type: "item",
            icon: <Icon.Circle size={10} />,
            navLink: "/components/nav-component",
            permissions: [ROLE_ADMIN, ROLE_TEACHER]
          },
          {
            id: "navbar",
            title: "Navbar",
            type: "item",
            icon: <Icon.Circle size={10} />,
            navLink: "/components/navbar",
            permissions: [ROLE_ADMIN, ROLE_TEACHER]
          },
          {
            id: "tabsComponent",
            title: "Tabs Component",
            type: "item",
            icon: <Icon.Circle size={10} />,
            navLink: "/components/tabs-component",
            permissions: [ROLE_ADMIN, ROLE_TEACHER]
          },
          {
            id: "pillsComponent",
            title: "Pills Component",
            type: "item",
            icon: <Icon.Circle size={10} />,
            navLink: "/components/pills-component",
            permissions: [ROLE_ADMIN, ROLE_TEACHER]
          },
          {
            id: "tooltips",
            title: "Tooltips",
            type: "item",
            icon: <Icon.Circle size={10} />,
            navLink: "/components/tooltips",
            permissions: [ROLE_ADMIN, ROLE_TEACHER]
          },
          {
            id: "popovers",
            title: "Popovers",
            type: "item",
            icon: <Icon.Circle size={10} />,
            navLink: "/components/popovers",
            permissions: [ROLE_ADMIN, ROLE_TEACHER]
          },
          {
            id: "badges",
            title: "Badges",
            type: "item",
            icon: <Icon.Circle size={10} />,
            navLink: "/components/badges",
            permissions: [ROLE_ADMIN, ROLE_TEACHER]
          },
          {
            id: "pillBadges",
            title: "Pill Badges",
            type: "item",
            icon: <Icon.Circle size={10} />,
            navLink: "/components/pill-badges",
            permissions: [ROLE_ADMIN, ROLE_TEACHER]
          },
          {
            id: "progress",
            title: "Progress",
            type: "item",
            icon: <Icon.Circle size={10} />,
            navLink: "/components/progress",
            permissions: [ROLE_ADMIN, ROLE_TEACHER]
          },
          {
            id: "mediaObjects",
            title: "Media Objects",
            type: "item",
            icon: <Icon.Circle size={10} />,
            navLink: "/components/media-objects",
            permissions: [ROLE_ADMIN, ROLE_TEACHER]
          },
          {
            id: "spinners",
            title: "Spinners",
            type: "item",
            icon: <Icon.Circle size={10} />,
            navLink: "/components/spinners",
            permissions: [ROLE_ADMIN, ROLE_TEACHER]
          },
          {
            id: "toasts",
            title: "Toasts",
            type: "item",
            icon: <Icon.Circle size={10} />,
            navLink: "/components/toasts",
            permissions: [ROLE_ADMIN, ROLE_TEACHER]
          }
        ]
      },
      {
        id: "extraComponents",
        title: "Extra Components",
        type: "dropdown",
        icon: <Icon.Box size={16} />,
        children: [
          {
            id: "autoComplete",
            title: "Auto Complete",
            type: "item",
            icon: <Icon.Circle size={10} />,
            navLink: "/extra-components/auto-complete",
            permissions: [ROLE_ADMIN, ROLE_TEACHER]
          },
          {
            id: "avatar",
            title: "Avatar",
            type: "item",
            icon: <Icon.Circle size={10} />,
            navLink: "/extra-components/avatar",
            permissions: [ROLE_ADMIN, ROLE_TEACHER]
          },
          {
            id: "chips",
            title: "Chips",
            type: "item",
            icon: <Icon.Circle size={10} />,
            navLink: "/extra-components/chips",
            permissions: [ROLE_ADMIN, ROLE_TEACHER]
          },
          {
            id: "divider",
            title: "Divider",
            type: "item",
            icon: <Icon.Circle size={10} />,
            navLink: "/extra-components/divider",
            permissions: [ROLE_ADMIN, ROLE_TEACHER]
          }
        ]
      },
      {
        id: "extensions",
        title: "Extensions",
        type: "dropdown",
        icon: <Icon.PlusCircle size={16} />,
        children: [
          {
            id: "sweetAlertExt",
            title: "Sweet Alerts",
            icon: <Icon.AlertCircle size={16} />,
            type: "item",
            navLink: "/extensions/sweet-alert",
            permissions: [ROLE_ADMIN, ROLE_TEACHER]
          },
          {
            id: "toastrExt",
            title: "Toastr",
            icon: <Icon.Zap size={16} />,
            type: "item",
            navLink: "/extensions/toastr",
            permissions: [ROLE_ADMIN, ROLE_TEACHER]
          },
          {
            id: "rcSlider",
            title: "Rc Slider",
            icon: <Icon.Sliders size={16} />,
            type: "item",
            navLink: "/extensions/slider",
            permissions: [ROLE_ADMIN, ROLE_TEACHER]
          },
          {
            id: "fileUploader",
            title: "File Uploader",
            icon: <Icon.UploadCloud size={16} />,
            type: "item",
            navLink: "/extensions/file-uploader",
            permissions: [ROLE_ADMIN, ROLE_TEACHER]
          },
          {
            id: "wysiwygEditor",
            title: "Wysiwyg Editor",
            icon: <Icon.Edit size={16} />,
            type: "item",
            navLink: "/extensions/wysiwyg-editor",
            permissions: [ROLE_ADMIN, ROLE_TEACHER]
          },
          {
            id: "drag_&_drop",
            title: "Drag & Drop",
            icon: <Icon.Droplet size={16} />,
            type: "item",
            navLink: "/extensions/drag-and-drop",
            permissions: [ROLE_ADMIN, ROLE_TEACHER]
          },
          {
            id: "tour",
            title: "Tour",
            icon: <Icon.Info size={16} />,
            type: "item",
            navLink: "/extensions/tour",
            permissions: [ROLE_ADMIN, ROLE_TEACHER]
          },
          {
            id: "clipBoard",
            title: "Clipboard",
            icon: <Icon.Copy size={16} />,
            type: "item",
            navLink: "/extensions/clipboard",
            permissions: [ROLE_ADMIN, ROLE_TEACHER]
          },
          {
            id: "contextMenu",
            title: "Context Menu",
            icon: <Icon.Menu size={16} />,
            type: "item",
            navLink: "/extensions/context-menu",
            permissions: [ROLE_ADMIN, ROLE_TEACHER]
          },
          {
            id: "swiper",
            title: "Swiper",
            icon: <Icon.Smartphone size={16} />,
            type: "item",
            navLink: "/extensions/swiper",
            permissions: [ROLE_ADMIN, ROLE_TEACHER]
          },
          {
            id: "access-control",
            title: "Access Control",
            icon: <Icon.Lock size={20} />,
            type: "item",
            navLink: "/extensions/access-control",
            permissions: [ROLE_ADMIN, ROLE_TEACHER]
          },
          {
            id: "i18n",
            title: "I18n",
            icon: <Icon.Globe size={16} />,
            type: "item",
            navLink: "/extensions/i18n",
            permissions: [ROLE_ADMIN, ROLE_TEACHER]
          },
          {
            id: "treeView",
            title: "Tree",
            icon: <Icon.GitPullRequest size={16} />,
            type: "item",
            navLink: "/extensions/tree",
            permissions: [ROLE_ADMIN, ROLE_TEACHER]
          },
          {
            id: "extPagination",
            title: "Pagination",
            icon: <Icon.MoreHorizontal size={16} />,
            type: "item",
            navLink: "/extensions/pagination",
            permissions: [ROLE_ADMIN, ROLE_TEACHER]
          },
          {
            id: "extImport",
            title: "Import",
            icon: <Icon.DownloadCloud size={16} />,
            type: "item",
            navLink: "/extensions/import",
            permissions: [ROLE_ADMIN, ROLE_TEACHER]
          },
          {
            id: "extExport",
            title: "Export",
            icon: <Icon.UploadCloud size={16} />,
            type: "item",
            navLink: "/extensions/export",
            permissions: [ROLE_ADMIN, ROLE_TEACHER]
          },
          {
            id: "extExportSelected",
            title: "Export Selected",
            icon: <Icon.CheckSquare size={16} />,
            type: "item",
            navLink: "/extensions/export-selected",
            permissions: [ROLE_ADMIN, ROLE_TEACHER]
          }
        ]
      }
    ]
  },
  {
    id: "forms-tables",
    title: "Forms & Tables",
    type: "dropdown",
    icon: <Icon.Edit3 size={16} />,
    children: [
      {
        id: "formElements",
        title: "Form Elements",
        type: "dropdown",
        icon: <Icon.Copy size={16} />,
        children: [
          {
            id: "select",
            title: "Select",
            type: "item",
            icon: <Icon.Circle size={10} />,
            navLink: "/forms/elements/select",
            permissions: [ROLE_ADMIN, ROLE_TEACHER]
          },
          {
            id: "switch",
            title: "Switch",
            type: "item",
            icon: <Icon.Circle size={10} />,
            navLink: "/forms/elements/switch",
            permissions: [ROLE_ADMIN, ROLE_TEACHER]
          },
          {
            id: "checkbox",
            title: "Checkbox",
            type: "item",
            icon: <Icon.Circle size={10} />,
            navLink: "/forms/elements/checkbox",
            permissions: [ROLE_ADMIN, ROLE_TEACHER]
          },
          {
            id: "radio",
            title: "Radio",
            type: "item",
            icon: <Icon.Circle size={10} />,
            navLink: "/forms/elements/radio",
            permissions: [ROLE_ADMIN, ROLE_TEACHER]
          },
          {
            id: "input",
            title: "Input",
            type: "item",
            icon: <Icon.Circle size={10} />,
            navLink: "/forms/elements/input",
            permissions: [ROLE_ADMIN, ROLE_TEACHER]
          },
          {
            id: "inputGroup",
            title: "Input Group",
            type: "item",
            icon: <Icon.Circle size={10} />,
            navLink: "/forms/elements/input-group",
            permissions: [ROLE_ADMIN, ROLE_TEACHER]
          },
          {
            id: "numberInput",
            title: "Number Input",
            type: "item",
            icon: <Icon.Circle size={10} />,
            navLink: "/forms/elements/number-input",
            permissions: [ROLE_ADMIN, ROLE_TEACHER]
          },
          {
            id: "textarea",
            title: "Textarea",
            type: "item",
            icon: <Icon.Circle size={10} />,
            navLink: "/forms/elements/textarea",
            permissions: [ROLE_ADMIN, ROLE_TEACHER]
          },
          {
            id: "date_&_timePicker",
            title: "Date & Time Picker",
            type: "item",
            icon: <Icon.Circle size={10} />,
            navLink: "/forms/elements/pickers",
            permissions: [ROLE_ADMIN, ROLE_TEACHER]
          },
          {
            id: "inputMask",
            title: "Input Mask",
            type: "item",
            icon: <Icon.Circle size={10} />,
            navLink: "/forms/elements/input-mask",
            permissions: [ROLE_ADMIN, ROLE_TEACHER]
          }
        ]
      },
      {
        id: "formLayouts",
        title: "Form Layouts",
        type: "item",
        icon: <Icon.Box size={16} />,
        navLink: "/forms/layout/form-layout",
        permissions: [ROLE_ADMIN, ROLE_TEACHER]
      },
      {
        id: "wizard",
        title: "Form Wizard",
        type: "item",
        icon: <Icon.MoreHorizontal size={16} />,
        navLink: "/forms/wizard",
        permissions: [ROLE_ADMIN, ROLE_TEACHER]
      },
      {
        id: "formik",
        title: "Formik",
        type: "item",
        icon: <Icon.CheckCircle size={16} />,
        navLink: "/forms/formik",
        permissions: [ROLE_ADMIN, ROLE_TEACHER]
      },
      {
        id: "tables",
        title: "Tables",
        type: "dropdown",
        icon: <Icon.Server size={16} />,
        children: [
          {
            id: "tablesReactstrap",
            title: "Reactstrap Tables",
            type: "item",
            icon: <Icon.Circle size={10} />,
            navLink: "/tables/reactstrap",
            permissions: [ROLE_ADMIN, ROLE_TEACHER]
          },
          {
            id: "reactTables",
            title: "React Tables",
            type: "item",
            icon: <Icon.Circle size={10} />,
            navLink: "/tables/react-tables",
            permissions: [ROLE_ADMIN, ROLE_TEACHER]
          },
          {
            id: "aggrid",
            title: "agGrid Table",
            type: "item",
            icon: <Icon.Circle size={10} />,
            navLink: "/tables/agGrid",
            permissions: [ROLE_ADMIN, ROLE_TEACHER]
          },
          {
            id: "dataTable",
            title: "DataTables",
            type: "item",
            icon: <Icon.Circle size={12} />,
            permissions: [ROLE_ADMIN, ROLE_TEACHER],
            navLink: "/tables/data-tables"
          }
        ]
      }
    ]
  },
  {
    id: "pages",
    title: "Pages",
    type: "dropdown",
    icon: <Icon.File size={16} />,
    children: [
      {
        id: "profile",
        title: "Profile",
        type: "item",
        icon: <Icon.User size={16} />,
        navLink: "/pages/profile",
        permissions: [ROLE_ADMIN, ROLE_TEACHER]
      },
      {
        id: "accountSettings",
        title: "Account Settings",
        type: "item",
        icon: <Icon.Settings size={16} />,
        navLink: "/pages/account-settings",
        permissions: [ROLE_ADMIN, ROLE_TEACHER]
      },
      {
        id: "faq",
        title: "FAQ",
        type: "item",
        icon: <Icon.HelpCircle size={16} />,
        navLink: "/pages/faq",
        permissions: [ROLE_ADMIN, ROLE_TEACHER]
      },
      {
        id: "knowledgeBase",
        title: "Knowledge Base",
        type: "item",
        icon: <Icon.Info size={16} />,
        navLink: "/pages/knowledge-base",
        permissions: [ROLE_ADMIN, ROLE_TEACHER],
        parentOf: [
          "/pages/knowledge-base/category/questions",
          "/pages/knowledge-base/category"
        ]
      },
      {
        id: "search",
        title: "Search",
        type: "item",
        icon: <Icon.Search size={16} />,
        navLink: "/pages/search",
        permissions: [ROLE_ADMIN, ROLE_TEACHER]
      },
      {
        id: "invoice",
        title: "Invoice",
        type: "item",
        icon: <Icon.File size={16} />,
        navLink: "/pages/invoice",
        permissions: [ROLE_ADMIN, ROLE_TEACHER]
      },
      {
        id: "authentication",
        title: "Authentication",
        type: "dropdown",
        icon: <Icon.Unlock size={16} />,
        children: [
          {
            id: "login",
            title: "Login",
            type: "item",
            icon: <Icon.Circle size={10} />,
            navLink: "/pages/login",
            permissions: [ROLE_ADMIN, ROLE_TEACHER]
          },
          {
            id: "register",
            title: "Register",
            type: "item",
            icon: <Icon.Circle size={10} />,
            navLink: "/pages/register",
            permissions: [ROLE_ADMIN, ROLE_TEACHER]
          },
          {
            id: "forgotPassword",
            title: "Forgot Password",
            type: "item",
            icon: <Icon.Circle size={10} />,
            navLink: "/pages/forgot-password",
            permissions: [ROLE_ADMIN, ROLE_TEACHER]
          },
          {
            id: "resetPassword",
            title: "Reset Password",
            type: "item",
            icon: <Icon.Circle size={10} />,
            navLink: "/pages/reset-password",
            permissions: [ROLE_ADMIN, ROLE_TEACHER]
          },
          {
            id: "lockScreen",
            title: "Lock Screen",
            type: "item",
            icon: <Icon.Circle size={10} />,
            navLink: "/pages/lock-screen",
            permissions: [ROLE_ADMIN, ROLE_TEACHER]
          }
        ]
      },
      {
        id: "miscellaneous",
        title: "Miscellaneous",
        type: "dropdown",
        icon: <Icon.FileText size={16} />,
        children: [
          {
            id: "comingSoon",
            title: "Coming Soon",
            type: "item",
            icon: <Icon.Circle size={10} />,
            navLink: "/misc/coming-soon",
            permissions: [ROLE_ADMIN, ROLE_TEACHER]
          },
          {
            id: "error",
            title: "Error",
            type: "dropdown",
            icon: <Icon.Circle size={10} />,
            children: [
              {
                id: "404",
                title: "404",
                type: "item",
                icon: <Icon.Circle size={10} />,
                navLink: "/public/misc/error/404",
                permissions: [ROLE_ADMIN, ROLE_TEACHER]
              },
              {
                id: "500",
                title: "500",
                type: "item",
                icon: <Icon.Circle size={10} />,
                navLink: "/public/misc/error/500",
                permissions: [ROLE_ADMIN, ROLE_TEACHER]
              }
            ]
          },
          {
            id: "notAuthorized",
            title: "Not Authorized",
            type: "item",
            icon: <Icon.Circle size={10} />,
            navLink: "/public/misc/not-authorized",
            permissions: [ROLE_ADMIN, ROLE_TEACHER]
          },
          {
            id: "maintenance",
            title: "Maintenance",
            type: "item",
            icon: <Icon.Circle size={10} />,
            navLink: "/public/misc/maintenance",
            permissions: [ROLE_ADMIN, ROLE_TEACHER]
          }
        ]
      }
    ]
  },
  {
    id: "charts-maps",
    title: "Charts & Maps",
    type: "dropdown",
    icon: <Icon.BarChart2 size={16} />,
    children: [
      {
        id: "charts",
        title: "Charts",
        type: "dropdown",
        badge: "success",
        badgeText: "3",
        icon: <Icon.PieChart size={16} />,
        children: [
          {
            id: "apex",
            title: "Apex",
            type: "item",
            icon: <Icon.Circle size={10} />,
            navLink: "/charts/apex",
            permissions: [ROLE_ADMIN, ROLE_TEACHER]
          },
          {
            id: "chartJs",
            title: "ChartJS",
            type: "item",
            icon: <Icon.Circle size={10} />,
            navLink: "/charts/chartjs",
            permissions: [ROLE_ADMIN, ROLE_TEACHER]
          },
          {
            id: "recharts",
            title: "Recharts",
            type: "item",
            icon: <Icon.Circle size={10} />,
            navLink: "/charts/recharts",
            permissions: [ROLE_ADMIN, ROLE_TEACHER]
          }
        ]
      },
      {
        id: "leafletMaps",
        title: "Leaflet Maps",
        icon: <Icon.Map size={16} />,
        type: "item",
        navLink: "/maps/leaflet",
        permissions: [ROLE_ADMIN, ROLE_TEACHER]
      }
    ]
  },
  {
    id: "others",
    title: "Others",
    type: "dropdown",
    icon: <Icon.MoreHorizontal size={16} />,
    children: [
      {
        id: "menuLevels",
        title: "Menu Levels",
        icon: <Icon.Menu size={16} />,
        type: "dropdown",
        children: [
          {
            id: "secondLevel",
            title: "Second Level",
            icon: <Icon.Circle size={10} />,
            type: "item",
            navlink: "",
            permissions: [ROLE_ADMIN, ROLE_TEACHER]
          },
          {
            id: "secondLevel1",
            title: "Second Level",
            icon: <Icon.Circle size={10} />,
            type: "dropdown",
            children: [
              {
                id: "ThirdLevel",
                title: "Third Level",
                icon: <Icon.Circle size={10} />,
                type: "item",
                navLink: "",
                permissions: [ROLE_ADMIN, ROLE_TEACHER]
              },
              {
                id: "ThirdLevel1",
                title: "Third Level",
                icon: <Icon.Circle size={10} />,
                type: "item",
                navLink: "",
                permissions: [ROLE_ADMIN, ROLE_TEACHER]
              }
            ]
          }
        ]
      },
      {
        id: "disabledMenu",
        title: "Disabled Menu",
        icon: <Icon.EyeOff size={16} />,
        type: "item",
        navLink: "#",
        permissions: [ROLE_ADMIN, ROLE_TEACHER],
        disabled: true
      },
      {
        id: "documentation",
        title: "Documentation",
        icon: <Icon.Folder size={16} />,
        type: "external-link",
        navLink: "google.com",
        permissions: [ROLE_ADMIN, ROLE_TEACHER]
      },
      {
        id: "raiseSupport",
        title: "Raise Support",
        icon: <Icon.LifeBuoy size={16} />,
        type: "external-link",
        newTab: true,
        navLink: "https://pixinvent.ticksy.com/",
        permissions: [ROLE_ADMIN, ROLE_TEACHER]
      }
    ]
  }
]

export default horizontalMenuConfig
