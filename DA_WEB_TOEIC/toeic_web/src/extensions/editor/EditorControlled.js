import React from "react"
import { Card, CardHeader, CardBody, CardTitle } from "reactstrap"
import { EditorState } from "draft-js"
import { Editor } from "react-draft-wysiwyg"
import {stateToHTML} from 'draft-js-export-html';
class EditorControlled extends React.Component {
  state = {
    editorState: EditorState.createEmpty()
  }

  onEditorStateChange = editorState => {
   // console.log(editorState.getCurrentContent().getPlainHTML('\u0001'));
console.log(   stateToHTML(this.state.editorState.getCurrentContent()));
    this.setState({
      editorState
    })
  }

  render() {
    const { editorState } = this.state

    return (
      <Card>
        <CardHeader>
          <CardTitle>Controlled</CardTitle>
        </CardHeader>
        <CardBody>
          <Editor
            editorState={editorState}
            wrapperClassName="demo-wrapper"
            editorClassName="demo-editor"
            onEditorStateChange={this.onEditorStateChange}
          />
        </CardBody>
      </Card>
    )
  }
}

export default EditorControlled
