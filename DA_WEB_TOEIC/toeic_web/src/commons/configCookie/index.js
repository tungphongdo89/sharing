import * as Cookies from "js-cookie";
import {TIME_ALIVE} from "../constants/Config";

export const setSessionCookie = (session) => {
  Cookies.remove("sessionToken");
  // Cookies.set("sessionToken", session, {expires: 14});
  // Cookies.set("sessionToken", session);
  let d = new Date();
  d.setTime(d.getTime() + (TIME_ALIVE * 60 * 1000));

  Cookies.set("sessionToken", session, {path: "/", expires: d});
};

export const getSessionCookie = () => {
  const sessionCookie = Cookies.get("sessionToken");
  if (sessionCookie === undefined) {
    return null;
  } else {
    return sessionCookie;
  }
};
export const removeSession = (session) => {
  Cookies.remove("sessionToken");
};
export const setSessionLanggue = (session) => {
  localStorage.setItem("sessionLanggue", session);
};
export const getSessionLanggue = () => {
  const sessionCookie = localStorage.getItem("sessionLanggue");
  if (sessionCookie === undefined || sessionCookie === null) {
    return "vn";
  } else {
    return sessionCookie;
  }
};

