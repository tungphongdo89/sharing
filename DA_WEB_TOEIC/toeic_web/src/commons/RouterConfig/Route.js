import React, {lazy} from "react"
import {Redirect} from "react-router-dom"
import knowledgeBaseCategory from "../../views/pages/knowledge-base/Category"
import knowledgeBaseQuestion from "../../views/pages/knowledge-base/Questions"
import HomePageOther from "../../components/public/HomePageForOtherUser";

// Route-based code splitting
const analyticsDashboard = lazy(() =>
  import("../../views/dashboard/analytics/AnalyticsDashboard")
)
const ecommerceDashboard = lazy(() =>
  import("../../views/dashboard/ecommerce/EcommerceDashboard")
)

const email = lazy(() => import("../../views/apps/email/Email"))
const gridTopic = lazy(() => import("../../views/apps/user/list/GridTopic"))
const todo = lazy(() => import("../../views/apps/todo/Todo"))
const calendar = lazy(() => import("../../views/apps/user/list/ListPagination"))
const shop = lazy(() => import("../../views/apps/ecommerce/shop/Shop"))
const wishlist = lazy(() => import("../../views/apps/ecommerce/wishlist/Wishlist"))
const checkout = lazy(() => import("../../views/apps/ecommerce/cart/Cart"))
const productDetail = lazy(() => import("../../views/apps/ecommerce/detail/Detail"))
const grid = lazy(() => import("../../views/ui-elements/grid/Grid"))
const typography = lazy(() =>
  import("../../views/ui-elements/typography/Typography")
)
const textutilities = lazy(() =>
  import("../../views/ui-elements/text-utilities/TextUtilities")
)
const syntaxhighlighter = lazy(() =>
  import("../../views/ui-elements/syntax-highlighter/SyntaxHighlighter")
)
const colors = lazy(() => import("../../views/ui-elements/colors/Colors"))
const reactfeather = lazy(() =>
  import("../../views/pages/dictionary/listDictionary")
)
const basicCards = lazy(() => import("../../views/ui-elements/cards/basic/Cards"))
const statisticsCards = lazy(() =>
  import("../../views/ui-elements/cards/statistics/StatisticsCards")
)
const analyticsCards = lazy(() =>
  import("../../views/ui-elements/cards/analytics/Analytics")
)
const actionCards = lazy(() =>
  import("../../views/ui-elements/cards/actions/CardActions")
)
const Alerts = lazy(() => import("../../components/reactstrap/alerts/Alerts"))
const Buttons = lazy(() => import("../../components/reactstrap/buttons/Buttons"))
const Breadcrumbs = lazy(() =>
  import("../../components/reactstrap/breadcrumbs/Breadcrumbs")
)
const Carousel = lazy(() => import("../../components/reactstrap/carousel/Carousel"))
const Collapse = lazy(() => import("../../components/reactstrap/collapse/Collapse"))
const Dropdowns = lazy(() =>
  import("../../components/reactstrap/dropdowns/Dropdown")
)
const ListGroup = lazy(() =>
  import("../../components/reactstrap/listGroup/ListGroup")
)
const Modals = lazy(() => import("../../components/reactstrap/modal/Modal"))

// -------------------Popup edit profile : quản lý tài khoản------------------------
const ModalManagementAccount = lazy(() => import("../../views/pages/account-management/AccountManagement"))

const ModalSizes = lazy(() => import("../../components/reactstrap/modal/ModalSizes"))

const Pagination = lazy(() =>
  import("../../components/reactstrap/pagination/Pagination")
)
const NavComponent = lazy(() =>
  import("../../components/reactstrap/navComponent/NavComponent")
)
const Navbar = lazy(() => import("../../components/reactstrap/navbar/Navbar"))
const Tabs = lazy(() => import("../../components/reactstrap/tabs/Tabs"))
const TabPills = lazy(() => import("../../components/reactstrap/tabPills/TabPills"))
const Tooltips = lazy(() => import("../../components/reactstrap/tooltips/Tooltips"))
const Popovers = lazy(() => import("../../components/reactstrap/popovers/Popovers"))
const Badge = lazy(() => import("../../components/reactstrap/badge/Badge"))
const BadgePill = lazy(() =>
  import("../../components/reactstrap/badgePills/BadgePill")
)
const Progress = lazy(() => import("../../components/reactstrap/progress/Progress"))
const Media = lazy(() => import("../../components/reactstrap/media/MediaObject"))
const Spinners = lazy(() => import("../../components/reactstrap/spinners/Spinners"))
const Toasts = lazy(() => import("../../components/reactstrap/toasts/Toasts"))
const avatar = lazy(() => import("../../components/@vuexy/avatar/Avatar"))
const AutoComplete = lazy(() =>
  import("../../components/@vuexy/autoComplete/AutoComplete")
)
const chips = lazy(() => import("../../components/@vuexy/chips/Chips"))
const divider = lazy(() => import("../../components/@vuexy/divider/Divider"))
const vuexyWizard = lazy(() => import("../../components/@vuexy/wizard/Wizard"))
const listView = lazy(() => import("../../views/ui-elements/data-list/ListView"))
const thumbView = lazy(() => import("../../views/ui-elements/data-list/ThumbView"))
const select = lazy(() => import("../../views/forms/form-elements/select/Select"))
const switchComponent = lazy(() =>
  import("../../views/forms/form-elements/switch/Switch")
)
const checkbox = lazy(() =>
  import("../../views/forms/form-elements/checkboxes/Checkboxes")
)
const radio = lazy(() => import("../../views/forms/form-elements/radio/Radio"))
const input = lazy(() => import("../../views/forms/form-elements/input/Input"))
const group = lazy(() =>
  import("../../views/forms/form-elements/input-groups/InputGoups")
)
const numberInput = lazy(() =>
  import("../../views/forms/form-elements/number-input/NumberInput")
)
const textarea = lazy(() =>
  import("../../views/forms/form-elements/textarea/Textarea")
)
const pickers = lazy(() =>
  import("../../views/forms/form-elements/datepicker/Pickers")
)
const inputMask = lazy(() =>
  import("../../views/forms/form-elements/input-mask/InputMask")
)
const gridCategory = lazy(() => import("../../views/pages/category-management/grid-category/index"));
const girdTest = lazy(() => import("../../views/pages/list-test-management"));
const layout = lazy(() => import("../../views/forms/form-layouts/FormLayouts"))
const formik = lazy(() => import("../../views/forms/formik/Formik"))
const tables = lazy(() => import("../../views/tables/reactstrap/Tables"))
const ReactTables = lazy(() =>
  import("../../views/tables/react-tables/ReactTables")
)
const Aggrid = lazy(() => import("../../views/tables/aggrid/Aggrid"))
const DataTable = lazy(() => import("../../views/tables/data-tables/DataTables"))
const profile = lazy(() => import("../../views/pages/profile/Profile"))
const faq = lazy(() => import("../../views/pages/faq/FAQ"))
const knowledgeBase = lazy(() =>
  import("../../views/pages/knowledge-base/KnowledgeBase")
)
const search = lazy(() => import("../../views/pages/search/Search"))

// -------------------------Account Setting-----------------------------

// const accountSettings = lazy(() =>
//   import("../../views/pages/account-settings/AccountSettings")
// )

const accountSettings = lazy(() =>
  import("../../views/pages/account-setting/AccountSetting")
)

// -------------------------Account Setting-----------------------------

const userDetail = lazy(() => import("../../views/pages/users-management/userDetail"))
const invoice = lazy(() => import("../../views/pages/invoice/Invoice"))
const comingSoon = lazy(() => import("../../views/pages/misc/ComingSoon"))
const error404 = lazy(() => import("../../views/pages/misc/error/404"))
const error500 = lazy(() => import("../../views/pages/misc/error/500"))
const authorized = lazy(() => import("../../views/pages/misc/NotAuthorized"))
const maintenance = lazy(() => import("../../views/pages/misc/Maintenance"))
const apex = lazy(() => import("../../views/charts/apex/ApexCharts"))
const chartjs = lazy(() => import("../../views/charts/chart-js/ChartJS"))
const extreme = lazy(() => import("../../views/charts/recharts/Recharts"))
const leafletMaps = lazy(() => import("../../views/maps/Maps"))
const toastr = lazy(() => import("../../extensions/toastify/Toastify"))
const sweetAlert = lazy(() => import("../../extensions/sweet-alert/SweetAlert"))
const rcSlider = lazy(() => import("../../extensions/rc-slider/Slider"))
const uploader = lazy(() => import("../../extensions/dropzone/Dropzone"))
const editor = lazy(() => import("../../extensions/editor/Editor"))
const drop = lazy(() => import("../../extensions/drag-and-drop/DragAndDrop"))
const tour = lazy(() => import("../../extensions/tour/Tour"))
const clipboard = lazy(() =>
  import("../../extensions/copy-to-clipboard/CopyToClipboard")
)
const menu = lazy(() => import("../../extensions/contexify/Contexify"))
const swiper = lazy(() => import("../../extensions/swiper/Swiper"))
const i18n = lazy(() => import("../../extensions/i18n/I18n"))
const reactPaginate = lazy(() => import("../../extensions/pagination/Pagination"))
const tree = lazy(() => import("../../extensions/treeview/TreeView"))
const Import = lazy(() => import("../../extensions/import-export/Import"))
const Export = lazy(() => import("../../extensions/import-export/Export"))
const ExportSelected = lazy(() =>
  import("../../extensions/import-export/ExportSelected")
)
const userList = lazy(() => import("../../views/apps/user/list/List"))
// const userList = lazy(() => import("../../views/pages/users-management/gridManagement/index"))
const userEdit = lazy(() => import("../../views/apps/user/edit/Edit"))
const userView = lazy(() => import("../../views/apps/user/view/View"))

// const Login = lazy(() => import("../../views/pages/authentication/login/Login"))

// ----------------------UI login mới-----------------------
const Login = lazy(() => import("../../views/pages/authentication/login/LoginNewPage"))
// ----------------------UI login mới-----------------------

// ----------------------UI danh sách chủ đề bài luyện tập-----------------------
const topicExercise = lazy(() => import("../../views/pages/list-topic-exersice/ListTopicExercise"))
// ----------------------UI danh sách chủ đề bài luyện tập-----------------------

const forgotPassword = lazy(() =>
  import("../../views/pages/authentication/forgot-password/ForgotPassword")
)
const lockScreen = lazy(() => import("../../views/pages/authentication/LockScreen"))
const resetPassword = lazy(() =>
  import("../../views/pages/authentication/ResetPassword/ResetPassword")
)
const register = lazy(() =>
  import("../../views/pages/authentication/register/Register")
)
const accessControl = lazy(() =>
  import("../../extensions/access-control/AccessControl")
)
const contactPage = lazy(() => import("../../views/pages/ContactPage"))
const profilePage = lazy(() => import("../../views/pages/profilePage"))
const categoryForm = lazy(() => import("../../views/pages/category-management"))
const practiceListeningMulti = lazy(()=> import("../../views/pages/Practices/PracticeMulti"))

const listeningWordFill = lazy(() => import("../../views/pages/Practices/Listening/ListeningWordFill"))
const readingWordFill = lazy(() => import("../../views/pages/Practices/Reading/ReadingWordFill"))
const TranslationAV = lazy(() => import("../../views/pages/translation-excercise/translation-excercise-av"))
const TranslationVA = lazy(() => import("../../views/pages/translation-excercise/translation-excercise-va"))
const AggregatedResults = lazy(() => import("../../views/pages/Practices/AggregatedResults"))

const ListeningToeicConversation = lazy(() => import("../../views/pages/Practices/ListeningToeicConversationPractice"))
const ReadingCompletedPassage = lazy(()=> import("../../views/pages/Practices/Reading/ReadingCompletedPassage"))
const ListeningToeicShortTalking = lazy(() => import("../../views/pages/Practices/ListeningToeicShortTalkingPractice"))
const PracticeReadingSingleQuestion = lazy(()=>import("../../views/pages/Practices/Reading/single-reading"))

const testForm = lazy(()=> import("../../views/pages/list-test-management/test-management"))
const listBanner = lazy(()=> import("../../views/pages/banner-management/list-banner"))

const ListTests = lazy(()=>import("../../views/pages/doing-test"))
const Minitest = lazy(()=>import("../../views/pages/minitest"))
const FullTest = lazy(()=>import("../../views/pages/fulltest"))


export const ADMIN_ROUTES = [
  {
    path: "/private/admin/",
    exact: true,
    component: analyticsDashboard
  },
  {
    path: "/private/admin/ecommerce-dashboard",
    exact: false,
    component: ecommerceDashboard,

  },
]

export const PUBLIC_ROUTES = [
  {
    path: "/",
    exact: true,
    component: HomePageOther
  },
  {
    path: "/misc/coming-soon",
    exact: false,
    component: comingSoon,
    publicLayout: true
  },
  {
    path: "/public/misc/error/404",
    exact: false,
    component: error404,
    fullLayout: true
  },
  // {
  //   path: "/pages/login",
  //   exact: false,
  //   component: Login,
  //   // fullLayout: true
  // },

  // -----------------------Login New Page-------------------------
  {
    path: "/pages/login",
    exact: false,
    component: Login,
    // fullLayout: true
    // true : 1 page riêng , false : trong page home
  },
  // -----------------------Login New Page-------------------------

  // -----------------------Danh sách chủ đề luyện tập-------------------------
  {
    path: "/pages/topic-exercise",
    exact: false,
    component: topicExercise,
    // fullLayout: true
  },
  // ------------------------Danh sách chủ đề luyện tập-- ------------------------

  {
    path: "/pages/register",
    exact: false,
    component: register,
    // fullLayout: true
  },
  {
    path: "/pages/forgot-password",
    exact: false,
    component: forgotPassword,
    // fullLayout: true
  },
  {
    path: "/pages/lock-screen",
    exact: false,
    component: lockScreen,
    fullLayout: true
  },
  {
    path: "/pages/reset-password",
    exact: false,
    component: resetPassword,
// fullLayout: true
  },
  {
    path: "/public/misc/error/500",
    exact: false,
    component: error500,
    fullLayout: true
  },
  {
    path: "/public/misc/not-authorized",
    exact: false,
    component: authorized,
    fullLayout: true
  },
  {
    path: "/public/misc/maintenance",
    exact: false,
    component: maintenance,
    fullLayout: true
  }, {
    path: "/email",
    exact: false,
    component: () => <Redirect to="/email/inbox"/>
  }, {
    path: "/app/user/edit",
    exact: false,
    component: userEdit
  },
  {
    path: "/email/:filter",
    exact: false,
    component: email
  },
  {
    path: "/app/topic/list",
    exact: false,
    component: gridTopic
  },
  {
    path: "/todo",
    exact: false,
    component: () => <Redirect to="/todo/all"/>
  },
  {
    path: "/todo/:filter",
    exact: false,
    component: todo
  },
  {
    path: "/app/category/list",
    exact: false,
    component: gridCategory
  },
  {
    path: "/app/test/list",
    exact: false,
    component: girdTest
  },
  {
    path: "/ecommerce/shop",
    exact: false,
    component: shop
  },
  {
    path: "/ecommerce/wishlist",
    exact: false,
    component: wishlist
  },
  {
    path: "/ecommerce/product-detail",
    exact: false,
    component: productDetail
  },
  {
    path: "/ecommerce/checkout",
    exact: false,
    component: checkout,
    permission: "ADMIN"
  },
  {
    path: "/data-list/list-view",
    exact: false,
    component: listView
  },
  {
    path: "/data-list/thumb-view",
    exact: false,
    component: thumbView
  },
  {
    path: "/ui-element/grid",
    exact: false,
    component: grid
  },
  {
    path: "/ui-element/typography",
    exact: false,
    component: typography
  },
  {
    path: "/ui-element/textutilities",
    exact: false,
    component: textutilities
  },
  {
    path: "/ui-element/syntaxhighlighter",
    exact: false,
    component: syntaxhighlighter
  },
  {
    path: "/colors/colors",
    exact: false,
    component: colors
  },
  {
    path: "/ui-elements/dictionary",
    exact: false,
    component: reactfeather
  },
  {
    path: "/cards/basic",
    exact: false,
    component: basicCards
  },
  {
    path: "/cards/statistics",
    exact: false,
    component: statisticsCards
  },
  {
    path: "/cards/analytics",
    exact: false,
    component: analyticsCards
  },
  {
    path: "/cards/action",
    exact: false,
    component: actionCards
  },
  {
    path: "/components/alerts",
    exact: false,
    component: Alerts
  },
  {
    path: "/components/buttons",
    exact: false,
    component: Buttons
  },
  {
    path: "/components/breadcrumbs",
    exact: false,
    component: Breadcrumbs
  },
  {
    path: "/components/carousel",
    exact: false,
    component: Carousel
  },
  {
    path: "/components/collapse",
    exact: false,
    component: Collapse
  },
  {
    path: "/components/dropdowns",
    exact: false,
    component: Dropdowns
  },
  {
    path: "/components/list-group",
    exact: false,
    component: ListGroup
  },
  {
    path: "/components/modals",
    exact: false,
    component: Modals
  },

  // ------------------Modal quản lý tài khoản---------------------
  {
    path: "/components/account",
    exact: false,
    component: ModalManagementAccount
  },

  {
    path: "/components/modal-size",
    exact: false,
    component: ModalSizes
  },
  // ------------------Modal quản lý tài khoản---------------------

  {
    path: "/components/pagination",
    exact: false,
    component: Pagination
  },
  {
    path: "/components/nav-component",
    exact: false,
    component: NavComponent
  },
  {
    path: "/components/navbar",
    exact: false,
    component: Navbar
  },
  {
    path: "/components/tabs-component",
    exact: false,
    component: Tabs
  },
  {
    path: "/components/pills-component",
    exact: false,
    component: TabPills
  },
  {
    path: "/components/tooltips",
    exact: false,
    component: Tooltips
  },
  {
    path: "/components/popovers",
    exact: false,
    component: Popovers
  },
  {
    path: "/components/badges",
    exact: false,
    component: Badge
  },
  {
    path: "/components/pill-badges",
    exact: false,
    component: BadgePill
  },
  {
    path: "/components/progress",
    exact: false,
    component: Progress
  },
  {
    path: "/components/media-objects",
    exact: false,
    component: Media
  },
  {
    path: "/components/spinners",
    exact: false,
    component: Spinners
  },
  {
    path: "/components/toasts",
    exact: false,
    component: Toasts
  },
  {
    path: "/extra-components/auto-complete",
    exact: false,
    component: AutoComplete
  },
  {
    path: "/extra-components/avatar",
    exact: false,
    component: avatar
  },
  {
    path: "/extra-components/chips",
    exact: false,
    component: chips
  },
  {
    path: "/extra-components/divider",
    exact: false,
    component: divider
  },
  {
    path: "/forms/wizard",
    exact: false,
    component: vuexyWizard
  },
  {
    path: "/forms/elements/select",
    exact: false,
    component: select
  },
  {
    path: "forms/elements/switch",
    exact: false,
    component: switchComponent
  },
  {
    path: "/forms/elements/checkbox",
    exact: false,
    component: checkbox
  },
  {
    path: "/forms/elements/input",
    exact: false,
    component: input
  },
  {
    path: "/forms/elements/input-group",
    exact: false,
    component: group
  },
  {
    path: "/forms/elements/number-input",
    exact: false,
    component: numberInput
  },
  {
    path: "/forms/elements/textarea",
    exact: false,
    component: textarea
  },
  {
    path: "/forms/elements/pickers",
    exact: false,
    component: pickers
  },
  {
    path: "/forms/elements/input-mask",
    exact: false,
    component: inputMask
  },
  {
    path: "/forms/layout/form-layout",
    exact: false,
    component: layout
  },
  {
    path: "/forms/formik",
    exact: false,
    component: formik
  },
  {
    path: "/tables/reactstrap",
    exact: false,
    component: tables
  },
  {
    path: "/tables/react-tables",
    exact: false,
    component: ReactTables
  },
  {
    path: "tables/agGrid",
    exact: false,
    component: Aggrid
  },
  {
    path: "/tables/data-tables",
    exact: false,
    component: DataTable
  },
  {
    path: "/pages/profile",
    exact: false,
    component: profile
  },
  {
    path: "/pages/faq",
    exact: false,
    component: faq
  },
  {
    path: "/pages/knowledge-base",
    exact: true,
    component: knowledgeBase
  },
  {
    path: "/pages/knowledge-base/category",
    exact: true,
    component: knowledgeBaseCategory
  },
  {
    path: "/pages/knowledge-base/category/questions",
    exact: false,
    component: knowledgeBaseQuestion
  },
  {
    path: "/pages/search",
    exact: false,
    component: search
  },
  {
    path: "/account-settings",
    exact: false,
    component: accountSettings,
    fullLayout: true
  },
  {
    path: "/pages/users/userDetail",
    exact: false,
    component: userDetail
  },
  {
    path: "/pages/invoice",
    exact: false,
    component: invoice
  },
  {
    path: "/app/user/list",
    exact: false,
    component: userList
  }

  , {
    path: "/app/user/view",
    exact: false,
    component: userEdit
  },
  {
    path: "/charts/apex",
    exact: false,
    component: apex
  },
  {
    path: "/charts/chartjs",
    exact: false,
    component: chartjs
  },
  {
    path: "/charts/recharts",
    exact: false,
    component: extreme
  },
  {
    path: "/maps/leaflet",
    exact: false,
    component: leafletMaps
  },
  {
    path: "/extensions/sweet-alert",
    exact: false,
    component: sweetAlert
  },
  {
    path: "/extensions/toastr",
    exact: false,
    component: toastr
  },
  {
    path: "/extensions/slider",
    exact: false,
    component: rcSlider
  },
  {
    path: "/extensions/file-uploader",
    exact: false,
    component: uploader
  },
  {
    path: "/extensions/wysiwyg-editor",
    exact: false,
    component: editor
  },
  {
    path: "/extensions/drag-and-drop",
    exact: false,
    component: drop
  },
  {
    path: "/extensions/tour",
    exact: false,
    component: tour
  },
  {
    path: "/extensions/clipboard",
    exact: false,
    component: clipboard
  },
  {
    path: "/extensions/context-menu",
    exact: false,
    component: menu
  },
  {
    path: "/extensions/swiper",
    exact: false,
    component: swiper
  },
  {
    path: "/extensions/access-control",
    exact: false,
    component: accessControl
  },
  {
    path: "/extensions/i18n",
    exact: false,
    component: i18n
  },
  {
    path: "/extensions/tree",
    exact: false,
    component: tree
  },
  {
    path: "/extensions/import",
    exact: false,
    component: Import
  },
  {
    path: "/extensions/export",
    exact: false,
    component: Export
  },
  {
    path: "/extensions/export-selected",
    exact: false,
    component: ExportSelected
  },
  {
    path: "/extensions/pagination",
    exact: false,
    component: reactPaginate
  },
  {
    path: "/contact",
    exact: false,
    component: contactPage
  },
  {
    path: "/pages/profiles",
    exact: false,
    component: profilePage
  },
  {
    path:"/pages/category-form",
    exact:false,
    component: categoryForm
  },
  {
    path: "/pages/listeningWordFill",
    exact: false,
    component: listeningWordFill
  },
  {
    path: "/pages/readingWordFill",
    exact: false,
    component: readingWordFill
  },
  {
	path: "/pages/translation-av",
	exact: false,
	component: TranslationAV
  },
  {
	path: "/pages/translation-va",
	exact: false,
	component: TranslationVA
  },
  {
    path:"/pages/practice-listening-painting",
    exact: false,
    component: practiceListeningMulti
  },
  {
    path:"/pages/practice-listening-question-answer",
    exact: false,
    component: practiceListeningMulti
  },
  {
    path: "/pages/practice-listening-toeic-conversation",
    exact: false,
    component: ListeningToeicConversation
  },
  {
    path: "/pages/practice-listening-toeic-short-talking",
    exact: false,
    component: ListeningToeicShortTalking
  },
  {
    path:"/pages/aggregatedResults",
    exact: false,
    component: AggregatedResults
  },
  {
    path:"/pages/practice-reading-single",
    exact: false,
    component: PracticeReadingSingleQuestion
  },
  {
    path:"/pages/practice-reading-completed-passage",
    exact: false,
    component: ReadingCompletedPassage
  },
  {
    path:"/pages/test-form",
    exact: false,
    component: testForm
  },
  {
    path:"/pages/practice-reading-dual",
    exact: false,
    component: PracticeReadingSingleQuestion
  },
  {
    path:"/pages/list-banner",
    exact: false,
    component: listBanner
  },
  {
    path:"/pages/doing-test",
    exact: false,
    component: ListTests
  },
  {
    path:"/pages/minitest",
    exact: false,
    component: Minitest
  },
  {
    path:"/pages/fulltest",
    exact: false,
    component: FullTest
  }
]
