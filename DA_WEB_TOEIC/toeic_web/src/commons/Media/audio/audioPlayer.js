import React, {Component} from 'react';
import ReactPlayer from 'react-player'

class AudioPlayer extends Component {

  ref = player => {
    this.player = player
  }
  componentWillUpdate(nextProps, nextState, nextContext) {
    const {timeAgain} = nextProps;
    this.player.seekTo(timeAgain, 'seconds')
  }
  render() {
    const {fileAudio} = this.props;
    return (
      <div style={{marginLeft: '30%'}} >
        <div>
          <ReactPlayer
            ref={this.ref}
            url={fileAudio}
            playing={true}
            controls={true}
            config={{
              file: {
                attributes: {
                  controlsList: 'nodownload',
                }
              }
            }}
            width="300px"
            height="2em"
          />
        </div>
      </div>
    );
  }
}
export default AudioPlayer;
