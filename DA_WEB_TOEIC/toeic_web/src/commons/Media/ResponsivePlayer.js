import React, {Component} from 'react';
import PropTypes from 'prop-types';
import ReactPlayer from 'react-player/youtube';
import {Button, withStyles} from '@material-ui/core'
import styles from "./styles"


class ResponsivePlayer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      url: null,
      pip: false,
      playing: false,
      controls: true,
      light: false,
      volume: 0.8,
      muted: false,
      played: 0,
      loaded: 0,
      duration: 0,
      playbackRate: 1.0,
      loop: false
    }
  }

  load = url => {
    this.setState({
      url,
      played: 0,
      loaded: 0,
      pip: false
    })
  }


  handleSeekMouseDown = e => {
    debugger
    this.setState({seeking: true})
  }

  handleSeekChange = e => {
    debugger
    this.setState({played: parseFloat(e.target.value)})
  }

  handleSeekMouseUp = (e, value) => {
    debugger
    this.setState({seeking: false})
    this.player.seekTo(value, 'seconds')
  }

  handleClick = (e, value) => {
    debugger
    this.handleSeekMouseUp(e, value)
    this.handleSeekChange(e, value)
    this.handleSeekMouseDown(e, value)
  }

  ref = player => {
    this.player = player
  }

  render() {
    const {classes} = this.props;
    const {seeking, played, playing, controls} = this.state;
    return (
      <div>
        <div className={classes.playerWrapper}>
          <ReactPlayer
            ref={this.ref}
            className={classes.reactPlayer}
            controls={controls}
            url={['https://www.youtube.com/watch?time_continue=7&v=BFZtNN6eNvQ&ab_channel=TED']}
            width='100%'
            height='100%'
            onSeek={e => console.log('onSeek', e)}
            playing={playing}

          />
        </div>
        {/*<div style={{cursor: "pointer", marginTop: "2%"}}>*/}
          {/*<Button type="button" onClick={(e) => this.handleClick(e, 90)} variant="contained" color="secondary"*/}
                  {/*style={{cursor: "pointer", marginRight: "3px"}}*/}
                  {/*value={90}>1:30</Button>*/}
          {/*<Button onClick={(e) => this.handleClick(e, 120)} variant="contained"*/}
                  {/*style={{cursor: "pointer", marginRight: "3px"}}*/}
                  {/*color="secondary" value={120}>2:00</Button>*/}
          {/*<Button onClick={(e) => this.handleClick(e, 180)} variant="contained"*/}
                  {/*style={{cursor: "pointer", marginRight: "3px"}}*/}
                  {/*color="secondary" value={180}>3:00</Button>*/}
        {/*</div>*/}

      </div>
    );
  }
}

ResponsivePlayer.propTypes = {
  classes: PropTypes.object
};

export default withStyles(styles)(ResponsivePlayer);
