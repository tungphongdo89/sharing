const styles = ({
  playerWrapper: {
    position: 'relative',
    paddingTop: "56.25%"
  },
  reactPlayer: {
    position: 'absolute',
    top: 0,
    left: 0,
  }
})
export default styles;
