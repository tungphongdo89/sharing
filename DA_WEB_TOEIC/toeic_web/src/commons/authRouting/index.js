import {RouteComponentProps} from 'react-router-dom';

interface
Props
{
  Component: React.FC < RouteComponentProps >;
  path: string;
  exact ? : boolean;
  requiredRoles: string[];
}
const AuthRoute = ({Component, path, exact = false, requiredRoles}: Props): JSX.Element => {
  const isAuthed = !!localStorage.getItem(ACCESS_TOKEN);
  const {userRole}: useContext(UserRoleContext);
  const userHasRequiredRole = requiredRoles.includes(userRole);
  const message = userHasRequiredRole ? 'Please log in to view this page' : "You can't be here!"
  return (
    <Route
      exact={exact}
      path={path}
      render={(props: RouteComponentProps) =>
        isAuthed && userHasRequiredRole ? (
          <Component {...props} />
        ) : (
          <Redirect
            to={{
              pathname: userHasRequiredRole ?
                NonAuthRoutes.signin :
                NonAuthRoutes.unauthorized,
              state: {
                message,
                requestedPath: path
              }
            }}
          />
        )
      }
    />
  );
};

export default AuthRoute
