import {getSessionLanggue} from "../configCookie";
import vn from "../../assets/data/locales/vn.json";
import en from "../../assets/data/locales/en.json";

export function showMessage(key) {
  let url = {}
  let message = ""
  if (key === "admin") {
    debugger
  }
  if (getSessionLanggue() === "vn") {
    url = vn;
    message = url[key];
    if (message === undefined || message.length === 0) {
      return "Không có thông báo!"
    }
  } else {
    url = en;
    message = url[key];
    if (message === undefined || message.length === 0) {
      return "No notification content!"
    }
  }
  return message
}

export function showMessageVn(key) {
  let url = vn;
  let message = url[key];
  if (message === undefined || message.length === 0) {
    return "Không có thông báo!"
  }
  return message
}
