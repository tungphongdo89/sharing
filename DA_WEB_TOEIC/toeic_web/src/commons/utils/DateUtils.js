// var dateObj = new Date('2020-08-06T17:00:00.000+0000');
export function getUTCTime(dateObj){
  var month = dateObj.getMonth() + 1; //months from 1-12
  var day = dateObj.getDate();
  var year = dateObj.getFullYear();
  var hours = dateObj.getHours();
  var minutes = dateObj.getMinutes();
  var seconds = dateObj.getSeconds();
  if(day<10){day='0'+day}
  if(month<10){month='0'+month}
  if(hours<10){hours='0'+hours}
  if(minutes<10){minutes='0'+minutes}

  return   day + "/" + month + "/" + year + " " + hours + ":" + minutes;
}

export function getUTCTimeSeconds(dateObj){
  var month = dateObj.getUTCMonth() + 1; //months from 1-12
  var day = dateObj.getDate();
  var year = dateObj.getUTCFullYear();
  var hours = dateObj.getHours();
  var minutes = dateObj.getUTCMinutes();
  var seconds = dateObj.getUTCSeconds();
  if(day<10){day='0'+day}
  if(month<10){month='0'+month}
  if(hours<10){hours='0'+hours}
  if(minutes<10){minutes='0'+minutes}
  if(seconds<10){seconds='0'+seconds}

  return   day + "/" + month + "/" + year + " " + hours + ":" + minutes + ":" + seconds;
}

export function getUTCTimeSecondsForHistoryPractices(dateObj){

  var month = dateObj.getUTCMonth() + 1;
  var day = dateObj.getUTCDate();
  var year = dateObj.getUTCFullYear();
  var hours = dateObj.getUTCHours();

  if(hours >= 24){
    hours = hours - 24;
    day = day + 1;
  }

  if(month === 1 || month === 3 || month === 5 || month === 7 || month === 8 || month === 10 || month === 12){
    if(day > 31){
      day = day - 31;
      month = month + 1;
    }
  }
  if(month === 4 || month === 6 || month === 9 || month === 11){
    if(day > 30){
      day = day - 30;
      month = month + 1;
    }
  }
  let checkYear = year % 4;
  if(month === 2){
    if(checkYear === 0){
      if(day > 28){
        day = day - 28;
        month = month + 1;
      }
    }
    else{
      if(day > 29){
        day = day - 29;
        month = month + 1;
      }
    }
  }

  var minutes = dateObj.getUTCMinutes();
  var seconds = dateObj.getUTCSeconds();
  if(day<10){day='0'+day}
  if(month<10){month='0'+month}
  if(hours<10){hours='0'+hours}
  if(minutes<10){minutes='0'+minutes}
  if(seconds<10){seconds='0'+seconds}

  return   day + "/" + month + "/" + year + " " + hours + ":" + minutes + ":" + seconds;
}
