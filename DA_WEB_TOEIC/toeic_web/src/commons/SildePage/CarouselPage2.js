import React, {Component} from 'react';
import "react-bootstrap-carousel/dist/react-bootstrap-carousel.css";
import Carousel from 'react-bootstrap/Carousel'
import {withStyles} from "@material-ui/core";
import styles from "./styles";
import {FormattedMessage} from "react-intl";

class CarouselPage2 extends Component {
  constructor(props) {
    super(props);
    this.handleSelect = this.handleSelect.bind(this);
    this.state = {
      indextabs: 0
    }
  }


  handleSelect(selectedIndex, e) {
    this.setState({
      indextabs: selectedIndex,
    });
  }

  render() {
    const {indextabs} = this.state;
    const {classes} = this.props
    return (
      <div >
        <Carousel activeIndex={indextabs}
                  touch="true"
                  slide="true"
                  className={classes.Carousel}
                  data-ride="carousel" data-interval="15000"
                  onSelect={this.handleSelect}>
          <Carousel.Item className={classes.CarouselItem}>
            <img
              className={classes.img}
              src="https://mdbootstrap.com/img/Photos/Slides/img%20(68).jpg"
              alt="First slide"
            />
            <Carousel.Caption className={classes.CarouselCaption}>
              <h1> HỌC TIẾNG ANH ONLINE 1 KÈM 1</h1>
              <p> Học tiếng Anh trực tuyến với giáo viên nước ngoài qua Skype.</p>
              <button className={classes.btnRedirect}><FormattedMessage id={"Detail"}/></button>
            </Carousel.Caption>
          </Carousel.Item>
          <Carousel.Item className={classes.CarouselItem}>
            <img
              className={classes.img}
              src="https://mdbootstrap.com/img/Photos/Slides/img%20(6).jpg"
              alt="Second slide"
            />

            <Carousel.Caption className={classes.CarouselCaption}>
              <h1>Second slide label</h1>
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
              <button className={classes.btnRedirect}><FormattedMessage id={"Detail"}/></button>
            </Carousel.Caption>
          </Carousel.Item>
          <Carousel.Item className={classes.CarouselItem}>
            <img
              className={classes.img}
              src="https://mdbootstrap.com/img/Photos/Slides/img%20(9).jpg"
              alt="Third slide"
            />

            <Carousel.Caption className={classes.CarouselCaption}>
              <h1>Third slide label</h1>
              <p>
                Praesent commodo cursus magna, vel scelerisque nisl consectetur.
              </p>
              <button className={classes.btnRedirect}><FormattedMessage id={"Detail"}/></button>
            </Carousel.Caption>
          </Carousel.Item>
        </Carousel>
      </div>

    );
  }
}

export default withStyles(styles)(CarouselPage2);
