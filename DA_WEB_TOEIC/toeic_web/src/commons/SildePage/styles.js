const styles = theme => ({
  Carousel: {
    height: "75vh"
  }, CarouselItem: {
    height: "100vh"
  },
  CarouselCaption: {
    left: "15%",
    top: "15%",
    textAlign: "left",
    fontSize: 30,
    lineHeight: 1.5,
    letterSpacing: "1.5px",
    "&>h1": {
      shadowColor: '#000000',
      shadowRadius: 10,
      shadowOpacity: 0.6,
      elevation: 8,
      fontSize: 50,
      fontWeight: 600,
      shadowOffset: {
        width: 0,
        height: 4
      }
    },
    brightness: "80%"
  },
  img: {
    width: "100%",
    height: "75%",
    opacity: "0.8"
  },
  btnRedirect: {
    backgroundColor: "#2196f3",
    border: "1px solid #2196f3",
    borderRadius: '30px',
    color: "#FFFFFF",
    textAlign: 'center',
    minWidth: '210px',
    height: " 45px",
    lineHeight: "45px",
    marginTop: "15px",
    transition: "all 0.3s",
    fontSize: "14px",
    textTransform: "uppercase",
    fontWeight: 600,
    display: " inline-block",
    marginRight: "24px",
    "&:hover": {
      color: "red"
    }
  }
})
export default styles
