import {getSessionCookie} from "./../configCookie/index";

export function authHeader() {
  // return authorization header with jwt token
  let user = getSessionCookie() !== null ? getSessionCookie() : null;
  if (user && user !== null ) {
    // const token = `Bearer ` + user.token
    return user;
  } else {
    return null;
  }
}
