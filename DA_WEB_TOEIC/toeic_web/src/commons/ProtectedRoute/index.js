import React from 'react';
import {Redirect, Route} from "react-router-dom";
import {connect} from "react-redux";
import {authHeader} from "./../_helpers/autHeader";
import {getSessionCookie} from "../configCookie";

const ProtectedRoutes = ({component: Component, ...rest}) => {
  // const {statusCode} = this.props;
  return <Route {...rest} render={
    props => (authHeader() !== null && getSessionCookie()
      ?
      <Component {...props} />
      : <Redirect to={{pathname: '/pages/login', state: {from: props.location}}}/>)
  }>
  </Route>
}
const mapStateToProps = (state, ownProps) => {
  return {
    statusCode: state.userReducer.logInCode
  }
}

export default connect(mapStateToProps)(ProtectedRoutes)

