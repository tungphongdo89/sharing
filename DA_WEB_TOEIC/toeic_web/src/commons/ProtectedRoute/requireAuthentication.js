import React from 'react';
import {connect} from 'react-redux';
import {history} from "../../history";

export function requireAuthentication(Component) {

  class AuthenticatedComponent extends React.Component {

    componentWillMount() {
      const {isStudent, isTeacher, isAdmin} = this.props;
      this.checkAuth(this.props.isAuthenticated, isStudent, isTeacher, isAdmin);
    }

    componentWillReceiveProps(nextProps) {
      this.checkAuth(nextProps.isAuthenticated, nextProps.isStudent, nextProps.isTeacher, nextProps.isAdmin);
    }

    checkAuth(isAuthenticated, isStudent, isTeacher, isAdmin) {
      const {location} = this.props.history;
      if (!isAuthenticated) {
        if (history) {
          history.push("/pages/login")
        }
      } else {
        if (isStudent) {
          if (!location.pathname.includes("/private/student/")) {
            alert("Bạn không có quyền truy cập!")
            history.push("/pages/login")
          }
        } else if (isAdmin) {
          if (!location.pathname.includes("/private/admin/")) {
            alert("Bạn không có quyền truy cập!")
            history.push("/pages/login")
          }
        } else if (isTeacher) {
          if (!location.pathname.includes("/private/teacher/")) {
            alert("Bạn không có quyền truy cập!")
            history.push("/pages/login")
          }
        }
      }
    }

    render() {
      console.log('vao day');
      return (
        <div>
          {this.props.isAuthenticated
            ? <Component {...this.props}/>
            : null
          }
        </div>
      )

    }
  }

  const mapStateToProps = (state) => ({
    token: state.userReducer.token,
    isAuthenticated: state.authenticationReducer.isAuthenticated,
    isStudent: state.authenticationReducer.isStudent,
    isTeacher: state.authenticationReducer.isTeacher,
    isAdmin: state.authenticationReducer.isAdmin,
  });

  return connect(mapStateToProps)(AuthenticatedComponent);
}
