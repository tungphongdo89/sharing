import React, {Component} from 'react';
import PropTypes from "prop-types";
import {useTheme,makeStyles} from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableHead from "@material-ui/core/TableHead";
import TableCell from "@material-ui/core/TableCell";
import TableFooter from "@material-ui/core/TableFooter";
import TablePagination from "@material-ui/core/TablePagination";
import TableContainer from "@material-ui/core/TableContainer";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import IconButton from "@material-ui/core/IconButton";
import FirstPageIcon from "@material-ui/icons/FirstPage";
import KeyboardArrowLeft from "@material-ui/icons/KeyboardArrowLeft";
import KeyboardArrowRight from "@material-ui/icons/KeyboardArrowRight";
import LastPageIcon from "@material-ui/icons/LastPage";
import {withStyles} from "@material-ui/core";
import styles from "./styles"

const rowsPerPageOptions = [5, 10, 15]

const defaultRowsPerPageOption = 5
const useStyles1 = makeStyles(theme => ({
  root: {
    flexShrink: 0,
    marginLeft: theme.spacing(2.5)
  }
}));

function TablePaginationActions(props) {
  const classes = useStyles1();
  const theme = useTheme();
  console.log('xxxxxxxx', props);
  const {count, page, rowsPerPage, onChangePage} = props;

  function handleFirstPageButtonClick(event) {
    onChangePage(event, 0);
  }

  function handleBackButtonClick(event) {
    onChangePage(event, page - 1);
  }

  function handleNextButtonClick(event) {
    onChangePage(event, page + 1);
  }

  function handleLastPageButtonClick(event) {
    onChangePage(event, Math.max(0, Math.ceil(count / rowsPerPage) - 1));
  }

  return (
    <div className={classes.root}>
      <IconButton
        onClick={handleFirstPageButtonClick}
        disabled={page === 0}
        aria-label="first page"
      >
        {theme.direction === "rtl" ? <LastPageIcon/> : <FirstPageIcon/>}
      </IconButton>
      <IconButton
        onClick={handleBackButtonClick}
        disabled={page === 0}
        aria-label="previous page"
      >
        {theme.direction === "rtl" ? (
          <KeyboardArrowRight/>
        ) : (
          <KeyboardArrowLeft/>
        )}
      </IconButton>
      <IconButton
        onClick={handleNextButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="next page"
      >
        {theme.direction === "rtl" ? (
          <KeyboardArrowLeft/>
        ) : (
          <KeyboardArrowRight/>
        )}
      </IconButton>
      <IconButton
        onClick={handleLastPageButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="last page"
      >
        {theme.direction === "rtl" ? <FirstPageIcon/> : <LastPageIcon/>}
      </IconButton>
    </div>
  );
}

const rows = [
  {name: "Cupcake", calories: 305, fat: 3.7},
  {name: "Donut", calories: 452, fat: 25.0},
  {name: "Eclair", calories: 262, fat: 16.0},
  {name: "Frozen yoghurt", calories: 159, fat: 6.0},
  {name: "Gingerbread", calories: 356, fat: 16.0},
  {name: "Honeycomb", calories: 408, fat: 3.2},
  {name: "Ice cream sandwich", calories: 237, fat: 9.0},
  {name: "Jelly Bean", calories: 375, fat: 0.0},
  {name: "KitKat", calories: 518, fat: 26.0},
  {name: "Lollipop", calories: 392, fat: 0.2},
  {name: "Marshmallow", calories: 318, fat: 0},
  {name: "Nougat", calories: 360, fat: 19.0},
  {name: "Oreo", calories: 437, fat: 18.0}
]

class LazyTablePaging extends Component {
  constructor(props) {
    super(props);
    this.state = {
      pages: 0,
      rowsPerPage: defaultRowsPerPageOption || rowsPerPageOptions[0],
      columns: props.columns,
      data: props.data,
      count: 100
    }
  }

  handleChangePage = (event, newPage) => {
    this.setState({
      pages: newPage
    });
  }

  handleChangeRowsPerPage = event => {
    const rowsPerPage = parseInt(event.target.value, 10);
    this.setState({
      rowsPerPage: rowsPerPage,
      pages: 0
    })
  }

  render() {
    console.log('tada');
    const {rowsPerPage, pages} = this.state;
    const emptyRows = rowsPerPage - Math.min(rowsPerPage, rows.length - pages * rowsPerPage);
    const {classes} = this.props;
    return (

      <Paper className={classes.root}>
        <div className={classes.tableWrapper}>
          <TableContainer>
            <Table className={classes.table}>
              <TableHead>
                <TableRow>
                  <TableCell className={classes.head} align="center">name</TableCell>
                  <TableCell className={classes.head} align="center">Calories</TableCell>
                  <TableCell className={classes.head} align="center">fat</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {rows
                  .slice(pages * rowsPerPage, pages * rowsPerPage + rowsPerPage)
                  .map(row => (
                    <TableRow key={row.name}>
                      <TableCell className={classes.rowBody} component="th" scope="row">
                        {row.name}
                      </TableCell>
                      <TableCell className={classes.rowBody} align="right">{row.calories}</TableCell>
                      <TableCell className={classes.rowBody} align="right">{row.fat}</TableCell>
                    </TableRow>
                  ))}

                {emptyRows > 0 && (
                  <TableRow style={{height: 48 * emptyRows}}>
                    <TableCell colSpan={6}/>
                  </TableRow>
                )}
              </TableBody>
              {/*<TableFooter className={classes.tableFooter}>*/}
              {/*<TableRow className={classes.tableRow}>*/}
              {/*</TableRow>*/}
              {/*</TableFooter>*/}
            </Table>
          </TableContainer>
          <TablePagination className={classes.tableFooter}
                           rowsPerPageOptions={rowsPerPageOptions}
                           colSpan={3}
                           count={rows.length}
                           rowsPerPage={rowsPerPage}
                           page={pages}
                           SelectProps={{
                             inputProps: {"aria-label": "rows per page"},
                             value: rowsPerPage
                           }}
                           onChangePage={this.handleChangePage}
                           onChangeRowsPerPage={this.handleChangeRowsPerPage}
                           ActionsComponent={TablePaginationActions}
          />
        </div>
      </Paper>
    );
  }
}

LazyTablePaging.propTypes = {
  count: PropTypes.number.isRequired,
  onChangePage: PropTypes.func.isRequired,
  page: PropTypes.number.isRequired,
  rowsPerPage: PropTypes.number.isRequired
};

export default withStyles(styles)(LazyTablePaging);
