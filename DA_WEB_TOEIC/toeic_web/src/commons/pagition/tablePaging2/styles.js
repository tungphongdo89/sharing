const styles = theme => ({
  root: {
    width: "100%",
    marginTop: theme.spacing(3)
  },
  table: {
    minWidth: 500
  },
  tableWrapper: {
    overflowX: "auto"
  },
  tableFooter: {
    float: "right"
  },
  head: {
    // borderRight: "1px solid gray"
  },
  rowBody: {
    borderRight: "1px solid gray"
  }

})
export default styles;
