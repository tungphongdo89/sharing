import React, {Component} from 'react';
import MaterialTable from 'material-table';

class TablePaging extends Component {
  constructor(props) {
    super(props);
    this.state = {
      columns: props.columns,
      data: props.data,
      count: 100
    }
  }

  render() {
    console.log('tada');
    const {state} = this;
    return (
      <MaterialTable
        title={this.props.title}
        columns={state.columns}
        data={state.data}
        count={state.count}
        editable={{
          onRowAdd: (newData) =>
            new Promise((resolve) => {
              setTimeout(() => {
                resolve();
                this.setState((prevState) => {
                  const data = [...prevState.data];
                  data.push(newData);
                  return {...prevState, data};
                });
              }, 600);
            }),
          onRowUpdate: (newData, oldData) =>
            new Promise((resolve) => {
              setTimeout(() => {
                resolve();
                if (oldData) {
                  this.setState((prevState) => {
                    const data = [...prevState.data];
                    data[data.indexOf(oldData)] = newData;
                    return {...prevState, data};
                  });
                }
              }, 600);
            }),
          onRowDelete: (oldData) =>
            new Promise((resolve) => {
              setTimeout(() => {
                resolve();
                this.setState((prevState) => {
                  const data = [...prevState.data];
                  data.splice(data.indexOf(oldData), 1);
                  return {...prevState, data};
                });
              }, 600);
            }),
        }}
      />
    );
  }
}

export default TablePaging;
