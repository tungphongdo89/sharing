const STATUS_CODE = {
  SUCCESS: 200,
  CREATED: 201,
  UPDATED: 202,
  AUTHEN_FALSE:401
}

export default STATUS_CODE
