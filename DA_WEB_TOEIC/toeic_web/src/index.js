import React, {lazy, Suspense} from "react"
import ReactDOM from "react-dom"
import {Provider} from "react-redux"
import {Auth0Provider} from "./authServices/auth0/auth0Service"
import config from "./authServices/auth0/auth0Config.json"
import {IntlProviderWrapper} from "./utility/context/Internationalization"
import Layout from "./utility/context/Layout"
import store, {persistor} from "./redux/storeConfig/store"
import Spinner from "./components/@vuexy/spinner/Fallback-spinner"
import "./index.scss"
import "./@fake-db"
import {PersistGate} from 'redux-persist/lib/integration/react';
import {autoRehydrate} from 'redux-persist'
import {ToastContainer} from 'react-toastify';
import "react-toastify/dist/ReactToastify.css"
import "./assets/scss/plugins/extensions/toastr.scss"
import * as events from './event.js'; //Tra cuu tu vung 
import TranslatePopup from "./views/pages/translatePopup/TranslatePopup"

const LazyApp = lazy(() => import("./App"))


// configureDatabase()

ReactDOM.render(
  <Auth0Provider
    domain={config.domain}
    client_id={config.clientId}
    redirect_uri={window.location.origin + process.env.REACT_APP_PUBLIC_PATH}
  >
    <Provider store={store}>
      <Suspense fallback={<Spinner/>}>
        <PersistGate loading={null} persistor={persistor}>
          <Layout>
            <IntlProviderWrapper>
               <LazyApp />
               <TranslatePopup />
            </IntlProviderWrapper>
          </Layout>
          <ToastContainer/>
        </PersistGate>
      </Suspense>
    </Provider>
  </Auth0Provider>,
  document.getElementById("root")
)

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
// serviceWorker.unregister()
