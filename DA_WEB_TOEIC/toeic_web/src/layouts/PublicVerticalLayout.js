import React, {PureComponent} from "react";
import {withStyles} from "@material-ui/core";
import styles from "./styles"
import PrimarySearchAppBar from "../components/public/GeneralDirectory/Header/PrimarySearchAppBar";
import BannerComponent from "../views/pages/banner/Banner";
// import themeConfig from "../configs/themeConfig"
import Customizer from "../components/@vuexy/customizer/Customizer";
import {changeFooterType, hideScrollToTop} from "../redux/actions/customizer/index";
import Footer from "./components/footer/Footer";
import {connect} from "react-redux";
import {compose} from "redux";

// import ContentHomePage from "../components/public/GeneralDirectory/Content";



class PublicVerticalLayout extends PureComponent {
  constructor(props) {
    super(props);
    this.myRef = React.createRef();
  }

  // renderContent() {
  //   let xhtml = null;
  //   const {openContactForm} = this.props;
  //   if (openContactForm == 1) {
  //     xhtml = (<ContentContactPage/>);
  //   } else if (openContactForm == 0) {
  //     xhtml = (<ContentHomePage/>);
  //   }
  //   return xhtml;
  // }

  scrollToMyRef = () =>  this.myRef.current.scrollIntoView();

  render() {
    const {classes} = this.props
    let appProps = this.props.app.customizer;
    let footerProps = {
      footerType: appProps.footerType,
      hideScrollToTop: appProps.hideScrollToTop
    };

    let customizerProps = {
      changeFooterType: this.props.changeFooterType,
      hideScrollToTop: this.props.hideScrollToTop,
      scrollToTop: appProps.hideScrollToTop
    };

    return (
      <div className={classes.dashboard}>
        <div>
          <div>
            <PrimarySearchAppBar onRef={this.scrollToMyRef}/>
          </div>
          <div className="content-wrapper">
            <div>
              <BannerComponent/>
            </div>
            <div ref={this.myRef}>
              {this.props.children}
            </div>
            {/*{this.renderContent()}*/}
          </div>
        </div>
        <div className="container">
          <Footer {...footerProps} />
          {appProps.disableCustomizer !== true ? (
            <Customizer {...customizerProps} />
          ) : null}
          <div
            className="sidenav-overlay"
          />
        </div>
      </div>
    );
  }
}

// PublicVerticalLayout.propTypes = {
//   userInfo: PropTypes.object
// };
const mapStateToProps = (state) => ({
  // userInfo: state.auth.login,
  // openContactForm: state.ContactReducer.openContactForm,
  app: state.customizer
});
const withconnect = connect(mapStateToProps, {
  changeFooterType,
  hideScrollToTop
})
export default compose(withStyles(styles), withconnect)(PublicVerticalLayout);
