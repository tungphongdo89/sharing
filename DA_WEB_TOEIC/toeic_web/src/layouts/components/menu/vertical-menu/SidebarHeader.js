import React, { Component } from "react"
import { NavLink } from "react-router-dom"
import { Disc, X, Circle } from "react-feather"
import classnames from "classnames"
import { useAuth0 } from "../../../../authServices/auth0/auth0Service"
import { history } from "../../../../history"
import store from "./../../../../redux/actions/auth/loginActions";
import loginActions from './../../../../redux/actions/auth/loginActions';
import { connect } from "react-redux";

//const { logout, isAuthenticated } = useAuth0();

const SideBarHeaderRender = (props) => {
  const { useAuth0 } = props;
  const { logout } = useAuth0();

  return (
    <a className="navbar-brand" // to="/app/user/list" 
      onClick={e => {
        e.preventDefault()
        debugger;
        if (1) {
          //window.history.pushState({}, null, "/");
          //history.push("/");
          return logout({
            returnTo: window.location.origin
          })
        }


      }}>
      <div className="brand-logo" />
    </a>
  )
}

class SidebarHeader extends Component {
  backToHome = () => {

    //history.push("/");
    // logout({
    //   returnTo: window.location.origin + process.env.REACT_APP_PUBLIC_PATH
    // });
  }

  render() {

    let {
      toggleSidebarMenu,
      activeTheme,
      collapsed,
      toggle,
      sidebarVisibility,
      menuShadow
    } = this.props
    return (
      <div className="navbar-header">
        <ul className="nav navbar-nav flex-row">
          <li className="nav-item mr-auto">
            {/* <SideBarHeaderRender {...this.props} /> */}
            <a className="navbar-brand" // to="/app/user/list" 
              href="/"    
            >
              <div className="brand-logo" />
            </a>
            {/* <a className="navbar-brand"  // to="/app/user/list"
              onClick={e => {
                e.preventDefault()
                debugger;
              }}>
              <div className="brand-logo" />
              <h2 className="brand-text mb-0">Vuexy</h2>
            </a> */}
          </li>
          <li className="nav-item nav-toggle">
            <div className="nav-link modern-nav-toggle">
              {collapsed === false ? (
                <Disc
                  onClick={() => {
                    toggleSidebarMenu(true)
                    toggle()
                  }}
                  className={classnames(
                    "toggle-icon icon-x d-none d-xl-block font-medium-4",
                    {
                      "text-primary": activeTheme === "primary",
                      "text-success": activeTheme === "success",
                      "text-danger": activeTheme === "danger",
                      "text-info": activeTheme === "info",
                      "text-warning": activeTheme === "warning",
                      "text-dark": activeTheme === "dark"
                    }
                  )}
                  size={20}
                  data-tour="toggle-icon"
                />
              ) : (
                  <Circle
                    onClick={() => {
                      toggleSidebarMenu(false)
                      toggle()
                    }}
                    className={classnames(
                      "toggle-icon icon-x d-none d-xl-block font-medium-4",
                      {
                        "text-primary": activeTheme === "primary",
                        "text-success": activeTheme === "success",
                        "text-danger": activeTheme === "danger",
                        "text-info": activeTheme === "info",
                        "text-warning": activeTheme === "warning",
                        "text-dark": activeTheme === "dark"
                      }
                    )}
                    size={20}
                  />
                )}
              <X
                onClick={sidebarVisibility}
                className={classnames(
                  "toggle-icon icon-x d-block d-xl-none font-medium-4",
                  {
                    "text-primary": activeTheme === "primary",
                    "text-success": activeTheme === "success",
                    "text-danger": activeTheme === "danger",
                    "text-info": activeTheme === "info",
                    "text-warning": activeTheme === "warning",
                    "text-dark": activeTheme === "dark"
                  }
                )}
                size={20}
              />
            </div>
          </li>
        </ul>
        <div
          className={classnames("shadow-bottom", {
            "d-none": menuShadow === false
          })}
        />
      </div>
    )
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    useAuth0: useAuth0
  }
}

export default connect(null, mapDispatchToProps)(SidebarHeader)
//export default SidebarHeader
