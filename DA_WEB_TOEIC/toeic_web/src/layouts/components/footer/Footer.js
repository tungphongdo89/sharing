import React from "react"
import ScrollToTop from "react-scroll-up"
import {Button, FormGroup, Input, Label} from "reactstrap"
import {ArrowUp, Facebook, Heart, Instagram, Twitter,Linkedin} from "react-feather"
import classnames from "classnames"

const Footer = props => {
  let footerTypeArr = ["sticky", "static", "hidden"]
  return (
    <footer
      className={classnames("footer footer-light", {
        "footer-static": props.footerType === "static" || !footerTypeArr.includes(props.footerType),
        "d-none": props.footerType === "hidden"
      })}
    >
      <p className="mb-0 clearfix">
        <span className="float-md-left d-block d-md-inline-block mt-25">
          Design © {new Date().getFullYear()} by
          <a
            href="https://migitek.com"
            target="_blank"
            rel="migitek"
          >
            MIGI
          </a>
          Technology Co., Ltd
        </span>
        <span className="float-md-right d-none d-md-block">
          {/*<span className="align-middle">Hand-crafted & Made with</span>{" "}*/}
          <a href="https://www.facebook.com/MIGI-Technology-Co-Ltd-100414928209410" target="_blank">
              <Facebook className="font-medium-5 text-primary" size={22}/>
          </a>
          <a href="https://www.linkedin.com/company/migitek/" target="_blank" >
            <Linkedin className="font-medium-5 text-primary" size={22}></Linkedin>
          </a>
          {/*<Instagram className="font-medium-5 text-danger" size={22}/>*/}
          {/*<Twitter className="font-medium-5 text-info" size={22}/>*/}
        </span>
      </p>
      {props.hideScrollToTop === false ? (
        <ScrollToTop showUnder={160}>
          <Button color="primary" className="btn-icon scroll-top">
            <ArrowUp size={15}/>
          </Button>
        </ScrollToTop>
      ) : null}
    </footer>
  )
}

export default Footer
