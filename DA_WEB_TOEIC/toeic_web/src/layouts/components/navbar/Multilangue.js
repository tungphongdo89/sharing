import React from "react"
import {Dropdown, DropdownItem, DropdownMenu, DropdownToggle} from "reactstrap"
import {useAuth0} from "../../../authServices/auth0/auth0Service"
import {history} from "../../../history"
import ReactCountryFlag from "react-country-flag";
import {IntlContext} from "../../../utility/context/Internationalization"
import {bindActionCreators} from "redux";
import loginActions from "../../../redux/actions/auth/loginActions";
import {connect} from "react-redux";
import {getSessionCookie, getSessionLanggue, setSessionLanggue} from "../../../commons/configCookie";
import {toast} from "react-toastify";
import {showMessage} from "../../../commons/utils/convertDataForToast";
import {ROLE_STUDENT} from "../../../commons/constants/CONSTANTS";
import langgueActions from "../../../redux/actions/lang";

const logoutUser = (props) => {
  debugger
  const {loginAction} = props
  const {logoutWithJWT} = loginAction
  toast.error(showMessage("expired.session"));
  history.push("/");
  logoutWithJWT()
}

const checkSession = (props) => {
  return logoutUser(props)
}

const setLangguage = (data) => {
  const {langgueActions} = this.props
  langgueActions(data)
}

class Multilangue extends React.PureComponent {
  state = {
    langDropdown: false,
  }
  handleLangDropdown = () =>
    this.setState({langDropdown: !this.state.langDropdown})

  render() {
    return (
      <IntlContext.Consumer>
        {context => {
          let langArr = {
            "vn": "Việt Nam",
            "en": "English",
          }
          return (
            <Dropdown
              tag="li"
              className="dropdown-language nav-item d-flex"
              isOpen={this.state.langDropdown}
              toggle={this.handleLangDropdown}
              data-tour="language"
            >
              <DropdownToggle
                tag="a"
                className="nav-link"
              >
                <ReactCountryFlag
                  className="country-flag"
                  countryCode={
                    context.state.locale === "en"
                      ? "us"
                      : context.state.locale
                  }
                  svg
                />
                <span className="d-sm-inline-block d-none text-capitalize align-middle ml-50">
                    {langArr[context.state.locale]}
                  {setSessionLanggue(context.state.locale)}
                  </span>
              </DropdownToggle>
              <DropdownMenu right hidden={this.props.checkDisabled} aria-hidden={this.props.checkDisabled}>
                <DropdownItem
                  tag="a"
                  onClick={e => getSessionCookie() || this.props.user.login.userRole === ROLE_STUDENT ? context.switchLanguage("vn") : checkSession(this.props)}
                >
                  <ReactCountryFlag className="country-flag" countryCode="vn" svg/>
                  <span className="ml-1">Việt Nam</span>
                </DropdownItem>
                <DropdownItem
                  tag="a"
                  onClick={e => getSessionCookie() || this.props.user.login.userRole === ROLE_STUDENT ? context.switchLanguage("en") : checkSession(this.props)}
                >
                  <ReactCountryFlag className="country-flag" countryCode="us" svg/>
                  <span className="ml-1">English</span>
                </DropdownItem>
              </DropdownMenu>
            </Dropdown>
          )
        }}
      </IntlContext.Consumer>
    )
  }
}

const mapStateToProps = state => {
  return {
    user: state.auth
  }
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    loginAction: bindActionCreators(loginActions, dispatch),
    langgueActions: bindActionCreators(langgueActions, dispatch),
    useAuth0: useAuth0
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Multilangue)
