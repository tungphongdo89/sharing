import React from "react"
import {DropdownItem, DropdownMenu, DropdownToggle, UncontrolledDropdown} from "reactstrap"
import {useAuth0} from "../../../authServices/auth0/auth0Service"
import {history} from "../../../history"
import styled from "styled-components"
import {bindActionCreators} from "redux";
import loginActions from "../../../redux/actions/auth/loginActions";
import {connect} from "react-redux";
import {toast} from "react-toastify";
import {showMessage} from "../../../commons/utils/convertDataForToast";
import * as Icon from "react-bootstrap-icons"
import Multilangue from "./Multilangue";
import {FormattedMessage} from "react-intl";

const DropdownMenuWidth50 = styled(DropdownMenu)`
  width : 150%;
`

// Chuyển Page
const handleNavigation = (e, path) => {
  e.preventDefault()
  history.push(path)
}

const UserDropdown = props => {
  const {logout, isAuthenticated} = useAuth0()

  return (
    <DropdownMenuWidth50 right style={{maxWidth: '200px', with: 'auto'}}>
      {/*Quản lý tài khoản*/}
      <DropdownItem
        tag="a"
        href="#"
        onClick={e => handleNavigation(e, "/components/account")}
      >
        {/*<Icon.User size={14} className="mr-50"/>*/}
        <span className="align-middle"><FormattedMessage id="management.account"/></span>
      </DropdownItem>
      <DropdownItem
        tag="a"
        href="#"
        onClick={e => handleNavigation(e, "/pages/reset-password")}
      >
        <span className="align-middle"><FormattedMessage id="change.pass"/></span>
      </DropdownItem>
      <DropdownItem
        tag="a"
        // href="/pages/login"
        onClick={e => {
          e.preventDefault()
          if (isAuthenticated) {
            return logout({
              returnTo: window.location.origin + process.env.REACT_APP_PUBLIC_PATH
            })
          } else {
            const provider = props.loggedInWith
            if (provider !== null) {
              if (provider === "jwt") {
                history.push("/");
                return props.logoutWithJWT()
              }
            } else {
              history.push("/");
              return props.logoutWithJWT()
              // history.push("/pages/login")
            }

          }

        }}
      >
        {/*<Icon.Power size={14} className="mr-50"/>*/}
        <span className="align-middle"><FormattedMessage id="logout"/></span>
      </DropdownItem>
    </DropdownMenuWidth50>
  )
}

const logoutUser = (props) => {
  const {loginAction} = props
  const {logoutWithJWT} = loginAction
  toast.error(showMessage("expired.session"));
  history.push("/");
  logoutWithJWT()
}

const checkSession = (props) => {
  return logoutUser(props)
}

class NavbarUser extends React.PureComponent {
  state = {
    navbarSearch: false,
    shoppingCart: [
      {
        id: 1,
        name:
          "Apple - Apple Watch Series 1 42mm Space Gray Aluminum Case Black Sport Band - Space Gray Aluminum",
        desc:
          "Durable, lightweight aluminum cases in silver, space gray, gold, and rose gold. Sport Band in a variety of colors. All the features of the original Apple Watch, plus a new dual-core processor for faster performance. All models run watchOS 3. Requires an iPhone 5 or later.",
        price: "$299",
        img: require("../../../assets/img/pages/eCommerce/4.png"),
        width: 75
      },
      {
        id: 2,
        name:
          "Apple - Macbook® (Latest Model) - 12' Display - Intel Core M5 - 8GB Memory - 512GB Flash Storage Space Gray",
        desc:
          "MacBook delivers a full-size experience in the lightest and most compact Mac notebook ever. With a full-size keyboard, force-sensing trackpad, 12-inch Retina display,1 sixth-generation Intel Core M processor, multifunctional USB-C port, and now up to 10 hours of battery life,2 MacBook features big thinking in an impossibly compact form.",
        price: "$1599.99",
        img: require("../../../assets/img/pages/eCommerce/dell-inspirion.jpg"),
        width: 100,
        imgClass: "mt-1 pl-50"
      },
      {
        id: 3,
        name: "Sony - PlayStation 4 Pro Console",
        desc:
          "PS4 Pro Dynamic 4K Gaming & 4K Entertainment* PS4 Pro gets you closer to your game. Heighten your experiences. Enrich your adventures. Let the super-charged PS4 Pro lead the way.** GREATNESS AWAITS",
        price: "$399.99",
        img: require("../../../assets/img/pages/eCommerce/7.png"),
        width: 88
      },
      {
        id: 4,
        name:
          "Beats by Dr. Dre - Geek Squad Certified Refurbished Beats Studio Wireless On-Ear Headphones - Red",
        desc:
          "Rock out to your favorite songs with these Beats by Dr. Dre Beats Studio Wireless GS-MH8K2AM/A headphones that feature a Beats Acoustic Engine and DSP software for enhanced clarity. ANC (Adaptive Noise Cancellation) allows you to focus on your tunes.",
        price: "$379.99",
        img: require("../../../assets/img/pages/eCommerce/10.png"),
        width: 75
      },
      {
        id: 5,
        name:
          "Sony - 75' Class (74.5' diag) - LED - 2160p - Smart - 3D - 4K Ultra HD TV with High Dynamic Range - Black",
        desc:
          "This Sony 4K HDR TV boasts 4K technology for vibrant hues. Its X940D series features a bold 75-inch screen and slim design. Wires remain hidden, and the unit is easily wall mounted. This television has a 4K Processor X1 and 4K X-Reality PRO for crisp video. This Sony 4K HDR TV is easy to control via voice commands.",
        price: "$4499.99",
        img: require("../../../assets/img/pages/eCommerce/sony-75class-tv.jpg"),
        width: 100,
        imgClass: "mt-1 pl-50"
      },
      {
        id: 6,
        name:
          "Nikon - D810 DSLR Camera with AF-S NIKKOR 24-120mm f/4G ED VR Zoom Lens - Black",
        desc:
          "Shoot arresting photos and 1080p high-definition videos with this Nikon D810 DSLR camera, which features a 36.3-megapixel CMOS sensor and a powerful EXPEED 4 processor for clear, detailed images. The AF-S NIKKOR 24-120mm lens offers shooting versatility. Memory card sold separately.",
        price: "$4099.99",
        img: require("../../../assets/img/pages/eCommerce/canon-camera.jpg"),
        width: 70,
        imgClass: "mt-1 pl-50"
      }
    ],
    suggestions: []
  }

  // cutStringName = (data) => {
  //   let str = data;
  //   if (data !== null) {
  //     if (data.toString().length <= 20) {
  //       str = data;
  //     } else {
  //       str = data.slice(0, 20) + '...';
  //     }
  //   }
  //   return str;
  // }

  render() {

    const {userShowName} = this.props.user.login.userInfoLogin;

    return (
      <ul className="nav navbar-nav navbar-nav-user float-right ">
        <Multilangue checkDisabled={true}/>
        <UncontrolledDropdown tag="li" className="dropdown-user nav-item ">
          <DropdownToggle tag="a" className="nav-link dropdown-user-link">
            <div className=" d-sm-flex d-flex">
              <span className="user-name text-bold-600 success mr-1" title={userShowName} style={{
                marginLeft: "1em ! important",
                whiteSpace: 'nowrap',
                maxWidth: '150px',
                overflow: 'hidden',
                textOverflow: 'ellipsis',
                margin: 'auto',
                paddingTop: '7%',
                paddingBottom: '6%'
              }}>
                {userShowName}
              </span>
            </div>
            <div className="user-nav d-sm-flex d-none m-auto">
                 <span data-tour="user">
              {
                this.props.userImg === null ?
                  <Icon.PersonCircle size={35}/> :
                  <img
                    src={this.props.userImg}
                    className="round"
                    height="40"
                    width="40"
                  />
              }
            </span>
            </div>

          </DropdownToggle>
          <UserDropdown {...this.props} />
        </UncontrolledDropdown>
      </ul>
    )
  }
}

const mapStateToProps = state => {
  return {
    user: state.auth
  }
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    loginAction: bindActionCreators(loginActions, dispatch),
    useAuth0: useAuth0
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(NavbarUser)
