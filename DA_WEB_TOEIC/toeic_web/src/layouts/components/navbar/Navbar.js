import React from "react"
import {Navbar} from "reactstrap"
import {connect} from "react-redux"
import classnames from "classnames"
import {useAuth0} from "../../../authServices/auth0/auth0Service"
import loginActions from "../../../redux/actions/auth/loginActions"
import NavbarUser from "./NavbarUser"
import {bindActionCreators} from "redux";
import ButtonBack from "../../../views/ui-elements/commons/ButtonBack";

const UserName = props => {
  let username = ""
  if (props.userdata !== undefined) {
    if (props.userdata.userInfoLogin.userShowName !== undefined) {
      username = props.userdata.userInfoLogin.userShowName
    } else {
      username = "John Doe"
    }

  } else {
    username = "John Doe"
  }
  return username
}

const UserNameEmail = props => {
  let usernameEmail = props.userdata.userInfoLogin.userName
  return usernameEmail
}

const logoutUser = (props) => {
  const {loginAction} = props
  const {logoutWithJWT} = loginAction
  logoutWithJWT()
}
//
// const goToPreviousPath = (props) => {
//   if (getSessionCookie()) {
//     history.goBack()
//   } else {
//     debugger
//     toast.error(showMessage("expired.session"));
//     history.push("/");
//     return logoutUser(props)
//   }
// }

const ThemeNavbar = props => {
  const {user} = useAuth0()
  const colorsArr = ["primary", "danger", "success", "info", "warning", "dark"]
  const navbarTypes = ["floating", "static", "sticky", "hidden"]

  return (
    <React.Fragment>
      <div className="content-overlay"/>
      <div className="header-navbar-shadow"/>
      <Navbar
        className={classnames(
          "header-navbar navbar-expand-lg navbar navbar-with-menu navbar-shadow",
          {
            "navbar-light": props.navbarColor === "default" || !colorsArr.includes(props.navbarColor),
            "navbar-dark": colorsArr.includes(props.navbarColor),
            "bg-primary":
              props.navbarColor === "primary" && props.navbarType !== "static",
            "bg-danger":
              props.navbarColor === "danger" && props.navbarType !== "static",
            "bg-success":
              props.navbarColor === "success" && props.navbarType !== "static",
            "bg-info":
              props.navbarColor === "info" && props.navbarType !== "static",
            "bg-warning":
              props.navbarColor === "warning" && props.navbarType !== "static",
            "bg-dark":
              props.navbarColor === "dark" && props.navbarType !== "static",
            "d-none": props.navbarType === "hidden" && !props.horizontal,
            "floating-nav":
              (props.navbarType === "floating" && !props.horizontal) || (!navbarTypes.includes(props.navbarType) && !props.horizontal),
            "navbar-static-top":
              props.navbarType === "static" && !props.horizontal,
            "fixed-top": props.navbarType === "sticky" || props.horizontal,
            "scrolling": props.horizontal && props.scrolling

          }
        )}
      >
        <div className="navbar-wrapper">
          <div className="navbar-container content">
            <div
              className="navbar-collapse d-flex justify-content-between align-items-center float-right w-100"
              id="navbar-mobile"
            >
              <ButtonBack />
              {/*<div className="bookmark-wrapper">*/}
              {/*<NavbarBookmarks*/}
              {/*sidebarVisibility={props.sidebarVisibility}*/}
              {/*handleAppOverlay={props.handleAppOverlay}*/}
              {/*/>*/}
              {/*</div>*/}
              {props.horizontal ? (
                <div className="logo d-flex align-items-center">
                </div>
              ) : null}
              <NavbarUser
                handleAppOverlay={props.handleAppOverlay}
                changeCurrentLang={props.changeCurrentLang}
                userName={<UserName userdata={props.user.login}/>}
                userNameEmail={<UserNameEmail userdata={props.user.login}/>}
                // userImg={
                //   props.user.login.values !== undefined &&
                //   props.user.login.values.loggedInWith !== "jwt" &&
                //   props.user.login.values.photoUrl
                //     ? props.user.login.values.photoUrl
                //     : user !== undefined && user.picture ? user.picture
                //     : (props.user.login.userInfoLogin.userAvatar)
                // }
                userImg={props.user.login.userInfoLogin.userAvatar}
                loggedInWith={
                  props.user !== undefined &&
                  props.user.login.values !== undefined
                    ? props.user.login.values.loggedInWith
                    : null
                }
                logoutWithJWT={() => logoutUser(props)}
                // logoutWithFirebase={props.logoutWithFirebase}
              />
            </div>
          </div>
        </div>
      </Navbar>
    </React.Fragment>
  )
}

const mapStateToProps = state => {
  return {
    user: state.auth
  }
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    loginAction: bindActionCreators(loginActions, dispatch),
    useAuth0: useAuth0
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(ThemeNavbar)
