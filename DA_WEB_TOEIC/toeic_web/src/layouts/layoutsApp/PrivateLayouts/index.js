import React, {Component} from 'react';
import {ADMIN_ROUTES} from "../../../constants/Routes";
import AdminLayoutRoute from "../AdminLayoutRoute";

class PrivateLayouts extends Component {
  renderAdminRoute() {
    let xhtml = null;

    xhtml = ADMIN_ROUTES.map(route => {
      return <AdminLayoutRoute key={route.path} path={route.path} component={route.component} exact={route.exact}
                               name={route.name}/>
    })
    return xhtml;
  }

  render() {

    return (
      <div>
        {this.renderAdminRoute()}
      </div>

    );
  }
}

export default PrivateLayouts;
