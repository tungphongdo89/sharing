import React, {Component} from 'react';
import {DEFAULT_ROUTES} from "../../../constants/Routes";
import DefaultLayoutRoute from "../DefaultLayoutRoute";

class PublicLayouts extends Component {
  renderDefaultRoute() {
    let xhtml = null;

    xhtml = DEFAULT_ROUTES.map(route => {
      return <DefaultLayoutRoute key={route.path} path={route.path} component={route.component} exact={route.exact}
                                 name={route.name}/>
    })
    return xhtml;
  }

  render() {
    return (
      <div>
        {this.renderDefaultRoute()}
      </div>

    );
  }
}

export default PublicLayouts;
