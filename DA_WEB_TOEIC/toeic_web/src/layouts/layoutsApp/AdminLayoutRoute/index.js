import React, {Component} from 'react';
import {Route} from "react-router-dom";
import Dashboard from "../../../Component/Private/Dashboard";

class AdminLayoutRoute extends Component {
  render() {
    // const {path,} = this.props
    const {component: YourComponent, name, ...remainProps} = this.props;
    return (
      <Route {...remainProps} render={routeProps => {
        return (<Dashboard name={name}>
          <YourComponent {...routeProps}/>
        </Dashboard>)
      }}>
      </Route>
    );
  }
}

export default AdminLayoutRoute;
