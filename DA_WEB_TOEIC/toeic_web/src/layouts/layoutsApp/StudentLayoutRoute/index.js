import React, {Component} from 'react';
import {Route} from "react-router-dom";

class StudentLayoutRoute extends Component {
  render() {
    // const {path,} = this.props
    const {component: YourComponent, name, ...remainProps} = this.props;
    return (
      <Route {...remainProps} render={routeProps => {
        return <YourComponent name={name} {...routeProps}/>
      }}>
      </Route>
    );
  }
}

export default StudentLayoutRoute;
