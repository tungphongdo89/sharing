import * as Icon from "react-feather"
import React from "react";

const IconForMenu = [{
  name: "Home",
  value: <Icon.Home size={20}/>,
},
  {
    name: "User",
    value: <Icon.User size={20}/>,
  },
  {
    name: "Topic",
    value: <Icon.Tag size={20}/>,
  },
  {
    name: "Category",
    value: <Icon.Thermometer size={20}/>,
  },
  {
    name: "Test",
    value: <Icon.List size={20}/>,
  },
  {
    name: "Banner",
    value: <Icon.Droplet size={20}/>,
  }
  ,
  {
    name: "Dictionary",
    value: <Icon.Eye size={20}/>,
  }
]

export default IconForMenu
