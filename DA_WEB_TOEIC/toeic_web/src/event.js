import store from "../src/redux/storeConfig/store"
import wordAction from "../src/redux/actions/word/wordAction"
import { delay } from "redux-saga/effects";
import { ROLE_STUDENT  } from "../src/commons/constants/CONSTANTS";
var t = '';
var result = '';
var cordinate = {};
var wordInHtml = '';

function eventGetHighLightText(e) {
    // console.log(store.getState().auth.login.isAuthenticated,
    //      store.getState().auth.login.userRole,  
    //      store.getState().doTestReducer.noTranslating);
  if(!store.getState().auth.login.isAuthenticated || store.getState().auth.login.userRole != ROLE_STUDENT || 
      store.getState().doTestReducer.noTranslating == true )
    return;
   
  t = (document.all) ? document.selection.createRange().text : document.getSelection();
  // var box = t ? e.target.getBoundingClientRect() : {};
  // console.log(box);
  // cordinate= { x: box.x+70  , y: box.y-60};
  //console.log({x:box.top,y:box.left});  && t.anchorOffset>0 
  //console.log(t);
  if ( t != null &&  t.baseNode && t.baseNode.data && t.baseOffset >= 0 && t.extentOffset >= 0) {
    if(t.extentNode.wholeText !== t.baseNode.wholeText) return;
    t.baseOffset < t.extentOffset ?
      result = t.baseNode.data.substr(t.baseOffset, t.extentOffset - t.baseOffset)
      
      : result = t.baseNode.data.substr(t.extentOffset, t.baseOffset - t.extentOffset);
      if(!result || result.trim().length>20) return;
      if(result.length != Math.abs(t.baseOffset - t.extentOffset ) ) return;
      //z = result.replace(/[`~!@#$%^&*()_|+\=?;:",.<>\{\}\[\]\\\/]/gi, '');
      wordInHtml = result.trim();
      result = result.replace(/^[`~'!@#$%^&*()_\- |+\=?;:",.<>\{\}\[\]\\\/]*|[`~'!@#$%^&*()_\- |+\=?;:",.<>\{\}\[\]\\\/\/]*$/g, '').trim();
      // console.log("hello-",result);
      // console.log("hello123-",wordHighlight.trim());
      let range = new Range();
      let offset1 = 0;
      //console.log(t);
      if(result.length<1) return;
      if(t.baseOffset <= t.extentOffset ) {
        offset1 = t.baseOffset +result.length/2 +1  < t.baseNode.length 
         ? result.length/2 : 0;
        range.setStart( t.baseNode, t.baseOffset +offset1-1 > 0 ? t.baseOffset +offset1-2 : t.baseOffset  );
        range.setEnd( t.baseNode, t.extentOffset -offset1 < t.baseNode.length-1 ? t.extentOffset-offset1 : t.extentOffset );
      }
      else{
        offset1 =  t.extentOffset + result.length/2+1 < t.baseNode.length  
        ? result.length/2 : 0;
        range.setStart( t.baseNode, t.extentOffset + offset1-1 >0 ? t.extentOffset + offset1-2 : t.extentOffset );
        range.setEnd( t.baseNode,t.baseOffset- offset1 <  t.baseNode.length-1 ? t.baseOffset - offset1 :  t.baseOffset  );
      }
      //console.log(range);
      var box = t ? range.getBoundingClientRect() : {};
      //console.log(box);
      //cordinate= { x: ((box.x + window.scrollX -45)%width)*(100/width) , y: ((box.y +window.scrollY -15)%height)*(100/height)  };
      cordinate= { x: box.x + window.scrollX -45, y: box.y +window.scrollY -15  };
      
      //console.log(cordinate);
      if (result!=''  && result!=store.getState().wordReducer.title ) { //
      store.dispatch(wordAction.setCordinatePointer(cordinate));
      store.dispatch(wordAction.togglePopup(false));
      const obj = {
        result : result.trim(), wordInHtml :wordInHtml
      }
      store.dispatch(wordAction.setHighLightWord(obj));  
     // if (store.getState().wordReducer.description) {  
      setTimeout(() => store.dispatch(wordAction.togglePopup(true)) , 199);
      //}
    } 
  }
}

document.onmouseup = eventGetHighLightText;
if (!document.all) document.captureEvents(Event.MOUSEUP);

document.addEventListener('click', function (evt) {
  const popoverElement = document.querySelector(".popover-inner");
  let targetElement = evt.target; // clicked element
    do {
      if (targetElement == popoverElement ) {//Click ben trong popoverElement.contains(targetElement) targetElement == popoverElement
        //console.log("Clicked inside!");
        //t = '';
        evt.preventDefault();
        return false;
      }
      targetElement = targetElement.parentNode;
    } 
  while (targetElement);
  if (store.getState().wordReducer.title!='' || result == '') {  //   targetElement != flyoutElement -> Click ben ngoai
     //console.log("Clicked outside!"); 
     store.dispatch(wordAction.toggleStatusListening(false));
     store.dispatch(wordAction.fetchDetailVocabularyFailure({})); 
      
     return;
  }
});








