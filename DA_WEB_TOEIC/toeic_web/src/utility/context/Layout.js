import React from "react"
import VerticalLayout from "../../layouts/VerticalLayout"
import FullLayout from "../../layouts/FullpageLayout"
import themeConfig from "../../configs/themeConfig"
import {connect} from "react-redux"
import PublicVerticalLayout from "../../layouts/PublicVerticalLayout";
import {ROLE_ADMIN, ROLE_PREVIEW} from "../../commons/constants/CONSTANTS";
import {getSessionCookie} from "../../commons/configCookie";
import {bindActionCreators} from "redux";
import loginActions from "../../redux/actions/auth/loginActions";

const layouts = {
  vertical: VerticalLayout,
  full: FullLayout,
  // horizontal: HorizontalLayout
}
const layoutsMember = {
  vertical: PublicVerticalLayout,
  full: FullLayout,
  // horizontal: HorizontalLayout
}

const ContextLayout = React.createContext()

class Layout extends React.Component {
  state = {
    activeLayout: themeConfig.layout,
    width: window.innerWidth,
    lastLayout: null,
    direction: themeConfig.direction,
    verticalLayouts: layoutsMember["vertical"]
  }

  updateWidth = () => {
    this.setState({
      width: window.innerWidth
    })
  }

  componentWillReceiveProps(nextProps, nextContext) {
    console.log('vao day roi 1');
    const {location} = window;
    const {userInfo} = nextProps;
    if (userInfo.userRole.toUpperCase().trim() === ROLE_ADMIN || userInfo.userRole.toUpperCase().trim() === ROLE_PREVIEW) {
      if (location.pathname.includes("/public/")) {
        this.setState({
          verticalLayouts: layoutsMember["vertical"]
        });
      } else {
        this.setState({
          verticalLayouts: layouts["vertical"]
        });
      }
    } else {
      this.setState({
        verticalLayouts: layoutsMember["vertical"]
      });
    }
  }

  componentWillMount() {
    const {location} = window;
    const {userInfo} = this.props;
    const {loginActions} = this.props;
    const {changeRole} = loginActions;
    if (getSessionCookie() === null) {
      // Chuyển cai trong routerConfig vào public_Router rôi check nếu url trong mảng public_Router thì mở changRole , ko thì ko mở
      // changeRole()
    }
    if ((userInfo.userRole.toUpperCase().trim() === ROLE_ADMIN || userInfo.userRole.toUpperCase().trim() === ROLE_PREVIEW )&& getSessionCookie() !== null) {
      if (location.pathname.includes("/public/") || location.pathname === "/" || location.pathname === "/toeic_web" || location.pathname === "/toeic_web/") {
        this.setState({
          verticalLayouts: layoutsMember["vertical"]
        });
      } else {
        this.setState({
          verticalLayouts: layouts["vertical"]
        });
      }
    } else {
      this.setState({
        verticalLayouts: layoutsMember["vertical"]
      });
    }
  }


  // componentWillMount() {
  //   debugger
  //   const {location} = window;
  //   const {userInfo} = this.props;
  //   if (userInfo.userRole.toUpperCase().trim() === ROLE_ADMIN) {
  //     if (location.pathname.includes("/public/")) {
  //       this.setState({
  //         verticalLayouts: layoutsMember["vertical"]
  //       });
  //     } else {
  //       this.setState({
  //         verticalLayouts: layouts["vertical"]
  //       });
  //     }
  //   } else {
  //     this.setState({
  //       verticalLayouts: layoutsMember["vertical"]
  //     });
  //   }
  // }

  handleWindowResize = () => {
    this.updateWidth()
    if (this.state.activeLayout === "horizontal" && this.state.width <= 1199) {
      this.setState({
        activeLayout: "vertical",
        lastLayout: "horizontal"
      })
    }

    if (this.state.lastLayout === "horizontal" && this.state.width >= 1199) {
      this.setState({
        activeLayout: "horizontal",
        lastLayout: "vertical"
      })
    }
  }

  componentDidMount = () => {
    if (window !== "undefined") {
      window.addEventListener("resize", this.handleWindowResize)
    }
    this.handleDirUpdate()
    if (this.state.activeLayout === "horizontal" && this.state.width <= 1199) {
      this.setState({
        activeLayout: "vertical"
      })
    } else if (
      themeConfig.layout === "horizontal" &&
      this.state.width >= 1200
    ) {
      this.setState({
        activeLayout: "horizontal"
      })
    } else {
      this.setState({
        activeLayout: "vertical"
      })
    }
  }

  componentDidUpdate() {
    this.handleDirUpdate()
  }

  handleDirUpdate = () => {
    let dir = this.state.direction
    if (dir === "rtl")
      document.getElementsByTagName("html")[0].setAttribute("dir", "rtl")
    else document.getElementsByTagName("html")[0].setAttribute("dir", "ltr")
  }


  render() {
    const {children, userInfo} = this.props
    const {verticalLayouts} = this.state
    return (
      <ContextLayout.Provider
        value={{
          state: this.state,
          fullLayout: layouts["full"],
          VerticalLayout: verticalLayouts,
          switchLayout: layout => {
            this.setState({activeLayout: layout})
          },
          switchDir: dir => {
            this.setState({direction: dir})
          }
        }}
      >
        {children}
      </ContextLayout.Provider>
    )
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    userInfo: state.auth.login
  }
}
const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    loginActions: bindActionCreators(loginActions, dispatch)
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(Layout)
export {ContextLayout}

