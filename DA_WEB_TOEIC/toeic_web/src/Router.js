import React, {lazy, Suspense} from "react"
// import {Route, Switch, HashRouter as Router} from "react-router-dom"
import {Route, Router, Switch} from "react-router-dom"
// import {Route, Switch, BrowserRouter as Router} from "react-router-dom"
import {history} from "./history"
import {connect} from "react-redux"
import Spinner from "./components/@vuexy/spinner/Loading-spinner"
import {ContextLayout} from "./utility/context/Layout"
import AdminHomePage from "./components/private/management/AdminHomePage";
import PropTypes from "prop-types";
import ProtectedRoutes from "./commons/ProtectedRoute";
import {PUBLIC_ROUTES} from "./commons/RouterConfig/Route";

const error404 = lazy(() => import("./views/pages/misc/error/404"));

export const RouteConfig = ({component: Component, fullLayout, publicLayout, ...rest}) => (
  <Route
    {...rest}
    render={props => {

      return (
        <ContextLayout.Consumer>
          {context => {
            let LayoutTag =
              fullLayout === true
                ? context.fullLayout
                : context.state.activeLayout === "horizontal"
                ? context.horizontalLayout
                : publicLayout === true ? context.PublicVerticalLayout : context.VerticalLayout;
            return (
              <LayoutTag {...props} >
                <Suspense fallback={<Spinner/>}>
                  <Component {...props} />
                </Suspense>
              </LayoutTag>
            )
          }}
        </ContextLayout.Consumer>
      )
    }}
  />
);
const mapStateToProps = (state) => ({
  user: state.auth.login.userRole
});

const AppRoute = connect(mapStateToProps)(RouteConfig);


class AppRouter extends React.Component {
  renderDefaultRoute() {
    let xhtml = null;
    xhtml = PUBLIC_ROUTES.map(route => {
      console.log('route.path = ', route.path);
      return <AppRoute key={route.path} path={route.path} component={route.component} exact={route.exact}
                       fullLayout={route.fullLayout ? route.fullLayout : false}
                       publicLayout={route.publicLayout ? route.publicLayout : false}/>
      // return <DefaultLayoutRoute key={route.path} path={route.path} component={route.component} exact={route.exact}
      //                            name={route.name}/>
    });
    return xhtml;
  }

  // componentDidMount() {
  //   // Chan khong cho quay lai tren browser
  //   window.addEventListener("popstate", () => {
  //     history.go(1);
  //   });
  // }


  render() {
    return (
      // Set the directory path if you are deploying in sub-folder
      <Router history={history}>
        <Switch>
          <ProtectedRoutes exact path={["/private/*", "/error"]} component={AdminHomePage}/>
          {this.renderDefaultRoute()}
          <AppRoute component={error404} fullLayout/>
        </Switch>
      </Router>
    )
  }
}

AppRouter.propTypes = {
  user: PropTypes.object,
};

export default AppRouter
