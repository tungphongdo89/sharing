import axiosService from '../service/AxiosService'
import * as config from '../../commons/constants/Config'

export const fetchHistoryTest = (uri, data) => {
  return axiosService.post(`${config.API_ENDPOINT_SERVER}/${uri}`, data)
}

export const getDetailOfTest = (uri, data) => {
  return axiosService.post(`${config.API_ENDPOINT_SERVER}/${uri}`, data)
}

export const getRankOfTest = (uri, data) => {
  return axiosService.post(`${config.API_ENDPOINT_SERVER}/${uri}`, data)
}
export const getHistoryFullTest = (uri, data) => {
  return axiosService.post(`${config.API_ENDPOINT_SERVER}/${uri}`, data)
}
