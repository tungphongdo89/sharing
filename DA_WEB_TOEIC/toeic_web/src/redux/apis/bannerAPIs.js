import axiosService from '../service/AxiosService'
import * as config from '../../commons/constants/Config'

const uriGetBanner = "v1/banner/getBannersActive";
export const getBanner = () => {
  return axiosService.post(`${config.API_ENDPOINT_SERVER}/${uriGetBanner}` , {})
}

const uriCreateBanner = "v1/banner/create";
export const createBanner = (data) => {
  return axiosService.post(`${config.API_ENDPOINT_SERVER}/${uriCreateBanner}` , data)
}

const uriShowBannerActive = "v1/banner/getBannersActive";
export const showBannerActive = () => {
  return axiosService.post(`${config.API_ENDPOINT_SERVER}/${uriShowBannerActive}` , {})
}

const uriGetListBannerActive = "v1/banner/getBannersActive";
export const getListBannerActive = () => {
  return axiosService.get(`${config.API_ENDPOINT_SERVER}/${uriGetListBannerActive}` , {})
}

const uriGetListBannerNotActive = "v1/banner/getBannersNotActive";
export const getListBannerNotActive = (data) => {
  return axiosService.post(`${config.API_ENDPOINT_SERVER}/${uriGetListBannerNotActive}` , data)
}

const uriDeleteBanner = "v1/banner/delete";
export const deleteBanner = (data) => {
  return axiosService.post(`${config.API_ENDPOINT_SERVER}/${uriDeleteBanner}` , data)
}

const uriUpdateBanner = "v1/banner/update";
export const updateBanner = (data) => {
  return axiosService.post(`${config.API_ENDPOINT_SERVER}/${uriUpdateBanner}` , data)
}

const uriUpdateOrderBanner = "v1/banner/updateOrderBanner";
export const updateOrderBanner = (data) => {
  return axiosService.post(`${config.API_ENDPOINT_SERVER}/${uriUpdateOrderBanner}` , data)
}

