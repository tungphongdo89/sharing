import axiosService from '../service/AxiosService'
import * as config from '../../commons/constants/Config'

// lấy list question minitest
export const getListQuestionMinitest = (uri) =>{
  debugger
  return axiosService.post(`${config.API_ENDPOINT_SERVER}/${uri}`)
}

//submit answer minitest

// export const submitAnswerMinitest = (uri, data) =>{
//   debugger
//   return axiosService.post(`${config.API_ENDPOINT_SERVER}/${uri}`, data, {
//     headers: {
//       'content-type': 'multipart/form-data;boundary=----WebKitFormBoundaryyrV7KO0BoCBuDbTL',
//       'Accept': 'application/json'
//     }
//   })
// }

export const submitAnswerMinitest = (uri, data) =>{
  return axiosService.post(`${config.API_ENDPOINT_SERVER}/${uri}`, data)
}
export const getDetailFullTest = (uri,data)=>{
  return axiosService.post(`${config.API_ENDPOINT_SERVER}/${uri}`,data)
}
export const getResultFullTest = (uri,data)=>{
  return axiosService.post(`${config.API_ENDPOINT_SERVER}/${uri}`,data)
}

export const saveResultMinitest = (uri,data)=>{
  return axiosService.post(`${config.API_ENDPOINT_SERVER}/${uri}`,data)
}

export const getListHistoryMinitest = (uri,data)=>{
  return axiosService.post(`${config.API_ENDPOINT_SERVER}/${uri}`,data)
}

export const getListNameTest = (uri,data)=>{
  return axiosService.post(`${config.API_ENDPOINT_SERVER}/${uri}`,data)
}
