import axiosService from '../service/AxiosService'
import * as config from '../../commons/constants/Config'

const uriGetProfile = "v1/account/getDetail";
export const getProfile = () => {
  return axiosService.post(`${config.API_ENDPOINT_SERVER}/${uriGetProfile}` , {})
}

const uriUpdateProfile = "v1/account/update";
export const updateProfileAdmin = (data) => {
  return axiosService.post(`${config.API_ENDPOINT_SERVER}/${uriUpdateProfile}` , data , {
    headers: {
      'content-type': 'multipart/form-data;boundary=----WebKitFormBoundaryyrV7KO0BoCBuDbTL',
      'Accept': 'application/json'
    }
  })
}

const uriForgotPassword = "v1/account/resetPassword";
export const resetPassword = (email) => {
  return axiosService.post(`${config.API_ENDPOINT_SERVER}/${uriForgotPassword}`, email)
}

const uriSettingAccount = "v1/account/settingAccount";
export const settingAccount = (data) => {
  return axiosService.post(`${config.API_ENDPOINT_SERVER}/${uriSettingAccount}`, data , {
    headers: {
      'content-type': 'multipart/form-data;boundary=----WebKitFormBoundaryyrV7KO0BoCBuDbTL',
      'Accept': 'application/json'
    }
  })
}

