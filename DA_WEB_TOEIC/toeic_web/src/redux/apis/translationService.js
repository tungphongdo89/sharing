import axiosService from '../service/AxiosService'
import * as config from '../../commons/constants/Config'

//by topic
export const fetchQaTransAvByTopic = (uri,data) =>{
  return axiosService.post(`${config.API_ENDPOINT_SERVER}/${uri}`,data)
}

export const fetchQaTransVaByTopic = (uri,data) =>{
  return axiosService.post(`${config.API_ENDPOINT_SERVER}/${uri}`,data)
}

//by level
export const fetchQaTransAvByLevel = (uri,data) =>{
  return axiosService.post(`${config.API_ENDPOINT_SERVER}/${uri}`,data)
}

export const fetchQaTransVaByLevel = (uri,data) =>{
  return axiosService.post(`${config.API_ENDPOINT_SERVER}/${uri}`,data)
}
