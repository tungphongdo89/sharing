import axiosService from '../service/AxiosService'
import * as config from '../../commons/constants/Config'

export const getListMultiQuestion = (data,uri) => {
  return axiosService.post(`${config.API_ENDPOINT_SERVER}/${uri}`,data);
}

export const checkMultiListenQuest = (data,uri) =>{
  return axiosService.post(`${config.API_ENDPOINT_SERVER}/${uri}`,data);
}

export const getListQuestionOfReadingWordFill = (data,uri) => {
  return axiosService.post(`${config.API_ENDPOINT_SERVER}/${uri}`,data);
}

export const getResultQuestionOfReadingWordFill = (data,uri) => {
  return axiosService.post(`${config.API_ENDPOINT_SERVER}/${uri}`,data);
}

export const getListQuestionOfListeningWordFill = (data,uri) => {
  return axiosService.post(`${config.API_ENDPOINT_SERVER}/${uri}`,data);
}

export const getResultQuestionOfListeningWordFill = (data,uri) => {
  return axiosService.post(`${config.API_ENDPOINT_SERVER}/${uri}`,data);
}

export const getListQuestionOfReadingCompletedPassage = (data, uri) => {
  return axiosService.post(`${config.API_ENDPOINT_SERVER}/${uri}`,data);
}

export const getResultQuestionOfReadingCompletedPassage = (data, uri) => {
  return axiosService.post(`${config.API_ENDPOINT_SERVER}/${uri}`,data);
}

export const getListSingleAndDualReadingQuestion = (data,uri)=>{
  return axiosService.post(`${config.API_ENDPOINT_SERVER}/${uri}`,data);
}

export const fetchListeningToeicConverstation = (data,uri) => {
  debugger
  return axiosService.post(`${config.API_ENDPOINT_SERVER}/${uri}`,data);
}

//get detail history listening
export const getDetailHistoryListening = (uri,data)=>{
  return axiosService.post(`${config.API_ENDPOINT_SERVER}/${uri}`,data)
}

//get detail history lis single
export const getDetailHistoryLisSingle = (uri,data)=>{
  return axiosService.post(`${config.API_ENDPOINT_SERVER}/${uri}`,data)
}

export const submitSingleAndDualReadingQuestion = (data,uri)=>{
  return axiosService.post(`${config.API_ENDPOINT_SERVER}/${uri}`,data);
}

export const createHistoryListenFill = (data, uri)=>{
  return axiosService.post(`${config.API_ENDPOINT_SERVER}/${uri}`,data);
}

export const getHistoryListenFill = (data, uri) =>{
  return axiosService.post(`${config.API_ENDPOINT_SERVER}/${uri}`,data);
}

export const createHistoryReadingWordFill = (data, uri)=>{
  return axiosService.post(`${config.API_ENDPOINT_SERVER}/${uri}`,data);
}
export const createHistoryReadingCompletedPassage = (data, uri) => {
  return axiosService.post(`${config.API_ENDPOINT_SERVER}/${uri}`,data)
}
export const createHistoryReadingSingleAndDual = (data, uri) => {
  return axiosService.post(`${config.API_ENDPOINT_SERVER}/${uri}`,data)
}
export const getHistoryInCompleteSentences = (data, uri) =>{
  return axiosService.post(`${config.API_ENDPOINT_SERVER}/${uri}`,data);
}
export const getDetailHistoryCompletedPassage = (data, uri) =>{
  return axiosService.post(`${config.API_ENDPOINT_SERVER}/${uri}`,data);
}
export const getHistoryReadingSingleAndDual = (uri, data) => {
  return axiosService.post(`${config.API_ENDPOINT_SERVER}/${uri}`,data)
}
 
