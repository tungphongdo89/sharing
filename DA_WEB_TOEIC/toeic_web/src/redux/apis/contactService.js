import axiosService from '../service/AxiosService'
import * as config from '../../commons/constants/Config'
import STATUS_CODE from "../../commons/constants/STATUS_CODE"
import {put} from 'redux-saga/effects'
import contacAction from "../../redux/actions/contact"


export const contact = (data,uri) => {
  return axiosService.post(`${config.API_ENDPOINT_SERVER}/${uri}`,data);
}
