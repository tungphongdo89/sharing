import axiosService from '../service/AxiosService'
import * as config from '../../commons/constants/Config'

export const fetchTest = (uri, data) => {
  console.log(data);
  return axiosService.post(`${config.API_ENDPOINT_SERVER}/${uri}`, data)
}
export const fetchDataFilter = (uri, data) => {
  return axiosService.post(`${config.API_ENDPOINT_SERVER}/${uri}`, data)
}

export const createTest = (uri, data) => {
  return axiosService.post(`${config.API_ENDPOINT_SERVER}/${uri}`, data,{
    headers: {
      'content-type': 'multipart/form-data;boundary=----WebKitFormBoundaryyrV7KO0BoCBuDbTL',
      'Accept': 'application/json'
    }
  })
}

// export const createTest = (uri, data) => {
//   console.log("data send service : " , data)
//   return axiosService.post(`${config.API_ENDPOINT_SERVER}/${uri}`, data,{
//     headers: {
//       'content-type': 'multipart/form-data'
//     }
//   })
// }

export const updateTest = (uri, data) => {
  return axiosService.post(`${config.API_ENDPOINT_SERVER}/${uri}`, data)
}
export const deleteTest = (uri, data) => {
  return axiosService.post(`${config.API_ENDPOINT_SERVER}/${uri}`, data)
}
export const getListTypeExercise = (uri) => {
  return axiosService.post(`${config.API_ENDPOINT_SERVER}/${uri}`)
}
export const getListPartExercise = (uri,data) => {
  return axiosService.post(`${config.API_ENDPOINT_SERVER}/${uri}`,data)
}
export const getListTypeLevel = (uri) => {
  return axiosService.post(`${config.API_ENDPOINT_SERVER}/${uri}`)
}
export const getListTypeFileUpload = (uri) => {
  return axiosService.post(`${config.API_ENDPOINT_SERVER}/${uri}`)
}
export const getListTopicByPart = (uri,data) => {
  return axiosService.post(`${config.API_ENDPOINT_SERVER}/${uri}`,data)
}
export const getDetailTest = (uri,data) => {
  return axiosService.post(`${config.API_ENDPOINT_SERVER}/${uri}`,data)
}
export const getListCategory = (uri, data)=>{
  return axiosService.post(`${config.API_ENDPOINT_SERVER}/${uri}`,data)
}
export const getStudentFaildByTestId = (uri,data)=>{
  return axiosService.post(`${config.API_ENDPOINT_SERVER}/${uri}`,data)
}
