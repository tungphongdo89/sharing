import axiosService from '../service/AxiosService'
import * as config from '../../commons/constants/Config'


export const getStudentProfile = (data,uri) => {
  return axiosService.post(`${config.API_ENDPOINT_SERVER}/${uri}`,data);
}
export const updateStudentProfile = (data,uri) => {
  return axiosService.post(`${config.API_ENDPOINT_SERVER}/${uri}`,data);
}
