import axiosService from '../service/AxiosService'
import * as config from '../../commons/constants/Config'

export const getListHistoryPractices = (uri,data)=>{
  return axiosService.post(`${config.API_ENDPOINT_SERVER}/${uri}`,data)
}

export const getTypeForHistoryPractices = (uri)=>{
  return axiosService.post(`${config.API_ENDPOINT_SERVER}/${uri}`)
}

export const getPartForHistoryPractices = (uri, data)=>{
  return axiosService.post(`${config.API_ENDPOINT_SERVER}/${uri}`, data)
}

export const getTopicForHistoryPractices = (uri, data)=>{
  return axiosService.post(`${config.API_ENDPOINT_SERVER}/${uri}`, data)
}

export const saveResultHistoryListening = (uri,data)=>{
  return axiosService.post(`${config.API_ENDPOINT_SERVER}/${uri}`,data)
}

//save result history lis single
export const saveResultHistoryLisSingle = (uri,data)=>{
  return axiosService.post(`${config.API_ENDPOINT_SERVER}/${uri}`,data)
}
