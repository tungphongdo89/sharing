import axiosService from '../service/AxiosService'
import * as config from '../../commons/constants/Config'

export const login = (data, uri) => {
  return axiosService.post(`${config.API_ENDPOINT_SERVER}/${uri}`, data)
}

export const fecthUser = (uri, data) => {
  return axiosService.post(`${config.API_ENDPOINT_SERVER}/${uri}`, data)
}
export const registerAccount = (uri, data)=>{
  return axiosService.post(`${config.API_ENDPOINT_SERVER}/${uri}`, data)
}

export const changePassword = ( uri, data) => {
  return axiosService.post(`${config.API_ENDPOINT_SERVER}/${uri}`,data)
}

export const updateUserDetail = (uri,data) => {
  return axiosService.post(`${config.API_ENDPOINT_SERVER}/${uri}`, data)
}
export const resendEmail = (uri,data) => {
  return axiosService.post(`${config.API_ENDPOINT_SERVER}/${uri}`, data)
}

export const createUser = (uri, data) => {
  return axiosService.post(`${config.API_ENDPOINT_SERVER}/${uri}`, data)
}

export const sendMailInviteUser = (uri, data) => {
  return axiosService.post(`${config.API_ENDPOINT_SERVER}/${uri}`, data)
}
