import AxiosService from '../service/AxiosService';
import * as config from '../../commons/constants/Config';
export const createDictionary =(uri,data)=>{
    return AxiosService.post(`${config.API_ENDPOINT_SERVER}/${uri}`,data);
}
export const updateDictionary =(uri,data)=>{
    return AxiosService.post(`${config.API_ENDPOINT_SERVER}/${uri}`,data)
}
export const fetchDictionary =(uri,data)=>{
    return AxiosService.post(`${config.API_ENDPOINT_SERVER}/${uri}`,data)
}
export const getDictionaryById =(uri,data)=>{
    return AxiosService.get(`${config.API_ENDPOINT_SERVER}/${uri}/${data}`)
}
export const deleteDictionary=(uri,data)=>{
    return AxiosService.post(`${config.API_ENDPOINT_SERVER}/${uri}`,data)
}