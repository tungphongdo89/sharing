import axiosService from '../service/AxiosService'
import * as config from '../../commons/constants/Config'

export const fetchTopic = (uri,data) =>{
  return axiosService.post(`${config.API_ENDPOINT_SERVER}/${uri}`,data)
}
export const deletTopic = (uri,data) =>{
  return axiosService.post(`${config.API_ENDPOINT_SERVER}/${uri}`,data)
}
export const addTopic = (uri,data) =>{
  return axiosService.post(`${config.API_ENDPOINT_SERVER}/${uri}`,data)
}
export const updateTopic = (uri,data) =>{
  return axiosService.post(`${config.API_ENDPOINT_SERVER}/${uri}`,data)
}

export const fetchTopicByPart = (uri,data) =>{
  return axiosService.post(`${config.API_ENDPOINT_SERVER}/${uri}`,data)
}

export const fetchTopicByPartAndLevel = (uri,data) =>{
  return axiosService.post(`${config.API_ENDPOINT_SERVER}/${uri}`,data)
}
export const fetchListPractices = (uri) =>{
  return axiosService.post(`${config.API_ENDPOINT_SERVER}/${uri}`)
}

export const fetchTypeTopic = (uri) =>{
  return axiosService.post(`${config.API_ENDPOINT_SERVER}/${uri}`)
}
export const getListTypeTopic = (uri,data) =>{
  return axiosService.post(`${config.API_ENDPOINT_SERVER}/${uri}`,data)
}
export const getPartByTopic = (uri,data) =>{
  return axiosService.post(`${config.API_ENDPOINT_SERVER}/${uri}`,data)
}
export const fetchPartTopicByType = (uri,data) =>{
  return axiosService.post(`${config.API_ENDPOINT_SERVER}/${uri}`,data)
}

export const fetchTopicByTypeTopicAndPart = (uri,data) =>{
	  return axiosService.post(`${config.API_ENDPOINT_SERVER}/${uri}`,data)
}

export const getMenuListTest = (uri) =>{
  return axiosService.post(`${config.API_ENDPOINT_SERVER}/${uri}`)
}


