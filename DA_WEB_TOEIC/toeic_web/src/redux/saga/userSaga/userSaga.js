import {call, delay, fork, put, takeEvery} from "redux-saga/effects";
import * as config from "../../../commons/constants/Config";
import * as Api from "../../apis/userService";
import STATUS_CODE from '../../../commons/constants/STATUS_CODE';
import userAction from "../../actions/users/userActions";

function* startAction() {
  console.log('da vao auth saga');
}
function* fetchUser({payload}){
  const { data } = payload;
  const resp = yield call(Api.fecthUser, 'v1/users/getListAllUser',data);
  const {status: statusCode, data: dataResp} = resp;
  yield delay(500)
  if(STATUS_CODE.SUCCESS === statusCode) {
    yield put(userAction.fetchUserSuccess(dataResp));
  }
}

function* updateUserDetail({payload}) {
  const { data } = payload;
  const resp = yield call(Api.updateUserDetail,'v1/users/update',data);
  console.log(resp);
  const {status: statusCode, data: dataResp} = resp;
  yield delay(500);
  if(STATUS_CODE.SUCCESS === statusCode){
    yield put(userAction.updateUserDetailSuccess(dataResp));
  }
  else {
    yield put(userAction.updateUserDetailFailed(dataResp));
  }
}

function * createUser({payload}) {
  let respSendMailInviteUser = yield call(Api.sendMailInviteUser , "v1/users/inviteUser" , payload.data.userName)
  if(respSendMailInviteUser.status === STATUS_CODE.SUCCESS){
    let resp = yield call(Api.createUser , "v1/users/createUserInvited" , payload.data)
    if(resp.status === STATUS_CODE.SUCCESS){
      let payload = {};
      payload.data = {
        page:1,
        pageSize:10
      }
      yield * fetchUser({payload})
    }
  }
  else {
    yield put(userAction.createUserInvitedFailude())
  }
}

function* userSaga() {
  yield fork(startAction)
  yield takeEvery(config.UPDATE_USER_DETAIL, updateUserDetail)
  yield takeEvery(config.FETCH_USER,fetchUser)
  yield takeEvery(config.CREATE_USER_ADMIN_INVITED , createUser)
}

export default userSaga;
