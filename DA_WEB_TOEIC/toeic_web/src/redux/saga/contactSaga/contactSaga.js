import * as config from "../../../commons/constants/Config";
import {call, takeEvery,  delay, fork,put} from "redux-saga/effects";
import * as Api from "../../apis/contactService";
import STATUS_CODE from "../../../commons/constants/STATUS_CODE";
import contactACtion from "../../actions/contact";



 function * contactAction({payload}) {
  const {data} = payload;
  const resp = yield call(Api.contact, data, 'contact');
  const {status: statusCode, data: dataResp} = resp;
  yield delay(1000);
  if(STATUS_CODE.SUCCESS===statusCode){
    yield put(contactACtion.sendSuccess());
  }else{
    yield put(contactACtion.sendFailed());
  }
}


function* contactSaga() {
  yield takeEvery(config.CONTACT_ACTION,contactAction)
}

export default contactSaga;
