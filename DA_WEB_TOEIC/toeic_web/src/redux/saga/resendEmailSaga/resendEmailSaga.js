import {call, delay, fork, put, takeEvery} from "redux-saga/effects";
import * as config from "../../../commons/constants/Config";
import * as Api from "../../apis/userService";
import STATUS_CODE from '../../../commons/constants/STATUS_CODE';
import resendEmailAction from "../../actions/auth/resendEmailActions";

function* startAction() {
  console.log('da vao auth saga');
}

function* resendEmail({payload}){
  const { data } = payload;
  const resp = yield call(Api.resendEmail, 'reSendEmail', data);
  const {status: statusCode, data: dataResp} = resp;
  yield delay(500)
  if(STATUS_CODE.SUCCESS === statusCode){
    yield put(resendEmailAction.resendEmailSuccess(dataResp));
  }
  else {
    yield put(resendEmailAction.resendEmailFailed(dataResp));
  }
}

function* resendEmailSaga() {
  yield fork(startAction)
  yield takeEvery(config.RESEND_EMAIL, resendEmail)
}
export default resendEmailSaga;
