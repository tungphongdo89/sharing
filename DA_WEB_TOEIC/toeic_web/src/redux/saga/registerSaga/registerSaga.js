import {call, delay, put, takeEvery} from "redux-saga/effects";
import * as config from "../../../commons/constants/Config";
import STATUS_CODE from '../../../commons/constants/STATUS_CODE'
import * as Api from "../../apis/userService"
import {history} from "../../../history";
import registerActions from '../../actions/auth/registerActions';

function* registerAcount({payload}){
  const {data} = payload
  const resp = yield call(Api.registerAccount,'account/create',data)
  const {status: statusCode} = resp;
  yield delay(500)
  if(STATUS_CODE.SUCCESS === statusCode)
  {
    if(history)
    {
      history.push("/pages/login")
    }
  }else {
    yield  put(registerActions.registerFaild())
  }
}
function* registerSaga() {
  yield takeEvery(config.REGISTER_WITH_EMAIL,registerAcount);

}
export default registerSaga
