import {call, delay, put, takeEvery} from "redux-saga/effects";
import * as config from "../../../commons/constants/Config";
import STATUS_CODE from '../../../commons/constants/STATUS_CODE'
import * as Api from '../../apis/historyPracticesService'
import historyPracticesAction from '../../actions/history-practices'



function* getListHistoryPractices({payload}) {
  const {data} = payload;
  const resp = yield call(Api.getListHistoryPractices, 'v1/historyPractices/doSearch', data)
  const {status: statusCode, data: dataResp} = resp;
  delay(500)
  if (STATUS_CODE.SUCCESS === statusCode) {
    yield put(historyPracticesAction.getListHistoryPracticesSuccess(dataResp))
  } else {
    yield put(historyPracticesAction.getListHistoryPracticesFailed(dataResp))
  }
}

function* getTypeForHistoryPractices() {
  // const {data} = payload;
  const resp = yield call(Api.getTypeForHistoryPractices, 'v1/historyPractices/getTypeForHistoryPractices')
  const {status: statusCode, data: dataResp} = resp;
  delay(500)
  if (STATUS_CODE.SUCCESS === statusCode) {
    yield put(historyPracticesAction.getTypeForHistoryPracticesSuccess(dataResp))
  } else {
    yield put(historyPracticesAction.getTypeForHistoryPracticesFailed(dataResp))
  }
}

function* getPartForHistoryPractices({payload}) {
  const {data} = payload;
  const resp = yield call(Api.getPartForHistoryPractices, 'v1/historyPractices/getPartForHistoryPractices', data)
  const {status: statusCode, data: dataResp} = resp;
  delay(500)
  if (STATUS_CODE.SUCCESS === statusCode) {
    yield put(historyPracticesAction.getPartForHistoryPracticesSuccess(dataResp))
  } else {
    yield put(historyPracticesAction.getPartForHistoryPracticesFailed(dataResp))
  }
}

function* getTopicForHistoryPractices({payload}) {
  const {data} = payload;
  const resp = yield call(Api.getTopicForHistoryPractices, 'v1/historyPractices/getListTopicsForHistoryPractices', data)
  const {status: statusCode, data: dataResp} = resp;
  delay(500)
  if (STATUS_CODE.SUCCESS === statusCode) {
    yield put(historyPracticesAction.getTopicForHistoryPracticesSuccess(dataResp))
  } else {
    yield put(historyPracticesAction.getTopicForHistoryPracticesFailed(dataResp))
  }
}

function* saveResultHistoryListening({payload}) {
  const {data} = payload;
  const resp = yield call(Api.saveResultHistoryListening, 'v1/historyPractices/saveResultHistoryListening', data)
  const {status: statusCode, data: dataResp} = resp;
  delay(500)
  if (STATUS_CODE.SUCCESS === statusCode) {
    yield put(historyPracticesAction.saveResultHistoryListeningSuccess(dataResp))
  } else {
    yield put(historyPracticesAction.saveResultHistoryListeningFailed(dataResp))
  }
}

function* saveResultHistoryLisSingle({payload}) {
  const {data} = payload;
  const resp = yield call(Api.saveResultHistoryLisSingle, 'v1/historyPractices/saveResultHistoryLisSingle', data)
  const {status: statusCode, data: dataResp} = resp;
  delay(500)
  if (STATUS_CODE.SUCCESS === statusCode) {
    yield put(historyPracticesAction.saveResultHistoryLisSingleSuccess(dataResp))
  } else {
    yield put(historyPracticesAction.saveResultHistoryLisSingleFailed(dataResp))
  }
}



function* historyPracticesSaga() {
  yield takeEvery(config.GET_LIST_HISTORY_PRACTICES, getListHistoryPractices)
  yield takeEvery(config.GET_TYPE_FOR_HISTORY_PRACTICES, getTypeForHistoryPractices)
  yield takeEvery(config.GET_PART_FOR_HISTORY_PRACTICES, getPartForHistoryPractices)
  yield takeEvery(config.GET_TOPIC_FOR_HISTORY_PRACTICES, getTopicForHistoryPractices)
  yield takeEvery(config.SAVE_RESULT_HISTORY_LISTENING, saveResultHistoryListening)
  yield takeEvery(config.SAVE_RESULT_HISTORY_LIS_SINGLE, saveResultHistoryLisSingle)
}

export default historyPracticesSaga
