import { isPromise } from "formik";
import { put, takeEvery, call, delay, take, takeLatest } from "redux-saga/effects";
import *  as actionType from '../../../commons/constants/Config';
import dictionaryAction from '../../actions/dictionary/dictionaryAction';
import STATUS_CODE from '../../../commons/constants/STATUS_CODE';

import * as dictionaryService from '../../apis/dictionaryService';


function* createDictionary({ payload }) {
    let result = yield call(dictionaryService.createDictionary, 'v1/dictionaries/insert', payload.data)
    delay(500)
    if (result.status === 200) {
        yield put(dictionaryAction.updateDictionarySuccess(result.data))
        let dictionary = {};
        dictionary.keySearch = payload.keySearch;
        dictionary.page = 1;
        dictionary.pageSize = 10;
        yield put(dictionaryAction.fetchDictionary(dictionary))
    }else {
      yield put(dictionaryAction.createDictionaryFailed(result.data))
    }
}


function* updateDictionary({ payload }) {
    let result = yield call(dictionaryService.updateDictionary, 'v1/dictionaries/update', payload.data)
    delay(500)
    if (result.status === 200) {
        yield put(dictionaryAction.updateDictionarySuccess(result.data))
        let dictionary = {};
        dictionary.keySearch = payload.keySearch;
        dictionary.page = payload.page;
        dictionary.pageSize = 10;
        yield put(dictionaryAction.fetchDictionary(dictionary))
    } else {
        yield put(dictionaryAction.updateDictionaryFailed(result.data))
    }
}

function* fetchDictionary({ payload }) {
    let result = yield call(dictionaryService.fetchDictionary, 'v1/dictionaries/doSearch', payload.data)
    // const { status, data } = result;
    delay(500)
    if (result.status === 200) {
        yield put(dictionaryAction.fetchDictionarySuccess(result.data));
    } else {
        yield put(dictionaryAction.fetchDictionaryFailed(result.data));
    }
}

function* deleteDictionary({ payload }) {
    let result = yield call(dictionaryService.getDictionaryById, 'v1/dictionaries', payload.data)
    if (result.status === 200 && result.data !== null) {
        let res = yield call(dictionaryService.deleteDictionary, 'v1/dictionaries/delete', result.data)
        yield delay(500)
        if (res.status === 200) {
            yield put(dictionaryAction.deleteDictionarySuccess(res.status));
            let dictionary = {};
            dictionary.page = 1;
            dictionary.pageSize = 10;
            yield put(dictionaryAction.fetchDictionary(dictionary))
        } else {
            yield put(dictionaryAction.deleteDictionaryFailed(res))
        }

    }
}


function* dictionarySaga() {
    yield takeEvery(actionType.CREATE_DICTIONARY, createDictionary);
    yield takeEvery(actionType.FETCH_DICTIONARY, fetchDictionary);
    yield takeEvery(actionType.DELETE_DICTIONARY, deleteDictionary);
    yield takeEvery(actionType.UPDATE_DICTIONARY, updateDictionary);


}

export default dictionarySaga


