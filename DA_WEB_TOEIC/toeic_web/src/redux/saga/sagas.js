import {fork} from "redux-saga/effects";
import authSaga from "./authSaga/uathSaga";
import accountSaga from "./accountSaga/accountSaga"
import bannerSaga from "./bannerSaga/bannerSaga"

import registerAcount from "./registerSaga/registerSaga";
import contactSaga from "./contactSaga/contactSaga";
import changePasswordSaga from './authSaga/changePasswordSaga';
import studentAccountSaga from "./studentAccountSaga/studentAccountSaga";
import userSaga from "./userSaga/userSaga";
import resendEmailSaga from "./resendEmailSaga/resendEmailSaga";
import forgotPassword from "./forgotPasswordSaga/forgotPasswordSaga"
import topicSaga from './topicSaga/topicSaga';
import categorySaga from "./categorySaga/categorySaga";
import multiListeningSaga from "./practice/listening/multiListeningSaga";
import translationSaga from "./translationSaga/translationSaga";
import readingSaga from "./practice/reading/readingSaga";
import testSaga from "./testSaga/testSaga";
import doTestSaga from "./do-test-management/doTestSaga";
import dictionarySaga from './dictionarySaga/dictionarySaga';
import historyTestSaga from './historyTestSaga/historyTestSaga';
import wordSaga from './wordSaga/wordSaga';
import historyPracticesSaga from './historyPracticseSaga/historyPracticesSaga'

function* watchListTaskAction() {
  console.log('da vao day');

}

function* rootSaga() {
  yield fork(watchListTaskAction)
  yield fork(authSaga)
  yield fork(contactSaga)
  yield fork(studentAccountSaga);
  yield fork(accountSaga)
  yield fork(bannerSaga)
  yield fork(registerAcount)
  yield fork(changePasswordSaga)
  yield fork(userSaga)
  yield fork (resendEmailSaga)
  yield fork(forgotPassword)
  yield fork(topicSaga)
  yield fork(categorySaga)
  yield fork(multiListeningSaga)
  yield fork(translationSaga)
  yield fork(testSaga)
  yield fork(readingSaga)
  yield fork(doTestSaga)
  yield fork(dictionarySaga)
  yield fork(historyTestSaga)
  yield fork(wordSaga)
  yield fork(historyPracticesSaga)
}

export default rootSaga
