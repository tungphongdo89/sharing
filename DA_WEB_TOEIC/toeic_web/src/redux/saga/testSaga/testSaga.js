import {call, delay, put, takeEvery} from "redux-saga/effects";
import * as config from "../../../commons/constants/Config";
import * as Api from '../../apis/testService';
import STATUS_CODE from '../../../commons/constants/STATUS_CODE'
import testAction from '../../actions/test'
import { history } from "../../../history";

function* fetchDataFilter({payload}) {
  const {data} = payload;
  const resp = yield call(Api.fetchDataFilter,'v1/categories/getDataFilter',data);
  const {status: statusCode, data: dataResp} = resp;
  delay(500)
  if(STATUS_CODE.SUCCESS === statusCode)
  {
    yield put(testAction.fetchDataFilerSuccess(dataResp))
  }else {
    yield put(testAction.fetchDataFilerFaild(dataResp))
  }
}

function* fetchTest({payload}) {
  const {data} = payload;
  const resp = yield call(Api.fetchTest,'v1/tests/doSearch',data);
  const {status: statusCode, data: dataResp} = resp;
  if(STATUS_CODE.SUCCESS === statusCode)
  {
    yield put(testAction.fetchTestSuccess(dataResp))
  }else {
    yield put(testAction.fetchTestFaild(dataResp))
  }

}
function* createTest({payload}) {
  const {data} = payload;
  const resp = yield call(Api.createTest,'v1/tests/createTest',data);
  const {status: statusCode, data: dataResp} = resp;
  delay(500);
  if(STATUS_CODE.SUCCESS === statusCode)
  {
    yield put(testAction.createTestSuccess(dataResp))
    if(history){
      history.push("/app/test/list")
    }
  }else {
    yield put(testAction.createTestFailed(dataResp))
  }
}

function* updateTest({payload}) {
  const {data} = payload;
  const resp = yield call(Api.updateTest,'v1/categories/update',data);
  const {status: statusCode, data: dataResp} = resp;
  if(STATUS_CODE.SUCCESS === statusCode)
  {
    yield put(testAction.updateTestSuccess(dataResp))
    if(history){
      history.push("/app/test/list")
    }
  }else {
    yield put(testAction.updateTestFailed(dataResp))
  }
}
function* deleteTest({payload}) {
  const {data} = payload;
  const resp = yield call(Api.deleteTest,'v1/tests/delete',data);
  const {status: statusCode, data: dataResp} = resp;
  if(STATUS_CODE.SUCCESS === statusCode)
  {
    let category ={};
    category.page = 1;
    category.pageSize =10;
    yield put(testAction.deleteTestSuccess(dataResp))
    yield put(testAction.fetchTest(category))
  }else {
    yield put(testAction.deleteTestFaild(dataResp))
  }
}

function* getListTypeExercise() {
  const resp = yield call(Api.getListTypeExercise,'v1/topics/getListTypeExercise');
  const {status: statusCode, data: dataResp} = resp;
  if(STATUS_CODE.SUCCESS === statusCode)
  {
    yield put(testAction.getListTypeExerciseSuccess(dataResp))
  }else {
    yield put(testAction.getListTypeExerciseFailed(dataResp))
  }
}

function* getListPartExercise({payload}) {
  const {data} = payload;
  const resp = yield call(Api.getListPartExercise,'v1/topics/getListPartExercise',{paramValue: data });
  const {status: statusCode, data: dataResp} = resp;
  if(STATUS_CODE.SUCCESS === statusCode)
  {
    yield put(testAction.getListPartExerciseSuccess(dataResp))
  }else {
    yield put(testAction.getListPartExerciseFailed(dataResp))
  }
}
function* getListTypeLevel() {
  const resp = yield call(Api.getListTypeLevel,'v1/categories/getListTypeLevel');
  const {status: statusCode, data: dataResp} = resp;
  if(STATUS_CODE.SUCCESS === statusCode)
  {
    yield put(testAction.getListTypeLevelSuccess(dataResp))
  }else {
    yield put(testAction.getListTypeLevelFailed(dataResp))
  }
}
function* getListTypeFileUpload() {
  const resp = yield call(Api.getListTypeFileUpload,'v1/categories/getListTypeDataInput');
  const {status: statusCode, data: dataResp} = resp;
  if(STATUS_CODE.SUCCESS === statusCode)
  {
    yield put(testAction.getListTypeFileUploadSuccess(dataResp))
  }else {
    yield put(testAction.getListTypeFileUploadFailed(dataResp))
  }
}
function* getListTopicByPart({payload}) {
  const {data} = payload;
  const resp = yield call(Api.getListTopicByPart,'v1/topics/getListTopicByPart',data);
  const {status: statusCode, data: dataResp} = resp;
  if(STATUS_CODE.SUCCESS === statusCode)
  {
    yield put(testAction.getListTopicByPartSuccess(dataResp))
  }else {
    yield put(testAction.getListTopicByPartFailed(dataResp))
  }
}
function* getDetailTest({payload}) {
  const {data, check} = payload;
  const resp = yield call(Api.getDetailTest,'v1/tests/getDetail',{id : data.id});
  const {status: statusCode, data: dataResp} = resp;
  if(STATUS_CODE.SUCCESS === statusCode)
  {
    yield put(testAction.fetchDetailTestSuccess(dataResp, check))
  }else {
    yield put(testAction.fetchDetailTestFailed(dataResp, check))
  }
}
function* getListCategory({payload}) {
  const {data} = payload;
  const resp = yield call(Api.getListCategory,'v1/tests/getListCategoryTypeReadAndListen',data);
  const {status: statusCode, data: dataResp} = resp;
  if(STATUS_CODE.SUCCESS === statusCode)
  {
    yield put(testAction.getListCategorySuccess(dataResp))
  }else {
    yield put(testAction.getListCategoryFaild(dataResp))
  }
}

function* getStudentFaildByTestId({payload}){
  const {data} = payload;
  const resp = yield call(Api.getStudentFaildByTestId,'v1/testLog/getListFail',data);
  yield delay(1000);
  const {status: statusCode, data: dataResp} = resp;
  if(STATUS_CODE.SUCCESS === statusCode)
  {
    yield put(testAction.getStudentFaildByTestIdSuccess(dataResp))
  }else {
    yield put(testAction.getStudentFaildByTestIdFaild(dataResp))
  }
}

function* testSaga() {
  yield takeEvery(config.FETCH_TEST,fetchTest)
  yield takeEvery(config.CREATE_TEST, createTest)
  yield  takeEvery(config.UPDATE_TEST, updateTest)
  yield takeEvery(config.FETCH_DATA_FILTER, fetchDataFilter)
  yield takeEvery(config.DELETE_TEST, deleteTest)
  yield takeEvery(config.GET_LIST_TEST_TYPE_EXERCISE, getListTypeExercise)
  yield takeEvery(config.GET_LIST_TEST_PART_EXERCISE, getListPartExercise)
  yield takeEvery(config.GET_LIST_TEST_TYPE_LEVEL, getListTypeLevel)
  yield takeEvery(config.GET_LIST_TEST_TYPE_FILE_UPLOAD, getListTypeFileUpload)
  yield takeEvery(config.GET_LIST_TEST_TOPIC_BY_PART, getListTopicByPart)
  yield takeEvery(config.FETCH_DETAIL_TEST, getDetailTest)
  yield takeEvery(config.GET_LIST_CATEGORY, getListCategory)
  yield takeEvery(config.GET_LIST_STUDENT_FAILD_BY_TEST_ID,getStudentFaildByTestId)
}

export default testSaga;
