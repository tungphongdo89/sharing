import {call, put, takeEvery} from "redux-saga/effects";
import * as config from "../../../commons/constants/Config";
import STATUS_CODE from '../../../commons/constants/STATUS_CODE'
import * as Api from "../../apis/accountAPIs"
import {history} from "../../../history";
import * as forgotPassAction from '../../actions/email/index';

function* forgotPassword({payload}){
  let response = yield call(Api.resetPassword , payload.email)
  if(response.status === STATUS_CODE.SUCCESS)
  {
    yield put(forgotPassAction.sendEmailSuccess())
    history.push("/pages/login")
  }else {
    yield  put(forgotPassAction.sendEmailFailure())
    history.push("/pages/forgot-password");
  }
}
function* forgotPasswordSaga() {
  yield takeEvery(config.SEND_EMAIL,forgotPassword);

}
export default forgotPasswordSaga
