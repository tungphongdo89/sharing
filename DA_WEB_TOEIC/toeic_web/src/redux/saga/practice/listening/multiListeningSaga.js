import * as config from "../../../../commons/constants/Config";
import {call, takeEvery,  delay, fork,put} from "redux-saga/effects";
import * as Api from "../../../apis/practiceMultiQuestion";
import STATUS_CODE from "../../../../commons/constants/STATUS_CODE";
import practiceListenMultiActions from "../../../actions/practice/listening";
import translationAction from "../../../actions/translation-exercise/translationAction";
import historyTestAction from "../../../actions/history-test";
import {history} from "../../../../history";
import historyPracticesAction from "../../../actions/history-practices";


function * multiListeningAction({payload}) {
  const {data} = payload;
  const resp = yield call(Api.getListMultiQuestion, data, 'v1/questions/listening/getListQuestionByTopicId');
  const {status: statusCode, data: dataResp} = resp;
  yield delay(1000);
  if(STATUS_CODE.SUCCESS===statusCode){
    yield put(practiceListenMultiActions.getListListeningQuestionOfMultiSuccess(dataResp));
  }else{
    yield put(practiceListenMultiActions.getListListeningQuestionOfMultiFaild());
  }
}

function * checkMultiListenQuest({payload}) {
  const {data} = payload
  const resp = yield call(Api.checkMultiListenQuest,data,'v1/questions/listening/checkAnswermultiListeningQuestion')
  const {status: statusCode, data: dataResp} = resp;
  yield delay(1000);
  if(STATUS_CODE.SUCCESS===statusCode){
    yield put(practiceListenMultiActions.checkAnswerMultiListeningQuestionSuccess(dataResp));
  }else{
    yield put(practiceListenMultiActions.checkAnswerMultiListeningQuestionFaild());
  }
}

function * listeningWordFill({payload}) {
  const {data} = payload;
  const resp = yield call(Api.getListQuestionOfListeningWordFill, data, 'v1/questions/listening/getListQuestionOfListenAndFill');
  const {status: statusCode, data: dataResp} = resp;
  yield delay(1000);
  if(STATUS_CODE.SUCCESS===statusCode){
    yield put(practiceListenMultiActions.getListQuestionOfListeningWordFillSuccess(dataResp));
  }else{
    yield put(practiceListenMultiActions.getListQuestionOfListeningWordFillFail(dataResp));
  }
}

function * resultListeningWordFill({payload}) {
  const {data} = payload;
  const resp = yield call(Api.getResultQuestionOfListeningWordFill, data, 'v1/questions/listening/getResultQuestionOfListenWordFill');
  const {status: statusCode, data: dataResp} = resp;
  yield delay(1000);
  if(STATUS_CODE.SUCCESS===statusCode){
    yield put(practiceListenMultiActions.getResultQuestionOfListeningWordFillSuccess(dataResp));
  }else{
    yield put(practiceListenMultiActions.getResultQuestionOfListeningWordFillFail(dataResp));
  }
}

//listening toeic conversation
function* fetchListeningToeicConverstation({payload}) {
  const {data} = payload;
  const resp = yield call(Api.fetchListeningToeicConverstation, data,  'v1/questions/listening/getPracticeConversation')
  const {status: statusCode, data: dataResp} = resp;
  delay(500)
  if (STATUS_CODE.SUCCESS === statusCode) {
    yield put(practiceListenMultiActions.fetchListeningToeicConverstationSuccess(resp))
  } else {
    yield put(practiceListenMultiActions.fetchListeningToeicConverstationFailed(resp))
  }
}

//get detail history listening
function* getDetailHistoryListening({payload}) {
  const {data} = payload;
  const resp = yield call(Api.getDetailHistoryListening, 'v1/historyPractices/getDetailHistoryPracticesListening', data)
  const {status: statusCode, data: dataResp} = resp;
  delay(500)
  if (STATUS_CODE.SUCCESS === statusCode) {
    yield put(practiceListenMultiActions.getDetailHistoryListeningSuccess(dataResp))
  } else {
    yield put(practiceListenMultiActions.getDetailHistoryListeningFailed(dataResp))
  }
}

//get detail history lis single
function* getDetailHistoryLisSingle({payload}) {
  const {data} = payload;
  const resp = yield call(Api.getDetailHistoryLisSingle, 'v1/historyPractices/getDetailHistoryLisSingle', data)
  const {status: statusCode, data: dataResp} = resp;
  delay(500)
  if (STATUS_CODE.SUCCESS === statusCode) {
    yield put(practiceListenMultiActions.getDetailHistoryLisSingleSuccess(dataResp))
  } else {
    yield put(practiceListenMultiActions.getDetailHistoryLisSingleFailed(dataResp))
  }
}

function* createHistoryListenFill({payload}) {
  const {data} = payload;
  const resp = yield call(Api.createHistoryListenFill, data,  'v1/questions/listening/createHistoryListenFill')
  const {status: statusCode, data: dataResp} = resp;
  delay(500)
  if (STATUS_CODE.SUCCESS === statusCode) {
    yield put(practiceListenMultiActions.createHistoryListenFillSuccess(resp))
  } else {
    yield put(practiceListenMultiActions.createHistoryListenFillFailed(resp))
  }
}

function* getHistoryListenFill({payload}) {
  const {data} = payload;
  const resp = yield call(Api.getHistoryListenFill, data,  'v1/testLog/getDetailHistoryLF')
  const {status: statusCode, data: dataResp} = resp;
  delay(500)
  if(dataResp === undefined){
    yield put(historyTestAction.updateActiveTabWhenFail("2"))
    history.push("/pages/profiles")
  } else {
    if (STATUS_CODE.SUCCESS === statusCode) {
      yield put(practiceListenMultiActions.getHistoryListenFillSuccess(resp))
    } else {
      yield put(practiceListenMultiActions.getHistoryListenFillFailed(resp))
    }
  }
}

function* multiListeningSaga() {
  yield takeEvery(config.GET_LIST_LISTENING_QUESTIONS_OF_MULTI,multiListeningAction)
  yield takeEvery(config.CHECK_ANSWER_MULTI_LISTENING_QUESTION,checkMultiListenQuest)
  yield takeEvery(config.GET_LIST_QUESTION_OF_LISTENING_WORD_FILL,listeningWordFill)
  yield takeEvery(config.GET_RESULT_QUESTION_OF_LISTENING_WORD_FILL,resultListeningWordFill)
  yield takeEvery(config.FETCH_LISTENING_TOEIC_CONVERSATION,fetchListeningToeicConverstation)
  yield takeEvery(config.CREATE_HISTORY_LISTEN_FILL, createHistoryListenFill)
  yield takeEvery(config.GET_HISTORY_LISTEN_FILL, getHistoryListenFill)
  yield takeEvery(config.GET_DETAIL_HISTORY_LISTENING, getDetailHistoryListening)
  yield takeEvery(config.GET_DETAIL_HISTORY_LIS_SINGLE, getDetailHistoryLisSingle)
}

export default multiListeningSaga;
