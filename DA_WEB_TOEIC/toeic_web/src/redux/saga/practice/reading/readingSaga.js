import * as config from "../../../../commons/constants/Config";
import {call, takeEvery,  delay, fork,put} from "redux-saga/effects";
import * as Api from "../../../apis/practiceMultiQuestion";
import STATUS_CODE from "../../../../commons/constants/STATUS_CODE";
import readingActions from "../../../actions/practice/reading";
import practiceListenMultiActions from "../../../actions/practice/listening";


function * readingWordFill({payload}) {
  const {data} = payload;
  const resp = yield call(Api.getListQuestionOfReadingWordFill, data, 'v1/questions/reading/getListQuestionsOfReadWordFill');
  const {status: statusCode, data: dataResp} = resp;
  yield delay(1000);
  if(STATUS_CODE.SUCCESS===statusCode){
    yield put(readingActions.getListQuestionOfReadingWordFillSuccess(dataResp));
  }else{
    yield put(readingActions.getListQuestionOfReadingWordFillFail(dataResp));
  }
}

function * resultReadingWordFill({payload}) {
  const {data} = payload;
  const resp = yield call(Api.getResultQuestionOfReadingWordFill, data, 'v1/questions/reading/getResultQuestionOfReadWordFill');
  const {status: statusCode, data: dataResp} = resp;
  yield delay(1000);
  if(STATUS_CODE.SUCCESS===statusCode){
    yield put(readingActions.getResultQuestionOfReadingWordFillSuccess(dataResp));
  }else{
    yield put(readingActions.getResultQuestionOfReadingWordFillFail(dataResp));
  }
}

function * readingCompletedPassage({payload}) {
  const {data} = payload;
  const resp = yield call(Api.getListQuestionOfReadingCompletedPassage, data, 'v1/categories/getListQuestionOfReadingAndCompliting');
  const {status: statusCode, data: dataResp} = resp;
  yield delay(1000);
  if(STATUS_CODE.SUCCESS===statusCode){
    yield put(readingActions.getListQuestionOfReadingCompletedPassageSuccess(dataResp));
  }else{
    yield put(readingActions.getListQuestionOfReadingCompletedPassageFail(dataResp));
  }
}

function * resultReadingCompletedPassage({payload}) {
  const {data} = payload;
  const resp = yield call(Api.getResultQuestionOfReadingCompletedPassage, data, 'v1/categories/getResultQuestionOfReadingAndCompliting');
  const {status: statusCode, data: dataResp} = resp;
  yield delay(1000);
  if(STATUS_CODE.SUCCESS===statusCode){
    yield put(readingActions.getResultQuestionOfReadingCompletedPassageSuccess(dataResp));
  }else{
    yield put(readingActions.getResultQuestionOfReadingCompletedPassageFail(dataResp));
  }
}

function * getListSingleAndDualReadingQuestion({payload}){
  debugger
  const {data} = payload;
  const resp = yield call(Api.getListSingleAndDualReadingQuestion,data,'v1/questions/reading/getListQuestionSingleOrDual');
  const {status:statusCode,data:dataResp} = resp;
  yield delay(1000);
  if(STATUS_CODE.SUCCESS===statusCode){
    yield put(readingActions.getListSingleAndDualReadingQuestionSuccess(dataResp))
  }else{
    yield put(readingActions.getListSingleAndDualReadingQuestionFaild())
  }
}

function * submitSingleAndDualReadingQuestion({payload}){
  const {data} = payload;
  const resp = yield call(Api.submitSingleAndDualReadingQuestion,data,'v1/questions/reading/submitListQuestionSingleOrDual');
  const {status:statusCode,data:dataResp} = resp;
  yield delay(1000);
  if(STATUS_CODE.SUCCESS===statusCode){
    yield put(readingActions.submitSingleAndDualReadingQuestionSuccess(dataResp))
  }else{
    yield put(readingActions.submitSingleAndDualReadingQuestionFaild())
  }
}

function* createHistoryReadingWordFill({payload}) {
  const {data} = payload;
  const resp = yield call(Api.createHistoryReadingWordFill, data,  'v1/questions/reading/createHistoryReadingWordFill')
  const {status: statusCode, data: dataResp} = resp;
  delay(500)
  if (STATUS_CODE.SUCCESS === statusCode) {
    yield put(readingActions.createdHistoryReadingWordFillSuccess(resp))
  } else {
    yield put(readingActions.createdHistoryReadingWordFillFailed(resp))
  }
}

function* createHistoryReadingCompletedPassage({payload}) {
  const {data} = payload;
  const resp = yield call(Api.createHistoryReadingCompletedPassage, data,  'v1/categories/createHistoryReadingAndCompliting')
  const {status: statusCode, data: dataResp} = resp;
  delay(500)
  if (STATUS_CODE.SUCCESS === statusCode) {
    yield put(readingActions.createHistoryReadingCompletedPassageSuccess(resp))
  } else {
    yield put(readingActions.createHistoryReadingCompletedPassageFailed(resp))
  }
}

function* createHistoryReadingSingleDual({payload}) {
  const {data} = payload;
  const resp = yield call(Api.createHistoryReadingSingleAndDual, data, 'v1/categories/createHistoryReadingSingleAndDual')
  const {status: statusCode, data: dataResp} = resp;
  delay(500)
  if (STATUS_CODE.SUCCESS === statusCode) {
    yield put(readingActions.createHistoryReadingSingleDualSuccess(resp))
  } else {
    yield put(readingActions.createHistoryReadingSingleDualFailed(resp))
  }
} 

function* getHistoryInCompleteSentences({payload}) {
  const {data} = payload;
  const resp = yield call(Api.getHistoryInCompleteSentences, data, 'v1/testLog/getDetailHistoryReadingFill')
  const {status: statusCode, data: dataResp} = resp;
  delay(500)
  if (STATUS_CODE.SUCCESS === statusCode) {
    yield put(readingActions.getHistoryInCompleteSentencesSuccess(resp))
  } else {
    yield put(readingActions.getHistoryInCompleteSentencesFailed(resp))
  }
}

function* getDetailHistoryReadingCompletedPassage({payload}) {
  const {data} = payload;
  const resp = yield call(Api.getDetailHistoryCompletedPassage, data, 'v1/testLog/getDetailHistoryReadingCompletedPassage')
  const {status: statusCode, data: dataResp} = resp;
  delay(500)
  if (STATUS_CODE.SUCCESS === statusCode) {
    yield put(readingActions.getDetailHistoryReadingCompletedPassageSuccess(resp))
  } else {
    yield put(readingActions.getDetailHistoryReadingCompletedPassageFailed(resp))
  }
}
 
//get detail history reading singleDual
function* getDetailHistoryReadingSingleDual({payload}) {
  debugger
  const {data} = payload;
  const resp = yield call(Api.getHistoryReadingSingleAndDual, 'v1/historyPractices/getDetailHistoryReadingSingle', data)
  const {status: statusCode, data: dataResp} = resp;
  delay(500)
  if (STATUS_CODE.SUCCESS === statusCode) {
    yield put(readingActions.getDetailHistoryReadingSingleSuccess(dataResp))
  } else {
    yield put(readingActions.getDetailHistoryReadingSingleFailed(dataResp))
  }
}

function* readingSaga() {
  yield takeEvery(config.GET_LIST_QUESTION_OF_READING_WORD_FILL,readingWordFill)
  yield takeEvery(config.GET_RESULT_QUESTION_OF_READING_WORD_FILL,resultReadingWordFill)
  yield takeEvery(config.GET_LIST_QUESTION_OF_READING_COMPLETED_PASSAGE,readingCompletedPassage)
  yield takeEvery(config.GET_RESULT_QUESTION_OF_READING_COMPLETED_PASSAGE,resultReadingCompletedPassage)
  yield takeEvery(config.GET_LIST_SINGLE_AND_DUAL_READING_QUESTION,getListSingleAndDualReadingQuestion)
  yield takeEvery(config.SUBMIT_SINGLE_AND_DUAL_READING_QUESTION,submitSingleAndDualReadingQuestion)
  yield takeEvery(config.CREATE_HISTORY_READING_WORD_FILL,createHistoryReadingWordFill)
  yield takeEvery(config.CREATE_HISTORY_READING_COMPLETED_PASSAGE,createHistoryReadingCompletedPassage)
  yield takeEvery(config.CREATE_HISTORY_READING_SINGLE_DUAL,createHistoryReadingSingleDual)
  yield takeEvery(config.GET_HISTORY_READING_INCOMPLETE_SENTENCES,getHistoryInCompleteSentences)
  yield takeEvery(config.GET_DETAIL_HISTORY_READING_COMPLETED_PASSAGE,getDetailHistoryReadingCompletedPassage)
  yield takeEvery(config.GET_DETAIL_HISTORY_READING_SINGLEDUAL,getDetailHistoryReadingSingleDual)
}


export default readingSaga;
