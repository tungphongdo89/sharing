import {takeEvery, fork, call, put, delay} from "redux-saga/effects";
import * as config from "../../../commons/constants/Config"
import * as Api from "../../apis/studentAccountAPIs";
import studentProfileAction from "../../actions/studentAccount";
import STATUS_CODE from "../../../commons/constants/STATUS_CODE";


function* getStudentProfile() {
  const resp = yield call(Api.getStudentProfile, {},"v1/account/getDetail");
  console.log("studentProfile:",resp);
  const {status: statusCode, data: dataResp} = resp;
  yield delay(500);
  if (statusCode === STATUS_CODE.SUCCESS) {
    yield put(studentProfileAction.getStudentProfileSuccess(dataResp));
  }else{
    yield put(studentProfileAction.getStudentProfileFailure(resp));
  }
}

function* updateStudentProfile({payload}) {
  let resp = yield call(Api.updateStudentProfile ,payload.data, "v1/account/update")
  yield delay(500);
  if(STATUS_CODE.SUCCESS === resp.status){
    let resGetProfile = yield call(Api.getStudentProfile, payload.data,"v1/account/getDetail");
    if(STATUS_CODE.SUCCESS === resGetProfile.status){
      yield put(studentProfileAction.updateStudentProfileSuccess(resGetProfile.data))
    }
    else{
      yield put(studentProfileAction.updateStudentProfileFailure())
    }
  }
  else {
    yield put(studentProfileAction.updateStudentProfileFailure())
  }
}

function* studentAccountSaga() {
  yield takeEvery(config.GET_STUDENT_PROFILE, getStudentProfile);
  yield takeEvery(config.UPDATE_STUDENT_PROFILE, updateStudentProfile);
}

export default studentAccountSaga;
