import {call, delay, fork, put, takeEvery} from "redux-saga/effects";
import * as config from "../../../commons/constants/Config";
import * as Api from "../../apis/userService";
import STATUS_CODE from '../../../commons/constants/STATUS_CODE';
import changePasswordAction from "../../actions/auth/changePasswordActions";
import { history } from "../../../history";
import { toast } from "react-toastify";

function* startAction() {
    console.log('da vao auth saga');
  }

function* changePassword({payload}){
    const { data } = payload;
    const resp = yield call(Api.changePassword, 'v1/account/changePassword', data);
    const {status: statusCode, data: dataResp} = resp;
    yield delay(500)
    if(STATUS_CODE.SUCCESS === statusCode){
        yield put(changePasswordAction.changePasswordSuccess(dataResp));
        if(history){
          if(data.userRole === "STUDENT"){
            history.push("/")
          } else {
            history.push("/private/admin/")
          }
        }
    }
    else {
        yield put(changePasswordAction.changePasswordFailed(dataResp));
    }
}

function* changePasswordSaga() {
    yield fork(startAction)
    yield takeEvery(config.CHANGE_PASSWORD, changePassword)
}

export default changePasswordSaga;
