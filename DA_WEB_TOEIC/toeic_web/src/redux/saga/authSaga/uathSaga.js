import {call, delay, fork, put, takeEvery} from "redux-saga/effects";
import * as config from "../../../commons/constants/Config";
import {history} from "../../../history";
import * as Api from "../../apis/userService"
import STATUS_CODE from '../../../commons/constants/STATUS_CODE'
import loginActions from '../../../redux/actions/auth/loginActions'
import {ROLE_ADMIN, ROLE_PREVIEW, ROLE_STUDENT, ROLE_TEACHER} from "../../../commons/constants/CONSTANTS";
import {toast} from "react-toastify"

function* startAction() {
  console.log('da vao auth saga');
}

function* loginUser({payload}) {
  const {data} = payload
  const resp = yield call(Api.login, data, 'login');
  const {status: statusCode, data: dataResp} = resp;
  yield delay(1000)
  console.log(resp);
  if (STATUS_CODE.SUCCESS === statusCode) {
    if(dataResp.userDTO.status === 1){
      yield put(loginActions.loginSuccess(dataResp))
      if (history) {
        if (dataResp.userDTO.lstRole[0].roleCode === ROLE_ADMIN || dataResp.userDTO.lstRole[0].roleCode === ROLE_PREVIEW) {
          history.push("/private/admin/")
        } else if (dataResp.userDTO.lstRole[0].roleCode === ROLE_STUDENT) {
          history.push("/")
          // history.push("/student/a")
        } else if (dataResp.userDTO.lstRole[0].roleCode === ROLE_TEACHER) {
          history.push("/private/teacher/")
          // history.push("/teacher/b")
        }
      }
    }
    else if(dataResp.userDTO.status === 0){
      toast.error("Tài khoản đã bị khóa!")
      history.push("/pages/login")
      yield put(loginActions.loginFalure(resp))
    }
    else if(dataResp.userDTO.status === -1){
      toast.error("Tài khoản chưa được kích hoạt")
      history.push("/pages/login")
      yield put(loginActions.loginFalure(resp))
    }
    else {
      toast.error("Tài khoản không có quyền truy cập")
      history.push("/pages/login")
      yield put(loginActions.loginFalure(resp))
    }
  }
  else {
    history.push("/pages/login")
    yield put(loginActions.loginFalure(resp))
  }
}


function* authSaga() {
  yield fork(startAction)
  yield takeEvery(config.LOGIN_USER_REQUEST, loginUser)
}

export default authSaga
