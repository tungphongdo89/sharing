import {call, delay, put, takeEvery} from "redux-saga/effects";
import * as config from "../../../commons/constants/Config";
import STATUS_CODE from '../../../commons/constants/STATUS_CODE'
import * as Api from '../../apis/translationService'
import translationAction from '../../actions/translation-exercise/translationAction'


//by topic
function* fetchQaTransAvByTopic({payload}) {
  const {data} = payload
  const resp = yield call(Api.fetchQaTransAvByTopic, 'v1/questions/translationAV/getListQaTransAVByTopic', data)
  const {status: statusCode, data: dataResp} = resp;
  delay(500)
  if (STATUS_CODE.SUCCESS === statusCode) {
    yield put(translationAction.fetchQaTransAvByTopicSuccess(dataResp))
  } else {
    yield put(translationAction.fetchQaTransAvByTopicFailed(dataResp))
  }
}

function* fetchQaTransVaByTopic({payload}) {
  const {data} = payload
  const resp = yield call(Api.fetchQaTransVaByTopic, 'v1/questions/translationVA/getListQaTransVAByTopic', data)
  const {status: statusCode, data: dataResp} = resp;
  delay(500)
  if (STATUS_CODE.SUCCESS === statusCode) {
    yield put(translationAction.fetchQaTransVaByTopicSuccess(dataResp))
  } else {
    yield put(translationAction.fetchQaTransVaByTopicFailed(dataResp))
  }
}

//by level
function* fetchQaTransAvByLevel({payload}) {
  const {data} = payload
  const resp = yield call(Api.fetchQaTransAvByLevel, 'v1/questions/translationAV/getListQaTransAVByLevel', data)
  const {status: statusCode, data: dataResp} = resp;
  delay(500)
  if (STATUS_CODE.SUCCESS === statusCode) {
    yield put(translationAction.fetchQaTransAvByLevelSuccess(dataResp))
  } else {
    yield put(translationAction.fetchQaTransAvByLevelFailed(dataResp))
  }
}

function* fetchQaTransVaByLevel({payload}) {
  const {data} = payload
  const resp = yield call(Api.fetchQaTransVaByLevel, 'v1/questions/translationVA/getListQaTransVAByLevel', data)
  const {status: statusCode, data: dataResp} = resp;
  delay(500)
  if (STATUS_CODE.SUCCESS === statusCode) {
    yield put(translationAction.fetchQaTransVaByLevelSuccess(dataResp))
  } else {
    yield put(translationAction.fetchQaTransVaByLevelFailed(dataResp))
  }
}

function* translationSaga() {
  //by topic
  yield takeEvery(config.FETCH_QA_TRANS_AV_BY_TOPIC, fetchQaTransAvByTopic)
  yield takeEvery(config.FETCH_QA_TRANS_VA_BY_TOPIC, fetchQaTransVaByTopic)

  //by level
  yield takeEvery(config.FETCH_QA_TRANS_AV_BY_LEVEL, fetchQaTransAvByLevel)
  yield takeEvery(config.FETCH_QA_TRANS_VA_BY_LEVEL, fetchQaTransVaByLevel)
}

export default translationSaga
