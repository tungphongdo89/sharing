import {call, delay, put, takeEvery} from "redux-saga/effects";
import * as config from "../../../commons/constants/Config";
import * as Api from '../../apis/categoryService';
import STATUS_CODE from '../../../commons/constants/STATUS_CODE'
import categoryAction from '../../actions/category-action/index'
import {history} from "../../../history";

function* fetchDataFilter({payload}) {
  const {data} = payload;
  const resp = yield call(Api.fetchDataFilter, 'v1/categories/getDataFilter', data);
  const {status: statusCode, data: dataResp} = resp;
  delay(500)
  if (STATUS_CODE.SUCCESS === statusCode) {
    yield put(categoryAction.fetchDataFilerSuccess(dataResp))
  } else {
    yield put(categoryAction.fetchDataFilerFaild(dataResp))
  }
}

function* fetchCategory({payload}) {
  const {data} = payload;
  const resp = yield call(Api.fetchCategory, 'v1/categories/doSearch', data);
  const {status: statusCode, data: dataResp} = resp;
  delay(500)
  if (STATUS_CODE.SUCCESS === statusCode) {
    yield put(categoryAction.fetchCategorySuccess(dataResp))
  } else {
    yield put(categoryAction.fetchCategoryFaild(dataResp))
  }

}

function* createCategory({payload}) {
  const {data} = payload;
  const resp = yield call(Api.createCategory, 'v1/categories/create', data);
  const {status: statusCode, data: dataResp} = resp;
  if (STATUS_CODE.SUCCESS === statusCode) {
    yield put(categoryAction.createCategorySuccess(dataResp))
    if (history) {
      history.push("/app/category/list")
    }
  } else {
    yield put(categoryAction.createCategoryFailed(dataResp))
  }
}

function* updateCategory({payload}) {
  const {data} = payload;
  const resp = yield call(Api.updateCategory, 'v1/categories/update', data);
  const {status: statusCode, data: dataResp} = resp;
  if (STATUS_CODE.SUCCESS === statusCode) {
    yield put(categoryAction.updateCategorySuccess(dataResp))
    if (history) {
      history.push("/app/category/list")
    }
  } else {
    yield put(categoryAction.updateCategoryFailed(dataResp))
  }
}

function* deleteCategory({payload}) {
  const {data} = payload;
  const resp = yield call(Api.deleteCategory, 'v1/categories/delete', data);
  const {status: statusCode, data: dataResp} = resp;
  delay(2000)
  if (STATUS_CODE.SUCCESS === statusCode) {
    let category = {};
    category.page = 1;
    category.pageSize = 10;
    yield put(categoryAction.deleteCategorySuccess(dataResp))
    yield put(categoryAction.fetchCategory(category))
  } else {
    yield put(categoryAction.deleteCategoryFaild(dataResp))
  }
}

function* getListTypeExercise() {
  const resp = yield call(Api.getListTypeExercise, 'v1/topics/getListTypeExercise');
  const {status: statusCode, data: dataResp} = resp;
  if (STATUS_CODE.SUCCESS === statusCode) {
    yield put(categoryAction.getListTypeExerciseSuccess(dataResp))
  } else {
    yield put(categoryAction.getListTypeExerciseFailed(dataResp))
  }
}

function* getListPartExercise({payload}) {
  const {data} = payload;
  const resp = yield call(Api.getListPartExercise, 'v1/topics/getListPartExercise', {paramValue: data});
  const {status: statusCode, data: dataResp} = resp;
  if (STATUS_CODE.SUCCESS === statusCode) {
    yield put(categoryAction.getListPartExerciseSuccess(dataResp))
  } else {
    yield put(categoryAction.getListPartExerciseFailed(dataResp))
  }
}

function* getListTypeLevel() {
  const resp = yield call(Api.getListTypeLevel, 'v1/categories/getListTypeLevel');
  const {status: statusCode, data: dataResp} = resp;
  if (STATUS_CODE.SUCCESS === statusCode) {
    yield put(categoryAction.getListTypeLevelSuccess(dataResp))
  } else {
    yield put(categoryAction.getListTypeLevelFailed(dataResp))
  }
}

function* getListTypeFileUpload() {
  const resp = yield call(Api.getListTypeFileUpload, 'v1/categories/getListTypeDataInput');
  const {status: statusCode, data: dataResp} = resp;
  if (STATUS_CODE.SUCCESS === statusCode) {
    yield put(categoryAction.getListTypeFileUploadSuccess(dataResp))
  } else {
    yield put(categoryAction.getListTypeFileUploadFailed(dataResp))
  }
}

function* getListTopicByPart({payload}) {
  const {data} = payload;
  const resp = yield call(Api.getListTopicByPart, 'v1/topics/getListTopicByPart', data);
  const {status: statusCode, data: dataResp} = resp;
  if (STATUS_CODE.SUCCESS === statusCode) {
    yield put(categoryAction.getListTopicByPartSuccess(dataResp))
  } else {
    yield put(categoryAction.getListTopicByPartFailed(dataResp))
  }
}

function* getDetailCategory({payload}) {
  const {data, check} = payload;
  const resp = yield call(Api.getDetailCategory, 'v1/categories/getDetail', {categoryId: data.categoryId});
  const {status: statusCode, data: dataResp} = resp;
  if (STATUS_CODE.SUCCESS === statusCode) {
    yield put(categoryAction.fetchDetailCategorySuccess(dataResp, check))
  } else {
    yield put(categoryAction.fetchDetailCategoryFailed(dataResp, check))
  }
}

function* categorySaga() {
  yield takeEvery(config.FETCH_CATEGORY, fetchCategory)
  yield takeEvery(config.CREATE_CATEGORY, createCategory)
  yield  takeEvery(config.UPDATE_CATEGORY, updateCategory)
  yield takeEvery(config.FETCH_DATA_FILTER, fetchDataFilter)
  yield takeEvery(config.DELETE_CATEGORY, deleteCategory)
  yield takeEvery(config.GET_LIST_TYPE_EXERCISE, getListTypeExercise)
  yield takeEvery(config.GET_LIST_PART_EXERCISE, getListPartExercise)
  yield takeEvery(config.GET_LIST_TYPE_LEVEL, getListTypeLevel)
  yield takeEvery(config.GET_LIST_TYPE_FILE_UPLOAD, getListTypeFileUpload)
  yield takeEvery(config.GET_LIST_TOPIC_BY_PART, getListTopicByPart)
  yield takeEvery(config.FETCH_DETAIL_CATEGORY, getDetailCategory)
}

export default categorySaga;
