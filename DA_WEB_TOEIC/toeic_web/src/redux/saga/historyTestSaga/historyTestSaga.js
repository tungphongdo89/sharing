import {call, delay, put, takeEvery} from "redux-saga/effects";
import * as config from "../../../commons/constants/Config";
import * as Api from '../../apis/historyTestService';
import STATUS_CODE from '../../../commons/constants/STATUS_CODE'
import historyTestAction from '../../actions/history-test'
import {history} from './../../../history';

function* fetchHistoryTest({payload}) {
  const {data} = payload;
  const resp = yield call(Api.fetchHistoryTest,'v1/testLog/doSearch',data);
  const {status: statusCode, data: dataResp} = resp;
  if(STATUS_CODE.SUCCESS === statusCode)
  {
    yield put(historyTestAction.fetchHistoryTestSuccess(dataResp))
  }else {
    yield put(historyTestAction.fetchHistoryTestFailed(dataResp))
  }
}

function* getDetailOfTest({payload}) {
  const {data} = payload;
  const resp = yield call(Api.getDetailOfTest,'v1/testLog/getDetailHistoryTest',data);
  const {status: statusCode, data: dataResp} = resp;
  if(STATUS_CODE.SUCCESS === statusCode)
  {
    yield put(historyTestAction.getDetailOfTestSuccess(dataResp))
  }else {
    yield put(historyTestAction.getDetailOfTestFailed(dataResp))
  }
}

function* getRankOfTest({payload}) {
  const {data} = payload;
  const resp = yield call(Api.getRankOfTest,'v1/testLog/getListRankOfTest',data);
  const {status: statusCode, data: dataResp} = resp;
  if(STATUS_CODE.SUCCESS === statusCode)
  {
    yield put(historyTestAction.getRankOfTestSuccess(dataResp))
  }else {
    yield put(historyTestAction.getRankOfTestFailed(dataResp))
  }
}

function* getHistoryFullTest({payload}) {
  const {data} = payload;
  const resp = yield call(Api.getHistoryFullTest,'v1/testLog/getDetailHistoryFullTest',data);
  const {status: statusCode, data: dataResp} = resp;
  if(dataResp === undefined){
    yield put(historyTestAction.updateActiveTabWhenFail("2"))
    history.push("/pages/profiles")
  } else {
    if(STATUS_CODE.SUCCESS === statusCode)
    {
      yield put(historyTestAction.getHistoryFullTestSuccess(dataResp))
    }else {
      yield put(historyTestAction.getHistoryFullTestFailed(dataResp))
    }
  }

}

function* historyTestSaga() {
  yield takeEvery(config.FETCH_HISTORY_TEST,fetchHistoryTest)
  yield takeEvery(config.GET_DETAIL_TEST,getDetailOfTest)
  yield takeEvery(config.GET_RANK_TEST,getRankOfTest)
  yield takeEvery(config.GET_HISTORY_FULL_TEST,getHistoryFullTest)

}

export default historyTestSaga;
