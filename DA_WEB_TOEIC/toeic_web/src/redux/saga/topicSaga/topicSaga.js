import {call, delay, put, takeEvery} from "redux-saga/effects";
import * as config from "../../../commons/constants/Config";
import STATUS_CODE from '../../../commons/constants/STATUS_CODE'
import * as Api from '../../apis/topicService'
import topicAction from '../../actions/topic/topicAction'


function* fetchTopic({payload}) {
  const {data} = payload
  const resp = yield call(Api.fetchTopic, 'v1/topics/doSearch', data)
  const {status: statusCode, data: dataResp} = resp;
  console.log("xxxxxxxxxxxxxxxxx",data);
  delay(500)
  if (STATUS_CODE.SUCCESS === statusCode) {
    yield put(topicAction.fetchTopicSuccess(dataResp))
  } else {
    yield put(topicAction.fetchTopicFaild(dataResp))
  }
}

function* deleteTopic({payload}) {
  const {data} = payload;
  const resp = yield call(Api.deletTopic, 'v1/topics/delete', data)
  const {status: statusCode} = resp;
  delay(500)
  if (STATUS_CODE.SUCCESS === statusCode) {
    yield put(topicAction.deleteTopicSuccess(resp))
    let topic = {};
    topic.page = 1;
    topic.pageSize = 10;
  yield put(topicAction.fetchTopic(topic))
  } else {
    yield put(topicAction.deleteTopicFaild(resp))
  }
}

function* addTopic({payload}) {
  const {data} = payload
  const resp = yield call(Api.addTopic, 'v1/topics/create', data)
  const {status: statusCode} = resp;
  delay(500)
  if (STATUS_CODE.SUCCESS === statusCode) {
     yield put(topicAction.addTopicSuccess(resp))
     let topic = {};
     topic.page = 1;
     topic.pageSize = 10;
	 yield put(topicAction.fetchTopic(topic))
  } else {
    yield put(topicAction.addTopicFaild(resp))
  }

}

function* updateTopic({payload}) {
  console.log("test :  ",payload);
  const {data} = payload
  const resp = yield call(Api.addTopic, 'v1/topics/update', data)
  const {status: statusCode} = resp;
  delay(500)
  if (STATUS_CODE.SUCCESS === statusCode) {
    yield put(topicAction.updateTopicSuccess(resp))
    let topic = {};
    topic.page = 1;
    topic.pageSize = 10;
    yield put(topicAction.fetchTopic(topic))
  } else {
    yield put(topicAction.updateTopicFaild(resp))
  }
}

function* fetchTopicByPart({payload}) {
  const resp = yield call(Api.fetchTopicByPart, 'v1/topics/getListTopicByPart', payload.part)
  if (resp.status === STATUS_CODE.SUCCESS) {
    yield put(topicAction.fetchTopicByPartSuccess(resp.data))
  } else {
    yield put(topicAction.fetchTopicByPartFailude())
  }
}

function * fetchTopicByPartAndLevel({payload}) {
  const {data} = payload;
  const resp = yield call(Api.fetchTopicByPartAndLevel,'v1/topics/getListTopicByPartAndLevel',data)
  if(resp.status === STATUS_CODE.SUCCESS){
    yield put(topicAction.fetchTopicByPartAndLevelSuccess(resp.data))
  }
  else {
    yield put(topicAction.fetchTopicByPartAndLevelFail())
  }
}

function* fetchListPractices() {
  const resp = yield call(Api.fetchListPractices, 'v1/topics/getListPractices')
  if (resp.status === STATUS_CODE.SUCCESS) {
    yield put(topicAction.fetchListPracticesSuccess(resp.data))
  } else {
    yield put(topicAction.fetchTopicByPartFailude())
  }
}

function* fetchGetTypeTopic()
{
  const resp = yield call(Api.getListTypeTopic, 'v1/data/getTypeTopic',{})
  const {status: statusCode, data: dataResp} = resp;
  if (statusCode === STATUS_CODE.SUCCESS) {
    yield put(topicAction.fetchTypeTopicSuccess(dataResp))
  }
}

function* fetchGetPartByTypeTopic({payload})
{
  const {data} = payload;
  const resp = yield call(Api.getPartByTopic, 'v1/data/getPartByTopic',data)
  const {status: statusCode, data: dataResp} = resp;
  if (statusCode === STATUS_CODE.SUCCESS) {
    yield put(topicAction.fetchPartByTopicSuccess(dataResp))
  }
}

// function * fetchPartTopicByType({payload}) {
//   const resp = yield call(Api.fetchPartTopicByType,'v1/topics/getListPartExercise',payload.type)
//   if(resp.status === STATUS_CODE.SUCCESS){
// 	 yield put(topicAction.fetchPartTopicByTypeSuccess(resp.data))
//   }
//   else {
// 	 yield put(topicAction.fetchPartTopicByTypeFailed())
//   }
// }

function* fetchTopicByTypeTopicAndPart({payload})
{
  const {data} = payload;
  const resp = yield call(Api.fetchTopicByTypeTopicAndPart, 'v1/data/getTopicByTypeTopicAndPart',data)
  const {status: statusCode, data: dataResp} = resp;
  delay(500)
  if (statusCode === STATUS_CODE.SUCCESS) {
    yield put(topicAction.fetchTopicNameByPartAndTypeTopicSuccess(dataResp))
  }
}

function* getMenuListTest()
{
  const resp = yield call(Api.getMenuListTest, 'v1/tests/getMenuListTest')
  const {status: statusCode, data: dataResp} = resp;
  delay(500)
  if (statusCode === STATUS_CODE.SUCCESS) {
    yield put(topicAction.getMenuListTestSuccess(dataResp))
  }
  else {
    yield put(topicAction.getMenuListTestFailed())
  }
}


function* topicSaga() {
  yield takeEvery(config.FETCH_TOPIC, fetchTopic)
  yield takeEvery(config.DELETE_TOPIC, deleteTopic)
  yield takeEvery(config.UPDATE_TOPIC, updateTopic)
  yield takeEvery(config.ADD_TOPIC, addTopic)
  yield takeEvery(config.FETCH_TOPIC_BY_PART, fetchTopicByPart)
  yield takeEvery(config.FETCH_LIST_PRACTICES, fetchListPractices)
  yield takeEvery(config.FETCH_TYPE_TOPIC, fetchGetTypeTopic)
  yield takeEvery(config.FETCH_PART_BY_TOPIC, fetchGetPartByTypeTopic)
  // yield takeEvery(config.FETCH_PART_TOPIC_BY_TYPE , fetchPartTopicByType)
  yield takeEvery(config.FETCH_TOPIC_BY_PART_LEVEL , fetchTopicByPartAndLevel)
  yield takeEvery(config.FETCH_TOPIC_NAME_BY_TYPE_TOPIC_AND_PART , fetchTopicByTypeTopicAndPart)
  yield takeEvery(config.GET_MENU_LIST_TEST, getMenuListTest)
}

export default topicSaga
