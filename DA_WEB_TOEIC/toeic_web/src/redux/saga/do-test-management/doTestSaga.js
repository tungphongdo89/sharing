import {call, delay, put, takeEvery} from "redux-saga/effects";
import * as config from "../../../commons/constants/Config";
import STATUS_CODE from '../../../commons/constants/STATUS_CODE'
import * as Api from '../../apis/doTestService'
import testAction from '../../actions/do-test-management'


function* getListQuestionMinitest() {
  // const {data} = payload
  const resp = yield call(Api.getListQuestionMinitest, 'v1/questions/minitest/getListQuestionMinitest')
  const {status: statusCode, data: dataResp} = resp;
  delay(500)
  if (STATUS_CODE.SUCCESS === statusCode) {
    yield put(testAction.getListQuestionMinitestSuccess(dataResp))
  } else {
    yield put(testAction.getListQuestionMinitestFailed(dataResp))
  }
}

function* submitAnswerMinitest({payload}) {
  const {data} = payload;
  const resp = yield call(Api.submitAnswerMinitest, 'v1/questions/minitest/submitAnswer', data)
  const {status: statusCode, data: dataResp} = resp;
  delay(500)
  if (STATUS_CODE.SUCCESS === statusCode) {
    yield put(testAction.submitAnswerMinitestSuccess(dataResp))
  } else {
    yield put(testAction.submitAnswerMinitestFailed(dataResp))
  }
}

function* getDetailFullTest({payload}) {
  const {data} = payload
  const resp = yield call(Api.getDetailFullTest, 'v1/tests/getDetailFullTestById',data)
  const {status: statusCode, data: dataResp} = resp;
  delay(500)
  if (STATUS_CODE.SUCCESS === statusCode) {
    yield put(testAction.getDetailFullTestSuccess(dataResp))
  } else {
    yield put(testAction.getDetailFullTestFailed(dataResp))
  }
}

function* getResultFullTest({payload}) {
  const {data} = payload;
  const resp = yield call(Api.submitAnswerMinitest, 'v1/tests/getResultFullTestById', data)
  const {status: statusCode, data: dataResp} = resp;
  delay(500)
  if (STATUS_CODE.SUCCESS === statusCode) {
    yield put(testAction.getResultFullTestSuccess(dataResp))
  } else {
    yield put(testAction.getResultFullTestFailed(dataResp))
  }
}

function* saveResultMinitest({payload}) {
  const {data} = payload;
  const resp = yield call(Api.saveResultMinitest, 'v1/questions/minitest/saveResultMinitest', data)
  const {status: statusCode, data: dataResp} = resp;
  delay(500)
  if (STATUS_CODE.SUCCESS === statusCode) {
    yield put(testAction.saveResultMinitestSuccess(dataResp))
  } else {
    yield put(testAction.saveResultMinitestFailed(dataResp))
  }
}

function* getListHistoryMinitest({payload}) {
  const {data} = payload;
  const resp = yield call(Api.getListHistoryMinitest, 'v1/questions/minitest/doSearch', data)
  const {status: statusCode, data: dataResp} = resp;
  delay(500)
  if (STATUS_CODE.SUCCESS === statusCode) {
    yield put(testAction.getListHistoryMinitestSuccess(dataResp))
  } else {
    yield put(testAction.getListHistoryMinitestFailed(dataResp))
  }
}

function* getListNameTest() {
  const resp = yield call(Api.getListNameTest, 'v1/tests/getListNameTest', {})
  const {status: statusCode, data: dataResp} = resp;
  delay(500)
  if (STATUS_CODE.SUCCESS === statusCode) {
    yield put(testAction.getListNameTestSuccess(dataResp))
  } else {
    yield put(testAction.getListNameTestFail(dataResp))
  }
}

function* doTestSaga() {
  yield takeEvery(config.GET_LIST_QUESTION_MINITEST, getListQuestionMinitest)
  yield takeEvery(config.SUBMIT_ANSWER_MINITEST, submitAnswerMinitest)
  yield takeEvery(config.GET_DETAIL_FULL_TEST, getDetailFullTest)
  yield takeEvery(config.GET_RESULT_FULL_TEST, getResultFullTest)
  yield takeEvery(config.SAVE_RESULT_MINITEST, saveResultMinitest)
  yield takeEvery(config.GET_LIST_HISTORY_MINITEST, getListHistoryMinitest)
  yield takeEvery(config.GET_LIST_NAME_TEST, getListNameTest)
}

export default doTestSaga
