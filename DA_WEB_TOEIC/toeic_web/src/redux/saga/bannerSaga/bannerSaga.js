import { put, takeEvery, call } from "redux-saga/effects";
import * as actionType from "../../../commons/constants/Config"
// reducer
import bannerAction from "../../actions/banner/bannerAction"
// api server
import * as bannerAPI from "../../apis/bannerAPIs"
import testAction from "../../actions/test";

function * getBanner(){
  let res = yield call(bannerAPI.getBanner)
  if(res.status === 200){
    yield put(bannerAction.getBannerSuccess(res.data))
  }
  else{
    yield put(bannerAction.getBannerFailure())
  }
}

function * createBanner({payload}){
  let res = yield call(bannerAPI.createBanner , payload.data)
  console.log("banner saga : " , res.data)
  if(res.status === 200){
    yield put(bannerAction.createBannerSuccess(res.data))
    let banner ={};
    banner.page = 1;
    banner.pageSize =10;
    yield put(bannerAction.showBannerActive())
    yield put(bannerAction.getListBannerNotActive(banner))
  } else {
    yield put(bannerAction.createBannerFailure(res.data))
  }
}

function * showBannerActive(){
  let res = yield call(bannerAPI.showBannerActive)
  console.log("banner saga : " , res.data)
  if(res.status === 200){
    yield put(bannerAction.showBannerActiveSuccess(res.data))
  } else {
    yield put(bannerAction.showBannerActiveFailed(res.data))
  }
}

function * getListBannerActive(){
  let res = yield call(bannerAPI.getListBannerActive)
  console.log("banner saga : " , res.data)
  if(res.status === 200){
    yield put(bannerAction.getListBannerActiveSuccess(res.data))
  } else {
    yield put(bannerAction.getListBannerActiveFailed(res.data))
  }
}

function * getListBannerNotActive({payload}){
  let res = yield call(bannerAPI.getListBannerNotActive,payload.data)
  if(res.status === 200){
    yield put(bannerAction.getListBannerNotActiveSuccess(res.data))
  } else {
    yield put(bannerAction.getListBannerNotActiveFailed(res.data))
  }
}

function * deleteBanner({payload}){
  let res = yield call(bannerAPI.deleteBanner,payload.data)
  if(res.status === 200){
    yield put(bannerAction.deleteBannerSuccess(res.data))
    let banner ={};
    banner.page = 1;
    banner.pageSize =10;
    yield put(bannerAction.showBannerActive())
    yield put(bannerAction.getListBannerNotActive(banner))
  } else {
    yield put(bannerAction.deleteBannerFailed(res.data))
  }
}

function * updateBanner({payload}){
  let res = yield call(bannerAPI.updateBanner,payload.data)
  if(res.status === 200){
    yield put(bannerAction.updateBannerSuccess(res.data))
    let banner ={};
    banner.page = 1;
    banner.pageSize =10;
    yield put(bannerAction.showBannerActive())
    yield put(bannerAction.getListBannerNotActive(banner))
  } else {
    yield put(bannerAction.updateBannerFailed(res.data))
  }
}

function * updateOrderBanner({payload}){
  let res = yield call(bannerAPI.updateOrderBanner,payload.data)
  if(res.status === 200){
    yield put(bannerAction.updateOrderBannerSuccess(res.data))
  } else {
    yield put(bannerAction.updateOrderBannerFailed(res.data))
  }
}

function * rootSaga(){
  yield takeEvery(actionType.GET_BANNER , getBanner)
  yield takeEvery(actionType.CREATE_BANNER , createBanner)
  yield takeEvery(actionType.SHOW_BANNER_ACTIVE , showBannerActive)
  yield takeEvery(actionType.GET_LIST_BANNER_NOT_ACTICVE , getListBannerNotActive)
  yield takeEvery(actionType.GET_LIST_BANNER_ACTICVE , getListBannerActive)
  yield takeEvery(actionType.DELETE_BANNER , deleteBanner)
  yield takeEvery(actionType.UPDATE_BANNER , updateBanner)
  yield takeEvery(actionType.UPDATE_ORDER_BANNER , updateOrderBanner)
}

export default rootSaga
