import { isPromise } from "formik";
import { put, takeEvery, call,delay, take, takeLatest } from "redux-saga/effects";
import *  as actionType from '../../../commons/constants/Config';
import wordAction from '../../actions/word/wordAction';
import STATUS_CODE from '../../../commons/constants/STATUS_CODE';
import * as dictionaryService from '../../apis/dictionaryService';

function* doTranslateWord({ payload }) {
    const obj = {nameEng: payload.data.result}; //keySearch nameEng translate
    const resp = yield call(dictionaryService.fetchDictionary, 'v1/dictionaries/translate', obj)  //doSearch
    delay(300)
    const {status, data} = resp;
    console.log(resp);
    if (status === 200  ) {
        yield put(wordAction.fetchDetailVocabularySuccess(data)); //.data
    }
    else{
        yield put(wordAction.fetchDetailVocabularyFailure(data));
    }
}

function* wordSaga() {
    yield takeEvery(actionType.SET_HIGHLIGHT_WORD, doTranslateWord);
}

export default wordSaga


