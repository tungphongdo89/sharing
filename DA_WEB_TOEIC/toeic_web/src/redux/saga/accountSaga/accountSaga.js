import {call, put, takeEvery} from "redux-saga/effects";
import * as actionType from "../../../commons/constants/Config"
import * as accountAction from "../../actions/account/accountActions"
import * as accountApis from "../../apis/accountAPIs"
import {history} from "../../../history";
import STATUS_CODE from '../../../commons/constants/STATUS_CODE'

function * updateProfileAdmin({payload}){
  let resUpdateProfle = yield call(accountApis.updateProfileAdmin , payload.data)
  if(resUpdateProfle.status === STATUS_CODE.SUCCESS){
    let resGetProfile = yield call(accountApis.getProfile)
    if(resGetProfile.status === STATUS_CODE.SUCCESS){
      yield put(accountAction.updateProfileAdminSuccess(resGetProfile.data))
    }
    else{
      yield put(accountAction.updateProfileAdminFail())
    }
  }
  else {
    yield put(accountAction.updateProfileAdminFail())
  }
}


function * settingAccount({payload}) {
  let res = yield call(accountApis.settingAccount , payload.data)
  yield put(accountAction.loginSettingAccount())
  history.push("/pages/login")
}

function * rootSaga(){
  yield takeEvery(actionType.UPDATE_PROFILE_ADMIN , updateProfileAdmin)
  yield takeEvery(actionType.SETTING_ACCOUNT , settingAccount)
}

export default rootSaga
