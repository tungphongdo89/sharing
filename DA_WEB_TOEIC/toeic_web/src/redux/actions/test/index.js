import * as config from "../../../commons/constants/Config";


const testAction = {
  addQuestionInForm,
  fetchTest,
  fetchTestSuccess,
  fetchTestFaild,
  createTest,
  createTestSuccess,
  createTestFailed,
  fetchDetailTest,
  fetchDetailTestSuccess,
  fetchDetailTestFailed,
  updateTest,
  updateTestSuccess,
  updateTestFailed,
  fetchDataFiler,
  fetchDataFilerSuccess,
  deleteTest,
  deleteTestSuccess,
  deleteTestFaild,
  fetchDataFilerFaild,
  getListTypeExercise,
  getListTypeExerciseSuccess,
  getListTypeExerciseFailed,
  getListPartExercise,
  getListPartExerciseSuccess,
  getListPartExerciseFailed,
  getListTypeLevel,
  getListTypeLevelSuccess,
  getListTypeLevelFailed,
  getListTypeFileUpload,
  getListTypeFileUploadSuccess,
  getListTypeFileUploadFailed,
  getListTopicByPart,
  getListTopicByPartSuccess,
  getListTopicByPartFailed,
  editDetailTest,
  updateListCheckedListening,
  deleteListChecked,
  updateListCheckedReading,
  getListCategory,
  getListCategorySuccess,
  getListCategoryFaild,
  getStudentFaildByTestId,
  getStudentFaildByTestIdSuccess,
  getStudentFaildByTestIdFaild,
  updateStatusPopupTest
}

function addQuestionInForm() {

}

function fetchTest(data) {
  return {
    type: config.FETCH_TEST,
    payload: {data}
  }
}
function fetchTestSuccess(data) {
  console.log(data);
  return {
    type: config.FETCH_TEST_SUCCESS,
    payload: {data}
  }

}
function fetchTestFaild(data) {
  return {
    type: config.FETCH_TEST_FAILD,
    payload: {data}
  }
}
function createTest(data) {
  return {
    type: config.CREATE_TEST,
    payload: {data}
  }
}
function createTestSuccess(data) {
  return {
    type: config.CREATE_TEST_SUCCESS,
    payload: {data}
  }
}
function createTestFailed(data) {
  return {
    type: config.CREATE_TEST_FAILED,
    payload: {data}
  }
}
function updateTest(data) {
  return {
    type: config.UPDATE_TEST,
    payload: {
      data
    }
  }
}
function updateTestSuccess(data) {
  return {
    type: config.UPDATE_TEST_SUCCESS,
    payload: {
      data
    }
  }
}
function updateTestFailed(data) {
  return {
    type: config.UPDATE_TEST_FAILED,
    payload: {
      data
    }
  }
}
function fetchDetailTest(data, check) {
  return {
    type: config.FETCH_DETAIL_TEST,
    payload: {data, check}
  }
}
function fetchDetailTestSuccess(data, check){
  return {
    type: config.FETCH_DETAIL_TEST_SUCCESS,
    payload:{data, check}
  }
}
function fetchDetailTestFailed(data, check){
  return {
    type: config.FETCH_DETAIL_TEST_FAILED,
    payload: {data, check}
  }
}
function fetchDataFiler(data) {
  return {
    type: config.FETCH_DATA_FILTER,
    payload: {
      data
    }
  }

}

function fetchDataFilerSuccess(data) {
  return {
    type: config.FETCH_DATA_FILTER_SUCCESS,
    payload: {
      data
    }
  }
}

function fetchDataFilerFaild(data) {
  return {
    type: config.FETCH_DATA_FILTER_SUCCESS,
    payload: {
      data
    }
  }

}
function deleteTest(data) {
  return {
    type: config.DELETE_TEST,
    payload: {
      data
    }
  }
}function deleteTestSuccess(data) {
  return {
    type: config.DELETE_TEST_SUCCESS,
    payload: {
      data
    }
  }
}function deleteTestFaild(data) {
  return {
    type: config.DELETE_TEST_FAILD,
    payload: {
      data
    }
  }
}

function getListTypeExercise() {
  return {
    type: config.GET_LIST_TEST_TYPE_EXERCISE
  }
}
function getListTypeExerciseSuccess(data) {
  return {
    type: config.GET_LIST_TEST_TYPE_EXERCISE_SUCCESS,
    payload: {
      data
    }
  }
}
function getListTypeExerciseFailed(data) {
  return {
    type: config.GET_LIST_TEST_TYPE_EXERCISE_FAILED,
    payload: {
      data
    }
  }
}
function getListPartExercise(data) {
  return {
    type: config.GET_LIST_TEST_PART_EXERCISE,
    payload : {data}
  }
}
function getListPartExerciseSuccess(data) {
  return {
    type: config.GET_LIST_TEST_PART_EXERCISE_SUCCESS,
    payload: {
      data
    }
  }
}
function getListPartExerciseFailed(data) {
  return {
    type: config.GET_LIST_TEST_PART_EXERCISE_FAILED,
    payload: {
      data
    }
  }
}
function getListTypeLevel() {
  return {
    type: config.GET_LIST_TEST_TYPE_LEVEL
  }
}
function getListTypeLevelSuccess(data) {
  return {
    type: config.GET_LIST_TEST_TYPE_LEVEL_SUCCESS,
    payload: {
      data
    }
  }
}
function getListTypeLevelFailed(data) {
  return {
    type: config.GET_LIST_TEST_TYPE_LEVEL_FAILED,
    payload: {
      data
    }
  }
}
function getListTypeFileUpload() {
  return {
    type: config.GET_LIST_TEST_TYPE_FILE_UPLOAD
  }
}
function getListTypeFileUploadSuccess(data) {
  return {
    type: config.GET_LIST_TEST_TYPE_FILE_UPLOAD_SUCCESS,
    payload: {
      data
    }
  }
}
function getListTypeFileUploadFailed(data) {
  return {
    type: config.GET_LIST_TEST_TYPE_FILE_UPLOAD_FAILED,
    payload: {
      data
    }
  }
}
function getListTopicByPart(data) {
  return {
    type: config.GET_LIST_TEST_TOPIC_BY_PART,
    payload : {data}
  }
}
function getListTopicByPartSuccess(data) {
  return {
    type: config.GET_LIST_TEST_TOPIC_BY_PART_SUCCESS,
    payload: {
      data
    }
  }
}
function getListTopicByPartFailed(data) {
  return {
    type: config.GET_LIST_TEST_TOPIC_BY_PART_FAILED,
    payload: {
      data
    }
  }
}
function editDetailTest() {
  return {
    type: config.EDIT_DETAIL_TEST
  }

}
function updateListCheckedReading(data) {
  return {
    type:config.UPDATE_LIST_CHECKED_READING,
    payload:{data}
  }
}

function updateListCheckedListening(data) {
  return {
    type: config.UPDATE_LIST_CHECKED_LISTENING,
    payload: {data}
  }
}

function deleteListChecked() {
  return {
    type: config.DELETE_LIST_CHECKED
  }
}

function getListCategory(data) {
  return {
    type: config.GET_LIST_CATEGORY,
    payload: { data }
  }
}

function getListCategorySuccess(data) {
  return {
    type: config.GET_LIST_CATEGORY_SUCCESS,
    payload: {data}
  }
}

function getListCategoryFaild(data) {
  return {
    type: config.GET_LIST_CATEGORY_FAILD,
    payload: { data }
  }
}

function getStudentFaildByTestId (data){
  return{
    type:config.GET_LIST_STUDENT_FAILD_BY_TEST_ID,
    payload: {data}
  }
}

function getStudentFaildByTestIdSuccess(data){
  return{
    type:config.GET_LIST_STUDENT_FAILD_BY_TEST_ID_SUCCESS,
    payload: {data}
  }
}

function getStudentFaildByTestIdFaild(data){
  return{
    type:config.GET_LIST_STUDENT_FAILD_BY_TEST_ID_FAILD,
    payload: { data }
  }
}
function updateStatusPopupTest(data) {
  return {
    type: config.UPDATE_STATUS_POPUP_TEST,
    payload: {data}
  }
}

export default testAction;
