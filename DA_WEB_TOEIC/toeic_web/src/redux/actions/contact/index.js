import {
  CONTACT_ACTION,CONTACT_FAILED,CONTACT_SUCCESS
} from "../../../commons/constants/Config";

const ShowContactActions = {
  sendContact,
  sendSuccess,
  sendFailed,
}

function sendFailed() {
  return{
    type: CONTACT_FAILED,
  }
}

function sendSuccess() {
    return{
      type: CONTACT_SUCCESS,
    }
}

function sendContact(email, name, context) {
  return {
    type: CONTACT_ACTION,
    payload: {
      data: {
        email: email,
        name: name,
        context: context,
      }
    }
  }
}



export default ShowContactActions;
