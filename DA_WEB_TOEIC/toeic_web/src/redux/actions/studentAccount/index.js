import * as config from "../../../commons/constants/Config"

const studentProfileAction = {
  getStudentProfile,
  getStudentProfileSuccess,
  getStudentProfileFailure,
  updateStudentProfile,
  updateStudentProfileSuccess,
  updateStudentProfileFailure,
  openEdit,
}


function openEdit() {
  return {
    type: config.OPEN_EDIT_STUDENT_PROFILE
  }
}

function closeEdit() {
  return {
    type: config.CLOSE_EDIT_STUDENT_PROFILE
  }
}

function getStudentProfile() {
  return {
    type: config.GET_STUDENT_PROFILE,
    payload: {}
  }
}

function getStudentProfileSuccess(data) {
  return {
    type: config.GET_STUDENT_PROFILE_SUCCESS,
    payload: {
      data
    }
  }
}

function getStudentProfileFailure() {
  return {
    type: config.GET_STUDENT_PROFILE_FAILURE
  }
}

function updateStudentProfile(data) {
  return {
    type: config.UPDATE_STUDENT_PROFILE,
    payload: {
      data
    }
  }
}

function updateStudentProfileSuccess(data) {
  return {
    type: config.UPDATE_STUDENT_PROFILE_SUCCESS,
    payload: {
      data
    }
  }
}

function updateStudentProfileFailure() {
  return {
    type: config.UPDATE_STUDENT_PROFILE_FAILURE
  }
}

export default studentProfileAction;
