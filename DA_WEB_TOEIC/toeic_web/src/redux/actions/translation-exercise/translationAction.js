import * as config from '../../../commons/constants/Config';

const translationAction = {
  //by topic
	fetchQaTransAvByTopic,
  fetchQaTransAvByTopicSuccess,
  fetchQaTransAvByTopicFailed,

  fetchQaTransVaByTopic,
  fetchQaTransVaByTopicSuccess,
  fetchQaTransVaByTopicFailed,

  //by level
  fetchQaTransAvByLevel,
  fetchQaTransAvByLevelSuccess,
  fetchQaTransAvByLevelFailed,

  fetchQaTransVaByLevel,
  fetchQaTransVaByLevelSuccess,
  fetchQaTransVaByLevelFailed,

  disableAnswerWhenSubmit,
  setLevelTitleForAVVA
}

//dịch anh - việt by topic
function fetchQaTransAvByTopic(data) {
  return {
    type: config.FETCH_QA_TRANS_AV_BY_TOPIC,
    payload: {data}
  }

}
function fetchQaTransAvByTopicSuccess(data) {
  return{
    type: config.FETCH_QA_TRANS_AV_BY_TOPIC_SUCCESS,
    payload: {data}
  }

}
function fetchQaTransAvByTopicFailed(data) {
  return{
    type: config.FETCH_QA_TRANS_AV_BY_TOPIC_FAILED,
    payload: {data}
  }
}

//dịch việt - anh by topic
function fetchQaTransVaByTopic(data) {
  return {
    type: config.FETCH_QA_TRANS_VA_BY_TOPIC,
    payload: {data}
  }

}
function fetchQaTransVaByTopicSuccess(data) {
  return{
    type: config.FETCH_QA_TRANS_VA_BY_TOPIC_SUCCESS,
    payload: {data}
  }

}
function fetchQaTransVaByTopicFailed(data) {
  return{
    type: config.FETCH_QA_TRANS_VA_BY_TOPIC_FAILED,
    payload: {data}
  }
}

//dịch anh - việt by level
function fetchQaTransAvByLevel(data) {
  return {
    type: config.FETCH_QA_TRANS_AV_BY_LEVEL,
    payload: {data}
  }

}
function fetchQaTransAvByLevelSuccess(data) {
  return{
    type: config.FETCH_QA_TRANS_AV_BY_LEVEL_SUCCESS,
    payload: {data}
  }

}
function fetchQaTransAvByLevelFailed(data) {
  return{
    type: config.FETCH_QA_TRANS_AV_BY_LEVEL_FAILED,
    payload: {data}
  }
}

//dịch việt - anh by level
function fetchQaTransVaByLevel(data) {
  return {
    type: config.FETCH_QA_TRANS_VA_BY_LEVEL,
    payload: {data}
  }

}
function fetchQaTransVaByLevelSuccess(data) {
  return{
    type: config.FETCH_QA_TRANS_VA_BY_LEVEL_SUCCESS,
    payload: {data}
  }

}
function fetchQaTransVaByLevelFailed(data) {
  return{
    type: config.FETCH_QA_TRANS_VA_BY_LEVEL_FAILED,
    payload: {data}
  }
}

//disable answer when submiting
function disableAnswerWhenSubmit(data) {
  return {
    type: config.DISABLE_ANSWER_WHEN_SUBMIT,
    payload: {data}
  }
}

//set level title for av va
function setLevelTitleForAVVA(data) {
  return {
    type: config.SET_LEVEL_TITLE_FOR_AV_VA,
    payload: {data}
  }
}


export default translationAction
