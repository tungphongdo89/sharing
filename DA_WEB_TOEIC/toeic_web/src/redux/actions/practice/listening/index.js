import * as config from "../../../../commons/constants/Config";

const practiceListenMultiActions = {
  getListListeningQuestionOfMulti,
  getListListeningQuestionOfMultiSuccess,
  getListListeningQuestionOfMultiFaild,

  resetCountCheckListeningPractices,

  getTopicId,
  checkAnswerMultiListeningQuestion,
  checkAnswerMultiListeningQuestionSuccess,
  checkAnswerMultiListeningQuestionFaild,
  clearTempQuestionAnswer,
  clearTotalCorrect,
  clearAnsweredMultiListenQuest,
  getListQuestionOfListeningWordFill,
  getListQuestionOfListeningWordFillSuccess,
  getListQuestionOfListeningWordFillFail,
  getResultQuestionOfListeningWordFill,
  getResultQuestionOfListeningWordFillSuccess,
  getResultQuestionOfListeningWordFillFail,
  cleanListQuestionOfListeningWordFill,
  cleanResultQuestionOfListeningWordFill,
  cleanNumberCorrectAnswerWordFill,
  getValueTypeCode,
  getPartId,
  getNameTopic,
  cleanListQuestionAndAnswerListening,

  fetchListeningToeicConverstation,
  fetchListeningToeicConverstationSuccess,
  fetchListeningToeicConverstationFailed,
  updateCheckListening,
  updateCheckListeningFalse,
  getIndexListen,
  saveListCorrect,
  getPartPracticeListening,
  setQuestionAnsweredPracticeListening,

  createHistoryListenFill,
  createHistoryListenFillSuccess,
  createHistoryListenFillFailed,

  getHistoryListenFill,
  getHistoryListenFillSuccess,
  getHistoryListenFillFailed,
  updateSubmitFill,


  //get detail history listening
  cleanBeforeGetDetailHL,

  getDetailHistoryListening,
  getDetailHistoryListeningSuccess,
  getDetailHistoryListeningFailed,

  getDetailHistoryLisSingle,
  getDetailHistoryLisSingleSuccess,
  getDetailHistoryLisSingleFailed,


  setPartListeningPar1,
  setPartListeningPar2,

  updateViewBtnListening,
  cleanTypeNameLevel,
  updateCheckButtonSaveListening,

  hidePathFileWhenListenAgain,
  setTotalCorrectAnswerWhenReview,

  showAudioWhenReviewPart12
}

function getListListeningQuestionOfMulti(topicId, levelCode) {
  return {
    type: config.GET_LIST_LISTENING_QUESTIONS_OF_MULTI,
    payload: {
      data: {
        topicId: topicId,
        levelCode: levelCode
      }
    }
  }
}

function getListListeningQuestionOfMultiSuccess(data) {
  return {
    type: config.GET_LIST_LISTENING_QUESTIONS_OF_MULTI_SUCCESS,
    payload: {
      data: data
    }
  }
}

function getListListeningQuestionOfMultiFaild() {
  return {
    type: config.GET_LIST_LISTENING_QUESTIONS_OF_MULTI_FAILD,
  }
}

//set count check = 0
function resetCountCheckListeningPractices() {
  return {
    type: config.RESET_COUNT_CHECK_LISTENING_PRACTICES
  }
}

//Show file nghe gốc khi xem lại đáp án part1, part2
function showAudioWhenReviewPart12() {
  return {
    type: config.SHOW_AUDIO_WHEN_REVIEW_PART12
  }
}


function getTopicId(topicId, levelCode) {
  return {
    type: config.GET_TOPICID_FOR_PRACTICE,
    payload: {
      data: topicId,
      levelCode: levelCode
    }
  }
}

function checkAnswerMultiListeningQuestion(topicId, qid, userChoose) {
  return {
    type: config.CHECK_ANSWER_MULTI_LISTENING_QUESTION,
    payload: {
      data: {
        topicId: topicId,
        qid: qid,
        userChoose: userChoose
      }
    }
  }
}

function checkAnswerMultiListeningQuestionSuccess(data) {
  return {
    type: config.CHECK_ANSWER_MULTI_LISTENING_QUESTION_SUCCESS,
    payload: {
      data: data
    }
  }
}

function checkAnswerMultiListeningQuestionFaild() {
  return {
    type: config.CHECK_ANSWER_MULTI_LISTENING_QUESTION_FAILD,
  }
}

function clearTempQuestionAnswer() {
  return{
    type:config.CLEAR_TEMP_MULTI_LISTENING_QUESTION
  }
}

function clearTotalCorrect() {
  return{
    type:config.CLEAR_TOTAL_CORRECT_MULTI_LISTEN_QUESTION
  }
}

function clearAnsweredMultiListenQuest() {
  return {
    type: config.CLEAR_LIST_ANSWERED_MULTI_LISTEN_QUESTION
  }
}

function getListQuestionOfListeningWordFill(data) {
  return {
    type: config.GET_LIST_QUESTION_OF_LISTENING_WORD_FILL,
    payload: {
      data
    }
  }
}

function getListQuestionOfListeningWordFillSuccess(data) {
  return {
    type: config.GET_LIST_QUESTION_OF_LISTENING_WORD_FILL_SUCCESS,
    payload: {data}
  }
}

function getListQuestionOfListeningWordFillFail(data) {
  return {
    type: config.GET_LIST_QUESTION_OF_LISTENING_WORD_FILL_FAIL,
    payload: {data}
  }
}

function getResultQuestionOfListeningWordFill(id, userFill) {
  return {
    type: config.GET_RESULT_QUESTION_OF_LISTENING_WORD_FILL,
    payload: {
      data: {
        id,
        userFill
      }
    }
  }
}

function getResultQuestionOfListeningWordFillSuccess(data) {
  return {
    type: config.GET_RESULT_QUESTION_OF_LISTENING_WORD_FILL_SUCCESS,
    payload: {data}
  }
}

function getResultQuestionOfListeningWordFillFail(data) {
  return {
    type: config.GET_RESULT_QUESTION_OF_LISTENING_WORD_FILL_FAIL,
    payload: {data}
  }
}

function cleanListQuestionOfListeningWordFill() {
  return {
    type: config.CLEAN_LIST_QUESTION_OF_LISTENING_WORD_FILL
  }
}

function cleanResultQuestionOfListeningWordFill() {
  return {
    type: config.CLEAN_RESULT_QUESTION_OF_LISTENING_WORD_FILL
  }
}

function cleanNumberCorrectAnswerWordFill() {
  return {
    type: config.CLEAN_NUMBER_CORRECT_ANSWER_WORD_FILL
  }
}

function getValueTypeCode(data) {
  return {
    type: config.GET_VALUE_TYPE_CODE,
    payload: {data}
  }
}

function getPartId(data) {
  return {
    type: config.GET_PART_ID,
    payload: {data}
  }
}

function getNameTopic(data) {
  return {
    type: config.GET_NAME_TOPIC,
    payload:{
      data
    }
  }
}

function cleanListQuestionAndAnswerListening() {
  return {
    type: config.CLEAN_LIST_QUESTION_AND_ANSWER_LISTENING
  }
}

// listening toeic conversation
function fetchListeningToeicConverstation(data) {
  return {
    type: config.FETCH_LISTENING_TOEIC_CONVERSATION,
    payload: {data}
  }

}
function fetchListeningToeicConverstationSuccess(data) {
  return{
    type: config.FETCH_LISTENING_TOEIC_CONVERSATION_SUCCESS,
    payload: {data}
  }

}
function fetchListeningToeicConverstationFailed(data) {
  return{
    type: config.FETCH_LISTENING_TOEIC_CONVERSATION_FAILED,
    payload: {data}
  }
}

function saveListCorrect(numberCorrect, numberQues) {
  return {
    type: config.SAVE_LIST_CORRECT_LISTENING,
    payload: {
      data: {numberCorrect, numberQues}
    }
  }
}
function updateCheckListening() {
  return {
    type: config.UPDATE_CHECK_LISTENING
  }
}
function updateCheckListeningFalse() {
  return{
    type: config.UPDATE_CHECK_LISTENING_FALSE
  }
}
function getIndexListen(data) {
  return{
    type: config.GET_INDEX_LISTEN,
    payload: { data}
  }
}

function getPartPracticeListening(data){
  return {
    type:config.SET_PART_PRACTICE_LISTENING,
    payload:{
      data:data
    }
  }
}
//set part for listening part1, 2
function setPartListeningPar1() {
  return{
    type: config.SET_PART_LISTENING_PART1
  }
}
function setPartListeningPar2() {
  return{
    type: config.SET_PART_LISTENING_PART2
  }
}


function setQuestionAnsweredPracticeListening(data,index){
  return{
    type: config.SET_QUESTION_ANSWERED_PRACTICE_LISTENING,
    payload:{
      data: {
        question:data,
        index:index,
      }
    }
  }
}

function createHistoryListenFill(data) {
  return {
    type: config.CREATE_HISTORY_LISTEN_FILL,
    payload:{data}
  }
}
function createHistoryListenFillSuccess(data) {
  return {
    type: config.CREATE_HISTORY_LISTEN_FILL_SUCCESS,
    payload:{data}
  }
}
function createHistoryListenFillFailed(data) {
  return {
    type: config.CREATE_HISTORY_LISTEN_FILL_FAILED,
    payload:{data}
  }
}

function getHistoryListenFill(data) {
  return {
    type: config.GET_HISTORY_LISTEN_FILL,
    payload:{data}
  }
}

function getHistoryListenFillSuccess(data) {
  return {
    type: config.GET_HISTORY_LISTEN_FILL_SUCCESS,
    payload:{data}
  }
}

function getHistoryListenFillFailed(data) {
  return {
    type: config.GET_HISTORY_LISTEN_FILL_FAILED,
    payload:{data}
  }
}
function updateSubmitFill() {
  return {
    type: config.UPDATE_SUBMIT_FILL
  }
}

//----------clean before get detail history listening----------------
function cleanBeforeGetDetailHL() {
  return {
    type: config.CLEAN_BEFORE_GET_DETAIL_HL
  }

}
//----------end clean before get detail history listening--------------

//----------------get detail history listening---------------------------
function getDetailHistoryListening(data) {
  return {
    type: config.GET_DETAIL_HISTORY_LISTENING,
    payload: {data}
  }

}
function getDetailHistoryListeningSuccess(data) {
  return{
    type: config.GET_DETAIL_HISTORY_LISTENING_SUCCESS,
    payload: {data}
  }

}
function getDetailHistoryListeningFailed(data) {
  return{
    type: config.GET_DETAIL_HISTORY_LISTENING_FAILED,
    payload: {data}
  }
}
//----------------End get detail history listening---------------------------


//--------------Ẩn file nghe gốc khi nghe lại----------------
function hidePathFileWhenListenAgain(data) {
  return {
    type: config.HIDE_PATH_FILE_WHEN_LISTEN_AGAIN,
    payload: {data}
  }
}
//--------------End Ẩn file nghe gốc khi nghe lại----------------


//--------------set total correct khi xem lại đáp án----------------------
function setTotalCorrectAnswerWhenReview(data) {
  return {
    type: config.SET_TOTAL_CORRECT_WHEN_REVIEW,
    payload: {data}
  }
}
//--------------end set total correct khi xem lại đáp án----------------------



//----------------get detail history listening---------------------------
function getDetailHistoryLisSingle(data) {
  return {
    type: config.GET_DETAIL_HISTORY_LIS_SINGLE,
    payload: {data}
  }

}
function getDetailHistoryLisSingleSuccess(data) {
  return{
    type: config.GET_DETAIL_HISTORY_LIS_SINGLE_SUCCESS,
    payload: {data}
  }

}
function getDetailHistoryLisSingleFailed(data) {
  return{
    type: config.GET_DETAIL_HISTORY_LIS_SINGLE_FAILED,
    payload: {data}
  }
}

function updateViewBtnListening() {
  return {
    type: config.UPDATE_VIEW_BTN_LISTENING
  }
}
function cleanTypeNameLevel() {
  return {
    type: config.CLEAN_TYPE_NAME_LEVEL
  }
}
function updateCheckButtonSaveListening() {
  return {
    type: config.UPDATE_CHECK_HIDDEN_BUTTON_SAVE_LISTENING
  }
}
//----------------End get detail history listening---------------------------





export default practiceListenMultiActions;
