
import * as config from '../../../../commons/constants/Config';

const readingActions ={
  getListQuestionOfReadingWordFill,
  getListQuestionOfReadingWordFillSuccess,
  getListQuestionOfReadingWordFillFail,
  getResultQuestionOfReadingWordFill,
  getResultQuestionOfReadingWordFillSuccess,
  getResultQuestionOfReadingWordFillFail,
  cleanListQuestionOfReadingWordFill,
  cleanResultQuestionOfReadingWordFill,
  cleanNumberCorrectAnswer,
  getNameTopic,
  cleanListQuestionAndAnswerReading,
  getListQuestionOfReadingCompletedPassage,
  getListQuestionOfReadingCompletedPassageSuccess,
  getListQuestionOfReadingCompletedPassageFail,
  getResultQuestionOfReadingCompletedPassage,
  getResultQuestionOfReadingCompletedPassageSuccess,
  getResultQuestionOfReadingCompletedPassageFail,
  cleanListQuestionOfReadingCompletedPassage,
  cleanResultQuestionOfReadingCompletedPassage,
  cleanNumberCorrectAnswerReadingCompletedPassage,
  getNameTopicReadingCompletedPassage,
  cleaningListQuestionAndAnswerReadingCompletedPassage,
  getListSingleAndDualReadingQuestion,
  getListSingleAndDualReadingQuestionSuccess,
  getListSingleAndDualReadingQuestionFaild,
  clearTempReadingSingleAndDualQuestion,
  submitSingleAndDualReadingQuestion,
  submitSingleAndDualReadingQuestionSuccess,
  submitSingleAndDualReadingQuestionFaild,
  clearTotalTrueReadingSingleAndDual,
  saveScoreCorrect,
  getNameTopicPassage,
  setPartPracticeReading,
  clearTotalQuestion,
  updateCheckReview,
  getIndex,
  getPart,
  saveListCorrect,
  saveCurrentSumScore,
  updateCheckReviewFalse,
  setTempAnsweredReadingSingleAndDualQuest,
  getLevelCode,
  storeDataToRepracticeReading,
  setValueIndex,
  updateIsSubmit,
  createdHistoryReadingWordFill,
  createdHistoryReadingWordFillSuccess,
  createdHistoryReadingWordFillFailed,
  getTopicId,
  getTypeCodeReading,
  saveUserChoose,
  createHistoryReadingCompletedPassage,
  createHistoryReadingCompletedPassageSuccess,
  createHistoryReadingCompletedPassageFailed,
  cleanNumberSelected,
  createHistoryReadingSingleDual,
  createHistoryReadingSingleDualSuccess,
  createHistoryReadingSingleDualFailed, 
  getHistoryInCompleteSentences,
  getHistoryInCompleteSentencesSuccess,
  getHistoryInCompleteSentencesFailed,
  updateViewBtnReading,
  getDetailHistoryReadingCompletedPassage,
  getDetailHistoryReadingCompletedPassageSuccess,
  getDetailHistoryReadingCompletedPassageFailed,
  getDetailHistoryReadingSingle,
  getDetailHistoryReadingSingleSuccess,
  getDetailHistoryReadingSingleFailed,
  updateCheckButtonSaveReading,
  clearTempReadingSingleWhenNextQues,
 
}

function getListQuestionOfReadingWordFill(data){
  return{
    type: config.GET_LIST_QUESTION_OF_READING_WORD_FILL,
    payload: {data}
  }
}

function getListQuestionOfReadingWordFillSuccess(data){
  return{
    type: config.GET_LIST_QUESTION_OF_READING_WORD_FILL_SUCCESS,
    payload: {data}
  }
}

function getListQuestionOfReadingWordFillFail(data){
  return{
    type: config.GET_LIST_QUESTION_OF_READING_WORD_FILL_FAIL,
    payload: {data}
  }
}

function getResultQuestionOfReadingWordFill(id, userChoose, numberSelected){
  return{
    type: config.GET_RESULT_QUESTION_OF_READING_WORD_FILL,
    payload: {
      data:{
        id,
        userChoose,
        numberSelected
      }
    }
  }
}

function getResultQuestionOfReadingWordFillSuccess(data){
  return{
    type: config.GET_RESULT_QUESTION_OF_READING_WORD_FILL_SUCCESS,
    payload: {data}
  }
}

function getResultQuestionOfReadingWordFillFail(data){
  return{
    type: config.GET_RESULT_QUESTION_OF_READING_WORD_FILL_FAIL,
    payload: {data}
  }
}

function cleanListQuestionOfReadingWordFill() {
  return {
    type: config.CLEAN_LIST_QUESTION_OF_READING_WORD_FILL
  }
}

function cleanResultQuestionOfReadingWordFill() {
  return {
    type: config.CLEAN_RESULT_QUESTION_OF_READING_WORD_FILL
  }
}

function cleanNumberCorrectAnswer() {
  return {
    type: config.CLEAN_NUMBER_CORRECT_ANSWER
  }
}

function getNameTopic(data) {
  return {
    type: config.GET_NAME_TOPIC_READ,
    payload:{
      data
    }
  }
}

function cleanListQuestionAndAnswerReading() {
  return {
    type: config.CLEAN_LIST_QUESTION_AND_ANSWER_READING
  }
}

function getListQuestionOfReadingCompletedPassage(data) {
  return {
    type: config.GET_LIST_QUESTION_OF_READING_COMPLETED_PASSAGE,
    payload: { data }
  }
}

function getListQuestionOfReadingCompletedPassageSuccess(data) {
  return {
    type: config.GET_LIST_QUESTION_OF_READING_COMPLETED_PASSAGE_SUCCESS,
    payload: { data }
  }
}

function getListQuestionOfReadingCompletedPassageFail(data) {
  return {
    type: config.GET_LIST_QUESTION_OF_READING_COMPLETED_PASSAGE_FAIL,
    payload: { data }
  }
}

function getResultQuestionOfReadingCompletedPassage(data) {
  return {
    type: config.GET_RESULT_QUESTION_OF_READING_COMPLETED_PASSAGE,
    payload: { data}
  }
}

function getResultQuestionOfReadingCompletedPassageSuccess(data) {
  return {
    type: config.GET_RESULT_QUESTION_OF_READING_COMPLETED_PASSAGE_SUCCESS,
    payload:{ data}
  }
}

function getResultQuestionOfReadingCompletedPassageFail(data) {
  return {
    type: config.GET_RESULT_QUESTION_OF_READING_COMPLETED_PASSAGE_FAIL,
    payload: { data }
  }
}

function cleanListQuestionOfReadingCompletedPassage() {
  return {
    type: config.CLEAN_LIST_QUESTION_OF_READING_COMPLETED_PASSAGE
  }
}

function cleanResultQuestionOfReadingCompletedPassage() {
  return {
    type: config.CLEAN_RESULT_QUESTION_OF_READING_COMPLETED_PASSAGE
  }
}

function cleanNumberCorrectAnswerReadingCompletedPassage() {
  return {
    type: config.CLEAN_NUMBER_CORRECT_ANSWER_READING_COMPLETED_PASSAGE
  }
}

function getNameTopicReadingCompletedPassage(data) {
  return {
    type: config.GET_NAME_TOPIC_READ_COMPLETED_PASSAGE,
    payload: { data }
  }
}

function cleaningListQuestionAndAnswerReadingCompletedPassage() {
  return {
    type: config.CLEAN_LIST_QUESTION_AND_ANSWER_READING_COMPLETED_PASSAGE
  }
}

function getListSingleAndDualReadingQuestion(data){
  return{
    type:config.GET_LIST_SINGLE_AND_DUAL_READING_QUESTION,
    payload: {
      data:data
    }
  }
}
function getListSingleAndDualReadingQuestionSuccess(data){
  return{
    type:config.GET_LIST_SINGLE_AND_DUAL_READING_QUESTION_SUCCESS,
    payload:{
      data:data
    }
  }
}
function getListSingleAndDualReadingQuestionFaild(){
  return{
    type:config.GET_LIST_SINGLE_AND_DUAL_READING_QUESTION_FAILD
  }
}

function clearTempReadingSingleAndDualQuestion(){
  return{
    type:config.CLEAR_TEMP_READING_SINGLE_AND_DUAL_QUESTION
  }
}

function submitSingleAndDualReadingQuestion(data){
  return{
    type:config.SUBMIT_SINGLE_AND_DUAL_READING_QUESTION,
    payload:{
      data:data
    }
  }
}

function submitSingleAndDualReadingQuestionSuccess(data){
  return{
    type:config.SUBMIT_SINGLE_AND_DUAL_READING_QUESTION_SUCCESS,
    payload:{
      data:data
    }
  }
}

function submitSingleAndDualReadingQuestionFaild(){
  return{
    type:config.SUBMIT_SINGLE_AND_DUAL_READING_QUESTION_FAILD,
  }
}

function clearTotalTrueReadingSingleAndDual(){
  return{
    type:config.CLEAR_TOTAL_TRUE_READING_SINGLE_AND_DUAL_QUESTION,
  }
}
function saveScoreCorrect(totalScore,correct,totalSubCorrect, stt) {
  return {
    type: config.SAVE_SCORE_CORRECT,
    payload: {
      data: {totalScore,correct, totalSubCorrect, stt }
    }
  }
}
function getNameTopicPassage(data) {
  return {
    type: config.GET_NAME_TOPIC_READ_COMPLETED_PASSAGE,
    payload: {data}
  }
}

function setPartPracticeReading(data){
  return{
    type:config.SET_PART_PRACTICE_READING,
      payload:{
      data : data
    }
  }
}

function clearTotalQuestion(){
  return{
    type:config.CLEAR_TOTAL_QUESTIONS,
  }
}
function updateCheckReview() {
  return {
    type: config.UPDATE_CHECK_REVIEW
  }
}
function getIndex(data) {
  return {
    type: config.GET_INDEX,
    payload: {data}
  }
}
function getPart(data) {
  return {
    type: config.GET_PART,
    payload: { data}
  }
}
function saveListCorrect(numberCorrect, numberQues) {
  return {
    type: config.SAVE_LIST_CORRECT,
    payload: {
      data: { numberCorrect, numberQues }
    }
  }
}
function saveCurrentSumScore(current,sum) {
  return {
    type: config.SAVE_CURRENT_SUM_SCORE,
    payload: {
      data: {current, sum}
    }
  }
}
function updateCheckReviewFalse() {
  return {
    type: config.UPDATE_CHECK_REVIEW_FALSE
  }
}

function setTempAnsweredReadingSingleAndDualQuest (data){
  return{
    type:config.SET_TEMP_ANSWERED_READING_SINGLE_AND_DUAL_QUEST,
    payload:{data:data}
  }
}

function getLevelCode(levelCode) {
  return {
    type: config.GET_LEVEL_READ_COMPLETED_PASSAGE,
    payload: { data:{levelCode}}
  }
}

function storeDataToRepracticeReading(data){
  return {
    type:config.STORE_DATA_TO_REPRACTICE_READING,
    payload:{data:data}
  }
}

function setValueIndex() {
  return{
    type: config.SET_VALUE_INDEX
  }
}
function updateIsSubmit() {
  return {
    type: config.UPDATE_IS_SUBMIT
  }
}
function createdHistoryReadingWordFill(data) {
  return {
    type: config.CREATE_HISTORY_READING_WORD_FILL,
    payload:{data}
  }
}
function createdHistoryReadingWordFillSuccess(data) {
  return {
    type: config.CREATE_HISTORY_READING_WORD_FILL_SUCCESS,
    payload:{data}
  }
}
function createdHistoryReadingWordFillFailed(data) {
  return {
    type: config.CREATE_HISTORY_READING_WORD_FILL_FAILED,
    payload:{data}
  }
}
function getTopicId(data) {
  return{
    type:config.GET_TOPIC_ID,
    payload:{data}
  }
}
function getTypeCodeReading(data) {
  return {
    type: config.GET_TYPE_CODE_READING,
    payload: {data}
  }
}
function saveUserChoose(id, userChoose) {
  return {
    type: config.SAVE_USER_CHOOSE,
    payload:{ id, userChoose}
  }
}
function createHistoryReadingCompletedPassage(data) {
  return {
    type: config.CREATE_HISTORY_READING_COMPLETED_PASSAGE,
    payload:{data}
  }
}
function createHistoryReadingCompletedPassageSuccess(data) {
  return {
    type: config.CREATE_HISTORY_READING_COMPLETED_PASSAGE_SUCCESS,
    payload:{data}
  }
}
function createHistoryReadingCompletedPassageFailed(data) {
  return {
    type: config.CREATE_HISTORY_READING_COMPLETED_PASSAGE_FAILED,
    payload:{data}
  }
}
function createHistoryReadingSingleDual(data) {
  return {
    type: config.CREATE_HISTORY_READING_SINGLE_DUAL,
    payload:{data}
  }
}
function createHistoryReadingSingleDualSuccess(data) {
  return {
    type: config.CREATE_HISTORY_READING_SINGLE_DUAL_SUCCESS,
    payload:{data}
  }
}
function createHistoryReadingSingleDualFailed(data) {
  return {
    type: config.CREATE_HISTORY_READING_SINGLE_DUAL_FAILED,
    payload:{data}
  }
}
function cleanNumberSelected() {
  return{
    type:config.CLEAN_NUMBER_SELECTED
  }
}  

function getDetailHistoryReadingSingle(data) {
  return {
    type: config.GET_DETAIL_HISTORY_READING_SINGLEDUAL ,
    payload:{data}
  }
}
function getDetailHistoryReadingSingleSuccess(data) {
  return {
    type: config.GET_DETAIL_HISTORY_READING_SINGLEDUAL_SUCCESS,
    payload:{data}
  }
}
function getDetailHistoryReadingSingleFailed(data) {
  return {
    type: config.GET_DETAIL_HISTORY_READING_SINGLEDUAL_FAILED,
    payload:{data}
  }
}
function getHistoryInCompleteSentences(data) {
  return {
    type: config.GET_HISTORY_READING_INCOMPLETE_SENTENCES,
    payload:{data}
  }
}

function getHistoryInCompleteSentencesSuccess(data) {
  return {
    type: config.GET_HISTORY_READING_INCOMPLETE_SENTENCES_SUCCESS,
    payload:{data}
  }
}
function getHistoryInCompleteSentencesFailed(data) {
  return {
    type: config.GET_HISTORY_READING_INCOMPLETE_SENTENCES_FAILED,
  }
}
 
function updateViewBtnReading() {
  return {
    type: config.UPDATE_VIEW_BTN_READING
  }
}
function getDetailHistoryReadingCompletedPassage(data) {
  return {
    type: config.GET_DETAIL_HISTORY_READING_COMPLETED_PASSAGE,
    payload:{data}
  }
}
function getDetailHistoryReadingCompletedPassageSuccess(data) {
  return {
    type: config.GET_DETAIL_HISTORY_READING_COMPLETED_PASSAGE_SUCCESS,
    payload:{data}
  }
}
function getDetailHistoryReadingCompletedPassageFailed(data) {
  return {
    type: config.GET_DETAIL_HISTORY_READING_COMPLETED_PASSAGE_FAILED,
    payload:{data}
  }
}
function updateCheckButtonSaveReading() {
  return {
    type: config.UPDATE_CHECK_HIDDEN_BUTTON_SAVE_READING
  }
}
function clearTempReadingSingleWhenNextQues() {
  return {
    type: config.CLEAR_TEMP_READING_SINGLE_WHEN_NEXT_QUESTION
  }
}
export default readingActions;
