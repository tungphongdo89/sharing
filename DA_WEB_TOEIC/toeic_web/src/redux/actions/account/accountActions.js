import * as config from "../../../commons/constants/Config"

export const updateProfileAdmin = (data) => {
  return {
    type : config.UPDATE_PROFILE_ADMIN,
    payload : {
      data
    }
  }
}

export const updateProfileAdminSuccess = (data) => {
  return {
    type : config.UPDATE_PROFILE_ADMIN_SUCCESS,
    payload : {
      data
    }
  }
}

export const updateProfileAdminFail = () => {
  return {
    type : config.UPDATE_PROFILE_ADMIN_FAIL
  }
}

// -----------------------------------------------------------------------

export const settingAccount = (data) => {
  return {
    type: config.SETTING_ACCOUNT,
    payload: {
      data
    }
  }
}

export const loginSettingAccount  = () => {
  return {
    type: config.LOGIN_SETTING_ACCOUNT,
    payload: {}
  }
}
