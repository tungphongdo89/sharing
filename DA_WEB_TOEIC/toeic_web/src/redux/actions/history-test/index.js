import * as config from "../../../commons/constants/Config"

const historyTestActions={
   fetchHistoryTest,
   fetchHistoryTestSuccess,
   fetchHistoryTestFailed,
   getDetailOfTest,
   getDetailOfTestSuccess,
   getDetailOfTestFailed,
   getRankOfTest,
   getRankOfTestSuccess,
   getRankOfTestFailed,
   getHistoryFullTest,
   getHistoryFullTestSuccess,
   getHistoryFullTestFailed,
   getDetailHistoryFullTest,
   cleanHistory,
   updateActiveTabWhenFail
}


function fetchHistoryTest(data) {
  return {
    type: config.FETCH_HISTORY_TEST,
    payload: {data}
  }
}

function fetchHistoryTestSuccess(data) {
  return {
    type: config.FETCH_HISTORY_TEST_SUCCESS,
    payload: {data}
  }
}

function fetchHistoryTestFailed(data) {
  return {
    type: config.FETCH_HISTORY_TEST_FAILED,
    payload: {data}
  }
}

function getDetailOfTest(data) {
  return {
    type: config.GET_DETAIL_TEST,
    payload: {data}
  }
}

function getDetailOfTestSuccess(data) {
  return {
    type: config.GET_DETAIL_TEST_SUCCESS,
    payload: {data}
  }
}

function getDetailOfTestFailed(data) {
  return {
    type: config.GET_DETAIL_TEST_FAILED,
    payload: {data}
  }
}

function getRankOfTest(data) {
  return {
    type: config.GET_RANK_TEST,
    payload:{data}
  }
}

function getRankOfTestSuccess(data) {
  return {
    type: config.GET_RANK_TEST_SUCCESS,
    payload:{data}
  }
}

function getRankOfTestFailed(data) {
  return {
    type: config.GET_RANK_TEST_FAILED,
    payload:{data}
  }
}

function getHistoryFullTest(data) {
  return{
    type: config.GET_HISTORY_FULL_TEST,
    payload: {data}
  }
}

function getHistoryFullTestSuccess(data) {
  return{
    type: config.GET_HISTORY_FULL_TEST_SUCCESS,
    payload: {data}
  }
}

function getHistoryFullTestFailed(data) {
  return{
    type: config.GET_HISTORY_FULL_TEST_FAILDED,
    payload: {data}
  }
}

function getDetailHistoryFullTest(data) {
  return{
    type: config.GET_DETAIL_HISTORY_FULL_TEST,
    payload:{data}
  }
}
function cleanHistory() {
  return{
    type: config.CLEAN_HISTORY
  }
}
function updateActiveTabWhenFail(data) {
  return {
    type: config.UPDATE_ACTIVE_TAB_WHEN_FAIL,
    payload:{data}
  }
}
export default historyTestActions;
