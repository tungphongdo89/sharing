import * as config from "../../../commons/constants/Config";


const categoryAction = {
  addQuestionInForm,
  fetchCategory,
  fetchCategorySuccess,
  fetchCategoryFaild,
  createCategory,
  createCategorySuccess,
  createCategoryFailed,
  fetchDetailCategory,
  fetchDetailCategorySuccess,
  fetchDetailCategoryFailed,
  updateCategory,
  updateCategorySuccess,
  updateCategoryFailed,
  fetchDataFiler,
  fetchDataFilerSuccess,
  deleteCategory,
  deleteCategorySuccess,
  deleteCategoryFaild,
  fetchDataFilerFaild,
  getListTypeExercise,
  getListTypeExerciseSuccess,
  getListTypeExerciseFailed,
  getListPartExercise,
  getListPartExerciseSuccess,
  getListPartExerciseFailed,
  getListTypeLevel,
  getListTypeLevelSuccess,
  getListTypeLevelFailed,
  getListTypeFileUpload,
  getListTypeFileUploadSuccess,
  getListTypeFileUploadFailed,
  getListTopicByPart,
  getListTopicByPartSuccess,
  getListTopicByPartFailed,
  editDetailCategory
}

function addQuestionInForm() {

}

function fetchCategory(data) {
  return {
    type: config.FETCH_CATEGORY,
    payload: {data}
  }
}
function fetchCategorySuccess(data) {
  return {
    type: config.FETCH_CATEGORY_SUCCESS,
    payload: {data}
  }

}
function fetchCategoryFaild(data) {
  return {
    type: config.FETCH_CATEGORY_SUCCESS,
    payload: {data}
  }
}
function createCategory(data) {
  return {
    type: config.CREATE_CATEGORY,
    payload: {data}
  }
}
function createCategorySuccess(data) {
  return {
    type: config.CREATE_CATEGORY_SUCCESS,
    payload: {data}
  }
}
function createCategoryFailed(data) {
  return {
    type: config.CREATE_CATEGORY_FAILED,
    payload: {data}
  }
}
function updateCategory(data) {
  return {
    type: config.UPDATE_CATEGORY,
    payload: {
      data
    }
  }
}
function updateCategorySuccess(data) {
  return {
    type: config.UPDATE_CATEGORY_SUCCESS,
    payload: {
      data
    }
  }
}
function updateCategoryFailed(data) {
  return {
    type: config.UPDATE_CATEGORY_FAILED,
    payload: {
      data
    }
  }
}
function fetchDetailCategory(data, check) {
  return {
    type: config.FETCH_DETAIL_CATEGORY,
    payload: {data, check}
  }
}
function fetchDetailCategorySuccess(data, check){
  return {
    type: config.FETCH_DETAIL_CATEGORY_SUCCESS,
    payload:{data, check}
  }
}
function fetchDetailCategoryFailed(data, check){
  return {
    type: config.FETCH_DETAIL_CATEGORY_FAILED,
    payload: {data, check}
  }
}
function fetchDataFiler(data) {
  return {
    type: config.FETCH_DATA_FILTER,
    payload: {
      data
    }
  }

}

function fetchDataFilerSuccess(data) {
  return {
    type: config.FETCH_DATA_FILTER_SUCCESS,
    payload: {
      data
    }
  }
}

function fetchDataFilerFaild(data) {
  return {
    type: config.FETCH_DATA_FILTER_SUCCESS,
    payload: {
      data
    }
  }

}
function deleteCategory(data) {
  return {
    type: config.DELETE_CATEGORY,
    payload: {
      data
    }
  }
}function deleteCategorySuccess(data) {
  return {
    type: config.DELETE_CATEGORY_SUCCESS,
    payload: {
      data
    }
  }
}function deleteCategoryFaild(data) {
  return {
    type: config.DELETE_CATEGORY_FAILD,
    payload: {
      data
    }
  }
}

function getListTypeExercise() {
  return {
    type: config.GET_LIST_TYPE_EXERCISE
  }
}
function getListTypeExerciseSuccess(data) {
  return {
    type: config.GET_LIST_TYPE_EXERCISE_SUCCESS,
    payload: {
      data
    }
  }
}
function getListTypeExerciseFailed(data) {
  return {
    type: config.GET_LIST_TYPE_EXERCISE_FAILED,
    payload: {
      data
    }
  }
}
function getListPartExercise(data) {
  return {
    type: config.GET_LIST_PART_EXERCISE,
    payload : {data}
  }
}
function getListPartExerciseSuccess(data) {
  return {
    type: config.GET_LIST_PART_EXERCISE_SUCCESS,
    payload: {
      data
    }
  }
}
function getListPartExerciseFailed(data) {
  return {
    type: config.GET_LIST_PART_EXERCISE_FAILED,
    payload: {
      data
    }
  }
}
function getListTypeLevel() {
  return {
    type: config.GET_LIST_TYPE_LEVEL
  }
}
function getListTypeLevelSuccess(data) {
  return {
    type: config.GET_LIST_TYPE_LEVEL_SUCCESS,
    payload: {
      data
    }
  }
}
function getListTypeLevelFailed(data) {
  return {
    type: config.GET_LIST_TYPE_LEVEL_FAILED,
    payload: {
      data
    }
  }
}
function getListTypeFileUpload() {
  return {
    type: config.GET_LIST_TYPE_FILE_UPLOAD
  }
}
function getListTypeFileUploadSuccess(data) {
  return {
    type: config.GET_LIST_TYPE_FILE_UPLOAD_SUCCESS,
    payload: {
      data
    }
  }
}
function getListTypeFileUploadFailed(data) {
  return {
    type: config.GET_LIST_TYPE_FILE_UPLOAD_FAILED,
    payload: {
      data
    }
  }
}
function getListTopicByPart(data) {
  return {
    type: config.GET_LIST_TOPIC_BY_PART,
    payload : {data}
  }
}
function getListTopicByPartSuccess(data) {
  return {
    type: config.GET_LIST_TOPIC_BY_PART_SUCCESS,
    payload: {
      data
    }
  }
}
function getListTopicByPartFailed(data) {
  return {
    type: config.GET_LIST_TOPIC_BY_PART_FAILED,
    payload: {
      data
    }
  }
}
function editDetailCategory() {
  return {
    type: config.EDIT_DETAIL_CATEGORY
  }

}
export default categoryAction;
