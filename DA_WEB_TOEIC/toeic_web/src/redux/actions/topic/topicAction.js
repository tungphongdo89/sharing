import * as config from '../../../commons/constants/Config';

const topicAction = {
  fetchTopic,
  fetchTopicSuccess,
  fetchTopicFaild,
  deleteTopic,
  deleteTopicSuccess,
  deleteTopicFaild,
  addTopic,
  addTopicSuccess,
  addTopicFaild,
  updateTopic,
  updateTopicFaild,
  updateTopicSuccess,
  fetchTopicByPart,
  fetchTopicByPartSuccess,
  fetchTopicByPartFailude,

  fetchTopicByPartAndLevel,
  fetchTopicByPartAndLevelSuccess,
  fetchTopicByPartAndLevelFail,

  fetchTypeTopic,
  fetchTypeTopicSuccess,
  fetchTypeTopicFalied,
  fetchPartTopicByType,
  fetchPartTopicByTypeSuccess,
  fetchPartTopicByTypeFailed,

  fetchListPractices,
  fetchListPracticesSuccess,
  fetchListPracticesFailude,
  fetchPartByTopic,
  fetchPartByTopicSuccess,
  fetchTopicNameByPartAndTypeTopic,

  fetchTopicNameByPartAndTypeTopicSuccess,

  getMenuListTest,
  getMenuListTestSuccess,
  getMenuListTestFailed

}

function fetchTopic(data) {
  return {
    type: config.FETCH_TOPIC,
    payload: {data}
  }

}
function fetchTopicSuccess(data) {
  return{
    type: config.FETCH_TOPIC_SUCCESS,
    payload: {data}
  }

}
function fetchTopicFaild(data) {
  return{
    type: config.FETCH_TOPIC_FAILD,
    payload: {data}
  }

}
function deleteTopic(data) {
  return {
    type: config.DELETE_TOPIC,
    payload: {data}
  }

}
function deleteTopicSuccess(data) {
  return{
    type: config.DELETE_TOPIC_SUCCESS,
    payload: {
      data
    }
  }

}
function deleteTopicFaild(data) {
  return{
    type: config.DELETE_TOPIC_FAILD,
    payload: {
      data
    }
  }

}
function addTopic(data) {
  return{
    type: config.ADD_TOPIC,
    payload : {data}
  }

}
function addTopicFaild(data) {
  return{
    type: config.ADD_TOPIC_FAILD,
    payload: {data}
  }

}
function addTopicSuccess(data) {
  return{
    type: config.ADD_TOPIC_SUCCESS,
    payload: {data}
  }

}
function updateTopic(data) {
  return{
    type: config.UPDATE_TOPIC,
    payload : {data}
  }

}
function updateTopicFaild(data) {
  {
    return{
      type: config.UPDATE_TOPIC_FAILD,
      payload:{data}
    }
  }

}

function updateTopicSuccess(data) {
  {
    return{
      type: config.UPDATE_TOPIC_SUCCESS,
      payload:{data}
    }
  }
}

//---------------------------------------------------------------

function fetchTopicByPartAndLevel(paramCode, paramValue) {
  return {
    type: config.FETCH_TOPIC_BY_PART_LEVEL,
    payload: {
      data:{
        paramCode,
        paramValue
      }
    }
  }
}

function fetchTopicByPartAndLevelSuccess(data) {
  return {
    type: config.FETCH_TOPIC_BY_PART_LEVEL_SUCCESS,
    payload: {
      data
    }
  }
}

function fetchTopicByPartAndLevelFail() {
  return {
    type: config.FETCH_TOPIC_BY_PART_LEVEL_FAIL
  }
}

//-------------------------------------------

function fetchTopicByPart(part) {
  return {
    type: config.FETCH_TOPIC_BY_PART,
    payload: {
      part
    }
  }
}

function fetchTopicByPartSuccess(data) {
  return {
    type: config.FETCH_TOPIC_BY_PART_SUCCESS,
    payload: {
      data
    }
  }
}

function fetchTopicByPartFailude() {
  return {
    type: config.FETCH_TOPIC_BY_PART_FAILUDE
  }
}

function fetchListPractices() {
	  return {
	    type: config.FETCH_LIST_PRACTICES
	  }
	}

	function fetchListPracticesSuccess(data) {
	  return {
	    type: config.FETCH_LIST_PRACTICES_SUCCESS,
	    payload: {
	      data
	    }
	  }
	}

function fetchListPracticesFailude() {
  return {
    type: config.FETCH_LIST_PRACTICES_FAILUDE
  }
}
function fetchTypeTopicSuccess(data) {
  return {
    type: config.FETCH_TYPE_TOPIC_SUCCESS,
    payload: {
      data
    }
  }
}
function fetchTypeTopic(data) {
  return {
    type: config.FETCH_TYPE_TOPIC,
    payload: {
      data
    }
  }
}
function fetchPartByTopic(data) {
  return {
    type: config.FETCH_PART_BY_TOPIC,
    payload: {
      data
    }
  }
}
function fetchPartByTopicSuccess(data) {
  return {
    type: config.FETCH_PART_BY_TOPIC_SUCCESS,
    payload: {
      data
    }
  }
}
function fetchTypeTopicFalied() {
  return {
    type: config.FETCH_TYPE_TOPIC_FAILED
  }
}

function fetchTopicNameByPartAndTypeTopic(data) {
  return {
    type: config.FETCH_TOPIC_NAME_BY_TYPE_TOPIC_AND_PART,
    payload: {
      data
    }
  }
}
function fetchTopicNameByPartAndTypeTopicSuccess(data) {
  return {
    type: config.FETCH_TOPIC_NAME_BY_TYPE_TOPIC_AND_PART_SUCCESS,
    payload: {
      data
    }
  }
}

function fetchPartTopicByType(type) {
  return {
    type: config.FETCH_PART_TOPIC_BY_TYPE,
    payload: {
      type
    }
  }
}

function fetchPartTopicByTypeSuccess(data) {
  return {
    type: config.FETCH_PART_TOPIC_BY_TYPE_SUCCESS,
    payload: {
      data
    }
  }
}

function fetchPartTopicByTypeFailed() {
  return {
    type: config.FETCH_PART_TOPIC_BY_TYPE_FAILED
  }
}
function fetchTopicByTypeTopicAndPart({data}) {
  return{
    type: config.FETCH_TOPIC_NAME_BY_TYPE_TOPIC_AND_PART,
    payload:{data}
  }

}
function fetchTopicByTypeTopicAndPartSuccess({data}) {
  return{
    type: config.FETCH_TOPIC_NAME_BY_TYPE_TOPIC_AND_PART_SUCCESS,
    payload:{data}
  }

}

//GET_MENU_LIST_TEST
function getMenuListTest() {
  return {
    type: config.GET_MENU_LIST_TEST,
    payload: {
    }
  }
}

function getMenuListTestSuccess(data) {
  return {
    type: config.GET_MENU_LIST_TEST_SUCCESS,
    payload: {
      data
    }
  }
}

function getMenuListTestFailed() {
  return {
    type: config.GET_MENU_LIST_TEST_FAILED
  }
}



export default topicAction
