import * as config from "../../../commons/constants/Config";

const resendEmailAction = {
  resendEmail,
  resendEmailSuccess,
  resendEmailFailed

}
function resendEmail(data){
  return{
    type: config.RESEND_EMAIL,
    payload:{
      data
    }
  }
}
function resendEmailSuccess(data) {
  return {
    type: config.RESEND_EMAIL_SUCCESS,
    payload:{
      data
    }
  }
}
function resendEmailFailed(data) {
  return {
    type: config.RESEND_EMAIL_FAILED,
    payload:{
      data
    }
  }
}
export default resendEmailAction;
