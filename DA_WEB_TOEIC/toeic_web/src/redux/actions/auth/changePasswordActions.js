import * as config from "../../../commons/constants/Config";

const changePasswordAction ={
    changePassword,
    changePasswordSuccess,
    changePasswordFailed
    
}
 function changePassword (data) {
  
    return {
        type: config.CHANGE_PASSWORD,
        payload: {
            data
        }
    }
}

function changePasswordSuccess (data){
    return {
        type: config.CHANGE_PASSWORD_SUCCESS,
        payload: {
            data
        }
    }
}

function changePasswordFailed (data) {
    return {
        type: config.CHANGE_PASSWORD_FAILED,
        payload: {
            data
        }
    }
}
export default changePasswordAction;