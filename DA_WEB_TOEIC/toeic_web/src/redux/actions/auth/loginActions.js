import * as config from "../../../commons/constants/Config";
import {removeSession} from "../../../commons/configCookie";


// Init firebase if not already initialized
const loginActions = {
  logoutWithJWT,
  loginWithJWT,
  loginSuccess,
  loginFalure,
  changeRole,
}

function loginWithJWT(data) {
  return {
    type: config.LOGIN_USER_REQUEST,
    payload: {
      data
    }
  }
  // debugger
  // return dispatch => {
  //   axios
  //     .post("/api/authenticate/login/user", {
  //       email: user.email,
  //       password: user.password
  //     })
  //     .then(response => {
  //       var loggedInUser
  //
  //       if (response.data) {
  //         loggedInUser = response.data.user
  //
  //         dispatch({
  //           type: "LOGIN_WITH_JWT",
  //           payload: {loggedInUser, loggedInWith: "jwt"}
  //         })
  //
  //         history.push("/")
  //       }
  //     })
  //     .catch(err => console.log(err))
  // }
}

function loginSuccess(data) {
  return {
    type: config.LOGIN_USER_SUCCESS,
    payload: {
      data
    }
  }
}

function loginFalure(data) {
  return {
    type: config.LOGIN_USER_FAILURE,
    payload: {
      data
    }
  }
}

// function logout() {
//   return {
//     type: config.LOGOUT_USER,
//     payload: {
//     }
//   }
// }

function logoutWithJWT() {
  // removeSession()
  // history.push("/pages/login")
  return {
    type: config.LOGOUT_USER, payload: {}
  }
}

function changeRole() {
  return {
    type: config.CHANGE_ROLE, payload: {}
  }
}
export default loginActions
