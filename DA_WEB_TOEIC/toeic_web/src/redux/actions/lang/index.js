import * as config from "../../../commons/constants/Config"

const langgueActions = {
  changeLanggue,
}


function changeLanggue(data) {
  return {
    type: config.CHANGE_LANGUAGE,
    payload: {data}
  }
}

export default langgueActions;
