import * as config from '../../../commons/constants/Config';

const historyPracticesAction = {

  getListHistoryPractices,
  getListHistoryPracticesSuccess,
  getListHistoryPracticesFailed,

  changeActiveTab,

  getTypeForHistoryPractices,
  getTypeForHistoryPracticesSuccess,
  getTypeForHistoryPracticesFailed,

  getPartForHistoryPractices,
  getPartForHistoryPracticesSuccess,
  getPartForHistoryPracticesFailed,

  getTopicForHistoryPractices,
  getTopicForHistoryPracticesSuccess,
  getTopicForHistoryPracticesFailed,


  saveResultHistoryListening,
  saveResultHistoryListeningSuccess,
  saveResultHistoryListeningFailed,

  showButtonSaveResultListening,

  saveResultHistoryLisSingle,
  saveResultHistoryLisSingleSuccess,
  saveResultHistoryLisSingleFailed,

  cleanHiddenBtnBeforeGetLisSingle,
  cleanHiddenBtnBeforeGetListening,
  updateCheckHiddenButtonSaveP3P4,

}

//------------change active tab-----------------
function changeActiveTab(data) {
  return {
    type: config.CHANGE_ACTIVE_TAB,
    payload: {data}
  }

}
//------------change active tab-----------------


//----------------get list history practices--------------------------
function getListHistoryPractices(data) {
  return {
    type: config.GET_LIST_HISTORY_PRACTICES,
    payload: {data}
  }

}
function getListHistoryPracticesSuccess(data) {
  return{
    type: config.GET_LIST_HISTORY_PRACTICES_SUCCESS,
    payload: {data}
  }

}
function getListHistoryPracticesFailed(data) {
  return{
    type: config.GET_LIST_HISTORY_PRACTICES_FAILED,
    payload: {data}
  }
}
//----------------end get list history practices--------------------------


//----------------Get type for history practices---------------------------
function getTypeForHistoryPractices() {
  return {
    type: config.GET_TYPE_FOR_HISTORY_PRACTICES,
    payload: {}
  }

}
function getTypeForHistoryPracticesSuccess(data) {
  return{
    type: config.GET_TYPE_FOR_HISTORY_PRACTICES_SUCCESS,
    payload: {data}
  }

}
function getTypeForHistoryPracticesFailed(data) {
  return{
    type: config.GET_TYPE_FOR_HISTORY_PRACTICES_FAILED,
    payload: {data}
  }
}
//----------------End get type for history practices---------------------------


//----------------Get part for history practices---------------------------
function getPartForHistoryPractices(data) {
  return {
    type: config.GET_PART_FOR_HISTORY_PRACTICES,
    payload: {data}
  }

}
function getPartForHistoryPracticesSuccess(data) {
  return{
    type: config.GET_PART_FOR_HISTORY_PRACTICES_SUCCESS,
    payload: {data}
  }

}
function getPartForHistoryPracticesFailed(data) {
  return{
    type: config.GET_PART_FOR_HISTORY_PRACTICES_FAILED,
    payload: {data}
  }
}
//----------------End get part for history practices---------------------------

//----------------Get topic for history practices---------------------------
function getTopicForHistoryPractices(data) {
  return {
    type: config.GET_TOPIC_FOR_HISTORY_PRACTICES,
    payload: {data}
  }

}
function getTopicForHistoryPracticesSuccess(data) {
  return{
    type: config.GET_TOPIC_FOR_HISTORY_PRACTICES_SUCCESS,
    payload: {data}
  }

}
function getTopicForHistoryPracticesFailed(data) {
  return{
    type: config.GET_TOPIC_FOR_HISTORY_PRACTICES_FAILED,
    payload: {data}
  }
}
//----------------End get topic for history practices---------------------------


//----------------save result history listening---------------------------
function saveResultHistoryListening(data) {
  return {
    type: config.SAVE_RESULT_HISTORY_LISTENING,
    payload: {data}
  }

}
function saveResultHistoryListeningSuccess(data) {
  return{
    type: config.SAVE_RESULT_HISTORY_LISTENING_SUCCESS,
    payload: {data}
  }

}
function saveResultHistoryListeningFailed(data) {
  return{
    type: config.SAVE_RESULT_HISTORY_LISTENING_FAILED,
    payload: {data}
  }
}
//----------------End save result history listening---------------------------


//---------------show button save result listening -----------------------
function showButtonSaveResultListening(data) {
  return {
    type: config.SHOW_BUTTON_SAVE_RESULT_LISTENING
  }
}
//---------------show button save result listening -----------------------


//----------------save result history lis single---------------------------
function saveResultHistoryLisSingle(data) {
  return {
    type: config.SAVE_RESULT_HISTORY_LIS_SINGLE,
    payload: {data}
  }

}
function saveResultHistoryLisSingleSuccess(data) {
  return{
    type: config.SAVE_RESULT_HISTORY_LIS_SINGLE_SUCCESS,
    payload: {data}
  }

}
function saveResultHistoryLisSingleFailed(data) {
  return{
    type: config.SAVE_RESULT_HISTORY_LIS_SINGLE_FAILED,
    payload: {data}
  }
}
//----------------End save result history lis single---------------------------


//------------clean hidden button before get listening---------------
function cleanHiddenBtnBeforeGetLisSingle() {
  return {
    type: config.CLEAN_HIDDEN_BUTTON_SAVE_RESULT_LIS_SINGLE
  }
}

function cleanHiddenBtnBeforeGetListening() {
  return {
    type: config.CLEAN_HIDDEN_BUTTON_SAVE_RESULT_LISTENING
  }

}
function updateCheckHiddenButtonSaveP3P4() {
  return {
    type: config.UPDATE_CHECK_HIDDEN_BUTTON_SAVE_P3_P4
  }
}
//------------end clean hidden button before get listening---------------






export default historyPracticesAction
