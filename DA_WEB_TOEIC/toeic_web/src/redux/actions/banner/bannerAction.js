import * as config from "../../../commons/constants/Config"

const bannerAction={
  getBanner,
  getBannerSuccess,
  getBannerFailure,
  createBanner,
  createBannerSuccess,
  createBannerFailure,
  showBannerActive,
  showBannerActiveSuccess,
  showBannerActiveFailed,
  getListBannerNotActive,
  getListBannerNotActiveSuccess,
  getListBannerNotActiveFailed,
  getListBannerActive,
  getListBannerActiveSuccess,
  getListBannerActiveFailed,
  deleteBanner,
  deleteBannerSuccess,
  deleteBannerFailed,
  updateBanner,
  updateBannerSuccess,
  updateBannerFailed,
  updateOrderBanner,
  updateOrderBannerSuccess,
  updateOrderBannerFailed,
  updateStatusPopup,
}
function getBanner() {
  return {
    type : config.GET_BANNER
  }
}

function getBannerSuccess(data){
  return {
    type : config.GET_BANNER_SUCCESS,
    payload : {
      data
    }
  }
}

function getBannerFailure(){
  return {
    type : config.GET_BANNER_FAILURE
  }
}

function createBanner(data){
  return {
    type : config.CREATE_BANNER,
    payload : {
      data
    }
  }
}

function createBannerSuccess (data){
  return {
    type : config.CREATE_BANNER_SUCCESS,
    payload : {
      data
    }
  }
}

function createBannerFailure(data){
  return{
    type: config.CREATE_BANNER_FAILURE,
    payload:{
      data
    }
  }
}

function showBannerActive (){
  return {
    type : config.SHOW_BANNER_ACTIVE,
  }
}

function showBannerActiveSuccess(data){
  return {
    type : config.SHOW_BANNER_ACTIVE_SUCCESS,
    payload : {
      data
    }
  }
}

function showBannerActiveFailed(data){
  return {
    type : config.SHOW_BANNER_ACTIVE_FAILED,
    payload : {
      data
    }
  }
}

function getListBannerNotActive(data) {
  return{
    type: config.GET_LIST_BANNER_NOT_ACTICVE,
    payload:{data}
  }
}
function getListBannerNotActiveSuccess(data) {
  return {
    type: config.GET_LIST_BANNER_NOT_ACTICVE_SUCCESS,
    payload:{data}
  }
}
function getListBannerNotActiveFailed(data) {
  return {
    type: config.GET_LIST_BANNER_NOT_ACTICVE_FAILED,
    payload:{data}
  }
}

function getListBannerActive() {
  return{
    type: config.GET_LIST_BANNER_ACTICVE,
  }
}
function getListBannerActiveSuccess(data) {
  return {
    type: config.GET_LIST_BANNER_ACTICVE_SUCCESS,
    payload:{data}
  }
}
function getListBannerActiveFailed(data) {
  return {
    type: config.GET_LIST_BANNER_ACTICVE_FAILED,
    payload:{data}
  }
}
function deleteBanner(data) {
  return {
    type: config.DELETE_BANNER,
    payload:{data}
  }
}

function deleteBannerSuccess(data) {
  return {
    type: config.DELETE_BANNER_SUCCESS,
    payload:{data}
  }
}

function deleteBannerFailed(data) {
  return {
    type: config.DELETE_BANNER_FAILED,
    payload:{data}
  }
}
function updateBanner(data) {
  return {
    type: config.UPDATE_BANNER,
    payload:{data}
  }
}

function updateBannerSuccess(data) {
  return {
    type: config.UPDATE_BANNER_SUCCESS,
    payload:{data}
  }
}

function updateBannerFailed(data) {
  return {
    type: config.UPDATE_BANNER_FAILED,
    payload:{data}
  }
}

function updateOrderBanner(data) {
  return {
    type: config.UPDATE_ORDER_BANNER,
    payload: {data}
  }
}
function updateOrderBannerSuccess(data) {
  return {
    type: config.UPDATE_ORDER_BANNER_SUCCESS,
    payload: {data}
  }
}
function updateOrderBannerFailed(data) {
  return {
    type: config.UPDATE_ORDER_BANNER_FAILED,
    payload: {data}
  }
}
function updateStatusPopup() {
  return {
    type: config.UPDATE_STATUS_POPUP
  }
}
export default bannerAction;
