 import *  as actionType from '../../../commons/constants/Config'

const dictionaryAction={
    createDictionary,
    createDictionarySuccess,
    createDictionaryFailed,
    fetchDictionary,
    fetchDictionarySuccess,
    fetchDictionaryFailed,
    deleteDictionary,
    deleteDictionaryFailed,
    deleteDictionarySuccess,
    updateDictionary,
    updateDictionarySuccess,
    updateDictionaryFailed,
    updateDictionaryStatusPopup,
    updateDictionaryPlayerList
}


function createDictionary(data,keySearch){
    return {
      type : actionType.CREATE_DICTIONARY,
      payload : {
        data,
        keySearch
      }
    }
  }

function createDictionarySuccess(data){
    return {
        type : actionType.CREATE_DICTIONARY_SUCCESS,
        payload :{
            data
        }

    }
}

function createDictionaryFailed(data){
  return {
    type : actionType.CREATE_DICTIONARY_FAILED,
    payload :{
      data
    }

  }
}


function fetchDictionary(data){
  return{
    type : actionType.FETCH_DICTIONARY,
    payload : {
      data

    }
  }

}


function fetchDictionarySuccess(data){
  return{
    type : actionType.FETCH_DICTIONARY_SUCCESS,
    payload : {
      data
    }
  }

}

function fetchDictionaryFailed(data){
  return{
    type : actionType.FETCH_DICTIONARY_FAILED,
    payload : {
      data
    }
  }

}


function deleteDictionary(data) {

  return{
    type : actionType.DELETE_DICTIONARY,
    payload : {
      data
    }
  }
}
function deleteDictionarySuccess(data) {

  return{
    type : actionType.DELETE_DICTIONARY_SUCCESS,
    payload : {
      data
    }
  }
}
function deleteDictionaryFailed(data) {

  return{
    type : actionType.DELETE_DICTIONARY_FAILED,
    payload : {
      data
    }
  }
}


function updateDictionary(data,page,keySearch) {

  return{
    type : actionType.UPDATE_DICTIONARY,
    payload : {
      data,
      page,
      keySearch
    }
  }
}

function updateDictionarySuccess(data) {
  return{
    type : actionType.UPDATE_DICTIONARY_SUCCESS,
    payload : {
      data
    }
  }
}

function updateDictionaryFailed(data) {

  return {
    type: actionType.UPDATE_DICTIONARY_FAILED,
    payload: {
      data
    }
  }
}
function updateDictionaryStatusPopup(data) {
  return {
    type: actionType.UPDATE_DICTIONARY_STATUS_POPUP,
    payload: {data}
  }
}
function updateDictionaryPlayerList(data){
  return {
      type : actionType.UPDATE_DICTIONARY_PLAYER_LIST,
      payload :{
          data
      }

  }
}

export default dictionaryAction;
