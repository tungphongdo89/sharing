import * as config from '../../../commons/constants/Config';

const testAction = {
  //lấy list question minitest
  getListQuestionMinitest,
  getListQuestionMinitestSuccess,
  getListQuestionMinitestFailed,
  getListQuesListeningChoosen,
  getListQuesReadingChoosen,

  //submit answer minitest
  submitAnswerMinitest,
  submitAnswerMinitestSuccess,
  submitAnswerMinitestFailed,

  doCurrentTestAgain,
  doReadingAfterListening,
  doReadingAfterListeningFulltest,

  getDetailFullTest,
  getDetailFullTestSuccess,
  getDetailFullTestFailed,
  getResultFullTest,
  getResultFullTestSuccess,
  getResultFullTestFailed,
  cleanTest,
  saveFullTest,
  cleanListChoosen,

  noTranslationWhenDoingTest,
  saveDoTime,

  // history minitest
  saveResultMinitest,
  saveResultMinitestSuccess,
  saveResultMinitestFailed,

  getListHistoryMinitest,
  getListHistoryMinitestSuccess,
  getListHistoryMinitestFailed,

  checkGetDetailOrSubmitMinitest,

  showHideButtonBackHistoryMinitest,

  getListNameTest,
  getListNameTestSuccess,
  getListNameTestFail,
}

function getListQuestionMinitest() {
  return {
    type: config.GET_LIST_QUESTION_MINITEST,
}

}
function getListQuestionMinitestSuccess(data) {
  return{
    type: config.GET_LIST_QUESTION_MINITEST_SUCCESS,
    payload: {data}
  }

}
function getListQuestionMinitestFailed(data) {
  return{
    type: config.GET_LIST_QUESTION_MINITEST_FAILED,
    payload: {data}
  }
}

//-----lấy danh sách các câu hỏi đã chọn đáp án
function getListQuesListeningChoosen(data) {
  return {
    type: config.GET_LIST_QUESTION_LISTENING_CHOOSEN,
    payload: {
      data
    }
  }
}

function getListQuesReadingChoosen(data) {
  return {
    type: config.GET_LIST_QUESTION_READING_CHOOSEN,
    payload: {
      data
    }
  }
}

//--------------do current test again----------------------
function doCurrentTestAgain(data) {
  return {
    type: config.DO_CURRENT_TEST_AGAIN,
    payload: {
      data
    }
  }
}

//--------------do reading after listening----------------------
function doReadingAfterListening(data) {
  return {
    type: config.DO_READING_AFTER_LISTENING,
    payload: {
      data
    }
  }
}

function doReadingAfterListeningFulltest(data) {
  return {
    type: config.DO_READING_AFTER_LISTENING_FULLTEST,
    payload: {
      data
    }
  }
}

//----------------check get detail history or submit minitest--------------------------
function checkGetDetailOrSubmitMinitest(data) {
  return {
    type: config.CHECK_GET_DETAIL_OR_SUBMIT_MINITEST,
    payload: {data}
  }

}
//----------------end check get detail history or submit minitest--------------------------


//----------------showHideButtonBackHistoryMinitest------------------------------
function showHideButtonBackHistoryMinitest(data) {
  return {
    type: config.SHOW_HIDE_BTN_BACK_HISTORY_MINITEST,
    payload: {data}
  }

}
//----------------end showHideButtonBackHistoryMinitest------------------------------


//----------------submit answer minitest--------------------------
function submitAnswerMinitest(data) {
  return {
    type: config.SUBMIT_ANSWER_MINITEST,
    payload: {data}
  }

}
function submitAnswerMinitestSuccess(data) {
  return{
    type: config.SUBMIT_ANSWER_MINITEST_SUCCESS,
    payload: {data}
  }

}
function submitAnswerMinitestFailed(data) {
  return{
    type: config.SUBMIT_ANSWER_MINITEST_FAILED,
    payload: {data}
  }
}
//----------------end submit answer minitest--------------------------



//----------------save result minitest--------------------------
function saveResultMinitest(data) {
  return {
    type: config.SAVE_RESULT_MINITEST,
    payload: {data}
  }

}
function saveResultMinitestSuccess(data) {
  return{
    type: config.SAVE_RESULT_MINITEST_SUCCESS,
    payload: {data}
  }

}
function saveResultMinitestFailed(data) {
  return{
    type: config.SAVE_RESULT_MINITEST_FAILED,
    payload: {data}
  }
}
//----------------end save result minitest--------------------------


//----------------get list history minitest--------------------------
function getListHistoryMinitest(data) {
  return {
    type: config.GET_LIST_HISTORY_MINITEST,
    payload: {data}
  }

}
function getListHistoryMinitestSuccess(data) {
  return{
    type: config.GET_LIST_HISTORY_MINITEST_SUCCESS,
    payload: {data}
  }

}
function getListHistoryMinitestFailed(data) {
  return{
    type: config.GET_LIST_HISTORY_MINITEST_FAILED,
    payload: {data}
  }
}
//----------------end get list history minitest--------------------------


function getDetailFullTest(data) {
  return{
    type: config.GET_DETAIL_FULL_TEST,
    payload:{data}
  }
}

function getDetailFullTestSuccess(data) {
  return{
    type: config.GET_DETAIL_FULL_TEST_SUCCESS,
    payload:{data}
  }
}

function getDetailFullTestFailed(data) {
  return{
    type: config.GET_DETAIL_FULL_TEST_FAILED,
    payload:{data}
  }
}

function getResultFullTest(data) {
  return{
    type: config.GET_RESULT_FULL_TEST,
    payload:{data}
  }
}

function getResultFullTestSuccess(data) {
  return{
    type: config.GET_RESULT_FULL_TEST_SUCCESS,
    payload:{data}
  }
}

function getResultFullTestFailed(data) {
  return{
    type: config.GET_RESULT_FULL_TEST_FAILED,
    payload:{data}
  }
}
function cleanTest() {
  return{
    type: config.CLEAN_TEST
  }
}
function saveFullTest(data) {
  debugger
  return {
    type: config.SAVE_FULL_TEST,
    payload: {data}
  }
}
function cleanListChoosen() {
  return {
    type: config.CLEAN_LIST_CHOOSEN
  }
}

//---------------Dịch câu------------
function noTranslationWhenDoingTest(data) {
  return {
    type: config.NO_TRANSLATION_WHEN_DOING_TEST,
    payload:{data}
  }
}
function saveDoTime(data) {
  return {
    type: config.SAVE_DO_TIME,
    payload:{data}
  }
}

function getListNameTest() {
  return {
    type:config.GET_LIST_NAME_TEST
  }
}

function getListNameTestSuccess(data) {
  return {
    type:config.GET_LIST_NAME_TEST_SUCCESS,
    payload:{data}
  }
}

function getListNameTestFail(data) {
  return {
    type:config.GET_LIST_NAME_TEST_FAIL,
    payload:{data}
  }
}

export default testAction
