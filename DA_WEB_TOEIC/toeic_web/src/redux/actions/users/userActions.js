import * as config from "../../../commons/constants/Config";

const userAction ={
  searchUser,
  fetchUserDetail,
  fetchUser,
  fetchUserSuccess,
  fetchUserFaild,
  updateUserDetail,
  updateUserDetailSuccess,
  updateUserDetailFailed,
  createUser,
  createUserInvitedFailude
}

function searchUser(data) {
  return{
    type:config.SEARCH_USER,
    payload:{
      data
    }
  }

}
function fetchUserDetail(data) {
  return {
    type: config.FETCH_USER_DETAIL,
    payload: {
      data
    }
  }
}

function updateUserDetail(data){
  return {
    type: config.UPDATE_USER_DETAIL,
    payload:{
      data
    }
  }
}

function updateUserDetailSuccess(data) {
  return {
    type: config.UPDATE_USER_DETAIL_SUCCESS,
    payload:{
      data
    }
  }
}

function updateUserDetailFailed(data) {
  return {
    type: config.UPDATE_USER_DETAIL_FAILED,
    payload: {
      data
    }
  }
}
function fetchUser(data) {
  return {
    type: config.FETCH_USER,
    payload: {
      data
    }
  }

}
function fetchUserFaild(data) {
  return {
    type: config.FETCH_USER_FAILURE,
    payload:{
      data
    }
  }

}
function fetchUserSuccess(data) {
  return {
    type:config.FETCH_USER_SUCCESS,
    payload:{
      data
    }
  }
}

function createUser(data) {
  return {
    type: config.CREATE_USER_ADMIN_INVITED,
    payload: {
      data
    }
  }
}

function createUserInvitedFailude () {
  return {
    type: config.CREATE_USER_ADMIN_INVITED_FAILUE,
  }
}

export default userAction;
