import {HIDE_SIDEBAR, SHOW_SIDEBAR} from '../../../commons/constants/Config'

const SideBarAction = {
  showSideBar,
  hideSideBar
}

function showSideBar(data) {
  return {
    type: SHOW_SIDEBAR,
    payload: {
      data
    }
  }
}

function hideSideBar(data) {
  return {
    type: HIDE_SIDEBAR,
    payload: {
      data
    }
  }
}

export default SideBarAction
