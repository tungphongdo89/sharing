import *  as config from '../../../commons/constants/Config'

const wordAction={
  setHighLightWord,
  setCordinatePointer, 
  togglePopup,
  fetchDetailVocabularySuccess,
  fetchDetailVocabularyFailure,
  toggleStatusListening,
}
function setHighLightWord(data){
  return{
    type: config.SET_HIGHLIGHT_WORD,
    payload: {
      data
    }
  }
}
function setCordinatePointer(data){
  return{
    type: config.SET_CORDINATE_POINTER,
    payload: {
      data
    }
  }
}
function togglePopup(data){
  return{
    type: config.TOGGLE_POPUP_TRANSLATE,
    payload: {
      data
    }
  }
}
function fetchDetailVocabularySuccess(data){
  return{
    type: config.FETCH_VOCA_DETAIL_SUCCESS,
    payload: {
      data
    }
  }
}
function fetchDetailVocabularyFailure(data){
  return{
    type: config.FETCH_VOCA_DETAIL_FAILURE,
    payload: {
      data
    }
  }
}
function toggleStatusListening(data){
  return{
    type : config.TOGGLE_STATUS_LISTENING,
    payload: {
      data
    }
  }
}
export default wordAction;
