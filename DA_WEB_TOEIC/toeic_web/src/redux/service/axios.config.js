// First we need to import axios.js
import axios from 'axios';
import {history} from "../../history";
import {authHeader} from "../../commons/_helpers/autHeader";
import {API_ENDPOINT_SERVER} from "../../commons/constants/Config";
import {getSessionLanggue, removeSession, setSessionCookie} from "../../commons/configCookie";
import {toast} from "react-toastify";
import store from "../../redux/storeConfig/store"
import loginActions from "../actions/auth/loginActions"


const axiosConfig = axios.create({
  baseURL: API_ENDPOINT_SERVER,
  timeout: 200000
});

/*set list toast*/
const toastErrorList = new Set();
const toastSuccessList = new Set();
const MAX_ERROR_TOAST = 1;
const MAX_SUCCESS_TOAST = 1;
/*set list toast*/

axiosConfig.interceptors.request.use(
  function (config) {
    const jwtToken = authHeader();
    // console.log(" jwt",jwtToken);
    // console.log('cmmm vao inter req');
    if (jwtToken && jwtToken !== null) {
      config.headers.Authorization = jwtToken;
      // config.headers.common['Authorization'] = jwtToken;
    }
    config.headers.common['Accept-Language'] = getSessionLanggue();
    return config;
  },
  function (err) {
    return Promise.reject(err);
  }
);
axiosConfig.interceptors.response.use(response => {
  const {data} = response;
  if (data != "")
    toast.success(data);
  // if (toastSuccessList.size < MAX_SUCCESS_TOAST) {
  //   debugger
  //   const id = toast.success(data, {
  //     onClose: () => toastSuccessList.delete(id)
  //   });
  //   toastSuccessList.add(id);
  // }

  console.log("axios config---");
  if (response.headers["authorization"]) {
    setSessionCookie(response.headers["authorization"])
  }

  return response;
}, error => {
  // debugger
  const {response} = error;

  if (response !== undefined) {
    // debugger
    const {data} = response;
    if (data.message && store.getState().auth.login.isError401 === false) {
      const {message} = data;

      if (toastErrorList.size < MAX_ERROR_TOAST) {
        const id = toast.error(message, {
          onClose: () => toastErrorList.delete(id)
        });
        toastErrorList.add(id);
      }
      // toast.error(message);
    } else {
      // debugger
      if (toastErrorList.size < MAX_ERROR_TOAST) {
        const id = toast.error(data, {
          onClose: () => toastErrorList.delete(id)
        });
        toastErrorList.add(id);
      }
      // toast.error(data);
    }
  } else {
    if (toastErrorList.size < MAX_ERROR_TOAST) {
      const id = toast.error("Lỗi kết nối server", {
        onClose: () => toastErrorList.delete(id)
      });
      toastErrorList.add(id);
    }
    // toast.error("Lỗi kết nối server");
  }

  console.log('cmmm vao inter error');
  const state = store.getState();
  console.log(state);
  if (error.response && error.response.status === 401) {
    removeSession();
    store.dispatch(loginActions.logoutWithJWT());  //isError401 -> true
    //console.log(store.getState());
    history.push("/pages/login");
  }

  if (error.response && error.response.status === 404) {
    history.push('/not-found');
  }

  return error;
  // return Promise.reject(error);
});

export default axiosConfig;
