import axios from 'axios'
import {LOGIN_URL, SIGNUP_URL} from "../../commons/constants/Config";
import axiosConfig from "./axios.config";

class axiosService {

  handleSuccess(response) {
    console.log('handleSuccess');
    return response
  }

  handleEror(error) {
    console.log('handleEror');
    return Promise.reject(error)
  }

  get(url) {
    return axiosConfig.get(url).then(res => {
      if (res.status === 200) {
        return res
      } else {
        console.log('loi roi');
      }
    }).catch((error) => {
      return error.response
    });
  }

  post(url, data) {
    if (url.includes(LOGIN_URL) || url.includes(SIGNUP_URL)) {
      delete axios.defaults.headers.common['Authorization']
    }
    return axiosConfig.post(url, data)
  }

  postUploadFile(url, data) {
    if (url.includes(LOGIN_URL) || url.includes(SIGNUP_URL)) {
      delete axios.defaults.headers.common['Authorization']
    }
    return axiosConfig.post(url, data , {
      headers: {
        'content-type': 'multipart/form-data;boundary=----WebKitFormBoundaryyrV7KO0BoCBuDbTL'
      }
    })
  }

  put(url, data) {
    return axiosConfig.put(url, data);
  }

  delete(url, data) {
    return axiosConfig.delete(url, data);
  }
}

export default new axiosService();
