import * as config from '../../../commons/constants/Config';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const intiState ={
  activeTab: "1",
  listHistoryPractices: [],
  total: 0,
  listTypeForHistoryPractices: [],
  listPartForHistoryPractices: [],
  listTopicForHistoryPractices: [],
  isLoadingDosearch: false,
  loadingSaveResult: false,
  hiddenBtnSaveResult: false,

  loadingSaveResultSingle: false,
  hiddenBtnSaveResultSingle: false
}

const historyPracticseReducer = (state = intiState,action)=> {
  switch (action.type) {

    //change active tab
    case config.CHANGE_ACTIVE_TAB:{
      const {data} = action.payload;
      return {...state, activeTab: data}
    }

    //get list history practices
    case config.GET_LIST_HISTORY_PRACTICES:{
      const {data} = action.payload;
      return {...state, isLoadingDosearch: true}
    }
    case config.GET_LIST_HISTORY_PRACTICES_SUCCESS:{
      const {data} = action.payload;
      let total = 0;
      if(action.payload.data.data[0]){
        total = action.payload.data.data[0].numberTest;
      }
      return {...state, listHistoryPractices: data, total: total, isLoadingDosearch: false}
    }
    case config.GET_LIST_HISTORY_PRACTICES_FAILED:{
      return {...state}
    }

    //get type for history practices
    case config.GET_TYPE_FOR_HISTORY_PRACTICES:{
      return {...state}
    }
    case config.GET_TYPE_FOR_HISTORY_PRACTICES_SUCCESS:{
      const {data} = action.payload;
      return {...state, listTypeForHistoryPractices: data}
    }
    case config.GET_TYPE_FOR_HISTORY_PRACTICES_FAILED:{
      return {...state}
    }

    //get part for history practices
    case config.GET_PART_FOR_HISTORY_PRACTICES:{
      const {data} = action.payload;
      return {...state}
    }
    case config.GET_PART_FOR_HISTORY_PRACTICES_SUCCESS:{
      const {data} = action.payload;
      return {...state, listPartForHistoryPractices: data}
    }
    case config.GET_PART_FOR_HISTORY_PRACTICES_FAILED:{
      return {...state}
    }

    //get topic for history practices
    case config.GET_TOPIC_FOR_HISTORY_PRACTICES:{
      return {...state}
    }
    case config.GET_TOPIC_FOR_HISTORY_PRACTICES_SUCCESS:{
      const {data} = action.payload;
      return {...state, listTopicForHistoryPractices: data}
    }
    case config.GET_TOPIC_FOR_HISTORY_PRACTICES_FAILED:{
      return {...state}
    }

    //save result history listening
    case config.SAVE_RESULT_HISTORY_LISTENING:{
      const {data} = action.payload;
      return {...state, loadingSaveResult: true}
    }
    case config.SAVE_RESULT_HISTORY_LISTENING_SUCCESS:{
      const {data} = action.payload;
      return {...state, loadingSaveResult: false, hiddenBtnSaveResult: true}
    }
    case config.SAVE_RESULT_HISTORY_LISTENING_FAILED:{
      toast.error("lưu thất bại");
      return {...state}
    }

    //show button save result listening
    case config.SHOW_BUTTON_SAVE_RESULT_LISTENING:{
      return {...state, hiddenBtnSaveResult: false}
    }

    //save result history lis single
    case config.SAVE_RESULT_HISTORY_LIS_SINGLE:{
      const {data} = action.payload;
      return {...state, loadingSaveResultSingle: true}
    }
    case config.SAVE_RESULT_HISTORY_LIS_SINGLE_SUCCESS:{
      const {data} = action.payload;
      return {...state, loadingSaveResultSingle: false, hiddenBtnSaveResultSingle: true}
    }
    case config.SAVE_RESULT_HISTORY_LIS_SINGLE_FAILED:{
      toast.error("lưu thất bại");
      return {...state}
    }

    //------------clean hidden button before get listening---------------
    case config.CLEAN_HIDDEN_BUTTON_SAVE_RESULT_LIS_SINGLE:{
      return {...state, hiddenBtnSaveResultSingle: false}
    }
    case config.CLEAN_HIDDEN_BUTTON_SAVE_RESULT_LISTENING:{
      return {...state, hiddenBtnSaveResult: false}
    }
    case config.UPDATE_CHECK_HIDDEN_BUTTON_SAVE_P3_P4:{
      return {...state,hiddenBtnSaveResultSingle: true }
    }

    default: {
      return state;
    }
  }
}

export default historyPracticseReducer
