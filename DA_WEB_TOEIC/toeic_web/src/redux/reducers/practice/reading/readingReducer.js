import * as config from "../../../../commons/constants/Config";

const initial = {
  listQuestion: [],
  resultQuestion: [],
  isSelect: false,
  userChoose: '',
  numberCorrectAnswer: 0,
  listQuestionAndAnswer: [],
  listQuestionAndAnswerTemp: [],
  nameTopic: '',
  totalTrue:0,
  loading: true,
  checkReview: false,
  index: 0,
  lstQuestionCheckCorrect: [],
  part: '',
  totalTrueScore: 0,
  lstCorrect: [],
  currentScore: 0,
  totalScore: 0,
  numberSelected: 0,
  levelCode: '',
  listUserChoose: [],
  isSubmit: false,
  topicId: 0,
  typeCode: '',
  loadingSave: false,
  loadingGetQuestionReadFill: false,
  hiddenButtonWhenViewHistory: false,
  loadingGetHistoryReadFill: false,
  loadingGetHistoryReadSingleDual: false,
  lstUserChooseFirst: [],
  checkHiddenButtonReading: false
}

const readingReducer = (state = initial, action) => {
  switch (action.type) {
    case config.GET_LIST_QUESTION_OF_READING_WORD_FILL:
      return {...state, loadingGetQuestionReadFill: true, hiddenButtonWhenViewHistory: false, checkHiddenButtonReading:false}
    case config.GET_LIST_QUESTION_OF_READING_WORD_FILL_SUCCESS:
      return {...state, listQuestion: action.payload.data, loadingGetQuestionReadFill: false}
    case config.GET_LIST_QUESTION_OF_READING_WORD_FILL_FAIL:
      return {...state, loadingGetQuestionReadFill: false}
    case config.GET_RESULT_QUESTION_OF_READING_WORD_FILL:
      let userChooseTemp = state.listUserChoose;

      if(userChooseTemp.length === 0){
        userChooseTemp.push({id:action.payload.data.id, choosen:action.payload.data.userChoose})
      } else {
        if(userChooseTemp[userChooseTemp.length -1].id === action.payload.data.id){
          userChooseTemp.splice(userChooseTemp.length -1,1)
        }
        userChooseTemp.push({id:action.payload.data.id, choosen:action.payload.data.userChoose})
      }
      return {...state, isSelect: true, userChoose: action.payload.data.userChoose,
        numberSelected: action.payload.data.numberSelected, isSubmit:true,
        listUserChoose:userChooseTemp }
    case config.GET_RESULT_QUESTION_OF_READING_WORD_FILL_SUCCESS:
      let totalTrueTemp = state.totalTrue;
      let number = state.numberCorrectAnswer;
      let listQuestionAndAnswerTemp = state.listQuestionAndAnswer;
      // let listQuestionAndAnswerDemo = state.listQuestionAndAnswer;
      if(state.userChoose.toLowerCase() === action.payload.data.answer.toLowerCase() && state.numberSelected <= 3){
        number += 1;
        totalTrueTemp++;
      }
      action.payload.data.nameTopic = state.nameTopic;
      action.payload.data.userChoose = state.userChoose;
      action.payload.data.correct = number;
      action.payload.data.numberQuest = 1;
      action.payload.data.numberSelect = state.numberSelected;
      if(listQuestionAndAnswerTemp.length === 0){
        listQuestionAndAnswerTemp.push(action.payload.data);
      } else {
        if(listQuestionAndAnswerTemp[listQuestionAndAnswerTemp.length-1].id === action.payload.data.id){
          state.listQuestionAndAnswerTemp.splice(listQuestionAndAnswerTemp.length - 1,1)
        }
        listQuestionAndAnswerTemp.push(action.payload.data);
      }
      return {
        ...state, resultQuestion: action.payload.data, isSelect: false, totalTrue:totalTrueTemp,
        listQuestionAndAnswer: listQuestionAndAnswerTemp, listQuestionAndAnswerTemp: listQuestionAndAnswerTemp
      }
    case config.GET_RESULT_QUESTION_OF_READING_WORD_FILL_FAIL:
      return {...state, isSelect: false}
    case config.CLEAN_LIST_QUESTION_OF_READING_WORD_FILL:
      return {...state, listQuestion: [], userChoose: '', listQuestionAndAnswer: [],listQuestionAndAnswerTemp:[],
        listUserChoose:[],numberSelected:0, levelCode:'', isSubmit: false, checkReview: false,
        topicId: 0, typeCode:'', lstCorrect: []}
    case config.CLEAN_RESULT_QUESTION_OF_READING_WORD_FILL:
      return {...state, resultQuestion: {}}
    case config.CLEAN_NUMBER_CORRECT_ANSWER:
      return {...state, numberCorrectAnswer: 0, totalTrue: 0}
    case config.GET_NAME_TOPIC_READ:
      return {...state, nameTopic: action.payload.data}
    case config.CLEAN_LIST_QUESTION_AND_ANSWER_READING:
      return {...state, listQuestionAndAnswer: []}
    case config.GET_LIST_QUESTION_OF_READING_COMPLETED_PASSAGE:
      return {...state, loadingGetQuestionReadFill: true, hiddenButtonWhenViewHistory: false, checkHiddenButtonReading:false}
    case config.GET_LIST_QUESTION_OF_READING_COMPLETED_PASSAGE_SUCCESS:
      return {...state, loadingGetQuestionReadFill: false, listQuestion: action.payload.data }
    case config.GET_LIST_QUESTION_OF_READING_COMPLETED_PASSAGE_FAIL:
      return {...state, loadingGetQuestionReadFill: false}
    case config.GET_RESULT_QUESTION_OF_READING_COMPLETED_PASSAGE:
      return {...state, isSelect: true, isSubmit: true }
    case config.GET_RESULT_QUESTION_OF_READING_COMPLETED_PASSAGE_SUCCESS:
        let numberCorrectTemp = state.numberCorrectAnswer;
       let listQuestionAndAnswerTemp1 = state.listQuestionAndAnswer;
        let lstResultTemp = action.payload.data;
      listQuestionAndAnswerTemp1.push(lstResultTemp);
      numberCorrectTemp = lstResultTemp[lstResultTemp.length - 1].numberCorrect + numberCorrectTemp;
      return {...state, resultQuestion: action.payload.data ,isSelect: false,
        listQuestionAndAnswer: listQuestionAndAnswerTemp1, listQuestionAndAnswerTemp: listQuestionAndAnswerTemp1,
        numberCorrectAnswer: numberCorrectTemp }

    case config.GET_RESULT_QUESTION_OF_READING_COMPLETED_PASSAGE_FAIL:
      return {...state, isSelect: false}
    case config.CLEAN_LIST_QUESTION_OF_READING_COMPLETED_PASSAGE:
      return {...state, listQuestion: []}
    case config.CLEAN_RESULT_QUESTION_OF_READING_COMPLETED_PASSAGE:
      return {...state, resultQuestion: {}, listUserChoose: [], isSubmit: false, checkView: false }
    case config.CLEAN_NUMBER_CORRECT_ANSWER_READING_COMPLETED_PASSAGE:
      return {...state, numberCorrectAnswer: 0}
    case config.GET_NAME_TOPIC_READ_COMPLETED_PASSAGE:
      return {...state, nameTopic: action.payload.data}
    case config.CLEAN_LIST_QUESTION_AND_ANSWER_READING_COMPLETED_PASSAGE:
      return {...state, listQuestionAndAnswer: [], lstCorrect: [], listQuestionAndAnswerTemp: [], nameTopic:'', part: ''}
    case config.GET_LIST_SINGLE_AND_DUAL_READING_QUESTION:
      debugger
      return {...state, loading: true, checkReview: false,hiddenButtonWhenViewHistory: false, checkHiddenButtonReading:false,tempAnsweredReadingSingleAndDualQuest:null, listCategorySingleAndDual:null}
    case config.GET_LIST_SINGLE_AND_DUAL_READING_QUESTION_SUCCESS:
      debugger
      let countQuest = 0;
      for(let i = 0;i<action.payload.data.length;i++){
        action.payload.data[i].numberQuest = action.payload.data[i].listQuestion.length;
        countQuest = countQuest + action.payload.data[i].listQuestion.length;
      }
      return {...state,listCategorySingleAndDual:action.payload.data ,loadingGetHistoryReadFill:false,loadingGetHistoryReadSingleDual:false, loadingSave:false,loading: false,totalQuestion:countQuest}
    case config.GET_LIST_SINGLE_AND_DUAL_READING_QUESTION_FAILD:
      return{...state,loading:false}
    case config.CLEAR_TEMP_READING_SINGLE_AND_DUAL_QUESTION:
      return{...state,tempAnsweredReadingSingleAndDualQuest:undefined,listCategorySingleAndDual:[],listQuestionAndAnswer: [],listQuestionAndAnswerTemp:[],nameTopic:'', part: ''}
    case config.SUBMIT_SINGLE_AND_DUAL_READING_QUESTION:
      return{...state,loading:true}
    case config.SUBMIT_SINGLE_AND_DUAL_READING_QUESTION_SUCCESS:
      let count = state.totalTrue;
      let lstAnswered = state.listQuestionAndAnswer ? state.listQuestionAndAnswer  : [];
      action.payload.data.correct = 0;
      for(let i = 0; i<action.payload.data.listQuestion.length;i++){
        if(action.payload.data.listQuestion[i].correct){
          action.payload.data.correct ++;
          count++;
        }
      }
      action.payload.data.numberQuest = action.payload.data.listQuestion.length
      lstAnswered.push(action.payload.data)
      return{...state,tempAnsweredReadingSingleAndDualQuest:action.payload.data,loading:false,totalTrue:count,listQuestionAndAnswer: lstAnswered,listQuestionAndAnswerTemp:lstAnswered}
    case config.SUBMIT_SINGLE_AND_DUAL_READING_QUESTION_FAILD:
      return{...state,loading:false}
    case config.CLEAR_TOTAL_TRUE_READING_SINGLE_AND_DUAL_QUESTION:
      return{...state,totalTrue:0}
    case config.SAVE_SCORE_CORRECT:
      const { totalScore,correct,totalSubCorrect, stt} = action.payload.data;
      for(let i=0; i< state.listQuestionAndAnswer.length; i++){
        if(stt === i){
          state.listQuestionAndAnswer[i].totalScore = totalScore;
          state.listQuestionAndAnswer[i].correct = correct;
          state.listQuestionAndAnswer[i].totalSubCorrect = totalSubCorrect;
          state.listQuestionAndAnswer[i].numberQuest = state.listQuestionAndAnswer[i].length
        }
      }
      return {...state }
    case config.GET_NAME_TOPIC_READ_COMPLETED_PASSAGE:
      return {...state, nameTopic: action.payload.data }
    case config.SET_PART_PRACTICE_READING:
      debugger
      return{...state,partPractice:action.payload.data,part: action.payload.data}
    case config.CLEAR_TOTAL_QUESTIONS:
      return{...state,totalQuestion:0}
    case config.UPDATE_CHECK_REVIEW:
      return {...state, checkReview: true}
    case config.GET_INDEX:
      return {...state, index: action.payload.data}
    case config.GET_PART:
      return {...state, part: action.payload.data}
    case config.SAVE_LIST_CORRECT:
      let lstCorrectTemp = state.lstCorrect;
      const {numberCorrect, numberQues} = action.payload.data;
      lstCorrectTemp.push({numberCorrect, numberQues})
      return {...state,lstCorrect: lstCorrectTemp }
    case config.SAVE_CURRENT_SUM_SCORE:
      const {current, sum}= action.payload.data;
      return {...state, currentScore: current, totalScore: sum}
    case config.UPDATE_CHECK_REVIEW_FALSE:
      return {...state, checkReview: false}
    case config.SET_TEMP_ANSWERED_READING_SINGLE_AND_DUAL_QUEST:
      return {...state,tempAnsweredReadingSingleAndDualQuest:action.payload.data}
    case config.GET_LEVEL_READ_COMPLETED_PASSAGE:
      return {...state, levelCode: action.payload.data.levelCode}
    case config.STORE_DATA_TO_REPRACTICE_READING:
      return {...state,storeDataRepracticeReading:action.payload.data}
    case config.SET_VALUE_INDEX:
      return {...state, index: 0}
    case config.UPDATE_IS_SUBMIT:{
      let isSubmitTemp = state.isSubmit;
      return {...state, isSubmit: !isSubmitTemp}
    }
    case config.CREATE_HISTORY_READING_WORD_FILL:{
      return {...state, loadingSave: true}
    }
    case config.CREATE_HISTORY_READING_WORD_FILL_SUCCESS:{
      return {...state, loadingSave: false, checkHiddenButtonReading:true}
    }
    case config.CREATE_HISTORY_READING_WORD_FILL_FAILED:{
      return {...state, loadingSave: false}
    }
    case config.GET_TOPIC_ID:{
      return {...state, topicId: action.payload.data}
    }
    case config.GET_TYPE_CODE_READING:{
      return {...state, typeCode: action.payload.data}
    }
    case config.SAVE_USER_CHOOSE:{
      let userChooseTemp = state.listUserChoose;
      if(userChooseTemp.length == 0){
        userChooseTemp.push({id:action.payload.id, userChoose:action.payload.userChoose})
      } else {
        if(userChooseTemp[userChooseTemp.length -1].id === action.payload.id){
          userChooseTemp.splice(userChooseTemp.length -1,1)
        }
        userChooseTemp.push({id:action.payload.id, userChoose:action.payload.userChoose})
      }
      return {...state,  listUserChoose:userChooseTemp }
    }
    case config.CREATE_HISTORY_READING_COMPLETED_PASSAGE:{
      return {...state, loadingSave:true}
    }
    case config.CREATE_HISTORY_READING_COMPLETED_PASSAGE_SUCCESS:{
      return {...state, loadingSave:false, checkHiddenButtonReading: true}
    }
    case config.CREATE_HISTORY_READING_COMPLETED_PASSAGE_FAILED:{
      return {...state, loadingSave:false}
    }
    case config.CLEAN_NUMBER_SELECTED:{
      return {...state, numberSelected: 0}
    }
    case config.CREATE_HISTORY_READING_SINGLE_DUAL:{
      return {...state, loadingSave:true}
    }
    case config.CREATE_HISTORY_READING_SINGLE_DUAL_SUCCESS:{
      return {...state, loadingSave:false}
    }
    case config.CREATE_HISTORY_READING_SINGLE_DUAL_FAILED:{
      return {...state, loadingSave:false}
    }

    case config.GET_HISTORY_READING_INCOMPLETE_SENTENCES:{
      return {...state, loadingGetHistoryReadFill: true}
    }
    case config.GET_HISTORY_READING_INCOMPLETE_SENTENCES_SUCCESS:{
      let totalTrueTemp = state.totalTrue;
      let listQuestionAndAnswerTemp = state.listQuestionAndAnswer;
      listQuestionAndAnswerTemp = action.payload.data.data;
      let numberCorrectTemp = state.numberCorrectAnswer;
      for(let i=0;i< listQuestionAndAnswerTemp.length;i++){
        if(listQuestionAndAnswerTemp[i].listAnswerToChoose){
          numberCorrectTemp = listQuestionAndAnswerTemp[i].
            listAnswerToChoose[listQuestionAndAnswerTemp[i].listAnswerToChoose.length - 1].numberCorrect + numberCorrectTemp;
        }
        if(listQuestionAndAnswerTemp[i].indexInCorrect === null && listQuestionAndAnswerTemp[i].numberSelected <= 3){
          totalTrueTemp++
        }
      }
      return {
        ...state, isSelect: false,totalTrue:totalTrueTemp,
        listQuestionAndAnswer: listQuestionAndAnswerTemp, listQuestionAndAnswerTemp: listQuestionAndAnswerTemp,
        listQuestion: action.payload.data.data, hiddenButtonWhenViewHistory: true, loadingGetHistoryReadFill: false
      }
    }
    case config.GET_HISTORY_READING_INCOMPLETE_SENTENCES_FAILED:{
      return {...state, loadingGetHistoryReadFill: false}
    }
    case config.UPDATE_VIEW_BTN_READING:{
      return {...state, hiddenButtonWhenViewHistory: true}
    }

    case config.GET_DETAIL_HISTORY_READING_COMPLETED_PASSAGE:{
      return {...state, loadingGetQuestionReadFill: true, loadingGetHistoryReadFill: true}
    }
    case config.GET_DETAIL_HISTORY_READING_COMPLETED_PASSAGE_SUCCESS:{
      let numberCorrectTemp = state.numberCorrectAnswer;
      let listQuestionAndAnswerTemp1 = state.listQuestionAndAnswer;
      listQuestionAndAnswerTemp1 = action.payload.data.data;
      let lstQuestionAnswer = [];
      for(let i=0; i< listQuestionAndAnswerTemp1.length ; i++){
        if(listQuestionAndAnswerTemp1[i] && listQuestionAndAnswerTemp1[i].listAnswerToChoose){
          lstQuestionAnswer.push(listQuestionAndAnswerTemp1[i].listAnswerToChoose);
          for(let j=0;j<listQuestionAndAnswerTemp1[i].listAnswerToChoose.length;j++){
            if(listQuestionAndAnswerTemp1[i].listAnswerToChoose[j].indexInCorrect === null){
              numberCorrectTemp++;
            }
          }
        }
      }
      return {...state, loadingGetQuestionReadFill: false, hiddenButtonWhenViewHistory: true,
        listQuestionAndAnswer: lstQuestionAnswer, listQuestionAndAnswerTemp: lstQuestionAnswer,
        numberCorrectAnswer: numberCorrectTemp, listQuestion:listQuestionAndAnswerTemp1, loadingGetHistoryReadFill:false }
    }
    case config.GET_DETAIL_HISTORY_READING_COMPLETED_PASSAGE_FAILED:{
      return {...state, loadingGetQuestionReadFill: false,loadingGetHistoryReadFill: false}
    }
    case config.GET_DETAIL_HISTORY_READING_SINGLEDUAL:{
      return {...state, loadingGetHistoryReadSingleDual:true}
    }
    case config.GET_DETAIL_HISTORY_READING_SINGLEDUAL_SUCCESS:{
      debugger
      const {data} = action.payload;
      let count = state.totalTrue;
      let countQuest = 0;
      let lstAnswered = state.listQuestionAndAnswer
      lstAnswered = data;
      action.payload.data.correct = 0;
      for(let i = 0; i<action.payload.data.length;i++){
            lstAnswered[i].correct = lstAnswered[i].correctQuesNumber; 
            count += lstAnswered[i].correctQuesNumber
            lstAnswered[i].numberQuest = action.payload.data[i].listQuestion.length
            countQuest +=action.payload.data[i].listQuestion.length
      }
      return{...state,totalQuestion:countQuest,loading:false,totalTrue:count,listQuestionAndAnswer: lstAnswered,
        listQuestionAndAnswerTemp:lstAnswered, listCategorySingleAndDual : lstAnswered,
        hiddenButtonWhenViewHistory:true ,loadingSave :false, loadingGetHistoryReadSingleDual: false,loadingGetQuestionReadFill: false
        
      }
    }
    case config.GET_DETAIL_HISTORY_READING_SINGLEDUAL_FAILED:{
      return {...state,  loadingGetHistoryReadSingleDual: false,loadingGetQuestionReadFill: false,loading:false,loadingSave :false}
    }
    case config.UPDATE_CHECK_HIDDEN_BUTTON_SAVE_READING:{
      return {...state, checkHiddenButtonReading: true}
    }
    case config.CLEAR_TEMP_READING_SINGLE_WHEN_NEXT_QUESTION:
      return{...state,tempAnsweredReadingSingleAndDualQuest:undefined,listQuestionAndAnswerTemp:[]}
    default:
      return state;
  }
}

export default readingReducer;
