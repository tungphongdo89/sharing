import * as config from "../../../../commons/constants/Config";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import {FormattedMessage} from "react-intl";
import {showMessage} from "../../../../commons/utils/convertDataForToast";


const initial = {
  loading: false,
  listQuestionAnswer:[],
  reviewPractice:false,
  totalCorrect:0,
  countCheck: 0,
  hidePathFile: false,
  paramValue: '',
  isSubmitWordFill: false,
  partId: 0,
  userFill: [],
  nameTopic: '',
  lstNumberCorrect: [],
  listCategories: [],
  checkedReview: false,
  listSize: [],
  indexListen: 0,
  lstIndex:[],
  part:'',
  topicId: 0,
  levelCode: '',
  lstAnswerCut: [],
  loadingSave: false,
  submitFill: false,
  index: 0,
  indexQuestion: 0,
  hiddenBtnWhenReviewResult: false,
  data: false,
  isLoading: false,
  isLoadFill: false,
  checkHiddenButtonListen: false,
  viewHistory: false,
  loadingGetHistoryListenFill: false
}

const multiListeningReducer = (state = initial, action) => {
  switch (action.type) {
    case config.GET_LIST_LISTENING_QUESTIONS_OF_MULTI:
      return {...state,loading:true,reviewPractice:false, indexQuestion: 0, hiddenBtnWhenReviewResult:false, hidePathFile: false}
    case config.GET_LIST_LISTENING_QUESTIONS_OF_MULTI_SUCCESS:
      return {...state,listQuestionPart1:action.payload.data,loading:false}
    case config.GET_LIST_LISTENING_QUESTIONS_OF_MULTI_FAILD:
      return {...state,loading:false}

      //set count check = 0
    case config.RESET_COUNT_CHECK_LISTENING_PRACTICES:
      debugger
      return {...state, countCheck: 0, hidePathFile: false}

    case config.GET_TOPICID_FOR_PRACTICE:
      return {...state,topicId:action.payload.data,levelCode:action.payload.levelCode}

    case config.CHECK_ANSWER_MULTI_LISTENING_QUESTION:
      debugger
      return {...state,loading:true}
    case config.CHECK_ANSWER_MULTI_LISTENING_QUESTION_SUCCESS:
      debugger
      action.payload.data.nameTopic = state.nameTopic;
      // state.listQuestionAnswer.push(action.payload.data)

      if(action.payload.data.part === "PART1"){
        if(action.payload.data.correct===true){
          if(state.countCheck < 3){
            state.listQuestionAnswer.push(action.payload.data)
          }
          let newTotal = state.totalCorrect + 1;
          return {...state,questionAnswer:action.payload.data,loading:false,totalCorrect: newTotal, countCheck: 3, hidePathFile: false}
        }
        if(state.countCheck < 2){
          // toast.error("Phương án bạn vừa chọn không đúng. Vui lòng chọn lại")
          toast.error(showMessage("your.option.is.incorrect.please.choose.again"))
          return {...state,questionAnswer:action.payload.data,loading:false, countCheck: state.countCheck + 1, hidePathFile: false}
        }
        else {
          state.listQuestionAnswer.push(action.payload.data)
          return {...state,questionAnswer:action.payload.data, loading:false, countCheck: 3, hidePathFile: false}
        }
      }

      if(action.payload.data.part === "PART2"){
        if(action.payload.data.correct===true){
          if(state.countCheck < 2){
            state.listQuestionAnswer.push(action.payload.data)
          }
          let newTotal = state.totalCorrect + 1;
          return {...state,questionAnswer:action.payload.data,loading:false,totalCorrect: newTotal, countCheck: 2, hidePathFile: false}
        }
        if(state.countCheck < 1){
          // toast.error("Phương án bạn vừa chọn không đúng. Vui lòng chọn lại")
          toast.error(showMessage("your.option.is.incorrect.please.choose.again"))
          return {...state,questionAnswer:action.payload.data,loading:false, countCheck: state.countCheck + 1, hidePathFile: false}
        }
        else {
          state.listQuestionAnswer.push(action.payload.data)
          return {...state,questionAnswer:action.payload.data, loading:false, countCheck: 2, hidePathFile: false}
        }
      }

    //set total correct khi xem lại đáp án
    case config.SET_TOTAL_CORRECT_WHEN_REVIEW:{
      const {data} = action.payload;
      return {...state,totalCorrect:data}
    }

    //Show file nghe gốc khi xem lại đáp án part1, part2
    case config.SHOW_AUDIO_WHEN_REVIEW_PART12:{
      debugger
      return {...state,hidePathFile: false}
    }

    case config.CHECK_ANSWER_MULTI_LISTENING_QUESTION_FAILD:
      return {...state,loading:false}
    case config.CLEAR_TEMP_MULTI_LISTENING_QUESTION:
      return {...state,questionAnswer:undefined}
    case config.CLEAR_TOTAL_CORRECT_MULTI_LISTEN_QUESTION:
      return {...state,totalCorrect: 0}
    case config.CLEAR_LIST_ANSWERED_MULTI_LISTEN_QUESTION:
      return {...state,listQuestionAnswer:[]}
      return {...state,Tid:action.payload.data}
    case config.GET_LIST_QUESTION_OF_LISTENING_WORD_FILL:
      return {...state, isLoadFill: true, checkHiddenButtonListen: false, hiddenBtnWhenReviewResult:false, viewHistory: false}
    case config.GET_LIST_QUESTION_OF_LISTENING_WORD_FILL_SUCCESS:
      return {...state, listQuestionWordFill: action.payload, isLoadFill: false}
    case config.GET_LIST_QUESTION_OF_LISTENING_WORD_FILL_FAIL:
      return {...state, isLoadFill: false}
    case config.GET_RESULT_QUESTION_OF_LISTENING_WORD_FILL:
      let userFillTemp = state.userFill;
      userFillTemp.push({id:action.payload.data.id,fill:action.payload.data.userFill})
      return {...state, isSubmitWordFill: true, userFill: userFillTemp,submitFill:true }
    case config.GET_RESULT_QUESTION_OF_LISTENING_WORD_FILL_SUCCESS:
      let lstIndexTemp = state.lstIndex;
      let listQuestionAndAnswerTemp = state.listQuestionAnswer;
      let lstTemp = state.lstNumberCorrect;
      let listSizeTemp = state.listSize;
      listSizeTemp.push(action.payload.data.lstSize)
      lstTemp.push(action.payload.data.numberCorrect)
      listQuestionAndAnswerTemp.push(action.payload.data);
      if(action.payload.data.lstIndexCorrect.length === 0){
        lstIndexTemp.push({correct:null,incorrect:action.payload.data.lstIndexInCorrect})
      } else if(action.payload.data.lstIndexInCorrect.length === 0){
        lstIndexTemp.push({correct:action.payload.data.lstIndexCorrect,incorrect:null})
      } else {
        lstIndexTemp.push({correct:action.payload.data.lstIndexCorrect,incorrect:action.payload.data.lstIndexInCorrect})
      }
      return {...state, resultQuestionWordFill: action.payload, isSubmitWordFill: false,
         numberSameAnswer: 0, listQuestionAnswer: listQuestionAndAnswerTemp, lstNumberCorrect: lstTemp,listSize: listSizeTemp,
        lstIndex:lstIndexTemp, lstAnswerCut: action.payload.data.lstAnswerCut }
    case config.GET_RESULT_QUESTION_OF_LISTENING_WORD_FILL_FAIL:
      return {...state, isSubmitWordFill: false}
    case config.CLEAN_LIST_QUESTION_OF_LISTENING_WORD_FILL:
      return {...state, listQuestionWordFill: []  }
    case config.CLEAN_RESULT_QUESTION_OF_LISTENING_WORD_FILL:
      return {...state, resultQuestionWordFill: {}, lstIndex:[], submitFill:false}
    case config.CLEAN_NUMBER_CORRECT_ANSWER_WORD_FILL:
      return {...state, lstNumberCorrect: [], listSize:[] }

    case config.GET_VALUE_TYPE_CODE:
      return {...state, paramValue: action.payload.data}
    case config.GET_PART_ID:
      return {...state, partId: action.payload.data}
    case config.GET_NAME_TOPIC:
      return {...state, nameTopic: action.payload.data}
    case config.CLEAN_LIST_QUESTION_AND_ANSWER_LISTENING:
      return {...state, listQuestionAnswer: [], userFill: [], indexListen: 0}

    //listening toeic conversation
    case config.FETCH_LISTENING_TOEIC_CONVERSATION:{
      return {...state,isLoading: true}
    }
    case config.FETCH_LISTENING_TOEIC_CONVERSATION_SUCCESS:{
      const {data} = action.payload;
      return {...state,listCategories: data.data, index: 0, isLoading: false}
    }
    case config.FETCH_LISTENING_TOEIC_CONVERSATION_FAILED:{
      return {...state,isLoading: false}
    }

    //clean before get detail history listening
    case config.CLEAN_BEFORE_GET_DETAIL_HL:{
      return {...state, index: 0}
    }

    //get detail history listening
    case config.GET_DETAIL_HISTORY_LISTENING:{
      const {data} = action.payload;
      return {...state, isLoading: true}
    }
    case config.GET_DETAIL_HISTORY_LISTENING_SUCCESS:{
      const {data} = action.payload;
      return {...state, listCategories: data, index: 3, isLoading: false}
    }
    case config.GET_DETAIL_HISTORY_LISTENING_FAILED:{
      return {...state}
    }

    //get detail history lis single
    case config.GET_DETAIL_HISTORY_LIS_SINGLE:{
      debugger
      const {data} = action.payload;
      return {...state, part: data.part}
    }
    case config.GET_DETAIL_HISTORY_LIS_SINGLE_SUCCESS:{
      debugger
      const {data} = action.payload;
      return {...state, listQuestionAnswer: data, indexQuestion: 5,
        reviewPractice: true, paramValue: "2", hiddenBtnWhenReviewResult:true}
    }
    case config.GET_DETAIL_HISTORY_LIS_SINGLE_FAILED:{
      return {...state}
    }

      //Ẩn file nghe gốc khi nghe lại
    case config.HIDE_PATH_FILE_WHEN_LISTEN_AGAIN:{
      const {data} = action.payload;
      return {...state, hidePathFile: data}
    }


    case config.UPDATE_CHECK_LISTENING:{
      return {...state, checkedReview: true }
    }
    case config.UPDATE_CHECK_LISTENING_FALSE:
      return {...state, checkedReview: false}
    case config.GET_INDEX_LISTEN:
      return {...state, indexListen: action.payload.data}
    case config.SET_PART_PRACTICE_LISTENING:{
      return {...state,part:action.payload.data}
    }

      //set part for listening part1, 2
    case config.SET_PART_LISTENING_PART1:{
      // return {...state, part: "PART1" }
      return {...state }
    }
    case config.SET_PART_LISTENING_PART2:{
      // return {...state, part: "PART2" }
      return {...state }
    }

    case config.SET_QUESTION_ANSWERED_PRACTICE_LISTENING:{
      debugger
      return {...state,questionAnswer:action.payload.data.question,reviewPractice:true, countCheck: 3,
        indexQuestion:action.payload.data.index}
    }
    case config.CREATE_HISTORY_LISTEN_FILL:{
      return {...state, loadingSave: true}
    }
    case config.CREATE_HISTORY_LISTEN_FILL_SUCCESS:{
      return {...state, loadingSave:false, checkHiddenButtonListen: true}
    }
    case config.CREATE_HISTORY_LISTEN_FILL_FAILED:{
      return {...state, loadingSave: false}
    }
    case config.GET_HISTORY_LISTEN_FILL:{
      return {...state, isSubmitWordFill: true, loadingGetHistoryListenFill: true}
    }
    case config.GET_HISTORY_LISTEN_FILL_SUCCESS:{
      let lstIndexTemp = state.lstIndex;
      let listQuestionAndAnswerTemp = state.listQuestionAnswer;
      let listSizeTemp = state.listSize;
      let userFillTempp = state.userFill;
      let lstTemp = state.lstNumberCorrect;
      listQuestionAndAnswerTemp = action.payload.data.data;
      for(let i=0; i< listQuestionAndAnswerTemp.length; i++){
        if( listQuestionAndAnswerTemp[i].lstIndexCorrect !== undefined && listQuestionAndAnswerTemp[i].lstIndexInCorrect !== undefined &&
          (listQuestionAndAnswerTemp[i].lstIndexCorrect.length + listQuestionAndAnswerTemp[i].lstIndexInCorrect.length) < listQuestionAndAnswerTemp[i].lstSize){
          listSizeTemp.push((listQuestionAndAnswerTemp[i].lstIndexCorrect.length + listQuestionAndAnswerTemp[i].lstIndexInCorrect.length))
        }
        else {
          listSizeTemp.push(listQuestionAndAnswerTemp[i].lstSize)
        }
        if(listQuestionAndAnswerTemp[i].lstIndexCorrect){
          lstTemp.push(listQuestionAndAnswerTemp[i].lstIndexCorrect.length)
        }
        userFillTempp[i] = ({id:listQuestionAndAnswerTemp[i].id,fill:listQuestionAndAnswerTemp[i].lstUserFill})
        if(listQuestionAndAnswerTemp[i].lstIndexCorrect.length === 0){
          lstIndexTemp.push({correct:null,incorrect:listQuestionAndAnswerTemp[i].lstIndexInCorrect})
        } else if(listQuestionAndAnswerTemp[i].lstIndexInCorrect.length === 0){
          lstIndexTemp.push({correct:listQuestionAndAnswerTemp[i].lstIndexCorrect,incorrect:null})
        } else {
          lstIndexTemp.push({correct:listQuestionAndAnswerTemp[i].lstIndexCorrect,incorrect:listQuestionAndAnswerTemp[i].lstIndexInCorrect})
        }
      }


      return {...state, listQuestionWordFill: action.payload.data.data, isSubmitWordFill: false,lstNumberCorrect: lstTemp,
        numberSameAnswer: 0, listQuestionAnswer: listQuestionAndAnswerTemp,listSize: listSizeTemp,
        lstIndex:lstIndexTemp, lstAnswerCut: action.payload.data.lstAnswerCut, hiddenBtnWhenReviewResult:true,
        viewHistory:true, userFill: userFillTempp,submitFill:true, loadingGetHistoryListenFill: false }
    }
    case config.GET_HISTORY_LISTEN_FILL_FAILED:{
      return {...state,isSubmitWordFill: false, loadingGetHistoryListenFill: false}
    }
    case config.UPDATE_SUBMIT_FILL:{
      return {...state, submitFill: false}
    }
    case config.UPDATE_VIEW_BTN_LISTENING:{
      return {...state, hiddenBtnWhenReviewResult: true}
    }
    case config.CLEAN_TYPE_NAME_LEVEL:{
      return {...state, paramValue:'', nameTopic:'',levelCode:''}
    }
    case config.UPDATE_CHECK_HIDDEN_BUTTON_SAVE_LISTENING:{
      return {...state, checkHiddenButtonListen: true}
    }
    default:
      return state;
  }
}

export default multiListeningReducer;
