import * as config from '../../../commons/constants/Config';

const intiState = {
  listTopic: [],
  dataResp: {},
  isLoading: false,
  isLogging: false,
  totalTopic: 0,
  listTopicByPart: [],
  listPractices: [],
  lstTypeTopic: [],
  lstPartByTopic: [],
  listPartTopicByType: [],
  isLoadingGetListTopic: null,
  lstTopicByTypeTopicAndPart: [],
  listTypeTests: [],
  isCheck: false,
  isClose: false
}

const topicReducer = (state = intiState, action) => {
  switch (action.type) {
    case config.FETCH_TOPIC: {
      return {...state, isLoading: true}
    }
    case config.FETCH_TOPIC_SUCCESS: {
      const {data} = action.payload;
      return {...state, dataResp: data, listTopic: data.data, totalTopic: data.total, isLoading: false}
    }
    case config.FETCH_TOPIC_FAILD: {
      return {...state, isLoading: false}
    }
    case config.DELETE_TOPIC: {
      return {...state, isLoading: true}
    }
    case config.DELETE_TOPIC_SUCCESS: {
      return {...state, isLoading: false}
    }
    case config.DELETE_TOPIC_FAILD: {
      return {...state, isLoading: false}
    }
    case config.ADD_TOPIC: {
      return {...state, isLoading: true, isClose: false}
    }
    case config.ADD_TOPIC_SUCCESS: {
      return {...state, isLoading: false, isClose: true}
    }
    case config.ADD_TOPIC_FAILD: {
      return {...state, isLoading: false, isClose: false}
    }
    case config.UPDATE_TOPIC: {
      return {...state, isLoading: true, isClose: false}
    }
    case config.UPDATE_TOPIC_SUCCESS: {
      return {...state, isLoading: false, isCheck: false, isClose: true}
    }
    case config.UPDATE_TOPIC_FAILD: {

      return {...state, isLoading: false, isCheck: true, isClose: false}
    }

    case config.FETCH_TOPIC_BY_PART_LEVEL_SUCCESS :
      return {...state, isLoadingGetListTopic: true, listTopicByPart: action.payload.data}

    case config.FETCH_TOPIC_BY_PART_LEVEL_FAIL :
      return {...state, isLoadingGetListTopic: false}

    case config.FETCH_TOPIC_BY_PART_SUCCESS :
      return {...state, listTopicByPart: action.payload.data}

    case config.FETCH_TOPIC_BY_PART_FAILUDE :
      return state

    //-------------------------
    case config.FETCH_PART_TOPIC_BY_TYPE: {
      return {...state, isLoading: false}
    }
    case config.FETCH_PART_TOPIC_BY_TYPE_SUCCESS :
      return {...state, listPartTopicByType: action.payload.data}

    case config.FETCH_LIST_PRACTICES_SUCCESS :
      return {...state, listPractices: action.payload.data}

    case config.FETCH_LIST_PRACTICES_FAILUDE :
      return state
    case config.FETCH_TYPE_TOPIC: {
      return {...state}
    }
    case config.FETCH_TYPE_TOPIC_SUCCESS: {
      const {data} = action.payload;
      return {...state, lstTypeTopic: data}
    }
    case config.FETCH_PART_BY_TOPIC: {
      return {...state}
    }
    case config.FETCH_PART_BY_TOPIC_SUCCESS: {
      const {data} = action.payload
      return {...state, lstPartByTopic: data}
    }
    case config.FETCH_TOPIC_NAME_BY_TYPE_TOPIC_AND_PART: {
      return {...state}
    }
    case config.FETCH_TOPIC_NAME_BY_TYPE_TOPIC_AND_PART_SUCCESS: {
      const {data} = action.payload;
      return {...state, lstTopicByTypeTopicAndPart: data}
    }
    //---
    case config.GET_MENU_LIST_TEST: {
      return {...state, isLoading: true}
    }
    case config.GET_MENU_LIST_TEST_SUCCESS: {
      const {data} = action.payload;
      return {...state, dataResp: data, listTypeTests: data}
    }
    case config.GET_MENU_LIST_TEST_FAILED: {
      return {...state, isLoading: false}
    }

    default: {
      return state;
    }
  }
}

export default topicReducer
