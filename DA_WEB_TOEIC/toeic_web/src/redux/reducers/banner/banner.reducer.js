import * as config from "../../../commons/constants/Config";

const initialState = {
  lstBanner: [],
  lstBannerActive: [],
  lstBannerNotActive: [],
  loading: false,
  total: 0,
  detailBanner: {},
  isLoading: false,
}


const bannerReducer = (state = initialState, action) => {
  switch (action.type) {
    case config.GET_BANNER_SUCCESS:
      const {data} = action.payload
      return {...state, lstBanner: data}
    case config.GET_BANNER_FAILURE:
      state.lstBanner = []
      return {...state}
    case config.SHOW_BANNER_ACTIVE:{
      return {...state, loading: true}
    }
    case config.SHOW_BANNER_ACTIVE_SUCCESS:
      const {data: dataResult} = action.payload.data
      return {...state, lstBannerActive: dataResult, loading:false}
    case config.SHOW_BANNER_ACTIVE_FAILED:
      return {...state, loading:false}
    case config.GET_LIST_BANNER_NOT_ACTICVE:{
      return {...state, loading: true}
    }
    case config.GET_LIST_BANNER_NOT_ACTICVE_SUCCESS:{
      return {...state,lstBannerNotActive: action.payload.data , loading: false, total: action.payload.data.total}
    }
    case config.GET_LIST_BANNER_NOT_ACTICVE_FAILED:{
      return {...state, loading: false}
    }
    case config.GET_LIST_BANNER_ACTICVE:{
      return {...state, loading: true}
    }
    case config.GET_LIST_BANNER_ACTICVE_SUCCESS:{
      return {...state,lstBannerActive: action.payload.data , loading: false}
    }
    case config.GET_LIST_BANNER_ACTICVE_FAILED:{
      return {...state, loading: false}
    }
    case config.DELETE_BANNER:{
      return{...state, loading:true }
    }
    case config.DELETE_BANNER_SUCCESS:{
      return{...state, loading: false }
    }
    case config.DELETE_BANNER_FAILED:{
      return{...state, loading:false }
    }
    case config.CREATE_BANNER:{
      return {...state, loading: true}
    }
    case config.CREATE_BANNER_SUCCESS:{
      return {
        ...state, loading: false, isLoading: false,
      }
    }
    case config.CREATE_BANNER_FAILURE:{
      return {
        ...state, loading: false, isLoading: true,
      }
    }
    case config.UPDATE_BANNER:{
      return {...state, loading: true}
    }
    case config.UPDATE_BANNER_SUCCESS:{
      return {...state, loading: false, isLoading: false}
    }
    case config.UPDATE_BANNER_FAILED:{
      return {...state, loading: false, isLoading: true}
    }
    case config.UPDATE_ORDER_BANNER:{
      return {...state, loading: true, lstBannerActive: action.payload.data}
    }
    case config.UPDATE_ORDER_BANNER_SUCCESS:{
      return {...state, loading: false}
    }
    case config.UPDATE_ORDER_BANNER_FAILED:{
      return {...state, loading: false}
    }
    case config.UPDATE_STATUS_POPUP:{
      return {...state, isLoading: !state.isLoading}
    }
    default:
      return {...state}
  }
}

export default bannerReducer
