import * as config from "../../../commons/constants/Config";

const initialCategory = {
  lstCategory: [],
  dataRes: {},
  isLoading: false,
  loading: false,
  detailCategory: {},//kiểm tra xem có categoryId ko (có=>sửa|không=>thêm)
  isViewDetail: false,
  part: 1,
  loadingCategory: false,
  total: 0,
  dataFilter: {},
  lstTypeExercise: [],
  lstPartExercise: [],
  lstTypeLevel: [],
  lstTypeFile: [],
  lstTopicTrans: [],
  lstTopicByPart: [],
  loadingPart: false,
  loadingTopic: false
}

const CategoryReducer = (state = initialCategory, action) => {
  switch (action.type) {
    case config.FETCH_CATEGORY: {
      debugger
      return {...state, isLoading: true}
    }
    case config.FETCH_CATEGORY_SUCCESS: {
      debugger
      const {data} = action.payload;
      return {...state, dataRes: data, lstCategory: data.data, isLoading: false, total: data.total}
    }
    case config.FETCH_CATEGORY_FAILD: {
      debugger
      return {...state, isLoading: false}
    }
    case config.FETCH_DATA_FILTER: {
    }
    case config.FETCH_DATA_FILTER_SUCCESS: {
      const {data} = action.payload;
      return {...state, dataFilter: data}
    }
    case config.FETCH_DETAIL_CATEGORY: {
      debugger;
      const {data, check} = action.payload;
      return {...state, detailCategory: data, isViewDetail: check, loadingViewDetail: true}
    }
    case config.FETCH_DETAIL_CATEGORY_SUCCESS:{
      const {data, check} = action.payload;
      let total=0;
      if(data.listQuestion){
        data.listQuestion.forEach(function (item) {
          let score = parseFloat(item.score);
          if(score){
            total= total + score;
          }else{
            item.score = 0;
          }
        })
      }
      data.totalScore = total;
      return {...state,detailCategory: data, isViewDetail: check, loadingViewDetail: false,detailCategoryTemp:data}
    }
    case config.FETCH_DETAIL_CATEGORY_FAILED: {
      const {data, check} = action.payload;
      return {...state, detailCategory: data, isViewDetail: check, loadingViewDetail: false}
    }
    case config.CREATE_CATEGORY: {
      return {...state, isViewDetail: false, detailCategory: {}, loading: true}
    }
    case config.CREATE_CATEGORY_SUCCESS: {
      const {data} = action.payload;
      return {
        ...state,
        lstCategory: state.lstCategory.concat([data]),
        isViewDetail: false,
        detailCategory: {},
        loading: false
      }
    }
    case config.CREATE_CATEGORY_FAILED: {
      const {data} = action.payload;
      return {...state, isViewDetail: false, detailCategory: {}, loading: false}
    }
    case config.UPDATE_CATEGORY: {
      return {...state, loading: true, isViewDetail: false}
    }
    case config.UPDATE_CATEGORY_SUCCESS: {
      const {data} = action.payload;
      return {...state, loading: false, isViewDetail: false}
    }
    case config.UPDATE_CATEGORY_FAILED: {
      return {...state, loading: false, isViewDetail: false}
    }
    case  config.DELETE_CATEGORY: {
      debugger
      return {...state, loadingCategory: true}
    }
    case  config.DELETE_CATEGORY_SUCCESS: {
      debugger
      return {...state, loadingCategory: false}
    }
    case  config.DELETE_CATEGORY_FAILD: {
      return {...state, loadingCategory: false}
    }
    case config.GET_LIST_TYPE_EXERCISE: {
      return {...state}
    }
    case config.GET_LIST_TYPE_EXERCISE_SUCCESS: {
      const {data} = action.payload
      return {...state, lstTypeExercise: data}
    }
    case config.GET_LIST_TYPE_EXERCISE_FAILED: {
      return {...state}
    }
    case config.GET_LIST_PART_EXERCISE: {
      return {...state, loadingPart: true}
    }
    case config.GET_LIST_PART_EXERCISE_SUCCESS: {
      const {data} = action.payload
      if (data !== null && data.length!==0) {
        if (data[0].paramValue === null) {
          state.lstTopicTrans = data;
          state.loadingTopic = false
        } else {
          state.lstPartExercise = data
          state.loadingPart = false
        }
      }
      else{
        state.lstTopicTrans=[];
        state.lstPartExercise=[];
      }
      return {...state}
    }
    case config.GET_LIST_PART_EXERCISE_FAILED: {
      return {...state, loadingPart: false}
    }
    case config.GET_LIST_TYPE_LEVEL: {
      return {...state}
    }
    case config.GET_LIST_TYPE_LEVEL_SUCCESS: {
      const {data} = action.payload
      return {...state, lstTypeLevel: data}
    }
    case config.GET_LIST_TYPE_LEVEL_FAILED: {
      return {...state}
    }
    case config.GET_LIST_TYPE_FILE_UPLOAD: {
      return {...state}
    }
    case config.GET_LIST_TYPE_FILE_UPLOAD_SUCCESS: {
      const {data} = action.payload
      return {...state, lstTypeFile: data}
    }
    case config.GET_LIST_TYPE_FILE_UPLOAD_FAILED: {
      return {...state}
    }
    case config.GET_LIST_TOPIC_BY_PART: {
      return {...state, loadingTopic: true}
    }
    case config.GET_LIST_TOPIC_BY_PART_SUCCESS: {
      const {data} = action.payload
      return {...state, lstTopicByPart: data, loadingTopic: false}
    }
    case config.GET_LIST_TOPIC_BY_PART_FAILED: {
      return {...state, loadingTopic: false}
    }
    case config.EDIT_DETAIL_CATEGORY: {
      return {...state, detailCategory: {}, isViewDetail: false}
    }
    default:
      return state;
  }
}

export default CategoryReducer;
