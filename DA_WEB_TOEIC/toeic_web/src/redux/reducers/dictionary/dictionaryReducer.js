import * as config from "../../../commons/constants/Config";
const initialState = {
    lstDictionary: [],
    dataResult:{},
    total: 0,
    loading: false,
    detailDictionary: {},
    isLoading: false,
    playersList:[]

  }
const dictionaryReducer = (state = initialState, action) => {
  switch (action.type) {
    case config.FETCH_DICTIONARY: {
      return {...state, loading: true}}
    case config.FETCH_DICTIONARY_SUCCESS: {
      const {data} = action.payload;
      const playersList = data.data.map(item => {
        return {
          audio : new Audio(item.mp3),
          item,
          playing: false,
        }
      })
      return {...state, playersList:playersList,dataResult: data, lstDictionary: data.data, total: data.total, loading: false}}
    case config.FETCH_DICTIONARY_FAILED:
      state.lstBanner = []
      return {...state}
    case config.DELETE_DICTIONARY:{
      return{...state, loading:true }
    }
    case config.DELETE_DICTIONARY_SUCCESS:{
      return{...state, loading: false }
    }
    case config.DELETE_DICTIONARY_FAILED:{
      return{...state, loading:false }
    }
    case config.CREATE_DICTIONARY:{
      return {...state, loading: true, isLoading: true}
    }
    case config.CREATE_DICTIONARY_SUCCESS:{
      return {
        ...state, loading: false, isLoading: false,
      }
    }
    case config.CREATE_DICTIONARY_FAILED:{
      return {
        ...state, loading: false, isLoading: true,
      }
    }
    case config.UPDATE_DICTIONARY:{
      return {...state, loading: true}
    }
    case config.UPDATE_DICTIONARY_SUCCESS:{
      return {...state, loading: false, isLoading: false}
    }
    case config.UPDATE_DICTIONARY_FAILED:{
      return {...state, loading: false, isLoading: true}
    }
    case config.UPDATE_DICTIONARY_STATUS_POPUP:{
      return {...state, isLoading: !state.isLoading}
    }
    case config.UPDATE_DICTIONARY_PLAYER_LIST:{
      return {...state, playersList: action.payload.data}
    }
    default  :
      return {...state}
  }
}
export default dictionaryReducer;
