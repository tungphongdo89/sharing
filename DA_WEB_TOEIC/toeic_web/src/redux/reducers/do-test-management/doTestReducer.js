import * as config from '../../../commons/constants/Config';

const intiState ={
	listQuestionMinitest: [],
	listQuestionFullTest: [],
  loadingQuestion: false,
  loadingSubmitAnswer: false,
  listQuestionListeningChoosenDTOS: [],
  listQuestionReadingChoosenDTOS: [],
  listQuestionAndAnswerMinitest: [],
  listQuestionAndAnswerFullTest: [],
  showAnswer: false,
  doCurrentTest: false,
  doReading: false,
  doReadingFulltest: false,
  minuteForMinitest: 60,
  secondForMinitest: 0,
  hourForFulltest: 2,
  minuteForFulltest:0,
  secondForFulltest: 0,
  detailTest: {},
  noTranslating: false,
  doTime: '',
  loadingSaveResultMinitest: false,
  listHistoryMinitest: [],
  total: 0,
  checkGetListQuestionMinitest: false,
  listNameTest: [],
  loadingLstName: false,
  loadingGetListHistoryMinitest: false,
  showBtnBackHistoryMinitest: false,
  hideBtnSavHistoryMinitest: false
}

const doTestReducer = (state = intiState,action)=> {
  switch (action.type) {
    //lấy list question minitest
    case config.GET_LIST_QUESTION_MINITEST:{
      return {...state,checkGetListQuestionMinitest: false, loadingQuestion: false, hideBtnSavHistoryMinitest: false}
    }
    case config.GET_LIST_QUESTION_MINITEST_SUCCESS:{
      const {data} = action.payload;
      return {...state, listQuestionMinitest: data, loadingQuestion: true,
        showAnswer: false, doReading: false} //minuteForMinitest: 6, secondForMinitest: 10
    }
    case config.GET_LIST_QUESTION_MINITEST_FAILED:{
      return {...state}
    }

    //lấy list question đã chọn đáp án
    case config.GET_LIST_QUESTION_LISTENING_CHOOSEN:{
      const {data} = action.payload;
      return {...state, listQuestionListeningChoosenDTOS: data}
    }

    case config.GET_LIST_QUESTION_READING_CHOOSEN:{
      const {data} = action.payload;
      return {...state, listQuestionReadingChoosenDTOS: data}
    }

    //do current test again
    case config.DO_CURRENT_TEST_AGAIN:{
      const {data} = action.payload;
      return {...state, doCurrentTest: data}
    }

    //do reading after listening
    case config.DO_READING_AFTER_LISTENING:{
      const {data} = action.payload;
      return {...state, doReading: data}
    }

    //do reading after listening full test
    case config.DO_READING_AFTER_LISTENING_FULLTEST:{
      const {data} = action.payload;
      return {...state, doReadingFulltest: data}
    }

    //check get detail history or submit minitest
    case config.CHECK_GET_DETAIL_OR_SUBMIT_MINITEST:{

      const {data} = action.payload;
      return {...state, checkGetListQuestionMinitest: true, loadingQuestion: false}
    }

    //show Hide Button Back History Minitest
    case config.SHOW_HIDE_BTN_BACK_HISTORY_MINITEST:{

      const {data} = action.payload;
      return {...state, showBtnBackHistoryMinitest: data}
    }

    //submit answer minitest
    case config.SUBMIT_ANSWER_MINITEST:{
      const {data} = action.payload;
      return {...state, data, loadingSubmitAnswer: true}
    }
    case config.SUBMIT_ANSWER_MINITEST_SUCCESS:{
      const {data} = action.payload;
      return {...state, listQuestionAndAnswerMinitest: data, showAnswer: true, loadingSubmitAnswer: false, loadingQuestion: true}
    }
    case config.SUBMIT_ANSWER_MINITEST_FAILED:{
      return {...state}
    }

    //save result minitest
    case config.SAVE_RESULT_MINITEST:{
      const {data} = action.payload;
      return {...state, loadingSaveResultMinitest: true}
    }
    case config.SAVE_RESULT_MINITEST_SUCCESS:{
      const {data} = action.payload;
      return {...state, loadingSaveResultMinitest: false, hideBtnSavHistoryMinitest: true}
    }
    case config.SAVE_RESULT_MINITEST_FAILED:{
      return {...state}
    }

    //get list history of minitest
    case config.GET_LIST_HISTORY_MINITEST:{

      const {data} = action.payload;
      return {...state, loadingGetListHistoryMinitest: true}
    }
    case config.GET_LIST_HISTORY_MINITEST_SUCCESS:{

      const {data} = action.payload;
      let total = 0;
      if(action.payload.data.data[0]){
        total = action.payload.data.data[0].numberTest;
      }
      return {...state, listHistoryMinitest: data, total: total, loadingGetListHistoryMinitest: false}
    }
    case config.GET_LIST_HISTORY_MINITEST_FAILED:{
      return {...state}
    }

    //fulltest
    case config.GET_DETAIL_FULL_TEST:{
      return {...state, loadingQuestion: false}
    }
    case config.GET_DETAIL_FULL_TEST_SUCCESS:{
      return {...state, listQuestionFullTest: action.payload.data, loadingQuestion: true, showAnswer: false}
    }
    case config.GET_DETAIL_FULL_TEST_FAILED:{
      return {...state, loadingQuestion: true}
    }
    case config.GET_RESULT_FULL_TEST:{
      return {...state, loadingSubmitAnswer: true, }
    }
    case config.GET_RESULT_FULL_TEST_SUCCESS:{
      return {...state,listQuestionAndAnswerFullTest: action.payload.data,loadingSubmitAnswer: false, showAnswer: true, }
    }
    case config.GET_RESULT_FULL_TEST_FAILED:{
      return {...state, loadingSubmitAnswer: false,}
    }
    case config.CLEAN_TEST:{
      return {...state, listQuestionFullTest: [], listQuestionListeningChoosenDTOS: [],
        listQuestionReadingChoosenDTOS: [], checkGetListQuestionMinitest: false, loadingQuestion: false,
        listQuestionAndAnswerFullTest: [], showAnswer: false, doReadingFulltest:false, detailTest:{}}
    }
    case config.SAVE_FULL_TEST:{
      return {...state, detailTest:action.payload.data}
    }
    case config.CLEAN_LIST_CHOOSEN:{
      return {...state, listQuestionReadingChoosenDTOS: [],
        listQuestionListeningChoosenDTOS: [] }
    }

    //-------------------dịch câu-----------------------s
    case config.NO_TRANSLATION_WHEN_DOING_TEST:{
      const {data} = action.payload;
      return {...state, noTranslating: data}
    }
    case config.SAVE_DO_TIME:{
      return {...state, doTime: action.payload.data}
    }

    case config.GET_LIST_NAME_TEST:{
      return {...state,loadingLstName: true }
    }
    case config.GET_LIST_NAME_TEST_SUCCESS:{
      return {...state, listNameTest: action.payload.data,loadingLstName: false }
    }
    case config.GET_LIST_NAME_TEST:{
      return {...state, loadingLstName: false }
    }
    default: {
      return state;
    }
  }
}

export default doTestReducer
