import { combineReducers } from "redux"
import { login } from "./loginReducer"
import { register } from "./registerReducers"
import { changePassword } from "./changePasswordReducer"

const authReducers = combineReducers({
  login,
  register,
  changePassword
})

export default authReducers
