import * as config from "../../../commons/constants/Config";
import {toast} from "react-toastify";

const initialState = false
const resendEmail = (state = initialState, action) => {
  switch (action.type) {
    case config.RESEND_EMAIL:{
      state = true
      return state
    }
    case config.RESEND_EMAIL_SUCCESS: {
      state = false
      return state
    }
    case config.RESEND_EMAIL_FAILED:
      return state

    default:
      return state

  }
}
export default resendEmail
