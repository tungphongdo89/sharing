import * as config from "../../../commons/constants/Config";


const initialState = {
    passNew: "",
    passOld:"",
    passConfirm:"",
    loading: false
}

export const changePassword = (state = initialState, action ) => {
    switch (action.type) {
        case config.CHANGE_PASSWORD:{
            return {
                ...state,
              loading: true
            }
        }
        case config.CHANGE_PASSWORD_SUCCESS: {

            return {
                ...state,
              loading: false
            }

        }
        case config.CHANGE_PASSWORD_FAILED: {

            return {
                ...state,
              loading: false
            }
        }
        default:
            return state
    }
}
