import * as config from "../../../commons/constants/Config";
import {ROLE_ADMIN, ROLE_PREVIEW, ROLE_STUDENT, ROLE_TEACHER} from "../../../commons/constants/CONSTANTS";
import {getSessionCookie, removeSession} from "../../../commons/configCookie";
import {toast} from "react-toastify"
import {history} from "../../../history"
import {showMessage} from "../../../commons/utils/convertDataForToast";

let token = getSessionCookie()
const initialState = {
  isAuthenticated: false,
  userRole: ROLE_STUDENT,
  isAdmin: false,
  isStudent: false,
  isTeacher: false,
  isMember: true,
  isLogging: false,
  listObject: [],
  userInfoLogin: {},
  isLoadingUpdateProfile: null,
  isEdit: false,
  studentDetail: {},
  isLoading: false,
  isError401: false,
}
export const login = (state = initialState, action) => {
  switch (action.type) {
    case config.LOGIN_USER_REQUEST: {
      removeSession();
      return {...state, userRole: ROLE_STUDENT, isLogging: true, isMember: true}
    }
    case config.LOGIN_USER_SUCCESS: {
      // toast.success("Đăng nhập thành công!")
      toast.success(showMessage("login.successful"));
      // MessageNotification.success("Đăng nhập thành công!")
      const {data} = action.payload
      if (data.userDTO.lstRole.length > 0) {
        if (data.userDTO.lstRole[0].roleCode.toUpperCase() === (ROLE_ADMIN)) {
          return {
            ...state,
            isAuthenticated: true,
            isLogging: false,
            userRole: ROLE_ADMIN,
            token: JSON.stringify(data),
            listObject: data.userDTO.lstObject,
            userInfoLogin: data.userDTO,
            isError401: false
          }
        } else if (data.userDTO.lstRole[0].roleCode.toUpperCase() === (ROLE_STUDENT)) {
          return {
            ...state,
            isAuthenticated: true,
            isLogging: false,
            userRole: ROLE_STUDENT,
            token: JSON.stringify(data),
            listObject: data.userDTO.lstObject,
            userInfoLogin: data.userDTO,
            isError401: false
          }
        } else if (data.userDTO.lstRole[0].roleCode === ROLE_PREVIEW) {
          return {
            ...state,
            isAuthenticated: true,
            isLogging: false,
            userRole: ROLE_PREVIEW,
            token: JSON.stringify(data),
            listObject: data.userDTO.lstObject,
            userInfoLogin: data.userDTO,
            isError401: false
          }
        } else if (data.userDTO.lstRole[0].roleCode.toUpperCase() === (ROLE_TEACHER)) {
          return {
            ...state,
            isAuthenticated: true,
            isLogging: false,
            userRole: ROLE_TEACHER,
            token: JSON.stringify(data),
            listObject: data.userDTO.lstObject,
            userInfoLogin: data.userDTO,
            isError401: false
          }
        }
      }
      return {
        ...state, isAuthenticated: true, isMember: true, isLogging: false, token: JSON.stringify(data),
        listObject: data.userDTO.lstObject
      }
    }
    case config.LOGIN_USER_FAILURE: {
      return {...initialState}
    }
    case config.LOGOUT_USER:
      removeSession()
      // history.push("/")
      console.log("-------------------log out user");
      return {
        ...state,
        isAuthenticated: false,
        isLogging: false,
        token: "",
        listObject: [],
        userRole: ROLE_STUDENT,
        userInfoLogin: {},
        isError401: true
      }
    case config.CHANGE_ROLE:
      removeSession()
      history.push("/")
      return {...state, isAuthenticated: false, isLogging: false, token: "", listObject: [], userRole: ROLE_STUDENT}

    case config.LOGIN_SETTING_ACCOUNT:
      removeSession()
      return {
        ...state,
        isAuthenticated: false,
        isLogging: false,
        token: "",
        listObject: [],
        userRole: ROLE_STUDENT,
        userInfoLogin: {}
      }
    case config.UPDATE_PROFILE_ADMIN: {
      return {...state, isLoadingUpdateProfile: true}
    }
    case config.UPDATE_PROFILE_ADMIN_SUCCESS:
      state.userInfoLogin = {
        ...state.userInfoLogin,
        userShowName: action.payload.data.userShowName,
        userAvatar: action.payload.data.userAvatar
      }
      state.isLoadingUpdateProfile = false
      return {...state}

    case config.UPDATE_PROFILE_ADMIN_FAIL:
      return {...state, isLoadingUpdateProfile: false}
    case config.GET_STUDENT_PROFILE: {
      return {...state, isLoading: true}
    }
    case config.GET_STUDENT_PROFILE_SUCCESS: {
      return {...state, studentDetail: action.payload.data, isEdit: false, isLoading: false}
    }
    case config.GET_STUDENT_PROFILE_FAILURE: {
      return {...state, isLoading: false}
    }
    case config.UPDATE_STUDENT_PROFILE: {
      return {
        ...state,
        isEdit: true
      }
    }
    case config.UPDATE_STUDENT_PROFILE_SUCCESS: {
      state.userInfoLogin = {
        ...state.userInfoLogin,
        userShowName: action.payload.data.userShowName,
        userAvatar: action.payload.data.userAvatar,
        phone: action.payload.data.phone,
        target: action.payload.data.target,
        currentScore: action.payload.data.currentScore
      }
      state.studentDetail = state.userInfoLogin
      return {
        ...state,
        isEdit: false
      }
    }
    case config.UPDATE_STUDENT_PROFILE_FAILURE:
      return {...state, isEdit: false}

    default:
      return state
  }
}
