import * as config from "../../../commons/constants/Config";

const initialStatusSendMail = 0

const sendMailReducer = (state = initialStatusSendMail, action) => {
  switch (action.type) {
    case config.SEND_EMAIL_SUCCESS :
      state = 1
      return state

    case config.SEND_EMAIL_FAILED :
      state = initialStatusSendMail
      return state

    default:
      return state;
  }
}
export default sendMailReducer
