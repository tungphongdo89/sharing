import * as config from "../../../commons/constants/Config";

const initialState = {
  isLogging: false,
}
export const register = (state = initialState, action) => {
  switch (action.type) {
    case config.REGISTER_WITH_EMAIL: {
      return { ...state,isLogging: true, values: action.payload}
    }
    case config.SIGNUP_WITH_JWT:
      return {
        ...state,
        isLogging: true
      }
    case config.REGISTER_SUCCESS:{
      break;
    }
    case config.REGISTER_FAILD:{
      return{
        ...state,isLogging: false
      }
    }
    default: {
      return state
    }
  }
}
