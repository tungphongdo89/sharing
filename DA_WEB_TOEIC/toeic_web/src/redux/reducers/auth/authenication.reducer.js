// import {getSessionCookie, removeSession} from "../../../commons/configCookie/index";
// import * as config from "../../../commons/constants/Config";
// // import MessageNotification from "../commons/notification/notifyCommon";
// import {ROLE_ADMIN, ROLE_PREVIEW, ROLE_STUDENT, ROLE_TEACHER} from "../../../commons/constants/CONSTANTS";
//
// let token = getSessionCookie()
// const initialState = {
//   isAuthenticated: false,
//   isAdmin: false,
//   isStudent: false,
//   isTeacher: false,
//   isMember: true,
//   isPreview: false
// }
//
// const authenticationReducer = (state = initialState, action) => {
//   switch (action.type) {
//     case config.LOGIN_USER_REQUEST: {
//       removeSession();
//       return {...state, isAdmin: false, isStudent: false, isTeacher: false, isMember: true}
//     }
//     case config.LOGIN_USER_SUCCESS: {
//       // MessageNotification.success("Đăng nhập thành công!")
//       const {data} = action.payload
//       if (data.userDTO.lstRole[0].roleCode === ROLE_ADMIN ) {
//         return {...state, isAuthenticated: true, isAdmin: true, token: JSON.stringify(data)}
//       } else if (data.userDTO.lstRole[0].roleCode === ROLE_PREVIEW) {
//         return {...state, isAuthenticated: true, isPreview: true, token: JSON.stringify(data)}
//       } else if (data.userDTO.lstRole[0].roleCode === ROLE_STUDENT) {
//         return {...state, isAuthenticated: true, isStudent: true, token: JSON.stringify(data)}
//       } else if (data.userDTO.lstRole[0].roleCode === ROLE_TEACHER) {
//         return {...state, isAuthenticated: true, isTeacher: true, token: JSON.stringify(data)}
//       }
//       return {...state, isAuthenticated: true, isMember: true, token: JSON.stringify(data)}
//     }
//     case config.LOGIN_USER_FAILURE: {
//       // MessageNotification.error("Đăng nhập thất bại!")
//       return {...state, isAuthenticated: false, token: ""}
//     }
//     case config.LOGOUT_USER:
//       return {...state, isAuthenticated: false, token: ""}
//     default:
//       return state
//   }
// }
// export default authenticationReducer
