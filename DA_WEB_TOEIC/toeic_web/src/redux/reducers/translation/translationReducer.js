import * as config from '../../../commons/constants/Config';

const intiState ={
	listQaTransAv: [],
  isLoading: false,
  listQaTransVa: [],
  checkReadOnly: false,
  levelName: ""
}

const translationReducer = (state = intiState,action)=> {
  switch (action.type) {
    //dịch anh - việt by topic
    case config.FETCH_QA_TRANS_AV_BY_TOPIC:{
      return {...state,isLoading: true}
    }
    case config.FETCH_QA_TRANS_AV_BY_TOPIC_SUCCESS:{
      const {data} = action.payload;
      return {...state,dataResp: data,isLoading:true}
    }
    case config.FETCH_QA_TRANS_AV_BY_TOPIC_FAILED:{
      return {...state,isLoading: false}
    }

    //dịch việt - anh by topic
    case config.FETCH_QA_TRANS_VA_BY_TOPIC:{
      return {...state,isLoading: true}
    }
    case config.FETCH_QA_TRANS_VA_BY_TOPIC_SUCCESS:{
      const {data} = action.payload;
      return {...state,dataResp: data,isLoading:true}
    }
    case config.FETCH_QA_TRANS_VA_BY_TOPIC_FAILED:{
      return {...state,isLoading: false}
    }

    //dịch anh - việt by level
    case config.FETCH_QA_TRANS_AV_BY_LEVEL:{
      return {...state,isLoading: true}
    }
    case config.FETCH_QA_TRANS_AV_BY_LEVEL_SUCCESS:{
      const {data} = action.payload;
      return {...state,dataResp: data, listQaTransAv: data, isLoading:true}
    }
    case config.FETCH_QA_TRANS_AV_BY_LEVEL_FAILED:{
      return {...state,isLoading: false}
    }

    //dịch việt - anh by level
    case config.FETCH_QA_TRANS_VA_BY_LEVEL:{
      return {...state,isLoading: true}
    }
    case config.FETCH_QA_TRANS_VA_BY_LEVEL_SUCCESS:{
      const {data} = action.payload;
      return {...state,dataResp: data, listQaTransVa: data, isLoading:true}
    }
    case config.FETCH_QA_TRANS_VA_BY_LEVEL_FAILED:{
      return {...state,isLoading: false}
    }

    //disable answer when submiting
    case config.DISABLE_ANSWER_WHEN_SUBMIT:{
      const {data} = action.payload;
      return {...state,checkReadOnly: data}
    }

    //set level title for av va
    case config.SET_LEVEL_TITLE_FOR_AV_VA:{
      const {data} = action.payload;
      return {...state,levelName: data}
    }

    default: {
      return state;
    }
  }
}

export default translationReducer
