import * as config from "../../../commons/constants/Config";
// import MessageNotification from "../commons/notification/notifyCommon";
import {toast} from "react-toastify";
import {showMessage} from "../../../commons/utils/convertDataForToast";

const initial = {
  dataResult:{},
  lstUser: [],
  total: 0,
  loading: false,
  userDetail: { },
  isLoadingInvitedUser : null
}
const userReducer = (state = initial, action) => {
  switch (action.type) {
    case config.FETCH_USER: {
      return {...state,loading:true}
    }
    case config.FETCH_USER_SUCCESS: {
      // MessageNotification.success("Lấy thông tin user thành công!")
      const {data} = action.payload
      return {...state, dataResult: data,lstUser: data.data,total: data.total,loading: false}
    }

    case config.FETCH_USER_FAILURE: {
      // MessageNotification.success("Lấy thông tin user thất bại!")
      return {...state, dataResult: {}}
    }
    case config.FETCH_USER_DETAIL:{
      const {data} = action.payload;
      return {
        ...state,
        userDetail: data
      }
    }
    case config.UPDATE_USER_DETAIL:{
      return {
        ...state
      }
    }
    case config.UPDATE_USER_DETAIL_SUCCESS:{
      const { data } = action.payload;
      toast.success(showMessage("update.status.successful"))
      return {
        ...state,
        userDetail: data
      }
    }
    case config.CREATE_USER_ADMIN_INVITED_FAILUE :
    	{
    	  if(state.isLoadingInvitedUser !== null){
          state.isLoadingInvitedUser = !state.isLoadingInvitedUser
        }
    	  else {
          state.isLoadingInvitedUser = false
        }
        return state
    	}

    default:
      return state;
  }
}
export default userReducer
