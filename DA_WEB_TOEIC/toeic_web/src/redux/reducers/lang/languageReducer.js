import * as config from "../../../commons/constants/Config"

let initialState = {
  language: "vn"
}

const languageReducer = (state = initialState, action) => {
  switch (action.type) {
    case config.CHANGE_LANGUAGE:
      const {data} = action.payload
      return {...state, data}
    default:
      return {...state}
  }
}
export default languageReducer
