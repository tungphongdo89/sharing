import * as config from "../../../commons/constants/Config";

const initial = {
  loading:false
}

const ContactReducer = (state = initial, action) => {
  switch (action.type) {
    case config.CONTACT_ACTION: {
      return {...state, data: action.payload,loading: true}
    }
    case config.CONTACT_SUCCESS:{
      return {...state,loading:false}
    }
    case config.CONTACT_FAILED:{
      return {...state,loading: false}
    }
    default:
      return state;
  }
}

export default ContactReducer;
