import {combineReducers} from "redux"
import calenderReducer from "./calendar/"
import emailReducer from "./email/"
import chatReducer from "./chat/"
import todoReducer from "./todo/"
import customizer from "./customizer/"
import auth from "./auth/"
import navbar from "./navbar/Index"
import dataList from "./data-list/"
import sideBarReducer from "./sidebar/sideBarReducer"
import bannerReducer from "./banner/banner.reducer"
import userReducer from "./user/user.reducer"
import authenticationReducer from "./auth/authenication.reducer"
import ContactReducer from "./contact/contactReducer";
import studentAccountReducer from "./studentAccount/studentAccountReducer";
import resendEmailReducer from "./auth/resendEmailReducer"
import forgotPasswordReducer from "./auth/forgotPasswordReducer"
import topicReducer from './topic/topicReducer'
import categoryReducer from './category-management/categoryReducer';
import multiListeningReducer from "./practice/listening/multiListentingReducer";
import translationReducer from './translation/translationReducer';
import readingReducer from './practice/reading/readingReducer';
import testReducer from './test/testReducer';
import doTestReducer from './do-test-management/doTestReducer';
import dictionaryReducer from './dictionary/dictionaryReducer';
import historyTestReducer from './history-test/historyTestReducer';
import wordReducer from './word/wordReducer';
import historyPracticseReducer from './historyPractices/historyPracticesReducer'
import languageReducer from './lang/languageReducer'

const rootReducer = combineReducers({
  calendar: calenderReducer,
  emailApp: emailReducer,
  todoApp: todoReducer,
  chatApp: chatReducer,
  customizer: customizer,
  auth: auth,
  navbar: navbar,
  dataList: dataList,
  ContactReducer:ContactReducer,
  // bannerReducer,
  userReducer,
  authenticationReducer,
  sideBarReducer,
  bannerReducer,
  studentAccountReducer,
  resendEmailReducer,
  forgotPasswordReducer,
  topicReducer,
  categoryReducer,
  testReducer,
  multiListeningReducer,
  translationReducer,
  readingReducer,
  doTestReducer,
  dictionaryReducer,
  historyTestReducer,
  wordReducer,
  historyPracticseReducer,
  languageReducer
})

export default rootReducer
