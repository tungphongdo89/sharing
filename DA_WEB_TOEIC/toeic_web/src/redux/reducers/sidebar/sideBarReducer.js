import {HIDE_SIDEBAR, SHOW_SIDEBAR} from '../../../commons/constants/Config'

const initState = {
  open: true
}
const sideBarReducer = (state = initState, action) => {
  switch (action.type) {
    case SHOW_SIDEBAR:
      const {data} = action.payload
      return {...state, open: !data}
    case HIDE_SIDEBAR:
      return {...state, open: false}
    default:
      return state;
  }
}
export default sideBarReducer
