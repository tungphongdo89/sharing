import * as config from "../../../commons/constants/Config";
import {showMessage} from "../../../commons/utils/convertDataForToast";
const initialState = {
    highLightWord:'',
    wordTranslated:'',
    description :'',
    cordinate: {
        x:0,y:0
    },
    status:false,
    title :'',
    foundSuccess :false,
    synonymous:'',
    transcribe:'',
    mp3 :'',
    statusListening:false,
    wordInHtml:'',
}
const wordReducer = (state = initialState, action) => {
    switch (action.type) {
        case config.SET_HIGHLIGHT_WORD: {
            //state.wordPair = {};
            state.highLightWord = action.payload.data.result;
            state.wordInHtml = action.payload.data.wordInHtml;
            console.log(state.wordInHtml);
            //state.title =   action.payload.data;
            return { ...state ,description:'',title :'' }
        }
        case config.FETCH_VOCA_DETAIL_SUCCESS:{
            // console.log(action.payload.data);
            //const oldWord = state.wordInHtml;
            if(action.payload.data && action.payload.data.description) {   
                 state= {
                    ...state,
                    transcribe :action.payload.data.transcribe,
                    description: action.payload.data.description,
                    synonymous : action.payload.data.synonymous,
                    mp3 : action.payload.data.mp3,
                    foundSuccess :true,
                    title:state.highLightWord
                }
            }
            else {
                state = {
                    ...state,
                    title : showMessage("lookup.in.dic"),
                    // description:
                    //  'Không tìm thấy từ "<b>'+oldWord+'</b>" trong từ điển. Xin vui lòng tìm kiếm trực tuyến.<br/><i>Xin cảm ơn!</i>',
                     foundSuccess:false
                }
                //state.wordPair = null;
            }
            return {...state}
        }
        case config.FETCH_VOCA_DETAIL_FAILURE:{
            return { ...initialState }
        }
        case config.SET_CORDINATE_POINTER: {
            state.cordinate  =  action.payload.data;
            return { ...state }
        }
        case config.TOGGLE_STATUS_LISTENING: {
            state= {
                ...state,
                statusListening : action.payload.data,
            }
            return { ...state }
        }
        case config.TOGGLE_POPUP_TRANSLATE:{
            state = {
                ...state,
                status: action.payload.data
            }
            return { ...state }
        }
        default:
            return { ...state }
    }
}
export default wordReducer;
