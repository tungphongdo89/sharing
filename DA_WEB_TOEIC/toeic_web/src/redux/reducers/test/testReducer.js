import * as config from "../../../commons/constants/Config";

const initialTest = {
  lstTest: [],
  dataRes: {},
  isLoading: false,
  loading: false,
  detailTest: {},//kiểm tra xem có categoryId ko (có=>sửa|không=>thêm)
  isViewDetail: false,
  part: 1,
  loadingTest: false,
  total: 0,
  dataFilter: {},
  lstTypeExercise: [],
  lstPartExercise: [],
  lstTypeLevel: [],
  lstTypeFile: [],
  lstTopicTrans: [],
  lstTopicByPart: [],
  loadingPart: false,
  loadingTopic: false,
  listCheckedListening: [],
  listCheckedReading: [],
  listCategory: [],
  listStudentFaildByTestId:[],
  totalStudentFail: 0
}

const TestReducer = (state = initialTest, action) => {
  switch (action.type) {
    case config.FETCH_TEST: {
      return {...state, isLoading: true}
    }
    case config.FETCH_TEST_SUCCESS: {
      const {data} = action.payload;
      return {...state, dataRes: data, lstTest: data.data, isLoading: false, total: data.total}
    }
    case config.FETCH_TEST_FAILD: {
      return {...state, lstTest: [],total: 0, isLoading: false}
    }
    case config.FETCH_DATA_FILTER: {
    }
    case config.FETCH_DATA_FILTER_SUCCESS: {
      const {data} = action.payload;
      return {...state, dataFilter: data}
    }
    case config.FETCH_DETAIL_TEST: {
      const {data, check} = action.payload;
      return {...state, detailTest: data, isViewDetail: check, loadingViewDetail: true}
    }
    case config.FETCH_DETAIL_TEST_SUCCESS:{
      const {data, check} = action.payload;
      return {...state,detailTest: data, isViewDetail: check, loadingViewDetail: false,detailTestTemp:data,isLoading: false}
    }
    case config.FETCH_DETAIL_TEST_FAILED: {
      const {data, check} = action.payload;
      return {...state, detailTest: data, isViewDetail: check, loadingViewDetail: false,isLoading: false}
    }
    case config.CREATE_TEST: {
      return {...state, isViewDetail: false, detailTest: {}, loading: true}
    }
    case config.CREATE_TEST_SUCCESS: {
      const {data} = action.payload;
      return {
        ...state,
        lstTest: state.lstTest.concat([data]),
        isViewDetail: false,
        detailTest: {},
        loading: false
      }
    }
    case config.CREATE_TEST_FAILED: {
      const {data} = action.payload;
      return {...state, isViewDetail: false, detailTest: {}, loading: false}
    }
    case config.UPDATE_TEST: {
      return {...state, loading: true, isViewDetail: false}
    }
    case config.UPDATE_TEST_SUCCESS: {
      const {data} = action.payload;
      return {...state, loading: false, isViewDetail: false}
    }
    case config.UPDATE_TEST_FAILED: {
      return {...state, loading: false, isViewDetail: false}
    }
    case  config.DELETE_TEST: {
      return {...state, loadingTest: true}
    }
    case  config.DELETE_TEST_SUCCESS: {
      return {...state, loadingTest: false}
    }
    case  config.DELETE_TEST_FAILD: {
      return {...state, loadingTest: false}
    }
    case config.GET_LIST_TYPE_EXERCISE: {
      return {...state}
    }
    case config.GET_LIST_TYPE_EXERCISE_SUCCESS: {
      const {data} = action.payload
      return {...state, lstTypeExercise: data}
    }
    case config.GET_LIST_TYPE_EXERCISE_FAILED: {
      return {...state}
    }
    case config.GET_LIST_PART_EXERCISE: {
      return {...state, loadingPart: true}
    }
    case config.GET_LIST_PART_EXERCISE_SUCCESS: {
      const {data} = action.payload
      if (data !== null && data.length!==0) {
        if (data[0].paramValue === null) {
          state.lstTopicTrans = data;
          state.loadingTopic = false
        } else {
          state.lstPartExercise = data
          state.loadingPart = false
        }
      }
      else{
        state.lstTopicTrans=[];
        state.lstPartExercise=[];
      }
      return {...state}
    }
    case config.GET_LIST_PART_EXERCISE_FAILED: {
      return {...state, loadingPart: false}
    }
    case config.GET_LIST_TYPE_LEVEL: {
      return {...state}
    }
    case config.GET_LIST_TYPE_LEVEL_SUCCESS: {
      const {data} = action.payload
      return {...state, lstTypeLevel: data}
    }
    case config.GET_LIST_TYPE_LEVEL_FAILED: {
      return {...state}
    }
    case config.GET_LIST_TYPE_FILE_UPLOAD: {
      return {...state}
    }
    case config.GET_LIST_TYPE_FILE_UPLOAD_SUCCESS: {
      const {data} = action.payload
      return {...state, lstTypeFile: data}
    }
    case config.GET_LIST_TYPE_FILE_UPLOAD_FAILED: {
      return {...state}
    }
    case config.GET_LIST_TOPIC_BY_PART: {
      return {...state, loadingTopic: true}
    }
    case config.GET_LIST_TOPIC_BY_PART_SUCCESS: {
      const {data} = action.payload
      return {...state, lstTopicByPart: data, loadingTopic: false}
    }
    case config.GET_LIST_TOPIC_BY_PART_FAILED: {
      return {...state, loadingTopic: false}
    }
    case config.EDIT_DETAIL_TEST: {
      return {...state, detailTest: {}, isViewDetail: false}
    }
    case config.UPDATE_LIST_CHECKED_LISTENING:{
      let index = state.listCheckedListening.indexOf(action.payload.data)
      if(index === -1){
        state.listCheckedListening.push(action.payload.data)
      } else {
        state.listCheckedListening.splice(index,1)
      }
      return {...state}
    }
    case config.UPDATE_LIST_CHECKED_READING:{
      let index = state.listCheckedReading.indexOf(action.payload.data)
      if(index === -1){
        state.listCheckedReading.push(action.payload.data)
      } else {
        state.listCheckedReading.splice(index,1)
      }
      return {...state}
    }
    case config.DELETE_LIST_CHECKED:{
      return {...state,
        listCheckedListening: [],  listCheckedReading: []}
    }
    case config.GET_LIST_CATEGORY: {
      return {...state, isLoading: true}
    }
    case config.GET_LIST_CATEGORY_SUCCESS:{
      return {...state,listCategory: action.payload.data , isLoading: false}
    }
    case config.GET_LIST_CATEGORY_FAILD:{
      return {...state, isLoading: false}
    }
    case config.GET_LIST_STUDENT_FAILD_BY_TEST_ID:{
      return {...state,isLoadingPopUp: true}
    }
    case config.GET_LIST_STUDENT_FAILD_BY_TEST_ID_SUCCESS:{
      return {...state,isLoadingPopUp: false,dataStudentFaild:action.payload.data.data, totalStudentFail: action.payload.data.total}
    }
    case config.GET_LIST_STUDENT_FAILD_BY_TEST_ID_FAILD:{
      return {...state,isLoadingPopUp: false}
    }
    case config.UPDATE_STATUS_POPUP_TEST:{
      return {...state, isLoading: action.payload.data}
    }
    default:
      return state;
  }
}

export default TestReducer;
