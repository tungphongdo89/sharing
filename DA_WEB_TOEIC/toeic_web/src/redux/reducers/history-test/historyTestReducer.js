import * as config from "../../../commons/constants/Config";

const initialTest = {
  lstHistoryTest: [],
  lstDetailOfTest: [],
  lstRankOfTest: [],
  isLoading: false,
  loading: false,
  loadingRank: false,
  total: 0,
  count: 0,
  numberOfUser: 0,
  listQuestionAndAnswerFullTest: [],
  loadingResult: false,
  viewHistory: false,
  detailHistoryFullTest: {},
  active: false,
}

const HistoryTestReducer = (state = initialTest, action) => {
  switch (action.type) {
    case config.FETCH_HISTORY_TEST: {
      return {...state, isLoading: true}
    }
    case config.FETCH_HISTORY_TEST_SUCCESS: {
      let total = 0;
      if(action.payload.data.data[0]){
        total = action.payload.data.data[0].numberTest;
      }
      return {...state, isLoading: false, lstHistoryTest: action.payload.data, total: total}
    }
    case config.FETCH_HISTORY_TEST_FAILED: {
      return {...state, isLoading: false}
    }
    case config.GET_DETAIL_TEST: {
      return {...state, loading: true}
    }
    case config.GET_DETAIL_TEST_SUCCESS:{
      return {...state, lstDetailOfTest: action.payload.data, loading: false, count: action.payload.data.total}
    }
    case config.GET_DETAIL_TEST_FAILED:{
      return {...state, loading: false}
    }
    case config.GET_RANK_TEST:{
      return {...state, loadingRank: true}
    }
    case config.GET_RANK_TEST_SUCCESS:{
      return {...state, loadingRank: false, lstRankOfTest: action.payload.data, numberOfUser: action.payload.data[0].numberUser }
    }
    case config.GET_RANK_TEST_FAILED: {
      return {...state, loadingRank: false}
    }
    case config.GET_HISTORY_FULL_TEST:{
      return {...state,loadingResult: false, viewHistory: true}
    }
    case config.GET_HISTORY_FULL_TEST_SUCCESS:{
      return {...state, listQuestionAndAnswerFullTest: action.payload.data,loadingResult: true, viewHistory: true }
    }
    case config.GET_HISTORY_FULL_TEST_FAILDED:{
      return {...state, loadingResult: true }
    }
    case config.GET_DETAIL_HISTORY_FULL_TEST:{
      return{...state,detailHistoryFullTest: action.payload.data}
    }
    case config.CLEAN_HISTORY:{
      return {...state,detailHistoryFullTest: {},listQuestionAndAnswerFullTest: [],loadingResult: false, viewHistory:false }
    }
    case config.UPDATE_ACTIVE_TAB_WHEN_FAIL:{
      return {...state, active: action.payload.data}
    }
    default:
      return state;
  }
}

export default HistoryTestReducer;
