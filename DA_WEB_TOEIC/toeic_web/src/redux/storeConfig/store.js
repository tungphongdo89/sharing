import {applyMiddleware, compose, createStore} from "redux"
import createDebounce from "redux-debounced"
import thunk from "redux-thunk"
import rootReducer from "../reducers/rootReducer"
import rootSaga from "../saga/sagas";
import createSagaMiddleware from 'redux-saga'
import {persistReducer, persistStore} from 'redux-persist';
import storage from 'redux-persist/lib/storage';

const sagaMiddleware = createSagaMiddleware();

const middlewares = [thunk, createDebounce(), sagaMiddleware];

const persistConfig = {
  key: 'root',
  storage: storage,
  blacklist: ['authReducers']
};

const pReducer = persistReducer(persistConfig, rootReducer);


const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose
const store = createStore(
  pReducer,
  {},
  composeEnhancers(applyMiddleware(...middlewares))
)

export default store;
export const persistor = persistStore(store);
sagaMiddleware.run(rootSaga)
