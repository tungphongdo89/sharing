const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');
const PreloadWebpackPlugin = require('preload-webpack-plugin');
// const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;


// const getClientEnvironment = require('./.env');

// const publicUrl = '';
// // Get environment variables to inject into our app.
// const env = getClientEnvironment(publicUrl);

const VENDOR_LIBS = [
  'axios',
  'jquery',
  'react',
  'react-dom',
  'react-redux',
  'react-router-dom',
  'redux',
  'redux-thunk',
]
const devServer = {
  port: 3000,
  open: true,
  historyApiFallback: true,
  compress: true,
  stats: 'minimal'
}

const config = {
  entry: {
    bundle: "./src/index.js",
    vendors: VENDOR_LIBS
  },
  output: {
    filename: '[name].[chunkhash].js',
    // chunkFilename: "[name]/[id].[chunkhash].chunk.js",
    path: path.resolve(__dirname, 'build'),
    publicPath: '/'
  },
  module: {
    rules: [
      {test: /\.js$/, use: 'babel-loader', exclude: /(node_modules)/},
      {
        test: /\.s?[ac]ss$/,
        use: [
          MiniCssExtractPlugin.loader,
          {loader: 'css-loader', options: {url: false, sourceMap: true}},
          {loader: 'sass-loader', options: {sourceMap: true}}
        ],
      },
      // {
      //   test: /\.s[ac]ss$/i,
      //   use: [
      //     'style-loader',
      //     'css-loader',
      //     'sass-loader'
      //   ]
      // },
      {
        loader: 'file-loader',
        test: /\.jpe?g$|\.gif$|\.png$|\.svg$|\.woff$|\.woff2$|\.eot$|\.tff$|\.wav$|\.mp3$|\.ico$|\.ttf$/i,
        options: {
          esModule: false,
        }
      }
    ]
  },
  plugins: [
    // new ExtractTextPlugin('translation.module.css'),
    // new BundleAnalyzerPlugin(),
    new MiniCssExtractPlugin({
      filename: "style.css"
    }),
    // new webpack.DefinePlugin(env.stringified),
    new webpack.ProvidePlugin({
      '$': 'jquery',
      'jQuery': 'jquery',
      'windown.$': 'jquery',
      'windown.jQuery': 'jquery',
    }),
    // new config.optimization.splitChunks({
    //     name: 'vendor',
    // }),
    new HtmlWebpackPlugin({
      template: './src/index.html',
      filename: 'index.html',
      // inject: false,
      hash: true,
    }),
    new webpack.DefinePlugin({
      'process.env': {
        // This has effect on the react lib size
        'NODE_ENV': JSON.stringify('production'),
      }
    }),
    new PreloadWebpackPlugin({
      rel: 'preload',
      as(entry) {
        if (/\.s?[ac]ss$/.test(entry)) return 'style';
        if (/\.woff$|\.woff2$|\.eot$/.test(entry)) return 'font';
        if (/\.jpe?g$|\.gif$|\.png$|\.svg$|\.tff$|\.ico$|\.ttf$/i.test(entry)) return 'image';
        if (/\.wav$|\.mp3$/.test(entry)) return 'video';
        return 'script';
      },
      include: ['bundle', 'vendors', 'asyncChunks', '[name].style.css']
      // include: 'asyncChunks'
    })
  ],
  optimization: {
    // splitChunks: {
    //   cacheGroups: {
    //     commons: {
    //       test: /[\\/]node_modules[\\/]/,
    //       name: 'vendors',
    //       chunks: 'all',
    //       minChunks: 1,
    //       priority: -10,
    //     },
    //     default: {
    //       minChunks: 2,
    //       priority: -20,
    //       reuseExistingChunk: true,
    //     },
    //   },
    // },
    splitChunks: {
      chunks: 'all',
      maxInitialRequests: 10,
      minSize: 0,
      cacheGroups: {
        vendor: {
          test: /[\\/]node_modules[\\/]/,
          chunks: chunk => chunk.name === 'frame',
          name(module) {
            const packageName = module.context.match(
              /[\\/]node_modules[\\/](.*?)([\\/]|$)/,
            )[1];
            // Remove @ from package names to guarantee compatibility with all servers
            return `npm.${packageName.replace('@', '')}`;
          },
        },
      },
    },
    runtimeChunk: {
      name: "manifest",
    },
    minimize: true,
    minimizer: [
      new TerserPlugin({
        terserOptions: {
          ecma: undefined,
          warnings: false,
          parse: {},
          compress: {},
          mangle: true, // Note `mangle.properties` is `false` by default.
          module: false,
          output: null,
          toplevel: false,
          nameCache: null,
          ie8: false,
          keep_classnames: undefined,
          keep_fnames: false,
          safari10: false,
        },
      }),
    ],
  },
  devServer
}

module.exports = config
