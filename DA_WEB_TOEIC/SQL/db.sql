   select * from CATEGORY where ID = 304;
                SELECT
                    ID as categoryId,
                    NAME as nameCategory,
                    PATH_FILE_1 as pathFile1,
                    PATH_FILE_2 as pathFile2,
                    TRANSCRIPT as transcript,
                    LEVEL_CODE as levelCode,
                    topicName, 
                    topicId
                FROM
                (
                    SELECT
                        CATEGORY.ID ,
                        CATEGORY.NAME ,
                        CATEGORY.PATH_FILE_1 ,
                        CATEGORY.PATH_FILE_2 ,
                        CATEGORY.TRANSCRIPT,
                        CATEGORY.LEVEL_CODE,
                        TOPICS.NAME as topicName,
                        TOPICS.ID as topicId
                    FROM CATEGORY 
                    INNER JOIN TOPICS 
                    ON CATEGORY.PARENT_ID = TOPICS.ID
                    WHERE TOPICS.PART_TOPIC_CODE = 'PART3' AND CATEGORY.LEVEL_CODE = 'EASY' AND CATEGORY.ID IN 
                    (
                    SELECT * FROM ( SELECT 
                    CATEGORY.ID
                    FROM QUESTION_ANSWER_LISTENING INNER JOIN CATEGORY
                    ON QUESTION_ANSWER_LISTENING.PARENT_ID = CATEGORY.ID
                    INNER JOIN TOPICS
                    ON TOPICS.ID = CATEGORY.PARENT_ID
                    WHERE TOPICS.ID = 562))
                    order by DBMS_RANDOM.RANDOM
                )
                WHERE rownum <= 7;
                
                --get list question by categoryId
                SELECT
                QUESTION_ANSWER_LISTENING.ID id,
                QUESTION_ANSWER_LISTENING.NAME name,
                QUESTION_ANSWER_LISTENING.ANSWERS_TO_CHOOSE answersToChoose,
                QUESTION_ANSWER_LISTENING.START_TIME startTime,
                QUESTION_ANSWER_LISTENING.DESCRIPTION description,
                QUESTION_ANSWER_LISTENING.ANSWER answer,
                QUESTION_ANSWER_LISTENING.SCORE score
                FROM QUESTION_ANSWER_LISTENING INNER JOIN CATEGORY
                ON CATEGORY.ID = QUESTION_ANSWER_LISTENING.PARENT_ID
                WHERE CATEGORY.ID = 462;
                --:categoryId;
                
select * from (select * from category order by DBMS_RANDOM.RANDOM) where parent_id = 562 and rownum <= 3  ;

                        select ID as id,
                        NAME as name,
                        ANSWER as answer,
                        PARENT_ID as parentId,
                        ANSWERS_TO_CHOOSE as answersToChoose ,
                        DESCRIPTION as description,
                        STATUS as status ,
                        SCORE as score ,
                        AP_PARAMNAME as ap_paramName 
                        from ( select Q_A_TRANSLATION_A_V.ID, 
                                Q_A_TRANSLATION_A_V.NAME, 
                                Q_A_TRANSLATION_A_V.ANSWER, 
                                Q_A_TRANSLATION_A_V.PARENT_ID ,
                                Q_A_TRANSLATION_A_V.ANSWERS_TO_CHOOSE ,
                                Q_A_TRANSLATION_A_V.DESCRIPTION ,
                                Q_A_TRANSLATION_A_V.STATUS ,
                                Q_A_TRANSLATION_A_V.SCORE ,
                                AP_PARAM.NAME as ap_paramName 
                                from Q_A_TRANSLATION_A_V 
                                left join AP_PARAM on Q_A_TRANSLATION_A_V.PARENT_ID = AP_PARAM.ID order by DBMS_RANDOM.RANDOM ) 
                                where rownum <= 3 ;
                                
                                
                                
select distinct TOPICS.ID topicId , 
                        TOPICS.NAME topicName 
                        from TOPICS left join CATEGORY on TOPICS.ID = CATEGORY.PARENT_ID  
                        inner join ap_param on category.type_code = ap_param.value 
                        where upper(TOPICS.PART_TOPIC_CODE) LIKE upper('PART3') escape '&' and CATEGORY.LEVEL_CODE = 'EASY' 
                        AND ap_param.value = '2';
                        
--?? quy
select id as paramId, name as paramName , code as paramCode , value as paramValue , parent_code as paramParentCode 
                        from ( select ap_param.id, ap_param.name , ap_param.code , ap_param.value , ap_param.parent_code from ap_param where type like 'TOPIC_TYPE' OR type like 'PART_TOPIC') t1 
                        UNION all
                        select a1.id as paramId, a1.name as paramName, a1.code as paramCode , a2.value as paramValue , 
                        a1.parent_code as paramParentCode from ap_param a1 left join ap_param a2 on a1.parent_code = a2.code 
                        where a1.value is null;
                        
-----------------
--query 1
select name as typeTestName, code as typeTestCode, value as typeTestValue from ap_param where ap_param.type='TOPIC_TYPE_TEST';

--query 2
select name as partName, code as partCode, value as partValue, parent_code as parentCode from ap_param where ap_param.parent_code='LISTENING_UNIT_TEST' or ap_param.parent_code='READING_UNIT_TEST' ;


-- for minitest
--get random topic by partTopicCode
                    select 
                        ID as topicId,
                        NAME as topicName ,
                        PART_TOPIC_CODE as partTopicCode 
                        from(
                            select
                            ID ,
                            NAME  ,
                            PART_TOPIC_CODE   
                            from TOPICS where PART_TOPIC_CODE = 'PART3'
                            order by DBMS_RANDOM.RANDOM
                        )
                        where rownum < = 1;
                        
--get category by parentId (topicId)
                    SELECT 
                        CATEGORY.ID categoryId, 
                        CATEGORY.NAME nameCategory, 
                        CATEGORY.PATH_FILE_1 pathFile1, 
                        CATEGORY.PATH_FILE_2 pathFile2, 
                        CATEGORY.PARENT_ID parentId 
                        from CATEGORY where CATEGORY.PARENT_ID = 438 and CATEGORY.TYPE_CODE = 1;
                        
                       -- select * from CATEGORY where CATEGORY.PARENT_ID= 438 and CATEGORY.TYPE_CODE = 1;

--get questionAnswerListening by parentId
                    SELECT 
                        QUESTION_ANSWER_LISTENING.ID id,
                        QUESTION_ANSWER_LISTENING.NAME name,
                        QUESTION_ANSWER_LISTENING.ANSWERS_TO_CHOOSE answersToChoose,
                        QUESTION_ANSWER_LISTENING.ANSWER answer,
                        QUESTION_ANSWER_LISTENING.START_TIME startTime, QUESTION_ANSWER_LISTENING.SCORE score, 
                        QUESTION_ANSWER_LISTENING.PART_FILE_1 pathFile1,
                        QUESTION_ANSWER_LISTENING.PART_FILE_2 pathFile2 
                        FROM QUESTION_ANSWER_LISTENING 
                        WHERE QUESTION_ANSWER_LISTENING.PARENT_ID = null;
                        
                        select * from QUESTION_ANSWER_LISTENING where ID= 120;

--get questionAnswerReading by parentId
                    SELECT 
                        QUESTION_ANSWER_READING.ID id,
                        QUESTION_ANSWER_READING.NAME name,
                        QUESTION_ANSWER_READING.ANSWERS_TO_CHOOSE answersToChoose,
                        QUESTION_ANSWER_READING.ANSWER answer,
                        QUESTION_ANSWER_READING.SCORE score
                        FROM QUESTION_ANSWER_READING 
                        WHERE QUESTION_ANSWER_READING.PARENT_ID = 664;
                        
--------------------------submit answer minitest--------------------------------
--get answerListeningById
                SELECT 
                        QUESTION_ANSWER_LISTENING.ID id,
                        QUESTION_ANSWER_LISTENING.ANSWER answer,
                        QUESTION_ANSWER_LISTENING.ANSWERS_TO_CHOOSE answersToChoose,
                        QUESTION_ANSWER_LISTENING.PARENT_ID parentId
                        FROM QUESTION_ANSWER_LISTENING 
                        WHERE QUESTION_ANSWER_LISTENING.ID = 403;
                        
--get answerReadingById
                SELECT 
                        QUESTION_ANSWER_READING.ID id,
                        QUESTION_ANSWER_READING.ANSWER answer,
                        QUESTION_ANSWER_READING.ANSWERS_TO_CHOOSE answersToChoose,
                        QUESTION_ANSWER_READING.SCORE score,
                        QUESTION_ANSWER_READING.DESCRIPTION description,
                        QUESTION_ANSWER_READING.PARENT_ID parentId
                        FROM QUESTION_ANSWER_READING 
                        WHERE QUESTION_ANSWER_READING.ID = 302;
----------------------------------------------------------------------------------
--get categoryById
                SELECT 
                        CATEGORY.ID categoryId, 
                        CATEGORY.PATH_FILE_1 pathFile1, 
                        CATEGORY.PATH_FILE_2 pathFile2
                        from CATEGORY where CATEGORY.ID = 1
                        

                        
                    


                        
                        
