package com.tungphongdo.service1.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController{
	
	@RequestMapping(value = "/service1", method = RequestMethod.GET)
	public String sayHello() {
		return "Hello from service 1";
	}

}
