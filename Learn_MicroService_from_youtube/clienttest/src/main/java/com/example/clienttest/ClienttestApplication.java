package com.example.clienttest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@EnableDiscoveryClient
@SpringBootApplication
public class ClienttestApplication {

	public static void main(String[] args) {
		SpringApplication.run(ClienttestApplication.class, args);
	}

}
