package com.example.clienttest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.netflix.appinfo.InstanceInfo;
import com.netflix.discovery.EurekaClient;

@RestController
public class ClientTestController {
	
	// Inject EurekaClient để gọi đến discovery server để lấy về địa chỉ của servicetest
	@Autowired
	private EurekaClient client;
	
	// Để gọi đến endpoint của servicetest, ta cần phải có 1 rest template
	@Autowired
	private RestTemplateBuilder templateBuilder;
	
	@RequestMapping("/")
	public String callService() {
	
		// Sử dụng EurekaClient gọi đến discovery server để lấy ra service cần lấy, thông qua tên của service được truyền vào
		// Ở đây ta truyền lên "servicetest" là tên của servicetest, 
		// Discovery server sẽ trả về cho chúng ta 1 instanse của servicetest
		InstanceInfo  instance = client.getNextServerFromEureka("servicetest", false); 
		
		// Từ instanse bên trên, ta sẽ lấy được base URL của servicetest
//		String url = instance.getHomePageUrl();
		String url = client.getNextServerFromEureka("servicetest", false).getHomePageUrl();
		
		// Build 1 rest template từ RestTemplateBuilder
		RestTemplate restTemplate = templateBuilder.build();
		
		// Sử dung restTemplate để gọi đến endpoint của servicetest
		/*
		 * ResponseEntity là kiểu String vì bên servicetest trả về kiểu String (method sayHello trả về kiểu String)
		 * url chính là url được lấy từ instanse bên trên
		 * HttpMethod = GET vì method sayHello là GetMapping
		 * null thì không cần care
		 * String.class ( lý do như ResponseEntity bên trên )
		*/
		ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.GET, null, String.class);
		
		// Sau đó chúng ta chỉ cần trả về cái response này
		return response.getBody();
	}
}
