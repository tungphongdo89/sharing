package com.tungphongdo.servicetest.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ServiceTestController {
	
	@GetMapping("/")
	public String sayHello() {
		return "greeting from Service test";
	}

}
