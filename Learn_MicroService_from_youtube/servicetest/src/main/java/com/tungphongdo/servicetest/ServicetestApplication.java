package com.tungphongdo.servicetest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@EnableDiscoveryClient
@SpringBootApplication
public class ServicetestApplication {

	public static void main(String[] args) {
		SpringApplication.run(ServicetestApplication.class, args);
	}

}
