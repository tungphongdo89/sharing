package com.tungphongdo.configclient.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

import com.tungphongdo.configclient.config.ClientConfig;

@RestController
@RequestScope
public class ConfigClientController {
	
	// Inject ConfigClient tạo ở package config
	@Autowired
	private ClientConfig config;
	
	// key sample.property2 sẽ tương ứng với key sample.property2 trên github
	@Value("${sample.property2}")
	private String property2;
	
	//Tạo 1 endpoint để trả về thông tin cấu hình
	@GetMapping("/config")
	public String printConfigInfo() {
		
		// Trả về giá trị của property1 thông qua ClientConfig này
		return config.getProperty1() + " - " + property2;
	}

}

