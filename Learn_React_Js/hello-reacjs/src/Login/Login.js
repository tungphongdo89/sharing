import React from 'react';
import styles from '../App.module.css';

import {connect} from "react-redux";
import {bindActionCreators, compose} from 'redux';
import studentAction from '../Redux/actions/studentAction';

import { Redirect } from 'react-router';

import {
	  Card,
	  CardHeader,
	  CardTitle,
	  CardBody,
	  Button,
	  Label,
	  FormGroup
	} from "reactstrap"
import { Formik, Field, Form, ErrorMessage } from "formik";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Input from "reactstrap/es/Input";

import LoginSchema  from '../Validation/ValidationLogin';

class Login extends React.Component{
	render(){
		debugger
		const token = localStorage.token;
		
		return(
			<div className="container-fluid" id={styles.background} >
				<div className={styles.loginFormFather}>
					<div className={styles.loginFormChild}>
						<Card>
				          	<CardBody>
				          		<Formik
				          			initialValues={{
				          				username: "",
				          				password: ""
				          			}}
				          			
				          			onSubmit={values=>{
				          				debugger
				          				setTimeout(()=>{
				          					const {studentAction} = this.props;
				          					const {login} = studentAction;
				          					let account  = {};
				          					account.username = values.username;
				          					account.password = values.password;
				          					login(account)
				          				}, 1000)
				          			}}
				          		
				          			validationSchema={LoginSchema}
				          		
				          			render={({ errors, touched })=>(
				        
				          				<Form>
				          					<p className={styles.title}>ĐĂNG NHẬP</p>
				          					<FormGroup>
				          						<label htmlFor="name">Tên đăng nhập</label>
				          						<Field 
				          							className = "form-control"
				          							name = "username"
				          							placeholder="tên đăng nhập"
				          						    
				          						/>
				          						<ErrorMessage
				          							name = "username"
				          							component = "span"
				          							className = "field-error text-danger"
				          						/>
				          					</FormGroup>
				          					<FormGroup>
				          						<label htmlFor="id">Mật khẩu</label>
				          						<Field 
				          							type="password"
				          							className = "form-control"
				          							name = "password"
				          							placeholder="mật khẩu"
				          						/>
				          						<ErrorMessage
					          						name = "password"
					          						component = "span"
					          						className = "field-error text-danger"
					          					/>
				          					</FormGroup>
				          					<div style={{textAlign: "center"}}>
					          					<button className="btn btn-danger" type="submit" style={{marginLeft: '2px'}}>
					          	                  OK
					          	                </button>
					          	                
				          	                </div>
				          				</Form>
				          			)}
				          		/>
				          		<ToastContainer />
				          		{token ? (
				          				(<Redirect to="/" />)
				          			) : null
				          		}
				          	</CardBody>
					    </Card>
					</div>
				</div>
			</div>
		);
	}
}

const mapStateToProps = state => {
  return {
	  values: state.studentReducer.respLogin
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
	  studentAction: bindActionCreators(studentAction, dispatch)
  }

}
export default compose(connect(mapStateToProps, mapDispatchToProps))(Login)
