import React from 'react';
import {Link} from 'react-router-dom';

import {connect} from "react-redux";
import {bindActionCreators, compose} from 'redux';
import studentAction from '../Redux/actions/studentAction';

import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import { Redirect } from 'react-router';

class Home extends React.Component{
	
	doLogout =()=>{
		debugger
		localStorage.removeItem("token");
		window.location.replace("/login");
	}
	
	render(){
		debugger
		const token = localStorage.token;
		const resp = this.props.values.resp;
		return(
			<div>
				<h3>Welcome to Home</h3>
				<Link to="/pages/about">About | </Link>
				{
					token ? (<span>
						        <a href="/pages/admin">Admin |</a>
								<Link to="/pages/admin">Admin chuẩn | </Link>
								<button className="btn btn-success"
									onClick={()=>this.doLogout()}
								>Đăng xuất</button>
							</span>) 
					: (
						<>
							<a href="/pages/login">Login bằng thẻ a |</a>
							<Link to="/pages/login"> Login bằng Link  </Link>

						</>
						)
				}
				{resp ? toast.success(resp.message) : null}
				<ToastContainer />
			</div>
		);
	}
}

const mapStateToProps = state => {
  return {
	  values: state.studentReducer.respLogin
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
	  studentAction: bindActionCreators(studentAction, dispatch)
  }

}
export default compose(connect(mapStateToProps, mapDispatchToProps))(Home)
