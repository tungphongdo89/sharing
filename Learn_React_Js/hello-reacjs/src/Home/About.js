import React from 'react';
import {Link} from 'react-router-dom'

class About extends React.Component{
	render(){
		return(
			<div>
				<h3>Welcome to About</h3>
				<Link to="/">Home | </Link>
				<Link to="/pages/admin">Admin</Link>
			</div>
		);
	}
}

export default About