import React from 'react';
import { Route, Switch } from 'react-router-dom';
import Home from './Home/Home';
import Admin from './Admin/Admin';
import About from './Home/About';
import Login from './Login/Login';

class App extends React.Component{
	
	render(){
		return(
			<div>
				<Switch>
		            <Route path="/" component={Home} exact />
		            {/*<Route path="/pages/admin" component={Admin} exact />*/}
		            {/*<Route path="/pages/about" component={About} exact />*/}
		            <Route path="/pages/login" component={Login} exact />
		        </Switch>
			</div>
		);
	}
}

export default App;
