import React from 'react';
import styles from '../App.module.css';
import AddStudent from '../StudentModal/AddStudentModel';
import AddStudent_formik from '../StudentModal/AddStudentModel_formik';
import UpdateStudent from '../StudentModal/UpdateStudentModel';
import UpdateStudent_formik from '../StudentModal/UpdateStudentModel_formik';
import DeleteStudent from '../StudentModal/DeleteStudentModel';
import DetailStudent from '../StudentModal/DetailStudentModel';

import {connect} from "react-redux";
import {bindActionCreators, compose} from 'redux';
import studentAction from '../Redux/actions/studentAction';

import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import PaginationStudent from '../Pagination/Pagination';
import { Button, Spinner } from 'reactstrap';

import { Redirect } from 'react-router';


class Admin extends React.Component{
	constructor(props){
		super(props);
		this.state = {
			listStudents: [],
			isLoading: false,
			studentInfo: {}
		}
	}
	
//	 componentDidMount() {
//		debugger
//	    this.setState({isLoading: true});
//	    fetch('api/students')
//	      .then(response => response.json())
//	      .then(body => 
//	      	setTimeout(()=>{
//	      		this.setState({students: body.data, isLoading: false})
//	      	}, 500)
//	      );
//		const {apiListStudents} = studentApis;
//		let resp = {};
//		resp = apiListStudents();
//		setTimeout(()=>{
//			this.setState({ 
//				students: resp.data,
//				isLoading: false
//			});
//		}, 1000)
//		this.setState({
//			isLoading: true
//		})
//		setTimeout(()=>{
//			this.setState({
//				isLoading: false
//			})
//		}, 1000)
//	}
	 
	 componentDidMount(){
		 debugger
		 const {studentAction} = this.props;
		 const {fetchStudents} = studentAction;
		 let data = {};
		 data.page = 0;
		 data.size = 3;
		 data.start = 0;
		 if(localStorage.token){
			 fetchStudents(data);
		 }
		 
	 }
	
	getStudentInfo =(a)=> {
		this.setState({
			studentInfo: a
		})
	}
	
	render(){
		debugger
		
		const {students} = this.props;
		const {resp} = students;

//		if (isLoading) {
//		   return <p>Loading...</p>;
//		}
		const token = localStorage.token;
//		return (
//			<div>
//				{token ? <Redirect to="/login" /> : null}
//			</div>
//		);
		
		return(
			<div>
				{token ? (
					<div className="container">
						
						<h2 className={styles.textHeader}>Quản lý sinh viên </h2><br/>
							<a type="button" className="btn btn-primary" href="/">Trang chủ</a>
							<a type="button" className="btn btn-primary" href="/pages/about">Giới thiệu</a>
							<button type="button" className="btn btn-primary" data-toggle="modal" data-target="#AddStudentModal">
								Thêm
							</button>
							<button type="button" className="btn btn-primary" data-toggle="modal" data-target="#AddStudentModalFormik">
								Thêm(formik)
							</button>
							<table className="table table-striped" id={styles.table}>
							  <thead>
							    <tr>
							      <th className={styles.th}>ID</th>
							      <th className={styles.th}>HỌ TÊN</th>
							      <th className={styles.th}>ĐỊA CHỈ</th>
							      <th className={styles.th}>THAO TÁC</th>
							    </tr>
							  </thead>
							  
							  {resp ? resp.data.content.map( (student)=>
							  	<tbody>
							     <tr>
								      <td key="studentId">{student.id}</td>
								      <td key="studentName">{student.name}</td>
								      <td key="studentAddress">{student.address}</td>
								      <td>
								        <button className="btn btn-info" 
								        	data-toggle="modal" data-target="#DetailStudentModal"
									      	onClick={(e) => this.getStudentInfo(student)}
								        >Xem
								        </button>
								      	<button className="btn btn-success" style={{marginLeft: '5px'}}
								      		data-toggle="modal" data-target="#UpdateStudentModal"
								      		onClick={(e) => this.getStudentInfo(student)}
								      	> Sửa
										</button>
								      	
								      	<button className="btn btn-success" style={{marginLeft: '5px'}}
								      		data-toggle="modal" data-target="#UpdateStudentModal2"
								      		onClick={(e) => this.getStudentInfo(student)}
								      	> Sửa (formik)
										</button>
								      	
								      	<button className="btn btn-danger" style={{marginLeft: '5px'}}
								      		data-toggle="modal" data-target="#DeleteStudentModal"
								      		onClick={(e) => this.getStudentInfo(student)}
								      	> Xóa
								      	</button>
								      </td>
							     </tr>
							    </tbody>
							  	) : 
							  	(<div style={{textAlign: 'right'}}>
								  	<Button color="primary">
									    <Spinner
									      as="span"
									      animation="border"
									      size="sm"
									      role="status"
									      aria-hidden="true"
									    />
									    <span>Loading...</span>
								    </Button>
							  	</div>)
							  }
							  
							</table>
							
							<AddStudent />
							<UpdateStudent studentInfo={this.state.studentInfo}/>
							<AddStudent_formik />
							<UpdateStudent_formik studentInfo={this.state.studentInfo}/>
							<DeleteStudent studentInfo={this.state.studentInfo}/> 
							<DetailStudent studentInfo={this.state.studentInfo}/>
							<ToastContainer />
							<PaginationStudent listStudents = {this.props.students}/>
					</div>
					): <Redirect to="/pages/login" />
				}
			</div>
		);
	}
}
const mapStateToProps = state => {
  return {
	  students: state.studentReducer.students,
	  values: state.studentReducer
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
	  studentAction: bindActionCreators(studentAction, dispatch)
  }

}

export default compose(connect(mapStateToProps, mapDispatchToProps))(Admin)
