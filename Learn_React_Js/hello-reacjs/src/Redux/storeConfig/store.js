import {applyMiddleware, compose, createStore} from "redux"
//import createDebounce from "redux-debounced"
//import thunk from "redux-thunk"
import combineReducers from "../reducers/rootReducer"
import rootSaga from "../sagas/saga";
import createSagaMiddleware from 'redux-saga'
//import {persistReducer, persistStore} from 'redux-persist';
//import storage from 'redux-persist/lib/storage';

const sagaMiddleware = createSagaMiddleware();

const middlewares = [ sagaMiddleware];

const persistConfig = {
  key: 'root'
//  storage: storage
};

//const pReducer = persistReducer(persistConfig, combineReducers);
const pReducer = combineReducers


const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose
const store = createStore(
  pReducer,
  {},
  composeEnhancers(applyMiddleware(...middlewares))
)

export default store;
//export const persistor = persistStore(store);
sagaMiddleware.run(rootSaga)
