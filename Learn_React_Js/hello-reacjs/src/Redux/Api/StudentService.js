import React from 'react';

const studentApis = {
	apiLogin,
	apiListStudents,
	apiAddStudent,
	apiUpdateStudent,
	apiDeleteStudent
	
}
async function apiLogin(data){
	debugger
	return await fetch(
		'/api/login',
		{
			method: 'POST',
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json'
			},
			body: JSON.stringify(data.data)
		}
	)
	.then(response => response.json())
	.then(body =>  body)
//	.then(body =>  {localStorage.setItem("token", body.token)})
	.catch((error) => { throw error }) 
}

async function apiListStudents(data){
	debugger
	return await fetch(
		'/api/students',
		{
			method: 'POST',
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json',
				'Authorization': localStorage.token
			},
			body: JSON.stringify(data.data)
		}
	)
	.then(response => response.json())
	.then(body => body)
	.catch((error) => { throw error }) 
}

async function apiAddStudent (student){
	return await fetch(
		'/api/create', 
		{
			method: 'POST',
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json'
			},
			body: JSON.stringify(student.data)
		}
	)
	.then(response => response.json())
	.then(body => body)
    .catch((error) => { throw error })
}

async function apiUpdateStudent(student){
	return await fetch(
		'/api/update', 
		{
			method: 'PUT',
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json'
		    },
			body: JSON.stringify(student.data)
		}
	)
	.then(response => response.json())
	.then(body => body)
    .catch((error) => { throw error })
	
}

async function apiDeleteStudent(student){
	return await fetch(
		'/api/delete', 
		{
			method: 'DELETE',
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json'
			},
			body: JSON.stringify(student.data)
		}
	)
	.then(response => response.json())
	.then(body => body)
    .catch((error) => { throw error })
}


export default studentApis