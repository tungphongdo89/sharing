import * as config from '../const/const';

const studentAction = {
		
	login,
	loginSuccess,
	loginFailed,
		
	fetchStudents,
	fetchStudentsSuccess,
	fetchStudentsFailed,

	addStudent,
	addStudentSuccess,
	addStudentFailed,
	
	updateStudent,
	updateStudentSuccess,
	updateStudentFailed,
	
	deleteStudent,
	deleteStudentSuccess,
	deleteStudentFailed
}

//Login
function login(data){
	debugger
	return{
		type: config.LOGIN,
		payload: {data}
	}
}

function loginSuccess(resp){
	debugger
	return{
		type: config.LOGIN_SUCCESS,
		payload: {resp}
	}
}

function loginFailed(resp){
	debugger
	return{
		type: config.LOGIN_FAILED,
		payload: {resp}
	}
}


//Get all students
function fetchStudents(data){
	debugger
	return{
		type: config.FETCH_STUDENTS,
		payload: {data}
	}
	
}

function fetchStudentsSuccess(resp){
	debugger
	return{
		type: config.FETCH_STUDENTS_SUCCESS,
		payload: {resp}
	}
}

function fetchStudentsFailed(resp){
	debugger
	return{
		type: config.FETCH_STUDENTS_FAILED,
		payload: {resp}
	}
}

//Add student
function addStudent(data){
	return {
		type: config.ADD_STUDENT,
		payload: {data}
	}
}

function addStudentSuccess(resp){
	return {
		type: config.ADD_STUDENT_SUCCESS,
		payload: {resp}
	}
}

function addStudentFailed(resp){
	return {
		type: config.ADD_STUDENT_FAILED,
		payload: {resp}
	}
}

//Update student
function updateStudent(data){
	return {
		type: config.UPDATE_STUDENT,
		payload: {data}
	}
}

function updateStudentSuccess(resp){
	return {
		type: config.UPDATE_STUDENT_SUCCESS,
		payload: {resp}
	}
}

function updateStudentFailed(resp){
	return {
		type: config.UPDATE_STUDENT_FAILED,
		payload: {resp}
	}
}

//Delete student
function deleteStudent(data){
	debugger
	return {
		type: config.DELETE_STUDENT,
		payload: {data}
	}
}

function deleteStudentSuccess(resp){
	debugger
	return {
		type: config.DELETE_STUDENT_SUCCESS, 
		payload: {resp}
	}
}

function deleteStudentFailed(resp){
	debugger
	return {
		type: config.DELETE_STUDENT_FAILED,
		payload: {resp}
	}
}


export default studentAction