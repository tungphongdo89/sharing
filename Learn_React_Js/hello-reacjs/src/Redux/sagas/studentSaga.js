import {call, delay, put, takeEvery} from "redux-saga/effects";
import * as config from '../const/const';
import studentAction from '../actions/studentAction';
import studentApis from '../Api/StudentService'

function* login({payload}){
	debugger
	const data = payload;
	const resp = yield call(studentApis.apiLogin, data);
	if("SUCCESS" === resp.status){
		yield put(studentAction.loginSuccess(resp))
	}
	else{
		yield put(studentAction.loginFailed(resp))
	}
}

function* fetchStudents({payload}){
	const data = payload;
	const resp = yield call(studentApis.apiListStudents, data);
	if("SUCCESS" === resp.status){
		yield put(studentAction.fetchStudentsSuccess(resp))
	}
	else{
		yield put(studentAction.fetchStudentsFailed(resp))
	}
}

function* addStudent({payload}){
	const data = payload;
	const resp = yield call(studentApis.apiAddStudent, data);
	if("SUCCESS" === resp.status){
		yield put(studentAction.addStudentSuccess(resp))
		yield put(studentAction.fetchStudents({}))
	}
	else{
		yield put(studentAction.addStudentFailed(resp))
	}
}

function* updateStudent({payload}){
	const data = payload;
	const resp = yield call(studentApis.apiUpdateStudent, data);
	if("SUCCESS" === resp.status){
		yield put(studentAction.updateStudentSuccess(resp))
		yield put(studentAction.fetchStudents())
	}
	else{
		yield put(studentAction.updateStudentFailed(resp))
	}
}

function* deleteStudent({payload}){
	const data = payload;
	const resp = yield call(studentApis.apiDeleteStudent, data);
	if("SUCCESS" === resp.status){
		yield put(studentAction.deleteStudentSuccess(resp))
		yield put(studentAction.fetchStudents())
	}
	else{
		yield put(studentAction.deleteStudentFailed(resp))
	}
}


function* studentSaga(){
	yield takeEvery(config.FETCH_STUDENTS, fetchStudents)
	yield takeEvery(config.ADD_STUDENT, addStudent)
	yield takeEvery(config.UPDATE_STUDENT, updateStudent)
	yield takeEvery(config.DELETE_STUDENT, deleteStudent)
	yield takeEvery(config.LOGIN, login)
}

export default studentSaga