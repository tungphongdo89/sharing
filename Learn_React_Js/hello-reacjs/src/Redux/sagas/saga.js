import {fork} from "redux-saga/effects";
import studentSaga from './studentSaga';

function* rootSaga(){
	yield fork(studentSaga)
}

export default rootSaga

