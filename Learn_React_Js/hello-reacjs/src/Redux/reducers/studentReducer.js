import * as config from '../const/const';

import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import { Redirect } from 'react-router';

const initState = {
	students: {},
	respLogin: {},
	message: ""
	
}

const studentReducer = (state = initState, action) =>{
	switch(action.type){
		//login
		case config.LOGIN: {
			debugger
			const data = action.payload;
			state = {...state};
		    return state;
		}
		
		case config.LOGIN_SUCCESS: {
			debugger
			const resp = action.payload;
			localStorage.setItem("token", action.payload.resp.token);
			state = {...state, respLogin: resp, message: action.payload.resp.message};
//			toast.success("Đăng nhập thành công");
		    return state;
		}
		
		case config.LOGIN_FAILED: {
			debugger
			const resp = action.payload;
			state = {...state, respLogin: resp};
			toast.error("Đăng nhập thất bại - " + action.payload.resp.message)
		    return state;
		}
	
		//get list student
		case config.FETCH_STUDENTS: {
			debugger
			const data = action.payload;
			state = {...state};
		    return state;
		}
		case config.FETCH_STUDENTS_SUCCESS: {
			debugger
			const resp = action.payload;
			state = {...state,students: resp};
			return state;
		}
		case config.FETCH_STUDENTS_FAILED: {
			debugger
			const resp = action.payload;
			state = {...state, dataResp: resp}
			return state;
		}
		
		//add student
		
		case config.ADD_STUDENT: {
			const data = action.payload;
			state = {...state};
			return state;
		}
		case config.ADD_STUDENT_SUCCESS: {
			const resp = action.payload;
			state = {...state, resp: resp};
			toast.success("Thêm thành công");
//			window.location.reload(false);
			return state;
		}
		case config.ADD_STUDENT_FAILED: {
			const resp = action.payload;
			state = {...state, resp: resp};
			toast.error("Thêm thất bại - " + action.payload.resp.message);
			return state;
		}
		
		//update student
		case config.UPDATE_STUDENT: {
			const data = action.payload;
			state = {...state};
			return state;
		} 
		
		case config.UPDATE_STUDENT_SUCCESS: {
			const resp = action.payload;
			state = {...state, resp: resp};
			toast.success("Cập nhật thành công");
			return state;
		}
		
		case config.UPDATE_STUDENT_FAILED: {
			const resp = action.payload;
			state = {...state, resp: resp};
			toast.error("Cập nhật thất bại - " + action.payload.resp.messages);
			return state;
		}
		
		//delete Student
		case config.DELETE_STUDENT: {
			const data = action.payload;
			state = {...state};
			return state;
		}
		
		case config.DELETE_STUDENT_SUCCESS: {
			const resp = action.payload;
			state = {...state, resp: resp};
			setTimeout(()=>{
				toast.success("Xóa sinh viên thành công")
			}, 0)
			return state;
		}
		
		case config.DELETE_STUDENT_FAILED: {
			const resp = action.payload;
			state = {...state, resp: resp};
			toast.error("Xóa sinh viên thất bại - " + action.payload.resp.message)
			return state;
		}
		
		default:
		    return state;
	}
}

export default studentReducer