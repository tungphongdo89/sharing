import React from 'react';
import { Pagination, PaginationItem, PaginationLink } from 'reactstrap';

import {connect} from "react-redux";
import {bindActionCreators, compose} from 'redux';
import studentAction from '../Redux/actions/studentAction';

class PaginationStudent extends React.Component{
	constructor(props){
		super(props);
	}
	
	generationPageNumbers =(listPages, number)=>{
		debugger
		let pagingPage = [];
		listPages.forEach((element) =>
			pagingPage.push(
					<PaginationItem active={ element===number+1 ? true : false}>
				        <PaginationLink onClick={(e)=> this.changePage(element-1, e)} href="#">
				        	{element}
				        </PaginationLink>
				    </PaginationItem>
				)
		);
//		for(let i=1; i<=5; i++){
//			pagingPage.push(
//				<PaginationItem active={ i===number+1 ? true : false}>
//			        <PaginationLink onClick={(e)=> this.changePage(i-1, e)} href="/admin">
//			        	{i}
//			        </PaginationLink>
//			    </PaginationItem>
//			)
//			console.log("value", i.value);
//		}
		
		return pagingPage;
	}
	
	firstPage =(e)=>{
		e.preventDefault();
		this.changePage(0, e);
	}
	
	previousPage =(number, e)=>{
		e.preventDefault();
		if(number >= 1){
			this.changePage(number-1, e);
		}
		
	}
	
	nextPage =(number, totalPages, e)=>{
		e.preventDefault();
		if(number < totalPages - 1){
			this.changePage(number+1, e);
		}
	}
	
	lastPage =(totalPages, e)=> {
		e.preventDefault();
		this.changePage(totalPages-1, e);
	}
	
	changePage =(number, e)=>{
		debugger
		e.preventDefault();
		const {studentAction} = this.props;
		const {fetchStudents} = studentAction;
		let data = {};
		data.page = number;
		data.size = 3;
		data.start = number;
		fetchStudents(data);
	}
	
	render(){
	  debugger
	  const resp = this.props.listStudents.resp;
//	  const listPages = this.props.listStudents.resp.listPages;
	
	  return (
	    <Pagination aria-label="Page navigation example">
	      {resp ? (
	    	  <PaginationItem disabled={0 === resp.data.number? true : false}>
	  	        <PaginationLink first href="#" 
	  	        	onClick={(e) => this.firstPage(e)}
	  	        >Đầu
	  	        </PaginationLink>
	  	      </PaginationItem>
	          ): null
	      }
	      
	      {resp ? (
		      <PaginationItem disabled={0 === resp.data.number? true : false}>
			  	<PaginationLink  href="#"
				   onClick={(e) => this.previousPage(resp.data.number, e)}
				>Trước
				</PaginationLink>
		      </PaginationItem>
		      ): null
		  }
	      
	      {resp ? (
	    		  	this.generationPageNumbers(resp.listPages, resp.data.number)
		          ) : null
	      }
	      
	      {resp ? (
		      <PaginationItem disabled={resp.data.totalPages-1 === resp.data.number? true : false}>
			  	<PaginationLink href="#"
			  		onClick={(e) => this.nextPage(resp.data.number, resp.data.totalPages, e)}
				>Sau
				</PaginationLink>
		      </PaginationItem>
	      	  ): null
	      }
	      
	      {resp ? (
		      <PaginationItem disabled={resp.data.totalPages-1 === resp.data.number? true : false}>
		  	    <PaginationLink first href="#" 
		  	        onClick={(e) => this.lastPage(resp.data.totalPages, e)}
		  	    >Cuối
		  	    </PaginationLink>
		  	  </PaginationItem>
		      ): null
		  }
	        
	    </Pagination>
	  )
	}
}

const mapStateToProps = state => {
  return {
	  values: state.studentReducer
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
	  studentAction: bindActionCreators(studentAction, dispatch)
  }

}

export default compose(connect(mapStateToProps, mapDispatchToProps))(PaginationStudent)
