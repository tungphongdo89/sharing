import React from 'react';
import styles from '../App.module.css';

import {connect} from "react-redux";
import {bindActionCreators, compose} from 'redux';
import studentAction from '../Redux/actions/studentAction';

import {
	  Card,
	  CardHeader,
	  CardTitle,
	  CardBody,
	  Button,
	  FormGroup
	} from "reactstrap"
import { Formik, Field, Form, ErrorMessage } from "formik";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import * as Yup from 'yup';

import AddSchema  from '../Validation/validationModelForm'

const UpdateSchema = Yup.object().shape({
   name: Yup.string()
    .min(5, 'Tên phải dài hơn 5 ký tự!')
    .max(50, 'Tên không được dài hơn 50 ký tự!')
    .required('Không được để trống'),
   address: Yup.string()
    .min(5, 'địa chỉ phải dài hơn 5 ký tự!')
    .max(100, 'địa chỉ không được quá 100 ký tự')
    .required('Không được để trống')
});

class UpdateStudent_formik extends React.Component{
	constructor(props){
		super(props);
		this.state = {
		}
	}
	
	render(){
		return(
			<div>
				<div className="modal fade" id="UpdateStudentModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
				  <div className="modal-dialog" role="document">
				    <div className="modal-content">
				      <div className="modal-header">
				        <h5 className="modal-title" id="exampleModalLabel"><h3>Sửa thông tin sinh viên</h3></h5>
				        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
				          <span aria-hidden="true">&times;</span>
				        </button>
				      </div>
				      <div className="modal-body">
					    
						<Formik
		          			initialValues={{
		          				id: this.props.studentInfo.id,
		          				name: this.props.studentInfo.name,
		          				address: this.props.studentInfo.address
		          			}}
		          		
		          			validationSchema={UpdateSchema}
		          			
		          			onSubmit={values=>{
		          				setTimeout(()=>{
		          					const {studentAction} = this.props;
		          					const {updateStudent} = studentAction;
		          					let student  = {};
		          					student.id = values.id;
		          					student.name = values.name;
		          					student.address = values.address;
		          					updateStudent(student)
		          				}, 1000)
		          			}}
						>
					    
						{({errors, touched}) => (
							<Form>
	          					<FormGroup>
	          						<label htmlFor="name">Tên sinh viên</label>
	          						<Field 
	          							name = "name"
	          							className = "form-control"
	          						/>
	          						<ErrorMessage
	          							name = "name"
	          							component = "span"
	          							className = "field-error text-danger"
	          						/>
	          						
	          					</FormGroup>
	          					<FormGroup>
	          						<label htmlFor="address">Địa chỉ</label>
	          						<Field 
	          							name = "address"
	          							className = "form-control"
	          						>
	          						</Field>
	          						<ErrorMessage
		          						name = "address"
		          						component = "span"
		          						className = "field-error text-danger"
		          					/>
	          					</FormGroup>
	          					<Button color="success" type="submit">
	          	                  Sửa
	          	                </Button>
	          				</Form>
						)}
						</Formik>
						
				      </div>
				    </div>
				  </div>
				</div>
			</div>
		);
	}
}

const mapStateToProps = state => {
  return {
	  values: state.studentReducer
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
	  studentAction: bindActionCreators(studentAction, dispatch)
  }
}

export default compose(connect(mapStateToProps, mapDispatchToProps))(UpdateStudent_formik)
