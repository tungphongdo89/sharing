import React from 'react';
import styles from '../App.module.css';

import {connect} from "react-redux";
import {bindActionCreators, compose} from 'redux';
import studentAction from '../Redux/actions/studentAction';

import {
	  Card,
	  CardHeader,
	  CardTitle,
	  CardBody,
	  Button,
	  FormGroup
	} from "reactstrap"
import { Formik, Field, Form, ErrorMessage } from "formik";

import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import * as Yup from 'yup';

import AddSchema  from '../Validation/validationModelForm'

//formik
//const SignupSchema = Yup.object().shape({
//	name: Yup.string()
//     .min(5, 'Tên phải dài hơn 5 ký tự!')
//     .max(50, 'Tên không được dài hơn 50 ký tự!')
//     .required('Không được để trống'),
//    address: Yup.string()
//     .min(5, 'địa chỉ phải dài hơn 5 ký tự!')
//     .max(100, 'địa chỉ không được quá 100 ký tự')
//     .required('Không được để trống')
//});

class AddStudent extends React.Component{
	
	constructor(props){
		super(props);
		this.state = {
			name: "",
			address: "",
			errName: "",
			errAddress: ""
		}
	}
	
	changeInput = (e) =>{
		let text = e.target.name;
		let val = e.target.value;
		this.setState({
			[text]: val
			
		})
		if(text==="name" && val===""){
			this.setState({
				errName: "Tên sinh viên không được để trống"
			})
		}
		else{
			this.setState({
				errName: ""
			})
		}
		if(text==="address" && val===""){
			this.setState({
				errAddress: "Địa chỉ không được để trống"
			})
		}
		else{
			this.setState({
				errAddress: ""
			})
		}
		
	}
	
	actionAddStudent (a) {
		const {studentAction} = this.props;
		const {addStudent} = studentAction;
		let err = "Không được bỏ trống";
		const student = {
				name: a.name,
				address: a.address
			  }
		if(a.name !== "" && a.address !== ""){
			addStudent(student);
		}
		else {
			if(a.name === ""){
				this.setState({
					errName: "Tên sinh viên không được để trống"
				})
			}
			if(a.address === ""){
				this.setState({
					errAddress: "Địa chỉ không được để trống"
				})
			}
		}
	}
	
	
	render(){
		return(
			<div>
				<div className="modal fade" id="AddStudentModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
				  <div className="modal-dialog" role="document">
				    <div className="modal-content">
				      <div className="modal-header">
				        <h5 className="modal-title" id="exampleModalLabel"><h3>Thêm sinh viên</h3></h5>
				        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
				          <span aria-hidden="true">&times;</span>
				        </button>
				      </div>
				      <div className="modal-body">
			
				      	<form>
					      <div className="form-group">
					        <label htmlFor="text">Nhập tên sinh viên:</label>
					        <input type="text" className="form-control" name="name" 
					        	onChange={this.changeInput}
					        />
					        <p style={{color: "red"}}><i><strong> {this.state.errName} </strong></i></p>
					      </div>
					      <div className="form-group">
					        <label htmlFor="text">Nhập địa chỉ:</label>
					        <input type="text" className="form-control" name="address" 
					        	onChange={this.changeInput}
					        />
					        <p style={{color: "red"}}><i><strong> {this.state.errAddress} </strong></i></p>
					      </div>
					      <div className="modal-footer">
					        <button type="button" className="btn btn-secondary" data-dismiss="modal">Đóng</button>
					        <button type="button" className="btn btn-primary" 
					        	onClick={this.actionAddStudent.bind(this, this.state)}> Thêm
					        </button>
					      </div>
					    </form>
					    
					    <h3>Formik add student</h3>
					    
					    <Card>
					    	<CardHeader>
					    		<CardTitle>Basic</CardTitle>
				          	</CardHeader>
				          	<CardBody>
				          		<Formik
				          			initialValues={{
				          				name: "",
				          				address: ""
				          			}}
				          		
				          			validationSchema={AddSchema}
				          			
				          			onSubmit={values=>{
				          				setTimeout(()=>{
				          					const {studentAction} = this.props;
				          					const {addStudent} = studentAction;
				          					let student  = {};
				          					student.name = values.name;
				          					student.address = values.address;
				          					addStudent(student)
//				          					toast.success(JSON.stringify(values))
				          				}, 1000)
				          			}}
				          		
				          			render={({ errors, touched })=>(
				          				<Form>
				          					<FormGroup>
				          						<label htmlFor="id">Tên sinh viên</label>
				          						<Field 
				          						
				          							className = "form-control"
				          							name = "name"
				          							placeholder="tên sinh viên"
				          						/>
				          						<ErrorMessage
				          							name = "name"
				          							component = "span"
				          							className = "field-error text-danger"
				          						/>
				          					</FormGroup>
				          					<FormGroup>
				          						<label htmlFor="id">Địa chỉ</label>
				          						<Field 
				          							className = "form-control"
				          							name = "address"
				          							placeholder="địa chỉ"
				          						/>
				          						<ErrorMessage
					          						name = "address"
					          						component = "span"
					          						className = "field-error text-danger"
					          					/>
				          					</FormGroup>
				          					<Button color="primary" type="submit">
				          	                  Thêm
				          	                </Button>
				          				</Form>
				          			)}
				          		/>
				          		<ToastContainer />
				          	</CardBody>
					    </Card>
					    
				      </div>
				    </div>
				  </div>
				</div>
			</div>
		);
	}
}

const mapStateToProps = state => {
  return {
	  values: state.studentReducer
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
	  studentAction: bindActionCreators(studentAction, dispatch)
  }

}

export default compose(connect(mapStateToProps, mapDispatchToProps))(AddStudent)