import React from 'react';
import styles from '../App.module.css';

class DetailStudent extends React.Component{
	
	render(){
		return(
			<div>
				<div className="modal fade" id="DetailStudentModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
				  <div className="modal-dialog" role="document">
				    <div className="modal-content">
				      <div className="modal-header">
				        <h5 className="modal-title" id="exampleModalLabel"><h3>Sửa thông tin sinh viên</h3></h5>
				        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
				          <span aria-hidden="true">&times;</span>
				        </button>
				      </div>
				      <div className="modal-body">
				      
				      	<form>
				      	  <div className="form-group">
					        <label htmlFor="text">ID sinh viên:</label>
					        <input type="text" className="form-control" name="id" 
					        	value = {this.props.studentInfo.id} 
					        >
					        </input>
					      </div>
					      <div className="form-group">
					        <label htmlFor="text">Tên sinh viên:</label>
					        <input type="text" className="form-control" name="name" 
					        	value = {this.props.studentInfo.name} 
					        >
					        </input>
					      </div>
					      <div className="form-group">
					        <label htmlFor="text">Địa chỉ:</label>
					        <input type="text" className="form-control" name="address" 
					        	value = {this.props.studentInfo.address}
					        />
					      </div>
					      <div className="modal-footer">
					        <button type="button" className="btn btn-secondary" data-dismiss="modal">Đóng</button>
					      </div>
					    </form>
					    
				      </div>
				    </div>
				  </div>
				</div>
			</div>
		);
	}
}

export default DetailStudent;