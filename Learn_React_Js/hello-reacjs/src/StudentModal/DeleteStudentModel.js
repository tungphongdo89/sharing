import React from 'react';
import styles from '../App.module.css';

import {connect} from "react-redux";
import {bindActionCreators, compose} from 'redux';
import studentAction from '../Redux/actions/studentAction';

class DeleteStudent extends React.Component{

	actionDeleteStudent (){
		let student = {
			id: this.props.studentInfo.id
		}
		const {studentAction} = this.props;
		const {deleteStudent} = studentAction; 
		deleteStudent(student);
		
	}
	
	render(){
		
		return(
			<div>
				<div className="modal fade" id="DeleteStudentModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
				  <div className="modal-dialog" role="document">
				    <div className="modal-content">
				      <div className="modal-header">
				        <h5 className="modal-title" id="exampleModalLabel"><h3>Xóa thông tin sinh viên</h3></h5>
				        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
				          <span aria-hidden="true">&times;</span>
				        </button>
				      </div>
				      <div className="modal-body">
				      	  <h3 style={{textAlign: 'center'}}>Bạn có chắc muốn xóa sinh viên này không ?</h3>
				      </div>
				      <div className="modal-footer">
				        <button type="button" className="btn btn-secondary" data-dismiss="modal">Đóng</button>
				        <button type="button" className="btn btn-danger" 
				        	onClick={()=>this.actionDeleteStudent()}> Xóa
				        </button>
				      </div>
				 
				    </div>
				  </div>
				</div>
			</div>
		);
	}
}

const mapStateToProps = state => {
  return {
	  values: state.studentReducer
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
	  studentAction: bindActionCreators(studentAction, dispatch)
  }
}

export default compose(connect(mapStateToProps, mapDispatchToProps))(DeleteStudent)