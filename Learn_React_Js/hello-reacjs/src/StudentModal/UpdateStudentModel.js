import React from 'react';
import styles from '../App.module.css';

import {connect} from "react-redux";
import {bindActionCreators, compose} from 'redux';
import studentAction from '../Redux/actions/studentAction';

import {
	  Card,
	  CardHeader,
	  CardTitle,
	  CardBody,
	  Button,
	  FormGroup
	} from "reactstrap"
import { Formik, Field, Form, ErrorMessage } from "formik";

import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import * as Yup from 'yup';

import AddSchema  from '../Validation/validationModelForm'

const UpdateSchema = Yup.object().shape({
   name: Yup.string()
    .min(5, 'Tên phải dài hơn 5 ký tự!')
    .max(50, 'Tên không được dài hơn 50 ký tự!')
    .required('Không được để trống'),
   address: Yup.string()
    .min(5, 'địa chỉ phải dài hơn 5 ký tự!')
    .max(100, 'địa chỉ không được quá 100 ký tự')
    .required('Không được để trống')
});

class UpdateStudent extends React.Component{
	constructor(props){
		super(props);
		this.state = {
		    name: "",
		    address: "",
		    errName: "",
			errAddress: ""
		}
	}
	
	
	changeInput = (e) =>{
		let text = e.target.name;
		let val = e.target.value;
		this.setState({
			[text]: val
			
		})
		if(text==="name" && val===""){
			this.setState({
				errName: "Tên sinh viên không được để trống"
			})
		}
		else{
			this.setState({
				errName: ""
			})
		}
		if(text==="address" && val===""){
			this.setState({
				errAddress: "Địa chỉ không được để trống"
			})
		}
		else{
			this.setState({
				errAddress: ""
			})
		}
	}
	
	actionUpdateStudent (a) {
		if(a.name===""){
			a.name = this.props.studentInfo.name
		}
		if(a.address===""){
			a.address = this.props.studentInfo.address
		}
		const student = {
			id: this.props.studentInfo.id,
			name: a.name,
			address: a.address
	    }
		
		const {studentAction} = this.props;
		const {updateStudent} = studentAction;
		let err = "Không được bỏ trống";
		if(a.name !== "" && a.address !== ""){
			updateStudent(student);
		}
		else {
			if(a.name === ""){
				this.setState({
					errName: "Tên sinh viên không được để trống"
				})
			}
			if(a.address === ""){
				this.setState({
					errAddress: "Địa chỉ không được để trống"
				})
			}
		}
		
	}
	
	render(){
		return(
			<div>
				<div className="modal fade" id="UpdateStudentModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
				  <div className="modal-dialog" role="document">
				    <div className="modal-content">
				      <div className="modal-header">
				        <h5 className="modal-title" id="exampleModalLabel"><h3>Sửa thông tin sinh viên</h3></h5>
				        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
				          <span aria-hidden="true">&times;</span>
				        </button>
				      </div>
				      <div className="modal-body">
				      
				      	<form>
					      <div className="form-group">
					        <label htmlFor="text">Tên sinh viên:</label>
					        <input type="text" className="form-control" name="name" 
					        	defaultValue = {this.props.studentInfo.name}
					        	onChange={this.changeInput} 
					        >
					        </input>
					        <p style={{color: "red"}}><i><strong> {this.state.errName} </strong></i></p>
					      </div>
					      <div className="form-group">
					        <label htmlFor="text">Địa chỉ:</label>
					        <input type="text" className="form-control" name="address" 
					        	defaultValue = {this.props.studentInfo.address}
					        	onChange={this.changeInput}
					        />
					        <p style={{color: "red"}}><i><strong> {this.state.errAddress} </strong></i></p>
					      </div>
					      <div className="modal-footer">
					        <button type="button" className="btn btn-secondary" data-dismiss="modal">Đóng</button>
					        <button type="button" className="btn btn-success" 
					        	onClick={this.actionUpdateStudent.bind(this, this.state)}> Lưu
					        </button>
					      </div>
					    </form>
					    
					    <h3>Formik update student</h3>
					    
						<Formik
		          			initialValues={{
		          				id: this.props.studentInfo.id,
		          				name: "",
		          				address: ""
		          			}}
		          		
		          			validationSchema={UpdateSchema}
		          			
		          			onSubmit={values=>{
		          				setTimeout(()=>{
		          					const {studentAction} = this.props;
		          					const {updateStudent} = studentAction;
		          					let student  = {};
		          					student.name = values.name;
		          					student.address = values.address;
		          					updateStudent(student)
	//	          					toast.success(JSON.stringify(values))
		          				}, 1000)
		          			}}
						>
					    
						{({error, touched}) => (
							<Form>
	          					<FormGroup>
	          						<label htmlFor="name">Tên sinh viên</label>
	          						<Field 
	          							type = "text"
	          							className = "form-control"
	          							name = "name"
	          							placeholder="tên sinh viên"
	          							
	          						/>
	          						<ErrorMessage
	          							name = "name"
	          							component = "span"
	          							className = "field-error text-danger"
	          						/>
	          					</FormGroup>
	          					<FormGroup>
	          						<label htmlFor="address">Địa chỉ</label>
	          						<Field 
	          							className = "form-control"
	          							name = "address"
	          							placeholder="địa chỉ"
	          						/>
	          						<ErrorMessage
		          						name = "address"
		          						component = "span"
		          						className = "field-error text-danger"
		          					/>
	          					</FormGroup>
	          					<Button color="primary" type="submit">
	          	                  Thêm
	          	                </Button>
	          				</Form>
						)}
						</Formik>
						
				      </div>
				    </div>
				  </div>
				</div>
			</div>
		);
	}
}

const mapStateToProps = state => {
  return {
	  values: state.studentReducer
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
	  studentAction: bindActionCreators(studentAction, dispatch)
  }
}

export default compose(connect(mapStateToProps, mapDispatchToProps))(UpdateStudent)
