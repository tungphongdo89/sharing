import React from 'react';
import styles from '../App.module.css';

import {connect} from "react-redux";
import {bindActionCreators, compose} from 'redux';
import studentAction from '../Redux/actions/studentAction';

import {
	  Card,
	  CardHeader,
	  CardTitle,
	  CardBody,
	  Button,
	  FormGroup
	} from "reactstrap"
import { Formik, Field, Form, ErrorMessage } from "formik";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Input from "reactstrap/es/Input";

import * as Yup from 'yup';

import AddSchema  from '../Validation/validationModelForm'


class AddStudent_formik extends React.Component{
	constructor(props){
		super(props);
//		this.state = {
//			name: "",
//			address: ""
//		}
	}
	render(){
		return(
			<div>
				<div className="modal fade" id="AddStudentModalFormik" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
				  <div className="modal-dialog" role="document">
				    <div className="modal-content">
				      <div className="modal-header">
				        <h5 className="modal-title" id="exampleModalLabel"><h3>Thêm sinh viên</h3></h5>
				        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
				          <span aria-hidden="true">&times;</span>
				        </button>
				      </div>
				      <div className="modal-body">
					    
					    <Card>
				          	<CardBody>
				          		<Formik
				          			initialValues={{
				          				name: "",
				          				address: ""
				          			}}
				          		
				          			validationSchema={AddSchema}
				          			
				          			onSubmit={(values, actions)=>{
				          				debugger
				          				setTimeout(()=>{
				          					const {studentAction} = this.props;
				          					const {addStudent} = studentAction;
				          					let student  = {};
				          					student.name = values.name;
				          					student.address = values.address;
				          					addStudent(student);
				          				}, 1000)
				          			}}
				          		
				          			render={({ errors, touched })=>(
				          				<Form>
				          					<FormGroup>
				          						<label htmlFor="name">Tên sinh viên</label>
				          						<Field 
				          							className = "form-control"
				          							name = "name"
				          							placeholder="tên sinh viên"
				          						    
				          						/>
				          						<ErrorMessage
				          							name = "name"
				          							component = "span"
				          							className = "field-error text-danger"
				          						/>
				          					</FormGroup>
				          					<FormGroup>
				          						<label htmlFor="id">Địa chỉ</label>
				          						<Field 
				   
				          							className = "form-control"
				          							name = "address"
				          							placeholder="địa chỉ"
				          						/>
				          						<ErrorMessage
					          						name = "address"
					          						component = "span"
					          						className = "field-error text-danger"
					          					/>
				          					</FormGroup>
				          					<div style={{textAlign: "center"}}>
					          					<button type="button" className="btn btn-primary" data-dismiss="modal">Đóng</button>
					          					<button className="btn btn-primary" type="submit" style={{marginLeft: '2px'}}>
					          	                  Thêm
					          	                </button>
					          	                <button className="btn btn-primary" type="button" style={{marginLeft: '2px'}}
					          	                	onClick={()=>this.createNew()}
					          	                >Tạo mới
					          	                </button>
				          	                </div>
				          				</Form>
				          			)}
				          		/>
				          	</CardBody>
					    </Card>
					    
				      </div>
				    </div>
				  </div>
				</div>
			</div>
		);
	}
}

const mapStateToProps = state => {
  return {
	  values: state.studentReducer
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
	  studentAction: bindActionCreators(studentAction, dispatch)
  }

}

export default compose(connect(mapStateToProps, mapDispatchToProps))(AddStudent_formik)
