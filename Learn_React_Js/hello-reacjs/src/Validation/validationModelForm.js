import * as Yup from "yup";

//formik
const AddSchema = Yup.object().shape({
	name: Yup.string()
     .min(4, 'Tên phải dài hơn 4 ký tự!')
     .max(50, 'Tên không được dài hơn 50 ký tự!')
     .required('Không được để trống'),
    address: Yup.string()
     .min(4, 'địa chỉ phải dài hơn 4 ký tự!')
     .max(100, 'địa chỉ không được quá 100 ký tự')
     .required('Không được để trống'),
   
});

export default AddSchema