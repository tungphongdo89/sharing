package com.tungphongdo.Service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.tungphongdo.DTO.StudentDTO;
import com.tungphongdo.Entity.StudentEntity;
import com.tungphongdo.Repository.StudentRepository;
import com.tungphongdo.Service.ServiceResult.Status;

@Service
public class StudentService {
	
	@Autowired
	private StudentRepository studentRepository;
	
	public ServiceResult findAll(ServicePagination pagination) {
		ServiceResult result  = new ServiceResult();
		List<StudentEntity> studentEntities = studentRepository.findAll();
		result.setData(studentEntities);
		if(pagination.getPage()==0 && pagination.getSize()==0) {
			result.setData(studentEntities);
		}
		else {
			Pageable pageable = PageRequest.of(pagination.getPage(), pagination.getSize());
			result.setData(studentRepository.findStudent(pageable)); // số bản ghi trong 1 trang
			int start = pagination.getStart() + 1;
			int totalPages = studentRepository.findStudent(pageable).getTotalPages();
			List<Integer> list = new ArrayList<Integer>();
			if(start==1 || start ==2 || start == 3) {
				for(int i=1; i<=5; i++) {
					list.add(i);
				}
			}
			else {
				if(start < (totalPages - 2) ) {
					for(int i = (start-2) ; i <= (start+2) ; i++) {
						list.add(i);
					}
				}
				else {
					for(int i = (totalPages - 4) ; i <= totalPages ; i++) {
						list.add(i);
					}
				}
			}
			System.out.println(studentRepository.findStudent(pageable).getTotalPages());
			result.setListPages(list);
		}
		return result;
	}
	
//	public ServiceResult findById(int id) {
//		ServiceResult result = new ServiceResult();
//		StudentEntity studentEntity = studentRepository.findById(id);
//		result.setData(studentEntity);
//		return result;
//	}
	
	public ServiceResult create(StudentDTO studentDTO) throws Exception {
	    ServiceResult result = new ServiceResult();
	    StudentEntity student = studentRepository.findByName(studentDTO.getName());
	    if(null == student) {
	    	result.setData(studentRepository.save(studentDTO.toModel()));
	    	return result;
	    }
	    else {
	    	result.setMessage("Tên sinh viên đã tồn tại");
	    	result.setStatus(Status.FAILED);
	    	return result;
	    }
	}
	
	public ServiceResult update(StudentDTO studentDTO) {
		ServiceResult result = new ServiceResult();
		StudentEntity student = studentRepository.findByName(studentDTO.getName());
		StudentEntity student2 = studentRepository.findById(studentDTO.getId()).get();
		if (null == student2) {
			result.setStatus(Status.FAILED);
		      result.setMessage("Không tìm thấy sinh viên này");
		} 
		else {
			if(null == student) {
				result.setData(studentRepository.save(studentDTO.toModel()));
				result.setMessage("Cập nhật thành công");
				result.setStatus(Status.SUCCESS);
			}
			else {
				result.setMessage("Tên sinh viên đã tồn tại");
		    	result.setStatus(Status.FAILED);
			}
		}
		return result;
	}
	
	public ServiceResult delete(StudentDTO studentDTO) {
	    ServiceResult result = new ServiceResult();
	    StudentEntity student = studentRepository.findById(studentDTO.getId()).get();
	    if (null == student) {
		    result.setStatus(Status.FAILED);
		    result.setMessage("Không tìm thấy sinh viên này");
	    } 
	    else {
	    	studentRepository.delete(studentDTO.toModel());
	        result.setMessage("thành công");
	    }
	    return result;
	}
	

}
