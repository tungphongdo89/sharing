package com.tungphongdo.Service;

import java.util.List;

public class ServiceResult {
	private Status status = Status.SUCCESS;
	private String message;
	private Object data;
	private List<Integer> listPages;
	private String token;
	public enum Status {
	  SUCCESS, FAILED;
	}
	
	public ServiceResult() {
		super();
	}
	
	public Status getStatus() {
		return status;
	}
	public void setStatus(Status status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Object getData() {
		return data;
	}
	public void setData(Object data) {
		this.data = data;
	}

	public List<Integer> getListPages() {
		return listPages;
	}

	public void setListPages(List<Integer> listPages) {
		this.listPages = listPages;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
	

}
