package com.tungphongdo.Repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.tungphongdo.DTO.StudentDTO;
import com.tungphongdo.Entity.StudentEntity;

@Repository
public interface StudentRepository extends JpaRepository<StudentEntity, Integer>{
	public StudentEntity findByName(String name);
	
	@Query("select s from StudentEntity s")
	Page<StudentEntity> findStudent(Pageable pageable);

	
	
}
