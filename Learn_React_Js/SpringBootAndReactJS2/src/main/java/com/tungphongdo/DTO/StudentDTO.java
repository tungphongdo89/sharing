package com.tungphongdo.DTO;

import com.tungphongdo.Entity.StudentEntity;

public class StudentDTO {
	private int id;
	private String username;
	private String password;
	private String name;
	private String address;
	
	public StudentDTO() {
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public StudentEntity toModel() {
		StudentEntity student = new StudentEntity();
		student.setId(this.id);
		student.setUsername(this.username);
		student.setPassword(this.password);
		student.setName(this.name);
		student.setAddress(this.address);
		return student;
		
	}

}
