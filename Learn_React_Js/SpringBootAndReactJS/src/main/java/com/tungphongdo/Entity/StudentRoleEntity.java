package com.tungphongdo.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.tungphongdo.DTO.StudentRoleDTO;

@Entity
@Table(name = "student_role")
public class StudentRoleEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@Column(name = "student_id")
	private int studentId;
	
	@Column(name = "role_id")
	private int roleId;

	public StudentRoleEntity() {
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getStudentId() {
		return studentId;
	}

	public void setStudentId(int studentId) {
		this.studentId = studentId;
	}

	public int getRoleId() {
		return roleId;
	}

	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}
	
	public StudentRoleDTO toModel() {
		StudentRoleDTO studentRoleDTO = new StudentRoleDTO();
		studentRoleDTO.setId(this.id);
		studentRoleDTO.setStudentId(this.studentId);
		studentRoleDTO.setRoleId(this.roleId);
		return studentRoleDTO;
	}

}
