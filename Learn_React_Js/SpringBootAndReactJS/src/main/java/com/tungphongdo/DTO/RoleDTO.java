package com.tungphongdo.DTO;

import com.tungphongdo.Entity.RoleEntity;

public class RoleDTO {
	
	private int id;
	private String roleName;
	
	public RoleDTO() {
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	
	public RoleEntity toModel() {
		RoleEntity role = new RoleEntity();
		role.setId(this.id);
		role.setRoleName(this.roleName);
		return role;
	}

}
