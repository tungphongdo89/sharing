package com.tungphongdo.DTO;

import java.util.ArrayList;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import com.tungphongdo.Entity.StudentEntity;

public class StudentDTO {
	private int id;
	private String username;
	private String password;
	private String name;
	private String address;
	
	public List<GrantedAuthority> getAuthorities(List<RoleDTO> lisRoleDTOs){
		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		for (RoleDTO roleDTO : lisRoleDTOs) {
			authorities.add(new SimpleGrantedAuthority(roleDTO.getRoleName()));
		}
		return authorities;
	}
	
	public StudentDTO() {
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public StudentEntity toModel() {
		StudentEntity student = new StudentEntity();
		student.setId(this.id);
		student.setUsername(this.username);
		student.setPassword(this.password);
		student.setName(this.name);
		student.setAddress(this.address);
		return student;
		
	}

}
