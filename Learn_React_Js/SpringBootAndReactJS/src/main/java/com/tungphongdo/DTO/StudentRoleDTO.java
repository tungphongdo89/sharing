package com.tungphongdo.DTO;

import com.tungphongdo.Entity.StudentRoleEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
//@NoArgsConstructor
@AllArgsConstructor
public class StudentRoleDTO {
	
	private int id;
	private int studentId;
	private int roleId;
	
	private RoleDTO roleDTO;

	public StudentRoleDTO() {
		super();
	}

	public StudentRoleDTO(int id, int roleId, int studentId){
		this.id = id;
		this.studentId = studentId;
		this.roleId = roleId;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getStudentId() {
		return studentId;
	}

	public void setStudentId(int studentId) {
		this.studentId = studentId;
	}

	public int getRoleId() {
		return roleId;
	}

	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}
	
	public RoleDTO getRoleDTO() {
		return roleDTO;
	}

	public void setRoleDTO(RoleDTO roleDTO) {
		this.roleDTO = roleDTO;
	}

	public StudentRoleEntity toModel() {
		StudentRoleEntity studentRoleEntity = new StudentRoleEntity();
		studentRoleEntity.setId(this.id);
		studentRoleEntity.setStudentId(this.studentId);
		studentRoleEntity.setRoleId(this.roleId);
		return studentRoleEntity;
	}

}
