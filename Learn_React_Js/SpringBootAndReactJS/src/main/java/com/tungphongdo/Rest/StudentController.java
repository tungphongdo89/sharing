package com.tungphongdo.Rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tungphongdo.DTO.StudentDTO;
import com.tungphongdo.Service.ServicePagination;
import com.tungphongdo.Service.ServiceResult;
import com.tungphongdo.Service.StudentService;

@RestController
@RequestMapping(value= {"/api"})
public class StudentController {
	
	@Autowired
	private StudentService studentService;
	
	@PostMapping("/students")
	public ResponseEntity<ServiceResult> findAll(@RequestBody ServicePagination pagination){
		ResponseEntity<ServiceResult> result = new ResponseEntity<ServiceResult>(studentService.findAll(pagination), HttpStatus.OK);
		return result;
	}
	
//	@GetMapping("/students/{id}")
//	public ResponseEntity<ServiceResult> findById(@PathVariable int id){
//		ResponseEntity<ServiceResult> result = new ResponseEntity<ServiceResult>(studentService.findById(id), HttpStatus.OK);
//		return result;
//	}
	
	@PostMapping("/create")
	public ResponseEntity<ServiceResult> create(@RequestBody StudentDTO studentDTO) throws Exception{
		ResponseEntity<ServiceResult> responseEntity = new ResponseEntity<ServiceResult>(studentService.create(studentDTO), HttpStatus.OK);
		return responseEntity;
	}
	
	@PutMapping("/update")
	public ResponseEntity<ServiceResult> update(@RequestBody StudentDTO studentDTO){
		return new ResponseEntity<ServiceResult>(studentService.update(studentDTO), HttpStatus.OK);
	}
	
	@DeleteMapping("/delete")
	public ResponseEntity<ServiceResult> delete(@RequestBody StudentDTO studentDTO){
		return new ResponseEntity<ServiceResult>(studentService.delete(studentDTO), HttpStatus.OK);
	}
	
	@PostMapping("/login")
	public ResponseEntity<ServiceResult> login(@RequestBody StudentDTO studentDTO){
		ResponseEntity<ServiceResult> responseEntity = new ResponseEntity<ServiceResult>(studentService.login(studentDTO), HttpStatus.OK);
		return responseEntity;
	}

}
