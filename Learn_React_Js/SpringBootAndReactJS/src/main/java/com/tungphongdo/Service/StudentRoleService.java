package com.tungphongdo.Service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tungphongdo.Entity.StudentRoleEntity;
import com.tungphongdo.Repository.StudentRoleRepository;

@Service
public class StudentRoleService {
	
	@Autowired
	private StudentRoleRepository studentRoleRepository;
	
	public List<StudentRoleEntity> getListRolesByStudentId(int studentId){
		
		List<StudentRoleEntity> studentRoleEntities = studentRoleRepository.findByStudentId(studentId);
		
		
		return studentRoleEntities;
	}

}
