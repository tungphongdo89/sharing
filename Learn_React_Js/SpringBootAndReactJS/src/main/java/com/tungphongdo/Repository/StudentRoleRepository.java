package com.tungphongdo.Repository;

import java.util.List;

import com.tungphongdo.DTO.StudentRoleDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.tungphongdo.Entity.StudentRoleEntity;

@Repository
public interface StudentRoleRepository extends JpaRepository<StudentRoleEntity, Integer>{
	
	List<StudentRoleEntity> findByStudentId(int id);

	@Query("select new com.tungphongdo.DTO.StudentRoleDTO(s.id, s.roleId, s.studentId) from StudentRoleEntity s " +
			"where s.studentId = ?1")
	List<StudentRoleDTO> getListRolesByStudentId(int studentId);

//	@Query(value = "select s.id as id , s.role_id as roleId, s.student_id as studentId  from student_role s " +
//			"where s.student_id = ?", nativeQuery = true)
//	List<StudentRoleDTO> getListRolesByStudentId(int studentId);

}
