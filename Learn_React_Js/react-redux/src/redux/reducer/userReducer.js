import * as config from '../common/const';

const initState = {
    loginSuccess: false
}

const userReducer = (state = initState, action) =>{
    switch(action.type){
        //login
        case config.LOGIN: {
            debugger
            const data = action.payload;
            state = {...state};
            return state;
        }
        case config.LOGIN_SUCCESS: {
            debugger
            const data = action.payload.data;
            localStorage.setItem("token", data.token)
            localStorage.setItem("fullName", data.data.fullName)
            state = {...state, loginSuccess: true};
            return state;
        }
        case config.LOGIN_FAILED: {
            debugger
            const resp = action.payload;
            state = {...state};
            return state;
        }

        //logout
        case config.LOGOUT: {
            debugger
            state = {...state, loginSuccess: false};
            return state;
        }

        default:
            return state;
    }
}

export default userReducer