import {call, delay, put, takeEvery} from "redux-saga/effects";
import * as config from '../common/const';
import userAction from '../action/userAction';
import userApi from '../api/userApi';

function* login({payload}){
    debugger
    const data = payload;
    const resp = yield call(userApi.apiLogin, data);
    if("SUCCESS" === resp.status){
        yield put(userAction.loginSuccess(resp))
    }
    else{
        yield put(userAction.loginFailed(resp))
    }
}

function* userSaga(){
    yield takeEvery(config.LOGIN, login)
}

export default userSaga