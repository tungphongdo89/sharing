import * as config from '../common/const';

const userAction = {
    login,
    loginSuccess,
    loginFailed,

    logout
}

//Login
function login(data){
    debugger
    return{
        type: config.LOGIN,
        payload: {data}
    }
}

function loginSuccess(data){
    debugger
    return{
        type: config.LOGIN_SUCCESS,
        payload: {data}
    }
}

function loginFailed(data){
    debugger
    return{
        type: config.LOGIN_FAILED,
        payload: {data}
    }
}
//logout
function logout(){
    debugger
    return{
        type: config.LOGOUT
    }
}


export default userAction