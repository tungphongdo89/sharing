import * as Yup from "yup";

//formik
const LoginSchema = Yup.object().shape({
    username: Yup.string()
        .min(4, 'Tên đăng nhập phải dài hơn 4 ký tự!')
        .max(100, 'Tên đăng nhập không được quá 100 ký tự')
        .required('Tên đăng nhập không được để trống'),

    password: Yup.string()
        .min(5, 'Tên đăng nhập phải dài hơn 4 ký tự!')
        .max(100, 'Tên đăng nhập không được quá 100 ký tự')
        .required('Mật khẩu không được để trống'),
});

export default LoginSchema