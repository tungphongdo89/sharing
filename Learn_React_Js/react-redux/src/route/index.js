import React from 'react';
import { Route, Switch } from 'react-router-dom';

import Login from "../view/login";
import Home from "../view/home"

class Router extends React.Component{
    render() {
        return(
            <div>
                <Switch>
                    <Route path="/" component={Home} exact/>
                    <Route path="/pages/login" component={Login}/>
                </Switch>
            </div>
        );
    }
}

export default Router;