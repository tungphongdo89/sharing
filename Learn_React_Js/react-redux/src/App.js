import React from 'react';

import Router from "../src/route";
import {Route, Switch} from "react-router";
import Home from "./view/home";
import Login from "./view/login";

function App() {
  return (
      <>
        {/*<Router/>*/}
        <div>
          <Switch>
            <Route path="/" component={Home} />
            <Route path="/pages/login" component={Login} />
          </Switch>
        </div>
      </>
  );
}

export default App;