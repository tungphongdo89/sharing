import React from 'react';
import {connect} from "react-redux";
import {bindActionCreators, compose} from "redux";
import {withStyles} from "@material-ui/core";

import userAction from "../../redux/action/userAction"
import styles from "./style";
import {Redirect} from "react-router";

class Home extends React.Component{

    constructor(props){
        super(props);
        this.state={

        }
    }

    logoutUser =()=> {
        localStorage.removeItem("token");
        const {userAction} = this.props;
        const {logout} = userAction;
        logout();
        window.location.replace("/")
    }

    render() {
        debugger
        const token =  localStorage.token;
        const fullName = localStorage.fullName;
        const {classess} = this.props;
        return (
            <>
                <h2>Đây là trang chủ</h2>

                {undefined === token ?
                    <a href="/pages/login"><button>đăng nhập</button></a>
                    :
                    <>
                        <h3>Xin chào {fullName } </h3>
                        <button onClick={() => this.logoutUser()}>đăng xuất</button>
                    </>

                }

            </>
        );
    }

}

const mapStateToProps = (state) => {
    return {
        // token: state.userReducer.token,
        // user: state.userReducer.user
        loginSuccess: state.userReducer.loginSuccess
    }
}
const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        userAction : bindActionCreators(userAction , dispatch),
    }
}
const withConnect = connect(mapStateToProps, mapDispatchToProps)

export default compose(withStyles(styles), withConnect)(Home);

