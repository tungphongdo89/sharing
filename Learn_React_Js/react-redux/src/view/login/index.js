import React from 'react';
import {connect} from "react-redux";
import {bindActionCreators, compose} from "redux";
import {withStyles} from "@material-ui/core";

import {Card, CardHeader, CardTitle, CardBody, Button, Label, FormGroup} from "reactstrap"
import { Formik, Field, Form, ErrorMessage } from "formik";

import userAction from "../../redux/action/userAction"
import styles from "./style";

import { Redirect } from 'react-router';

import LoginSchema from "../../validation/validLogin"

class Login extends React.Component{

    constructor(props){
        super(props);
        this.state={

        }
    }

    render() {
        debugger
        const {classess} = this.props;
        const token =  localStorage.token;
        return (
            <>
                <div className="container-fluid" id={styles.background} >
                    <a href="/"><button>Trang chủ</button></a>
                    <div>
                        <div>
                            <Card>
                                <CardBody>
                                    <Formik
                                        initialValues={{
                                            username: "",
                                            password: ""
                                        }}

                                        onSubmit={values=>{
                                            debugger
                                            setTimeout(()=>{
                                                const {userAction} = this.props;
                                                const {login} = userAction;
                                                let user  = {};
                                                user.username = values.username;
                                                user.password = values.password;
                                                login(user)
                                            }, 1000)
                                        }}

                                        validationSchema={LoginSchema}

                                        render={({ errors, touched })=>(

                                            <Form>
                                                <p>ĐĂNG NHẬP</p>
                                                <FormGroup>
                                                    <label htmlFor="name">Tên đăng nhập</label>
                                                    <Field className = "form-control" name ="username" placeholder="tên đăng nhập" />
                                                    <ErrorMessage name = "username" component = "span" className = "field-error text-danger" />
                                                </FormGroup>
                                                <FormGroup>
                                                    <label htmlFor="id">Mật khẩu</label>
                                                    <Field type="password" className = "form-control" name ="password" placeholder="mật khẩu" />
                                                    <ErrorMessage name = "password" component = "span" className = "field-error text-danger" />
                                                </FormGroup>
                                                <div>
                                                    <button className="btn btn-danger" type="submit">
                                                        OK
                                                    </button>

                                                </div>
                                            </Form>
                                        )}
                                    />
                                    {undefined === token && this.props.loginSuccess === false ?
                                        null : <Redirect to="/" />
                                    }

                                </CardBody>
                            </Card>
                        </div>
                    </div>
                </div>
            </>
        );
    }

}

const mapStateToProps = (state) => {
    return {
        // token: state.userReducer.token,
        loginSuccess: state.userReducer.loginSuccess
    }
}
const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        userAction : bindActionCreators(userAction , dispatch),
    }
}
const withConnect = connect(mapStateToProps, mapDispatchToProps)

export default compose(withStyles(styles), withConnect)(Login);

