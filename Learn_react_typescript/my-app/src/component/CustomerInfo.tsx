import React from "react";

interface Customer {
  id: number;
  name: string;
  address?: string;
}

interface TypeCustomer {
  customer: {
    id: number;
    name: string;
    address?: string;
  };
  handleDeleting: (customerId: number) => void;
  handleEditing: (customerId: number) => void;
}

const CustomerInfo: React.FC<TypeCustomer> = (props) => {
  return (
    <tr style={{ border: "1px solid black" }}>
      <td style={{ border: "1px solid black" }}>{props.customer.id}</td>
      <td style={{ border: "1px solid black" }}>{props.customer.name}</td>
      <td style={{ border: "1px solid black" }}>{props.customer.address}</td>
      <td>
        <button onClick={() => props.handleEditing(props.customer.id)}>
          Edit
        </button>
        <button onClick={() => props.handleDeleting(props.customer.id)}>
          Delete
        </button>
      </td>
    </tr>
  );
};

export default CustomerInfo;
