interface Customer {
  id: number;
  name: string;
  address?: string;
}
export const listCustomers: Customer[] = [
  {
    id: 1,
    name: "tung",
    address: "hanoi",
  },
  {
    id: 2,
    name: "tung 2",
  },
  {
    id: 3,
    name: "tung 3",
    address: "thanh hoa",
  },
];
