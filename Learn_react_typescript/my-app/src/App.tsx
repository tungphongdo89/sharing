import React, { useState } from "react";
import "./App.css";
import CustomerInfo from "./component/CustomerInfo";
import { listCustomers } from "./component/ListCustomer";
import AddNewCustomer from "./component/AddNewCustomer";

interface Customer {
  id: number;
  name: string;
  address?: string;
}

const App: React.FC = () => {
  const [customerEdit, setCustomerEdit] = useState<Customer>();
  const [listCustomerNew, setListCustomerNew] = useState(listCustomers);

  const randomString = (length: number): string => {
    let result = "";
    let characters =
      "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    let charactersLength = characters.length;
    for (let i = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
  };

  const addRamdomCustomer = () => {
    let customer: Customer = {
      id: listCustomerNew.length + 1,
      name: "name " + randomString(5),
      address: "address " + randomString(5),
    };
    listCustomerNew.push(customer);
    setListCustomerNew(listCustomerNew.filter((customer) => customer));
  };

  const handleAddCustomer = (customer: Customer) => {
    listCustomerNew.push(customer);
    setListCustomerNew(listCustomerNew.filter((cus) => cus));
  };

  const handleEditing = (customerId: number) => {
    debugger;
    let customer = listCustomerNew.find((c) => c.id === customerId);
    console.log(customer);
    setCustomerEdit(customer);
  };

  const handleUpdateCustomer = (customer: Customer) => {
    debugger;
    // listCustomerNew.forEach(c => c.id === customer.id )
    for (let i = 0; i < listCustomerNew.length; i++) {
      if (listCustomerNew[i].id === customer.id) {
        listCustomerNew.splice(i, 1, customer);
        setListCustomerNew(listCustomerNew.filter((cus) => cus));
        return;
      }
    }
  };

  const handleDeleting = (customerId: number) => {
    console.log("customer id = " + customerId);
    setListCustomerNew(
      listCustomerNew.filter((customer) => customer.id !== customerId)
    );
  };

  return (
    <>
      <AddNewCustomer
        listCustomersLenght={listCustomerNew.length}
        handleAddCustomer={handleAddCustomer}
        handleUpdateCustomer={handleUpdateCustomer}
        customerEdit={customerEdit}
      />
      <button style={{ marginTop: "1%" }} onClick={addRamdomCustomer}>
        add random customer
      </button>
      <div>
        <table style={{ border: "1px solid black", marginTop: "1%" }}>
          <tr style={{ border: "1px solid black" }}>
            <th style={{ border: "1px solid black" }}>id</th>
            <th style={{ border: "1px solid black" }}>name</th>
            <th style={{ border: "1px solid black" }}>address</th>
            <th style={{ border: "1px solid black" }}>actions</th>
          </tr>
          {listCustomerNew &&
            listCustomerNew.map((customer) => (
              <>
                {/* <ManageCustomer
              id={customer.id}
              name={customer.name}
              address={customer.address ? customer.address : ""}
            /> */}
                <CustomerInfo
                  customer={customer}
                  handleEditing={handleEditing}
                  handleDeleting={handleDeleting}
                />
              </>
            ))}
        </table>
      </div>
    </>
  );
};

export default App;
