$(document).ready(function(){
	//hide-show login-changePass
	$("#goToChangePass").click(function(){
		$("#loginForm").hide();
		$("#changePassForm").show();
	});
	$("#goToLogin").click(function(){
		$("#loginForm").show();
		$("#changePassForm").hide();
	});
});

var myApp = angular.module("myApp", []);

myApp.controller("myController", function ($scope) {
	console.log("In my .....");
	
	//Xử lý form đăng nhập 
	$scope.account = {};
	
	$scope.resetLoginForm = function(){
		$scope.account.username = "";
		$scope.account.password = "";
	}
	
	//Xử lý form đổi mật khẩu
	$scope.newAccount = {};
	
	$scope.resetChangePassForm = function(){
		$scope.newAccount.username = "";
		$scope.newAccount.password = "";
		$scope.newAccount.newPassword = "";
		$scope.newAccount.rePassword = "";
	}
	
	
});