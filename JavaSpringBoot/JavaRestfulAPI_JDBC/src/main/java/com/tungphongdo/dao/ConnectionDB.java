package com.tungphongdo.dao;

import java.sql.Connection;
import java.sql.SQLException;

public class ConnectionDB {
	
	  public static Connection getConnection() throws ClassNotFoundException, SQLException {
		  return SqlServerConnection.getSQLServerConnection();
	  }
	   
	  public static void close(Connection conn) {
	      try {
	          conn.close();
	      } 
	      catch (Exception e) {
	      }
	  }
	
	  public static void rollback(Connection conn) {
	      try {
	          conn.rollback();
	      } 
	      catch (Exception e) {
	      }
	  }

}
