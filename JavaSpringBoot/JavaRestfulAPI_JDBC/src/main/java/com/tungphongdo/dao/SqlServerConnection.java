package com.tungphongdo.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class SqlServerConnection {
	public static Connection getSQLServerConnection()
            throws SQLException, ClassNotFoundException {
 
        String hostName = "localhost";
        String sqlInstanceName = "SQLEXPRESS";
        String database = "TestJavaRestfulApiJDBC";
        String userName = "sa";
        String password = "thomthom";
 
        return getSQLServerConnection(hostName, sqlInstanceName, database, userName, password);
    }
 
    // Kết nối tới SQL Server sử dụng thư viện JTDS.
    private static Connection getSQLServerConnection(String hostName, String sqlInstanceName, 
    		String database, String userName, String password) throws ClassNotFoundException, SQLException {
 
        Class.forName("net.sourceforge.jtds.jdbc.Driver");
 
        // jdbc:jtds:sqlserver://localhost:1433/TestJavaRestfulApiJDBC;instance=SQLEXPRESS
        String connectionURL = "jdbc:jtds:sqlserver://" + hostName + ":1433/"
                + database + ";instance=" + sqlInstanceName;
 
        Connection conn = DriverManager.getConnection(connectionURL, userName, password);
        
        if(conn != null) {
        	System.out.println("Thành công");
        }
        else {
			System.out.println("Thất bại");
		}
        return conn;
    }
}
