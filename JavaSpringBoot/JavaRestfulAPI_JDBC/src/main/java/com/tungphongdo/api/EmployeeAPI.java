package com.tungphongdo.api;

import java.sql.SQLException;
import java.util.List;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.tungphongdo.model.Employee;
import com.tungphongdo.service.QueryData;

@Path("/employee")
public class EmployeeAPI {
	
	//Lấy ra toàn bộ nhân viên
	@GET
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public List<Employee> findAll() throws ClassNotFoundException, SQLException{
		List<Employee> employees = QueryData.findAll();
		return employees;
	}
	
	//Lấy ra nhân viên theo Id
	@GET
	@Path("/{id}")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public Employee findById(@PathParam("id") int id) throws ClassNotFoundException, SQLException {
		return QueryData.findById(id);
	}
	
	//Thêm nhân viên
	@POST
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public void save(Employee employee) throws ClassNotFoundException, SQLException {
		QueryData.save(employee);
	}
	
	//Sửa thông tin nhân viên
	@PUT
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public void update(Employee employee) throws ClassNotFoundException, SQLException {
		QueryData.update(employee);
	}
	
	//Xóa nhân viên
	@DELETE
	@Path("/{id}")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public void delete(@PathParam("id") int id) throws ClassNotFoundException, SQLException {
		QueryData.delete(id);
	}

}
