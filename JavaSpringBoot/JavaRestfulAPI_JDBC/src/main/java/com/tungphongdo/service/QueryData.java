package com.tungphongdo.service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.tungphongdo.dao.ConnectionDB;
import com.tungphongdo.model.Employee;

public class QueryData {
	
	//Lấy ra danh sách toàn bộ nhân viên
	public static List<Employee> findAll() throws SQLException, ClassNotFoundException{
		
		Connection conn = ConnectionDB.getConnection();
		
		String sql = "select * from employee";
		
		PreparedStatement pstm = conn.prepareStatement(sql);
		ResultSet rs = pstm.executeQuery();
		
		List<Employee> employees = new ArrayList<Employee>();
		while(rs.next()) {
			int id = rs.getInt("id");
			String name = rs.getString("name");
			String address = rs.getString("address");
			
			Employee employee = new Employee();
			employee.setId(id);
			employee.setName(name);
			employee.setAddress(address);
			employees.add(employee);
		}
		return employees;
	}

	//Lấy ra nhân viên theo Id
	public static Employee findById(int id) throws SQLException, ClassNotFoundException {
		
		Connection conn = ConnectionDB.getConnection();
		
		String sql = "select * from employee e where e.id = ?";
		
		PreparedStatement pstm = conn.prepareStatement(sql);
		pstm.setInt(1, id);
		
		ResultSet rs = pstm.executeQuery();
		
		while(rs.next()) {
			String name = rs.getString("name");
			String address = rs.getString("address");
			
			Employee employee = new Employee();
			employee.setId(id);
			employee.setName(name);
			employee.setAddress(address);
			return employee;
		}
		return null;
	}
	
	//thêm nhân viên
	public static void save(Employee employee) throws SQLException, ClassNotFoundException {
		Connection conn = ConnectionDB.getConnection();
		
        String sql = "insert into employee(name, address) values (?,?)";
 
        PreparedStatement pstm = conn.prepareStatement(sql);
        pstm.setString(1, employee.getName());
        pstm.setString(2, employee.getAddress());
        pstm.executeUpdate();
    }
	
	//sửa thông tin nhân viên
	public static void update(Employee employee) throws SQLException, ClassNotFoundException {
		Connection conn = ConnectionDB.getConnection();
		
		String sql = "update employee set name = ?, address = ? where id= ? ";
		
		 PreparedStatement pstm = conn.prepareStatement(sql);
		 pstm.setString(1, employee.getName());
		 pstm.setString(2, employee.getAddress());
		 pstm.setInt(3, employee.getId());
		 pstm.executeUpdate();
	}
	
	//xóa nhân viên qua id
	public static void delete(int id) throws SQLException, ClassNotFoundException {
		
		Connection conn = ConnectionDB.getConnection();
		
        String sql = "delete from employee where id= ?";
 
        PreparedStatement pstm = conn.prepareStatement(sql);
        pstm.setInt(1, id);
        pstm.executeUpdate();
    }
}
