package com.tungphongdo.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.tungphongdo.entity.CustomerEntity;

@Repository
@Transactional(rollbackFor = Exception.class)
public class CustomerDao {
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public List<CustomerEntity> findAll() {
		Session session = this.sessionFactory.getCurrentSession();
		List<CustomerEntity> customerEntities = session.createQuery("from CustomerEntity", CustomerEntity.class).getResultList();
		return customerEntities;
	}
	
	public CustomerEntity findById(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		CustomerEntity customer = session.get(CustomerEntity.class, id);
		return customer;
	}
	
	public void save(CustomerEntity customerEntity) {
		Session session = this.sessionFactory.getCurrentSession();
		session.save(customerEntity);
	}
	
	public void update(CustomerEntity customerEntity) {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(customerEntity);
	}
	
	public void delete(CustomerEntity customerEntity) {
		Session session = this.sessionFactory.getCurrentSession();
		session.remove(customerEntity);
	}
	

}
