package com.tungphongdo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tungphongdo.entity.CustomerEntity;
import com.tungphongdo.service.ServiceResult.Status;

//Class này sẽ kiểm tra dữ liệu đầu vào, nó gọi tới CustomerService và trả dữ liệu v
@Service
public class CustomerCheckData {
	@Autowired
	private CustomerService customerService;
	
	public ServiceResult findAll() {
		ServiceResult result = new ServiceResult();
		result.setData(customerService.findAll());
		return result;
	}
	
	public ServiceResult findById(int id) {
		ServiceResult result = new ServiceResult();
		CustomerEntity customerEntity = customerService.findById(id);
		result.setData(customerEntity);
		return result;
	}
	
	public ServiceResult save(CustomerEntity customerEntity) {
		ServiceResult result = new ServiceResult();
		customerService.save(customerEntity);
		result.setMessage("Thêm thành công");
		result.setData(customerEntity);
		return result;
	}
	
	public ServiceResult update(CustomerEntity customerEntity) {
		ServiceResult result = new ServiceResult();
		
		if(customerService.findById(customerEntity.getId()) == null){
			result.setStatus(Status.FAILED);
		    result.setMessage("Không tìm thấy khách hàng");
		}
		else {
			customerService.update(customerEntity);
			result.setData(customerEntity);
		}
		return result;
	}
	
	public ServiceResult delete(int id) {
		ServiceResult result = new ServiceResult();
		
		CustomerEntity customer = customerService.findById(id);
		if(customer == null) {
			result.setStatus(Status.FAILED);
			result.setMessage("Không tìm thấy customer");
		}
		else {
		    customerService.delete(customer);
		    result.setMessage("Xóa thành công");
		}
		return result;
	}

}
