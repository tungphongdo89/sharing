package com.tungphongdo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tungphongdo.dao.CustomerDao;
import com.tungphongdo.entity.CustomerEntity;

@Service
@Transactional
public class CustomerService {
	@Autowired
	private CustomerDao customerDao;
	
	public List<CustomerEntity> findAll(){
		return customerDao.findAll();
	}
	
	public CustomerEntity findById(int id) {
		return customerDao.findById(id);
	}
	
	public void save(CustomerEntity customerEntity) {
		customerDao.save(customerEntity);
	}
	
	public void update(CustomerEntity customerEntity) {
		customerDao.update(customerEntity);
	}
	
	public void delete(CustomerEntity customerEntity) {
		customerDao.delete(customerEntity);
	}
	
	public void deleteById(int id) {
		CustomerEntity customerEntity =  customerDao.findById(id);
		if(customerEntity != null) {
			customerDao.delete(customerEntity);
		}
	}

}
