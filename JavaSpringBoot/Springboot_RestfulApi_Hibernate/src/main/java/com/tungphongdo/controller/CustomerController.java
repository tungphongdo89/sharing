package com.tungphongdo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tungphongdo.entity.CustomerEntity;
import com.tungphongdo.service.CustomerCheckData;
import com.tungphongdo.service.ServiceResult;

@RestController
@RequestMapping(value = "/api/v1")
public class CustomerController {
	
	@Autowired
	private CustomerCheckData CustomerCheckData;
	
	//lấy ra toàn bộ customer
	@GetMapping("/customer")
	public ResponseEntity<ServiceResult> findAllCustomer(){
		ResponseEntity<ServiceResult> responseEntity = 
				new ResponseEntity<ServiceResult>(CustomerCheckData.findAll(), HttpStatus.OK);
		return responseEntity;
	}
	
	//lấy customer theo id
	@GetMapping("/customer/{id}")
	public ResponseEntity<ServiceResult> findById(@PathVariable int id){
		return new ResponseEntity<ServiceResult>(CustomerCheckData.findById(id), HttpStatus.OK);
	}
	
	//tạo mới customer
	@PostMapping("/customer")
	public ResponseEntity<ServiceResult> createCustomer(@RequestBody CustomerEntity customer){
		return new ResponseEntity<ServiceResult>(CustomerCheckData.save(customer), HttpStatus.OK);
	}
	
	//cập nhật customer
	@PutMapping("/customer")
	public ResponseEntity<ServiceResult> updateCustomer(@RequestBody CustomerEntity customer){
		return new ResponseEntity<ServiceResult>(CustomerCheckData.update(customer), HttpStatus.OK);
	}
	
	//xóa customer
	@DeleteMapping("/customer")
	public ResponseEntity<ServiceResult> deleteCustomer(@RequestBody DeleteCustomerRequest request){
		return new ResponseEntity<ServiceResult>(CustomerCheckData.delete(request.getId()), HttpStatus.OK);
	}

}
