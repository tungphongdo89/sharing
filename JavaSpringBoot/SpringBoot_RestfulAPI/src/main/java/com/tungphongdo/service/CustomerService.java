package com.tungphongdo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tungphongdo.entities.Customer;
import com.tungphongdo.repository.CustomerRepository;
import com.tungphongdo.service.ServiceResult.Status;

@Service
public class CustomerService {
	
	@Autowired
	private CustomerRepository customerRepository;
	
	public ServiceResult findAll() {
		ServiceResult serviceResult = new ServiceResult();
		serviceResult.setData(customerRepository.findAll());
		return serviceResult;
	}
	
	public ServiceResult findById(int id) {
		ServiceResult serviceResult = new ServiceResult();
		Customer customer = customerRepository.findById(id).get();
		serviceResult.setData(customer);
		return serviceResult;
	}
	
	public ServiceResult create(Customer customer) {
		ServiceResult serviceResult = new ServiceResult();
		serviceResult.setData(customerRepository.save(customer));
		return serviceResult;
	}
	
	public ServiceResult update(Customer customer) {
		ServiceResult serviceResult = new ServiceResult();
		if(!customerRepository.findById(customer.getId()).isPresent()) {
			serviceResult.setStatus(Status.FAILED);
			serviceResult.setMessage("Customer not found");
		}
		else {
			serviceResult.setData(customerRepository.save(customer));
		}
		return serviceResult;
	}
	
	public ServiceResult delete(int id) {
		ServiceResult serviceResult = new ServiceResult();
		
		Customer customer = customerRepository.findById(id).get();
		if(customer == null) {
			serviceResult.setStatus(Status.FAILED);
			serviceResult.setMessage("Customer not found");
		}
		else {
		    customerRepository.delete(customer);
		    serviceResult.setMessage("delete successful");
		}
		return serviceResult;
	}
}
