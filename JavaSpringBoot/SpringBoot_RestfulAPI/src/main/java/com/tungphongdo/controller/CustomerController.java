package com.tungphongdo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tungphongdo.entities.Customer;
import com.tungphongdo.service.CustomerService;
import com.tungphongdo.service.ServiceResult;

@RestController
@RequestMapping(value = "/api/v1")
public class CustomerController {
	
	@Autowired
	private CustomerService customerService;
	
	//Get all customer
	@GetMapping("/customer")
	public ResponseEntity<ServiceResult> findAllCustomer(){
		ResponseEntity<ServiceResult> responseEntity = 
				new ResponseEntity<ServiceResult>(customerService.findAll(), HttpStatus.OK);
		return responseEntity;
	}
	
	//Get customer by Id
	@GetMapping("/customer/{id}")
	public ResponseEntity<ServiceResult> findById(@PathVariable int id){
		return new ResponseEntity<ServiceResult>(customerService.findById(id), HttpStatus.OK);
	}
	
	//create new customer
	@PostMapping("/customer")
	public ResponseEntity<ServiceResult> createCustomer(@RequestBody Customer customer){
		return new ResponseEntity<ServiceResult>(customerService.create(customer), HttpStatus.OK);
	}
	
	//Update customer
	@PutMapping("/customer")
	public ResponseEntity<ServiceResult> updateCustomer(@RequestBody Customer customer){
		return new ResponseEntity<ServiceResult>(customerService.update(customer), HttpStatus.OK);
	}
	
	//Delete customer
	@DeleteMapping("/customer")
	public ResponseEntity<ServiceResult> deleteCustomer(@RequestBody DeleteCustomerRequest request){
		return new ResponseEntity<ServiceResult>(customerService.delete(request.getId()), HttpStatus.OK);
	}

}
