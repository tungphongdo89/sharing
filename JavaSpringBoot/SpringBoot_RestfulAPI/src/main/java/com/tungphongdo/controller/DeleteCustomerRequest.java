package com.tungphongdo.controller;

public class DeleteCustomerRequest {
	private int id;

	public DeleteCustomerRequest(int id) {
		super();
		this.id = id;
	}

	public DeleteCustomerRequest() {
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	

}
