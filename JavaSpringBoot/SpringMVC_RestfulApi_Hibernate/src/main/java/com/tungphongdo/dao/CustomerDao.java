package com.tungphongdo.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.tungphongdo.entity.CustomerEntity;

@Repository
@Transactional(rollbackFor = Exception.class)
public class CustomerDao {
	@Autowired
	private SessionFactory sessionFactory;
	
	public List<CustomerEntity> findAll(){
		Session session = this.sessionFactory.getCurrentSession();
		List<CustomerEntity> customerEntities = session.createQuery("from CustomerEntity", CustomerEntity.class).getResultList();
		return customerEntities;
	}
	
	public CustomerEntity findById(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		CustomerEntity customerEntity = session.get(CustomerEntity.class, id);
		return customerEntity;
	}
	
	public void save(CustomerEntity customer) {
		Session session = this.sessionFactory.getCurrentSession();
		session.save(customer);
	}
	
	public void update(CustomerEntity customer) {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(customer);
	}
	
	public void delete(CustomerEntity customer) {
		Session session = this.sessionFactory.getCurrentSession();
		session.remove(customer);
	}

}
