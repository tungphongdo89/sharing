create database manage_user;
use manage_user;

create table user_type(
	id int not null auto_increment,
    code nvarchar(50) not null,
    typeName nvarchar(255),
    primary key(id)
);
create table user(
	id int not null auto_increment,
    code nvarchar(50) not null,
    typeUserCode nvarchar(50) not null,
    firstName nvarchar(255),
    lastName nvarchar(255),
    age int ,
    email nvarchar(255),
    address nvarchar(255),
    primary key(id)
);
insert into user_type (code, typeName) values('type01', 'vip');
insert into user_type (code, typeName) values('type02', 'medium');
insert into user_type (code, typeName) values('type03', 'new');

insert into user (code, typeUserCode, firstName, lastName, age, email, address) 
		   values('u2020918020202', 'type01', 'tung', 'hoang', 22, 'tungphongdo89@gmail.com', 'thanh hoa');
insert into user (code, typeUserCode, firstName, lastName, age, email, address) 
		   values('u2020918020201', 'type02', 'john', 'wick', 22, 'john@gmail.com', 'thanh hoa');
insert into user (code, typeUserCode, firstName, lastName, age, email, address) 
		   values('u2020918020204', 'type03', 'luong', 'tran', 22, 'luong@gmail.com', 'thanh hoa');
insert into user (code, typeUserCode, firstName, lastName, age, email, address) 
		   values('u2020918020203', 'type01', 'ha', 'nam', 22, 'ha89@gmail.com', 'thanh hoa');
insert into user (code, typeUserCode, firstName, lastName, age, email, address) 
		   values('u2020918020206', 'type02', 'le', 'lee', 22, 'le89@gmail.com', 'thanh hoa');
insert into user (code, typeUserCode, firstName, lastName, age, email, address) 
		   values('u2020918020208', 'type03', 'ronaldo', 'cr', 22, 'ronaldo@gmail.com', 'thanh hoa');
insert into user (code, typeUserCode, firstName, lastName, age, email, address) 
		   values('u2020918020205', 'type01', 'thom', 'tran', 22, 'thom@gmail.com', 'thanh hoa');
insert into user (code, typeUserCode, firstName, lastName, age, email, address) 
		   values('u2020918020207', 'type02', 'vanh', 'ngan', 22, 'vanh@gmail.com', 'thanh hoa');
           
use manage_user;
-- search user
select 
	u.code, 
    u.firstName, 
    u.lastName, 
    u.age, 
    u.email, 
    u.address, 
    ut.typeName
from user u join user_type ut on u.typeUserCode = ut.code
where 1 = 1
and u.code like '%t%' 
-- and  u.typeUserCode='type01' 
-- and lower (u.firstName) like lower ('%a%' )
-- and lower (ut.typeName) like lower ('%v%');

           
		
