package tungphongdo.com.manageuser.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import tungphongdo.com.manageuser.dto.SearchDTO;
import tungphongdo.com.manageuser.dto.ServiceResult;
import tungphongdo.com.manageuser.dto.UserDTO;
import tungphongdo.com.manageuser.dto.UserTypeDTO;
import tungphongdo.com.manageuser.service.serviceImpl.UserServiceImpl;

import javax.validation.Valid;
import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserServiceImpl userServiceImpl;


    @PostMapping("/search-user")
    public ResponseEntity<ServiceResult<UserDTO>> grid(@RequestBody SearchDTO<UserDTO> userSeachDTO) {
        return ResponseEntity.ok(userServiceImpl.searchUser(userSeachDTO));
    }

    @PostMapping("/dropdown-user-type")
    public ResponseEntity<List<UserTypeDTO>> dropdownUserType() {
        return ResponseEntity.ok(userServiceImpl.findAllUserTypes());
    }

    @PostMapping("/create-user")
    public ResponseEntity<ServiceResult<UserDTO>> createUser(@Valid  @RequestBody UserDTO userDTO) {
        return ResponseEntity.ok(userServiceImpl.createUser(userDTO));
    }

    @PostMapping("/write-excel-file")
    public ResponseEntity<ServiceResult<UserDTO>> writeExcelFile(@RequestBody List<UserDTO> userDTOs) throws IOException {
        return ResponseEntity.ok(userServiceImpl.writeExcelFile(userDTOs));
    }

    @PostMapping(value = "/import-users-from-excel", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<ServiceResult<UserDTO>> importUsersFromExcel(@RequestParam("file") MultipartFile file){
        return ResponseEntity.ok(userServiceImpl.importUsersFromExcel(file));
    }


}
