package tungphongdo.com.manageuser.service.serviceImpl;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import tungphongdo.com.manageuser.dto.SearchDTO;
import tungphongdo.com.manageuser.dto.ServiceResult;
import tungphongdo.com.manageuser.dto.UserDTO;
import tungphongdo.com.manageuser.dto.UserTypeDTO;
import tungphongdo.com.manageuser.entity.UserEntity;
import tungphongdo.com.manageuser.entity.UserType;
import tungphongdo.com.manageuser.repository.UserRepository;
import tungphongdo.com.manageuser.repository.UserTypeRepository;
import tungphongdo.com.manageuser.service.UserService;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class UserServiceImpl implements UserService {

    private static final String ERROR_GET_DATA = "Đã xảy ra lỗi trong quá trình lấy dữ liệu";
    private static final String ERROR_CREATE_USER = "Đã xảy ra lỗi trong quá trình thêm mới khách hàng";
    private static final String CREATE_USER_SUCCESS = "Thêm mới khách hàng thành công";
    private static final String IMPORT_USER_FILE_SUCCESS = "Import thành công";
    private static final String IMPORT_USER_FILE_FAILED = "Đã xảy ra lỗi trong quá trình import";

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ResourceBundleMessageSource resourceBundleMessageSource;

    @Autowired
    private UserTypeRepository userTypeRepository;

    @Override
    public ServiceResult<UserDTO> searchUser(SearchDTO<UserDTO> userSearchDTO) {
        ServiceResult<UserDTO> userServiceResult = new ServiceResult<>();
        Pageable pageable = PageRequest.of(userSearchDTO.getPage(), userSearchDTO.getPageSize());

        try{
            Page<Object[]> listUser = userRepository.searchUser( userSearchDTO.getData().getCode(),
                    userSearchDTO.getData().getTypeUserCode(),
                    userSearchDTO.getData().getFullName(),
                    userSearchDTO.getData().getTypeName(),
                    pageable);

//            System.out.println("tổng số bản ghi: " + listUser.getTotalElements());  // tổng số bản ghi của các page
//            System.out.println("tổng số page: " + listUser.getTotalPages());    // tổng số page
//            System.out.println("mảng các object: " + listUser.getContent());      // mảng các object
//            System.out.println("sắp xếp: " + listUser.getPageable().getSort());   // sắp xếp
//            System.out.println("trang hiện tại: " + listUser.getPageable().getPageNumber());  // trang hiện tại
//            System.out.println("số lượng bản ghi muốn lấy trong trang hiện tại: " + listUser.getPageable().getPageSize());   //số lượng bản ghi trong trang hiện tại
//            System.out.println("số lượng bản ghi hiện có trong trang hiện tại: " + listUser.getContent().size());


            userServiceResult.setTotalElements(listUser.getTotalElements());
            userServiceResult.setTotalPages(listUser.getTotalPages());
            userServiceResult.setCurrentPage(listUser.getPageable().getPageNumber());
            userServiceResult.setCurrentPageSize(listUser.getContent().size());

            List<UserDTO> listUsers = new ArrayList<>();
    for (Object[] objs : listUser.getContent()) {
                UserDTO userDTO = new UserDTO();
                userDTO.setCode(objs[0].toString());
                userDTO.setFullName(objs[1].toString() + " " + objs[2].toString());
                userDTO.setAge(Long.parseLong(objs[3].toString()));
                userDTO.setEmail(objs[4].toString());
                userDTO.setAddress(objs[5].toString());
                userDTO.setTypeName(objs[6].toString());
                listUsers.add(userDTO);
            }

            userServiceResult.setData(listUsers);
            userServiceResult.setStatus(ServiceResult.Status.SUCCESS);
            userServiceResult.setMessage(
                    resourceBundleMessageSource.getMessage("get.data.success", null, LocaleContextHolder.getLocale()));

        }
        catch (Exception e){
            userServiceResult.setMessage(ERROR_GET_DATA);
            userServiceResult.setStatus(ServiceResult.Status.FAILED);
        }

        return userServiceResult;

    }

    @Override
    public List<UserTypeDTO> findAllUserTypes() {
        List<UserType> listUserTypes = userTypeRepository.findAll();

        //cách 1 dùng java 7
//        List<UserTypeDTO> listUserTypeDTOS = new ArrayList<>();
//        for (UserType userType:listUserTypes) {
//            UserTypeDTO userTypeDTO = userType.toModel();
//            listUserTypeDTOS.add(userTypeDTO);
//        }

        //cách 2 dùng extra
//        List<UserTypeDTO> listUserTypeDTOS =
//                listUserTypes.stream().map(userType -> userType.toModel()).collect(Collectors.toList());

        //cách 3 dùng toán tử ::
        List<UserTypeDTO> listUserTypeDTOS = listUserTypes.stream().map(UserType::toModel).collect(Collectors.toList());


        return listUserTypeDTOS;
    }

    @Override
    public ServiceResult<UserDTO> createUser(UserDTO userDTO) {

        ServiceResult<UserDTO> createUserResult = new ServiceResult<>();

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("ddMMyyyyHHmmss");
        String code = simpleDateFormat.format(new Date());
        UserEntity user = userDTO.toModel();
        user.setCode("u"+code);
        try {
            userRepository.save(user);
            createUserResult.setStatus(ServiceResult.Status.SUCCESS);
            createUserResult.setMessage(CREATE_USER_SUCCESS);
        }
        catch (Exception ex){
            createUserResult.setStatus(ServiceResult.Status.FAILED);
            createUserResult.setMessage(ERROR_CREATE_USER);
        }

        return createUserResult;
    }

    @Override
    public ServiceResult<UserDTO> writeExcelFile(List<UserDTO> userDTOs) throws IOException {
        ServiceResult<UserDTO> result = new ServiceResult<>();

        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("Danh sách khách hàng");
        int rowNum = 0;
        Row firstRow = sheet.createRow(rowNum++);
        Cell firstCell = firstRow.createCell(0);
        firstCell.setCellValue("Danh sách khách hàng");

        for (UserDTO userDTO :userDTOs) {
            Row row = sheet.createRow(rowNum++);
            Cell cellCode = row.createCell(0);
            cellCode.setCellValue(userDTO.getCode());

            Cell cellFullName = row.createCell(1);
            cellFullName.setCellValue(userDTO.getFullName());

            Cell cellAge = row.createCell(2);
            cellAge.setCellValue(userDTO.getAge());

            Cell cellEmail = row.createCell(3);
            cellEmail.setCellValue(userDTO.getEmail());

            Cell cellAddress = row.createCell(4);
            cellAddress.setCellValue(userDTO.getAddress());

            Cell cellTypeName = row.createCell(5);
            cellTypeName.setCellValue(userDTO.getTypeName());
        }

        try {
            FileOutputStream outputStream = new FileOutputStream("D:/Demo-ApachePOI-Excel.xlsx");
            workbook.write(outputStream);
            result.setStatus(ServiceResult.Status.SUCCESS);
            result.setMessage("Ghi file excel thành công");
        } catch (FileNotFoundException e) {
            result.setStatus(ServiceResult.Status.FAILED);
            result.setMessage("Không tìm thấy file");
        } catch (IOException e) {
            result.setStatus(ServiceResult.Status.FAILED);
            result.setMessage("Đã có lỗi xảy ra khi lưu file");
        }
        finally {
            workbook.close();
        }

        return result;
    }

    @Override
    public ServiceResult<UserDTO> importUsersFromExcel(MultipartFile file) {
        ServiceResult<UserDTO> result = new ServiceResult<>();
        List<UserDTO> listUsers = new ArrayList<>();
        try {
            Path tempDir = Files.createTempDirectory("");
            File tempFile = tempDir.resolve(file.getOriginalFilename()).toFile();
            file.transferTo(tempFile);

//            FileInputStream userExcelFile = new FileInputStream(new File("src/main/resources/static/files/ListUsers.xlsx"));
            FileInputStream userExcelFile = new FileInputStream(tempFile);
            Workbook workbook = new XSSFWorkbook(userExcelFile);
            Sheet datatypeSheet = workbook.getSheetAt(0);
            DataFormatter fmt = new DataFormatter();

            Iterator<Row> iterator = datatypeSheet.iterator();
            Row firstRow = iterator.next();
            Cell firstCell = firstRow.getCell(0);



            while (iterator.hasNext()) {

                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("ddMMyyyyHHmmss");
                String code = simpleDateFormat.format(new Date());

                Row currentRow = iterator.next();
                UserDTO userDTO = new UserDTO();
                userDTO.setCode("u" + code);
                userDTO.setTypeUserCode(currentRow.getCell(1).getStringCellValue());
                userDTO.setFirstName(currentRow.getCell(2).getStringCellValue());
                userDTO.setLastName(currentRow.getCell(3).getStringCellValue());
                userDTO.setAge(Long.parseLong(fmt.formatCellValue(currentRow.getCell(4))));
                userDTO.setEmail(currentRow.getCell(5).getStringCellValue());
                userDTO.setAddress(currentRow.getCell(6).getStringCellValue());
                listUsers.add(userDTO);
            }

//            listUsers.forEach(user -> userRepository.save(user.toModel()));

            result.setStatus(ServiceResult.Status.SUCCESS);
            result.setMessage(IMPORT_USER_FILE_SUCCESS);


            for (UserDTO userDTO : listUsers) {
                System.out.println("--------------------------");
                System.out.println(userDTO.getCode());
                System.out.println(userDTO.getTypeUserCode());
                System.out.println(userDTO.getFirstName());
                System.out.println(userDTO.getLastName());
                System.out.println(userDTO.getAge());
                System.out.println(userDTO.getEmail());
                System.out.println(userDTO.getAddress());
            }
        }
        catch (FileNotFoundException e){
            e.printStackTrace();
            result.setStatus(ServiceResult.Status.FAILED);
            result.setMessage(IMPORT_USER_FILE_FAILED);
        }
        catch (IOException e) {
            e.printStackTrace();
            result.setStatus(ServiceResult.Status.FAILED);
            result.setMessage(IMPORT_USER_FILE_FAILED);
        }

        return result;
    }

}
