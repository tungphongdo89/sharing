package tungphongdo.com.manageuser.entity;

import tungphongdo.com.manageuser.dto.UserTypeDTO;

import javax.persistence.*;

@Entity
@Table(name = "user_type")
public class UserType {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "code")
    private String code;

    @Column(name = "type_name")
    private String typeName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public UserTypeDTO toModel(){
        UserTypeDTO userTypeDTO = new UserTypeDTO();
        userTypeDTO.setId(this.id);
        userTypeDTO.setCode(this.code);
        userTypeDTO.setTypeName(this.typeName);
        return userTypeDTO;
    }
}
