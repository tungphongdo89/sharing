package tungphongdo.com.manageuser.service;

import org.springframework.web.multipart.MultipartFile;
import tungphongdo.com.manageuser.dto.SearchDTO;
import tungphongdo.com.manageuser.dto.ServiceResult;
import tungphongdo.com.manageuser.dto.UserDTO;
import tungphongdo.com.manageuser.dto.UserTypeDTO;

import java.io.IOException;
import java.util.List;

public interface UserService {

    ServiceResult<UserDTO> searchUser(SearchDTO<UserDTO> userSearchDTO);

    List<UserTypeDTO> findAllUserTypes();

    ServiceResult<UserDTO> createUser(UserDTO userDTO);

    ServiceResult<UserDTO> writeExcelFile(List<UserDTO> userDTOs) throws IOException;

    ServiceResult<UserDTO> importUsersFromExcel(MultipartFile file);
}
