package tungphongdo.com.manageuser.dto;

import tungphongdo.com.manageuser.entity.UserEntity;

import javax.validation.constraints.NotEmpty;

public class UserDTO {

    private Long id;

    private String code;

    @NotEmpty(message = "Mã loại khách hàng không được bỏ trống")
    private String typeUserCode;

    @NotEmpty(message = "Frist name khách hàng không được bỏ trống")
    private String firstName;

    @NotEmpty(message = "last name khách hàng không được bỏ trống")
    private String lastName;

    private Long age;

    @NotEmpty(message = "Email khách hàng không được bỏ trống")
    private String email;

    private String address;

    private String fullName;

    private String typeName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getTypeUserCode() {
        return typeUserCode;
    }

    public void setTypeUserCode(String typeUserCode) {
        this.typeUserCode = typeUserCode;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Long getAge() {
        return age;
    }

    public void setAge(Long age) {
        this.age = age;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public UserEntity toModel(){
        UserEntity user = new UserEntity();
        user.setCode(this.getCode());
        user.setTypeUserCode(this.getTypeUserCode());
        user.setFirstName(this.getFirstName());
        user.setLastName(this.getLastName());
        user.setAge(this.getAge());
        user.setEmail(this.getEmail());
        user.setAddress(this.getAddress());

        return user;
    }
}
