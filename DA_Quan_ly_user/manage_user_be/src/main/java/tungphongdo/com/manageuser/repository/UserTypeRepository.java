package tungphongdo.com.manageuser.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tungphongdo.com.manageuser.entity.UserType;

@Repository
public interface UserTypeRepository extends JpaRepository<UserType, Long> {



}
