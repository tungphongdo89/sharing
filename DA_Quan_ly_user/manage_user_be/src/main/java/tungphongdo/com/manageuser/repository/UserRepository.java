package tungphongdo.com.manageuser.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import tungphongdo.com.manageuser.entity.UserEntity;

@Repository
public interface UserRepository extends JpaRepository<UserEntity, Long> {

    @Query(value = "select \n" +
            "\tu.code, \n" +
            "    u.first_name, \n" +
            "    u.last_name, \n" +
            "    u.age, \n" +
            "    u.email, \n" +
            "    u.address, \n" +
            "    ut.type_name\n" +
            "from user u join user_type ut on u.type_user_code = ut.code\n" +
            "where 1 = 1 and \n" +
            "(:code is null or lower (u.code) like lower (concat('%',:code , '%')) ) \n" +
            "and (:typeUserCode is null or u.type_user_code like :typeUserCode ) \n" +
            "and ( (:fullName is null or lower (u.first_name) like lower (concat('%', :fullName, '%') )) \n" +
            "or (:fullName is null or lower (u.first_name) like lower (concat('%', :fullName, '%'))) )\n" +
            "and (:typeName is null or lower (ut.type_name) like  lower(concat('%',:typeName , '%')))",

            countQuery = " select count(1)" +
                    "from user u join user_type ut on u.type_user_code = ut.code\n" +
                    "where 1 = 1 and \n" +
                    "(:code is null or lower (u.code) like lower (concat('%',:code , '%')) ) \n" +
                    "and (:typeUserCode is null or u.type_user_code like :typeUserCode ) \n" +
                    "and ( (:fullName is null or lower (u.first_name) like lower (concat('%', :fullName, '%') )) \n" +
                    "or (:fullName is null or lower (u.first_name) like lower (concat('%', :fullName, '%'))) )\n" +
                    "and (:typeName is null or lower (ut.type_name) like  lower(concat('%',:typeName , '%')))",
            nativeQuery = true)
    Page<Object[]> searchUser(@Param("code") String code,
                              @Param("typeUserCode") String typeUserCode,
                              @Param("fullName") String fullName,
                              @Param("typeName") String typeName, Pageable pageable);



}
