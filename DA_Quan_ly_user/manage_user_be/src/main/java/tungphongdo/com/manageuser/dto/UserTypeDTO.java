package tungphongdo.com.manageuser.dto;

import tungphongdo.com.manageuser.entity.UserType;

public class UserTypeDTO {

    private Long id;

    private String code;

    private String typeName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public UserType toModel(){
        UserType userType = new UserType();
        userType.setId(this.id);
        userType.setTypeName(this.code);
        userType.setTypeName(this.typeName);
        return userType;
    }
}
