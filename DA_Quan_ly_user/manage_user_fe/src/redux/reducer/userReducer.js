import * as config from '../common/const';
import {toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const initState = {
    listUser: [],
    totalElements: 0,
    totalPages: 0,
    currentPage: 0,
    currentPageSize: 0,

    listUserType: [],
    activePage: 0
}

const userReducer = (state = initState, action) =>{
    debugger
    switch(action.type){

        //search user
        case config.SEARCH_USER: {
            const data = action.payload;
            return state;
        }
        case config.SEARCH_USER_SUCCESS: {
            const data = action.payload.data;
            state = {...state,
                listUser: data.data,
                totalElements: data.totalElements,
                totalPages: data.totalPages,
                currentPage: data.currentPage,
                currentPageSize: data.currentPageSize,
                activePage: data.currentPage
            };
            toast.success(data.message)
            return state;
        }
        case config.SEARCH_USER_FAILED: {
            const data = action.payload.data;
            toast.error(data.message)
            return state;
        }

        //dropdown user type
        case config.DROPDOWN_USER_TYPE: {
            debugger
            return state;
        }
        case config.DROPDOWN_USER_TYPE_SUCCESS: {
            debugger
            const data = action.payload.data;
            state = {...state, listUserType: data}
            return state;
        }

        //search user
        case config.CREATE_USER: {
            const data = action.payload;
            return state;
        }
        case config.CREATE_USER_SUCCESS: {
            const data = action.payload.data;
            state = {...state};
            toast.success(data.message)
            return state;
        }
        case config.CREATE_USER_FAILED: {
            const data = action.payload.data;
            toast.error(data.message)
            return state;
        }

        //import excel file users
        case config.IMPORT_EXCEL_FILE_USERS: {
            debugger
            return state;
        }
        case config.IMPORT_EXCEL_FILE_USERS_SUCCESS: {
            debugger
            const data = action.payload.data;
            state = {...state};
            toast.success(data.message)
            return state;
        }
        case config.IMPORT_EXCEL_FILE_USERS_FAILED: {
            debugger
            const data = action.payload.data;
            toast.error(data.message)
            return state;
        }

        default:
            return state;
    }
}

export default userReducer