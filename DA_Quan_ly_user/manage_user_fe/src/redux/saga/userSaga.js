import {call, delay, put, takeEvery} from "redux-saga/effects";
import * as config from '../common/const';
import userAction from '../action/userAction';
import userApi from '../api/userApi';

function* searchUser({payload}){
    debugger
    const data = payload;
    const resp = yield call(userApi.searchUser, data);
    if("SUCCESS" === resp.status){
        yield put(userAction.searchUserSuccess(resp))
    }
    else{
        yield put(userAction.searchUserFailed(resp))
    }
}

function* dropdownUserType(){
    debugger
    const resp = yield call(userApi.dropdownUserType);
    yield put(userAction.dropdownUserTypeSuccess(resp))

}

function* createUser({payload}){
    debugger
    const data = payload;
    const resp = yield call(userApi.createUser, data);
    if("SUCCESS" === resp.status){
        yield put(userAction.createUserSuccess(resp))
    }
    else{
        yield put(userAction.createUserFailed(resp))
    }
}

function* importExcelDataUsers({payload}){
    debugger
    const data = payload;
    const resp = yield call(userApi.importExcelDataUsers, data);
    if("SUCCESS" === resp.status){
        yield put(userAction.importExcelFileUsersSuccess(resp))
    }
    else{
        yield put(userAction.importExcelFileUsersFailed(resp))
    }
}

function* userSaga(){
    yield takeEvery(config.SEARCH_USER, searchUser)
    yield takeEvery(config.DROPDOWN_USER_TYPE, dropdownUserType)
    yield takeEvery(config.CREATE_USER, createUser)
    yield takeEvery(config.IMPORT_EXCEL_FILE_USERS, importExcelDataUsers)
}

export default userSaga;

