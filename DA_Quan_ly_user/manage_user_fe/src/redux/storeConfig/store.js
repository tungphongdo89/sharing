import {applyMiddleware, compose, createStore} from "redux"
import combineReducers from "../reducer/rootReducer"
import rootSaga from "../saga/rootSaga";
import createSagaMiddleware from 'redux-saga';

const sagaMiddleware = createSagaMiddleware();

const middlewares = [ sagaMiddleware];

const persistConfig = {
    key: 'root'
//  storage: storage
};

const pReducer = combineReducers


const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose
const store = createStore(
    pReducer,
    {},
    composeEnhancers(applyMiddleware(...middlewares))
)

export default store;
sagaMiddleware.run(rootSaga)