import * as config from '../common/const';

const userAction = {
    searchUser,
    searchUserSuccess,
    searchUserFailed,

    dropdownUserType,
    dropdownUserTypeSuccess,

    createUser,
    createUserSuccess,
    createUserFailed,

    importExcelFileUsers,
    importExcelFileUsersSuccess,
    importExcelFileUsersFailed

}

//get forgot pass
function searchUser(data){
    debugger
    return{
        type: config.SEARCH_USER,
        payload: {data}
    }
}
function searchUserSuccess(data){
    debugger
    return{
        type: config.SEARCH_USER_SUCCESS,
        payload: {data}
    }
}
function searchUserFailed(data){
    return{
        type: config.SEARCH_USER_FAILED,
        payload: {data}
    }
}

//dropdown user type
function dropdownUserType(){
    debugger
    return{
        type: config.DROPDOWN_USER_TYPE
    }
}
function dropdownUserTypeSuccess(data){
    debugger
    return{
        type: config.DROPDOWN_USER_TYPE_SUCCESS,
        payload: {data}
    }
}

//create user
function createUser(data){
    debugger
    return{
        type: config.CREATE_USER,
        payload: {data}
    }
}
function createUserSuccess(data){
    debugger
    return{
        type: config.CREATE_USER_SUCCESS,
        payload: {data}
    }
}
function createUserFailed(data){
    debugger
    return{
        type: config.CREATE_USER_FAILED,
        payload: {data}
    }
}

//import excel file users
function importExcelFileUsers(data){
    debugger
    return{
        type: config.IMPORT_EXCEL_FILE_USERS,
        payload: {data}
    }
}
function importExcelFileUsersSuccess(data){
    debugger
    return{
        type: config.IMPORT_EXCEL_FILE_USERS_SUCCESS,
        payload: {data}
    }
}
function importExcelFileUsersFailed(data){
    debugger
    return{
        type: config.IMPORT_EXCEL_FILE_USERS_FAILED,
        payload: {data}
    }
}

export default userAction;