    import React from 'react';

const userApi = {
    searchUser,
    dropdownUserType,
    createUser,
    importExcelDataUsers
}

async function searchUser(data){
    debugger
    return await fetch(
        '/user/search-user',
        {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data.data)
        }
    )
        .then(response => response.json())
        .then(body =>  body)
        .catch((error) => { throw error })
}

async function dropdownUserType(){
    debugger
    return await fetch(
        '/user/dropdown-user-type',
        {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
            // body: JSON.stringify()
        }
    )
        .then(response => response.json())
        .then(body =>  body)
        .catch((error) => { throw error })
}

async function createUser(data){
    debugger
    return await fetch(
        '/user/create-user',
        {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data.data)
        }
    )
        .then(response => response.json())
        .then(body =>  body)
        .catch((error) => { throw error })
}

async function importExcelDataUsers(data){
    debugger
    return await fetch(
        '/user/import-users-from-excel',
        {
            method: 'POST',
            body: data.data
        }
    )
        .then(response => response.json())
        .then(body =>  body)
        .catch((error) => { throw error })
}

export default userApi;