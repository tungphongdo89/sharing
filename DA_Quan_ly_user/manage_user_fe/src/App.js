import logo from './logo.svg';
import React from 'react';
import './App.css';
import RootRouter from './route'

function App() {
  return (
    <RootRouter />
  );
}

export default App;
