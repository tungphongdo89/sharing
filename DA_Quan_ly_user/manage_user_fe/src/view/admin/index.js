import React, {useState, useEffect} from 'react';
import {Container, Row, Col, Table, Button} from 'reactstrap';
import {connect} from "react-redux";
import {bindActionCreators, compose} from "redux";
import userAction from '../../redux/action/userAction';
import { useForm } from "react-hook-form";
import ModalCreateUser from '../modal';

import ReactHTMLTableToExcel from  'react-html-table-to-excel';

import Paging from '../pagination'

function ManageUser(Props){

    const [typeUserCodeDropDown, setTypeUserCodeDropDown] = useState("");
    const [excelFile, setExcelFile] = useState();
    const {register, handleSubmit} = useForm();

    const onChangeDropdown =(e)=> {
        setTypeUserCodeDropDown(e.target.value);
    }

    useEffect(() => {
        const {searchUser, dropdownUserType} = Props.userAction;
        let info = {};
        info.data = {};
        info.page = 0;
        info.pageSize = 3;
        searchUser(info);

        dropdownUserType();
    }, [])


    const onSubmit = (formData) => {
        const {searchUser} = Props.userAction;
        let info = {};

        // info.data = {...formData};
        let data = {};
        data.code = formData.code === "" ? null : formData.code;
        // data.typeUserCode = formData.typeUserCode === "" ? null : formData.typeUserCode;
        data.typeUserCode = typeUserCodeDropDown === "" ? null : typeUserCodeDropDown
        data.fullName = formData.fullName === "" ? null : formData.fullName;
        data.typeName = formData.typeName === "" ? null : formData.typeName;

        info.data = data;
        info.page = Props.currentPage;
        info.pageSize = 3;
        searchUser(info);
    }

    const changePage =(newPage)=> {
        const {searchUser} = Props.userAction;
        let info = {};
        let data = {};
        info.data = data;
        info.page = newPage;
        info.pageSize = 3;
        searchUser(info);
    }

    const createUser =(data)=> {
        console.log(data);
        const {createUser} = Props.userAction;
        createUser(data);
    }

    const getExcelFile =(e)=> {
        console.log(e.target.files[0]);
        setExcelFile(e.target.files[0]);
    }

    const importExcelFile =()=> {
        const {importExcelFileUsers} = Props.userAction;

        let formData = new FormData();
        formData.append("file", excelFile);
        console.log(formData);

        importExcelFileUsers(formData)
    }

    return(
        <Container>
            <form onSubmit={handleSubmit(onSubmit)}>
                <Row>
                    <Col md={6}>
                        <label>Mã khách hàng</label>
                        <input
                            {...register("code", {
                                maxLength : {
                                    value: 20,
                                }
                            })}
                        />
                    </Col>
                    <Col md={6}>
                        <label>Mã loại khách hàng</label>
                        {/*<input type="select" value={}*/}
                            {/*{...register("typeUserCode", {*/}
                                {/*maxLength : {*/}
                                    {/*value: 20,*/}
                                {/*}*/}
                            {/*})}*/}
                        {/*/>*/}
                        <select
                            {...register("typeUserCode", {
                                maxLength : {
                                    value: 20,
                                }
                            })}
                            onChange={(e) => onChangeDropdown(e)}
                        >
                            <option value="">All</option>
                            {Props.listUserType.length > 0 ?
                                Props.listUserType.map((userType) =>
                                    <option value={userType.code}>{userType.typeName}</option>
                                ) : ""
                            }

                        </select>
                    </Col>
                    <Col md={6}>
                        <label>Tên khách hàng</label>
                        <input
                            {...register("fullName")}
                        />
                    </Col>
                    <Col md={6}>
                        <label>Tên loại khách hàng</label>
                        <input
                            {...register("typeName")}
                        />
                    </Col>
                    <Col md={12}>
                        <button type="submit">Tìm</button>
                    </Col>
                </Row>
            </form>

            {/*export table*/}
            <Row>
                <div>
                    <ModalCreateUser listUserType={Props.listUserType} createUser={createUser}/>
                    <ReactHTMLTableToExcel
                        id="test-table-xls-button"
                        className="download-table-xls-button"
                        table="listUserTable"
                        filename="tablexls"
                        sheet="tablexls"
                        buttonText="Download as XLS"/>
                </div>
            </Row>
            {/*end export table*/}

            {/*import data by excel*/}
            <Row>
                <div>
                    <input type="file" name="file" onChange={(e) => getExcelFile(e)}/>
                    <button onClick={importExcelFile}>Import file</button>
                </div>

            </Row>
            {/*end import data by excel*/}

            <Row style={{marginTop: '5%'}}>
                <Table id="listUserTable">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Mã khách hàng</th>
                        <th>Họ tên</th>
                        <th>Tuổi</th>
                        <th>Email</th>
                        <th>Address</th>
                        <th>Loại KH</th>
                        <th>Thao tác</th>
                    </tr>
                    </thead>
                    <tbody>
                    {Props.listUser.length > 0 ?
                        Props.listUser.map((u) =>
                            <tr>
                                <th scope="row">1</th>
                                <td>{u.code}</td>
                                <td>{u.fullName}</td>
                                <td>{u.age}</td>
                                <td>{u.email}</td>
                                <td>{u.address}</td>
                                <td>{u.typeName}</td>
                                <td>
                                    <Button color="success">Sửa</Button>
                                    <Button color="danger">Xóa</Button>
                                </td>
                            </tr>
                        )
                        : <p>No data found</p>
                    }
                    </tbody>
                </Table>
            </Row>
            <Row>
                <Paging
                    totalElements={Props.totalElements}
                    totalPages={Props.totalPages}
                    currentPage={Props.currentPage}
                    currentPageSize={Props.currentPageSize}
                    activePage={Props.activePage}
                    changePage={changePage}
                />
            </Row>
        </Container>
    );

}

const mapStateToProps = (state) => {
    return {
        listUser: state.userReducer.listUser,
        totalElements: state.userReducer.totalElements,
        totalPages: state.userReducer.totalPages,
        currentPage: state.userReducer.currentPage,
        currentPageSize: state.userReducer.currentPageSize,
        listUserType: state.userReducer.listUserType,
        activePage: state.userReducer.activePage
    }
}
const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        userAction : bindActionCreators(userAction , dispatch)
    }
}
const withConnect = connect(mapStateToProps, mapDispatchToProps)

export default compose(withConnect)(ManageUser);