import React, { useState } from 'react';
import {Button, Modal, ModalHeader, ModalBody, ModalFooter, Row, Col} from 'reactstrap';
import {useForm} from "react-hook-form";

const ModalCreateUser = (Props) => {
    const {
        className,
        createUser
    } = Props;

    const [modal, setModal] = useState(false);
    const [dropDown, setDropDown] = useState("");

    const toggle = () => setModal(!modal);

    const {register, handleSubmit, setError, formState: { errors } } = useForm();


    const onChangeDropdown =(e)=> {
        debugger
        setDropDown(e.target.value);
    }

    const onSubmitCreate =(formData)=> {
        debugger
        createUser(formData);
    }



    return (
        <div>
            <Button color="danger" onClick={toggle}>Thêm</Button>

                <Modal isOpen={modal} toggle={toggle} className={className}>
                    <ModalHeader toggle={toggle}>Thêm khách hàng</ModalHeader>
                    <form onSubmit={handleSubmit(onSubmitCreate)}>
                    <ModalBody>

                            <Row>
                                <Col md={4}>
                                    <label>Mã loại khách hàng</label>
                                </Col>
                                <Col md={8}>
                                    <select
                                        {...register("typeUserCode", {
                                            maxLength : {
                                                value: 20,
                                            }
                                        })}
                                        onChange={(e) => onChangeDropdown(e)}
                                    >
                                        {Props.listUserType.length > 0 ?
                                            Props.listUserType.map((userType) =>
                                                <option value={userType.code}>{userType.typeName}</option>
                                            ) : ""
                                        }

                                    </select>
                                    {errors.typeUserCode && <p style={{color: 'red'}}>{errors.typeUserCode.message}</p>}
                                </Col>
                            </Row>
                            <Row>
                                <Col md={4}>
                                    <label>Họ</label>
                                </Col>
                                <Col md={8}>
                                    <input
                                        {...register("lastName", {
                                            required: 'Họ không được để trống',
                                            minLength: {
                                                value: 4,
                                                message: "Họ phải có ít nhất 1 ký tự"
                                            },
                                            maxLength : {
                                                value: 20,
                                                message: "Họ không được quá 20 ký tự"
                                            }
                                        })}
                                    />
                                    {errors.lastName && <p style={{color: 'red'}}>{errors.lastName.message}</p>}
                                </Col>
                            </Row>
                            <Row>
                                <Col md={4}>
                                    <label>Tên</label>
                                </Col>
                                <Col md={8}>
                                    <input
                                        {...register("firstName", {
                                            required: 'Tên không được để trống',
                                            minLength: {
                                                value: 4,
                                                message: "Tên phải có ít nhất 1 ký tự"
                                            },
                                            maxLength : {
                                                value: 20,
                                                message: "Tên không được quá 20 ký tự"
                                            }
                                        })}
                                    />
                                    {errors.firstName && <p style={{color: 'red'}}>{errors.firstName.message}</p>}
                                </Col>
                            </Row>
                            <Row>
                                <Col md={4}>
                                    <label>Tuổi</label>
                                </Col>
                                <Col md={8}>
                                    <input
                                        {...register("age", {
                                            required: 'Tuổi  không được để trống',
                                            min: {
                                                value: 1,
                                                message: "Tuổi phải lớn hơn 0"
                                            },
                                            max : {
                                                value: 9999,
                                                message: "Tuổi phải nhỏ hơn 9999"
                                            }
                                        })}
                                    />
                                    {errors.age && <p style={{color: 'red'}}>{errors.age.message}</p>}
                                </Col>
                            </Row>
                            <Row>
                                <Col md={4}>
                                    <label>Email</label>
                                </Col>
                                <Col md={8}>
                                    <input
                                        {...register("email", {
                                            required: 'Email không được để trống',
                                            pattern: {
                                                value: /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i,
                                                message: "Email không đúng định dạng"
                                            }
                                        })}
                                    />
                                    {errors.email && <p style={{color: 'red'}}>{errors.email.message}</p>}
                                </Col>
                            </Row>
                            <Row>
                                <Col md={4}>
                                    <label>Địa chỉ</label>
                                </Col>
                                <Col md={8}>
                                    <input
                                        {...register("address", {
                                            required: 'Địa chỉ không được để trống',
                                            minLength: {
                                                value: 4,
                                                message: "Địa chỉ phải có ít nhất 4 ký tự"
                                            },
                                            maxLength : {
                                                value: 20,
                                                message: "Địa chỉ không được quá 20 ký tự"
                                            }
                                        })}
                                    />
                                    {errors.address && <p style={{color: 'red'}}>{errors.address.message}</p>}
                                </Col>
                            </Row>

                    </ModalBody>
                    <ModalFooter>
                        <Button color="primary" type="submit">Thêm</Button>{' '}
                        <Button color="secondary" onClick={toggle}>Cancel</Button>
                    </ModalFooter>
                </form>

                </Modal>

        </div>
    );
}

export default ModalCreateUser;