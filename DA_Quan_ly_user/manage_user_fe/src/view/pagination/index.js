import React from 'react';
import { Pagination, PaginationItem, PaginationLink } from 'reactstrap';

function Paging(Props) {

    const {
        totalElements,
        totalPages,
        currentPage,
        currentPageSize,
        changePage,
        activePage
    } = Props;


    const callBackChangePage =(newPage)=> {
        changePage(newPage);
    }

    let PagingItem = [];
    if(currentPage < totalPages - 5){
        for(let i=currentPage;i<currentPage+5;i++){
            PagingItem.push(
                <PaginationItem active={activePage === i}>
                    <PaginationLink key={i+1} onClick={()=> callBackChangePage(i)}>
                        {i+1}
                    </PaginationLink>
                </PaginationItem>
            )
        }
    }
    else {
        for(let i=totalPages-5;i<totalPages;i++){
            PagingItem.push(
                <PaginationItem active={activePage === i}>
                    <PaginationLink key={i+1} onClick={()=> callBackChangePage(i)}>
                        {i+1}
                    </PaginationLink>
                </PaginationItem>
            )
        }
    }


    return(
        <>
            <Pagination aria-label="Page navigation example">
                <PaginationItem disabled={currentPage <= 0}>
                    <PaginationLink first onClick={()=> callBackChangePage(0)} />
                </PaginationItem>
                <PaginationItem disabled={currentPage <= 0}>
                    <PaginationLink previous onClick={()=> callBackChangePage(currentPage - 1)} />
                </PaginationItem>

                {PagingItem}

                <PaginationItem disabled={currentPage >= totalPages - 1}>
                    <PaginationLink next onClick={()=> callBackChangePage(currentPage + 1)} />
                </PaginationItem>
                <PaginationItem disabled={currentPage >= totalPages - 1}>
                    <PaginationLink last onClick={()=> callBackChangePage(totalPages - 1)} />
                </PaginationItem>
            </Pagination>
        </>
    );

}

export default Paging;