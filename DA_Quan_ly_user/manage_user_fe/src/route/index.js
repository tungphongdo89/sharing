import React from 'react';
import {Router, Route, Switch} from 'react-router-dom';
import history from './history';

import Admin from '../view/admin';
import Home from '../view/home';

function RootRouter() {
    return(
        <Router history={history}>
            <Switch>
                <Route path="/admin" component={Admin} exact />
                <Route path="/" component={Home} exact />
            </Switch>
        </Router>
    );

}

export default RootRouter;
