create database manage_user;
use manage_user;

create table user_type(
	id int not null auto_increment,
    code nvarchar(50) not null,
    type_name nvarchar(255),
    primary key(id)
);
create table user(
	id int not null auto_increment,
    code nvarchar(50) not null,
    type_user_code nvarchar(50) not null,
    first_name nvarchar(255),
    last_name nvarchar(255),
    age int ,
    email nvarchar(255),
    address nvarchar(255),
    primary key(id)
);
insert into user_type (code, type_name) values('type01', 'vip');
insert into user_type (code, type_name) values('type02', 'medium');
insert into user_type (code, type_name) values('type03', 'new');

insert into user (code, typeUserCode, firstName, lastName, age, email, address) 
		   values('u2020918020202', 'type01', 'tung', 'hoang', 22, 'tungphongdo89@gmail.com', 'thanh hoa');
insert into user (code, typeUserCode, firstName, lastName, age, email, address) 
		   values('u2020918020201', 'type02', 'john', 'wick', 22, 'john@gmail.com', 'thanh hoa');
insert into user (code, typeUserCode, firstName, lastName, age, email, address) 
		   values('u2020918020204', 'type03', 'luong', 'tran', 22, 'luong@gmail.com', 'thanh hoa');
insert into user (code, typeUserCode, firstName, lastName, age, email, address) 
		   values('u2020918020203', 'type01', 'ha', 'nam', 22, 'ha89@gmail.com', 'thanh hoa');
insert into user (code, typeUserCode, firstName, lastName, age, email, address) 
		   values('u2020918020206', 'type02', 'le', 'lee', 22, 'le89@gmail.com', 'thanh hoa');
insert into user (code, typeUserCode, firstName, lastName, age, email, address) 
		   values('u2020918020208', 'type03', 'ronaldo', 'cr', 22, 'ronaldo@gmail.com', 'thanh hoa');
insert into user (code, typeUserCode, firstName, lastName, age, email, address) 
		   values('u2020918020205', 'type01', 'thom', 'tran', 22, 'thom@gmail.com', 'thanh hoa');
insert into user (code, typeUserCode, firstName, lastName, age, email, address) 
		   values('u2020918020207', 'type02', 'vanh', 'ngan', 22, 'vanh@gmail.com', 'thanh hoa');
           
insert into user (code, typeUserCode, firstName, lastName, age, email, address) 
		   values('u2021918020208', 'type03', 'ronaldo 2', 'cr', 22, 'ronaldo2@gmail.com', 'thanh hoa');
insert into user (code, typeUserCode, firstName, lastName, age, email, address) 
		   values('u2021918020205', 'type01', 'thom 2', 'tran', 22, 'thom2@gmail.com', 'thanh hoa');
insert into user (code, typeUserCode, firstName, lastName, age, email, address) 
		   values('u2021918020207', 'type02', 'vanh 2', 'ngan', 22, 'vanh2@gmail.com', 'thanh hoa');
           
insert into user (code, typeUserCode, firstName, lastName, age, email, address) 
		   values('u2022918020208', 'type03', 'ronaldo 3', 'cr', 22, 'ronaldo3@gmail.com', 'thanh hoa');
insert into user (code, typeUserCode, firstName, lastName, age, email, address) 
		   values('u2022918020205', 'type01', 'thom 3', 'tran', 22, 'thom3@gmail.com', 'thanh hoa');
insert into user (code, typeUserCode, firstName, lastName, age, email, address) 
		   values('u2022918020207', 'type02', 'vanh 3', 'ngan', 22, 'vanh3@gmail.com', 'thanh hoa');
           
insert into user (code, typeUserCode, firstName, lastName, age, email, address) 
		   values('u2019918020208', 'type03', 'ronaldo 4', 'cr', 22, 'ronaldo4@gmail.com', 'thanh hoa');
insert into user (code, typeUserCode, firstName, lastName, age, email, address) 
		   values('u2019918020205', 'type01', 'thom 4', 'tran', 22, 'thom4@gmail.com', 'thanh hoa');
insert into user (code, typeUserCode, firstName, lastName, age, email, address) 
		   values('u2019918020207', 'type02', 'vanh 4', 'ngan', 22, 'vanh4@gmail.com', 'thanh hoa');
           
insert into user (code, typeUserCode, firstName, lastName, age, email, address) 
		   values('u2019928020208', 'type03', 'ronaldo 5', 'cr', 22, 'ronaldo5@gmail.com', 'thanh hoa');
insert into user (code, typeUserCode, firstName, lastName, age, email, address) 
		   values('u2019928020205', 'type01', 'thom 5', 'tran', 22, 'thom5@gmail.com', 'thanh hoa');
insert into user (code, typeUserCode, firstName, lastName, age, email, address) 
		   values('u2019928020207', 'type02', 'vanh 5', 'ngan', 22, 'vanh5@gmail.com', 'thanh hoa');
           
insert into user (code, typeUserCode, firstName, lastName, age, email, address) 
		   values('u2019918020203', 'type03', 'ronaldo 6', 'cr', 22, 'ronaldo6@gmail.com', 'thanh hoa');
insert into user (code, typeUserCode, firstName, lastName, age, email, address) 
		   values('u2019918020201', 'type01', 'thom 6', 'tran', 22, 'thom6@gmail.com', 'thanh hoa');
insert into user (code, typeUserCode, firstName, lastName, age, email, address) 
		   values('u2019918020200', 'type02', 'vanh 6', 'ngan', 22, 'vanh6@gmail.com', 'thanh hoa');
           
insert into user (code, typeUserCode, firstName, lastName, age, email, address) 
		   values('u2019915020203', 'type03', 'ronaldo 7', 'cr', 22, 'ronaldo7@gmail.com', 'thanh hoa');
insert into user (code, typeUserCode, firstName, lastName, age, email, address) 
		   values('u2019915020201', 'type01', 'thom 7', 'tran', 22, 'thom7@gmail.com', 'thanh hoa');
insert into user (code, typeUserCode, firstName, lastName, age, email, address) 
		   values('u2019915020200', 'type02', 'vanh 7', 'ngan', 22, 'vanh7@gmail.com', 'thanh hoa');
           
use manage_user;
-- search user
select * from user;
select 
	u.code, 
    u.firstName, 
    u.lastName, 
    u.age, 
    u.email, 
    u.address, 
    ut.typeName
from user u join user_type ut on u.typeUserCode = ut.code
where 1 = 1
-- and lower (u.code) like lower ('%u%') 
-- and  u.typeUserCode like '% %' 
and (lower (u.firstName) like lower ('tung' ))
-- or lower (u.lastName) like lower ('%a%' ))
-- and lower (ut.typeName) like lower ('% %')
;

select code, typeName from  user_type;

select * from user;

           
		
