import React from 'react';
import {Link} from 'react-router-dom';
import {connect} from "react-redux";
import {bindActionCreators, compose} from "redux";
import {withStyles} from "@material-ui/core";

import 'bootstrap/dist/css/bootstrap.css';
import {Card, CardHeader, CardTitle, CardBody, Button, Label, FormGroup, Spinner, Col, Row} from 'reactstrap'
import { Formik, Field, Form, ErrorMessage } from "formik";

import userAction from "../../redux/action/userAction"
import styles from "./style";

import { Redirect } from 'react-router';

import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import ForgotPassSchema from "../../validation/validForgotPass";

class ForgotPass extends React.Component{

    constructor(props){
        super(props);
        this.state={

        }
    }

    loadingComponent = (
        <div style={{
            position: 'absolute',
            zIndex: 110,
            top: 0,
            left: 0,
            width: '100%',
            height: '100%',
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            background: 'rgba(255,255,255,0.8)',
        }}>
            <Spinner color="primary" className="reload-spinner"/>
        </div>
    );

    render() {
        debugger
        const {classes} = this.props;
        const token =  localStorage.token;
        return (
            <>

                <div className="container-fluid" id={styles.background} >
                    {this.props.loadingGetForgotPass ? <>{this.loadingComponent}</> : null}
                    <div style={{marginTop: '3%'}}>
                        <Link to="/"><Button color="primary">Trang chủ</Button></Link>
                    </div>
                    <Row style={{marginTop: '3%'}}>
                        <Col xs="0" sm="0" md="3" lg="3"></Col>
                        <Col xs="12" sm="12" md="6" lg="6">
                            <Card className={classes.cardBg}>
                                <CardBody>
                                    <Formik
                                        initialValues={{
                                            email: ""
                                        }}

                                        onSubmit={values=>{
                                            debugger
                                            const {userAction} = this.props;
                                            const {getForgotPass} = userAction;
                                            let user  = {};
                                            user.email = values.email;
                                            getForgotPass(user)
                                        }}

                                        validationSchema={ForgotPassSchema}

                                        render={({ errors, touched })=>(

                                            <Form>
                                                <h2 style={{textAlign: 'center'}}>Quên mật khẩu</h2>
                                                <FormGroup>
                                                    <label>Nhập vào email đăng ký tài khoản</label>
                                                    <Field className = "form-control" name ="email" placeholder="abc@gmail.com" />
                                                    <ErrorMessage name = "email" component = "span" className = "field-error text-danger" />
                                                </FormGroup>

                                                <div style={{textAlign: 'center'}}>
                                                    <button className="btn btn-danger" type="submit">Lấy lại mật khẩu</button>
                                                </div>
                                            </Form>

                                        )}
                                    />
                                    <ToastContainer />
                                </CardBody>
                            </Card>
                        </Col>
                        <Col xs="0" sm="0" md="3" lg="3"></Col>
                    </Row>
                </div>
            </>
        );
    }

}

const mapStateToProps = (state) => {
    return {
        // token: state.userReducer.token,
        loadingGetForgotPass: state.userReducer.loadingGetForgotPass
    }
}
const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        userAction : bindActionCreators(userAction , dispatch),
    }
}
const withConnect = connect(mapStateToProps, mapDispatchToProps)

export default compose(withStyles(styles), withConnect)(ForgotPass);

