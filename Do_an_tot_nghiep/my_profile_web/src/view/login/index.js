import React from 'react';
import {Link} from 'react-router-dom';
import {connect} from "react-redux";
import {bindActionCreators, compose} from "redux";
import {withStyles} from "@material-ui/core";

import 'bootstrap/dist/css/bootstrap.css';
import {Card, CardHeader, CardTitle, CardBody, Button, Label, FormGroup, Spinner, Col, Row} from 'reactstrap'
import { Formik, Field, Form, ErrorMessage } from "formik";

import userAction from "../../redux/action/userAction"
import styles from "./style";

import { Redirect } from 'react-router';

import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import LoginSchema from "../../validation/validLogin"

class Login extends React.Component{

    constructor(props){
        super(props);
        this.state={

        }
    }

    loadingComponent = (
        <div style={{
            position: 'absolute',
            zIndex: 110,
            top: 0,
            left: 0,
            width: '100%',
            height: '100%',
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            background: 'rgba(255,255,255,0.8)',
        }}>
            <Spinner color="primary" className="reload-spinner"/>
        </div>
    );

    render() {
        debugger
        const {classes} = this.props;
        const token =  localStorage.token;
        return (
            <>

                <div className="container-fluid" id={styles.background} >
                    {this.props.loadingLogin ? <>{this.loadingComponent}</> : null}
                    <div style={{marginTop: '3%'}}>
                        <Link to="/"><Button color="primary">Trang chủ</Button></Link>
                    </div>
                    <Row style={{marginTop: '3%'}}>
                        <Col xs="0" sm="0" md="3" lg="3"></Col>
                        <Col xs="12" sm="12" md="6" lg="6">
                            <Card className={classes.cardBg}>
                                <CardBody>
                                    <Formik
                                        initialValues={{
                                            username: "",
                                            password: ""
                                        }}

                                        onSubmit={values=>{
                                            debugger
                                            const {userAction} = this.props;
                                            const {login} = userAction;
                                            let user  = {};
                                            user.username = values.username;
                                            user.password = values.password;
                                            login(user)
                                        }}

                                        validationSchema={LoginSchema}

                                        render={({ errors, touched })=>(

                                            <Form>
                                                <h2 style={{textAlign: 'center'}}>ĐĂNG NHẬP</h2>
                                                <FormGroup>
                                                    <label htmlFor="name">Tên đăng nhập</label>
                                                    <Field className = "form-control" name ="username" placeholder="tên đăng nhập" />
                                                    <ErrorMessage name = "username" component = "span" className = "field-error text-danger" />
                                                </FormGroup>
                                                <FormGroup>
                                                    <label htmlFor="id">Mật khẩu</label>
                                                    <Field type="password" className = "form-control" name ="password" placeholder="mật khẩu" />
                                                    <ErrorMessage name = "password" component = "span" className = "field-error text-danger" />
                                                </FormGroup>
                                                <div style={{textAlign: 'center'}}>
                                                    <button className="btn btn-danger" type="submit">Đăng nhập</button>
                                                </div>
                                            </Form>

                                        )}
                                    />
                                    <div style={{textAlign: 'center', marginTop: '2%'}}>
                                        <Link to="/pages/forgot-pass">Quên mật khẩu ?</Link>
                                        <br/>
                                        <Link to="/pages/sign-up">Bạn chưa có tài khoản ?</Link>
                                    </div>

                                    {undefined === token && this.props.loginSuccess === false ?
                                        null : <Redirect to="/" />
                                    }

                                </CardBody>
                            </Card>
                        </Col>
                        <Col xs="0" sm="0" md="3" lg="3"></Col>
                    </Row>
                </div>
            </>
        );
    }

}

const mapStateToProps = (state) => {
    return {
        // token: state.userReducer.token,
        loginSuccess: state.userReducer.loginSuccess,
        loginFailed: state.userReducer.loginFailed,
        loadingLogin: state.userReducer.loadingLogin
    }
}
const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        userAction : bindActionCreators(userAction , dispatch),
    }
}
const withConnect = connect(mapStateToProps, mapDispatchToProps)

export default compose(withStyles(styles), withConnect)(Login);

