import React, { Component } from 'react'
import contact from './image/contactlogo.png';
import mailme from './image/mailmeimg.png';
import iconFB from './image2/icon_fb.png';
import iconYT from './image2/icon_yt.png';


class Contact extends Component {
    render() {
        return (
            <section className="container-1">
                <div>
                        <img style={{paddingBottom: '1%'}} id="contactimg" src={contact} width="180" height="180" alt="contactlogo"/>
                        <h3 style={{paddingBottom: '1%'}}> <strong>Hoàng Thanh Tùng</strong></h3>
                        <h3 style={{paddingBottom: '1%'}}>Địa chỉ: 99-Ngọa Long-Minh Khai-Bắc Từ liêm-Hà Nội</h3>
                        <h4 >Email : tungphongdo89@gmail.com</h4>

                        <a id="mail"href="https://mail.google.com/mail/?view=cm&fs=1&to=tungphongdo89@gmail.com">
                                <img id="mailmelogo" src={mailme} alt="mail me"/>Click vào đây để gửi Mail</a>
                        <div style={{paddingBottom: '1%'}}></div>
                        <h4>SỐ ĐT: (+84)815 954 557</h4>


                        <a style={{margin: '2%'}} href="https://www.facebook.com/profile.php?id=100008169721169">
                                {/*<span className="icon fa fa-facebook" style={{color:'antiquewhite'}} ></span>*/}
                            <img style={{width: '5%'}} src={iconFB}/>
                        </a>

                        <a style={{margin: '2%'}} href="https://www.youtube.com/channel/UCy9zUn42NxIv-AOuMRd7l-Q">
                                {/*<span className="icon fa fa-youtube" style={{color:'antiquewhite'}} ></span>*/}
                            <img style={{width: '3%', border: '1px solid white'}} src={iconYT}/>
                        </a>
                        {/*<a href="https://twitter.com/techstud101" >*/}
                                {/*<span className="icon fa fa-twitter"  style={{color:'antiquewhite'}}></span>*/}
                        {/*</a>*/}
                        <a style={{margin : '2%'}} href="https://github.com/tungphongdo89/student.github.io">
                                {/*<span className="icon fa fa-github" style={{color:'antiquewhite'}} ></span>*/}
                        </a>


                        <div style={{paddingBottom: '2%'}}></div>
                </div>
            </section>
        )
    }
}

export default Contact
