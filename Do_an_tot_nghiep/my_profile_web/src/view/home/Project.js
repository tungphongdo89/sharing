import React, { Component } from 'react'

import pyimg from './image/pythonimg.png'
import jsimg from './image/javascriptimg2.png'
import htmlimg from './image/html5logo.jpg'
import cssimg from './image/csslogo.png'
import wordpressimg from './image/wordpress.png'
import mongoimg from './image/mngodbimg.png'
import databasebimg from './image/database.jpg'

import javaImg from './image2/java.jpg'
import springImg from './image2/spring.PNG'
import hibernate from './image2/hibernate.PNG'
import reactjs from './image2/reactjs.png'

import mysql from './image2/mysql.PNG'
import sqlserver from './image2/sqlserver.PNG'
import oracle from './image2/oracle.PNG'
import gitlab from './image2/gitlab.jpg'
import github from './image2/github.png'
import restfullApi from './image2/restfullApi.png'


class Project extends Component {
    render() {
        return (
           <>
                <h2 style={{color: 'white', marginLeft: '5%'}}>BACK-END</h2>
                <section id="skillheader" className="flex-project-container">
                    <div><img src={javaImg} width="100" height="100" alt="java"/></div>
                    <div><img src={springImg} width="100" height="100" alt="spring"/></div>
                    <div><img src={hibernate} width="100" height="100" alt="hibernate"/></div>
                    <div><img src={restfullApi} width="100" height="100" alt="restfull_api"/></div>
                </section>
               <h2 style={{color: 'white', margin: '5% 0 0 5%'}}>FRONT-END</h2>
               <section id="skillheader" className="flex-project-container">
                   <div><img src={jsimg} width="100" height="100" alt="js"/></div>
                   <div><img src={htmlimg} width="100" height="100" alt="html"/></div>
                   <div><img src={cssimg} width="100" height="100" alt="css"/></div>
                   <div><img src={reactjs} width="100" height="100" alt="reactjs"/></div>
               </section>
               <h2 style={{color: 'white', margin: '5% 0 0 5%'}}>DATABASE</h2>
               <section id="skillheader" className="flex-project-container">
                   <div><img src={mysql} width="100" height="100" alt="mysql"/></div>
                   <div><img src={sqlserver} width="100" height="100" alt="sqlserver"/></div>
                   <div><img src={oracle} width="100" height="100" alt="oracle"/></div>
               </section>
               <h2 style={{color: 'white', margin: '5% 0 0 5%'}}>QUẢN LÝ SOURCE CODE</h2>
               <section id="skillheader" className="flex-project-container">
                   <div><img src={gitlab} width="100" height="100" alt="gitlab"/></div>
                   <div><img src={github} width="100" height="100" alt="github"/></div>
               </section>
           </>
        )
    }
}

export default Project
