import React, { Component } from 'react'
import biologo from './image/biopicimg.jpg'

import 'bootstrap/dist/css/bootstrap.css';
import {Button, Spinner, Row, Container, Col} from 'reactstrap';

import avt from './image2/avt2.PNG'
import about from './image2/icon_about.jpg'
import birthday from './image2/birthday.png'
import address from './image2/address.png'
import email from './image2/email.jpg'
import telephone from './image2/telephone.PNG'
import university from './image2/university.jpg'
import personal from './image2/personal.png'
import experience from './image2/experience.png'

class About extends Component {
    render() {
        return (
            <section id="container-about" className="container-about">
                <h1 style={{fontWeight: '700', backgroundColor: 'antiquewhite'}}>Thông tin cá nhân</h1>
                <Row>
                    <Col xs="12" md="12" sm="12" lg="2">

                    </Col>

                    <Col style={{marginTop: '5%', border: '5px solid gray'}} xs="12" md="12" sm="12" lg="4">
                        <img src={avt} style={{width: '100%'}} alt="abtimg"/>
                    </Col>

                    <Col style={{marginTop: '5%', border: '5px solid gray'}} xs="12" md="12" sm="12" lg="4">
                        <div style={{backgroundColor: '#dfd8d8', borderBottom: '3px solid gray', textAlign: 'center'}}>
                            <div style={{fontWeight: '700', fontSize: '220%'}}>Hoàng Thanh Tùng</div>
                            <p>Lập trình & Cuộc sống</p>
                        </div>
                        <Col xs="12" md="12" sm="12" lg="12" style={{borderBottom: '3px solid gray', paddingTop: '1%'}}>
                            <Row>
                                <Col xs="9" md="9" sm="9" lg="9" >
                                    <p style={{textAlign: 'left', fontSize: '120%', fontWeight: '600'}}>NGÀY SINH</p>
                                    <p style={{fontSize: '150%', fontWeight: '700'}}>31 / 08 / 1999</p>
                                </Col>
                                <Col xs="3" md="3" sm="3" lg="3" style={{textAlign: 'right'}}>
                                    <img style={{width: '60%'}} src={birthday}/>
                                </Col>
                            </Row>
                        </Col>
                        <Col xs="12" md="12" sm="12" lg="12" style={{borderBottom: '3px solid gray', paddingTop: '1%'}}>
                            <Row>
                                <Col xs="9" md="9" sm="9" lg="9" >
                                    <p style={{textAlign: 'left', fontSize: '120%', fontWeight: '600'}}>HỌC VẤN</p>
                                    <p style={{fontSize: '120%', fontWeight: '700'}}>
                                        Học Công nghệ thông tin(IT)  tại đại học Công Nghiệp Hà Nội(HaUI)
                                    </p>
                                </Col>
                                <Col xs="3" md="3" sm="3" lg="3" style={{textAlign: 'right'}}>
                                    <img style={{width: '60%'}} src={university}/>
                                </Col>
                            </Row>
                        </Col>
                        <Col xs="12" md="12" sm="12" lg="12" style={{borderBottom: '3px solid gray', paddingTop: '1%'}}>
                            <Row>
                                <Col xs="9" md="9" sm="9" lg="9" >
                                    <p style={{textAlign: 'left', fontSize: '120%', fontWeight: '600'}}>KINH NGHIỆM</p>
                                    <p style={{fontSize: '100%', fontWeight: '700'}}>
                                        Đã có 1 năm kinh nghiệm làm trong dự án thực tế tại công ty TNHH giải pháp
                                        công nghệ MIGI
                                        <div><a href="https://www.migitek.com">https://www.migitek.com</a></div>
                                    </p>
                                </Col>
                                <Col xs="3" md="3" sm="3" lg="3" style={{textAlign: 'right'}}>
                                    <img style={{width: '45%'}} src={experience}/>
                                </Col>
                            </Row>
                        </Col>
                        <Col xs="12" md="12" sm="12" lg="12" style={{paddingTop: '1%'}}>
                            <Row>
                                <Col xs="9" md="9" sm="9" lg="9" >
                                    <p style={{textAlign: 'left', fontSize: '120%', fontWeight: '600'}}>TÍNH CÁCH</p>
                                    <p style={{fontSize: '100%', fontWeight: '700'}}>
                                        Trung thực, chăm chỉ, hòa đồng với mọi người.
                                        Luôn cầu tiến trong công việc cũng như cuộc sống
                                    </p>
                                </Col>
                                <Col xs="3" md="3" sm="3" lg="3" style={{textAlign: 'right'}}>
                                    <img style={{width: '60%'}} src={personal}/>
                                </Col>
                            </Row>
                        </Col>
                    </Col>

                    <Col xs="12" md="12" sm="12" lg="2">

                    </Col>

                </Row>

                
            </section>
        )
    }
}

export default About
