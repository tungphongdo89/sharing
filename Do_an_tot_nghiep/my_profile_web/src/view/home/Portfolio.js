import React, { Component } from 'react'
import pybot from './image/pybot.png';
import Opencv from './image/Snapshot.png';
import NightMares from './image/menu3.png';
import RecorderMaintainer from './image/de.png';
import WordPress from './image/blog.png';
import Password from './image/bute.png';

import toeicWeb from './image2/toeic_web.PNG'
import qlan from './image2/qlan.PNG'
import myShop from './image2/myShop.PNG'
import qlts from './image2/qlts.PNG'
import musicWeb from './image2/musicweb.png'
import crm from "./image2/CRM.PNG"




class Portfolio extends Component {
    render() {
        return (
            
            <section className="projects">
                <h1 style={{fontWeight: '700', margin: '5% 0'}} id='Portfolio'>CÁC DỰ ÁN ĐÃ LÀM</h1>
                {/*<h2>ĐANG CẬP NHẬT</h2>*/}

                    <div className="container">

                        <a href="http://3.1.25.74:8181/qlts_web/">
                            <img style={{border: '1px solid gray'}} src={qlts} width="400" height="200" alt="qlts_img"/>
                            <p>Dự án: Quản lý tài sản</p>
                        </a>
                        <a href="https://www.toeic.migitek.com/">
                            <img style={{border: '1px solid gray'}} src={toeicWeb} width="400" height="200" alt="toeic_img"/>
                            <p>Dự án: Website luyện thi toeic</p>
                        </a>
                        <a href="https://github.com/tungphongdo89/myshop">
                            <img style={{border: '1px solid gray'}} src={myShop} width="400" height="200" alt="myShop_image"/>
                            <p>Dự án: Website bán quần áo</p>
                        </a>
                        <a href="http://27.118.22.7:4200/#/login">
                            <img style={{border: '1px solid gray'}} src={crm} width="400" height="200" alt="crm"/>
                            <p>Dự án: Website Chăm sóc khách hàng</p>
                        </a>
                        <a>
                            <img style={{border: '1px solid gray'}} src={musicWeb} width="400" height="200" alt="musicweb_image"/>
                            <p>Dự án: Website nghe nhạc trực tuyến</p>
                        </a>
                    </div>

                    {/*<div className="container">*/}
                        {/*<h2>1 SỐ DỰ ÁN KHÁC : Đang cập nhật</h2>*/}
                        {/*/!*<a href="https://github.com/abhishek305/Student-Record-Maintainer-In-PyQT5"><img src={RecorderMaintainer} width="200" height="200" alt="Pybot"/><p>Recorder Maintainer</p></a>*!/*/}
                        {/*/!*<a href="https://techstud105blog.wordpress.com/"><img src={WordPress} width="200" height="200" alt="Pybot"/><p>Wordpress Blog</p></a>*!/*/}
                        {/*/!*<a href="https://github.com/abhishek305/Brute-Forcing-password-in-python"><img src={Password} width="200" height="200" alt="Pybot"/><p>Password Cracker</p></a>*!/*/}
                    {/*</div>*/}

                       
            </section>
        )
    }
}

export default Portfolio


