import React from "react";
import {Nav} from "reactstrap";
import { Link, NavLink } from "react-router-dom";
// import { withRouter } from "react-router";
// import './style.css'

class Sidebar extends React.Component{
    render() {
        return (
            <>
                <Nav className="col-md-12 d-none d-md-block bg-light sidebar"
                     activeKey="/home"
                     onSelect={selectedKey => alert(`selected ${selectedKey}`)}
                >
                    <div className="sidebar-sticky"></div>
                    <Nav>
                        <Link href="/home">Active</Link>
                    </Nav>
                    <Nav>
                        <Link eventKey="link-1">Link</Link>
                    </Nav>
                    <Nav>
                        <Link eventKey="link-2">Link</Link>
                    </Nav>
                    <Nav>
                        <Link eventKey="disabled" disabled>
                            Disabled
                        </Link>
                    </Nav>
                </Nav>

            </>
        );
    }
};

export default Sidebar