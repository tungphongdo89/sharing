import React from 'react';
import {Link} from 'react-router-dom';
import {connect} from "react-redux";
import {bindActionCreators, compose} from "redux";
import {withStyles} from "@material-ui/core";

import userAction from "../../redux/action/userAction"
import styles from "./style";
import {Redirect} from "react-router";

import 'bootstrap/dist/css/bootstrap.css';
import {Button, Spinner, Row, Container, Col} from 'reactstrap';
import { Table } from 'reactstrap';
import {toast } from 'react-toastify';

// import '../../../App.css';
// import { Helmet } from 'react-helmet'

class Home2 extends React.Component{

    constructor(props){
        super(props);
        this.state={

        }
    }

    logoutUser =()=> {
        localStorage.removeItem("token");
        const {userAction} = this.props;
        const {logout} = userAction;
        logout();
    }

    componentWillMount() {

    }

    notification =()=> {
        toast.success("đăng nhập thành công")
    }

    loadingComponent = (
        <div style={{
            position: 'absolute',
            zIndex: 110,
            top: 0,
            left: 0,
            width: '100%',
            height: '100%',
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            background: 'rgba(255,255,255,0.8)',
        }}>
            <Spinner color="primary" className="reload-spinner"/>
        </div>
    );

    render() {
        debugger
        const token =  localStorage.token;
        const fullName = localStorage.fullName;
        const {classes} = this.props;
        return (
            <>
                <style>{`
                    #header-top{
                        height: 100vh;
                        -webkit-background-size: cover;
                        -moz-background-size: cover;
                        -o-background-size: cover;
                        background-size: cover;
                    }
                `}</style>
                <h2>Đây là trang chủ</h2>

                {this.props.logoutSuccess === false ?
                    <>
                        <Link to="/pages/login"><Button>Đăng nhập</Button></Link>
                    </>
                    :
                    <>
                        <h3>Xin chào {fullName } </h3>
                        <Button onClick={() => this.logoutUser()}>Đăng xuất</Button>
                    </>

                }
                {this.props.loginSuccess === false ?
                    this.notification : null
                }


            </>
        );
    }

}

const mapStateToProps = (state) => {
    return {
        // token: state.userReducer.token,
        // user: state.userReducer.user
        loginSuccess: state.userReducer.loginSuccess,
        logoutSuccess: state.userReducer.logoutSuccess
    }
}
const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        userAction : bindActionCreators(userAction , dispatch),
    }
}
const withConnect = connect(mapStateToProps, mapDispatchToProps)

export default compose(withStyles(styles), withConnect)(Home2);

