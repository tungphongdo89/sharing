import React from 'react';
import {Link} from 'react-router-dom';
import {connect} from "react-redux";
import {bindActionCreators, compose} from "redux";
import {withStyles} from "@material-ui/core";

import 'bootstrap/dist/css/bootstrap.css';
import {Card, CardHeader, CardTitle, CardBody, Button, Label, FormGroup, Spinner, Col, Row, Input} from 'reactstrap'
import { Formik, Field, Form, ErrorMessage} from "formik";

import userAction from "../../redux/action/userAction"
import styles from "./style";

import { Redirect } from 'react-router';

import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import SignUpSchema from "../../validation/validSignUp"

class SignUp extends React.Component{

    constructor(props){
        super(props);
        this.state={

        }
    }

    loadingComponent = (
        <div style={{
            position: 'absolute',
            zIndex: 110,
            top: 0,
            left: 0,
            width: '100%',
            height: '100%',
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            background: 'rgba(255,255,255,0.8)',
        }}>
            <Spinner color="primary" className="reload-spinner"/>
        </div>
    );

    render() {
        debugger
        const {classes} = this.props;
        const token =  localStorage.token;
        return (
            <>

                <div className="container-fluid" id={styles.background} >
                    {this.props.loadingSignUp ? <>{this.loadingComponent}</> : null}
                    <div style={{marginTop: '1%'}}>
                        <Link to="/"><Button color="primary">Trang chủ</Button></Link>
                    </div>
                    <Row style={{marginTop: '2%'}}>
                        <Col xs="0" sm="0" md="3" lg="3"></Col>
                        <Col xs="12" sm="12" md="6" lg="6">
                            <Card className={classes.cardBg}>
                                <CardBody>
                                    {this.props.signUpSuccess ?
                                        <Link to="/pages/login">Quay lại trang đăng nhập </Link>
                                         :
                                        <Formik
                                            initialValues={{
                                                username: "",
                                                password: "",
                                                fullName: "",
                                                gender: "",
                                                email: "",
                                                address: ""
                                            }}

                                            onSubmit={values=>{
                                                debugger
                                                const {userAction} = this.props;
                                                const {signUp} = userAction;
                                                let user  = {};
                                                user.username = values.username;
                                                user.password = values.password;
                                                user.fullName = values.fullName;
                                                user.gender = parseInt(values.gender);
                                                user.email = values.email;
                                                user.address = values.address;
                                                signUp(user)
                                            }}

                                            validationSchema={SignUpSchema}

                                            render={({ errors, touched })=>(

                                                <Form>
                                                    <h2 style={{textAlign: 'center'}}>ĐĂNG KÝ</h2>
                                                    <FormGroup>
                                                        <label>Tên đăng nhập</label>
                                                        <Field className = "form-control" name ="username" placeholder="tên đăng nhập" />
                                                        <ErrorMessage name = "username" component = "span" className = "field-error text-danger" />
                                                    </FormGroup>
                                                    <FormGroup>
                                                        <label>Mật khẩu</label>
                                                        <Field type="password" className = "form-control" name ="password" placeholder="mật khẩu" />
                                                        <ErrorMessage name = "password" component = "span" className = "field-error text-danger" />
                                                    </FormGroup>
                                                    <FormGroup>
                                                        <label>Họ và tên</label>
                                                        <Field className = "form-control" name ="fullName" placeholder="họ và tên" />
                                                        <ErrorMessage name = "fullName" component = "span" className = "field-error text-danger" />
                                                    </FormGroup>

                                                    <FormGroup>
                                                        <label style={{marginRight: '3px    '}}>Giới tính: </label>
                                                        <Field name="gender" component="select">
                                                            <option value="0">Nam</option>
                                                            <option value="1">Nữ</option>
                                                        </Field>
                                                    </FormGroup>

                                                    <FormGroup>
                                                        <label>Email</label>
                                                        <Field className = "form-control" name ="email" placeholder="email" />
                                                        <ErrorMessage name = "email" component = "span" className = "field-error text-danger" />
                                                    </FormGroup>
                                                    <FormGroup>
                                                        <label>Địa chỉ</label>
                                                        <Field className = "form-control" name ="address" placeholder="địa chỉ" />
                                                        <ErrorMessage name = "address" component = "span" className = "field-error text-danger" />
                                                    </FormGroup>
                                                    <div style={{textAlign: 'center'}}>
                                                        <button className="btn btn-danger" type="submit">GỬI</button>
                                                    </div>
                                                </Form>
                                            )}
                                        />
                                    }

                                    <ToastContainer />
                                    {/*{this.props.loginSuccess === false ?*/}
                                        {/*<ToastContainer /> : null*/}
                                    {/*}*/}
                                    {/*{undefined === token && this.props.loginSuccess === false ?*/}
                                        {/*null : <Redirect to="/" />*/}
                                    {/*}*/}

                                </CardBody>
                            </Card>
                        </Col>
                        <Col xs="0" sm="0" md="3" lg="3"></Col>
                    </Row>
                </div>
            </>
        );
    }

}

const mapStateToProps = (state) => {
    return {
        // token: state.userReducer.token,
        loginSuccess: state.userReducer.loginSuccess,
        loginFailed: state.userReducer.loginFailed,
        loadingSignUp: state.userReducer.loadingSignUp,
        signUpSuccess: state.userReducer.signUpSuccess
    }
}
const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        userAction : bindActionCreators(userAction , dispatch),
    }
}
const withConnect = connect(mapStateToProps, mapDispatchToProps)

export default compose(withStyles(styles), withConnect)(SignUp);

