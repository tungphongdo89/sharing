import * as Yup from "yup";

//formik
const ForgotPassSchema = Yup.object().shape({
    email: Yup.string()
        .required('email không được để trống')
        .email('email không hợp lệ')
});

export default ForgotPassSchema