import * as Yup from "yup";

//formik
const SignUpSchema = Yup.object().shape({
    username: Yup.string()
        .min(4, 'Tên đăng nhập phải dài hơn 4 ký tự!')
        .max(100, 'Tên đăng nhập không được quá 100 ký tự')
        .required('Tên đăng nhập không được để trống'),

    password: Yup.string()
        .min(5, 'Mật khẩu phải dài hơn 4 ký tự!')
        .max(100, 'Mật khẩu không được quá 100 ký tự')
        .required('Mật khẩu không được để trống'),

    fullName: Yup.string()
        .required('Họ và tên không được để trống')
        .max(100, 'Họ và tên không được quá 100 ký tự'),

    gender : Yup.string()
        .required('Bạn chưa chọn giới tính'),

    email: Yup.string()
        .required('email không được để trống')
        .email('email không hợp lệ'),

    address: Yup.string()
        .required('Địa chỉ không được để trống')
        .max(100, 'Địa chỉ không được quá 100 ký tự'),
});

export default SignUpSchema