import React from 'react';

const userApi = {
    apiLogin,
    apiSignUp,
    apiGetForgotPass
}
async function apiLogin(data){
    return await fetch(
        '/api/login',
        {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data.data)
        }
    )
        .then(response => response.json())
        .then(body =>  body)
        //	.then(body =>  {localStorage.setItem("token", body.token)})
        .catch((error) => { throw error })
}

async function apiSignUp(data){
    return await fetch(
        '/api/createUser',
        {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data.data)
        }
    )
        .then(response => response.json())
        .then(body =>  body)
        .catch((error) => { throw error })
}

async function apiGetForgotPass(data){
    debugger
    return await fetch(
        '/api/forgotPass',
        {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data.data)
        }
    )
        .then(response => response.json())
        .then(body =>  body)
        .catch((error) => { throw error })
}

export default userApi