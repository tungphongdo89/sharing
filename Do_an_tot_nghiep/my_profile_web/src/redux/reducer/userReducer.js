import * as config from '../common/const';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const initState = {
    loginSuccess: false,
    loadingLogin: false,
    logoutSuccess: false,
    signUpSuccess: false,
    loadingSignUp: false,
    getForgotPassSuccess: false,
    loadingGetForgotPass: false
}

const userReducer = (state = initState, action) =>{
    switch(action.type){
        //login
        case config.LOGIN: {
            const data = action.payload;
            state = {...state, loadingLogin: true};
            return state;
        }
        case config.LOGIN_SUCCESS: {
            const data = action.payload.data;
            localStorage.setItem("token", data.token)
            localStorage.setItem("fullName", data.data.fullName)
            state = {...state, loginSuccess: true, loadingLogin: false, logoutSuccess: true};
            toast.success(data.message)
            return state;
        }
        case config.LOGIN_FAILED: {
            const data = action.payload.data;
            state = {...state, loadingLogin: false};
            toast.error(data.message)
            return state;
        }

        //logout
        case config.LOGOUT: {
            state = {...state, loginSuccess: false, logoutSuccess: false};
            return state;
        }

        //sign up
        case config.SIGN_UP: {
            const data = action.payload;
            state = {...state, loadingSignUp: true};
            return state;
        }
        case config.SIGN_UP_SUCCESS: {
            const data = action.payload.data;
            state = {...state, loadingSignUp: false, signUpSuccess: true};
            toast.success(data.message)
            return state;
        }
        case config.SIGN_UP_FAILED: {
            const data = action.payload.data;
            state = {...state, loadingSignUp: false};
            toast.error(data.message)
            return state;
        }

        //get forgot pass
        case config.GET_FORGOT_PASS: {
            debugger
            const data = action.payload;
            state = {...state, loadingGetForgotPass: true};
            return state;
        }
        case config.GET_FORGOT_PASS_SUCCESS: {
            debugger
            const data = action.payload.data;
            state = {...state, getForgotPassSuccess: true, loadingGetForgotPass: false};
            toast.success(data.message)
            return state;
        }
        case config.GET_FORGOT_PASS_FAILED: {
            debugger
            const data = action.payload.data;
            state = {...state, loadingGetForgotPass: false};
            toast.error(data.message)
            return state;
        }

        default:
            return state;
    }
}

export default userReducer