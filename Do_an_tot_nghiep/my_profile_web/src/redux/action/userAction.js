import * as config from '../common/const';

const userAction = {
    login,
    loginSuccess,
    loginFailed,
    logout,

    signUp,
    signUpSuccess,
    signUpFailed,

    getForgotPass,
    getForgotPassSuccess,
    getForgotPassFailed
}

//Login
function login(data){
    return{
        type: config.LOGIN,
        payload: {data}
    }
}

function loginSuccess(data){
    return{
        type: config.LOGIN_SUCCESS,
        payload: {data}
    }
}

function loginFailed(data){
    return{
        type: config.LOGIN_FAILED,
        payload: {data}
    }
}
//logout
function logout(){
    return{
        type: config.LOGOUT
    }
}

//sign up
function signUp(data){
    debugger
    return{
        type: config.SIGN_UP,
        payload: {data}
    }
}

function signUpSuccess(data){
    debugger
    return{
        type: config.SIGN_UP_SUCCESS,
        payload: {data}
    }
}

function signUpFailed(data){
    debugger
    return{
        type: config.SIGN_UP_FAILED,
        payload: {data}
    }
}

//get forgot pass
function getForgotPass(data){
    debugger
    return{
        type: config.GET_FORGOT_PASS,
        payload: {data}
    }
}

function getForgotPassSuccess(data){
    debugger
    return{
        type: config.GET_FORGOT_PASS_SUCCESS,
        payload: {data}
    }
}

function getForgotPassFailed(data){
    debugger
    return{
        type: config.GET_FORGOT_PASS_FAILED,
        payload: {data}
    }
}


export default userAction