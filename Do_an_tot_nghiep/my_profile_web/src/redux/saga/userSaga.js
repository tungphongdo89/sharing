import {call, delay, put, takeEvery} from "redux-saga/effects";
import * as config from '../common/const';
import userAction from '../action/userAction';
import userApi from '../api/userApi';

function* login({payload}){
    const data = payload;
    const resp = yield call(userApi.apiLogin, data);
    if("SUCCESS" === resp.status){
        yield put(userAction.loginSuccess(resp))
    }
    else{
        yield put(userAction.loginFailed(resp))
    }
}

function* signUp({payload}){
    const data = payload;
    const resp = yield call(userApi.apiSignUp, data);
    if("SUCCESS" === resp.status){
        yield put(userAction.signUpSuccess(resp))
    }
    else{
        yield put(userAction.signUpFailed(resp))
    }
}

function* getForgotPass({payload}){
    debugger
    const data = payload;
    const resp = yield call(userApi.apiGetForgotPass, data);
    if("SUCCESS" === resp.status){
        yield put(userAction.getForgotPassSuccess(resp))
    }
    else{
        yield put(userAction.getForgotPassFailed(resp))
    }
}

function* userSaga(){
    yield takeEvery(config.LOGIN, login)
    yield takeEvery(config.SIGN_UP, signUp)
    yield takeEvery(config.GET_FORGOT_PASS, getForgotPass)
}

export default userSaga