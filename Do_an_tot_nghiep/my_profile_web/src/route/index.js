import React from 'react';
import { BrowserRouter,
  Routes,
  Route } from 'react-router-dom';

import Login from "../view/login";
import SignUp from "../view/signup";
import ForgotPass from "../view/login/forgotPass";
import Home from "../view/home";
import Home2 from "../view/home2"
import Admin from "../view/admin";

class Router extends React.Component{
    render() {
        return(
            <div>
                <Switch>
                    <Route path="/" component={Home2} exact />
                    <Route path="/pages/login" component={Login}  />
                    <Route path="/pages/sign-up" component={SignUp} />
                    <Route path="/pages/forgot-pass" component={ForgotPass}/>
                    <Route path="/pages/admin" component={Admin} />
                </Switch>
            </div>
        );
    }
}

export default Router;