package com.tungphongdo.service.serviceImplement;

import com.tungphongdo.config.MailConfig;
import com.tungphongdo.dto.UserDTO;
import com.tungphongdo.jwt.JwtService;
import com.tungphongdo.repository.UserRepository;
import com.tungphongdo.service.UserService;
import com.tungphongdo.util.Constant;
import com.tungphongdo.util.ServiceResult;
import net.bytebuddy.utility.RandomString;
import org.apache.commons.lang3.StringUtils;
import org.mindrot.jbcrypt.BCrypt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImp implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private JwtService jwtService;

    @Autowired
    private MailConfig mailConfig;

    @Override
    public ServiceResult login(UserDTO userDTO) {

        ServiceResult result = new ServiceResult();

        List<UserDTO> userDTOS = userRepository.findAllUser();

        for (UserDTO userDTO1 : userDTOS) {
            boolean valuate = BCrypt.checkpw(userDTO.getPassword(), userDTO1.getPassword());

            if(StringUtils.equals(userDTO.getUsername(), userDTO1.getUsername()) && valuate){
                result.setToken(jwtService.generateTokenLogin(userDTO.getUsername()));
                result.setStatus(ServiceResult.Status.SUCCESS);
                result.setData(userDTO1);
                result.setMessage(Constant.LOGIN_SUCCESS);
                return result;
            }
            else {
                result.setStatus(ServiceResult.Status.FAILED);
                result.setMessage(Constant.LOGIN_FAILED);
            }
        }

        return result;
    }

    @Override
    public ServiceResult createUser(UserDTO userDTO) {
        ServiceResult result = new ServiceResult();

        UserDTO userDTOForUsername = userRepository.findByUsername(userDTO.getUsername());
        UserDTO userDTOForEmail = userRepository.findByEmail(userDTO.getEmail());

        if(null == userDTOForUsername && null == userDTOForEmail){
            String encodePass = BCrypt.hashpw(userDTO.getPassword(), BCrypt.gensalt(12));
            userDTO.setPassword(encodePass);

            userDTO.setEnabled(1);

            userRepository.save(userDTO.toModel());
            result.setStatus(ServiceResult.Status.SUCCESS);
            result.setMessage(Constant.SIGN_UP_SUCCESS);
            return result;
        }
        else {
            if( null !=  userDTOForUsername){
                result.setStatus(ServiceResult.Status.FAILED);
                result.setMessage(Constant.USERNAME_EXIST);
                return result;
            }
            if( null !=  userDTOForEmail){
                result.setStatus(ServiceResult.Status.FAILED);
                result.setMessage(Constant.EMAIL_EXIST);
                return result;
            }
        }

        return result;
    }

    @Override
    public ServiceResult updateUser(UserDTO userDTO) {

        return null;
    }

    @Override
    public ServiceResult forgotPass(UserDTO userDTO) {
        ServiceResult result = new ServiceResult();

        UserDTO userDTO1 = userRepository.findByEmail(userDTO.getEmail());

        if(null != userDTO1){

            RandomString gen = new RandomString(5);
            String newPass = gen.nextString();

            SimpleMailMessage message = new SimpleMailMessage();
            message.setTo(userDTO.getEmail());
            message.setSubject("Get password");
            message.setText("Mật khẩu mới của bạn là: " + newPass);
            mailConfig.getJavaMailSender().send(message);

            String encodePass = BCrypt.hashpw(newPass, BCrypt.gensalt(12));
            userDTO1.setPassword(encodePass);
            userRepository.save(userDTO1.toModel());

            result.setStatus(ServiceResult.Status.SUCCESS);
            result.setMessage(Constant.GET_PASS_SUCCESS);
            return result;
        }
        else {
            result.setStatus(ServiceResult.Status.FAILED);
            result.setMessage(Constant.EMAIL_NOT_EXIST);
        }

        return result;
    }


}
