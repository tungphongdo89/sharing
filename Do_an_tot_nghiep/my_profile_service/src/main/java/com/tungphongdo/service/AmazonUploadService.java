package com.tungphongdo.service;

import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface AmazonUploadService {
    List<String> uploadFile(MultipartFile multipartFile);
    void deleteFile(String name);
}
