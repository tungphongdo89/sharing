package com.tungphongdo.controller;

import com.tungphongdo.dto.UserDTO;
import com.tungphongdo.service.serviceImplement.UserServiceImp;
import com.tungphongdo.util.ServiceResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = {"/api"})
public class UserController {

    @Autowired
    private UserServiceImp userServiceImp;

    @PostMapping("/login")
    public ResponseEntity<ServiceResult> login(@RequestBody UserDTO userDTO){
        ResponseEntity<ServiceResult> responseEntity = new ResponseEntity<>(userServiceImp.login(userDTO), HttpStatus.OK);
        return  responseEntity;
    }

    @PostMapping("/forgotPass")
    public ResponseEntity<ServiceResult> forgotPass(@RequestBody UserDTO userDTO){
        ResponseEntity<ServiceResult> responseEntity = new ResponseEntity<>(userServiceImp.forgotPass(userDTO), HttpStatus.OK);
        return  responseEntity;
    }

    @PostMapping("/createUser")
    public ResponseEntity<ServiceResult> createUser(@RequestBody UserDTO userDTO){
        ResponseEntity<ServiceResult> responseEntity = new ResponseEntity<>(userServiceImp.createUser(userDTO), HttpStatus.OK);
        return  responseEntity;
    }


}
