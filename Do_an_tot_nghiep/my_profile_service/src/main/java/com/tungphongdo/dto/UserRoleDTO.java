package com.tungphongdo.dto;

import com.tungphongdo.model.UserRole;
import lombok.Data;

@Data
public class UserRoleDTO {

    private Integer id;
    private Integer userId;
    private Integer roleId;

    public UserRoleDTO(){

    }
    public UserRoleDTO(Integer id, Integer userId, Integer roleId){
        this.id = id;
        this.userId = userId;
        this.roleId = roleId;
    }

    public UserRole toModel(){
        UserRole userRole = new UserRole();
        userRole.setId(id);
        userRole.setUserId(userId);
        userRole.setRoleId(roleId);
        return userRole;
    }
}
