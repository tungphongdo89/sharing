package com.tungphongdo.dto;

import com.tungphongdo.model.Role;
import lombok.Data;

@Data
public class RoleDTO {

    private Integer id;
    private String roleName;
    private Integer parentId;

    public RoleDTO(){

    }

    public RoleDTO(Integer id, String roleName, Integer parentId){
        this.id = id;
        this.roleName = roleName;
        this.parentId = parentId;
    }

    public Role toModel(){
        Role role = new Role();
        role.setId(id);
        role.setRoleName(roleName);
        role.setParentId(parentId);
        return role;
    }
}
