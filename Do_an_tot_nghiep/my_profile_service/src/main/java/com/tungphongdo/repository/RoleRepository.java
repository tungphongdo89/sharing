package com.tungphongdo.repository;

import com.tungphongdo.dto.RoleDTO;
import com.tungphongdo.dto.UserDTO;
import com.tungphongdo.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends JpaRepository<Role, Integer> {

    @Query("select new com.tungphongdo.dto.RoleDTO(r.id, r.roleName, r.parentId) from Role r where r.id = :roleId")
    RoleDTO findByRoleId(@Param("roleId") int roleId);

//    @Query("select new com.tungphongdo.dto.UserDTO(u.id, u.username, u.password, " +
//            "u.fullName, u.gender, u.email, u.address, u.enabled, u.parentId) from User u where u.id = :id")
//    UserDTO findById(@Param("id") int id);

}

