package com.tungphongdo.repository;

import com.tungphongdo.dto.UserDTO;
import com.tungphongdo.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {


    @Query("select new com.tungphongdo.dto.UserDTO(u.id, u.username, u.password, u.fullName, u.gender, u.email, " +
            "u.address, u.enabled, u.parentId) from User u")
    List<UserDTO> findAllUser();

    @Query("select new com.tungphongdo.dto.UserDTO(u.id, u.username, u.password, " +
            "u.fullName, u.gender, u.email, u.address, u.enabled, u.parentId) from User u where u.username = :username")
    UserDTO findByUsername(@Param("username") String username);

    @Query("select new com.tungphongdo.dto.UserDTO(u.id, u.username, u.password, " +
            "u.fullName, u.gender, u.email, u.address, u.enabled, u.parentId) from User u where u.email = :email")
    UserDTO findByEmail(@Param("email") String email);

    @Query("select new com.tungphongdo.dto.UserDTO(u.id, u.username, u.password, " +
            "u.fullName, u.gender, u.email, u.address, u.enabled, u.parentId) from User u where u.id = :id")
    UserDTO findById(@Param("id") int id);

}
