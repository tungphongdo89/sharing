package com.tungphongdo.repository;

import com.tungphongdo.dto.UserRoleDTO;
import com.tungphongdo.model.UserRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRoleRepository extends JpaRepository<UserRole, Integer> {

//    @Query("select new com.tungphongdo.dto.UserRoleDTO(ur.id, ur.userId, ur.roleId) from UserRole as ur where ur.userId = ?1")
//    List<UserRoleDTO> getListUserRolesByUserId(int userId);

    @Query("select new com.tungphongdo.dto.UserRoleDTO(ur.id, ur.userId, ur.roleId) from UserRole ur where ur.userId = :userId")
    List<UserRoleDTO> getListUserRolesByUserId(@Param("userId") int userId);

}
