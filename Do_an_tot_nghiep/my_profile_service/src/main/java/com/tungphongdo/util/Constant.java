package com.tungphongdo.util;

public class Constant {

    public static final String LOGIN_SUCCESS = "Đăng nhập thành công";
    public static final String LOGIN_FAILED = "Tên tài khoản hoặc mật khẩu không chính xác";

    public static final String MY_EMAIL = "tungphongdo89@gmail.com";  //"tung.hoang@migitek.com"
    public static final String MY_PASSWORD = "tungbengbeng";
    public static final String EMAIL_NOT_EXIST = "Email không tồn tại";
    public static final String GET_PASS_SUCCESS = "Lấy mật khẩu thành công, vui lòng kiểm tra email của bạn để nhận mật khẩu";

    public static final String SIGN_UP_SUCCESS = "Đăng ký tài khoản thành công";
    public static final String USERNAME_EXIST = "Tên đăng nhập đã có người sử dụng, vui lòng nhập tên tài khoản khác";
    public static final String EMAIL_EXIST = "Email đã có người sử dụng, vui lòng nhập email khác";
}
