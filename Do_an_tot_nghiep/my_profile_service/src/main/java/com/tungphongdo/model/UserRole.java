package com.tungphongdo.model;

import com.tungphongdo.dto.UserRoleDTO;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "user_role")
public class UserRole {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Integer id;

    @Column(name = "USER_ID")
    private Integer userId;

    @Column(name = "ROLE_ID")
    private Integer roleId;

    public UserRoleDTO toModel(){
        UserRoleDTO userRoleDTO = new UserRoleDTO();
        userRoleDTO.setId(id);
        userRoleDTO.setUserId(userId);
        userRoleDTO.setRoleId(roleId);
        return  userRoleDTO;
    }
}
