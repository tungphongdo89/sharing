package com.tungphongdo.model;

import com.tungphongdo.dto.RoleDTO;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "role")
public class Role {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Integer id;

    @Column(name = "ROLE_NAME")
    private String roleName;

    @Column(name = "PARENT_ID")
    private Integer parentId;

    public RoleDTO toModel(){
        RoleDTO roleDTO = new RoleDTO();
        roleDTO.setId(id);
        roleDTO.setRoleName(roleName);
        roleDTO.setParentId(parentId);
        return roleDTO;
    }
}
