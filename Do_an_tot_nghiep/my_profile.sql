-- create database shop;
-- use shop;
use tungphon_myshop;
create table user (
	ID int not null auto_increment,
    USERNAME nvarchar(50)not null,
    PASSWORD nvarchar(255) not null,
    FULLNAME nvarchar(255),
    GENDER int,
    EMAIL nvarchar(100),
    ADDRESS nvarchar(500),
    ENABLED int,
    PARENT_ID int,
    primary key(ID)
);

create table role (
	ID int auto_increment auto_increment,
    ROLE_NAME varchar(10),
    PARENT_ID int,
    primary key(ID)
);
create table user_role(
	ID int auto_increment auto_increment,
    USER_ID int,
    ROLE_ID int,
    primary key(ID)
);

insert into user(USERNAME, PASSWORD, FULLNAME, GENDER, EMAIL, ADDRESS, ENABLED, PARENT_ID) 
values('tungtung', '12345','Hoàng Thanh Tùng', 0, 'tungphongdo89@gmail.com', 'Thanh Hóa', 1, null );
insert into user(USERNAME, PASSWORD, FULLNAME, GENDER, EMAIL, ADDRESS, ENABLED, PARENT_ID) 
values('thomthom', '12345','Trần Thị Thơm', 1, 'thomtrantt99@gmail.com', 'Thái Bình', 1, null );
insert into user(USERNAME, PASSWORD, FULLNAME, GENDER, EMAIL, ADDRESS, ENABLED, PARENT_ID) 
values('anhanh', '12345','Nguyễn Văn Anh', 0, 'anhanh88@gmail.com', 'Hà Nội', 1, null );

insert into role(ROLE_NAME, PARENT_ID) values ('ROLE_ADMIN', null);
insert into role(ROLE_NAME, PARENT_ID) values ('ROLE_USER', null);

insert into user_role(USER_ID, ROLE_ID) values (1, 1);
insert into user_role(USER_ID, ROLE_ID) values (2, 2);
insert into user_role(USER_ID, ROLE_ID) values (3, 2);



