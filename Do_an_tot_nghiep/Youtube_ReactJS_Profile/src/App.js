import React from 'react';
// import logo from './logo.svg';
import './App.css';
import Header from './Components/header'
import Navbar from './Components/Navbar'
import Banner from './Components/Banner'
import About from './Components/About'
import Project from './Components/Project'
import Portfolio from './Components/Portfolio'
import Contact from './Components/Contact'
import { Helmet } from 'react-helmet'

const TITLE = 'My profile'

function App() {
  return (
      <>
          {/*<Helmet>*/}
              {/*<title>{ TITLE }</title>*/}
          {/*</Helmet>*/}
          <div style={{backgroundColor: 'black'}} className="App">
              <Header />
              <hr />
              <Navbar />
              <br />
              <Banner />
              <br />
              <About />
              <br />
              <hr />
              <h1 style={{fontWeight: '700'}} className="skillheader">CÁC KỸ NĂNG</h1>
              <hr />
              <Project />
              <hr />
              <Portfolio />
              <hr />
              <h1 id="contactnav" className="contact">THÔNG TIN LIÊN HỆ</h1>
              <hr />
              <Contact />

          </div>
      </>

  );
}

export default App;
