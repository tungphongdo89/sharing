package com.tungphongdo.service.serviceImplement;

import com.tungphongdo.dto.UserDTO;
import com.tungphongdo.jwt.JwtService;
import com.tungphongdo.model.User;
import com.tungphongdo.repository.UserRepository;
import com.tungphongdo.service.UserService;
import com.tungphongdo.util.ServiceResult;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImp implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private JwtService jwtService;

    @Override
    public ServiceResult login(UserDTO userDTO) {
        ServiceResult result = new ServiceResult();

        List<UserDTO> userDTOS = userRepository.findAllUser();

        for (UserDTO userDTO1 : userDTOS) {
            if(StringUtils.equals(userDTO.getUsername(), userDTO1.getUsername()) &&
                    StringUtils.equals(userDTO.getPassword(), userDTO1.getPassword())){
                result.setToken(jwtService.generateTokenLogin(userDTO.getUsername()));
                result.setStatus(ServiceResult.Status.SUCCESS);
                result.setMessage("Đăng nhập thành công");
                return result;
            }
            else {
                result.setStatus(ServiceResult.Status.FAILED);
                result.setMessage("Tên tài khoản hoặc mật khẩu không chính xác");
            }
        }

        return result;
    }
}
