package com.tungphongdo.service;

import com.tungphongdo.dto.UserDTO;
import com.tungphongdo.util.ServiceResult;

public interface UserService {

    ServiceResult login(UserDTO userDTO);
}
