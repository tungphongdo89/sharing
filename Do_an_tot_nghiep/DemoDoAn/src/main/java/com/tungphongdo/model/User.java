package com.tungphongdo.model;

import com.tungphongdo.dto.UserDTO;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "user")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private int id;

    @Column(name = "USERNAME")
    private String username;

    @Column(name = "PASSWORD")
    private String password;

    @Column(name = "FULLNAME")
    private String fullName;

    @Column(name = "GENDER")
    private int gender;

    @Column(name = "EMAIL")
    private String email;

    @Column(name = "ADDRESS")
    private String address;

    @Column(name = "ENABLED")
    private int enabled;

    @Column(name = "PARENT_ID")
    private int parentId;

    public UserDTO toModel(){
        UserDTO userDTO = new UserDTO();
        userDTO.setId(id);
        userDTO.setUsername(username);
        userDTO.setPassword(password);
        userDTO.setFullName(fullName);
        userDTO.setGender(gender);
        userDTO.setEmail(email);
        userDTO.setAddress(address);
        userDTO.setEnabled(enabled);
        userDTO.setParentId(parentId);
        return userDTO;
    }

}
