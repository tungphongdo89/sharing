package com.tungphongdo.model;

import com.tungphongdo.dto.UserRoleDTO;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "user_role")
public class UserRole {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private int id;

    @Column(name = "USER_ID")
    private int userId;

    @Column(name = "ROLE_ID")
    private int roleId;

    public UserRoleDTO toModel(){
        UserRoleDTO userRoleDTO = new UserRoleDTO();
        userRoleDTO.setId(id);
        userRoleDTO.setUserId(userId);
        userRoleDTO.setRoleId(roleId);
        return  userRoleDTO;
    }
}
