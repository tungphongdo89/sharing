package com.tungphongdo.util;

import lombok.Data;

import java.util.List;

@Data
public class ServiceResult {

    private Status status = Status.SUCCESS;
    private String message;
    private Object data;
    private List<Integer> listPages;
    private String token;
    public enum Status {
        SUCCESS, FAILED;
    }

}
