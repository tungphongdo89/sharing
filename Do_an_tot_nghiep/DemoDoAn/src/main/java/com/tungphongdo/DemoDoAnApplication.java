package com.tungphongdo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoDoAnApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoDoAnApplication.class, args);
	}

}
