package com.tungphongdo.jwt;

import com.tungphongdo.dto.RoleDTO;
import com.tungphongdo.dto.UserDTO;
import com.tungphongdo.dto.UserRoleDTO;
import com.tungphongdo.repository.RoleRepository;
import com.tungphongdo.repository.UserRepository;
import com.tungphongdo.repository.UserRoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/*Class này dùng để xác thực người dùng*/
public class JwtAuthenticationTokenFilter extends UsernamePasswordAuthenticationFilter {
    private final static String TOKEN_HEADER = "authorization";

    @Autowired
    private JwtService jwtService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserRoleRepository userRoleRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        String authToken = httpRequest.getHeader(TOKEN_HEADER);
        if (jwtService.validateTokenLogin(authToken)) {
            String username = jwtService.getUsernameFromToken(authToken);
            UserDTO userDTO = userRepository.findByUsername(username);

            List<RoleDTO> roleDTOS = new ArrayList<>();
            List<UserRoleDTO> userRoleDTOS = userRoleRepository.getListUserRolesByUserId(userDTO.getId());

            for (UserRoleDTO userRoleDTO: userRoleDTOS) {
                RoleDTO roleDTO = roleRepository.findById(userRoleDTO.getRoleId());
                roleDTOS.add(roleDTO);
            }

            UserDTO userDTO1 = new UserDTO();
            List<GrantedAuthority> authorities = userDTO1.getAuthorities(roleDTOS);

            if (userDTO1 != null) {
                boolean enabled = true;
                boolean accountNonExpired = true;
                boolean credentialsNonExpired = true;
                boolean accountNonLocked = true;
                UserDetails userDetail = new User(username, userDTO1.getPassword(), enabled, accountNonExpired,
                        credentialsNonExpired, accountNonLocked, authorities);
                UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userDetail,
                        null, userDetail.getAuthorities());
                authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(httpRequest));
                SecurityContextHolder.getContext().setAuthentication(authentication);
            }
        }
        chain.doFilter(request, response);
    }


}
