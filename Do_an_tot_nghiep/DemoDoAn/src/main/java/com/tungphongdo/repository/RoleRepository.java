package com.tungphongdo.repository;

import com.tungphongdo.dto.RoleDTO;
import com.tungphongdo.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends JpaRepository<Role, Integer> {

    @Query("select new com.tungphongdo.dto.RoleDTO(r.id, r.roleName, r.parentId) from Role r where r.id = :roleId")
    RoleDTO findById(@Param("roleId") int roleId);
}
