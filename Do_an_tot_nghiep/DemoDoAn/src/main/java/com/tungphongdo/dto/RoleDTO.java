package com.tungphongdo.dto;

import com.tungphongdo.model.Role;
import lombok.Data;

@Data
public class RoleDTO {

    private int id;
    private String roleName;
    private int parentId;

    public RoleDTO(){

    }

    public RoleDTO(int id, String roleName, int parentId){
        this.id = id;
        this.roleName = roleName;
        this.parentId = parentId;
    }

    public Role toModel(){
        Role role = new Role();
        role.setId(id);
        role.setRoleName(roleName);
        role.setParentId(parentId);
        return role;
    }
}
