package com.tungphongdo.dto;

import com.tungphongdo.model.User;
import lombok.Data;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.ArrayList;
import java.util.List;

@Data
public class UserDTO {
    private int id;
    private String username;
    private String password;
    private String fullName;
    private int gender;
    private String email;
    private String address;
    private int enabled;
    private int parentId;

    public UserDTO(){

    }
    public UserDTO(int id, String username, String password, String fullName, int gender, String email,
                   String address, int enabled, int parentId){
        this.id = id;
        this.username = username;
        this.password = password;
        this.fullName = fullName;
        this.gender = gender;
        this.email = email;
        this.address = address;
        this.enabled = enabled;
        this.parentId = parentId;
    }

    //lấy ra các role mà user này có
    public List<GrantedAuthority> getAuthorities(List<RoleDTO> listRoleDTOs){
        List<GrantedAuthority> authorities = new ArrayList<>();
        for (RoleDTO roleDTO : listRoleDTOs) {
            authorities.add(new SimpleGrantedAuthority(roleDTO.getRoleName()));
        }
        return authorities;
    }



    public User toModel(){
        User user = new User();
        user.setId(id);
        user.setUsername(username);
        user.setPassword(password);
        user.setFullName(fullName);
        user.setGender(gender);
        user.setEmail(email);
        user.setAddress(address);
        user.setEnabled(enabled);
        user.setParentId(parentId);

        return user;
    }

}
