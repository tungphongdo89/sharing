package com.tungphongdo.dto;

import com.tungphongdo.model.UserRole;
import lombok.Data;

@Data
public class UserRoleDTO {

    private int id;
    private int userId;
    private int roleId;

    public UserRoleDTO(){

    }
    public UserRoleDTO(int id, int userId, int roleId){
        this.id = id;
        this.userId = userId;
        this.roleId = roleId;
    }

    public UserRole toModel(){
        UserRole userRole = new UserRole();
        userRole.setId(id);
        userRole.setUserId(userId);
        userRole.setRoleId(roleId);
        return userRole;
    }
}
