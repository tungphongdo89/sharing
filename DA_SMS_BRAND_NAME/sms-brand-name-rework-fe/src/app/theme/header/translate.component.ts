import {Component} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {SettingsService} from '@core';

@Component({
  selector: 'app-translate',
  templateUrl: './translate.component.html',
  styles: [],
})
export class TranslateComponent {
  langs: any;
  langSelected: string = 'vi-VN';

  constructor(private _translate: TranslateService, private _settings: SettingsService) {
    _translate.addLangs(['vi-VN', 'en-US']);
    _translate.setDefaultLang('vi-VN');

    // const browserLang = navigator.language;
    let browserLang = localStorage.getItem('lang');
    if (!browserLang)
      browserLang = 'vi-VN';
    this.langSelected = browserLang;
    _translate.use(browserLang.match(/vi-VN|en-US/) ? browserLang : 'vi-VN');
    this.langs = {
      'vi-VN': 'common.language-vi',
      'en-US': 'common.language-en'
    };
  }

  useLanguage(language: string) {
    this._translate.use(language);
    this._settings.setLanguage(language);
    this.langSelected = language;
    window.location.reload();
  }
}
