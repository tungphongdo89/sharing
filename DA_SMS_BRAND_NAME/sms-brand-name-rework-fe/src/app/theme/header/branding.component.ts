import {Component} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-branding',
  template: `
    <div class="cursor" (click)="route()">
      <div class="row matero-branding ml-1 mt-1">
        <!--      <img src="./assets/icons/Logo_epass.svg" class="matero-branding-logo-expanded" alt="logo" />-->
        <h4 class="matero-branding-name">CRM</h4>
      </div>
      <span class="row small-name ml-1">Hệ thống chăm sóc khách hàng</span>
    </div>
  `,
  styles: [`
    .matero-branding-name {
      font-weight: bold;
    }

    .cursor:hover {
      cursor: pointer;
    }

    .matero-branding-name:hover {
      color: white;
    }

    .matero-branding:hover {
      text-decoration: none;
    }

    .small-name {
      font-size: 0.7rem;
      margin-top: -1rem;
    }
  `]
})
export class BrandingComponent {

  constructor(private router: Router) {
  }

  route() {
    this.router.navigateByUrl('');
  }
}
