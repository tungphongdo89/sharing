import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ProfileService } from '@app/core/services/profile/profile.service';
import { TranslateService } from '@ngx-translate/core';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  constructor(
    protected translateService: TranslateService,
    private toastr: ToastrService,
    private fb: FormBuilder,
    private profileService: ProfileService
  ) { }

  updateProfileForm: FormGroup = this.fb.group({
    firstName: [null, [Validators.required]],
    email: [null, [Validators.required]],
    phoneNumber: [null],
    authorities: [[]]
  });

  userData: any = null;
  updateData:any = null;

  ngOnInit(): void {
    this.getMyProfile();
  }

  getMyProfile() {
    this.profileService.getMyProfile().subscribe(res => {
      this.userData = res.body;
      this.updateData = Object.assign({}, res.body);

      if (res.body.authorities !== null && res.body.authorities.length > 0) {
        let authoritiesList: string[] = this.userData.authorities;
        let authorities: string[] = [];
        authoritiesList.forEach(x => {
          let y = " " + x.toLowerCase().replace('role_', '');
          authorities.push(y);
        })
        this.userData.authorities = authorities.toString();
      }
      this.updateProfileForm.patchValue(this.userData);
    }, error => {
      this.toastr.error("Có lỗi xảy ra")
    })
  }

  updateProfile() {
    this.updateProfileForm.markAllAsTouched();
    if (this.updateProfileForm.invalid) {
      this.toastr.error(this.translateService.instant("common.notify.update.error"));
    } else {
      this.updateData.firstName = this.updateProfileForm.value.firstName;
      this.updateData.email = this.updateProfileForm.value.email;

      this.profileService.updateProfile(this.updateData).subscribe(res => {
        this.toastr.success(this.translateService.instant("common.notify.save.success"));
      }, error => {
        this.toastr.error(this.translateService.instant("common.notify.update.error"));
        this.toastr.error(error.error.message);
        console.log(error.error.message);
      })
    }
  }

  
}
