import {Component} from '@angular/core';
import {LocalStorageService, SessionStorageService} from 'ngx-webstorage';
import {AccountService} from '@core/auth/account.service';
import {LoginService} from '@core/auth/login.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
})
export class UserComponent {
  userName: string;

  constructor(private localStorage: LocalStorageService,
              private sessionStorage: SessionStorageService,
              private accountService: AccountService,
              private authService: LoginService) {
    this.accountService.identity().subscribe((account: any) => {
      this.userName = account.login;
    });
  }

  logout() {
    this.authService.logout();
  }
}
