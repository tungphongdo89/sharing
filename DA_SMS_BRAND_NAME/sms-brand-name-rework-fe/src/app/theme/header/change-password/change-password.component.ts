import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LoginService } from '@app/core/auth/login.service';
import { ProfileService } from '@app/core/services/profile/profile.service';
import { TranslateService } from '@ngx-translate/core';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {

  constructor(
    protected translateService: TranslateService,
    private toastr: ToastrService,
    private fb: FormBuilder,
    private profileService: ProfileService,
    private router: Router,
    private authService: LoginService
  ) { }

  changeMyPasswordForm: FormGroup = this.fb.group({
    oldPassword: [null, [Validators.required]],
    newPassword: [null, [Validators.required]],
    confirmNewPassword: [null, [Validators.required]],
  })

  ngOnInit(): void {

  }

  changePassword() {
    this.changeMyPasswordForm.markAllAsTouched();
    if (this.changeMyPasswordForm.invalid) {
      this.toastr.error(this.translateService.instant("common.notify.update.error"));
    } else {
      // console.log(this.changeMyPasswordForm.value);
      this.profileService.changeMyPassword(this.changeMyPasswordForm.value).subscribe(res => {
        if (res.body.message === null) {
          this.toastr.success(this.translateService.instant('common.notify.save.success'));
          this.changeMyPasswordForm.reset();
          this.reloadComponent();
        } else if (res.body.message === "1") { // lan dau dang nhap
          this.toastr.success(this.translateService.instant('common.notify.save.success'));
          this.toastr.info(this.translateService.instant('login.notify-login-again'));
          this.authService.logout();
        } else {
          this.toastr.error(res.body.message);
          console.error(res.body.message);
        }
      })
    }
  }

  reloadComponent() {
    const currentUrl = this.router.url;
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
    this.router.onSameUrlNavigation = 'reload';
    this.router.navigate([currentUrl]);
  }
}
