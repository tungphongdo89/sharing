import {Component, Input} from '@angular/core';
import {Menu, MenuService} from '@core';

@Component({
  selector: 'app-sidemenu',
  templateUrl: './sidemenu.component.html',
})
export class SidemenuComponent {
  // NOTE: Ripple effect make page flashing on mobile
  @Input() ripple = false;

  menus = this._menu.getAll();

  constructor(private _menu: MenuService) {
    this.getMenuByLogin();
  }

  // Delete empty values and rebuild route
  buildRoute(routes: string[]) {
    let route = '';
    routes.forEach(item => {
      if (item && item.trim()) {
        route += '/' + item.replace(/^\/+|\/+$/g, '');
      }
    });
    return route;
  }

  getMenuByLogin(login?: any) {
    this._menu.getMenuByLogin(login).subscribe((value: Menu[]) => {
      if (value) {
        this._menu.recursMenuForTranslation(value, 'menu');
        this._menu.set(value);
        this._menu.setCodeList(value);
      }
    });
  }
}
