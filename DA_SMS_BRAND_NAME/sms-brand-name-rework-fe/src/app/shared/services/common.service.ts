import { Injectable } from '@angular/core';
import {finalize} from 'rxjs/operators';
import {HttpClient} from '@angular/common/http';
import {HelperService} from '@shared/services/helper.service';
import {ToastrService} from 'ngx-toastr';
import {TranslateService} from '@ngx-translate/core';
import * as fileSaver from 'file-saver';

@Injectable({
  providedIn: 'root'
})
export class CommonService {

  constructor(
    public httpClient: HttpClient,
    public helperService: HelperService,
    private toastr: ToastrService,
    private translateService: TranslateService
  ) { }


  downloadFile(url: string, data?: any, params?: any, fileName?: string, mimeType?: any) {
    this.helperService.isProcessing(true);
    this.httpClient.post(url, data, {
      observe: 'response',
      responseType: 'arraybuffer',
      params
    })
      .pipe(
        finalize(() => {
          this.helperService.isProcessing(false);
        })
      ).subscribe(res => {
      this.helperService.APP_TOAST_MESSAGE.next(res);
      try {
        const response = JSON.parse(new TextDecoder('utf-8').decode(res.body));
        this.toastr.error(this.translateService.instant(response.status.message));
      } catch {
        this.saveFile(res.body, fileName, mimeType);
      }
    }, error => {
      this.helperService.APP_TOAST_MESSAGE.next(error);
      this.toastr.error(this.translateService.instant('common.notify.fail'));
    });
  }

  saveFile(data: any, filename?: string, mimeType?: any) {
    const blob = new Blob([data], {type: mimeType || 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'});
    fileSaver.saveAs(blob, filename);
  }
}
