import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';

import {MaterialModule} from '../material.module';
import {MaterialExtensionsModule} from '@ng-matero/extensions';
import {FlexLayoutModule} from '@angular/flex-layout';
import {NgProgressModule} from 'ngx-progressbar';
import {NgProgressHttpModule} from 'ngx-progressbar/http';
import {NgProgressRouterModule} from 'ngx-progressbar/router';
import {NgSelectModule} from '@ng-select/ng-select';
import {FormlyModule} from '@ngx-formly/core';
import {FormlyMaterialModule} from '@ngx-formly/material';
import {ToastrModule} from 'ngx-toastr';
import {TranslateModule, TranslateService} from '@ngx-translate/core';

import {BreadcrumbComponent} from './components/breadcrumb/breadcrumb.component';
import {PageHeaderComponent} from './components/page-header/page-header.component';
import {ErrorCodeComponent} from './components/error-code/error-code.component';

import {DisplayDateMonthPipe} from './pipes/display-date-month.pipe';
import {DisplayHelperPipe} from './pipes/display-helper.pipe';
import {DisplayDatePipe} from './pipes/display-date.pipe';
import {FindByPipe, FormatCurrencyPipe} from './pipes/format-currency.pipe';
import {SafePipe} from './pipes/safe.pipe';

import {InputSpecialDirective} from './directives/input-special.directive';
import {InputTrimDirective} from './directives/input-trim.directive';
import {AutoFocusDirective} from './directives/auto-focus.directive';
import {RemoveWrapperDirective} from './directives/remove-wrapper';
import {FormatCurrencyDirective} from './directives/format-currency.directive';
import {ConfirmDialogComponent} from './components/confirm-dialog/confirm-dialog.component';
import {TreeComponentComponent} from './components/tree-component/tree-component.component';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';
import {MatPaginatorIntl} from '@angular/material/paginator';
import {PaginatorIntlService} from './utils/paging';
import {InlineMessageComponent} from './components/inline-message/inline-message.component';
import {OnlyNumberDirective} from '@shared/directives/only-number.directive';
import {FormatNumberPipe} from '@shared/pipes/format-number.pipe';
import {CommonService} from '@shared/common/common.service';
import {DragDirective} from '@shared/directives/drag.directive';
import {UploadFileComponent} from '@shared/components/upload-file/upload-file.component';
import {EditorComponent} from './components/editor/editor.component';
import {RichTextEditorModule} from '@syncfusion/ej2-angular-richtexteditor';
import {ProvinceCodeDirective} from '@shared/directives/province-code.directive';
import {OnlyNumberCharacterDirective} from '@shared/directives/only-number-character.directive';
import {SelectLazyLoadComponent} from './components/select-lazy-load/select-lazy-load.component';
import {AutoFocusFormDirective} from '@shared/directives/auto-focus-form.directive';
import {RemoveVnmDirective} from '@shared/directives/remove-vnm.directive';
import {RemoveCharacterDirective} from '@shared/directives/remove_character.directive';
import {OnlyCommasNumberDirective} from '@shared/directives/only-commas-number.directive';
import {TaxCodeDirective} from '@shared/directives/tax-code.directive';
import {ViNumberPipe} from '@shared/pipes/vi-number.pipe';
import {OnlyCharacterNormalDirective} from '@shared/directives/only-character-normal.directive';
import {ValueToTextPipe} from '@shared/pipes/value-to-text.pipe';
import {InlineMessageAdvanceComponent} from './components/inline-message-advance/inline-message-advance.component';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import {MomentDateAdapter} from '@angular/material-moment-adapter';
import {FORMATS_DATE, FORMATS_DATETIME} from '@shared/common/format-date';
// @ts-ignore
import {NGX_MAT_DATE_FORMATS} from '@angular-material-components/datetime-picker';
import {TimePickerComponent} from '@shared/components/time-picker/time-picker.component';
import { StepCustomComponent } from './components/step-custom/step-custom.component';
import {FormErrorMessagePipe} from '@shared/pipes/form-error-message.pipe';
import { TreeSelectComponent } from './components/tree-select/tree-select.component';
import { DateInputDirective } from '@shared/directives/date-input.directive';
import {AccumulationChartAllModule, ChartAllModule} from '@syncfusion/ej2-angular-charts';
import {ActionPermissionDirective} from '@core/directives/action-permission.directive';

const THIRD_MODULES = [
  MaterialModule,
  MaterialExtensionsModule,
  FlexLayoutModule,
  NgProgressModule,
  NgProgressRouterModule,
  NgProgressHttpModule,
  NgSelectModule,
  FormlyModule,
  FormlyMaterialModule,
  ToastrModule,
  CKEditorModule,
  TranslateModule,
  ChartAllModule,
  AccumulationChartAllModule
];
const COMPONENTS = [
  BreadcrumbComponent,
  PageHeaderComponent,
  ErrorCodeComponent,
  ConfirmDialogComponent,
  InlineMessageComponent,
  UploadFileComponent,
  EditorComponent,
  SelectLazyLoadComponent,
  TimePickerComponent
];
const COMPONENTS_DYNAMIC = [TreeComponentComponent];
const DIRECTIVES = [
  InputSpecialDirective,
  InputTrimDirective,
  AutoFocusDirective,
  RemoveWrapperDirective,
  FormatCurrencyDirective,
  OnlyNumberDirective,
  DragDirective,
  ProvinceCodeDirective,
  OnlyNumberCharacterDirective,
  AutoFocusFormDirective,
  RemoveCharacterDirective,
  RemoveVnmDirective,
  OnlyCommasNumberDirective,
  TaxCodeDirective,
  OnlyCharacterNormalDirective,
  DateInputDirective,
  ActionPermissionDirective
];
const PIPES = [
  DisplayHelperPipe,
  DisplayDatePipe,
  DisplayDateMonthPipe,
  SafePipe,
  FormatCurrencyPipe,
  FindByPipe,
  FormatNumberPipe,
  ViNumberPipe,
  ValueToTextPipe,
  FormErrorMessagePipe
];

@NgModule({
  declarations: [...COMPONENTS, ...COMPONENTS_DYNAMIC, ...DIRECTIVES, ...PIPES, SelectLazyLoadComponent, InlineMessageAdvanceComponent, StepCustomComponent, TreeSelectComponent],
  imports: [CommonModule, RichTextEditorModule, FormsModule, RouterModule, ReactiveFormsModule, ...THIRD_MODULES],
    exports: [
        CommonModule,
        FormsModule,
        RouterModule,
        ReactiveFormsModule,
        ...THIRD_MODULES,
        ...COMPONENTS,
        ...DIRECTIVES,
        ...PIPES,
        InlineMessageAdvanceComponent,
        StepCustomComponent,
        TreeSelectComponent
    ],
  entryComponents: COMPONENTS_DYNAMIC,
  providers: [
    CommonService,
    ValueToTextPipe,
    {provide: MAT_DATE_LOCALE, useValue: 'vi'},
    {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
    {provide: MAT_DATE_FORMATS, useValue: FORMATS_DATE},
    {provide: NGX_MAT_DATE_FORMATS, useValue: FORMATS_DATETIME},
    {
      provide: MatPaginatorIntl, deps: [TranslateService],
      useFactory: (translate) => {
        const service = new PaginatorIntlService();
        service.injectTranslateService(translate);
        return service;
      },
    }
  ]
})
export class SharedModule {
}
