import {Component, Input, OnInit} from '@angular/core';
import {FormControl} from '@angular/forms';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-inline-message-advance',
  templateUrl: './inline-message-advance.component.html',
  styleUrls: ['./inline-message-advance.component.scss']
})
export class InlineMessageAdvanceComponent implements OnInit {
  @Input() formName: FormControl;
  @Input() message: any;
  @Input() customMessage: any;
  constructor(private translate: TranslateService) { }

  ngOnInit() {
  }

  getMessage(error: any) {
    if (error) {
      const key = Object.keys(error);
      if (key) {
        if (this.customMessage && this.customMessage[key[0]]) {
          return this.customMessage[key[0]];
        } else return this.translate.instant( 'validation.' + key[0], {field: this.translate.instant( this.message) , value: error[key[0]] ? error[key[0]].requiredLength : ''});
      }
    }
  }
}
