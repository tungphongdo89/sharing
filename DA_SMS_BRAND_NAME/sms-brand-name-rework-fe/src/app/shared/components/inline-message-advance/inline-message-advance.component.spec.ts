import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InlineMessageAdvanceComponent } from './inline-message-advance.component';

describe('InlineMessageAdvanceComponent', () => {
  let component: InlineMessageAdvanceComponent;
  let fixture: ComponentFixture<InlineMessageAdvanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InlineMessageAdvanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InlineMessageAdvanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
