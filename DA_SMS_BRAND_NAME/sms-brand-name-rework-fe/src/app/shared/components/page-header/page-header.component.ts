import {Component, OnInit, ViewEncapsulation, Input, OnChanges, SimpleChanges} from '@angular/core';
import {MenuService} from '@core/bootstrap/menu.service';
import {Router} from '@angular/router';
import {Subscription} from 'rxjs';

@Component({
  selector: 'page-header',
  host: {
    class: 'matero-page-header',
  },
  templateUrl: './page-header.component.html',
  styleUrls: ['./page-header.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class PageHeaderComponent implements OnInit, OnChanges {
  @Input() title = '';
  @Input() subtitle = '';
  @Input() nav: { link: string; label: string }[] = [];
  @Input() showBreadCrumb = true;
  @Input() url: string;
  menus;

  constructor(private _router: Router, private _menu: MenuService) {
  }

  ngOnInit() {
    this.nav = Array.isArray(this.nav) ? this.nav : [];
    // this.title = this.title || this.nav[this.nav.length - 1];
  }

  ngOnChanges(changes: SimpleChanges): void {
    if ('url' in changes) {
      this.genBreadcrumb();
    }
  }

  genBreadcrumb() {
    if (!this.menus) {
      this.menus = this._menu.allMenu;
    }
    this.menus.forEach(menu => {
      const arr = [{label: 'menu.dashboard', link: '#'}];
      if (this.url === '')
        this.nav = arr;
      else {
        this.getContent(menu, arr);
      }
    });
  }

  getContent(menus: any, arr: any) {
    arr.push({label: menus.name, link: menus.type === 'link' ? menus.route : null});
    if (menus.route === this.url) {
      this.nav = arr;
    } else if (menus.children != null) {
      menus.children.forEach((menu, i) => {
        const arrs = [...arr];
        this.getContent(menu, arrs);
      });
    }
  }
}
