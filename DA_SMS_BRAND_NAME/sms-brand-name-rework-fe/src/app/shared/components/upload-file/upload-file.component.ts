import {
  AfterViewInit,
  Component,
  ElementRef, EventEmitter,
  HostBinding,
  Input,
  OnDestroy,
  OnInit,
  Optional, Output,
  Self,
  ViewChild
} from '@angular/core';
import {FileHandle} from '@shared/directives/drag.directive';
import {AbstractControl, ControlValueAccessor, FormBuilder, FormControl, FormGroup, NgControl} from '@angular/forms';
import {MatFormFieldControl} from '@angular/material/form-field';
import {combineLatest, Observable, Subject} from 'rxjs';
import {map, startWith, takeUntil} from 'rxjs/operators';
import {AutofillMonitor} from '@angular/cdk/text-field';
import {FocusMonitor} from '@angular/cdk/a11y';
import {coerceBooleanProperty} from '@angular/cdk/coercion';
import {TranslateService} from '@ngx-translate/core';

@Component({
  host: {
    '(focusout)': 'onTouched()',
  },
  providers: [
    {provide: MatFormFieldControl, useExisting: UploadFileComponent},
  ],
  selector: 'upload-file',
  templateUrl: './upload-file.component.html',
  styleUrls: ['./upload-file.component.scss']
})
export class UploadFileComponent implements AfterViewInit, ControlValueAccessor, MatFormFieldControl<any>,
  OnDestroy {

  files: FileHandle[] = [];
  static nextId = 0;

  @Input() multiple: boolean;
  @Input() accept: string;
  @Input() isDisplayToolBar = true;
  // tslint:disable-next-line:no-output-on-prefix
  @Output() onFileChange = new EventEmitter<any>();
  private _disabled = false;
  private _focused = false;
  private _placeholder = '';
  private _required = false;
  chooseFile = true;
  private destroy: Subject<void> = new Subject();
  form = this.fb.group({
    file: ['']
  });

  stateChanges: Subject<void> = new Subject();

  @ViewChild('fileInput', {read: ElementRef})
  fileInput: ElementRef<HTMLInputElement>;

  autofilled = false;

  readonly controlType: string = 'file';

  @Input()
  get disabled(): boolean {
    return this._disabled;
  }

  set disabled(value: boolean) {
    this._disabled = coerceBooleanProperty(value);
    this.stateChanges.next();
  }

  @Input()
  get placeholder(): string {
    return this._placeholder;
  }

  set placeholder(value: string) {
    this._placeholder = value;
    this.stateChanges.next();
  }

  @Input()
  get required(): boolean {
    return this._required;
  }

  set required(value: boolean) {
    this._required = coerceBooleanProperty(value);
    this.stateChanges.next();
  }

  @Input()
  get value(): any {
    if (!this.empty) {
      return this.getValueControl();
    }
    return null;
  }

  set value(value: any) {
    this.form.controls.file.setValue(value);
    this.stateChanges.next();
  }

  @HostBinding('attr.aria-describedby')
  describedBy = '';
  @HostBinding()
  id = `file-control-${++UploadFileComponent.nextId}`;

  @HostBinding('class.floating')
  get shouldLabelFloat(): boolean {
    return this.focused || !this.empty;
  }

  setValueControl(value) {
    try {
      let arrs = this.getValueControl();
      if (arrs == null || arrs === undefined || arrs === '') {
        arrs = [];
      }
      value.forEach(arr => {
        arrs.push(arr);
      });
      this.form.controls.file.setValue(arrs);
    } catch (e) {
      console.log(e);
    }
  }

  getValueControl() {
    return this.form.controls.file.value;
  }

  get empty(): boolean {
    return !this.getValueControl();
  }

  get errorState(): boolean {
    return this.ngControl.control.invalid && this.ngControl.control.touched;
  }

  get focused(): boolean {
    return this._focused;
  }

  set focused(value: boolean) {
    this._focused = value;
    this.stateChanges.next();
  }

  constructor(
    private focusMonitor: FocusMonitor,
    private fb: FormBuilder,
    private elementRef: ElementRef<HTMLElement>,
    private translateService: TranslateService,
    @Optional() @Self() public ngControl: NgControl | null,
    private autofillMonitor: AutofillMonitor,
  ) {
    if (ngControl) {
      // Set the value accessor directly (instead of providing
      // NG_VALUE_ACCESSOR) to avoid running into a circular import
      this.ngControl.valueAccessor = this;
      ngControl.valueAccessor = this;
    }
  }

  ngAfterViewInit(): void {
    this.focusMonitor.monitor(this.elementRef.nativeElement, true)
      .subscribe(focusOrigin => {
        this.focused = !!focusOrigin;
      });
    combineLatest(
      this.observeAutofill(this.fileInput),
    ).pipe(
      map(autofills => autofills.some(autofilled => autofilled)),
      takeUntil(this.destroy),
    ).subscribe(autofilled => this.autofilled = autofilled);
  }

  ngOnDestroy(): void {
    this.destroy.next();
    this.destroy.complete();
    this.stateChanges.complete();
    this.focusMonitor.stopMonitoring(this.elementRef.nativeElement);
    this.autofillMonitor.stopMonitoring(this.fileInput);
  }

  onContainerClick(event: MouseEvent): void {
    if ((event.target as Element).tagName.toLowerCase() !== 'input') {
      this.focusMonitor.focusVia(this.fileInput.nativeElement, 'mouse');
    }
  }

  onTouched(): void {
  }

  registerOnChange(onChange: (value: any | null) => void): void {
    this.form.controls.file.valueChanges.pipe(
      takeUntil(this.destroy),
    ).subscribe(onChange);
  }

  registerOnTouched(onTouched: () => void): void {
    this.onTouched = onTouched;
  }

  setDescribedByIds(ids: string[]): void {
    this.describedBy = ids.join(' ');
  }

  setDisabledState(shouldDisable: boolean): void {
    if (shouldDisable) {
      this.form.disable();
    } else {
      this.form.enable();
    }

    this.disabled = shouldDisable;
  }

  writeValue(value: any): void {
    try {
      this.form.controls.file.setValue(value || [], {emitEvent: false});
    } catch (e) {
      this.form.controls.file.setValue([], {emitEvent: false});
    }
  }

  private observeAutofill(ref: ElementRef): Observable<boolean> {
    return this.autofillMonitor.monitor(ref)
      .pipe(map((event: any) => event.isAutofilled))
      .pipe(startWith(false));
  }


  filesDropped(files: FileHandle[]): void {
    const arrs = [];
    files.forEach(arr => {
      arrs.push(arr.file);
    });
    if (this.multiple) {
      this.setValueControl(arrs);
    } else {
      this.form.controls.file.setValue(arrs);
      this.onFileChange.emit(arrs);
    }
    this.ngControl.control.markAllAsTouched();
  }

  uploadFileEvt(imgFile: any) {
    if (imgFile.target.files && imgFile.target.files[0]) {
      const arr = Array.from(imgFile.target.files);
      if (this.multiple) {
        this.setValueControl(arr);
      } else {
        this.form.controls.file.setValue(arr);
        this.onFileChange.emit(arr);
      }
      this.ngControl.control.markAllAsTouched();
    }
  }

  remove(i: number) {
    this.chooseFile = false;
    const arrs = this.getValueControl();
    arrs.splice(i, 1);
    this.form.controls.file.setValue(arrs);
    this.onFileChange.emit(arrs);
    this.ngControl.control.markAllAsTouched();
  }

  choose() {
    if (this.chooseFile) {
      this.fileInput.nativeElement.click();
    }
    this.chooseFile = true;
  }
}
