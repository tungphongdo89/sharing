import {
  AfterViewInit,
  Component,
  ElementRef,
  HostBinding,
  Input,
  OnDestroy,
  OnInit,
  Optional,
  Self, ViewChild
} from '@angular/core';
import {ControlValueAccessor, FormBuilder, NgControl} from '@angular/forms';
import {MatFormFieldControl} from '@angular/material/form-field';
import {FileHandle} from '@shared/directives/drag.directive';
import {combineLatest, Observable, Subject} from 'rxjs';
import {coerceBooleanProperty} from '@angular/cdk/coercion';
import {FocusMonitor} from '@angular/cdk/a11y';
import {TranslateService} from '@ngx-translate/core';
import {AutofillMonitor} from '@angular/cdk/text-field';
import {map, startWith, takeUntil} from 'rxjs/operators';
import { HtmlEditorService, ImageService, LinkService, ToolbarService } from '@syncfusion/ej2-angular-richtexteditor';

@Component({
  host: {
    '(focusout)': 'onTouched()',
  },
  providers: [
    {provide: MatFormFieldControl, useExisting: EditorComponent},
    ToolbarService, LinkService, ImageService, HtmlEditorService],
  selector: 'app-editor',
  templateUrl: './editor.component.html',
  styleUrls: ['./editor.component.scss']
})
export class EditorComponent implements OnInit, AfterViewInit, ControlValueAccessor, MatFormFieldControl<any>,
  OnDestroy {

  @Input()
  get disabled(): boolean {
    return this._disabled;
  }

  set disabled(value: boolean) {
    this._disabled = coerceBooleanProperty(value);
    this.stateChanges.next();
  }

  @Input()
  get placeholder(): string {
    return this._placeholder;
  }

  set placeholder(value: string) {
    this._placeholder = value;
    this.stateChanges.next();
  }

  @Input()
  get required(): boolean {
    return this._required;
  }

  set required(value: boolean) {
    this._required = coerceBooleanProperty(value);
    this.stateChanges.next();
  }

  @Input()
  get value(): any {
    if (!this.empty) {
      return this.getValueControl();
    }
    return null;
  }

  set value(value: any) {
    this.form.controls.editorContent.setValue(value);
    this.stateChanges.next();
  }

  @HostBinding('class.floating')
  get shouldLabelFloat(): boolean {
    return this.focused || !this.empty;
  }

  get empty(): boolean {
    return !this.getValueControl();
  }

  get errorState(): boolean {
    return this.ngControl.control.invalid && this.ngControl.control.touched;
  }

  get focused(): boolean {
    return this._focused;
  }

  set focused(value: boolean) {
    this._focused = value;
    this.stateChanges.next();
  }

  constructor(
    private focusMonitor: FocusMonitor,
    private fb: FormBuilder,
    private elementRef: ElementRef<HTMLElement>,
    private translateService: TranslateService,
    @Optional() @Self() public ngControl: NgControl | null,
    private autofillMonitor: AutofillMonitor,
  ) {
    if (ngControl) {
      // Set the value accessor directly (instead of providing
      // NG_VALUE_ACCESSOR) to avoid running into a circular import
      this.ngControl.valueAccessor = this;
      ngControl.valueAccessor = this;
    }
  }

  static nextId = 0;
  form = this.fb.group({
    editorValue: [],
    editorContent: []
  });
  files: FileHandle[] = [];
  @ViewChild('textEditor', {read: ElementRef})
  textEditor: ElementRef<HTMLInputElement>;

  @Input() multiple: boolean;
  @Input() accept: string;
  private _disabled = false;
  private _focused = false;
  private _placeholder = '';
  private _required = false;
  private destroy: Subject<void> = new Subject();
  stateChanges: Subject<void> = new Subject();
  tools: object = {
    items: ['Undo', 'Redo', '|',
      'Bold', 'Italic', 'Underline', 'StrikeThrough', '|',
      'FontName', 'FontSize', 'FontColor', 'BackgroundColor', '|',
      'SubScript', 'SuperScript', '|',
      'LowerCase', 'UpperCase', '|',
      'Formats', 'Alignments', '|', 'OrderedList', 'UnorderedList', '|',
      'Indent', 'Outdent', '|', 'CreateLink',
      'Image', '|', 'ClearFormat', 'Print', 'SourceCode', '|', 'FullScreen']
  };
  autofilled = false;

  readonly controlType: string = 'file';

  @HostBinding('attr.aria-describedby')
  describedBy = '';
  @HostBinding()
  id = `file-control-${++EditorComponent.nextId}`;

  getValueControl() {
    return this.form.controls.editorContent.value;
  }

  ngAfterViewInit(): void {
    this.focusMonitor.monitor(this.elementRef.nativeElement, true)
      .subscribe(focusOrigin => {
        this.focused = !!focusOrigin;
      });
    combineLatest(
      this.observeAutofill(this.textEditor),
    ).pipe(
      map(autofills => autofills.some(autofilled => autofilled)),
      takeUntil(this.destroy),
    ).subscribe(autofilled => this.autofilled = autofilled);
  }

  ngOnDestroy(): void {
    this.destroy.next();
    this.destroy.complete();
    this.stateChanges.complete();
    this.focusMonitor.stopMonitoring(this.elementRef.nativeElement);
    this.autofillMonitor.stopMonitoring(this.textEditor);
  }

  onTouched(): void {
  }

  registerOnChange(onChange: (value: any | null) => void): void {
    this.form.controls.editorContent.valueChanges.pipe(
      takeUntil(this.destroy),
    ).subscribe(onChange);
  }

  registerOnTouched(onTouched: () => void): void {
    this.onTouched = onTouched;
  }

  setDescribedByIds(ids: string[]): void {
    this.describedBy = ids.join(' ');
  }

  setDisabledState(shouldDisable: boolean): void {
    if (shouldDisable) {
      this.form.disable();
    } else {
      this.form.enable();
    }

    this.disabled = shouldDisable;
  }

  writeValue(value: any): void {
    this.form.controls.editorContent.setValue(value, {emitEvent: false});
  }

  private observeAutofill(ref: ElementRef): Observable<boolean> {
    return this.autofillMonitor.monitor(ref)
      .pipe(map((event: any) => event.isAutofilled))
      .pipe(startWith(false));
  }

  ngOnInit(): void {
  }

  onContainerClick(event: MouseEvent): void {
  }
}
