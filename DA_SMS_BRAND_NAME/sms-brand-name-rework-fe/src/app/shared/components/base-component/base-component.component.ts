import {EventEmitter, Inject, Injectable, TemplateRef} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {FormGroup} from '@angular/forms';
import {MtxDialog, MtxGridColumn} from '@ng-matero/extensions';
import {ComponentType, ToastrService} from 'ngx-toastr';
import {MatDialogRef} from '@angular/material/dialog';
import {TranslateService} from '@ngx-translate/core';
import {ConfirmDialogComponent, ConfirmDialogModel} from '../confirm-dialog/confirm-dialog.component';
import {HttpHeaders, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable()
export class BaseComponent {
  public resource;
  /**
   * Bien ho tro tim kiem chung
   */
  columns: MtxGridColumn[] = [];
  columnsData: MtxGridColumn[] = [];
  formSearch: FormGroup;
  private mainService: any;
  isLoading = true;

  searchModel: any = {};
  dataModel: any = {};
  isAdvSearch = false;
  pagesize = 10;
  startrecord = 0;
  totalRecord = 0;
  pageSizeList = [10, 50, 100];
  pageIndex = 0;
  currentPage: any;
  eventEmit?: EventEmitter<any> = new EventEmitter<any>();

  constructor(
    public actr?: ActivatedRoute,
    service?: any,
    @Inject(String) resource?,
    protected toastr?: ToastrService,
    protected translateService?: TranslateService,
    public dialog?: MtxDialog,
    public dialogRef?: MatDialogRef<ComponentType<any> | TemplateRef<any> | any>) {
    if (resource) {
      this.resource = resource;
    }
    this.mainService = service;
  }
  public processSearch(): void {
    this.isLoading = true;
    this.isAdvSearch ? this.pageIndex = 0 : null;
    const params = this.formSearch ? this.formSearch.value : null;
    params.startrecord = !this.isAdvSearch ? this.startrecord : 0;
    params.pagesize = this.pagesize;
    this.mainService.search(params).subscribe(res => {
      if (res.mess.code === 1) {
        this.dataModel.dataSource = res.data.listData;
        this.totalRecord = res.data.count;
        this.isLoading = false;
      } else {
        if (res.mess.code === 5) {
          this.dataModel.dataSource = [];
          this.isLoading = false;
        } else
          this.toastr.error(this.translateService.instant('common.notify.fail'));
      }
      this.isAdvSearch = false;
    }, err => {
      this.isAdvSearch = false;
      this.isLoading = false;
      this.toastr.error(this.translateService.instant('common.notify.fail'));
    });
  }


  public showPopupUpdate(config?, componentTemplate?) {
    return this.dialog.open({
      width: this.dataModel.popupWidth,
      data: {config},
    }, componentTemplate);
  }

  save(id: any) {
    this.confirmDialogUpdate(id);
  }

  confirmDialogUpdate(value: any): void {
    let title = '';
    let content = '';
    if (value) {
      content = this.translateService.instant('common.confirm.update');
      title = this.translateService.instant('common.confirm.title.update');
    } else {
      content = this.translateService.instant('common.confirm.insert');
      title = this.translateService.instant('common.confirm.title.insert');
    }
    const dialogData = new ConfirmDialogModel(title, content);

    const dialogRef = this.dialog.originalOpen(ConfirmDialogComponent, {
      maxWidth: '400px',
      data: dialogData
    });

    dialogRef.afterClosed().subscribe(dialogResult => {
      if (dialogResult) {
        if (value) {
          this.mainService.update(this.dataModel).subscribe(rs => this.onEditSuccess(rs.body),
              error => this.onEditError(error));
        } else {
          this.mainService.insert(this.dataModel).subscribe(rs => this.onInsertSuccess(rs.body),
              error => this.onInsertError(error));
        }
      }
    });
  }
  /**
   * Xu ly xoa
   */
  public processDelete(id): void {
    if (id && id > 0) {
      this.mainService.deleteById(id)
        .subscribe(res => {
          if (this.mainService.requestIsSuccess(res)) {
            this.processSearch();
          }
        });
    }
  }
  protected onSaveSuccess(body: any): void {
    this.toastr.success('Lưu thành công!', 'Thông báo');
    this.dialogRef.close(body);
  }

  protected onSaveError(errors): void {
    console.log('errors', errors);
    let mes = errors.error.title;
    if (errors.error.message === 'error.validation') {
      mes = 'Dữ liệu không đúng định dạng';
    }
    this.toastr.error(mes, 'Lưu thất bại');
  }

  onPageChange(event) {
    this.pageIndex = event.pageIndex;
    this.startrecord = event.pageIndex == 0 ? event.pageIndex : ((event.pageIndex * event.pageSize));
    this.pagesize = event.pageSize;
    this.processSearch();
  }

  doSave(dataForm, dialogRef1: any) {
    this.validateSave();
    if (!dataForm.valid) {
      return;
    }
    this.dialogRef = dialogRef1;
    if (dataForm.value.id) {
      this.subscribeToEditResponse(this.mainService.update(dataForm.value));
    } else {
      this.subscribeToInsertResponse(this.mainService.insert(dataForm.value));
    }
  }
  protected validateSave() {
    console.log('validateSave');
  }

  protected subscribeToInsertResponse(result: Observable<HttpResponse<any>>): void {
    result.subscribe(
      (res) => this.onInsertSuccess(res.body),
      (errors) => this.onInsertError(errors)
    );
  }

  protected subscribeToEditResponse(result: Observable<HttpResponse<any>>): void {
    result.subscribe(
      (res) => this.onEditSuccess(res.body),
      (errors) => this.onEditError(errors)
    );
  }
  protected getTitleFunction() {
    return '';
  }
  protected focusError(errors): void {
    console.log(errors);
  }
  protected onInsertSuccess(body: any): void {
    const func = this.getTitleFunction();
    this.toastr.success(this.translateService.instant('common.insert-success', {field: func}));
    this.dialogRef.close(body);
  }

  protected onEditSuccess(body: any): void {
    const func = this.getTitleFunction();
    this.toastr.success(this.translateService.instant('common.update-success', {field: func}));
    this.dialogRef.close(body);
  }

  protected onInsertError(errors): void {
    this.focusError(errors);
    this.toastr.error(errors.error.title);
  }
  protected onEditError(errors): void {
    this.focusError(errors);
    this.toastr.error(errors.error.title);
  }

  public onSearch() {
    this.setPage({pageIndex: 0, pageSize: 10});
  }

  public setPage(pageInfo) {
    this.currentPage = pageInfo;
    const pageToLoad: number = pageInfo.pageIndex;
    this.mainService.search({
      page: pageToLoad,
      size: pageInfo.pageSize
    }, this.formSearch.value).subscribe(res => this.onSearchSuccess(res, res.headers));
  }

  public onSearchSuccess(data: any | null, headers: HttpHeaders): void {
    console.log('data', data);
    this.dataModel.dataSource = data.body;
    this.totalRecord = Number(headers.get('X-Total-Count'));
    this.isLoading = false;
  }
}
