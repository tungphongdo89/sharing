import {FocusMonitor} from '@angular/cdk/a11y';
import {BooleanInput, coerceBooleanProperty} from '@angular/cdk/coercion';
import {Component, ElementRef, Input, OnDestroy, Optional, Self, ViewChild} from '@angular/core';
import {AbstractControl, ControlValueAccessor, FormBuilder, FormGroup, NgControl, Validators} from '@angular/forms';
import {MatFormFieldControl} from '@angular/material/form-field';
import {Subject} from 'rxjs';

export class Time {
  constructor(
    public hour: string,
    public minute: string
  ) {
  }
}

/** Custom `MatFormFieldControl` for telephone number input. */
@Component({
  selector: 'app-time-picker',
  templateUrl: 'time-picker.component.html',
  styleUrls: ['time-picker.component.scss'],
  providers: [{provide: MatFormFieldControl, useExisting: TimePickerComponent}],
  host: {
    '[class.example-floating]': 'shouldLabelFloat',
    '[id]': 'id',
  }
})
export class TimePickerComponent implements ControlValueAccessor, MatFormFieldControl<Time>, OnDestroy {

  get empty() {
    const {
      value: {hour, minute}
    } = this.parts;

    return !hour && !minute;
  }

  get shouldLabelFloat() {
    return this.focused || !this.empty;
  }

  @Input()
  get placeholder(): string {
    return this._placeholder;
  }

  set placeholder(value: string) {
    this._placeholder = value;
    this.stateChanges.next();
  }

  @Input()
  get required(): boolean {
    return this._required;
  }

  set required(value: boolean) {
    this._required = coerceBooleanProperty(value);
    this.stateChanges.next();
  }

  @Input()
  get disabled(): boolean {
    return this._disabled;
  }

  set disabled(value: boolean) {
    this._disabled = coerceBooleanProperty(value);
    this._disabled ? this.parts.disable() : this.parts.enable();
    this.stateChanges.next();
  }

  @Input()
  get value(): Time | null {
    const {
      value: {hour, minute}
    } = this.parts;
    return new Time(hour, minute);
  }

  set value(time: Time | null) {
    const {hour, minute} = time || new Time('', '');
    this.parts.setValue({hour, minute});
    this.stateChanges.next();
  }

  get errorState(): boolean {
    return this.parts.invalid && this.touched;
  }

  constructor(
    formBuilder: FormBuilder,
    private _focusMonitor: FocusMonitor,
    private _elementRef: ElementRef<HTMLElement>,
    @Optional() @Self() public ngControl: NgControl | null) {
    this.parts = formBuilder.group({
      hour: [null, [Validators.required, Validators.minLength(2), Validators.maxLength(2), Validators.max(23)]],
      minute: [null, [Validators.required, Validators.minLength(2), Validators.maxLength(2), Validators.max(59)]],
    });

    if (this.ngControl != null) {
      this.ngControl.valueAccessor = this;
      ngControl.valueAccessor = this;
    }
  }

  static nextId = 0;

  // tslint:disable-next-line:variable-name
  static ngAcceptInputType_disabled: BooleanInput;
  // tslint:disable-next-line:variable-name
  static ngAcceptInputType_required: BooleanInput;
  @ViewChild('hour') hourInput: HTMLInputElement;
  @ViewChild('minute') minuteInput: HTMLInputElement;

  parts: FormGroup;
  stateChanges = new Subject<void>();
  focused = false;
  touched = false;
  controlType = 'example-tel-input';
  id = `example-tel-input-${TimePickerComponent.nextId++}`;

  // tslint:disable-next-line:no-input-rename
  @Input('aria-describedby') userAriaDescribedBy: string;

  private _placeholder: string;

  private _required = false;

  private _disabled = false;
  onChange = (_: any) => {
  };
  onTouched = () => {
  };

  ngOnDestroy() {
    this.stateChanges.complete();
    this._focusMonitor.stopMonitoring(this._elementRef);
  }

  onFocusIn(event: FocusEvent) {
    if (!this.focused) {
      this.focused = true;
      this.stateChanges.next();
    }
  }

  onFocusOut(event: FocusEvent) {
    if (!this._elementRef.nativeElement.contains(event.relatedTarget as Element)) {
      this.touched = true;
      this.focused = false;
      this.onTouched();
      this.stateChanges.next();
    }
  }

  autoFocusNext(control: AbstractControl, nextElement?: HTMLInputElement): void {
    if (!control.errors && nextElement) {
      this._focusMonitor.focusVia(nextElement, 'program');
    }
  }

  autoFocusPrev(control: AbstractControl, prevElement: HTMLInputElement): void {
    if (control.value.length < 1) {
      this._focusMonitor.focusVia(prevElement, 'program');
    }
  }

  setDescribedByIds(ids: string[]) {
    const controlElement = this._elementRef.nativeElement
      .querySelector('.example-tel-input-container')!;
    controlElement.setAttribute('aria-describedby', ids.join(' '));
  }

  onContainerClick() {
    this._focusMonitor.focusVia(this.hourInput, 'program');
  }

  writeValue(time: Time | null): void {
    this.value = time;
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  _handleInput(control: AbstractControl, nextElement?: HTMLInputElement): void {
    this.autoFocusNext(control, nextElement);
    this.onChange(this.value);
  }

  hourFilter(event: any) {
    if (Number.isNaN(event?.target?.value)) return false;
    if (Number(event?.target?.value) > 23) {
      this.parts.controls.hour.setErrors({max: true});
      event.target.value = '';
      return false;
    } else {
      this.parts.controls.hour.setErrors(null);
      return true;
    }
  }

  minuteFilter(event: any) {
    if (Number.isNaN(event?.target?.value)) return false;
    if (Number(event?.target?.value) > 59) {
      this.parts.controls.minute.setErrors({max: true});
      event.target.value = '';
      return false;
    } else {
      this.parts.controls.minute.setErrors(null);
      return true;
    }
  }
}
