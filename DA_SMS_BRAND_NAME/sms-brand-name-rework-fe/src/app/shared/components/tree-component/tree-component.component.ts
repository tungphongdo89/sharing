import { NestedTreeControl } from '@angular/cdk/tree';
import { Component, Input, Output, ViewChild, EventEmitter } from '@angular/core';
import { MatTree, MatTreeNestedDataSource } from '@angular/material/tree';
import { SelectionModel } from '@angular/cdk/collections';


export class TreeModel {
  children: Array<TreeModel>;
  name: string;
}

@Component({
  selector: 'crm-tree-component',
  templateUrl: './tree-component.component.html',
  styleUrls: ['./tree-component.component.scss']
})
export class TreeComponentComponent {
  @ViewChild('tree') tree: MatTree<any>;
  isMutilSelect = false;
  @Input() treeData = [];
  subscription;
  count = 0;
  nestedTreeControl: NestedTreeControl<TreeModel>;
  nestedDataSource: MatTreeNestedDataSource<TreeModel>;
  inputValue;
  selectedData = new SelectionModel<any>(true /* multiple */);
  @Output() selectNode = new EventEmitter();
  constructor() {
    /** Data Source and Tree Control used by Tree View */
    this.nestedTreeControl = new NestedTreeControl<TreeModel>(this._getChildren);
    this.nestedDataSource = new MatTreeNestedDataSource();
  }

  ngOnInit() {
    const data = this.treeData;
    data.map(d => {
      d['expanded'] = true;
      return d;
    });
    this.nestedDataSource.data = data;
  }

  /** Checks if datasource for material tree has any child groups */
  hasNestedChild = (_: number, nodeData: TreeModel) => nodeData.children.length > 0;

  /** Returns child groups from security group */
  private _getChildren = (node: TreeModel) => node.children;

  clickedActive(element) {
    element.checked = !element.checked;
  }

  /** Loops recursively through data finding the amount of checked children */
  getCheckedAmount(data) {
    this.count = 0; // resetting count
    this.loopData(data.children);
    return this.count;
  }

  /** Used by getCheckedAmount() */
  loopData(data) {
    data.forEach(d => {
      if (d.checked) {
        this.count += 1;
      }
      if (d.children && d.children.length > 0) {
        this.loopData(d.children);
      }
    });
  }

  changeState(data) {
    data.expanded = !data.expanded;
  }
  chooseNode(item: any) {
    if (!this.isMutilSelect) {
      this.selectedData = item;
      this.selectNode.next(this.selectedData);
    }
  }
}
