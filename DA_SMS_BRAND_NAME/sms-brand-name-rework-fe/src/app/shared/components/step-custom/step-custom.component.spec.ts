import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StepCustomComponent } from './step-custom.component';

describe('StepCustomComponent', () => {
  let component: StepCustomComponent;
  let fixture: ComponentFixture<StepCustomComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StepCustomComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StepCustomComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
