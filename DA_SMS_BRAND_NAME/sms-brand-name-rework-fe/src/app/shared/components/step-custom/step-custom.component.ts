import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {EventModel} from '@core/models/event-model';

@Component({
  selector: 'app-step-custom',
  templateUrl: './step-custom.component.html',
  styleUrls: ['./step-custom.component.scss']
})
export class StepCustomComponent implements OnInit {
  @Input() currentStep: any;
  constructor() { }
  @Output() changeStep = new EventEmitter();

  ngOnInit(): void {
  }

  selectSteep(index: number) {
    this.changeStep.emit(index);
  }
}
