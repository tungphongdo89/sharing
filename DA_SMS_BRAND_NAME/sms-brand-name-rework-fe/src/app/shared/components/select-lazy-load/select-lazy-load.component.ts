import {
  AfterViewInit,
  Component,
  ElementRef,
  HostBinding,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Optional,
  Self,
  ViewChild
} from '@angular/core';
import {ControlValueAccessor, FormBuilder, NgControl} from '@angular/forms';
import {MatFormFieldControl} from '@angular/material/form-field';
import {coerceBooleanProperty} from '@angular/cdk/coercion';
import {FocusMonitor} from '@angular/cdk/a11y';
import {TranslateService} from '@ngx-translate/core';
import {AutofillMonitor} from '@angular/cdk/text-field';
import {Observable, Subject} from 'rxjs';
import {map, startWith, takeUntil} from 'rxjs/operators';
import {MatSelect} from '@angular/material/select';

@Component({
  selector: 'select-lazy-load',
  templateUrl: './select-lazy-load.component.html',
  styleUrls: ['./select-lazy-load.component.scss'],
  providers: [
    {provide: MatFormFieldControl, useExisting: SelectLazyLoadComponent}]
})
export class SelectLazyLoadComponent implements OnInit, AfterViewInit, ControlValueAccessor, MatFormFieldControl<any>,
  OnDestroy, OnChanges {

  @Input() touched: any;

  @Input()
  get disabled(): boolean {
    return this._disabled;
  }

  set disabled(value: boolean) {
    this._disabled = coerceBooleanProperty(value);
    this.stateChanges.next();
  }

  @Input()
  get placeholder(): string {
    return this._placeholder;
  }

  set placeholder(value: string) {
    this._placeholder = value;
    this.stateChanges.next();
  }

  @Input()
  get required(): boolean {
    return this._required;
  }

  set required(value: boolean) {
    this._required = coerceBooleanProperty(value);
    this.stateChanges.next();
  }

  @Input()
  get value(): any {
    if (!this.empty) {
      return this.getValueControl();
    }
    return null;
  }

  set value(value: any) {
    this.form.controls.selectValue.setValue(value);
    this.stateChanges.next();
  }

  @HostBinding('class.floating')
  get shouldLabelFloat(): boolean {
    return this.focused || !this.empty;
  }

  get empty(): boolean {
    return !this.getValueControl();
  }

  get errorState(): boolean {
    return !this.touched ? this.ngControl.control.invalid && this.ngControl.control.touched : this.ngControl.control.invalid;
  }

  get focused(): boolean {
    return this._focused;
  }

  set focused(value: boolean) {
    this._focused = value;
    this.stateChanges.next();
  }

  @Input()
  data = [];

  @Input()
  field: {text: string, value: string};

  items = [];
  dataFilter = [];

  labelField = 'label';
  valueField = 'value';
  current = 0;

  constructor(
    private focusMonitor: FocusMonitor,
    private fb: FormBuilder,
    private elementRef: ElementRef<HTMLElement>,
    private translateService: TranslateService,
    @Optional() @Self() public ngControl: NgControl | null,
    private autofillMonitor: AutofillMonitor,
  ) {
    if (ngControl) {
      // Set the value accessor directly (instead of providing
      // NG_VALUE_ACCESSOR) to avoid running into a circular import
      this.ngControl.valueAccessor = this;
      ngControl.valueAccessor = this;
    }
  }

  ngOnChanges(changes: import('@angular/core').SimpleChanges): void {
    if ('data' in changes) {
      this.dataFilter = this.data;
      if (this.data && this.data.length > 0) {
        this.items = this.dataFilter.slice(0, this.data.length > 20 ? 20 : this.data.length);
        this.current = 0;
      }
    }
  }

  static nextId = 0;

  form = this.fb.group({
    selectValue: [],
  });

  @ViewChild('select', {static: true})
  select: MatSelect;

  @Input() multiple: boolean;
  @Input() accept: string;
  private _disabled = false;
  private _focused = false;
  private _placeholder = '';
  private _required = false;
  private destroy: Subject<void> = new Subject();
  stateChanges: Subject<void> = new Subject();
  autofilled = false;

  readonly controlType: string = 'file';

  @HostBinding('attr.aria-describedby')
  describedBy = '';
  @HostBinding()
  id = `select-control-${++SelectLazyLoadComponent.nextId}`;

  getValueControl() {
    return this.form.controls.selectValue.value;
  }

  ngAfterViewInit(): void {
  }

  ngOnDestroy(): void {
    this.destroy.next();
    this.destroy.complete();
    this.stateChanges.complete();
  }

  onTouched(): void {
  }

  registerOnChange(onChange: (value: any | null) => void): void {
    this.form.controls.selectValue.valueChanges.pipe(
      takeUntil(this.destroy),
    ).subscribe(onChange);
  }

  registerOnTouched(onTouched: () => void): void {
    this.onTouched = onTouched;
  }

  setDescribedByIds(ids: string[]): void {
    this.describedBy = ids.join(' ');
  }

  setDisabledState(shouldDisable: boolean): void {
    if (shouldDisable) {
      this.form.disable();
    } else {
      this.form.enable();
    }

    this.disabled = shouldDisable;
  }

  writeValue(value: any): void {
    this.form.controls.selectValue.setValue(value, {emitEvent: false});
  }

  private observeAutofill(ref: ElementRef): Observable<boolean> {
    return this.autofillMonitor.monitor(ref)
      .pipe(map((event: any) => event.isAutofilled))
      .pipe(startWith(false));
  }

  ngOnInit(): void {
    this.select.openedChange.subscribe(() => this.registerPanelScrollEvent());
    if(this.field) {
      this.labelField = this.field.text;
      this.valueField = this.field.value;
    }
    if(!this.placeholder) {
      this.placeholder = this.translateService.instant('common.button.search');
    }
  }

  registerPanelScrollEvent() {
    if(!this.select.panel) return;
    const panel = this.select.panel.nativeElement;
    panel.addEventListener('scroll', event => this.loadAllOnScroll(event));
  }

  loadAllOnScroll(event) {
    if (event.target.scrollTop + event.target.offsetHeight >=
      event.target.scrollHeight - 5) {
      if(this.current === this.dataFilter.length) return;
      const index = this.dataFilter.length > this.current + 20 ? this.current + 20 : this.dataFilter.length;
      this.items = this.dataFilter.slice(0, index);
      this.current = index;
    }
  }

  onContainerClick(event: MouseEvent): void {
  }

  filter(value) {
    if(!value || value.trim() === '') {
      this.dataFilter = this.data;
    } else {
      this.dataFilter = this.data.filter(item => item[this.labelField].toLowerCase().includes(value.toLowerCase()));
    }
    this.current = 20;
    this.items = this.dataFilter.slice(0, this.current);
  }
}
