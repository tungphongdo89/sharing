import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {NestedTreeControl} from '@angular/cdk/tree';
import {MatTreeNestedDataSource} from '@angular/material/tree';
import {AbstractControl, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-tree-select',
  templateUrl: './tree-select.component.html',
  styleUrls: ['./tree-select.component.scss']
})
export class TreeSelectComponent implements OnInit {
  @ViewChild('matSelect', {static: true}) matSelect: any;
  treeControl = new NestedTreeControl<any>(node => node.children);
  @Input() dataSource = new MatTreeNestedDataSource<any>();
  @Input() documents: any = [];
  @Input() form: FormGroup;
  @Input() controlName: string;
  @Input() disable: boolean;
  @Output() selectChange: EventEmitter<any> = new EventEmitter<any>();
  hasChild = (_: number, node: any) => !!node.children && node.children.length > 0;
  constructor() { }

  ngOnInit(): void {
  }

  selectParent(node) {
    this.form.get(this.controlName).setValue(node.key);
    this.selectChange.emit(node);
    this.matSelect.close();
  }
}
