import {AbstractControl, FormControl, ValidationErrors, Validator, ValidatorFn} from '@angular/forms';

import * as moment from 'moment';

export class CustomValidator implements Validator {

  private static convertStringToDate(dateStr: string): Date {
    const date = dateStr.substr(0, 2);
    const month = dateStr.substr(3, 2);
    const year = dateStr.substr(6, 4);
    return new Date(Number(year), Number(month), Number(date));
  }


  static validateNoFullSpace(c: FormControl): { [key: string]: any } | null {
    return (c.value === undefined || c.value === null || c.value.trim() === '') ? {
      customRequired: {
        valid: false
      }
    } : null;
  }

  static validateDateFormat(c: FormControl): { [key: string]: any } | null {
    return (c.value != null && c.value !== '' && !moment(c.value, 'DD/MM/YYYY', true).isValid()) ? {
      dateFormat: {
        valid: true
      }
    } : null;
  }

  static checkFromDateAndToDate(dateField1, dateField2) {

    return (control: AbstractControl): { [key: string]: any } | null => {
      if ((control.get(dateField1).valid && control.get(dateField2).valid) &&
        control.get(dateField1).value && control.get(dateField2).value) {
        return (moment(control.get(dateField2).value, 'DD/MM/YYYY').diff(moment(control.get(dateField1).value, 'DD/MM/YYYY'), 'days')) >= 0  ? null : {
          fromDateAndToDate: true
        };
      }
    };
  }

  static checkDateAndCurrentDate(c: FormControl): { [key: string]: any } | null {
    return (moment().diff(moment(c.value, 'DD/MM/YYYY'), 'days')) <= 0 ? null : {
      lessThanNow: true
    };
  }

  static convertDate(value) {
    const date = value.substr(0, 2);
    const month = value.substr(3, 2);
    const year = value.substr(6, 4);
    return new Date(Number(year), Number(month) - 1, Number(date));
  }

  // static toDate() {
  //     var myDate = moment(this.dateForm.toDate, "DD/MM/YYYY");
  //     var arrTime = this.dateForm.toTime.split(':');
  //     myDate.hours(Number(arrTime[0])).minutes(Number(arrTime[1])).seconds(Number(arrTime[2]));
  //     return myDate.toDate()
  // }

  static toDate = (dateStr) => {
    const [day, month, year] = dateStr.split('/');
    return new Date(year, month - 1, day);
  }

  registerOnValidatorChange(fn: () => void): void {
  }


  validate(control: AbstractControl): ValidationErrors | null {
    return undefined;
  }
}
