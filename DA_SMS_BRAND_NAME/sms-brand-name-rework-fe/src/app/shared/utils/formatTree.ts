export function formatTreeCommon(data, parentId, key, keyValue) {
  const arr = [];
  for (let i = 0; i < data.length; i++) {
    const dataItem = {
      name: (data[i])[key],
      key: keyValue ? (data[i])[keyValue] : data[i],
      orgData: data[i],
      parentId: data[i].parentId ? (data[i].parentId + '') : (data[i].parent ? data[i].parent + '' : null),
      children: null,
      id: data[i].id + '',
      isLeaf: data[i].isPost
    };

    if (dataItem.id && dataItem.parentId + '' === parentId + '') {
      const children = formatTreeCommon(data, dataItem.id + '', key, keyValue);
      if (children.length > 0) {
        dataItem.children = children;
      }
      const dataTreeview = {
        name: dataItem.name,
        key: dataItem.key,
        orgData: dataItem.orgData,
        id: data[i].id + '',
        isLeaf: dataItem.isLeaf
      };
      if (dataItem.children) {
        dataTreeview['children'] = dataItem.children;
      }
      arr.push(dataTreeview);
    }
  }
  return arr;
}
