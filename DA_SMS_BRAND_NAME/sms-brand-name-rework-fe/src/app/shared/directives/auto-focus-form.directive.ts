import {Directive, ElementRef, HostListener, OnInit} from '@angular/core';
import {FormArray, FormControl, FormGroupDirective, NgControl, Validators} from '@angular/forms';

@Directive({
  selector: '[focus-form]',
})
export class AutoFocusFormDirective implements OnInit {

  hasSetvalidator = false;

  constructor(
    // Get the control directive
    private el: ElementRef,
    private formGroupDirective: FormGroupDirective
  ) {
  }

  ngOnInit() {
    for (const key of Object.keys(this.formGroupDirective.control.controls)) {
      if (this.formGroupDirective.control.get(key).disabled) {
        continue;
      }
      const invalidControl = this.el.nativeElement.querySelector('[formcontrolname="' + key + '"]');
      try {
        invalidControl.children[0].focus();
      } catch {
        invalidControl.focus();
      }
      break;
    }
    this.formGroupDirective.ngSubmit.subscribe(() => {
      for (const key of Object.keys(this.formGroupDirective.control.controls)) {
        if (this.formGroupDirective.control.controls[key].invalid) {
          const invalidControl = this.el.nativeElement.querySelector('[formcontrolname="' + key + '"]');
          try {
            invalidControl.children[0].focus();
          } catch {
            invalidControl.focus();
          }
          break;
        }
      }
    });
  }

}
