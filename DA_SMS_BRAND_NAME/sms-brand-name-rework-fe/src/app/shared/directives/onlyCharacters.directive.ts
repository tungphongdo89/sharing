import {AbstractControl, ValidatorFn} from '@angular/forms';

export function onlyCharacterValidator(nameRe: RegExp): ValidatorFn {
  return (control: AbstractControl): { [key: string]: any } | null => {
    let data = control.value;
    if (data) {
      data = data.trim();
    }
    // tslint:disable-next-line:variable-name
    const number = !nameRe.test(data);
    if (!control.value) {
      return null;
    }
    return number ? {onlyCharacter: {value: control.value}} : null;
  };
}
export function userBrandNameValid(nameRe: RegExp): ValidatorFn {
  return (control: AbstractControl): { [key: string]: any } | null => {
    let data = control.value;
    if (data) {
      data = data.trim();
    }
    // tslint:disable-next-line:variable-name
    const number = !nameRe.test(data);
    if (!control.value) {
      return null;
    }
    return number ? {invalidBrand: {value: control.value}} : null;
  };
}
export function cpCodeValid(nameRe: RegExp): ValidatorFn {
  return (control: AbstractControl): { [key: string]: any } | null => {
    let data = control.value;
    if (data) {
      data = data.trim();
    }
    // tslint:disable-next-line:variable-name
    const number = !nameRe.test(data);
    if (!control.value) {
      return null;
    }
    return number ? {invalidCpCode: {value: control.value}} : null;
  };
}
export function phoneNumberValid(nameRe: RegExp): ValidatorFn {
  return (control: AbstractControl): { [key: string]: any } | null => {
    let data = control.value;
    if (data) {
      data = data.trim();
    }
    // tslint:disable-next-line:variable-name
    const number = !nameRe.test(data);
    if (!control.value) {
      return null;
    }
    return number ? {invalidPhoneNumber: {value: control.value}} : null;
  };
}
export function passwordValid(nameRe: RegExp): ValidatorFn {
  return (control: AbstractControl): { [key: string]: any } | null => {
    let data = control.value;
    if (data) {
      data = data.trim();
    }
    // tslint:disable-next-line:variable-name
    const number = !nameRe.test(data);
    if (!control.value) {
      return null;
    }
    return number ? {invalidPassword: {value: control.value}} : null;
  };
}
export function smppPasswordValid(nameRe: RegExp): ValidatorFn {
  return (control: AbstractControl): { [key: string]: any } | null => {
    let data = control.value;
    if (data) {
      data = data.trim();
    }
    // tslint:disable-next-line:variable-name
    const number = !nameRe.test(data);
    if (!control.value) {
      return null;
    }
    return number ? {invalidSmppPassword: {value: control.value}} : null;
  };
}
