import {Directive, ElementRef, HostListener} from '@angular/core';

@Directive({
  selector: '[dateInput]',
})
export class DateInputDirective {

  REGEX: RegExp = /^[0-9/]$/;

  constructor(private el: ElementRef) {
  }

  @HostListener('keypress', ['$event'])
  onKeyPress(event) {
    return new RegExp(this.REGEX).test(event.key);
  }

  @HostListener('paste', ['$event'])
  blockPaste(event: KeyboardEvent) {
    this.validateFields(event);
  }

  validateFields(event) {
    setTimeout(() => {
      this.el.nativeElement.value = this.el.nativeElement.value.replace(/[^0-9/]/g, '').replace(/\s/g, '');
      event.preventDefault();
    }, 100);
  }
}
