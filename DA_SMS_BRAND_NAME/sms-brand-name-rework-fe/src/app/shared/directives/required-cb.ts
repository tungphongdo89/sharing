import {AbstractControl, ValidatorFn} from '@angular/forms';

export class ValidatorsCb {
  public static required(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
      if (!control.value || control.value === -1) {
        return {requiredCb: {value: control.value}};
      }
      return null;
    };
  }
}
