import {Injectable} from '@angular/core';
import {finalize} from 'rxjs/operators';
import {HttpClient} from '@angular/common/http';
import {HelperService} from '@shared/services/helper.service';
import {ToastrService} from 'ngx-toastr';
import {TranslateService} from '@ngx-translate/core';
import * as fileSaver from 'file-saver';
import {BehaviorSubject, Observable} from 'rxjs';
import {Router} from '@angular/router';
import {ConfirmDialogComponent, ConfirmDialogModel} from '@shared/components/confirm-dialog/confirm-dialog.component';
import {MtxDialog} from '@ng-matero/extensions';

@Injectable({
  providedIn: 'root'
})
export class CommonService {
  private dialogData = new BehaviorSubject(null);
  private searchData = new BehaviorSubject(null);

  constructor(
    public httpClient: HttpClient,
    public helperService: HelperService,
    private toastr: ToastrService,
    public dialog: MtxDialog,
    private translateService: TranslateService,
    private router: Router
  ) {
  }

  get dialogValue() {
    const data = this.dialogData.value;
    this.dialogData.next(null);
    return data;
  }

  get searchValue() {
    const data = this.searchData.value;
    this.searchData.next(null);
    return data;
  }

  downloadFile(url: string, data?: any, params?: any, fileName?: string, mimeType?: any) {
    this.helperService.isProcessing(true);
    this.httpClient.post(url, data, {
      observe: 'response',
      responseType: 'arraybuffer',
      params
    })
      .pipe(
        finalize(() => {
          this.helperService.isProcessing(false);
        })
      ).subscribe((res) => {
      this.helperService.APP_TOAST_MESSAGE.next(res);
      try {
        const response = JSON.parse(new TextDecoder('utf-8').decode(res.body));
        if (response.status.code === '200') {
          this.saveFile(response.data, fileName, mimeType);
          return;
        }
        this.toastr.error(response.status.message);
      } catch {
        this.saveFile(res.body, fileName, mimeType);
      }
    }, error => {
      this.helperService.APP_TOAST_MESSAGE.next(error);
      this.toastr.error(this.translateService.instant('common.notify.fail'));
    });
  }

  downloadFileImport(url: string, data?: any, params?: any, fileName?: string, mimeType?: any) {
    this.helperService.isProcessing(true);
    this.httpClient.post(url, data, {
      observe: 'response',
      responseType: 'arraybuffer',
      params
    })
      .pipe(
        finalize(() => {
          this.helperService.isProcessing(false);
        })
      ).subscribe((res) => {
      this.helperService.APP_TOAST_MESSAGE.next(res);
      try {

        if (res.status == 200) {
          this.toastr.error(this.translateService.instant('common.notify.import.fail'));
          this.saveFile(res.body, fileName, mimeType);
          return;
        }
        if(res.status == 202){
          this.toastr.success(this.translateService.instant('common.notify.import.success'));
          return;
        }
        // this.toastr.error(response.status.message);
      } catch {
        this.toastr.error(this.translateService.instant('common.notify.import.fail'));
        // this.saveFile(res.body, fileName, mimeType);
      }
    }, error => {
      this.helperService.APP_TOAST_MESSAGE.next(error);
      this.toastr.error(this.translateService.instant('common.notify.fail'));
    });
  }
  saveFile(data: any, filename?: string, mimeType?: any) {
    const blob = new Blob([data], {type: mimeType || 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'});
    fileSaver.saveAs(blob, filename);
  }

  openDialogConfirm(message: string, title: string, width?: string) {
    const dialogData = new ConfirmDialogModel(title, message);
    const dialogRef = this.dialog.originalOpen(ConfirmDialogComponent, {
      width: width || '400px',
      data: dialogData
    });
    return dialogRef.afterClosed();
  }
}
