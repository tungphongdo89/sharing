import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'formatNumber'
})
export class FormatNumberPipe implements PipeTransform {

  transform(value: any, separator? : any, valueEmpty? : any, roundValue? : any) {
    try {
      if (!separator || separator.trim() === '') {
        separator = ',';
      }
      let numberValue: any = parseFloat((value + '').replace(/\,/gi, '.'));
      if (isNaN(numberValue)) {
        if (valueEmpty === undefined || valueEmpty == null) {
          return 0;
        } else {
          return valueEmpty;
        }
      }
      if (roundValue !== undefined && roundValue >= 0) {
        numberValue = numberValue.toFixed(roundValue);
      }
      else {
        numberValue = numberValue.toFixed(0);
      }
      let numberString = numberValue.toString().trim().replace(',', '.');
      let negative = '';
      // kiểm tra nếu số truyen v?�o l?� số ?�m (nho hơn 0) th?� g?�n dấu trừ "-" ra 1 biến;
      if (numberString.indexOf('-') >= 0) {
        negative = '-';
        numberString = numberString.replace('-', '');
      }
      const res = numberString.split('.');
      const digitPart = res[0]; // digitPart: Phần nguy?�n
      const l = digitPart.length;
      let rs = '';
      let count = 0;
      for (let i = l - 1; i >= 0; i--) {// Duyệt từng chữ số, cứ 3 chữ số th?� + th?�m dấu ph?�n c?�ch h?�ng ngh?�n
        count++;
        if (count === 3 && i > 0) {
          rs = separator + digitPart[i] + rs;
          count = 0;
        } else {
          rs = digitPart[i] + rs;
        }
      }

      if (res[1] != null && res[1] !== '' && res[1] !== undefined && parseFloat(res[1]) > 0) {// kiểm tra phần thập ph?�n
        for (let i = res[1].length - 1; i > -1; i--) {// x?�a c?�c chữ số 0 v?� nghĩa ph?�a sau
          if (res[1][i] === 0 || res[1][i] === '0') {
            res[1] = res[1].substr(0, res[1].length - 1);
          } else {
            break;
          }
        }
        if (res[1].length > 0) {// Sau khi x?�a số 0, nếu phần thập ph?�n vẫn c?�n th?� + v?�o phần nguy?�n với dấu ph?�n c?�ch thập ph?�n
          // Nối digitPart v?� decimalPart. Nếu Separator l?� "."  th?� Decimal symbol l?� "," v?� ngược lại
          rs += separator === '.' ? ',' : '.';
          rs += res[1];
        }
      }
      // nối negative v?� value
      rs = negative + rs;
      return rs === '-0' ? '0' : rs;
    } catch (e) {
      return 0;
    }
  }

}
