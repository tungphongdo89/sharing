import {Pipe, PipeTransform} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {FormControl} from '@angular/forms';

@Pipe({
  name: 'formErrorMessage',
  pure: false,
})
export class FormErrorMessagePipe implements PipeTransform {

  constructor(private translateService: TranslateService) {
  }

  static readonly ERROR_MESSAGE_MAP: {
    [key: string]: {
      messageKey: string;
      paramKeys: string[];
    },
  } = {
    min: {
      messageKey: 'common.validate.min',
      paramKeys: ['min', 'actual'],
    },
    required: {
      messageKey: 'validation.required',
      paramKeys: [],
    },
    email: {
      messageKey: 'enter_correct_email_format',
      paramKeys: [],
    },
    pattern: {
      messageKey: 'validation.malformed',
      paramKeys: [],
    },
    maxlength: {
      messageKey: 'validation.max_length',
      paramKeys: ['requiredLength', 'actualLength'],
    }
  };

  transform(control: any, ...args: any[]): string {
    const keys = Object.keys(control.errors);
    const label = args[0];
    if(keys?.length > 0) {
      const key = keys[0];
      const configObj = FormErrorMessagePipe.ERROR_MESSAGE_MAP[key];
      if (configObj) {
        const params = {
          field: label,
        };
        for (const paramKey of configObj.paramKeys) {
          params[paramKey] = control.errors[key][paramKey];
        }
        return this.translateService.instant(configObj.messageKey, params);
      }
    }
    return 'UNKNOWN_ERROR';
  }

}
