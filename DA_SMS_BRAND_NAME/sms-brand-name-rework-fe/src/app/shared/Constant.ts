export class Constant {

  public static readonly OK = '200';
  public static readonly ALIAS_TMP_APPROVE_PENDING_RESTORE = 9;
  public static readonly ALIAS_TMP_APPROVE_PENDING_CANCEL = 4;
  public static readonly PATTERN_VN = /^[a-z0-9A-Z _aAàÀảẢãÃáÁạẠăĂằẰẳẲẵẴắẮặẶâÂầẦẩẨẫẪấẤậẬbBcCdDđĐeEèÈẻẺẽẼéÉẹẸêÊềỀểỂễỄếẾệỆfFgGhHiIìÌỉỈĩĨíÍịỊjJkKlLmMnNoOòÒỏỎõÕóÓọỌôÔồỒổỔỗỖốỐộỘơƠờỜởỞỡỠớỚợỢpPqQrRsStTuUùÙủỦũŨúÚụỤưƯừỪửỬữỮứỨựỰvVwWxXyYỳỲỷỶỹỸýÝỵỴzZ]*$/;
  public static readonly PATTERN_CURRENCY_VN = /^(([0-9]+)([\,]([0-9]+)){0,4}|([\,]([0-9]+))?)([\.]([0-9]+))?$/;
  public static readonly PATTERN_IP = /^([1-9][0-9]{0,2})([\.]([1-9][0-9]{0,2})){3}$/;
  public static readonly TEMPLATE_TYPE = 'CAMPAIN_TEMPLATE_TYPE';
  public static readonly TEXT_BOX_TYPE_ID = '21';
  public static readonly CBX_TYPE_ID = '22';
  public static readonly DATE_TYPE_ID = '23';
  public static readonly DATETIME_TYPE_ID = '24';
  public static readonly TIME_TYPE_ID = '25';
  public static readonly TEXT_AREA_TYPE_ID = '26';
  public static readonly CALL_PHONE_TYPE_ID = '27';
  public static readonly DIALOG_CONFIRM = 'confirm';
  public static readonly DIALOG_CANCEL = 'cancel';
  public static readonly TIME_TYPE_DAILY = 'D';
  public static readonly TIME_TYPE_WEEKLY = 'W';
  public static readonly TIME_TYPE_ONCE = 'O';
  public static readonly ANSWER_TYPE_TEXT = '3';
  public static readonly ANSWER_TYPE_RADIO = '1';
  public static readonly ANSWER_TYPE_CHECKBOX = '2';
  public static readonly ANSWER_TYPE_RANKING = '4';
  public static readonly EDITABLE = '1';
}
