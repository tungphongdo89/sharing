import {HttpClient, HttpClientModule} from '@angular/common/http';
import {APP_INITIALIZER, LOCALE_ID, NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {httpInterceptorProviders, TranslateLangService} from '@core';
import {FormlyModule} from '@ngx-formly/core';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
// @ts-ignore
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {ToastrModule} from 'ngx-toastr';
import {AppComponent} from './app.component';
import {CoreModule} from './core/core.module';
import {RoutesModule} from './routes/routes.module';
import {SharedModule} from './shared/shared.module';
import {ThemeModule} from './theme/theme.module';
import {NgxWebstorageModule} from 'ngx-webstorage';
import {default as ngLang} from '@angular/common/locales/vi';

const LANG = {
  abbr: 'vi-VN',
  ng: ngLang
};
// register angular
import {DatePipe, DecimalPipe, registerLocaleData} from '@angular/common';

registerLocaleData(LANG.ng, LANG.abbr);
const LANG_PROVIDES = [
  {provide: LOCALE_ID, useValue: LANG.abbr}
];

// Required for AOT compilation
export function TranslateHttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

export function TranslateLangServiceFactory(translateLangService: TranslateLangService) {
  return () => translateLangService.load();
}

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    HttpClientModule,
    CoreModule,
    SharedModule,
    ThemeModule,
    RoutesModule,
    FormlyModule.forRoot(),
    ToastrModule.forRoot(),
    NgxWebstorageModule.forRoot({prefix: 'crm', separator: '-'}),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: TranslateHttpLoaderFactory,
        deps: [HttpClient],
      }
    }),
    BrowserAnimationsModule,
  ],
  providers: [
    {provide: LOCALE_ID, useValue: LANG.abbr},
    httpInterceptorProviders,
    {
      provide: APP_INITIALIZER,
      useFactory: TranslateLangServiceFactory,
      deps: [TranslateLangService],
      multi: true,
    },
    DatePipe,
    DecimalPipe
  ],
  bootstrap: [AppComponent],
})
export class AppModule {
}
