import {Routes, RouterModule} from '@angular/router';
import {NgModule} from '@angular/core';
import {ReportProcessTimeComponent} from '@app/routes/report-process-time/report-process-time/report-process-time.component';

const routes: Routes = [
  {
    path: '',
    component: ReportProcessTimeComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportProcessTimeRoutingModule {
}
