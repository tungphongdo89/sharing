import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReportProcessTimeComponent } from './report-process-time/report-process-time.component';
import {SharedModule} from '@shared';
import {ReportProcessTimeRoutingModule} from '@app/routes/report-process-time/report-process-time-routing.module';



@NgModule({
  declarations: [ReportProcessTimeComponent],
  imports: [
    CommonModule,
    SharedModule,
    ReportProcessTimeRoutingModule
  ]
})
export class ReportProcessTimeModule { }
