import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {MtxGridColumn} from '@ng-matero/extensions';
import {ApDomainService} from '@core/services/ap-domain/ap-domain.service';
import {DepartmentService} from '@core/services/department/department.service';
import {BussinessTypeService} from '@core/services/bussiness-type/bussiness-type.service';
import {ReportComplainDetailService} from '@core/services/report/report-complain-detail.service';
import {CommonService} from '@shared/services/common.service';
import {map} from 'rxjs/operators';
import {ReportProcessTimeService} from '@core/services/report/report-process-time.service';

@Component({
  selector: 'app-report-process-time',
  templateUrl: './report-process-time.component.html',
  styleUrls: ['./report-process-time.component.scss']
})
export class ReportProcessTimeComponent implements OnInit {
  today = new Date();
  @Output() pageChange: EventEmitter<any> = new EventEmitter<any>();
  chanelTypes: any = [];
  requestTypes: any = [];
  priorities: any = [];
  businessTypes:  any = [];
  ticketRequestStatus: any = [];
  ticketStatus: any = [];
  departments: any = [];
  offset: any = 0;
  reportDetailForm: FormGroup = this.fb.group({
    idCode: null,
    phoneNumber: null,
    processDeadline: null,
    confirmDeadline: null,
    requestStatus: null,
    ticketStatus: null,
    fromDate: null,
    toDate: null
  });
  data: any = [];
  columns: MtxGridColumn[] = [
    {i18n: 'report-process-time.management.column.no', field: 'index'},
    {i18n: 'report-process-time.management.column.ticketCode', field: 'ticketCode'},
    {i18n: 'report-process-time.management.column.ticketRequestCode', field: 'ticketRequestCode'},
    {i18n: 'report-process-time.management.column.idCode', field: 'idCode'},
    {i18n: 'report-process-time.management.column.phoneNumber', field: 'phoneNumber'},
    {i18n: 'report-process-time.management.column.department', field: 'departmentName'},
    {i18n: 'report-process-time.management.column.priority', field: 'priority'},
    {i18n: 'report-process-time.management.column.requestStatus', field: 'requestStatus'},
    {i18n: 'report-process-time.management.column.processDeadline', field: 'processDeadline'},
    {i18n: 'report-process-time.management.column.confirmDeadline', field: 'confirmDeadline'},
    {i18n: 'report-process-time.management.column.receiveDate', field: 'receiveDate'},
    {i18n: 'report-process-time.management.column.receiveUser', field: 'receiveUser'},
    {i18n: 'report-process-time.management.column.processDate', field: 'processDate'},
    {i18n: 'report-process-time.management.column.processUser', field: 'processUser'},
    {i18n: 'report-process-time.management.column.confirmDate', field: 'confirmDate'},
    {i18n: 'report-process-time.management.column.confirmUser', field: 'confirmUser'},
  ];
  pageSizeList = [10, 50, 100];
  pagesize = 10;
  isLoading = false;
  total: any = 0;
  constructor(private fb: FormBuilder,
              private apDomainService: ApDomainService,
              private departmentService: DepartmentService,
              private bussinessTypeService: BussinessTypeService,
              private reportProcessTimeService: ReportProcessTimeService,
              private commonService: CommonService
  ) { }

  ngOnInit(): void {
    this.initData();
    // this.search();
  }
  initData() {
    // Kênh tiếp nhận
    this.apDomainService.getDomainByType('CHANEL_TYPE').subscribe(res => {
      this.chanelTypes = res;
    });
    // Loại yêu cầu
    this.apDomainService.getDomainByType('REQUEST_TYPE').subscribe(res => {
      this.requestTypes = res;
    });
    // Trạng thái yêu cầu
    this.apDomainService.getDomainByType('TICKET_REQUEST_STATUS').subscribe(res => {
      this.ticketRequestStatus = res;
    });
    // Trạng thái sự vụ
    this.apDomainService.getDomainByType('TICKET_STATUS').subscribe(res => {
      this.ticketStatus = res;
    });
    // Phòng ban
    this.departmentService.getLstDepartment().subscribe(res => {
      this.departments = res;
    });
    // Loại nghiệp vụ
    this.bussinessTypeService.getBussinessType().subscribe(res => {
      this.businessTypes = res;
    });
  }

  search(reset?: boolean) {
    if (reset) {
      this.offset = 0;
      this.pagesize = 10;
    }
    const data = {...this.reportDetailForm.value, queryType: 1, size: this.pagesize, page: this.offset};
    this.reportProcessTimeService.query(data, { size: this.pagesize, page: this.offset}).pipe(map(res => {
      return res;
    })).subscribe(res => {
      this.data = res.body;
      this.total = Number(res.headers.get('X-Total-Count'));
    });
  }
  export() {
    const data = {...this.reportDetailForm.value};
    this.reportProcessTimeService.export(data);
  }
  onPageChange(event) {
    this.offset = event.pageIndex;
    this.pagesize = event.pageSize;
    this.search();
  }
  onChangeTicketStatus() {
    if (this.reportDetailForm.get('ticketStatus').value === '1') {
      this.apDomainService.getDomainByType('TICKET_REQUEST_STATUS').subscribe(res => {
        this.ticketRequestStatus = res;
      });
    }
    if (this.reportDetailForm.get('ticketStatus').value === '2') {
      this.apDomainService.getDomainByType('TICKET_REQUEST_STATUS').subscribe(res => {
        this.ticketRequestStatus = res;
        this.ticketRequestStatus = this.ticketRequestStatus.filter(function(item){
          return item.code !== '1';
        });
      });
    }
    if (this.reportDetailForm.get('ticketStatus').value === '3' || this.reportDetailForm.get('ticketStatus').value === '4' || this.reportDetailForm.get('ticketStatus').value === '5') {
      this.ticketRequestStatus = this.ticketRequestStatus.filter(function(item){
        return item.code !== '1' && item.code !== '2';
      });
    }
  }
}
