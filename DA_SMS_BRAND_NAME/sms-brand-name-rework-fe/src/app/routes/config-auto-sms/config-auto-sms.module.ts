import {NgModule} from '@angular/core';
import {CommonModule, DatePipe} from '@angular/common';
import {SharedModule} from '@shared';
import {UpdateConfigAutoSmsComponent} from './update-config-auto-sms/update-config-auto-sms.component';
import {NgxMaterialTimepickerModule} from 'ngx-material-timepicker';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {ConfigAutoSmsRoutingModule} from '@app/routes/config-auto-sms/config-auto-sms-routing.module';
import {ConfigAutoSmsComponent} from '@app/routes/config-auto-sms/config-auto-sms/config-auto-sms.component';


@NgModule({
  declarations: [ConfigAutoSmsComponent, UpdateConfigAutoSmsComponent],
  imports: [
    CommonModule,
    SharedModule,
    ConfigAutoSmsRoutingModule,
    NgxMaterialTimepickerModule,
    MatFormFieldModule,
    MatInputModule
  ],
  providers: [
    DatePipe
  ]
})
export class ConfigAutoSmsModule {
}
