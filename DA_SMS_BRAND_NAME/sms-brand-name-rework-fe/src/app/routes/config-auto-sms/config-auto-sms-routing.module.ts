import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {ConfigAutoSmsComponent} from '@app/routes/config-auto-sms/config-auto-sms/config-auto-sms.component';

const routes: Routes = [
  {
    path: '',
    component: ConfigAutoSmsComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ConfigAutoSmsRoutingModule {
}
