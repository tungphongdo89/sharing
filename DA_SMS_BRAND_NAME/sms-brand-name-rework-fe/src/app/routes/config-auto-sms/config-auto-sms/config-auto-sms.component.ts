import {Component, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {ToastrService} from 'ngx-toastr';
import {TranslateService} from '@ngx-translate/core';
import {MtxDialog, MtxGridColumn} from '@ng-matero/extensions';
import {FormBuilder, FormGroup, NgForm, Validators} from '@angular/forms';
import {Constant} from '@shared/Constant';
import {finalize} from 'rxjs/operators';
import {ConfirmDialogComponent, ConfirmDialogModel} from '@shared/components/confirm-dialog/confirm-dialog.component';
import {ConfigAutoSmsService} from '@core/services/config-auto-sms/config-auto-sms.service';
import {UpdateConfigAutoSmsComponent} from '@app/routes/config-auto-sms/update-config-auto-sms/update-config-auto-sms.component';

@Component({
  selector: 'app-config-auto-email',
  templateUrl: './config-auto-sms.component.html',
  styleUrls: ['./config-auto-sms.component.scss']
})
export class ConfigAutoSmsComponent implements OnInit {
  form: FormGroup = this.fb.group({
    campaignSMSId: [null, Validators.required],
    name: [null, [Validators.required, Validators.maxLength(250)]],
    timeType: [null, Validators.required],
    dateOfWeek: [null],
    timeSend: [null, Validators.required],
    dateSend: [null],
    status: [null]
  });
  @ViewChild('button', {static: true}) button: TemplateRef<any>;
  @ViewChild('index', {static: true}) index: TemplateRef<any>;
  @ViewChild('timeTypeName', {static: true}) timeTypeName: TemplateRef<any>;
  @ViewChild('dateOfWeek', {static: true}) dateOfWeek: TemplateRef<any>;
  @ViewChild('dateSend', {static: true}) dateSend: TemplateRef<any>;
  @ViewChild('createTime', {static: true}) createTime: TemplateRef<any>;
  @ViewChild('status', {static: true}) status: TemplateRef<any>;
  columns: MtxGridColumn[] = [
    {
      i18n: 'No',
      width: '30px',
      field: 'no',
      cellTemplate: this.index
    },
    {i18n: 'config_auto_sms.configuration_name', width: '180px', field: 'name'},
    {i18n: 'config_auto_sms.campaign_name', width: '180px', field: 'campaignSMSName'},
    {i18n: 'config_auto_sms.time_type', width: '120px', field: 'type', cellTemplate: this.timeTypeName},
    {i18n: 'config_auto_sms.Weekdays', width: '120px', field: 'day', cellTemplate: this.dateOfWeek},
    {i18n: 'config_auto_sms.sent_date', width: '120px', field: 'dateS'},
    {i18n: 'config_auto_sms.sending_time', width: '130px', field: 'timeSend'},
    {i18n: 'config_auto_sms.Status', width: '150px', field: 'trangThai'},
    {i18n: 'email_blacklist.Creator', width: '150px', field: 'createUserName'},
    {i18n: 'email_blacklist.Date_created', width: '120px', field: 'time'},
    {
      i18n: 'common.action',
      width: '150px',
      pinned: 'right',
      right: '0px',
      field: 'options',
      cellTemplate: this.button,
    }
  ];
  lstCampaignSMSMarketing: any[] = [];
  lstConfigAutoSMS: any[] = [];
  lstTimeType = [
    {code: Constant.TIME_TYPE_DAILY, value: this.translate.instant('Daily')},
    {code: Constant.TIME_TYPE_WEEKLY, value: this.translate.instant('Weekly')},
    {code: Constant.TIME_TYPE_ONCE, value: this.translate.instant('Once')}];
  pageSizeList = [10, 50, 100];
  pageSize = 10;
  totalPage = 0;
  page: number = 0;
  today = new Date();
  lstDateOfWeek = [
    {code: 'MONDAY', value: this.translate.instant('Monday')},
    {code: 'TUESDAY', value: this.translate.instant('Tuesday')},
    {code: 'WEDNESDAY', value: this.translate.instant('Wednesday')},
    {code: 'THURSDAY', value: this.translate.instant('Thursday')},
    {code: 'FRIDAY', value: this.translate.instant('Friday')},
    {code: 'SATURDAY', value: this.translate.instant('Saturday')},
    {code: 'SUNDAY', value: this.translate.instant('Sunday')},
  ];
  @ViewChild('ngForm') ngForm: NgForm;

  constructor(private configAutoSmsService: ConfigAutoSmsService,
              private toast: ToastrService,
              private translate: TranslateService,
              private dialog: MtxDialog,
              private fb: FormBuilder,
  ) {
  }

  ngOnInit(): void {
    this.init();
  }

  init() {
    this.form.patchValue({status: true});
    this.form.patchValue({timeType: null});
    this.configAutoSmsService.findAllCampaignSMSMarketing().subscribe(res => {
      this.lstCampaignSMSMarketing = res;
      this.form.patchValue({campaignSMSId: null});
      this.search();
    });

  }

  search(reset?: boolean) {
    if (reset) {
      this.page = 0;
      this.pageSize = 10;
    }
    const data: any = this.form.value;
    data.size = this.pageSize;
    data.page = this.page;
    this.configAutoSmsService.search(data)
      .pipe(finalize(() => {
        this.lstConfigAutoSMS?.forEach((config: any) => {
          config.campaignSMSName = this.lstCampaignSMSMarketing.filter((e) => e.id === config.campaignSMSId)[0].name;
          config.timeTypeName = this.lstTimeType.filter((e) => e.code === config.timeType)[0]?.value;
          config.dateOfWeekValue = '';
          config.dateOfWeekArray = config.dateOfWeek?.split(',');
          config.dateOfWeekArray?.forEach(item => {
            config.dateOfWeekValue += this.lstDateOfWeek.filter((e) => e.code === item)[0]?.value + ', ';
          });
          config.dateOfWeekValue = config.dateOfWeekValue?.substring(0, config.dateOfWeekValue?.lastIndexOf(','));
        });
      }))
      .subscribe(res => {
        this.lstConfigAutoSMS = res.body;
        this.totalPage = res.headers.get('X-Total-Count');
      }, error => this.toast.error('', error));
  }

  create() {
    if (this.form.invalid) {
      return;
    }
    switch (this.form.get('timeType').value) {
      case Constant.TIME_TYPE_WEEKLY: {
        if (!this.form.get('dateOfWeek').value || this.form.get('dateOfWeek').value === '') {
          this.toast.warning('', this.translate.instant('config_auto_email.enter_day_of_week'));
          return;
        }
        break;
      }
      case Constant.TIME_TYPE_ONCE: {
        if (!this.form.get('dateSend').value || this.form.get('dateSend').value === ''){
          this.toast.warning('', this.translate.instant('config_auto_email.enter_sending_date'));
          return;
        }
        break;
      }
    }
    const data = this.form.value;
    data.dateOfWeek = data.dateOfWeek?.join();
    this.configAutoSmsService.create(data).subscribe(res => {
        this.toast.success('', this.translate.instant('successfully_added_new'));
        this.ngForm.resetForm();
        this.search();
        this.form.patchValue({status: true});
      },
      error => {
        this.toast.error('', error);
      });

  }

  onPageChange(event) {
    this.page = event.pageIndex;
    this.pageSize = event.pageSize;
    this.search();
  }


  delete(row: any) {
    const title = this.translate.instant('common.confirm.title.delete');
    const content = this.translate.instant('common.MSG0017');
    const dialogContent: ConfirmDialogModel = new ConfirmDialogModel(title, content);
    this.dialog.originalOpen(ConfirmDialogComponent, {
      width: '25vw',
      disableClose: true,
      hasBackdrop: true,
      data: dialogContent
    }).afterClosed().subscribe(result => {
      if (result.data === 'confirm') {
        this.configAutoSmsService.delete(row.id).subscribe(res => {
            this.toast.success('', this.translate.instant('delete_successfully'));
            this.search();
          }, error => this.toast.error('', error)
        );
      }
    });
  }

  update(row: any) {
    this.dialog.originalOpen(UpdateConfigAutoSmsComponent, {
      disableClose: true,
      hasBackdrop: true,
      data: row
    }).afterClosed().subscribe(result => {
      if (result) {
        this.search();
      }
    });
  }

  changeTimeType() {
    this.form.get('dateSend').reset();
    this.form.get('dateOfWeek').reset();
  }
}
