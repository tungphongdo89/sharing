import {Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef, ViewChild} from '@angular/core';
import {
  AccumulationChart,
  AccumulationChartComponent,
  AccumulationTheme,
  IAccLoadedEventArgs
} from '@syncfusion/ej2-angular-charts';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DashboardComponent implements OnInit {

  constructor(private cdr: ChangeDetectorRef) {
  }

  public primaryXAxis: any;
  public chartData: any = [];

  public data: any[] = [
    {x: 'Chrome', y: 37, text: '37%'}, {x: 'UC Browser', y: 17, text: '17%'},
    {x: 'iPhone', y: 19, text: '19%'},
    {x: 'Others', y: 4, text: '4%'}, {x: 'Opera', y: 11, text: '11%'},
    {x: 'Android', y: 12, text: '12%'}
  ];

  @ViewChild('pie')
  public pie: AccumulationChartComponent | AccumulationChart;
  public animation: any = {
    enable: false
  };
  // Initializing Legend
  public legendSettings: any = {
    visible: false,
  };
  // Initializing Datalabel
  public dataLabel: any = {
    visible: true,
    position: 'Inside', name: 'text',
    font: {
      fontWeight: '600'
    }
  };
  // custom code end
  public center: any = {x: '50%', y: '50%'};
  public startAngle: number = 0;
  public endAngle: number = 360;
  public explode: boolean = true;
  public enableAnimation: boolean = false;
  public tooltip: any = {enable: true, format: '${point.x} : <b>${point.y}%</b>'};
  public title: string = 'Mobile Browser Statistics';

  public pieangle(e: Event): void {
    const angle: string = (document.getElementById('pieangle') as HTMLInputElement).value;
    this.pie.series[0].startAngle = parseFloat(angle);
    this.pie.series[0].endAngle = parseFloat(angle);
    this.pie.series[0].animation.enable = false;
    document.getElementById('pieangleText').innerHTML = angle;
    this.pie.removeSvg();
    this.pie.refreshSeries();
    this.pie.refreshChart();
  }

  public pieradius(e: Event): void {
    const radius: string = (document.getElementById('pieradius') as HTMLInputElement).value;
    this.pie.series[0].radius = radius + '%';
    document.getElementById('pieradiusText').innerHTML = (parseInt(radius, 10) / 100).toFixed(2);
    this.pie.series[0].animation.enable = false;
    this.pie.removeSvg();
    this.pie.refreshSeries();
    this.pie.refreshChart();
  }

  public pieexploderadius(e: Event): void {
    const radius: string = (document.getElementById('pieexploderadius') as HTMLInputElement).value;
    this.pie.visibleSeries[0].explodeOffset = radius + '%';
    document.getElementById('pieexploderadiusText').innerHTML = (parseInt(radius, 10) / 100).toFixed(2);
    this.pie.series[0].animation.enable = false;
    this.pie.removeSvg();
    this.pie.refreshSeries();
    this.pie.refreshChart();
  }

  public pieexplodeindex(e: Event): void {
    const index: number = +(document.getElementById('pieexplodeindex') as HTMLInputElement).value;
    this.pie.visibleSeries[0].explodeIndex = index;
    document.getElementById('pieexplodeindexText').innerHTML = index.toString();
    this.pie.series[0].animation.enable = false;
    this.pie.removeSvg();
    this.pie.refreshSeries();
    this.pie.refreshChart();
  }

  public piecenterx(e: Event): void {
    const x: string = (document.getElementById('x') as HTMLInputElement).value;
    this.pie.center.x = x + '%';
    document.getElementById('xvalue').innerHTML = x + '%';
    this.pie.series[0].animation.enable = false;
    this.pie.removeSvg();
    this.pie.refreshSeries();
    this.pie.refreshChart();
  }

  public piecentery(e: Event): void {
    const y: string = (document.getElementById('y') as HTMLInputElement).value;
    this.pie.center.y = y + '%';
    document.getElementById('yvalue').innerHTML = y + '%';
    this.pie.series[0].animation.enable = false;
    this.pie.removeSvg();
    this.pie.refreshSeries();
    this.pie.refreshChart();
  }

  // custom code start
  public load(args: IAccLoadedEventArgs): void {
    let selectedTheme: string = location.hash.split('/')[1];
    selectedTheme = selectedTheme ? selectedTheme : 'Material';
    args.accumulation.theme = ((selectedTheme.charAt(0).toUpperCase() + selectedTheme.slice(1)).replace(/-dark/i, 'Dark') as AccumulationTheme);
  }

  ngOnInit() {
    this.chartData = [
      {month: 'Jan', sales: 35}, {month: 'Feb', sales: 28},
      {month: 'Mar', sales: 34}, {month: 'Apr', sales: 32},
      {month: 'May', sales: 40}, {month: 'Jun', sales: 32},
      {month: 'Jul', sales: 35}, {month: 'Aug', sales: 55},
      {month: 'Sep', sales: 38}, {month: 'Oct', sales: 30},
      {month: 'Nov', sales: 25}, {month: 'Dec', sales: 32}
    ];
    this.primaryXAxis = {
      valueType: 'Category'
    };
  }
}
