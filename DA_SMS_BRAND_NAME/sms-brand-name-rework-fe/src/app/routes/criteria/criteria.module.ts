import { NgModule } from '@angular/core';
import { CriteriaConfigComponent } from '@app/routes/criteria/criteria-config/criteria-config.component';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@shared';
import { CriteriaRouting } from '@app/routes/criteria/criteria.routing';
import { CriteriaDetailComponent } from './criteria-detail/criteria-detail.component';

@NgModule({
  declarations: [CriteriaConfigComponent, CriteriaDetailComponent],
  imports: [
    CommonModule,
    SharedModule,
    CriteriaRouting
  ]
})
export class CriteriaModule {}
