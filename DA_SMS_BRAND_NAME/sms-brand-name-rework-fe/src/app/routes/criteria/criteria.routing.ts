import { RouterModule, Routes } from '@angular/router';
import { CriteriaConfigComponent } from '@app/routes/criteria/criteria-config/criteria-config.component';
import { NgModule } from '@angular/core';
import {CriteriaDetailComponent} from '@app/routes/criteria/criteria-detail/criteria-detail.component';

const routes: Routes = [
  {
    path: '',
    component: CriteriaConfigComponent
  },
  {
    path: 'criteria-detail',
    component: CriteriaDetailComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CriteriaRouting {}
