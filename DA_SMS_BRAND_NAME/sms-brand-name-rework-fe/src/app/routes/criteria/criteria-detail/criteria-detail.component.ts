import {Component, Inject, OnInit, Optional} from '@angular/core';
import {
  HtmlEditorService,
  ImageService,
  LinkService,
  TableService,
  ToolbarService
} from '@syncfusion/ej2-angular-richtexteditor';
import {Router} from '@angular/router';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {CriteriaGroupService} from '@core/services/criteria-group/criteria-group.service';
import {ToastrService} from 'ngx-toastr';
import {MAT_DIALOG_DATA, MatDialog} from '@angular/material/dialog';
import {TranslateService} from '@ngx-translate/core';
import {MtxGridColumn} from '@ng-matero/extensions';
import {CriteriaDetailService} from '@core/services/criteria-detail/criteria-detail.service';
import {CriteriaGroupModel} from '@core/models/criteria-group-model';
import {PopupDeleteComponent} from '@app/routes/directory-management/popup-delete/popup-delete.component';
import {CriteriaDetailModel} from '@core/models/criteria-detail-model';
import {CampaignModel} from '@core/models/campaign';
import {CriteriaService} from '@core/services/criteria/criteria.service';

@Component({
  selector: 'app-criteria-detail',
  templateUrl: './criteria-detail.component.html',
  styleUrls: ['./criteria-detail.component.scss'],
  providers: [ToolbarService, LinkService, ImageService, HtmlEditorService, TableService]
})
export class CriteriaDetailComponent implements OnInit {

  public tools: object = {
    items: [
      'Bold', 'Italic', 'Underline', 'StrikeThrough', '|',
      'FontName', 'FontSize', 'FontColor', 'BackgroundColor', '|',
      'LowerCase', 'UpperCase', '|', 'Undo', 'Redo', '|',
      'Formats', 'Alignments', '|', 'OrderedList', 'UnorderedList', '|',
      'Indent', 'Outdent', '|', 'CreateLink', 'CreateTable',
      'Image', '|', 'ClearFormat', 'Print', 'SourceCode', '|', 'FullScreen'],
    type: 'Scrollable',
    enableFloating: false
  };

  formSearch: FormGroup;
  criteriaDetailForm: FormGroup;
  data: any = [];
  criteriaGroup: any = [];
  criteria: any = [];
  searchText: any = '';
  columns: MtxGridColumn[] = [];
  pageSizeList = [10, 50, 100];
  isLoading = false;
  offset: any = 0;
  pagesize = 10;
  total: any = 0;
  buttonTitle: string;
  buttonTitle2: string;
  criteriaDetailData: any = {};
  criteriaData: any = {};
  criteriaScores: any;

  constructor(
    private router: Router,
    private fb: FormBuilder,
    @Optional() @Inject(MAT_DIALOG_DATA) public dataCriteriaDetail: CriteriaDetailModel,
    private criteriaGroupService: CriteriaGroupService,
    private criteriaDetailService: CriteriaDetailService,
    private criteriaService: CriteriaService,
    private toast: ToastrService,
    private dialog: MatDialog,
    protected translateService: TranslateService
  ) {
    this.criteriaDetailData = dataCriteriaDetail;
  }

  ngOnInit(): void {
    this.criteriaDetailForm = this.fb.group({
      id: new FormControl(null),
      criteriaId: new FormControl(null, [Validators.required]),
      criteriaGroupId: new FormControl(null, [Validators.required]),
      content: new FormControl(null, [Validators.required, Validators.maxLength(500)]),
      description: new FormControl(null),
      scores: new FormControl(null, [Validators.required, Validators.min(1), Validators.max(this.criteriaScores)]),
      status: new FormControl(null),
      createDatetime: new FormControl(null),
      createUser: new FormControl(null),
      updateDatetime: new FormControl(null),
      updateUser: new FormControl(null),
    });
    this.loadData();
    this.checkButton();
  }

  checkButton() {
    if (this.criteriaDetailForm.get('id').value !== null) {
      this.buttonTitle = this.translateService.instant('common.button.update');
      this.buttonTitle2 = this.translateService.instant('common.button.back');
    } else {
      this.buttonTitle = this.translateService.instant('common.button.insert');
      this.buttonTitle2 = this.translateService.instant('common.button.back');
    }
  }

  addCriteriaDetail(){
    if (Number(this.criteriaDetailForm.get('scores').value) > Number(this.criteriaScores)){
      this.toast.warning(this.translateService.instant('criteria_detail.msg_waning'));
      return;
    }
    if (this.criteriaDetailForm.get('id').value){
      this.criteriaDetailService.updateCriteriaDetail(this.criteriaDetailForm.value).subscribe(res => {
        if (res.body.message === 'ok'){
          this.toast.success(this.translateService.instant('criteria_detail.msg_update_success'));
          // this.getAllCriteriaGroup(true);
          // this.criteriaDetailForm.reset();
          // this.backCriteria();
          this.criteriaDetailForm.get('content').reset();
          this.criteriaDetailForm.get('scores').reset();
          this.checkButton();
        } else {
          this.toast.error(this.translateService.instant('criteria_detail.msg_update_error'));
        }
      });
    } else {
      this.criteriaDetailService.addCriteriaDetail(this.criteriaDetailForm.value).subscribe(res => {
        if (res.body.message === 'ok'){
          this.toast.success(this.translateService.instant('criteria_detail.msg_create_success'));
          // this.getAllCriteriaGroup(true);
          // this.criteriaDetailForm.reset();
          // this.backCriteria();
          this.criteriaDetailForm.get('content').reset();
          this.criteriaDetailForm.get('scores').reset();
        } else {
          this.toast.error(this.translateService.instant('criteria_detail.msg_create_error'));
        }
      });
    }
  }

  deleteCriteriaDetail(criteriaDetailModel: CriteriaDetailModel){
    this.criteriaDetailForm.patchValue(criteriaDetailModel);
    const dialogRef = this.dialog.open(PopupDeleteComponent, {
      data: this.criteriaDetailForm
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result === undefined) {
        return;
      }
      if (result.event !== 'Cancel') {
        this.criteriaDetailService.deleteCriteriaDetail(this.criteriaDetailForm.value).subscribe(rs => {
          if (rs.body.message === 'ok') {
            this.toast.success(this.translateService.instant('criteria_detail.msg_delete_success'));
            this.criteriaDetailForm.reset();
            this.backCriteria();
            // this.getAllCriteriaGroup(false);
          } else {
            this.toast.error(this.translateService.instant('criteria_detail.msg_delete_error'));
          }
        });
      } else {
        this.criteriaDetailForm.reset();
      }
    });
  }

  loadData(){
    if(this.criteriaDetailData !== null){
      this.criteriaDetailForm.patchValue(this.criteriaDetailData);
      this.criteriaService.findOne(this.criteriaDetailForm.get('criteriaId').value).subscribe(res =>{
        console.log('log het ra ', res);
        this.criteriaData = res;
        this.getCriteriaByCriteriaGroupId(this.criteriaDetailForm.get('criteriaGroupId').value);
        this.criteriaScores = res.scores;
      });
    }
    this.initData();
  }

  initData(){
    this.criteriaGroupService.loadCriteriaGroupToCbx().subscribe(res => {
      this.criteriaGroup = res.body;
    });
  }

  getCriteriaByCriteriaGroupId(criteriaId: any){
    console.log('criteria id', criteriaId);
    this.criteriaService.getCriteriaByCriteriaGroupId(criteriaId).subscribe(res => {
      this.criteria = res.body;
    });
  }

  onSelectChange(item: any){
    this.criteriaScores = item.scores;
  }

  backCriteria(){
    this.dialog.closeAll();
  }

}
