import { Component, OnInit } from '@angular/core';
import {MtxDialog, MtxGridColumn} from '@ng-matero/extensions';
import { CriteriaGroupService } from '@core/services/criteria-group/criteria-group.service';
import {Router} from '@angular/router';
import { CriteriaService } from '@core/services/criteria/criteria.service';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ConfirmDialogComponent, ConfirmDialogModel } from '@shared/components/confirm-dialog/confirm-dialog.component';
import {CampaignUpdateComponent} from '@app/routes/campaign/campaign-update/campaign-update.component';
import {CriteriaDetailModel} from '@core/models/criteria-detail-model';
import {MatDialog} from '@angular/material/dialog';
import {CriteriaDetailComponent} from '@app/routes/criteria/criteria-detail/criteria-detail.component';
import {PopupDeleteComponent} from '@app/routes/directory-management/popup-delete/popup-delete.component';

import {Constant} from '@shared/Constant';
import {CriteriaDetailService} from '@core/services/criteria-detail/criteria-detail.service';

@Component({
  selector: 'app-criteria-config',
  templateUrl: './criteria-config.component.html',
  styleUrls: ['./criteria-config.component.scss'],
})
export class CriteriaConfigComponent implements OnInit {

  STATUS_ACTIVE = '1';
  STATUS_INACTIVE = '2';
  lstCriteriaGroup: any = [];
  pageIndex: any;
  pageSize: any;
  dataSearch = {
    page: null,
    size: null,
  };
  criteriaLst: any = [];
  pageSizeList = [10, 50, 100];
  totalRecord: any;
  formControl: FormGroup = this.fb.group({
    criteriaGroupId: new FormControl('', Validators.required),
    name: new FormControl(null, Validators.required),
    remainingScore: new FormControl(null),
    scores: new FormControl(null, Validators.required),
  });
  formSearch: FormGroup = this.fb.group({
    criteriaGroupId: [''],
    keyWord: null,
  });
  formSearchExpire: FormGroup = this.fb.group({
    criteriaGroupId: '',
    keyWord: null,
  });
  pageIndexExpire: any;
  pageSizeExpire: any;
  dataSearchExpire = {
    page: null,
    size: null,
  };
  criteriaLstExpire: any = [];
  pageSizeListExpire = [10, 50, 100];
  totalRecordExpire: any;
  criteriaId = -1;
  isUpdate: boolean = false;

  constructor(
    private criteriaGroupService: CriteriaGroupService,
    private router: Router,
    private dialog: MatDialog,
    public dialogMtx: MtxDialog,
    private service: CriteriaService,
    private criteriaDetailService: CriteriaDetailService,
    private toastr: ToastrService,
    private translate: TranslateService,
    private fb: FormBuilder,
  ) {
  }

  ngOnInit(): void {
    this.criteriaGroupService.loadToCbx().subscribe(res => {
      this.lstCriteriaGroup = res.body;
    });
    this.pageIndex = 0;
    this.pageSize = 10;
    this.pageIndexExpire = 0;
    this.pageSizeExpire = 10;
    this.onSearch();
    this.onSearchExpire();
  }

  onSearch() {
    this.dataSearch.page = this.pageIndex;
    this.dataSearch.size = this.pageSize;
    let criteriaGroupId;
    let keyWord;
    if (this.formSearch.get('criteriaGroupId').value !== '') {
      criteriaGroupId = this.formSearch.get('criteriaGroupId').value;
    } else {
      criteriaGroupId = -1;
    }
    if (this.formSearch.get('keyWord').value) {
      keyWord = this.formSearch.get('keyWord').value;
    } else {
      keyWord = null;
    }
    this.service.doSearch(this.dataSearch, this.STATUS_ACTIVE, criteriaGroupId, keyWord).subscribe(res => {
      if (res) {
        this.criteriaLst = res.body;
        this.totalRecord = Number(res.headers.get('X-Total-Count'));
      }
    }, error => {
      this.toastr.error('campaign-resource.error');
    });
  }

  search() {
    this.formSearch.markAllAsTouched();
    if (this.formSearch.invalid) {
      return;
    }
    this.pageIndex = 0;
    this.onSearch();
  }

  onPageChange(event) {
    this.pageIndex = event.pageIndex;
    this.pageSize = event.pageSize;
    this.onSearch();
  }

  onSearchExpire() {
    this.dataSearchExpire.page = this.pageIndexExpire;
    this.dataSearchExpire.size = this.pageSizeExpire;
    let criteriaGroupId;
    let keyWord;
    if (this.formSearchExpire.get('criteriaGroupId').value !== '') {
      criteriaGroupId = this.formSearchExpire.get('criteriaGroupId').value;
    } else {
      criteriaGroupId = -1;
    }
    if (this.formSearchExpire.get('keyWord').value) {
      keyWord = this.formSearchExpire.get('keyWord').value;
    } else {
      keyWord = null;
    }
    this.service.doSearch(this.dataSearch, this.STATUS_INACTIVE, criteriaGroupId, keyWord).subscribe(res => {
      if (res) {
        this.criteriaLstExpire = res.body;
        this.totalRecordExpire = Number(res.headers.get('X-Total-Count'));
      }
    }, error => {
      this.toastr.error('campaign-resource.error');
    });
  }

  searchExpire() {
    this.pageIndexExpire = 0;
    this.onSearchExpire();
  }

  onPageChangeExpire(event) {
    this.pageIndexExpire = event.pageIndex;
    this.pageSizeExpire = event.pageSize;
    this.onSearchExpire();
  }

  onChangeCriteriaGroup() {
    if (this.formControl.get('criteriaGroupId').value !== '') {
      this.criteriaGroupService.getRemainingScore(this.formControl.get('criteriaGroupId').value, this.criteriaId).subscribe(res => {
        if (res) {
          this.formControl.get('remainingScore').setValue(res.body);
          // this.formControl.get('scores').setValidators([Validators.max(Number(this.formControl.get('remainingScore').value)), Validators.required]);
        }
      });
    } else {
      this.formControl.get('remainingScore').setValue(null);
      // this.formControl.get('scores').setValidators([Validators.required]);
    }
  }

  edit(row) {
    this.isUpdate = true;
    this.criteriaId = row.id;
    this.formControl.updateValueAndValidity();
    this.formControl.patchValue(row);
  }

  cancel() {
    this.isUpdate = false;
    this.criteriaId = null;
    const controls = this.formControl.controls;
    controls.criteriaGroupId.reset('');
    controls.criteriaGroupId.setErrors(null);
    controls.name.reset();
    controls.name.setErrors(null);
    controls.scores.reset();
    controls.scores.setErrors(null);
  }

  save() {
    this.formControl.markAllAsTouched();
    if (this.formControl.invalid) {
      return;
    } else {
      if (this.formControl.get('criteriaGroupId').value === null || this.formControl.get('criteriaGroupId').value === '') {
        this.formControl.get('criteriaGroupId').setErrors(Validators.required);
        this.formControl.get('criteriaGroupId').updateValueAndValidity();
      }
      if (this.formControl.get('name').value === null) {
        this.formControl.get('name').setErrors(Validators.required);
        this.formControl.get('name').updateValueAndValidity();
      }
      if (this.formControl.get('scores').value === null) {
        this.formControl.get('scores').setErrors(Validators.required);
        this.formControl.get('scores').updateValueAndValidity();
      }
      if (this.formControl.invalid) {
        return;
      }
    }
    if (Number(this.formControl.get('scores').value) > Number(this.formControl.get('remainingScore').value)) {
      this.toastr.warning(this.translate.instant('criteria.score.max', {field: Number(this.formControl.get('remainingScore').value)}));
      return;
    }
    const data = this.formControl.value;
    data.id = this.criteriaId;
    this.service.save(data).subscribe(res => {
      if (!this.isUpdate) {
        this.toastr.success(this.translate.instant('criteria.insert.success'));
      } else {
        this.toastr.success(this.translate.instant('criteria.update.success'));
      }
      this.search();
      this.cancel();
      this.criteriaId = null;
    }, error => {
      this.toastr.error(error.error.title);
    });
  }

  changeStatus(row: any, isDelete: any) {
    let title;
    let content;
    if (isDelete) {
      title = this.translate.instant('common.confirm.title.delete');
      content = this.translate.instant('common.MSG0017');
    } else {
      title = this.translate.instant('criteria.change-status.title');
      content = this.translate.instant('criteria.change-status.content', { field: row.name });
    }
    const dialogContent: ConfirmDialogModel = new ConfirmDialogModel(title, content);
    this.dialogMtx.originalOpen(ConfirmDialogComponent, {
      width: '25vw',
      disableClose: true,
      hasBackdrop: true,
      data: dialogContent,
    }).afterClosed().subscribe((next: any) => {
      if (next.data === 'confirm') {
        this.service.changeStatus(row.id, isDelete).subscribe(res => {
          if (res) {
            if (isDelete) {
              this.toastr.success(this.translate.instant('criteria.delete.success'));
            } else {
              this.toastr.success(this.translate.instant('criteria.change-status.success'));
            }
            this.onSearch();
            this.onSearchExpire();
          }
        }, error => {
          if (isDelete) {
            this.toastr.error(this.translate.instant('criteria.delete.fail'));
          } else {
            this.toastr.error(this.translate.instant('criteria.change-status.fail'));
          }
        });
      }
    });
  }

  updateCriteriaDetail(criteriaDetailModel: CriteriaDetailModel){
    const dialogRef = this.dialog.open(CriteriaDetailComponent, {
      data: criteriaDetailModel,
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(res =>{
      this.onSearch();
      this.onSearchExpire();
    });
  }

  deleteTemp(criteriaDetailModel: CriteriaDetailModel){
    const title = this.translate.instant('common.confirm.title.delete');
    const content = this.translate.instant('common.MSG0017');
    const dialogContent: ConfirmDialogModel = new ConfirmDialogModel(title, content);
    this.dialogMtx.originalOpen(ConfirmDialogComponent, {
      width: '25vw',
      disableClose: true,
      hasBackdrop: true,
      data: dialogContent
    }).afterClosed().subscribe((next: any) => {
      if (next.data === Constant.DIALOG_CONFIRM) {
        this.deleteCriteriaDetail(criteriaDetailModel);
      }
    });
  }

  deleteCriteriaDetail(criteriaDetailModel: CriteriaDetailModel){
    this.criteriaDetailService.deleteCriteriaDetail(criteriaDetailModel).subscribe(res =>{
      if (res.body.message === 'ok'){
        this.toastr.success(this.translate.instant('criteria_detail.msg_delete_success'));
        this.onSearch();
        this.searchExpire();
      } else {
        this.toastr.error(this.translate.instant('criteria_detail.msg_delete_error'));
      }
    });
  }

  addCriteriaDetail(){
    const dialogRef = this.dialog.open(CriteriaDetailComponent, {
      data: null,
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(res => {
      this.search();
      this.searchExpire();
    });
  }
}
