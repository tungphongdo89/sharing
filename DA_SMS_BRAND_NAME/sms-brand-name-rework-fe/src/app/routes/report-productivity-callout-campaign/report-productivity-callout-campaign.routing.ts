import { RouterModule, Routes } from '@angular/router';
import { ReportProductivityCalloutCampaignComponent } from '@app/routes/report-productivity-callout-campaign/report-productivity-callout-campaign/report-productivity-callout-campaign.component';
import { NgModule } from '@angular/core';

const routes: Routes = [
  {
    path: '',
    component: ReportProductivityCalloutCampaignComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportProductivityCalloutCampaignRouting {}
