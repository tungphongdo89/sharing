import { NgModule } from '@angular/core';
import { ReportProductivityCalloutCampaignComponent } from '@app/routes/report-productivity-callout-campaign/report-productivity-callout-campaign/report-productivity-callout-campaign.component';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@shared';
import { ReportProductivityCalloutCampaignRouting } from '@app/routes/report-productivity-callout-campaign/report-productivity-callout-campaign.routing';

@NgModule({
  declarations: [ReportProductivityCalloutCampaignComponent],
  imports: [
    CommonModule,
    SharedModule,
    ReportProductivityCalloutCampaignRouting
  ]
})
export class ReportProductivityCalloutCampaignModule {}
