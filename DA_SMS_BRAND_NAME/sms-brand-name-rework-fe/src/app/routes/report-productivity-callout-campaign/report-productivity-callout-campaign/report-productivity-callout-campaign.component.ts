import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MtxGridColumn } from '@ng-matero/extensions';
import { CampaignResourceService } from '@core/services/campaign-resource/campaign-resource.service';
import { finalize } from 'rxjs/operators';
import { DatePipe } from '@angular/common';
import { ReportProductivityCalloutCampaignService } from '@core/services/report-productivity-callout-campaign/report-productivity-callout-campaign.service';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';
import * as fileSaver from 'file-saver';

@Component({
  selector: 'app-report-productivity-callout-campaign',
  templateUrl: './report-productivity-callout-campaign.component.html',
  styleUrls: ['./report-productivity-callout-campaign.component.scss'],
})
export class ReportProductivityCalloutCampaignComponent implements OnInit {

  formSearch: FormGroup;
  columns: MtxGridColumn[] = [];
  lstCampaign = [];
  pageIndex: number;
  pageSize: number;
  dataSearch = {
    page: null,
    size: null,
  };
  totalRecord: number;
  lst: any;
  pageSizeList = [10, 50, 100];
  isLoading = false;
  minToDate;
  maxFromDate;

  constructor(
    private fb: FormBuilder,
    private campaignResourceService: CampaignResourceService,
    private service: ReportProductivityCalloutCampaignService,
    private toastr: ToastrService,
    private translate: TranslateService,
  ) {
  }

  ngOnInit(): void {
    this.columns = [
      { i18n: 'report-productivity-callout-campaign.columns.index', width: '50px', field: 'no' },
      { i18n: 'report-productivity-callout-campaign.columns.operator', field: 'operator' },
      { i18n: 'report-productivity-callout-campaign.columns.total-call', field: 'totalCall' },
      { i18n: 'report-productivity-callout-campaign.columns.status-connect', field: 'statusConnect' },
      { i18n: 'report-productivity-callout-campaign.columns.status-call', field: 'statusCall' },
    ];
    this.formSearch = this.fb.group({
      campaign: '',
      operator: null,
      fromDate: null,
      toDate: null,
    });
    this.campaignResourceService.getLstCampaign().subscribe(res => {
      this.lstCampaign = res;
    });
    this.pageIndex = 0;
    this.pageSize = 10;
    this.onSearch();
  }

  onSearch() {
    this.isLoading = true;
    this.dataSearch.page = this.pageIndex;
    this.dataSearch.size = this.pageSize;
    let fromDate;
    let toDate;
    let name;
    let operator;
    if (this.formSearch.get('fromDate').value) {
      fromDate = this.setDateTime(this.formSearch.get('fromDate').value);
    } else {
      fromDate = null;
    }
    if (this.formSearch.get('toDate').value) {
      toDate = this.setDateTime(this.formSearch.get('toDate').value);
    } else {
      toDate = null;
    }
    if (this.formSearch.get('campaign').value !== '') {
      name = this.formSearch.get('campaign').value;
    } else {
      name = -1;
    }
    if (this.formSearch.get('operator').value) {
      operator = this.formSearch.get('operator').value;
    } else {
      operator = null;
    }
    this.service.doSearch(this.dataSearch, name, operator, fromDate, toDate).pipe(finalize(() => {
      this.isLoading = false;
    })).subscribe(res => {
      if (res) {
        this.lst = res.body;
        this.totalRecord = Number(res.headers.get('X-Total-Count'));
      }
    });
  }

  onPageChange(event) {
    this.pageIndex = event.pageIndex;
    this.pageSize = event.pageSize;
    this.onSearch();
  }

  search() {
    if (this.formSearch.invalid) {
      return;
    }
    this.pageIndex = 0;
    this.onSearch();
  }

  setDateTime(dateTime) {
    const pipe = new DatePipe('en-US');
    const date = pipe.transform(dateTime, 'dd/MM/yyyy', 'GMT+7');
    return date;
  }

  export() {
    if (this.formSearch.invalid) {
      return;
    }
    let fromDate;
    let toDate;
    let name;
    let operator;
    if (this.formSearch.get('fromDate').value) {
      fromDate = this.setDateTime(this.formSearch.get('fromDate').value);
    } else {
      fromDate = null;
    }
    if (this.formSearch.get('toDate').value) {
      toDate = this.setDateTime(this.formSearch.get('toDate').value);
    } else {
      toDate = null;
    }
    if (this.formSearch.get('campaign').value !== '') {
      name = this.formSearch.get('campaign').value;
    } else {
      name = -1;
    }
    if (this.formSearch.get('operator').value) {
      operator = this.formSearch.get('operator').value;
    } else {
      operator = null;
    }
    this.service.export(name, operator, fromDate, toDate).subscribe(res => {
      if (res) {
        console.log('res: ', res);
        const fileName = res.headers.get('filename');
        this.saveFile(res.body, fileName);
      }
    });
  }

  private saveFile(data: ArrayBuffer, fileName: string) {
    const blob = new Blob([data], { type: 'application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet; charset=UTF-8' });
    fileSaver.saveAs(blob, fileName);
  }

  onChangeFromDate() {
    this.minToDate = this.formSearch.get('fromDate').value;
  }

  onChangeToDate() {
    this.maxFromDate = this.formSearch.get('toDate').value;
  }

}
