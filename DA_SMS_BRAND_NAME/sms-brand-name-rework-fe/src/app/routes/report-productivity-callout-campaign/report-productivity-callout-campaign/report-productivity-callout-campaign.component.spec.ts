import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportProductivityCalloutCampaignComponent } from './report-productivity-callout-campaign.component';

describe('ReportProductivityCalloutCampaignComponent', () => {
  let component: ReportProductivityCalloutCampaignComponent;
  let fixture: ComponentFixture<ReportProductivityCalloutCampaignComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportProductivityCalloutCampaignComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportProductivityCalloutCampaignComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
