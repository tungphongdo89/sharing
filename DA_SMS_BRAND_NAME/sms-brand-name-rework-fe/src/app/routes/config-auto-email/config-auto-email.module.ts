import {NgModule} from '@angular/core';
import {CommonModule, DatePipe} from '@angular/common';
import {SharedModule} from '@shared';
import {ConfigAutoEmailRoutingModule} from '@app/routes/config-auto-email/config-auto-email-routing.module';
import {ConfigAutoEmailComponent} from '@app/routes/config-auto-email/config-auto-email/config-auto-email.component';
import {UpdateConfigAutoEmailComponent} from './update-config-auto-email/update-config-auto-email.component';
import {NgxMaterialTimepickerModule} from 'ngx-material-timepicker';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';


@NgModule({
  declarations: [ConfigAutoEmailComponent, UpdateConfigAutoEmailComponent],
  imports: [
    CommonModule,
    SharedModule,
    ConfigAutoEmailRoutingModule,
    NgxMaterialTimepickerModule,
    MatFormFieldModule,
    MatInputModule
  ],
  providers: [
    DatePipe
  ]
})
export class ConfigAutoEmailModule {
}
