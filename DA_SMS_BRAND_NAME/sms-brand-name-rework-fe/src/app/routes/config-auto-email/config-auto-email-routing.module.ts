import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {ConfigAutoEmailComponent} from '@app/routes/config-auto-email/config-auto-email/config-auto-email.component';

const routes: Routes = [
  {
    path: '',
    component: ConfigAutoEmailComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ConfigAutoEmailRoutingModule {
}
