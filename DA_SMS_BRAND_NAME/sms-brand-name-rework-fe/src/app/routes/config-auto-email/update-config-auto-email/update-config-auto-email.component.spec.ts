import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateConfigAutoEmailComponent } from './update-config-auto-email.component';

describe('UpdateConfigAutoEmailComponent', () => {
  let component: UpdateConfigAutoEmailComponent;
  let fixture: ComponentFixture<UpdateConfigAutoEmailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateConfigAutoEmailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateConfigAutoEmailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
