import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ToastrService} from 'ngx-toastr';
import {TranslateService} from '@ngx-translate/core';
import {ConfigAutoEmailService} from '@core/services/config-auto-email/config-auto-email.service';
import {Constant} from '@shared/Constant';

@Component({
  selector: 'app-update-config-auto-email',
  templateUrl: './update-config-auto-email.component.html',
  styleUrls: ['./update-config-auto-email.component.scss']
})
export class UpdateConfigAutoEmailComponent implements OnInit {
  form: FormGroup = this.fb.group({
    campaignEmailId: [null, Validators.required],
    name: [null, [Validators.required, Validators.maxLength(250)]],
    timeType: [null, Validators.required],
    dateOfWeekArray: [null],
    timeSend: [null, Validators.required],
    dateSend: [null],
    status: [null]
  });
  lstCampaignEmailMarketing = [];
  lstTimeType = [
    {code: Constant.TIME_TYPE_DAILY, value: this.translate.instant('Daily')},
    {code: Constant.TIME_TYPE_WEEKLY, value: this.translate.instant('Weekly')},
    {code: Constant.TIME_TYPE_ONCE, value: this.translate.instant('Once')}];
  lstDateOfWeek = [
    {code: 'MONDAY', value: this.translate.instant('Monday')},
    {code: 'TUESDAY', value: this.translate.instant('Tuesday')},
    {code: 'WEDNESDAY', value: this.translate.instant('Wednesday')},
    {code: 'THURSDAY', value: this.translate.instant('Thursday')},
    {code: 'FRIDAY', value: this.translate.instant('Friday')},
    {code: 'SATURDAY', value: this.translate.instant('Saturday')},
    {code: 'SUNDAY', value: this.translate.instant('Sunday')},
  ];
  today = new Date();

  constructor(public dialogRef: MatDialogRef<UpdateConfigAutoEmailComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private fb: FormBuilder,
              private toast: ToastrService,
              private translate: TranslateService,
              private configAutoEmailService: ConfigAutoEmailService) {
  }

  ngOnInit(): void {
    this.today = this.data?.dateSend;
    this.configAutoEmailService.findAllCampaignEmailMarketing().subscribe(res => {
      this.lstCampaignEmailMarketing = res;
      this.form.patchValue(this.data);
      this.form.patchValue({status: (this.data.status === 'true')});
    });
  }

  changeTimeType() {
    this.form.get('dateSend').setValue(null);
    this.form.get('dateOfWeekArray').reset();
  }

  save() {
    if (this.form.invalid) {
      return;
    }
    switch (this.form.get('timeType').value) {
      case Constant.TIME_TYPE_WEEKLY: {
        console.log(this.form.get('dateOfWeek')?.value);
        if (!this.form.get('dateOfWeekArray')?.value || this.form.get('dateOfWeekArray')?.value === []) {
          this.toast.warning('', this.translate.instant('config_auto_email.enter_day_of_week'));
          return;
        }
        break;
      }
      case Constant.TIME_TYPE_ONCE: {
        if (!this.form.get('dateSend')?.value || this.form.get('dateSend')?.value === ''){
          this.toast.warning('', this.translate.instant('config_auto_email.enter_sending_date'));
          return;
        }
        break;
      }
    }
    const data = this.form.value;
    data.dateOfWeek = data.dateOfWeekArray?.join();
    this.configAutoEmailService.update(this.form.value, this.data.id).subscribe(res => {
      this.toast.success('', this.translate.instant('update_successful'));
      this.dialogRef.close(true);
    });
  }

  cancel() {
    this.dialogRef.close(false);
  }
}
