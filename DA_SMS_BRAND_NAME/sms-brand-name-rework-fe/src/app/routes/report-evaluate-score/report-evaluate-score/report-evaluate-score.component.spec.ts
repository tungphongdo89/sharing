import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportEvaluateScoreComponent } from './report-evaluate-score.component';

describe('ReportEvaluateScoreComponent', () => {
  let component: ReportEvaluateScoreComponent;
  let fixture: ComponentFixture<ReportEvaluateScoreComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportEvaluateScoreComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportEvaluateScoreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
