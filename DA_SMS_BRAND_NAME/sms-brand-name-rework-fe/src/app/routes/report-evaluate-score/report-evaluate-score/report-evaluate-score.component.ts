import {Component, OnInit} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {ToastrService} from 'ngx-toastr';
import {TranslateService} from '@ngx-translate/core';
import {MtxGridColumn} from '@ng-matero/extensions';
import {EvaluateAssignmentService} from '@core/services/evaluate-assignment/evaluate-assignment.service';
import {CriteriaRatingService} from '@core/services/criteria-rating/criteria-rating.service';
import {EvaluateResultService} from '@core/services/evaluate-result/evaluate-result.service';
import * as fileSaver from 'file-saver';
import {DatePipe} from '@angular/common';
import {Router} from '@angular/router';

@Component({
  selector: 'app-report-evaluate-score',
  templateUrl: './report-evaluate-score.component.html',
  styleUrls: ['./report-evaluate-score.component.scss']
})
export class ReportEvaluateScoreComponent implements OnInit {

  constructor(
    private dialog: MatDialog,
    private toastr: ToastrService,
    private translate: TranslateService,
    private evaluateAssignmentService: EvaluateAssignmentService,
    private criteriaRatingService: CriteriaRatingService,
    private evaluateResultService: EvaluateResultService,
    private router: Router
  ) {
  }

  // Danh sách người giám sát
  evaluaterList: any = [];
  evaluaterId: any;

  // Danh sách tổng đài viên
  evaluatedList: any = [];
  evaluatedId: any;

  // Ngày đánh giá
  evaluateDate: Date = null;

  // Bảng
  isLoading: boolean;
  pageSize: number;
  pageIndex: number;
  pageSizeList = [5, 10, 20, 50];
  columns: MtxGridColumn[] = [];
  totalRecord = 0;
  dataList: any = [];
  isExport = false;

  private static saveFile(data: ArrayBuffer, fileName: string) {
    const blob = new Blob([data], {type: 'application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet; charset=UTF-8'});
    fileSaver.saveAs(blob, fileName);
  }

  ngOnInit(): void {
    this.createTable();
    this.getEvaluaterList();
    this.getEvaluatedList();
    this.pageIndex = 0;
    this.pageSize = 10;
    this.getDataList(this.pageIndex, this.pageSize);
  }

  onSearch(): void {
    this.pageIndex = 0;
    this.getDataList(this.pageIndex, this.pageSize);
  }

  getDataList(pageIndex: any, pageSize: any): void {
    this.isLoading = true;
    const data = {
      evaluaterId: this.evaluaterId ? this.evaluaterId : null,
      evaluatedId: this.evaluatedId ? this.evaluatedId : null,
      startDate: this.evaluateDate ? this.evaluateDate : null,
      endDate: this.evaluateDate ? this.evaluateDate : null
    };
    const pageable = {page: pageIndex, size: pageSize};
    this.evaluateResultService.getAllEvaluateResult(data, pageable).subscribe(res => {
        this.dataList = res.body;
        this.totalRecord = Number(res.headers.get('X-Total-Count'));
        this.isLoading = false;
      }, _ => this.isLoading = false
    );
  }

  getEvaluaterList(): void {
    this.evaluateAssignmentService.getUsersByType(2).subscribe(res => this.evaluaterList = res);
  }

  getEvaluatedList(): void {
    this.evaluateAssignmentService.getUsersByType(1).subscribe(res => this.evaluatedList = res);
  }

  onExportExcel(): void {
    this.isExport = true;
    const data = {
      evaluaterId: this.evaluaterId ? this.evaluaterId : null,
      evaluatedId: this.evaluatedId ? this.evaluatedId : null,
      startDate: this.evaluateDate ? this.evaluateDate : null,
      endDate: this.evaluateDate ? this.evaluateDate : null
    };
    this.evaluateResultService.reportEvaluateScore(data).subscribe(
      res => {
        if (res) {
          this.toastr.success(this.translate.instant('report-evaluate-score.response.success-export'));
          const pipe = new DatePipe('vi-VN');
          const fileName = this.translate.instant('report-evaluate-score.title') + ' - ' + pipe.transform(new Date(), 'ddMMyyyyHHmmss');
          ReportEvaluateScoreComponent.saveFile(res.body, fileName);
        }
        this.isExport = false;
      }, _ => this.isExport = false
    );
  }

  onRedoEvaluateDetail(id: number): void {
    this.router.navigate([`/evaluate-list/evaluate-redo/${id}`]).then();
  }

  createTable(): void {
    this.pageIndex = 0;
    this.pageSize = 10;

    this.columns = [
      {i18n: 'STT', field: 'no', width: '50px'},
      {i18n: 'report-evaluate-score.column.evaluater', field: 'evaluaterName', width: '150px'},
      {i18n: 'report-evaluate-score.column.evaluate-date', field: 'evaluateDate', width: '150px'},
      {i18n: 'report-evaluate-score.column.evaluated', field: 'evaluatedName', width: '150px'},
      {i18n: 'report-evaluate-score.column.content', field: 'content', width: '200px'},
      {i18n: 'report-evaluate-score.column.suggest', field: 'suggest', width: '200px'},
      {i18n: 'report-evaluate-score.column.score', field: 'totalScores', width: '150px'},
      {i18n: 'report-evaluate-score.column.score-new', field: 'totalScoresNew', width: '150px'},
      {i18n: 'report-evaluate-score.column.rating', field: 'criteriaRatingName', width: '150px'},
      {i18n: 'report-evaluate-score.column.error-1', field: 'error1Name', width: '150px'},
      {i18n: 'report-evaluate-score.column.error-2', field: 'error2Name', width: '150px'},
      {
        i18n: 'report-evaluate-score.column.tool',
        field: 'tools',
        width: '50px',
        pinned: 'right'
      }
    ];
  }

  onPageChange(event) {
    this.pageIndex = event.pageIndex;
    this.pageSize = event.pageSize;
    this.getDataList(this.pageIndex, this.pageSize);
  }
}
