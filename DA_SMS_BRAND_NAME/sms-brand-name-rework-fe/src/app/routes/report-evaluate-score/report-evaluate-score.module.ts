import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReportEvaluateScoreRoutingModule } from './report-evaluate-score-routing.module';
import { ReportEvaluateScoreComponent } from './report-evaluate-score/report-evaluate-score.component';
import {SharedModule} from "@shared";


@NgModule({
  declarations: [ReportEvaluateScoreComponent],
  imports: [
    CommonModule,
    ReportEvaluateScoreRoutingModule,
    SharedModule
  ]
})
export class ReportEvaluateScoreModule { }
