import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ReportEvaluateScoreComponent} from '@app/routes/report-evaluate-score/report-evaluate-score/report-evaluate-score.component';


const routes: Routes = [
  {
    path: '',
    component: ReportEvaluateScoreComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportEvaluateScoreRoutingModule { }
