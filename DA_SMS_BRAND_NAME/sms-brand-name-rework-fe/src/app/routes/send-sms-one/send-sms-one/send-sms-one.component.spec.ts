import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SendSmsOneComponent } from './send-sms-one.component';

describe('SendSmsOneComponent', () => {
  let component: SendSmsOneComponent;
  let fixture: ComponentFixture<SendSmsOneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SendSmsOneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SendSmsOneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
