import { Component, OnInit } from '@angular/core';
import {SendEmailOneService} from '@core/services/send-email-one/send-email-one.service';
import {TranslateService} from '@ngx-translate/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ToastrService} from 'ngx-toastr';
import {CampaignEmailResourceService} from '@core/services/campaign-email-resource/campaign-email-resource.service';
import {SendSmsOneService} from '@core/services/send-sms-one/send-sms-one.service';
import {getLocaleFirstDayOfWeek} from '@angular/common';

@Component({
  selector: 'app-send-sms-one',
  templateUrl: './send-sms-one.component.html',
  styleUrls: ['./send-sms-one.component.scss']
})
export class SendSmsOneComponent implements OnInit {


  sendSMSOne: FormGroup = this.fb.group({
    listPhoneNumber: [null, [Validators.required]],
    content: [null, [Validators.required]],
  });
  constructor(
    private sendSmsOneService: SendSmsOneService,
    private translate: TranslateService,
    private fb: FormBuilder,
    private toastr: ToastrService
  ) { }

  ngOnInit(): void {
  }


  sendEmail() {
    this.sendSMSOne.markAllAsTouched();
    if (this.sendSMSOne.invalid) {
      return;
    }
    console.log('form', this.sendSMSOne.getRawValue());
    const data = this.sendSMSOne.getRawValue();
    this.sendSmsOneService.addSMS(data).subscribe(res => {
      if (res.body.message === 'ok'){
        console.log('âccsấccacsa ' , res);
        this.showNotifySuccess();
        this.sendSMSOne.reset();
      } else {
        this.showUnknownErr();
      }
    });
  }


  showNotifySuccess() {
    this.toastr.success(this.translate.instant('send-sms-one.send-success'));
  }

  showUnknownErr() {
    this.toastr.error(this.translate.instant('send-sms-one.send-error'));
  }

}
