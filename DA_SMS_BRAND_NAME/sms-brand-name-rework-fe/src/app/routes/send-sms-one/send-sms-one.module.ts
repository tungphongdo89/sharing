import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SendSmsOneRoutingModule } from './send-sms-one-routing.module';
import { SendSmsOneComponent } from './send-sms-one/send-sms-one.component';
import {SharedModule} from '@shared';


@NgModule({
  declarations: [SendSmsOneComponent],
  imports: [
    CommonModule,
    SharedModule,
    SendSmsOneRoutingModule
  ]
})
export class SendSmsOneModule { }
