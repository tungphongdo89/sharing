import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {SendEmailOneComponent} from '@app/routes/send-email-one/send-email-one/send-email-one.component';
import {SendSmsOneComponent} from '@app/routes/send-sms-one/send-sms-one/send-sms-one.component';


const routes: Routes = [
  {
    path: '',
    component: SendSmsOneComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SendSmsOneRoutingModule { }
