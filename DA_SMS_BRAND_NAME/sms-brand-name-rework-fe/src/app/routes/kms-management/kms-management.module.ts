import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { KmsManagementComponent } from './kms-management/kms-management.component';
import {SharedModule} from '@shared';
import {KmsManagementRouting} from '@app/routes/kms-management/kms-management-routing';
import { CategoryTreeComponent } from './kms-management/category-tree/category-tree.component';
import { CategoryTreeCreateComponent } from './kms-management/category-tree-create/category-tree-create.component';
import { CategoryMoveComponent } from './kms-management/category-move/category-move.component';
import { PostCreateComponent } from './kms-management/post-create/post-create.component';
import { PostSearchComponent } from './kms-management/post-search/post-search.component';
import { PostMoveComponent } from './kms-management/post-move/post-move.component';
import { PostApproveComponent } from './kms-management/post-approve/post-approve.component';



@NgModule({
  declarations: [KmsManagementComponent, CategoryTreeComponent, CategoryTreeCreateComponent, CategoryMoveComponent, PostCreateComponent, PostSearchComponent, PostMoveComponent, PostApproveComponent],
  imports: [
    CommonModule,
    SharedModule,
    KmsManagementRouting
  ]
})
export class KmsManagementModule { }
