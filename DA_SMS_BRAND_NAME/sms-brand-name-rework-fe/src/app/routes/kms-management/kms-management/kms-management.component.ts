import {Component, ElementRef, Input, OnInit, ViewChild} from '@angular/core';
import {CategoryTreeCreateComponent} from '@app/routes/kms-management/kms-management/category-tree-create/category-tree-create.component';
import {formatTreeCommon} from '@shared/utils/formatTree';
import {KmsDocumentService} from '@core/services/kms-management/kms-document.service';
import {MatDialog} from '@angular/material/dialog';
import {NestedTreeControl} from '@angular/cdk/tree';
import {MatTreeNestedDataSource} from '@angular/material/tree';
import {PostCreateComponent} from '@app/routes/kms-management/kms-management/post-create/post-create.component';
import {KmsDocumentPostService} from '@core/services/kms-management/kms-document-post.service';
import {forkJoin} from 'rxjs';
import {PostApproveComponent} from '@app/routes/kms-management/kms-management/post-approve/post-approve.component';

@Component({
  selector: 'app-kms-management',
  templateUrl: './kms-management.component.html',
  styleUrls: ['./kms-management.component.scss']
})
export class KmsManagementComponent implements OnInit {
  @ViewChild('postSearchComponent') postSearchComponent: any;
  dataSource = new MatTreeNestedDataSource<any>();
  mostViewPosts: any = [];
  notApps: any = [];
  newNests: any = [];
  bookmarks: any = [];
  constructor(public dialog: MatDialog, private kmsDocumentService: KmsDocumentService, private kmsDocumentPostService: KmsDocumentPostService) { }

  ngOnInit(): void {
    this.search();
    this.initData();
  }
  search() {
    const queryCustomerTypes = [this.kmsDocumentService.query({}, {}),this.kmsDocumentPostService.query({}, {})];
    forkJoin(queryCustomerTypes).subscribe((res: any) => {
      const treeData = [...res[0]];
      this.dataSource.data = formatTreeCommon(treeData, null, 'name', 'id');
    });
  }
  initData() {
    this.kmsDocumentPostService.getMostView({page: 0, size: 5}).subscribe(res => {
      this.mostViewPosts = res;
    });
    this.kmsDocumentPostService.getNotApp({
      page: 0,
      size: 5
    }).subscribe(res => {
      this.notApps = res.body;
    });
    this.kmsDocumentPostService.getNewest({page: 0, size: 5}, {}).subscribe(res => {
      this.newNests = res;
    });
    this.kmsDocumentPostService.getBookMark({page: 0, size: 5}, {}).subscribe(res => {
      this.bookmarks = res;
    });
  }
  viewApprove() {
    const dialogRef = this.dialog.open(PostApproveComponent, {
      width: '100vw',
      height: '100vh',
      maxHeight: '100vh',
      maxWidth: '100vw',
      disableClose: true,
      hasBackdrop: true,
    });

    dialogRef.afterClosed().subscribe(result => {
        this.postSearchComponent.search();
        this.initData();
    });
  }

  viewPost(post: any) {
    const dialogRef = this.dialog.open(PostCreateComponent, {
      width: '100vw',
      height: '100vh',
      maxHeight: '100vh',
      maxWidth: '100vw',
      disableClose: true,
      hasBackdrop: true,
      data: {
        data: post,
        isView: true
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      this.changeSearch('change');
    });
  }

  changeSearch(data) {
    this.postSearchComponent.search();
    if (this.postSearchComponent.selectedPost) {
      this.postSearchComponent.history(this.postSearchComponent.selectedPost, false);
    }
    this.initData();
  }
}
