import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KmsManagementComponent } from './kms-management.component';

describe('KmsManagementComponent', () => {
  let component: KmsManagementComponent;
  let fixture: ComponentFixture<KmsManagementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KmsManagementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KmsManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
