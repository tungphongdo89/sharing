import {Component, EventEmitter, Inject, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MtxGridColumn} from '@ng-matero/extensions';
import {ApDomainService} from '@core/services/ap-domain/ap-domain.service';
import {DepartmentService} from '@core/services/department/department.service';
import {BussinessTypeService} from '@core/services/bussiness-type/bussiness-type.service';
import {ReportContactCenterService} from '@core/services/report/report-contact-center.service';
import {CommonService} from '@shared/services/common.service';
import {map} from 'rxjs/operators';
import {KmsDocumentPostService} from '@core/services/kms-management/kms-document-post.service';
import {PostCreateComponent} from '@app/routes/kms-management/kms-management/post-create/post-create.component';
import {MatDialog} from '@angular/material/dialog';
import {ConfirmDialogComponent, ConfirmDialogModel} from '@shared/components/confirm-dialog/confirm-dialog.component';
import {TranslateService} from '@ngx-translate/core';
import {ToastrService} from 'ngx-toastr';
import {CategoryMoveComponent} from '@app/routes/kms-management/kms-management/category-move/category-move.component';
import {PostMoveComponent} from '@app/routes/kms-management/kms-management/post-move/post-move.component';

@Component({
  selector: 'app-post-search',
  templateUrl: './post-search.component.html',
  styleUrls: ['./post-search.component.scss']
})
export class PostSearchComponent implements OnInit {
  today = new Date();
  @Output() searchChange: EventEmitter<any> = new EventEmitter<any>();
  chanelTypes: any = [];
  requestTypes: any = [];
  priorities: any = [];
  businessTypes:  any = [];
  ticketRequestStatus: any = [];
  ticketStatus: any = [];
  departments: any = [];
  reportDetailForm: FormGroup = this.fb.group({
  title: ''
  });
  data: any = [];
  columns: MtxGridColumn[] = [
    {i18n: 'STT', field: 'index'},
    {i18n: 'kms-management.post.titleName', field: 'title'},
    {i18n: 'kms-management.post.createDatetime', field: 'createDatetime'},
    {i18n: 'kms-management.post.viewTotal', field: 'viewTotal'},
    {i18n: 'kms-management.post.button', field: 'button'},
  ];
  columnsUser: MtxGridColumn[] = [
    {i18n: 'STT', field: 'index'},
    {i18n: 'userName', field: 'userName'},
    {i18n: 'kms-management.post.fullName', field: 'fullName'},
    {i18n: 'kms-management.post.createDatetime', field: 'createDatetime'},
  ];
  pageSizeList = [10, 50, 100];
  pagesize = 10;
  offset: any = 0;
  isLoading = false;
  total: any = 0;
  offsetUser: any = 0;
  pagesizeUser = 10;
  isLoadingUser = false;
  totalUser: any = 0;
  selectedPost: any;
  dataUser: any = [];
  constructor(
    public dialog: MatDialog,
    private fb: FormBuilder,
    private translateService: TranslateService,
    protected toastr: ToastrService,
    private apDomainService: ApDomainService,
    private kmsDocumentPostService: KmsDocumentPostService,
    private commonService: CommonService
  ) { }

  ngOnInit(): void {
    this.initData();
    this.search(true);
  }
  initData() {
  }

  search(reset?: boolean) {
    if (reset) {
      this.offset = 0;
      this.pagesize = 10;
    }
    const data = {...this.reportDetailForm.value ,size: this.pagesize, page: this.offset};
    this.kmsDocumentPostService.query(data, { size: this.pagesize, page: this.offset}).pipe(map(res => {
      return res;
    })).subscribe(res => {
      this.data = res.body;
      this.total = Number(res.headers.get('X-Total-Count'));
    },
      error => {
        this.toastr.error(error.error.title);
      });
  }
  onPageChange(event) {
    this.offset = event.pageIndex;
    this.pagesize = event.pageSize;
    this.search();
  }
  onPageChangeUser(event) {
    this.offsetUser = event.pageIndex;
    this.pagesizeUser = event.pageSize;
    if (this.selectedPost)
    this.history(this.selectedPost, false);
  }

  delete(row: any) {
    const title = this.translateService.instant('kms-management.post.deletePost');
    const mess = this.translateService.instant('kms-management.post.deletePostConfirm');
    const dialogData = new ConfirmDialogModel(title, mess);
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '400px',
      data: dialogData
    });

    dialogRef.afterClosed().subscribe(dialogResult => {
      if (dialogResult && dialogResult.data === 'confirm') {
        this.kmsDocumentPostService.delete(row.id).subscribe(res => {
            this.toastr.success(this.translateService.instant('kms-management.post.deleteSuccess'));
          this.search(true);
        },
          error => {
          this.toastr.error(error.error.title);
          }
          );
      }
    });
  }

  history(row: any , reset) {
    this.selectedPost = row;
    if (reset) {
      this.offsetUser = 0;
      this.pagesizeUser = 10;
    }
    this.kmsDocumentPostService.queryUser({ size: this.pagesizeUser, page: this.offsetUser}, row.id).pipe(map(res => {
      return res;
    })).subscribe(res => {
        this.dataUser = res.body;
        this.totalUser = Number(res.headers.get('X-Total-Count'));
      },
      error => {
        this.toastr.error(error.error.title);
      });
  }

  edit(row: any) {
    const dialogRef = this.dialog.open(PostCreateComponent, {
      width: '100vw',
      height: '100vh',
      maxHeight: '100vh',
      maxWidth: '100vw',
      disableClose: true,
      hasBackdrop: true,
      data: {
        data: row,
        isView: false
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.search();
        this.searchChange.emit('change');
      }
    });
  }

  block(row: any) {

  }
  viewPost(post: any) {
    const dialogRef = this.dialog.open(PostCreateComponent, {
      width: '100vw',
      height: '100vh',
      maxHeight: '100vh',
      maxWidth: '100vw',
      disableClose: true,
      hasBackdrop: true,
      data: {
        data: post,
        isView: true
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === 'search') {
        this.selectedPost = null;
      }
      this.searchChange.emit('change');
    });
  }
  move(row: any) {
    const dialogRef = this.dialog.open(PostMoveComponent, {
      width: '50vw',
      height: 'auto',
      disableClose: true,
      hasBackdrop: true,
      data: row
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.search();
        this.searchChange.emit('change');
      }
    });
  }

  addNewPost() {
    const dialogRef = this.dialog.open(PostCreateComponent, {
      width: '100vw',
      height: '100vh',
      maxHeight: '100vh',
      maxWidth: '100vw',
      disableClose: true,
      hasBackdrop: true,
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.search();
        this.searchChange.emit('change');
      }
    });
  }
}
