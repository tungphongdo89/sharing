import {FlatTreeControl, NestedTreeControl} from '@angular/cdk/tree';
import {AfterViewInit, Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {MatTreeFlatDataSource, MatTreeFlattener, MatTreeNestedDataSource} from '@angular/material/tree';
import {MatMenuTrigger} from '@angular/material/menu';
import {CategoryTreeCreateComponent} from '@app/routes/kms-management/kms-management/category-tree-create/category-tree-create.component';
import {MatDialog} from '@angular/material/dialog';
import {KmsDocumentService} from '@core/services/kms-management/kms-document.service';
import {formatTreeCommon} from '@shared/utils/formatTree';
import {ConfirmDialogComponent} from '@shared/components/confirm-dialog/confirm-dialog.component';
import {CategoryMoveComponent} from '@app/routes/kms-management/kms-management/category-move/category-move.component';
import {ToastrService} from 'ngx-toastr';
import {TranslateService} from '@ngx-translate/core';

/** Flat node with expandable and level information */
interface ExampleFlatNode {
  expandable: boolean;
  name: string;
  level: number;
}

/**
 * @title Tree with flat nodes
 */
@Component({
  selector: 'app-category-tree',
  templateUrl: './category-tree.component.html',
  styleUrls: ['./category-tree.component.scss'],
})
export class CategoryTreeComponent implements OnInit, AfterViewInit {
  @Input() treeControl = new NestedTreeControl<any>(node => node.children);
  @Input() dataSource = new MatTreeNestedDataSource<any>();
  @Output() searchChange: EventEmitter<any> = new EventEmitter<any>();

  constructor(public dialog: MatDialog,
              private kmsDocumentService: KmsDocumentService,
              protected translateService: TranslateService,
              private toastr: ToastrService) {
    // this.dataSource.data = TREE_DATA;
  }

  hasChild = (_: number, node: any) => !!node.children && node.children.length > 0;

  ngOnInit(): void {
  }
  search() {
    this.searchChange.emit('search');
  }
  openUpdate() {
    const dialogRef = this.dialog.open(CategoryTreeCreateComponent, {
      width: '50vw',
      height: 'auto',
      disableClose: true,
      hasBackdrop: true,
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.search();
      }
    });
  }

  edit(element: any) {
    const dialogRef = this.dialog.open(CategoryTreeCreateComponent, {
      width: '50vw',
      height: 'auto',
      disableClose: true,
      hasBackdrop: true,
      data: element.orgData
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.search();
      }
    });
  }

  move(dataCategory: any) {
    const dialogRef = this.dialog.open(CategoryMoveComponent, {
      width: '50vw',
      height: 'auto',
      disableClose: true,
      hasBackdrop: true,
      data: dataCategory.orgData
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.search();
      }
    });
  }

  delete(dataCategory: any) {
    const confirmDialog = this.dialog.open(ConfirmDialogComponent, {
      disableClose: true,
      hasBackdrop: true,
      width: '400px',
      data: {
        title: this.translateService.instant('kms-management.category.deleteCategory'),
        message: this.translateService.instant('kms-management.category.deleteCategoryConfirm')
      }
    });
    confirmDialog.afterClosed().subscribe((flag: any) => {
      if (flag && flag.data === 'confirm') {
        this.kmsDocumentService.delete(dataCategory.orgData.id).subscribe(res => {
          this.toastr.success(this.translateService.instant('kms-management.category.deleteCategorySuccess'));
          this.search();
        },
          error => {
            this.toastr.error(error.error.title);
          });
      }
    });
  }

  ngAfterViewInit(): void {
    document.getElementById('treeComponent').addEventListener('contextmenu', event => event.preventDefault());
  }
}
