import {Component, Inject, OnInit} from '@angular/core';
import {MatTreeNestedDataSource} from '@angular/material/tree';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {KmsDocumentService} from '@core/services/kms-management/kms-document.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {TranslateService} from '@ngx-translate/core';
import {ToastrService} from 'ngx-toastr';
import {formatTreeCommon} from '@shared/utils/formatTree';
import {KmsDocumentPostService} from '@core/services/kms-management/kms-document-post.service';

@Component({
  selector: 'app-post-move',
  templateUrl: './post-move.component.html',
  styleUrls: ['./post-move.component.scss']
})
export class PostMoveComponent implements OnInit {
  dataSource = new MatTreeNestedDataSource<any>();
  documents: any = [];
  documentsDes: any = [];
  dataDes = new MatTreeNestedDataSource<any>();
  formGroup: FormGroup = this.fb.group({
    desDocId: [null, [Validators.required]],
    srcDocId: [null, [Validators.required]],
  });
  constructor(private fb: FormBuilder,
              private kmsDocumentService: KmsDocumentService,
              private kmsDocumentPostService: KmsDocumentPostService,
              private dialogRef: MatDialogRef<PostMoveComponent>,
              protected translateService: TranslateService,
              protected toastr: ToastrService,
              @Inject(MAT_DIALOG_DATA) public dataDialog?: any,
  ) {
  }

  ngOnInit(): void {
    this.kmsDocumentService.query({}, {}).subscribe(res => {
      this.documents = res;
      this.dataSource.data = formatTreeCommon(res, null, 'name', 'id');
      this.documentsDes = res;
      this.dataDes.data = formatTreeCommon(res, null, 'name', 'id');
    });
    // this.formGroup.controls.srcDocId.valueChanges.subscribe(val => {
    //   this.formGroup.controls.newParentId.reset();
    //   this.documentsDes = [];
    //   this.dataDes.data = [];
    //   if (val) {
    //     this.kmsDocumentService.findNotChild(val).subscribe(res => {
    //       this.documentsDes = res;
    //       this.dataDes.data = formatTreeCommon(res, null, 'name', 'id');
    //     });
    //   }
    // });
    if (this.dataDialog) {
      this.formGroup.patchValue({
        srcDocId: this.dataDialog.documentId
      });
    }
  }

  save() {
    if (this.formGroup.invalid) {
      this.formGroup.markAllAsTouched();
    } else {
      const data = this.formGroup.value;
      this.kmsDocumentPostService.move(data).subscribe(res => {
          this.toastr.success(this.translateService.instant('kms-management.post.moveSuccess'));
          this.dialogRef.close('ok');
        },
        error => {
          this.toastr.error(error.error.title);
        });
    }

  }

  onDismiss() {
    this.dialogRef.close();
  }
}

