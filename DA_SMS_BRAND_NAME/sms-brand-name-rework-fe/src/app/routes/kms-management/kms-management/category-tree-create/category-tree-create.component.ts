import {Component, Inject, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {KmsDocumentService} from '@core/services/kms-management/kms-document.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {TranslateService} from '@ngx-translate/core';
import {ToastrService} from 'ngx-toastr';
import {formatTreeCommon} from '@shared/utils/formatTree';
import {NestedTreeControl} from '@angular/cdk/tree';
import {MatTreeNestedDataSource} from '@angular/material/tree';

@Component({
  selector: 'app-category-tree-create',
  templateUrl: './category-tree-create.component.html',
  styleUrls: ['./category-tree-create.component.scss']
})
export class CategoryTreeCreateComponent implements OnInit {
  treeControl = new NestedTreeControl<any>(node => node.children);
  dataSource = new MatTreeNestedDataSource<any>();
  formGroup: FormGroup = this.fb.group({
    parentId: [null, [Validators.required]],
    name: [null, [Validators.required]],
    id: null,
    createDatetime: null,
    createUser: null,
    status: null,
  });
  documents: any = [];
  constructor(private fb: FormBuilder,
              private kmsDocumentService: KmsDocumentService,
              private dialogRef: MatDialogRef<CategoryTreeCreateComponent>,
              protected translateService: TranslateService,
              protected toastr: ToastrService,
              @Inject(MAT_DIALOG_DATA) public dataDialog?: any,
  ) {
    if (this.dataDialog && this.dataDialog.id) {
      this.formGroup.patchValue(this.dataDialog);
    }
  }

  ngOnInit(): void {
    this.kmsDocumentService.query({}, {}).subscribe(res => {
      this.documents = res;
      this.dataSource.data = formatTreeCommon(res, null, 'name', 'id');
    });
  }

  save() {
    if (this.formGroup.invalid) {
      this.formGroup.markAllAsTouched();
    } else {
      const data = this.formGroup.value;
      if (!data.id) {
        this.kmsDocumentService.save(data).subscribe(res => {
          this.toastr.success(this.translateService.instant('kms-management.category.newSuccess'));
          this.dialogRef.close('ok');
        },
          error => {
            this.toastr.error(error.error.title);
          });
      } else {
        this.kmsDocumentService.update(data).subscribe(res => {
            this.toastr.success(this.translateService.instant('kms-management.category.editSuccess'));
            this.dialogRef.close('ok');
        },
          error => {
            this.toastr.error(error.error.title);
          });
      }
    }
  }

  onDismiss() {
    this.dialogRef.close();
  }
}
