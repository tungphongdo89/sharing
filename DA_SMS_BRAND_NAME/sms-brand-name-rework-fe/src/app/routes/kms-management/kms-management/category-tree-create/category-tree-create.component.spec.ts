import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CategoryTreeCreateComponent } from './category-tree-create.component';

describe('CategoryTreeCreateComponent', () => {
  let component: CategoryTreeCreateComponent;
  let fixture: ComponentFixture<CategoryTreeCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CategoryTreeCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CategoryTreeCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
