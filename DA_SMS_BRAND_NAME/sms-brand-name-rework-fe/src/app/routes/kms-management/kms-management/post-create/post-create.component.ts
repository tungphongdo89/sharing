import {Component, Inject, OnInit} from '@angular/core';
import {NestedTreeControl} from '@angular/cdk/tree';
import {MatTreeNestedDataSource} from '@angular/material/tree';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {KmsDocumentService} from '@core/services/kms-management/kms-document.service';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {TranslateService} from '@ngx-translate/core';
import {ToastrService} from 'ngx-toastr';
import {formatTreeCommon} from '@shared/utils/formatTree';
import {HtmlEditorService, ImageService, LinkService, ToolbarService} from '@syncfusion/ej2-angular-richtexteditor';
import {MtxGridColumn} from '@ng-matero/extensions';
import * as moment from 'moment';
import {LocalStorage} from 'ngx-webstorage';
import {KmsDocumentPostService} from '@core/services/kms-management/kms-document-post.service';
import {COMMA, ENTER} from '@angular/cdk/keycodes';
import {ConfirmDialogComponent, ConfirmDialogModel} from '@shared/components/confirm-dialog/confirm-dialog.component';

@Component({
  selector: 'app-post-create',
  templateUrl: './post-create.component.html',
  styleUrls: ['./post-create.component.scss'],
  providers: [ToolbarService, LinkService, ImageService, HtmlEditorService],

})
export class PostCreateComponent implements OnInit {
  public tools: object = {
    items: ['Undo', 'Redo', '|',
      'Bold', 'Italic', 'Underline', 'StrikeThrough', '|',
      'FontName', 'FontSize', 'FontColor', 'BackgroundColor', '|',
      'SubScript', 'SuperScript', '|',
      'LowerCase', 'UpperCase', '|',
      'Formats', 'Alignments', '|', 'OrderedList', 'UnorderedList', '|',
      'Indent', 'Outdent', '|', 'CreateLink',
      'Image', '|', 'ClearFormat', 'Print', 'SourceCode', '|', 'FullScreen'],
  };
  isView: boolean = false;
  isBookmarks: boolean = false;
  treeControl = new NestedTreeControl<any>(node => node.children);
  dataSource = new MatTreeNestedDataSource<any>();
  fileType = ['jpg', 'jpeg', 'png' , 'bmp', 'tiff','xls','xlsx','doc', 'docx'];
  formGroup: FormGroup = this.fb.group({
     documentPath:  [null],
     id: null,
     documentId: [null, Validators.required],
     name: null,
     title: [null, Validators.required],
     content: [null, Validators.required],
     status: null,
     tags: null,
     viewTotal: null,
    lstDelAttachs: [[]],
    createDatetime: null,
     createUser: null,
     approveDatetime: null,
     approveUser: null,
  });
  documents: any = [];
  arrBuffer: any = [];
  columnsRequestAttachment: MtxGridColumn[] = [];
  columnsRequestAttachmentView: MtxGridColumn[] = [
    { i18n: 'kms-management.post.fileName', field: 'fileName' },
    { i18n: 'kms-management.post.capacity', field: 'capacity'},
    { i18n: 'kms-management.post.createDatetimeFile', field: 'createDatetime'},
    { i18n: 'kms-management.post.createUserFile', field: 'createUserName'},
    { i18n: 'kms-management.post.downloadTotal', field: 'downloadTotal'}
  ];
  isLoading: true;
  pagesize = 5;
  pageSizeList = [5, 10, 50, 100];
  totalAttachmentProcess:number = 0;
  fileSave: any;
  listFile: any = [];
  readonly separatorKeysCodes = [ENTER, COMMA] as const;
  tags: any = [];
  constructor(private fb: FormBuilder,
    private kmsDocumentPostService: KmsDocumentPostService,
    private kmsDocumentService: KmsDocumentService,
    private dialogRef: MatDialogRef<PostCreateComponent>,
    public dialog: MatDialog,
    protected translateService: TranslateService,
    protected toastr: ToastrService,
  @Inject(MAT_DIALOG_DATA) public dataDialog?: any,
) {

  }

  ngOnInit(): void {
    this.initData();
    this.columnsRequestAttachment = [
      { i18n: 'STT', field: 'no' },
      { i18n: 'kms-management.post.fileName', field: 'fileName' },
      { i18n: 'kms-management.post.capacity', field: 'capacity'},
      { i18n: 'kms-management.post.createDatetimeFile', field: 'createDatetime'},
      { i18n: 'kms-management.post.createUserFile', field: 'createUserName'},
      { i18n: 'kms-management.post.downloadTotal', field: 'downloadTotal'},
      {
        i18n: 'tool',
        field: 'options',
        width: '200px',
        pinned: 'right',
        right: '0px',
        type: 'button',
      },
    ];

  }
  initData() {
    this.kmsDocumentService.query({}, {}).subscribe(res => {
      this.documents = res;
      this.dataSource.data = formatTreeCommon(res, null, 'name', 'id');
    });
    if (this.dataDialog) {
      this.isView = this.dataDialog.isView;
      if (this.dataDialog.isView) {
        if (this.dataDialog.data && this.dataDialog.data.id) {
          this.kmsDocumentPostService.getViewPost(this.dataDialog.data.id).subscribe(res => {
            this.isBookmarks = res.bookmarks;
            const data = res.documentPostDTO;
            if (data.tags) {
              data.tags  = data.tags.split(',');
              this.tags = data.tags;
            }
            this.formGroup.patchValue(data);
            this.listFile = res.lstDocAttach;
            const path = this.documents.find(e => e.id === res.documentPostDTO.documentId);
            if (path) {
              this.formGroup.patchValue({
                documentPath: path.pathName
              });
            }
          });
        }
      } else {
        if (this.dataDialog.data && this.dataDialog.data.id) {
          this.kmsDocumentPostService.findOne(this.dataDialog.data.id).subscribe(res => {
            const data = res.documentPostDTO;
            if (data.tags) {
              data.tags  = data.tags.split(',');
              this.tags = data.tags;
            }
            this.formGroup.patchValue(data);
            this.listFile = res.lstDocAttach;
            const path = this.documents.find(e => e.id === res.documentPostDTO.documentId);
            if (path) {
              this.formGroup.patchValue({
                documentPath: path.pathName
              });
            }
          });
        }
      }
    }
  }
  save() {

    console.log(this.formGroup.value.tags);
    if (this.formGroup.invalid) {
      this.formGroup.markAllAsTouched();
    } else {
      const data = this.formGroup.value;
      const formData = new FormData();
      formData.append('title', data.title);
      formData.append('content', data.content);
      if (data.tags)
      formData.append('tags', data.tags.join(','));
      formData.append('documentId', data.documentId);
      for (const item of this.listFile) {
        if (item.isNew) {
          const blob = new Blob([item.file]);
          formData.append(`documentAttachs`, blob, item.fileName);
        }
      }
      for (const item of data.lstDelAttachs) {
        formData.append(`lstDelAttachs`, item);
      }
      if (!data.id) {
        this.kmsDocumentPostService.save(formData).subscribe(res => {
          this.toastr.success(this.translateService.instant('kms-management.post.newPostSuccess'));
          this.dialogRef.close('ok');
        },
          error => {
          this.toastr.error(error.error.title);
          }
          );
      } else {
        formData.append('id', data.id);
        this.kmsDocumentPostService.update(formData).subscribe(res => {
          this.toastr.success(this.translateService.instant('kms-management.post.editPostSuccess'));
          this.dialogRef.close('ok');
        },
          error => {
            this.toastr.error(error.error.title);
          }
          );
      }
    }
  }
  add(event: any, input): void {
    const value = (event.value || '').trim();
    if (value) {
      this.tags.push(value);
      this.formGroup.patchValue({
        tags: this.tags
      });
    }
    input.value = '';
  }
  remove(tag: any): void {
      this.tags.splice(tag, 1);
      this.formGroup.patchValue({
      tags: this.tags
    });
  }
  onDismiss() {
    this.dialogRef.close();
  }
  onChangeFileRequest(event, removableInput): void {
    const len = event.target.files.length;
    const user = localStorage.getItem('user');
    const userName = localStorage.getItem('userName');
    for (let i = 0; i < len; i++) {
      const type = event.target.files[i].name.substring(event.target.files[i].name.lastIndexOf('.') + 1, event.target.files[i].name.length);
      if (this.fileType.includes(type)) {
        if ( event.target.files[0].size > 10485760) {
          this.toastr.error(this.translateService.instant('kms-management.post.errorFileBig'));
        } else {
          this.listFile.push({
            id: null,
            documentPostId: null,
            fileName: event.target.files[i].name,
            fileNameEncrypt: null,
            capacity: event.target.files[i].size,
            downloadTotal: 0,
            isNew: true,
            status: null,
            createDatetime: moment().toISOString(),
            createUser: user,
            createUserName: userName,
            file: event.target.files[i]
          });
        }
      } else {
        this.toastr.error(this.translateService.instant('kms-management.post.errorFileFormat'));
      }

    }
    removableInput.clear();
  }

  onPageChangeRequest($event: any) {
  }

  changeDocument(document: any) {
    this.formGroup.patchValue({
      documentPath: document.orgData.pathName || null
    });
  }

  deleteProcessAttachment(record: any, index) {
    console.log(record);
    if (record.id) {
      if (!this.formGroup.value.lstDelAttachs) {
        this.formGroup.patchValue({
          lstDelAttachs: []
        });
      }
      this.formGroup.patchValue({
        lstDelAttachs: [...this.formGroup.value.lstDelAttachs, record.id]
      });
    }
    this.listFile.splice(index, 1);
    this.toastr.success(this.translateService.instant('kms-management.post.deleteFileSuccess'));
  }

  bookmarkPost(id) {
    if (id) {
      if (!this.isBookmarks) {
        this.kmsDocumentPostService.bookmarkViewPost(id).subscribe(res => {
            this.isBookmarks = !this.isBookmarks;
            this.toastr.success(this.translateService.instant('kms-management.post.bookmarksSuccess'));
          },
          error => {
            this.toastr.error(error.error.title);
          });
      } else {
        this.kmsDocumentPostService.unBookmarkViewPost(id).subscribe(res => {
            this.isBookmarks = !this.isBookmarks;
            this.toastr.success(this.translateService.instant('kms-management.post.unBookmarksSuccess'));
          },
          error => {
            this.toastr.error(error.error.title);
          });
      }
    }
  }
  delete(row: any) {
    const title = this.translateService.instant('kms-management.post.stopPost');
    const mess = this.translateService.instant('kms-management.post.stopPostConfirm');
    const dialogData = new ConfirmDialogModel(title, mess);
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '400px',
      data: dialogData
    });

    dialogRef.afterClosed().subscribe(dialogResult => {
      if (dialogResult && dialogResult.data === 'confirm') {
        this.kmsDocumentPostService.delete(row.id).subscribe(res => {
            this.toastr.success(this.translateService.instant('kms-management.post.stopSuccess'));
            this.dialogRef.close('search');
          },
          error => {
            this.toastr.error(error.error.title);
          }
        );
      }
    });
  }
}

