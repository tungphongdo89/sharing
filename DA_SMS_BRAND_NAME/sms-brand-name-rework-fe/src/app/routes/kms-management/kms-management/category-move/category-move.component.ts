import {Component, Inject, OnInit} from '@angular/core';
import {NestedTreeControl} from '@angular/cdk/tree';
import {MatTreeNestedDataSource} from '@angular/material/tree';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {KmsDocumentService} from '@core/services/kms-management/kms-document.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {TranslateService} from '@ngx-translate/core';
import {ToastrService} from 'ngx-toastr';
import {formatTreeCommon} from '@shared/utils/formatTree';

@Component({
  selector: 'app-category-move',
  templateUrl: './category-move.component.html',
  styleUrls: ['./category-move.component.scss']
})
export class CategoryMoveComponent implements OnInit {
  dataSource = new MatTreeNestedDataSource<any>();
  documents: any = [];
  documentsDes: any = [];
  dataDes = new MatTreeNestedDataSource<any>();
  formGroup: FormGroup = this.fb.group({
    newParentId: [null, [Validators.required]],
    id: [null, [Validators.required]],
  });
  constructor(private fb: FormBuilder,
              private kmsDocumentService: KmsDocumentService,
              private dialogRef: MatDialogRef<CategoryMoveComponent>,
              protected translateService: TranslateService,
              protected toastr: ToastrService,
              @Inject(MAT_DIALOG_DATA) public dataDialog?: any,
  ) {
  }

  ngOnInit(): void {
    this.kmsDocumentService.query({}, {}).subscribe(res => {
      this.documents = res;
      this.dataSource.data = formatTreeCommon(res, null, 'name', 'id');
    });
    this.formGroup.controls.id.valueChanges.subscribe(val => {
      this.formGroup.controls.newParentId.reset();
      this.documentsDes = [];
      this.dataDes.data = [];
      if (val) {
        this.kmsDocumentService.findNotChild(val).subscribe(res => {
          this.documentsDes = res;
          this.dataDes.data = formatTreeCommon(res, null, 'name', 'id');
        });
      }
    });
    if (this.dataDialog) {
      this.formGroup.patchValue({
        id: this.dataDialog.id
      });
    }
  }

  save() {
    if (this.formGroup.invalid) {
      this.formGroup.markAllAsTouched();
    } else {
      const data = this.formGroup.value;
      this.kmsDocumentService.move(data).subscribe(res => {
        this.toastr.success(this.translateService.instant('kms-management.category.moveSuccess'));
        this.dialogRef.close('ok');
      },
        error => {
          this.toastr.error(error.error.title);
        });
    }

  }

  onDismiss() {
    this.dialogRef.close();
  }
}
