import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CategoryMoveComponent } from './category-move.component';

describe('CategoryMoveComponent', () => {
  let component: CategoryMoveComponent;
  let fixture: ComponentFixture<CategoryMoveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CategoryMoveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CategoryMoveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
