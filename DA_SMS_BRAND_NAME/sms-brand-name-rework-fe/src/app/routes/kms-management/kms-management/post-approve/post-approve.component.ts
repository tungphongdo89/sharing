import {Component, Inject, OnInit} from '@angular/core';
import {MatTreeNestedDataSource} from '@angular/material/tree';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {KmsDocumentService} from '@core/services/kms-management/kms-document.service';
import {KmsDocumentPostService} from '@core/services/kms-management/kms-document-post.service';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {TranslateService} from '@ngx-translate/core';
import {ToastrService} from 'ngx-toastr';
import {formatTreeCommon} from '@shared/utils/formatTree';
import {MtxGridColumn} from '@ng-matero/extensions';
import {map} from 'rxjs/operators';
import {CampaignEmailBatchModel} from '@core/models/campaign-email-batch.interface';
import {ConfirmDialogComponent, ConfirmDialogModel} from '@shared/components/confirm-dialog/confirm-dialog.component';

@Component({
  selector: 'app-post-approve',
  templateUrl: './post-approve.component.html',
  styleUrls: ['./post-approve.component.scss']
})
export class PostApproveComponent implements OnInit {
  posts: any = [];
  selectLists: any = [];
  postTable: MtxGridColumn[] = [
    {i18n: 'STT', field: 'no', width: '15px'},
    {i18n: 'kms-management.post.id', field: 'id'},
    {i18n: 'kms-management.post.title', field: 'title'},
    {i18n: 'kms-management.post.documentName', field: 'documentName'},
    {i18n: 'kms-management.post.createUserName', field: 'createUserName'},
  ];
  pageSizeList = [10, 50, 100];
  isLoading: boolean;
  total: number;
  offset = 0;
  pageIndex: any;
  pageSize: any;
  constructor(private fb: FormBuilder,
              private kmsDocumentService: KmsDocumentService,
              private kmsDocumentPostService: KmsDocumentPostService,
              private dialogRef: MatDialogRef<PostApproveComponent>,
              protected translateService: TranslateService,
              public dialog: MatDialog,
              protected toastr: ToastrService,
              @Inject(MAT_DIALOG_DATA) public dataDialog?: any,
  ) {
  }

  ngOnInit(): void {
    this.search(true);
  }

  save() {
    const data = this.selectLists.map(e => e.id);
    this.kmsDocumentPostService.approve({
      lstDocPostId: data,
      approveStatus: 2
    }).subscribe(res => {
        this.toastr.success(this.translateService.instant('kms-management.post.approveSuccess'));
        this.search(true);
      },
      error => {
        this.toastr.error(error.error.title);
      }
    );
  }
  search(reset?: boolean) {
    if (reset) {
      this.offset = 0;
      this.pageSize = 10;
    }
    this.selectLists = [];
    this.kmsDocumentPostService.getNotApp({page: this.offset, size: this.pageSize}).pipe(map(res => {
      return res;
    })).subscribe(res => {
      this.posts = res.body;
      this.total = Number(res.headers.get('X-Total-Count'));
    });
  }
  onDismiss() {
    this.dialogRef.close();
  }
  onRowSelect(event: any[]) {
    this.selectLists = event;
    console.log(this.selectLists);
  }
  onPageChange(event) {
    this.pageIndex = event.pageIndex;
    this.pageSize = event.pageSize;
    this.search();
  }
  delete() {
    const title = this.translateService.instant('kms-management.post.deletePost');
    const mess = this.translateService.instant('kms-management.post.deletePostConfirm');
    const dialogData = new ConfirmDialogModel(title, mess);
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '400px',
      data: dialogData
    });

    dialogRef.afterClosed().subscribe(dialogResult => {
      if (dialogResult && dialogResult.data === 'confirm') {
        const data = this.selectLists.map(e => e.id);
        this.kmsDocumentPostService.approve({
          lstDocPostId: data,
          approveStatus: 0
        }).subscribe(res => {
            this.toastr.success(this.translateService.instant('kms-management.post.deleteSuccess'));
            this.search(true);
          },
          error => {
            this.toastr.error(error.error.title);
          }
        );
      }
    });
  }
}
