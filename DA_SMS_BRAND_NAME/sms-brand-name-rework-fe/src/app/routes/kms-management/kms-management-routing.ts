import {Routes, RouterModule} from '@angular/router';
import {NgModule} from '@angular/core';
import {KmsManagementComponent} from '@app/routes/kms-management/kms-management/kms-management.component';

const routes: Routes = [
  {
    path: '',
    component: KmsManagementComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class KmsManagementRouting {
}
