import {Component, Inject, OnInit} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {MtxGridColumn} from '@ng-matero/extensions';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {ToastrService} from 'ngx-toastr';
import {TranslateService} from '@ngx-translate/core';
import {RoleService} from '@core/services/role/role.service';
import {UserService} from '@core/services/user/user.service';
import {UserRoleService} from '@core/services/user-role/user-role.service';

@Component({
  selector: 'app-roles-groups-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class RolesGroupsUserComponent implements OnInit {

  constructor(
    private dialog: MatDialog,
    private toastr: ToastrService,
    private translate: TranslateService,
    private roleService: RoleService,
    private userService: UserService,
    private userRoleService: UserRoleService,
    @Inject(MAT_DIALOG_DATA) public input: any
  ) {
  }

  // Thêm mới
  formSave: FormGroup;
  isAdding: boolean;
  userList: any = [];

  // Tìm kiếm
  searchName: string;
  isSearching: boolean;

  // Bảng
  isLoading: boolean;
  pageSize: number;
  pageIndex: number;
  pageSizeList = [5, 10, 20, 50];
  columns: MtxGridColumn[] = [];
  totalRecord: number;
  dataList: any = [];

  ngOnInit(): void {
    this.onStarting();
    this.getUserList();
    this.getData();
  }

  onStarting(): void {
    this.formSave = new FormGroup({
      userIdList: new FormControl(null)
    });

    this.searchName = '';
    this.pageIndex = 0;
    this.pageSize = 10;
    this.columns = [
      {i18n: 'STT', field: 'no', width: '50px'},
      {i18n: 'role-users.login', field: 'login'},
      {i18n: 'role-users.full-name', field: 'fullName'},
      {i18n: 'role-users.number', field: 'numberData'},
      {
        i18n: 'role-users.action',
        field: 'tools',
        width: '100px',
        pinned: 'right'
      }
    ];
  }

  getUserList(): void {
    this.userService.getAllUser().subscribe(
      res => {
        this.userList = res;
        console.log(res);
      }
    );
  }

  onSearch(): void {
    this.pageIndex = 0;
    this.getData();
  }

  getData(): void {
    this.isLoading = true;
    this.isSearching = true;
    const data = {login: this.searchName ? this.searchName.trim() : null, roleId: this.input.id};
    const pageable = {size: this.pageSize, page: this.pageIndex};
    this.userRoleService.getUserRole(data, pageable).subscribe(
      (res) => {
        this.dataList = res.body;
        this.totalRecord = Number(res.headers.get('X-Total-Count'));
        this.isLoading = false;
        this.isSearching = false;
      }, () => {
        this.isLoading = false;
        this.isSearching = false;
      }
    );
  }

  onAdd(): void {
    // Something
    if (this.formSave.value.userIdList) {
      this.isAdding = true;
      const data = {
        roleId: this.input.id,
        lstUserId: this.formSave.value.userIdList
      };
      this.userRoleService.createUserRole(data).subscribe(
        res => {
          if (res === true) {
            this.toastr.success(this.translate.instant('role-users.response.add-success'));
            this.pageIndex = 0;
            this.getData();
          }
          this.isAdding = false;
        }, () => this.isAdding = false
      );
    }
  }

  /**
   * Mở dialog xác nhận xóa người dùng khỏi nhóm quyền
   */
  openRolesGroupsUserDeleteDialog(data: any): void {
    const dialogRef = this.dialog.open(RolesGroupsUserDeleteComponent, {
      width: '40vw',
      disableClose: true,
      hasBackdrop: true,
      data
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result === undefined || !result) {
        return;
      }
      if (result.event !== 'cancel') {
        this.onDelete(data);
      }
    });
  }

  onDelete(data: any): void {
    data.roleId = this.input.id;
    data.lstUserRoleId = [data.id];
    this.userRoleService.removeUserRole(data).subscribe(
      res => {
        if (res === true) {
          this.toastr.success(this.translate.instant('role-users.response.remove-success'));
          this.pageIndex = 0;
          this.getData();
        }
      }
    );
  }

  onPageChange(event) {
    this.pageIndex = event.pageIndex;
    this.pageSize = event.pageSize;
    this.getData();
  }

}

/**
 * Conponent xác nhận user
 */
@Component({
  selector: 'app-roles-groups-user-delete',
  template: `
    <mat-card style="margin-bottom: 0">
      <mat-card-header class="padding-left-0">
        <div fxLayout="row wrap">
          <div fxFlex="100" fxFlex.lt-sm="100">
            <mat-icon>delete</mat-icon>
            <span
              class="custom-title"> {{'common.button.confirm'|translate}}</span>
            <mat-icon mat-dialog-close class="icon-close">close</mat-icon>
          </div>
        </div>
      </mat-card-header>
      <mat-card-content>
        <div>{{'common.confirm.delete'|translate}}</div>
      </mat-card-content>
      <mat-dialog-actions align="end">
        <button mat-raised-button mat-dialog-close>{{'common.no'|translate}}</button>
        <button mat-raised-button color="primary" (click)="onConfirmClick()">{{'common.yes'|translate}}</button>
      </mat-dialog-actions>
    </mat-card>
  `
})
export class RolesGroupsUserDeleteComponent {

  constructor(
    private translate: TranslateService,
    private dialogRef: MatDialogRef<RolesGroupsUserDeleteComponent>,
    @Inject(MAT_DIALOG_DATA) public input: any) {
  }

  onConfirmClick(): void {
    this.dialogRef.close(true);
  }
}
