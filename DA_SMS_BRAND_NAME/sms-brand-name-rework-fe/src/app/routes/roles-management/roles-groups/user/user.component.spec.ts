import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RolesGroupsUserComponent } from './user.component';

describe('RolesGroupsUserComponent', () => {
  let component: RolesGroupsUserComponent;
  let fixture: ComponentFixture<RolesGroupsUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RolesGroupsUserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RolesGroupsUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
