import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RolesGroupsEditComponent } from './edit.component';

describe('RolesGroupsEditComponent', () => {
  let component: RolesGroupsEditComponent;
  let fixture: ComponentFixture<RolesGroupsEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RolesGroupsEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RolesGroupsEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
