import {Component, Inject, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'app-roles-groups-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class RolesGroupsEditComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<RolesGroupsEditComponent>,
    @Inject(MAT_DIALOG_DATA) public input: any
  ) {
    this.role = input.data;
    this.roleList = input.roleList;
    this.createForm();
  }

  // Form
  formSave: FormGroup;

  // Input data
  role: any = Object;
  roleList: any = [];

  ngOnInit(): void {
  }

  createForm(): void {
    this.formSave = new FormGroup({
      name: new FormControl(this.role.name, [Validators.required, Validators.maxLength(255)]),
    });
  }

  onReturnData(): void {
    if (this.formSave.valid && this.beforeSave()) {
      this.formSave.value.id = this.role.id;
      this.dialogRef.close(this.formSave.value);
    }
  }

  beforeSave(): boolean {
    if (this.roleList.find(item => item.name === this.formSave.value.name && item.id !== this.role.id)) {
      this.formSave.get('name').setErrors({duplicate: true, error: true});
      return false;
    }
    return true;
  }

}
