import {Component, Inject, OnInit} from '@angular/core';
import {MtxGridColumn} from '@ng-matero/extensions';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {ToastrService} from 'ngx-toastr';
import {TranslateService} from '@ngx-translate/core';
import {RoleService} from '@core/services/role/role.service';
import {RolesGroupsEditComponent} from '@app/routes/roles-management/roles-groups/edit/edit.component';
import {RolesGroupsUserComponent} from '@app/routes/roles-management/roles-groups/user/user.component';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {RolesGroupsConfigComponent} from '@app/routes/roles-management/roles-groups/config/config.component';

@Component({
  selector: 'app-roles-groups',
  templateUrl: './roles-groups.component.html',
  styleUrls: ['./roles-groups.component.scss']
})
export class RolesGroupsComponent implements OnInit {

  constructor(
    private dialog: MatDialog,
    private toastr: ToastrService,
    private translate: TranslateService,
    private roleService: RoleService
  ) {
  }

  // Thêm mới
  formSave: FormGroup;
  isAdding: boolean;

  // Tìm kiếm
  searchName: string;
  isSearching: boolean;

  // Bảng
  isLoading: boolean;
  pageSize: number;
  pageIndex: number;
  pageSizeList = [5, 10, 20, 50];
  columns: MtxGridColumn[] = [];
  totalRecord: number;
  dataList: any = [];

  ngOnInit(): void {
    this.onStarting();
    this.getData();
  }

  onStarting(): void {
    this.formSave = new FormGroup(
      {name: new FormControl(null, [Validators.maxLength(255)])}
    );
    this.searchName = '';
    this.pageIndex = 0;
    this.pageSize = 10;
    this.columns = [
      {i18n: 'STT', field: 'no', width: '50px'},
      {i18n: 'roles-groups.column.name', field: 'name', width: '150px',},
      {i18n: 'roles-groups.column.user-number', field: 'totalUser', width: '150px'},
      {i18n: 'roles-groups.column.creator', field: 'createUserName', width: '150px'},
      {i18n: 'roles-groups.column.created-date', field: 'createDatetime', width: '150px'},
      {
        i18n: 'roles-groups.column.action',
        field: 'tools',
        width: '150px',
        pinned: 'right'
      }
    ];
  }

  onSearch(): void {
    this.pageIndex = 0;
    this.getData();
  }

  getData(): void {
    this.isLoading = true;
    const data = {name: this.searchName ? this.searchName.trim() : null};
    const pageable = {page: this.pageIndex, size: this.pageSize};
    this.roleService.getRole(data, pageable).subscribe(
      res => {
        this.dataList = res.body;
        if (this.dataList && this.dataList.length > 0) {
          for (const item of this.dataList) {
            item.checked = item.status === '1';
          }
        }
        this.totalRecord = Number(res.headers.get('X-Total-Count'));
        this.isLoading = false;
      }, () => this.isLoading = false
    );
  }

  /**
   * Thêm quyền
   */
  onAdd(): void {
    if (this.beforeAdd()) {
      this.isLoading = true;
      this.roleService.createRole(this.formSave.value).subscribe(
        _ => {
          this.toastr.success(this.translate.instant('roles-groups.response.add-success'));
          this.pageIndex = 0;
          this.getData();
          this.isLoading = false;
        }, () => this.isLoading = false
      );
    }
  }

  /**
   * Kiểm tra trùng lặp
   */
  beforeAdd(): boolean {
    if (this.dataList.find(item => item.name === this.formSave.value.name)) {
      this.formSave.get('name').setErrors({duplicate: true, error: true});
      return false;
    }
    return true;
  }


  /**
   * Mở dialog sửa tên quyền
   */
  openRoleEditDialog(data): void {
    const input = {data, roleList: this.dataList};
    const dialogRef = this.dialog.open(RolesGroupsEditComponent, {
      width: '40vw',
      disableClose: true,
      hasBackdrop: true,
      data: input
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result === undefined || !result) {
        return;
      }
      if (result.event !== 'cancel') {
        this.onEdit(result);
      }
    });
  }

  /**
   * Sửa tên quyền
   */
  onEdit(sendData: any): void {
    this.roleService.updateRole(sendData).subscribe(
      res => {
        if (res === true) {
          this.toastr.success(this.translate.instant('roles-groups.response.update-success'));
          this.pageIndex = 0;
          this.getData();
        }
      }
    );
  }

  /**
   * Mở dialog xác nhận kích hoạt/ khóa quyền
   */
  openRoleChangeStatus(data: any): void {
    const dialogRef = this.dialog.open(RoleChangeStatusComponent, {
      width: '40vw',
      disableClose: true,
      hasBackdrop: true,
      data
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result === undefined || !result) {
        this.getData();
        return;
      }
      if (result.event !== 'cancel') {
        this.onEnable(data);
      }
    });
  }

  /**
   * Kích hoạt / Khóa quyền
   */
  onEnable(data: any): void {
    this.roleService.actionRole(data).subscribe(
      res => {
        if (res === true) {
          data.status === '1' ? this.toastr.success(this.translate.instant('roles-groups.response.lock-success'))
            : this.toastr.success(this.translate.instant('roles-groups.response.active-success'));
          this.pageIndex = 0;
          this.getData();
        }
      }
    );
  }

  /**
   * Mở dialog danh sách người sử dụng nhóm quyền
   */
  openUserRole(data: any): void {
    const dialogRef = this.dialog.open(RolesGroupsUserComponent, {
      width: '80vw',
      disableClose: true,
      hasBackdrop: true,
      data
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result === undefined || !result) {
        this.getData();
        return;
      }
      if (result.event !== 'cancel') {
      }
    });
  }

  /**
   * Mở dialog cấu hình nhóm quyền
   */
  openRoleConfigDialog(data: any): void {
    const dialogRef = this.dialog.open(RolesGroupsConfigComponent, {
      disableClose: true,
      hasBackdrop: true,
      data
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result === undefined || !result) {
        this.getData();
        return;
      }
      if (result.event !== 'cancel') {
      }
    });
  }

  onPageChange(event) {
    this.pageIndex = event.pageIndex;
    this.pageSize = event.pageSize;
    this.getData();
  }

}

/**
 * Conponent xác nhận
 */
@Component({
  selector: 'app-roles-groups-delete',
  template: `
    <mat-card style="margin-bottom: 0">
      <mat-card-header class="padding-left-0">
        <div fxLayout="row wrap">
          <div fxFlex="100" fxFlex.lt-sm="100">
            <mat-icon>delete</mat-icon>
            <span
              class="custom-title">{{'common.button.confirm'|translate}}</span>
            <mat-icon mat-dialog-close class="icon-close">close</mat-icon>
          </div>
        </div>
      </mat-card-header>
      <mat-card-content>
        <div *ngIf="input.status === '0'">{{'roles-groups.confirm.active'|translate}}</div>
        <div *ngIf="input.status === '1'">{{'roles-groups.confirm.lock'|translate}}</div>
      </mat-card-content>
      <mat-dialog-actions align="end">
        <button mat-raised-button mat-dialog-close>{{'common.no'|translate}}</button>
        <button mat-raised-button color="primary" (click)="onConfirmClick()">{{'common.yes'|translate}}</button>
      </mat-dialog-actions>
    </mat-card>
  `
})
export class RoleChangeStatusComponent {

  constructor(
    private translate: TranslateService,
    private dialogRef: MatDialogRef<RoleChangeStatusComponent>,
    @Inject(MAT_DIALOG_DATA) public input: any) {
  }

  onConfirmClick(): void {
    this.dialogRef.close(true);
  }
}
