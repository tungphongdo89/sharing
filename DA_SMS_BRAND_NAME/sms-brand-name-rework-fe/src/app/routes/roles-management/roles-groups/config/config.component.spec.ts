import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RolesGroupsConfigComponent } from './config.component';

describe('RolesGroupsConfigComponent', () => {
  let component: RolesGroupsConfigComponent;
  let fixture: ComponentFixture<RolesGroupsConfigComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RolesGroupsConfigComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RolesGroupsConfigComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
