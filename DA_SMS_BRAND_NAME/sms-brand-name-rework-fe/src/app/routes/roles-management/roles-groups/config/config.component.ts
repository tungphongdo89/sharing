import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog} from '@angular/material/dialog';
import {ToastrService} from 'ngx-toastr';
import {TranslateService} from '@ngx-translate/core';
import {RoleService} from '@core/services/role/role.service';
import {UserService} from '@core/services/user/user.service';
import {UserRoleService} from '@core/services/user-role/user-role.service';
import {ConfigMenusService} from '@core/services/config-menus/config-menus.service';
import {ConfigMenuItemsService} from '@core/services/config-menu-items/config-menu-items.service';

@Component({
  selector: 'app-roles-groups-config',
  templateUrl: './config.component.html',
  styleUrls: ['./config.component.scss']
})
export class RolesGroupsConfigComponent implements OnInit {

  // Danh sách phân hệ
  menuId: number;
  menuList: any = [];
  menuItemsList: any = [];
  isLoading: boolean;

  // Select
  allChecked: boolean;
  someChecked: boolean;

  constructor(
    private dialog: MatDialog,
    private toastr: ToastrService,
    private translate: TranslateService,
    private roleService: RoleService,
    private userService: UserService,
    private userRoleService: UserRoleService,
    private configMenuService: ConfigMenusService,
    private configMenuItemsService: ConfigMenuItemsService,
    @Inject(MAT_DIALOG_DATA) public input: any
  ) {
  }

  ngOnInit(): void {
    this.getMenuList();
  }

  getMenuList(): void {
    this.configMenuService.getAllMenu().subscribe(
      res => {
        this.menuList = res;
        this.menuId = res[0].id;
        this.onGetMenuDetail(this.menuId);
      }
    );
  }

  onGetMenuDetail(menuId: any): void {
    this.configMenuService.getFunctionByMenuAndRole(this.input.id, menuId).subscribe(
      res => {
        this.menuItemsList = res;
        for (const item of this.menuItemsList) {
          if (item.roles) {
            item.expanded = true;
          }
        }
        this.allChecked = this.menuItemsList.every(item => item.checked);
      }
    );
  }

  onUpdateRoles(): void {
    const rolesList = [];
    for (const item of this.menuItemsList) {
      if (item.checked) {
        if (item.roles) {
          for (const role of item.roles) {
            if (role.checked) {
              rolesList.push(role);
            }
          }
        }
        item.roles = null;
        rolesList.push(item);
      }
    }
    const data = {
      roleId: this.input.id,
      menuId: this.menuId,
      functionItemDTOS: rolesList
    };
    this.configMenuItemsService.addFunctionMenu(data).subscribe(
      (res) => {
        if (res === true) {
          this.toastr.success(this.translate.instant('roles-groups.response.update-success'));
          this.onGetMenuDetail(this.menuId);
        }
      }
    );
  }

  onAllChecked(checked: boolean): void {
    this.allChecked = checked;
    this.menuItemsList.forEach(item => {
      item.checked = checked;
      if (item.roles) {
        item.roles.forEach(role => {
          role.checked = checked;
        });
      }
    });
  }

  onSomeMenuItemChecked(): boolean {
    return this.menuItemsList.filter(item => item.checked).length > 0 && !this.allChecked;
  }

  onUpdateAllChecked(): void {
    this.allChecked = this.menuItemsList.every(item => item.checked);
  }

  onMenuItemChecked(checked: boolean, menuItem: any): void {
    if (menuItem.roles) {
      menuItem.roles.forEach(role => role.checked = checked);
    }
  }

  onUpdateMenuItemChecked(menuItem: any): void {
    if (menuItem.roles) {
      menuItem.checked = true;
    }
  }
}
