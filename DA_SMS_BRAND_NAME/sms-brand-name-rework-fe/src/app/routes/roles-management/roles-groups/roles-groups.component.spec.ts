import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RolesGroupsComponent } from './roles-groups.component';

describe('RolesGroupsComponent', () => {
  let component: RolesGroupsComponent;
  let fixture: ComponentFixture<RolesGroupsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RolesGroupsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RolesGroupsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
