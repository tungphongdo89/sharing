import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RolesCategoryComponent } from './roles-category.component';

describe('RolesCategoryComponent', () => {
  let component: RolesCategoryComponent;
  let fixture: ComponentFixture<RolesCategoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RolesCategoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RolesCategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
