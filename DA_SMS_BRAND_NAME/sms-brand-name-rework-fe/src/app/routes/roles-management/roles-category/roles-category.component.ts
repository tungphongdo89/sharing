import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {ToastrService} from 'ngx-toastr';
import {TranslateService} from '@ngx-translate/core';
import {RoleService} from '@core/services/role/role.service';
import {UserService} from '@core/services/user/user.service';
import {UserRoleService} from '@core/services/user-role/user-role.service';
import {ConfigMenusService} from '@core/services/config-menus/config-menus.service';
import {ConfigMenuItemsService} from '@core/services/config-menu-items/config-menu-items.service';
import {ActionManagementComponent} from '@app/routes/roles-management/roles-category/action-management/action-management.component';

@Component({
  selector: 'app-roles-category',
  templateUrl: './roles-category.component.html',
  styleUrls: ['./roles-category.component.scss']
})
export class RolesCategoryComponent implements OnInit {

  // Danh sách phân hệ
  menuId: number;
  menuList: any = [];
  menuItemsList: any = [];
  isLoading: boolean;

  // Select
  allChecked: boolean;
  someChecked: boolean;

  constructor(
    private dialog: MatDialog,
    private toastr: ToastrService,
    private translate: TranslateService,
    private roleService: RoleService,
    private userService: UserService,
    private userRoleService: UserRoleService,
    private configMenuService: ConfigMenusService,
    private configMenuItemsService: ConfigMenuItemsService
  ) {
  }

  ngOnInit(): void {
    this.getMenuList();
  }

  getMenuList(): void {
    this.configMenuService.getAllMenu().subscribe(
      res => {
        this.menuList = res;
        this.menuId = res[0].id;
        this.onGetMenuDetail(this.menuId);
      }
    );
  }

  onGetMenuDetail(menuId: any): void {
    this.configMenuService.getFunctionByMenu(menuId).subscribe(
      res => {
        this.menuItemsList = res;
        for (const item of this.menuItemsList) {
          if (item.roles) {
            item.expanded = true;
          }
        }
        this.allChecked = this.menuItemsList.every(item => item.checked);
      }
    );
  }

  /**
   * Mở dialog xác nhận vô hiệu hóa thao tác
   */
  openActionManagementDialog(data: any, menuItem: any): void {
    data.menuItem = menuItem;
    const dialogRef = this.dialog.open(ActionManagementComponent, {
      width: '40vw',
      disableClose: true,
      hasBackdrop: true,
      data
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result === undefined || !result) {
        return;
      }
      if (result.event !== 'cancel') {
        if (!data.id) {
          result.menuId = this.menuId;
          result.parentId = menuItem.id;
          result.menuItemName = result.name;
          result.menuItemCode = result.code;
          result.type = 2;
          this.onAddAction(result);
        } else {
          result.menuItemName = result.name;
          result.menuItemCode = data.code;
          result.id = data.id;
          this.onEditAction(result);
        }
      }
    });
  }

  onAddAction(data: any): void {
    this.configMenuItemsService.createConfigMenuItem(data).subscribe(
      res => {
        if (res === true) {
          this.toastr.success(this.translate.instant('roles-groups.response.add-success'));
          this.onGetMenuDetail(this.menuId);
        }
      }
    );
  }

  onEditAction(data: any): void {
    this.configMenuItemsService.updateConfigMenuItem(data).subscribe(
      res => {
        if (res === true) {
          this.toastr.success(this.translate.instant('roles-groups.response.update-success'));
          this.onGetMenuDetail(this.menuId);
        }
      }
    );
  }

  /**
   * Mở dialog xác nhận vô hiệu hóa thao tác
   */
  openActionRemoveConfirmDialog(data: any): void {
    const dialogRef = this.dialog.open(ActionsRemoveConfirmComponent, {
      width: '40vw',
      disableClose: true,
      hasBackdrop: true,
      data
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result === undefined || !result) {
        return;
      }
      if (result.event !== 'cancel') {
        this.onRemove(data);
      }
    });
  }

  /**
   * Vô hiệu hóa thao tác
   */
  onRemove(data: any): void {
    this.configMenuItemsService.actionConfigMenuItem(data).subscribe(
      res => {
        if (res === true) {
          this.toastr.success(this.translate.instant('roles-groups.response.inactive-success'));
          this.onGetMenuDetail(this.menuId);
        }
      }
    );
  }
}

/**
 * Conponent xác nhận
 */
@Component({
  selector: 'app-actions-remove-confirm',
  template: `
    <mat-card style="margin-bottom: 0">
      <mat-card-header class="padding-left-0">
        <div fxLayout="row wrap">
          <div fxFlex="100" fxFlex.lt-sm="100">
            <mat-icon>delete</mat-icon>
            <span
              class="custom-title">{{'common.button.confirm'|translate}}</span>
            <mat-icon mat-dialog-close class="icon-close">close</mat-icon>
          </div>
        </div>
      </mat-card-header>
      <mat-card-content>
        <div>{{'roles-groups.confirm.inactive-action'|translate}}</div>
      </mat-card-content>
      <mat-dialog-actions align="end">
        <button mat-raised-button mat-dialog-close>{{'common.no'|translate}}</button>
        <button mat-raised-button color="primary" (click)="onConfirmClick()">{{'common.yes'|translate}}</button>
      </mat-dialog-actions>
    </mat-card>
  `
})
export class ActionsRemoveConfirmComponent {

  constructor(
    private translate: TranslateService,
    private dialogRef: MatDialogRef<ActionsRemoveConfirmComponent>,
    @Inject(MAT_DIALOG_DATA) public input: any) {
  }

  onConfirmClick(): void {
    this.dialogRef.close(true);
  }
}
