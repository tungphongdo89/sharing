import {Component, Inject, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {TranslateService} from '@ngx-translate/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'app-action-management',
  templateUrl: './action-management.component.html',
  styleUrls: ['./action-management.component.scss']
})
export class ActionManagementComponent implements OnInit {

  // Form
  formSave: FormGroup;

  constructor(
    private translate: TranslateService,
    private dialogRef: MatDialogRef<ActionManagementComponent>,
    @Inject(MAT_DIALOG_DATA) public input: any
  ) {
  }

  ngOnInit(): void {
    this.createForm();
    if (this.input.id) {
      this.setValue();
    }
  }

  createForm(): void {
    this.formSave = new FormGroup({
      code: new FormControl('', [Validators.required, Validators.maxLength(255)]),
      name: new FormControl('', [Validators.required, Validators.maxLength(255)]),
    });
  }

  setValue(): void {
    this.formSave.patchValue(
      {
        code: this.input.code,
        name: this.input.name
      }
    );
    this.formSave.get('code').disable();
  }

  onReturnData(): void {
    this.dialogRef.close(this.formSave.value);
  }

}
