import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {ToastrService} from 'ngx-toastr';
import {TranslateService} from '@ngx-translate/core';
import {RoleService} from '@core/services/role/role.service';
import {UserService} from '@core/services/user/user.service';
import {UserRoleService} from '@core/services/user-role/user-role.service';
import {ConfigMenusService} from '@core/services/config-menus/config-menus.service';
import {ConfigMenuItemsService} from '@core/services/config-menu-items/config-menu-items.service';

@Component({
  selector: 'app-roles-actions-recovery',
  templateUrl: './roles-actions-recovery.component.html',
  styleUrls: ['./roles-actions-recovery.component.scss']
})
export class RolesActionsRecoveryComponent implements OnInit {

  // Danh sách phân hệ
  menuId: number;
  menuList: any = [];
  menuItemsList: any = [];

  constructor(
    private dialog: MatDialog,
    private toastr: ToastrService,
    private translate: TranslateService,
    private roleService: RoleService,
    private userService: UserService,
    private userRoleService: UserRoleService,
    private configMenuService: ConfigMenusService,
    private configMenuItemsService: ConfigMenuItemsService
  ) {
  }

  ngOnInit(): void {
    this.getMenuList();
  }

  getMenuList(): void {
    this.configMenuService.getAllMenu().subscribe(
      res => {
        this.menuList = res;
        this.menuId = res[0].id;
        this.onGetMenuDetail(this.menuId);
      }
    );
  }

  onGetMenuDetail(menuId: any): void {
    this.configMenuService.getFunctionInActiveByMenu(menuId).subscribe(
      res => {
        this.menuItemsList = res;
        for (const item of this.menuItemsList) {
          if (item.roles) {
            item.expanded = true;
          }
        }
      }
    );
  }

  /**
   * Mở dialog xác nhận kích hoạt thao tác
   */
  openActionRecoveryConfirmDialog(data: any): void {
    const dialogRef = this.dialog.open(ActionsRecoveryConfirmComponent, {
      width: '40vw',
      disableClose: true,
      hasBackdrop: true,
      data
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result === undefined || !result) {
        return;
      }
      if (result.event !== 'cancel') {
        this.onRecovery(data);
      }
    });
  }

  /**
   * Kích hoạt thao tác
   */
  onRecovery(data: any): void {
    this.configMenuItemsService.actionConfigMenuItem(data).subscribe(
      res => {
        if (res === true) {
          this.toastr.success(this.translate.instant('roles-groups.response.active-action-success'));
          this.onGetMenuDetail(this.menuId);
        }
      }
    );
  }

}

/**
 * Conponent xác nhận
 */
@Component({
  selector: 'app-roles-actions-recovery-confirm',
  template: `
    <mat-card style="margin-bottom: 0">
      <mat-card-header class="padding-left-0">
        <div fxLayout="row wrap">
          <div fxFlex="100" fxFlex.lt-sm="100">
            <mat-icon>delete</mat-icon>
            <span
              class="custom-title">{{'common.button.confirm'|translate}}</span>
            <mat-icon mat-dialog-close class="icon-close">close</mat-icon>
          </div>
        </div>
      </mat-card-header>
      <mat-card-content>
        <div>{{'roles-groups.confirm.active-action'|translate}}</div>
      </mat-card-content>
      <mat-dialog-actions align="end">
        <button mat-raised-button mat-dialog-close>{{'common.no'|translate}}</button>
        <button mat-raised-button color="primary" (click)="onConfirmClick()">{{'common.yes'|translate}}</button>
      </mat-dialog-actions>
    </mat-card>
  `
})
export class ActionsRecoveryConfirmComponent {

  constructor(
    private translate: TranslateService,
    private dialogRef: MatDialogRef<ActionsRecoveryConfirmComponent>,
    @Inject(MAT_DIALOG_DATA) public input: any) {
  }

  onConfirmClick(): void {
    this.dialogRef.close(true);
  }
}
