import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RolesActionsRecoveryComponent } from './roles-actions-recovery.component';

describe('RolesActionsRecoveryComponent', () => {
  let component: RolesActionsRecoveryComponent;
  let fixture: ComponentFixture<RolesActionsRecoveryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RolesActionsRecoveryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RolesActionsRecoveryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
