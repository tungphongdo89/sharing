import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {RolesManagementRoutingModule} from './roles-management-routing.module';
import {RolesManagementComponent} from './roles-management/roles-management.component';
import {SharedModule} from '@shared';
import {ActionsRemoveConfirmComponent, RolesCategoryComponent} from './roles-category/roles-category.component';
import {
  RolesActionsRecoveryComponent,
  ActionsRecoveryConfirmComponent
} from './roles-actions-recovery/roles-actions-recovery.component';
import {RoleChangeStatusComponent, RolesGroupsComponent} from './roles-groups/roles-groups.component';
import {RolesGroupsEditComponent} from './roles-groups/edit/edit.component';
import {RolesGroupsUserComponent, RolesGroupsUserDeleteComponent} from './roles-groups/user/user.component';
import { RolesGroupsConfigComponent } from './roles-groups/config/config.component';
import { ActionManagementComponent } from './roles-category/action-management/action-management.component';


@NgModule({
  declarations: [
    RolesManagementComponent,
    RolesCategoryComponent,
    RolesActionsRecoveryComponent,
    RolesGroupsComponent,
    RolesGroupsEditComponent,
    RoleChangeStatusComponent,
    RolesGroupsUserComponent,
    RolesGroupsConfigComponent,
    RolesGroupsUserDeleteComponent,
    ActionsRecoveryConfirmComponent,
    ActionManagementComponent,
    ActionsRemoveConfirmComponent
  ],
  imports: [
    CommonModule,
    RolesManagementRoutingModule,
    SharedModule
  ]
})
export class RolesManagementModule {
}
