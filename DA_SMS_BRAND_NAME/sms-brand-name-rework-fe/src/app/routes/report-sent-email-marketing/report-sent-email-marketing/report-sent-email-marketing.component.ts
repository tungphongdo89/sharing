import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MtxDialog, MtxGridColumn } from '@ng-matero/extensions';
import { TranslateService } from '@ngx-translate/core';
import { CampaignEmailMarketingService } from '@core/services/campaign-email-marketing/campaign-email-marketing.service';
import { finalize } from 'rxjs/operators';
import { DatePipe } from '@angular/common';
import { ReportSentEmailMarketingService } from '@core/services/report-sent-email-marketing/report-sent-email-marketing.service';
import { ReportSentEmailMarketingDialogComponent } from '@app/routes/report-sent-email-marketing/report-sent-email-marketing-dialog/report-sent-email-marketing-dialog.component';

@Component({
  selector: 'app-report-sent-email-marketing',
  templateUrl: './report-sent-email-marketing.component.html',
  styleUrls: ['./report-sent-email-marketing.component.scss'],
})
export class ReportSentEmailMarketingComponent implements OnInit {
  formSearch: FormGroup;
  columns: MtxGridColumn[] = [];
  lstCampaign = [];
  statusLst = [
    {
      label: this.translate.instant('report-sent-email-marketing.error'),
      value: 0,
    },
    {
      label: this.translate.instant('report-sent-email-marketing.success'),
      value: 1,
    },
  ];
  minToDate;
  maxFromDate;
  pageIndex: any;
  pageSize: any;
  dataSearch = {
    page: null,
    size: null,
  };
  totalRecord: any;
  lst: any;
  pageSizeList = [10, 50, 100];
  isLoading = false;

  constructor(
    private fb: FormBuilder,
    private campaignEmailMarketingService: CampaignEmailMarketingService,
    private translate: TranslateService,
    private service: ReportSentEmailMarketingService,
    private dialog: MtxDialog,
  ) {
  }

  ngOnInit(): void {
    this.columns = [
      { i18n: 'report-sent-email-marketing.columns.index', width: '50px', field: 'no' },
      { i18n: 'report-sent-email-marketing.columns.campaign-name', field: 'campaignName' },
      { i18n: 'report-sent-email-marketing.columns.title', field: 'title' },
      { i18n: 'report-sent-email-marketing.columns.email', width: '150px', field: 'email' },
      { i18n: 'report-sent-email-marketing.columns.sent-user', field: 'sentUser' },
      { i18n: 'report-sent-email-marketing.columns.sent-date', width: '150px', field: 'sentDate' },
      { i18n: 'report-sent-email-marketing.columns.status', width: '150px', field: 'status' },
      { i18n: 'report-sent-email-marketing.columns.content', width: '200px', field: 'content' },
    ];
    this.formSearch = this.fb.group({
      campaign: '',
      fromDate: null,
      toDate: null,
      email: null,
      status: '',
    });
    this.campaignEmailMarketingService.getAllSearch().subscribe(res => {
      this.lstCampaign = res.body;
    });
    this.pageIndex = 0;
    this.pageSize = 10;
    this.onSearch();
  }

  onSearch() {
    this.isLoading = true;
    this.dataSearch.page = this.pageIndex;
    this.dataSearch.size = this.pageSize;
    let fromDate;
    let toDate;
    let name;
    let email;
    let status;
    if (this.formSearch.get('fromDate').value) {
      fromDate = this.setDateTime(this.formSearch.get('fromDate').value);
    } else {
      fromDate = null;
    }
    if (this.formSearch.get('toDate').value) {
      toDate = this.setDateTime(this.formSearch.get('toDate').value);
    } else {
      toDate = null;
    }
    if (this.formSearch.get('campaign').value !== '') {
      name = this.formSearch.get('campaign').value;
    } else {
      name = -1;
    }
    if (this.formSearch.get('email').value !== '') {
      email = this.formSearch.get('email').value;
    } else {
      email = null;
    }
    if (this.formSearch.get('status').value !== '') {
      status = this.formSearch.get('status').value;
    } else {
      status = null;
    }
    this.service.doSearch(this.dataSearch, name, fromDate, toDate, email, status).pipe(finalize(() => {
      this.isLoading = false;
    })).subscribe(res => {
      if (res) {
        this.lst = res.body;
        this.totalRecord = Number(res.headers.get('X-Total-Count'));
      }
    });
  }

  onPageChange(event) {
    this.pageIndex = event.pageIndex;
    this.pageSize = event.pageSize;
    this.onSearch();
  }

  search() {
    if (this.formSearch.invalid) {
      return;
    }
    this.pageIndex = 0;
    this.onSearch();
  }

  setDateTime(dateTime) {
    const pipe = new DatePipe('en-US');
    const date = pipe.transform(dateTime, 'dd/MM/yyyy', 'GMT+7');
    return date;
  }

  onChangeFromDate() {
    this.minToDate = this.formSearch.get('fromDate').value;
  }

  onChangeToDate() {
    this.maxFromDate = this.formSearch.get('toDate').value;
  }

  openDialog(row: any) {
    this.dialog.open({
      disableClose: true,
      hasBackdrop: true,
      width: '50vw',
      data: row,
    }, ReportSentEmailMarketingDialogComponent);
  }

}
