import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportSentEmailMarketingComponent } from './report-sent-email-marketing.component';

describe('ReportSentEmailMarketingComponent', () => {
  let component: ReportSentEmailMarketingComponent;
  let fixture: ComponentFixture<ReportSentEmailMarketingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportSentEmailMarketingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportSentEmailMarketingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
