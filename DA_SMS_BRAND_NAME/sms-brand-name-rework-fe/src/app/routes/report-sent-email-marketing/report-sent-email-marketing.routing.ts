import { RouterModule, Routes } from '@angular/router';
import { ReportSentEmailMarketingComponent } from '@app/routes/report-sent-email-marketing/report-sent-email-marketing/report-sent-email-marketing.component';
import { NgModule } from '@angular/core';

const routes: Routes = [
  {
    path: '',
    component: ReportSentEmailMarketingComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportSentEmailMarketingRouting {}
