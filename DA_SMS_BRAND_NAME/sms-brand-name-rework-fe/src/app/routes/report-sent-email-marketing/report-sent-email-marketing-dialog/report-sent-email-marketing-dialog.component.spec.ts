import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportSentEmailMarketingDialogComponent } from './report-sent-email-marketing-dialog.component';

describe('ReportSentEmailMarketingDialogComponent', () => {
  let component: ReportSentEmailMarketingDialogComponent;
  let fixture: ComponentFixture<ReportSentEmailMarketingDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportSentEmailMarketingDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportSentEmailMarketingDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
