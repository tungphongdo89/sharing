import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { HtmlEditorService, ImageService, LinkService, ToolbarService } from '@syncfusion/ej2-angular-richtexteditor';

@Component({
  selector: 'app-report-sent-email-marketing-dialog',
  templateUrl: './report-sent-email-marketing-dialog.component.html',
  styleUrls: ['./report-sent-email-marketing-dialog.component.scss'],
  providers: [LinkService, ImageService, HtmlEditorService],
})
export class ReportSentEmailMarketingDialogComponent implements OnInit {
  formGroup: FormGroup;

  constructor(
    private fb: FormBuilder,
    private translate: TranslateService,
    @Inject(MAT_DIALOG_DATA) public dataDialog?: any
  ) { }

  ngOnInit(): void {
    this.formGroup = this.fb.group({
      campaignName: this.dataDialog.data.campaignName,
      title: this.dataDialog.data.title,
      email: this.dataDialog.data.email,
      sentUser: this.dataDialog.data.sentUser,
      sentDate: this.dataDialog.data.sentDate,
      status: this.dataDialog.data.status === '1' ? this.translate.instant('report-sent-email-marketing.success') : this.translate.instant('report-sent-email-marketing.error'),
      content: this.dataDialog.data.content,
    });
    console.log(this.dataDialog.data.content);
  }

}
