import { NgModule } from '@angular/core';
import { ReportSentEmailMarketingComponent } from '@app/routes/report-sent-email-marketing/report-sent-email-marketing/report-sent-email-marketing.component';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@shared';
import { ReportSentEmailMarketingRouting } from '@app/routes/report-sent-email-marketing/report-sent-email-marketing.routing';
import { ReportSentEmailMarketingDialogComponent } from '@app/routes/report-sent-email-marketing/report-sent-email-marketing-dialog/report-sent-email-marketing-dialog.component';

@NgModule({
  declarations: [ReportSentEmailMarketingComponent, ReportSentEmailMarketingDialogComponent],
  imports: [
    CommonModule,
    SharedModule,
    ReportSentEmailMarketingRouting,
  ],
})
export class ReportSentEmailMarketingModule {}
