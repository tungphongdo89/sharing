import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReportEvaluateCriteriaRoutingModule } from './report-evaluate-criteria-routing.module';
import { ReportEvaluateCriteriaComponent } from './report-evaluate-criteria/report-evaluate-criteria.component';
import {SharedModule} from '@shared';


@NgModule({
  declarations: [ReportEvaluateCriteriaComponent],
  imports: [
    CommonModule,
    ReportEvaluateCriteriaRoutingModule,
    SharedModule
  ]
})
export class ReportEvaluateCriteriaModule { }
