import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ReportEvaluateCriteriaComponent} from '@app/routes/report-evaluate-criteria/report-evaluate-criteria/report-evaluate-criteria.component';

const routes: Routes = [
  {
    path: '',
    component: ReportEvaluateCriteriaComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportEvaluateCriteriaRoutingModule { }
