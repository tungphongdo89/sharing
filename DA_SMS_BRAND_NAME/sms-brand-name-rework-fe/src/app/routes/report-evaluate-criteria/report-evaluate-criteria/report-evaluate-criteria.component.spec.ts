import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportEvaluateCriteriaComponent } from './report-evaluate-criteria.component';

describe('ReportEvaluateCriteriaComponent', () => {
  let component: ReportEvaluateCriteriaComponent;
  let fixture: ComponentFixture<ReportEvaluateCriteriaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportEvaluateCriteriaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportEvaluateCriteriaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
