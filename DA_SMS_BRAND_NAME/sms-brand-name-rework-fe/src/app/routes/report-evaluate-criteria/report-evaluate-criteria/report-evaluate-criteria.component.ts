import {Component, OnInit} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {ToastrService} from 'ngx-toastr';
import {TranslateService} from '@ngx-translate/core';
import {MtxGridColumn} from '@ng-matero/extensions';
import {EvaluateAssignmentService} from '@core/services/evaluate-assignment/evaluate-assignment.service';
import {CriteriaRatingService} from '@core/services/criteria-rating/criteria-rating.service';
import {EvaluateResultService} from '@core/services/evaluate-result/evaluate-result.service';
import * as fileSaver from 'file-saver';
import {DatePipe} from '@angular/common';
import {CriteriaGroupService} from '@core/services/criteria-group/criteria-group.service';

@Component({
  selector: 'app-report-evaluate-criteria',
  templateUrl: './report-evaluate-criteria.component.html',
  styleUrls: ['./report-evaluate-criteria.component.scss']
})
export class ReportEvaluateCriteriaComponent implements OnInit {

  constructor(
    private dialog: MatDialog,
    private toastr: ToastrService,
    private translate: TranslateService,
    private evaluateAssignmentService: EvaluateAssignmentService,
    private criteriaRatingService: CriteriaRatingService,
    private evaluateResultService: EvaluateResultService,
    private criteriaGroupService: CriteriaGroupService,
  ) {
  }

  // Ngày đánh giá từ
  startDate: Date = null;

  // Ngày đánh giá đến
  endDate: Date = null;

  // Xếp loại
  criteriaGroupList: any = [];
  criteriaGroupId: any;

  // Bảng
  isLoading: boolean;
  pageSize: number;
  pageIndex: number;
  pageSizeList = [5, 10, 20, 50];
  columns: MtxGridColumn[] = [];
  totalRecord = 0;
  dataList: any = [];
  isExport = false;

  private static saveFile(data: ArrayBuffer, fileName: string) {
    const blob = new Blob([data], {type: 'application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet; charset=UTF-8'});
    fileSaver.saveAs(blob, fileName);
  }

  ngOnInit(): void {
    this.createTable();
    this.getCriteriaGroupDetail();
    this.pageIndex = 0;
    this.pageSize = 10;
    this.getDataList(this.pageIndex, this.pageSize);
  }

  onSearch(): void {
    if (this.beforeSearch()) {
      this.pageIndex = 0;
      this.getDataList(this.pageIndex, this.pageSize);
    }
  }

  getDataList(pageIndex: any, pageSize: any): void {
    if (this.criteriaGroupId) {
      this.isLoading = true;
      const data = {
        startDate: this.startDate ? this.startDate : null,
        endDate: this.endDate ? this.endDate : null,
        criteriaGroupId: this.criteriaGroupId ? this.criteriaGroupId : null
      };
      const pageable = {page: pageIndex, size: pageSize};
      this.evaluateResultService.getCriteriaCall(data, pageable).subscribe(res => {
          this.dataList = res.body;
          console.log(res.body);
          this.totalRecord = Number(res.headers.get('X-Total-Count'));
          this.isLoading = false;
        }, _ => this.isLoading = false
      );
    }
  }

  getCriteriaGroupDetail(): void {
    this.criteriaGroupService.findCriteriaGroupDetail().subscribe(res => this.criteriaGroupList = res);
  }

  onExportExcel(): void {
    if (this.criteriaGroupId && this.beforeSearch()) {
      this.isExport = true;
      const data = {
        startDate: this.startDate ? this.startDate : null,
        endDate: this.endDate ? this.endDate : null,
        criteriaGroupId: this.criteriaGroupId ? this.criteriaGroupId : null
      };
      this.evaluateResultService.criteriaCallReport(data).subscribe(
        res => {
          if (res) {
            this.toastr.success(this.translate.instant('evaluate-list.response.success-export'));
            const pipe = new DatePipe('vi-VN');
            const fileName = this.translate.instant('report-evaluate-criteria.title') + ' - ' + pipe.transform(new Date(), 'ddMMyyyyHHmmss');
            ReportEvaluateCriteriaComponent.saveFile(res.body, fileName);
          }
          this.isExport = false;
        }, _ => this.isExport = false
      );
    }
  }

  createTable(): void {
    this.pageIndex = 0;
    this.pageSize = 10;

    this.columns = [
      {i18n: 'STT', field: 'no', width: '50px'},
      {i18n: 'report-evaluate-criteria.column.criteria', field: 'criteriaName', width: '200px'},
      {i18n: 'report-evaluate-criteria.column.numberData', field: 'criteriaPerCall', width: '100px'},
      {i18n: 'report-evaluate-criteria.column.rate', field: 'rate', width: '100px'}
    ];
  }

  onPageChange(event) {
    this.pageIndex = event.pageIndex;
    this.pageSize = event.pageSize;
    this.getDataList(this.pageIndex, this.pageSize);
  }

  beforeSearch(): boolean {
    if (!this.criteriaGroupId) {
      this.toastr.error(this.translate.instant('report-evaluate-criteria.validator.required', {field: this.translate.instant('report-evaluate-criteria.search.group-criteria')}));
      document.getElementById('criteriaGroupId').focus();
      return false;
    }
    if (this.startDate && this.endDate && this.endDate < this.startDate) {
      this.toastr.error(this.translate.instant('report-evaluate-criteria.validator.invalid-date'));
      document.getElementById('endDate').focus();
      return false;
    }
    return true;
  }

}
