import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportRenderDetailOutputCallComponent } from './report-render-detail-output-call.component';

describe('ReportRenderDetailOutputCallComponent', () => {
  let component: ReportRenderDetailOutputCallComponent;
  let fixture: ComponentFixture<ReportRenderDetailOutputCallComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportRenderDetailOutputCallComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportRenderDetailOutputCallComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
