import { Component, OnInit } from '@angular/core';
import {MtxGridColumn} from '@ng-matero/extensions';

@Component({
  selector: 'app-report-render-detail-output-call',
  templateUrl: './report-render-detail-output-call.component.html',
  styleUrls: ['./report-render-detail-output-call.component.scss']
})
export class ReportRenderDetailOutputCallComponent implements OnInit {

  columns: MtxGridColumn[] = [];
  pageSizeList = [10, 50, 100];
  isLoading = false;
  offset: any = 0;
  pagesize = 10;
  total: any = 0;
  campaign: any = [];
  constructor() { }

  ngOnInit(): void {

    this.columns = [
      {i18n: 'report_render_detail_call.column.no', field: 'index', width: '70px',},
      {i18n: 'report_render_detail_call.column.cid', field: 'cid'},
      {i18n: 'report_render_detail_call.aa', field: 'aa'},
      {i18n: 'report_render_detail_call.column.status_connect', field: 'status_connect'},
      {i18n: 'report_render_detail_call.column.status_call', field: 'status_call'},
      {i18n: 'report_render_detail_call.column.date_call', field: 'date_call'},
      {i18n: 'report_render_detail_call.cauhoi', field: 'cauhoi'},
    ];
  }




  onPageChange(event) {
    this.offset = event.pageIndex;
    this.pagesize = event.pageSize;
  }

}
