import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ReportRenderDetailOutputCallComponent} from '@app/routes/report-render-detail-output-call/report-render-detail-output-call/report-render-detail-output-call.component';


const routes: Routes = [
  {
    path:'',
    component: ReportRenderDetailOutputCallComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportRenderDetailOutputCallRoutingModule { }
