import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReportRenderDetailOutputCallRoutingModule } from './report-render-detail-output-call-routing.module';
import { ReportRenderDetailOutputCallComponent } from './report-render-detail-output-call/report-render-detail-output-call.component';
import {SharedModule} from '@shared';


@NgModule({
  declarations: [ReportRenderDetailOutputCallComponent],
  imports: [
    CommonModule,
    SharedModule,
    ReportRenderDetailOutputCallRoutingModule
  ]
})
export class ReportRenderDetailOutputCallModule { }
