import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CampaignSmsMarketingConfigComponent } from './campaign-sms-marketing-config.component';

describe('CampaignSmsMarketingConfigComponent', () => {
  let component: CampaignSmsMarketingConfigComponent;
  let fixture: ComponentFixture<CampaignSmsMarketingConfigComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CampaignSmsMarketingConfigComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CampaignSmsMarketingConfigComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
