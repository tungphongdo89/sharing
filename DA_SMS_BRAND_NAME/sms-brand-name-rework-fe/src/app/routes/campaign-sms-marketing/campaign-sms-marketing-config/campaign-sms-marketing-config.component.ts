import { Component, OnInit } from '@angular/core';
import { MtxDialog, MtxGridColumn } from '@ng-matero/extensions';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';
import { CampaignSmsMarketingService } from '@core/services/campaign-sms-marketing/campaign-sms-marketing.service';
import { finalize } from 'rxjs/operators';
import { DatePipe } from '@angular/common';
import * as fileSaver from 'file-saver';
import { ConfirmDialogComponent, ConfirmDialogModel } from '@shared/components/confirm-dialog/confirm-dialog.component';

@Component({
  selector: 'app-campaign-sms-marketing-config',
  templateUrl: './campaign-sms-marketing-config.component.html',
  styleUrls: ['./campaign-sms-marketing-config.component.scss'],
})
export class CampaignSmsMarketingConfigComponent implements OnInit {
  columns: MtxGridColumn[] = [];
  pageIndex: any;
  pageSize: any;
  dataSearch = {
    page: null,
    size: null,
  };
  formSearch: FormGroup;
  formImport: FormGroup;
  totalRecord: any;
  lst: any;
  lstCampaignSMSName: any;
  lstCampaignSMSNameSearch: any;
  pageSizeList = [10, 50, 100];
  isLoading = false;
  file: any = null;
  distinct = [
    {
      value: 0,
      label: this.translate.instant('campaign-sms-marketing.no-duplicate-filter'),
    },
    {
      value: 1,
      label: this.translate.instant('campaign-sms-marketing.duplicate-filter-campaign'),
    },
    {
      value: 2,
      label: this.translate.instant('campaign-sms-marketing.duplicate-filter-all-campaign'),
    },
  ];
  statusActive;
  statusInactive;

  constructor(
    private service: CampaignSmsMarketingService,
    private fb: FormBuilder,
    private toastr: ToastrService,
    private translate: TranslateService,
    private router: Router,
    private dialog: MtxDialog,
  ) {
  }

  ngOnInit(): void {
    this.statusActive = this.translate.instant('campaign-sms-marketing.active');
    this.statusInactive = this.translate.instant('campaign-sms-marketing.inactive');

    this.getActiveCampaign();
    this.getAllCampaign();

    this.columns = [
      { i18n: 'campaign-sms-marketing.columns.index', width: '50px', field: 'no' },
      { i18n: 'campaign-sms-marketing.columns.campaign-name', field: 'name' },
      { i18n: 'campaign-sms-marketing.columns.title', field: 'title' },
      { i18n: 'campaign-sms-marketing.columns.amount-data', field: 'totalData' },
      { i18n: 'campaign-sms-marketing.columns.status', field: 'status' },
      { i18n: 'campaign-sms-marketing.columns.create-user', field: 'createUserName' },
      { i18n: 'campaign-sms-marketing.columns.create-date', field: 'createDateTimeView' },
      { i18n: 'common.action', field: 'options', width: '150px', pinned: 'right', type: 'button' },
    ];

    this.formSearch = this.fb.group({
      name: '',
      startDate: null,
      endDate: null,
    });

    this.formImport = this.fb.group({
      name: '',
      duplicateFilter: 0,
      file: null,
    });

    this.pageIndex = 0;
    this.pageSize = 10;
    this.onSearch();
  }

  onSearch() {
    this.isLoading = true;
    this.dataSearch.page = this.pageIndex;
    this.dataSearch.size = this.pageSize;
    let startDate;
    let endDate;
    let name;
    if (this.formSearch.get('startDate').value) {
      startDate = this.setDateTime(this.formSearch.get('startDate').value);
    } else {
      startDate = null;
    }
    if (this.formSearch.get('endDate').value) {
      endDate = this.setDateTime(this.formSearch.get('endDate').value);
    } else {
      endDate = null;
    }
    if (this.formSearch.get('name').value !== '') {
      name = this.formSearch.get('name').value;
    } else {
      name = -1;
    }
    this.service.doSearch(this.dataSearch, name, startDate, endDate).pipe(finalize(() => {
      this.isLoading = false;
    })).subscribe(res => {
      if (res) {
        if (res.body.length === 0) {
          this.toastr.error(this.translate.instant('campaign-resource.no-record-found'));
        }
        this.lst = res.body;
        this.totalRecord = Number(res.headers.get('X-Total-Count'));
      }
    });
  }

  search() {
    this.pageIndex = 0;
    this.onSearch();
  }

  getActiveCampaign() {
    this.service.getAll().subscribe(res => {
      if (res) {
        this.lstCampaignSMSName = res.body;
      }
    });
  }

  getAllCampaign() {
    this.service.getAllSearch().subscribe(res => {
      if (res) {
        this.lstCampaignSMSNameSearch = res.body;
      }
    });
  }

  setDateTime(dateTime) {
    const pipe = new DatePipe('en-US');
    const date = pipe.transform(dateTime, 'dd/MM/yyyy', 'GMT+7');
    return date;
  }

  new() {
    this.router.navigate(['/campaign-sms-marketing/new']);
  }

  downloadTemplate() {
    this.service.downloadTemplate().subscribe(res => {
      if (res) {
        const fileName = res.headers.get('filename');
        this.saveFile(res.body, fileName);
      }
    }, error => {
      this.toastr.error(error.error.title);
    });
  }

  private saveFile(data: ArrayBuffer, fileName: string) {
    const blob = new Blob([data], { type: 'application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet; charset=UTF-8' });
    fileSaver.saveAs(blob, fileName);
  }

  doImport() {
    if (this.formImport.get('name').value === '') {
      this.toastr.warning(this.translate.instant('campaign-sms-marketing.campaign-name-required'));
      return;
    }
    if (this.formImport.get('file').value === null) {
      this.toastr.warning(this.translate.instant('campaign-sms-marketing.file-required'));
      return;
    }
    const id = this.formImport.get('name').value;
    const distinct = this.formImport.get('duplicateFilter').value;
    this.service.importFile(this.file._files[0], id, distinct).subscribe(res => {
      if (res.body === -1){
        this.toastr.error(this.translate.instant('campaign-sms-marketing.validate-excel'));
      } else {
        const rs = res.headers.get('filename').split('-');
        this.toastr.success(this.translate.instant('campaign-sms-marketing.import-result.total') + rs[0]
          + '<br/>' + this.translate.instant('campaign-sms-marketing.import-result.success') + rs[1]
          + '<br/>' + this.translate.instant('campaign-sms-marketing.import-result.error') + rs[2]
          + '<br/>' + this.translate.instant('campaign-sms-marketing.import-result.blackList') + rs[3], '', {
          timeOut: 5000,
          enableHtml: true,
        });
        this.search();
      }
    }, error => {
      this.toastr.error(error.error.title);
    });
  }

  doCancel() {
    this.formImport.get('name').setValue('');
    this.formImport.get('duplicateFilter').setValue(0);
  }

  onPageChange(event) {
    this.pageIndex = event.pageIndex;
    this.pageSize = event.pageSize;
    this.onSearch();
  }

  edit(row: any) {
    this.router.navigate(['/campaign-sms-marketing/edit/' + row.id]);
  }

  delete(row: any) {
    const title = this.translate.instant('common.confirm.title.delete');
    const content = this.translate.instant('common.MSG0017');
    const dialogContent: ConfirmDialogModel = new ConfirmDialogModel(title, content);
    this.dialog.originalOpen(ConfirmDialogComponent, {
      width: '25vw',
      disableClose: true,
      hasBackdrop: true,
      data: dialogContent,
    }).afterClosed().subscribe((next: any) => {
      if (next.data === 'confirm') {
        this.service.delete(row.id).subscribe(res => {
          if (res) {
            this.toastr.success(this.translate.instant('campaign-sms-marketing.delete-success'));
            this.onSearch();
            this.getActiveCampaign();
            this.getAllCampaign();
          }
        }, error => {
          this.toastr.success(this.translate.instant('campaign-sms-marketing.delete-fail'));
        });
      }
    });
  }

}
