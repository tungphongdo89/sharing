import { RouterModule, Routes } from '@angular/router';
import { CampaignSmsMarketingConfigComponent } from '@app/routes/campaign-sms-marketing/campaign-sms-marketing-config/campaign-sms-marketing-config.component';
import { NgModule } from '@angular/core';
import { CampaignSmsMarketingDialogComponent } from '@app/routes/campaign-sms-marketing/campaign-sms-marketing-dialog/campaign-sms-marketing-dialog.component';

const routes: Routes = [
  {
    path: '',
    component: CampaignSmsMarketingConfigComponent
  },
  {
    path: 'new',
    component: CampaignSmsMarketingDialogComponent
  },
  {
    path: 'edit/:id',
    component: CampaignSmsMarketingDialogComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CampaignSmsMarketingRouting {}
