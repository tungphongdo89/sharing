import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CampaignSmsMarketingDialogComponent } from './campaign-sms-marketing-dialog.component';

describe('CampaignSmsMarketingDialogComponent', () => {
  let component: CampaignSmsMarketingDialogComponent;
  let fixture: ComponentFixture<CampaignSmsMarketingDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CampaignSmsMarketingDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CampaignSmsMarketingDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
