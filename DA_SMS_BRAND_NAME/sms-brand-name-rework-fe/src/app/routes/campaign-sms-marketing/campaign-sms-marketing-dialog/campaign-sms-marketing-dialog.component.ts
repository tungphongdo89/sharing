import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CampaignSmsMarketingService } from '@core/services/campaign-sms-marketing/campaign-sms-marketing.service';
import {formatDate} from '@angular/common';

@Component({
  selector: 'app-campaign-sms-marketing-dialog',
  templateUrl: './campaign-sms-marketing-dialog.component.html',
  styleUrls: ['./campaign-sms-marketing-dialog.component.scss'],
})
export class CampaignSmsMarketingDialogComponent implements OnInit {
  currentYear = new Date().getFullYear();
  currentMonth = new Date().getMonth();
  currentDay = new Date().getDate();
  minStartDate: Date = new Date(this.currentYear, this.currentMonth, this.currentDay + 1);
  minEndDate: Date = new Date(this.currentYear, this.currentMonth, this.currentDay + 1);
  maxStartDate = null;
  isUpdate: boolean = false;
  id: any = null;
  formControl: FormGroup;
  checkStartDate: boolean = true;
  checkEndDate: boolean = true;
  button: string;
  title: string;
  status = [
    {
      value: '1',
      label: this.translate.instant('campaign-sms-marketing.active')
    },
    {
      value: '0',
      label: this.translate.instant('campaign-sms-marketing.inactive')
    }
  ];

  constructor(
    private fb: FormBuilder,
    private toastr: ToastrService,
    private translate: TranslateService,
    private service: CampaignSmsMarketingService,
    private router: Router,
    private route: ActivatedRoute,
  ) {
  }

  ngOnInit(): void {
    this.formControl = this.fb.group({
      name: [null, [Validators.required]],
      startDate: [null, [Validators.required]],
      endDate: [null, [Validators.required]],
      status: ['1', Validators.required],
      title: [null, [Validators.required]],
      content: [null, [Validators.required]],
      startDateView: null,
      endDateView: null,
    });

    if (this.route.snapshot.routeConfig.path === 'edit/:id') {
      this.isUpdate = true;
    }
    if (this.isUpdate) {
      this.button = this.translate.instant('campaign-sms-marketing.update');
      this.title = this.translate.instant('campaign-sms-marketing.update-title');
      this.id = this.route.snapshot.params.id;
      this.service.getOne(this.id).subscribe(res => {
        this.formControl.patchValue(res.body);
        this.formControl.markAllAsTouched();
        const sd = this.formControl.get('startDateView').value.split('/');
        const startDate = new Date(sd[2], sd[1] - 1, sd[0]);
        this.minStartDate = null;
        // if (startDate <= this.minStartDate && !this.formControl.get('startDate').hasError('required')) {
        //   this.checkStartDate = false;
        // }
        this.minEndDate = startDate;
      });
    } else {
      this.button = this.translate.instant('campaign-sms-marketing.save');
      this.title = this.translate.instant('campaign-sms-marketing.add-title');
    }
  }

  onChangeStartDate() {
    this.checkStartDate = true;
    if (this.formControl.get('startDate').value) {
      this.minEndDate = this.formControl.get('startDate').value;
    } else {
      this.minEndDate = this.minStartDate;
    }
    if (this.isUpdate) {
      const ed = this.formControl.get('endDateView').value.split('/');
      const endDate = new Date(ed[2], ed[1] - 1, ed[0]);
      if (endDate <= this.minEndDate && !this.formControl.get('endDate').hasError('required')) {
        this.checkEndDate = false;
      }
    }
  }

  onChangeEndDate() {
    if (this.formControl.get('endDate').value) {
      this.maxStartDate = this.formControl.get('endDate').value;
    } else {
      this.maxStartDate = null;
    }
  }

  onSave() {
    if (this.formControl.invalid) {
      this.formControl.markAllAsTouched();
      return;
    }
    const data = this.formControl.getRawValue();
    if (this.isUpdate) {
      data.id = this.id;
    }
    this.service.save(data).subscribe(res => {
      if (res) {
        if (!this.isUpdate) {
          this.toastr.success(this.translate.instant('campaign-sms-marketing.create-success'));
        } else {
          this.toastr.success(this.translate.instant('campaign-sms-marketing.update-success'));
        }
        this.close();
      }
    }, error => {
      if (!this.isUpdate) {
        this.toastr.success(this.translate.instant('campaign-sms-marketing.create-fail'));
      } else {
        this.toastr.success(this.translate.instant('campaign-sms-marketing.update-fail'));
      }
    });
  }

  close() {
    this.router.navigate(['/campaign-sms-marketing']);
  }

}
