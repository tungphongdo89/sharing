import { NgModule } from '@angular/core';
import { CampaignSmsMarketingConfigComponent } from '@app/routes/campaign-sms-marketing/campaign-sms-marketing-config/campaign-sms-marketing-config.component';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@shared';
import { CampaignSmsMarketingRouting } from '@app/routes/campaign-sms-marketing/campaign-sms-marketing.routing';
import { CampaignSmsMarketingDialogComponent } from '@app/routes/campaign-sms-marketing/campaign-sms-marketing-dialog/campaign-sms-marketing-dialog.component';

@NgModule({
  declarations: [CampaignSmsMarketingConfigComponent, CampaignSmsMarketingDialogComponent],
  imports: [
    CommonModule,
    SharedModule,
    CampaignSmsMarketingRouting
  ]
})
export class CampaignSmsMarketingModule {}
