import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MatCheckboxChange} from '@angular/material/checkbox';
import {EmailConfigService} from '@core/services/email-config/email-config.service';
import {EmailConfigModel} from '@core/models/email-config.model';
import {TranslateService} from '@ngx-translate/core';
import {ToastrService} from 'ngx-toastr';
import {Router} from '@angular/router';

@Component({
  selector: 'app-email-config',
  templateUrl: './email-config.component.html',
  styleUrls: ['./email-config.component.scss']
})
export class EmailConfigComponent implements OnInit {
  lstEmailConfig: EmailConfigModel[] = [];
  checked: boolean;
  emailConfigForm: FormGroup = this.fb.group({
    id: [null],
    host: [null, [Validators.required,Validators.maxLength(100)]],
    port: [null, [Validators.required,Validators.maxLength(20)]],
    email: [null, [Validators.required, Validators.pattern('[A-Za-z0-9._%-]+@[A-Za-z0-9._%-]+\\.[a-z]{2,3}'),Validators.maxLength(50)]],
    password: [null,[Validators.required]],
    ssl: [null],
  });

  constructor(
    private fb: FormBuilder,
    private emailConfigService: EmailConfigService,
    private translateService: TranslateService,
    private toastr: ToastrService,
    private router: Router
  ) {
  }

  ngOnInit(): void {
    this.emailConfigService.findAll().subscribe(res => {
      this.lstEmailConfig = res;
      console.log('res', res);
      const {host, port, email, ssl} = this.lstEmailConfig[0];
      this.emailConfigForm.get('host').patchValue(host);
      this.emailConfigForm.get('port').patchValue(port);
      this.emailConfigForm.get('email').patchValue(email);
      this.emailConfigForm.get('ssl').patchValue(ssl);
      if (ssl === '1') {
        this.checked = true;
      } else if (ssl === '0') {
        this.checked = false;
      }
    });
  }

  save() {
    this.emailConfigForm.markAllAsTouched();
    this.emailConfigForm.markAsTouched();
    if (this.emailConfigForm.invalid) {
      return;
    }
    const data = this.emailConfigForm.getRawValue();
    this.emailConfigService.updateEmailConfig(this.lstEmailConfig[0].id, data)
      .subscribe(res => {
        this.showNotifySuccess();
        this.reloadComponent();
      }, error => {
        console.warn(error);
        this.showUnknownErr();
      });
  }

  onCheckBoxChange(event: MatCheckboxChange) {
    if (event.checked) {
      this.emailConfigForm.get('ssl').setValue('1');
    } else {
      this.emailConfigForm.get('ssl').setValue('0');
    }
  }

  showNotifySuccess() {
    this.toastr.success(this.translateService.instant('common.notify.save.success'));
  }

  reloadComponent() {
    const currentUrl = this.router.url;
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
    this.router.onSameUrlNavigation = 'reload';
    this.router.navigate([currentUrl]);
  }

  showUnknownErr() {
    this.toastr.error(this.translateService.instant('common.notify.save.error'));
  }

  cancel() {
    this.reloadComponent();
  }
}
