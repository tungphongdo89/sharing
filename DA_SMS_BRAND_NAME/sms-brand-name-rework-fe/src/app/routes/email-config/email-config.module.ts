import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {EmailConfigComponent} from './email-config/email-config.component';
import {SharedModule} from '@shared';
import {EmailConfigRoutingModule} from '@app/routes/email-config/email-config-routing.module';


@NgModule({
  declarations: [EmailConfigComponent],
  imports: [
    CommonModule,
    SharedModule,
    EmailConfigRoutingModule
  ]
})
export class EmailConfigModule {
}
