import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {EmailConfigComponent} from '@app/routes/email-config/email-config/email-config.component';

const routes: Routes = [
  {
    path: '',
    component: EmailConfigComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EmailConfigRoutingModule {
}
