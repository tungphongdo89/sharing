import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportComplaintDetailComponent } from './report-complaint-detail.component';

describe('ReportComplaintDetailComponent', () => {
  let component: ReportComplaintDetailComponent;
  let fixture: ComponentFixture<ReportComplaintDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportComplaintDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportComplaintDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
