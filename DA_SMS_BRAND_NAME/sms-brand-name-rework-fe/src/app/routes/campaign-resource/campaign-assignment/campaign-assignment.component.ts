import {Component, Inject, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {CampaignResultDetailService} from '@app/core/services/campaign-resource-detail/campaign-result-detail.service';
import {GroupUserService} from '@app/core/services/group-user.service';
import {SampleCampaignFormComponent} from '@app/routes/callout-campaign-management/sample-campaign-form/sample-campaign-form.component';
import {TranslateService} from '@ngx-translate/core';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-campaign-assignment',
  templateUrl: './campaign-assignment.component.html',
  styleUrls: ['./campaign-assignment.component.scss']
})
export class CampaignAssignmentComponent implements OnInit {

  formSave: FormGroup;

  data: any = {
    lstGroupDto: null,
    totalData: null,
    campaignResourceId: null
  };

  constructor(private dialogRef: MatDialogRef<SampleCampaignFormComponent>,
              protected translateService: TranslateService,
              protected toastr: ToastrService,
              private groupUserService: GroupUserService,
              private campaignResultDetailService: CampaignResultDetailService,
              @Inject(MAT_DIALOG_DATA) public dataDialog?: any) {
    this.data.campaignResourceId = this.dataDialog.data.campaignResourceId;
    this.campaignResultDetailService.getCampaignResourceDetailsNotGroup(this.data.campaignResourceId).subscribe(res => {
      this.data.totalData = res;
    });
  }

  ngOnInit(): void {
    this.getGroupUser();
    this.formSave = new FormGroup({});
  }

  getGroupUser(): void {
    this.groupUserService.getLstGroupUser().subscribe(
      res => {
        if (res) {
          this.data.lstGroupDto = res;
          this.data.lstGroupDto.forEach(
            item => {
              this.formSave.setControl(item.id.toString(), new FormControl(null, [Validators.min(0), Validators.max(this.data.totalData)]));
            }
          );
        }
      }, err => this.toastr.error(err));
  }

  assignmentAction() {
    if (this.handleData()) {
      this.dialogRef.close(this.data);
    }
  }

  handleData(): boolean {
    let sum = 0;
    this.data.lstGroupDto.forEach(
      item => {
        item.numberData = this.formSave.get(item.id.toString()).value;
        if (item.numberData) {
          sum += item.numberData;
        }
      }
    );
    if (sum > this.data.totalData) {
      this.toastr.error(this.translateService.instant('campaign-resource-assign.form.validate.overload'));
      return false;
    }
    return true;
  }
}
