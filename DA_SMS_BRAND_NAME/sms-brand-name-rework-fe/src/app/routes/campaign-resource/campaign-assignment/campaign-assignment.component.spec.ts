import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CampaignAssignmentComponent } from './campaign-assignment.component';

describe('CampaignAssignmentComponent', () => {
  let component: CampaignAssignmentComponent;
  let fixture: ComponentFixture<CampaignAssignmentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CampaignAssignmentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CampaignAssignmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
