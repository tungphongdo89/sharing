import { RouterModule, Routes } from '@angular/router';
import { CampaignResourceComponent } from '@app/routes/campaign-resource/campaign-resource/campaign-resource.component';
import { NgModule } from '@angular/core';

const routes: Routes = [
  {
    path: '',
    component: CampaignResourceComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CampaignResourceRouting {}
