import { NgModule } from '@angular/core';
import { CampaignResourceComponent } from '@app/routes/campaign-resource/campaign-resource/campaign-resource.component';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@shared';
import { CampaignResourceRouting } from '@app/routes/campaign-resource/campaign-resource.routing';
import { CampaignAssignmentComponent } from './campaign-assignment/campaign-assignment.component';

@NgModule({
  declarations: [CampaignResourceComponent, CampaignAssignmentComponent],
  imports: [
    CommonModule,
    SharedModule,
    CampaignResourceRouting
  ]
})
export class CampaignResourceModule {}
