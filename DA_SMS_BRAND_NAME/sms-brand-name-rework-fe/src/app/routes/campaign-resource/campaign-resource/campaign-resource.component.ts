import { Component, OnInit, ViewChild } from '@angular/core';
import { MtxDialog, MtxGridColumn } from '@ng-matero/extensions';
import { CampaignResourceService } from '@core/services/campaign-resource/campaign-resource.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { DatePipe } from '@angular/common';
import * as fileSaver from 'file-saver';
import { TranslateService } from '@ngx-translate/core';
import { ConfirmDialogComponent, ConfirmDialogModel } from '@shared/components/confirm-dialog/confirm-dialog.component';
import { RecordAudioPlayComponent } from '@app/routes/record-call-results/record-audio-play/record-audio-play.component';
import { CampaignAssignmentComponent } from '../campaign-assignment/campaign-assignment.component';
import { CampaignResultDetailService } from '@app/core/services/campaign-resource-detail/campaign-result-detail.service';
import { MatSlideToggle } from '@angular/material/slide-toggle';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-campaign-resource',
  templateUrl: './campaign-resource.component.html',
  styleUrls: ['./campaign-resource.component.scss'],
})
export class CampaignResourceComponent implements OnInit {

  columns: MtxGridColumn[] = [];
  distinct = [
    {
      value: 0,
      label: this.translate.instant('campaign-resource.no-duplicate-filter'),
    },
    {
      value: 1,
      label: this.translate.instant('campaign-resource.duplicate-filter-campaign'),
    },
    {
      value: 2,
      label: this.translate.instant('campaign-resource.duplicate-filter-all-campaign'),
    },
  ];
  active = this.translate.instant('campaign-resource.active');
  inactive = this.translate.instant('campaign-resource.inactive');
  lstCampaign: any = [];
  formControl: FormGroup;
  formSearch: FormGroup;
  file: any = null;
  pageIndex: any;
  pageSize: any;
  dataSearch = {
    page: null,
    size: null,
  };
  campaignResourceLst: any = [];
  pageSizeList = [10, 50, 100];
  isLoading = false;
  totalRecord: any;
  minToDate;
  maxFromDate;
  disableToggle: boolean = false;

  constructor(
    private service: CampaignResourceService,
    private fb: FormBuilder,
    private toastr: ToastrService,
    private translate: TranslateService,
    private dialog: MtxDialog,
    private campaignResultDetailService : CampaignResultDetailService
  ) {
  }

  ngOnInit(): void {
    this.columns = [
      { i18n: 'campaign-resource.columns.index', width: '50px', field: 'no' },
      { i18n: 'campaign-resource.columns.campaign-name', field: 'campaignName' },
      { i18n: 'campaign-resource.columns.resource-name', field: 'resourceName' },
      { i18n: 'campaign-resource.columns.total', field: 'total' },
      { i18n: 'campaign-resource.columns.called', field: 'called' },
      { i18n: 'campaign-resource.columns.waiting-call', field: 'waitingCall' },
      { i18n: 'campaign-resource.columns.waiting-share', field: 'waitingShare' },
      { i18n: 'campaign-resource.columns.status', field: 'status' },
      { i18n: 'campaign-resource.columns.create-user', field: 'createUserName' },
      { i18n: 'campaign-resource.columns.create-date', field: 'createDateTimeView' },
      {
        i18n: 'common.action',
        field: 'options',
        width: '160px',
        pinned: 'right',
        right: '0px',
        type: 'button',
        buttons: [
          {
            type: 'icon',
            tooltip: this.translate.instant('campaign-resource.assignment'),
            icon: 'shuffle',
            click: record => this.assignmentAction(record.id),
          },
          {
            type: 'icon',
            tooltip: this.translate.instant('campaign-resource.export-data'),
            icon: 'assignment_turned_in',
            click: record => this.exportDetail(record.id),
          },
          {
            type: 'icon',
            tooltip: this.translate.instant('campaign-resource.export-error'),
            color: 'warn',
            icon: 'assignment_late',
            click: record => this.exportError(record.id),
          },
        ],
      },
    ];
    this.service.getLstCampaign().subscribe(res => {
      this.lstCampaign = res;
    });
    this.formControl = this.fb.group({
      attachFile: [null],
      total: [null],
      campaign: ['', [Validators.required]],
      distinct: [0, [Validators.required]],
    });
    this.formSearch = this.fb.group({
      campaign: '',
      fromDate: null,
      toDate: null,
    });
    this.pageIndex = 0;
    this.pageSize = 10;
    this.onSearch();
  }

  doUpload() {
    this.formControl.markAllAsTouched();
    if (this.formControl.invalid) {
      return;
    }
    if (this.file) {
      this.service.uploadFile(this.file._files[0], this.formControl.get('campaign').value).subscribe(res => {
        if (res) {
          this.formControl.get('total').setValue(res);
          this.toastr.success(this.translate.instant('campaign-resource.upload-success'));
        }
      }, error => {
        this.toastr.error(error.error.title);
      });
    } else {
      this.formControl.get('attachFile').setErrors(Validators.required);
      this.formControl.get('attachFile').markAsTouched();
      this.toastr.warning(this.translate.instant('campaign-resource.must-select-file'));
      return;
    }
  }

  clear() {
    this.formControl.get('total').setValue(null);
  }

  doImport() {
    this.formControl.markAllAsTouched();
    if (this.formControl.invalid) {
      return;
    }
    if (this.formControl.get('total').value === null || this.formControl.get('total').value === undefined) {
      this.toastr.warning(this.translate.instant('campaign-resource.must-upload-file'));
      return;
    }
    const id = this.formControl.get('campaign').value;
    const distinct = this.formControl.get('distinct').value;
    this.service.importFile(this.file._files[0], id, distinct).subscribe(res => {
      const rs = res.headers.get('filename').split('-');
      this.toastr.success(this.translate.instant('campaign-resource.import-result.total') + rs[0]
        + '<br/>' + this.translate.instant('campaign-resource.import-result.success') + rs[1]
        + '<br/>' + this.translate.instant('campaign-resource.import-result.error') + rs[2]
        + '<br/>' + this.translate.instant('campaign-resource.import-result.distinct') + rs[3]
        + '<br/>' + this.translate.instant('campaign-resource.import-result.blackList') + rs[4], '', {
        timeOut: 5000,
        enableHtml: true,
      });
      this.search();
    }, error => {
      this.toastr.error(error.error.title);
    });
  }

  onSearch() {
    this.isLoading = true;
    this.dataSearch.page = this.pageIndex;
    this.dataSearch.size = this.pageSize;
    let fromDate;
    let toDate;
    let campaignId;
    if (this.formSearch.get('fromDate').value) {
      fromDate = this.setDateTime(this.formSearch.get('fromDate').value);
    } else {
      fromDate = null;
    }
    if (this.formSearch.get('toDate').value) {
      toDate = this.setDateTime(this.formSearch.get('toDate').value);
    } else {
      toDate = null;
    }
    if (this.formSearch.get('campaign').value !== '') {
      campaignId = this.formSearch.get('campaign').value;
    } else {
      campaignId = -1;
    }
    this.service.doSearch(this.dataSearch, campaignId, fromDate, toDate).subscribe(res => {
      if (res) {
        this.campaignResourceLst = res.body;
        this.totalRecord = Number(res.headers.get('X-Total-Count'));
        this.isLoading = false;
      }
    });
  }

  search() {
    this.pageIndex = 0;
    this.onSearch();
  }

  setDateTime(dateTime) {
    const pipe = new DatePipe('en-US');
    const date = pipe.transform(dateTime, 'dd/MM/yyyy', 'GMT+7');
    return date;
  }

  onPageChange(event) {
    this.pageIndex = event.pageIndex;
    this.pageSize = event.pageSize;
    this.onSearch();
  }

  downloadTemplate() {
    this.formControl.markAllAsTouched();
    if (this.formControl.invalid) {
      return;
    }
    this.service.downloadTemplate(this.formControl.get('campaign').value).subscribe(res => {
      if (res) {
        const fileName = res.headers.get('filename');
        this.saveFile(res.body, fileName);
      }
    }, error => {
      this.toastr.error(this.translate.instant('campaign-resource.error'));
    });
  }

  private saveFile(data: ArrayBuffer, fileName: string) {
    const blob = new Blob([data], { type: 'application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet; charset=UTF-8' });
    fileSaver.saveAs(blob, fileName);
  }

  changeStatus(row: any, event) {
    this.disableToggle = true;
    const status = event.checked ? '1' : '0';
    this.service.changeStatus(row.id, status).pipe(finalize(() => {
      this.disableToggle = false;
    })).subscribe(res => {
      if (res) {
        this.onSearch();
        if (status === '1') {
          this.toastr.success(this.translate.instant('campaign-resource.active-success'));
        } else {
          this.toastr.success(this.translate.instant('campaign-resource.inactive-success'));
        }
      }
    }, error => {
      this.toastr.error(error.error.title);
    });
  }


  assignmentAction(id: any) {
    this.dialog.open({
      width: '30vw',
      disableClose: true,
      hasBackdrop: true,
      data: {
        campaignResourceId: id,
      },
    }, CampaignAssignmentComponent).afterClosed().subscribe((result: any) => {
      if (result === undefined || !result) {
        return;
      }
      if (result.event !== 'cancel') {
        this.campaignResultDetailService.groupCampaignResourceDetails(result).subscribe(res => {
          this.toastr.success(this.translate.instant('campaign-resource-assign.form.success'));
        });
      }
    });
  }

  exportError(id: any) {
    this.service.exportError(id).subscribe(res => {
      if (res) {
        const fileName = res.headers.get('filename');
        this.saveFile(res.body, fileName);
      }
    }, error => {
      this.toastr.error(this.translate.instant('campaign-resource.export-fail'));
    });
  }

  exportDetail(id: any) {
    this.service.exportDetail(id).subscribe(res => {
      if (res) {
        const fileName = res.headers.get('filename');
        this.saveFile(res.body, fileName);
      }
    }, error => {
      this.toastr.error(this.translate.instant('campaign-resource.export-fail'));
    });
  }

  onChangeFromDate() {
    this.minToDate = this.formSearch.get('fromDate').value;
  }

  onChangeToDate() {
    this.maxFromDate = this.formSearch.get('toDate').value;
  }

}
