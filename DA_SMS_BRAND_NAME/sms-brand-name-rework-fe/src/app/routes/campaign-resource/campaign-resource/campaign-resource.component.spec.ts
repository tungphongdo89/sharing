import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CampaignResourceComponent } from './campaign-resource.component';

describe('CampaignResourceComponent', () => {
  let component: CampaignResourceComponent;
  let fixture: ComponentFixture<CampaignResourceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CampaignResourceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CampaignResourceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
