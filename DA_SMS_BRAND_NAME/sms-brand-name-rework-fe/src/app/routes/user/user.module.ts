import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserRoutingModule } from './user-routing.module';
import {SharedModule} from '@shared';
import { UserComponent } from './user/user.component';
import { CreateDialog } from './user/create-dialog/create-dialog.component';
import { ChangePasswordDialog } from './user/change-password-dialog/change-password-dialog.conponent';
import { UserListComponent } from './user/user-list/user-list.component';
import { AddAuthorizeDialogComponent } from './user/add-authorize-dialog/add-authorize-dialog.component';


@NgModule({
  declarations: [UserComponent, CreateDialog, ChangePasswordDialog, UserListComponent, AddAuthorizeDialogComponent],
  imports: [
    CommonModule,
    SharedModule,
    UserRoutingModule
  ]
})
export class UserModule { }
