import { Component, Inject, OnInit, Output } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { UserService } from "@app/core/services/user/user.service";
import { TranslateService } from "@ngx-translate/core";
import { ToastrService } from "ngx-toastr";

@Component({
    selector: 'change-password-dialog',
    templateUrl: './change-password-dialog.html',
})
export class ChangePasswordDialog implements OnInit {
    show:boolean = true;
    changePasswordForm: FormGroup = this.fb.group({
        login:[this.data.username],
        password: [null, [Validators.required]]
    })

    constructor(
        protected translateService: TranslateService,
        private fb: FormBuilder,
        private userService: UserService,
        private toastr: ToastrService,
        public dialogRef: MatDialogRef<ChangePasswordDialog>,
        @Inject(MAT_DIALOG_DATA) public data: { username:string}
    ) { }

    ngOnInit(): void {
        
    }

    changePassword(){
        
        this.userService.updateUser(this.changePasswordForm.value).subscribe(res => {
            this.toastr.success(this.translateService.instant("common.confirm.update.success"));
            this.dialogRef.close();
        }, error =>{
            this.toastr.error(this.translateService.instant("common.notify.update.error"));
            this.toastr.error(error.error.title);
            console.error("At updateUser of create-dialog.component.ts: ");
            console.error(error.error.title);
        })
    }
    
}
