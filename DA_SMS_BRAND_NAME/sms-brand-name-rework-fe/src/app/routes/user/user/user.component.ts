import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { DepartmentService } from '@app/core/services/department/department.service';
import { UserService } from '@app/core/services/user/user.service';
import { ConfirmDialogComponent, ConfirmDialogModel } from '@app/shared/components/confirm-dialog/confirm-dialog.component';
import { MtxGridColumn } from '@ng-matero/extensions';
import { TranslateService } from '@ngx-translate/core';
import { ToastrService } from 'ngx-toastr';
import { ChangePasswordDialog } from './change-password-dialog/change-password-dialog.conponent';
import { CreateDialog } from './create-dialog/create-dialog.component';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {

  constructor() { }

  selected: number = 0;

  ngOnInit(): void {
  }

  select(event: number){
    this.selected = event;
  }
}
