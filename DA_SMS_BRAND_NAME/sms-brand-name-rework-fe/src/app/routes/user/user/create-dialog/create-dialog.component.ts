import { Component, Inject, OnInit, Output } from "@angular/core";
import { FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { DepartmentService } from "@app/core/services/department/department.service";
import { UserService } from "@app/core/services/user/user.service";
import { TranslateService } from "@ngx-translate/core";
import { ToastrService } from "ngx-toastr";

@Component({
    selector: 'create-dialog',
    templateUrl: './create-dialog.html',
})
export class CreateDialog implements OnInit {
    show: boolean = true;
    userData: any = null;
    createForm: FormGroup = this.fb.group({
        firstName: [null],
        email: [null, [Validators.email]],
        imageUrl: [null],
        departments: [null, [Validators.required]],
        login: new FormControl({ value: this.userData?.login, disabled: !this.show }, Validators.required),
        password: [null, [this.show ? Validators.required : null]],
        type: [null, [Validators.required]],
        serverVoice: [null],
        connectionChannel: [null],
        internalNumber: [null],
        internalNumberPassword: [null]
    })
    departmentList: any = [];
    button: any = '';
    typeList: any = [
        { name: "Tổng đài viên", value: '1' },
        { name: "Trưởng nhóm", value: '2' },
        { name: "Admin", value: '3' }
    ];

    constructor(
        protected translateService: TranslateService,
        private fb: FormBuilder,
        private userService: UserService,
        private departmentService: DepartmentService,
        private toastr: ToastrService,
        public dialogRef: MatDialogRef<CreateDialog>,
        @Inject(MAT_DIALOG_DATA) public data: { title: string, username: string }
    ) { }

    ngOnInit(): void {
        this.getAllDepartMent();
        if (typeof this.data.username === 'undefined') {
            this.button = this.translateService.instant("common.button.insert");
            this.show = true;
        } else {
            this.show = false;
            this.createForm.controls['login'].disable();
            this.button = this.translateService.instant("common.button.update");
            this.userService.getByLogin(this.data.username).subscribe(res => {
                this.userData = res;
                this.createForm.patchValue(this.userData);
                this.createForm.controls.departments.setValue(res.departments);
               
            })
        }
    }

    createNewUser() {

        if (this.createForm.invalid) {
            this.toastr.error(this.translateService.instant("common.notify.create.error"));
        } else {
            if (this.createForm.value.email?.trim() === "") {
                this.createForm.value.email = null;
            }

            this.userService.createUser(this.createForm.value).subscribe(res => {
                this.toastr.success(this.translateService.instant("common.notify.save.success2"));
                this.dialogRef.close();
            }, error => {
                // console.log(error);
                this.toastr.error(this.translateService.instant("common.notify.create.error"));
                this.toastr.error(error.error.title || error.error.message);
                
            })
        }

    }

    getAllDepartMent() {
        this.departmentService.getDepartments().subscribe(res => {
            this.departmentList = res;
        })
    }

    updateUser() {
       
        this.createForm.value.login = this.userData?.login;
     
        if (this.createForm.value.email?.trim() === "") {
            this.createForm.value.email = null;
        }
        this.userData.firstName = this.createForm.value.firstName;
        this.userData.lastName = this.createForm.value.lastName;
        this.userData.email = this.createForm.value.email;
        this.userData.login = this.userData?.login;
        this.userData.departmentId = this.createForm.value.departmentId;
        this.userData.serverVoice = this.createForm.value.serverVoice;
        this.userData.connectionChannel = this.createForm.value.connectionChannel;
        this.userData.internalNumber = this.createForm.value.internalNumber;
        this.userData.internalNumberPassword = this.createForm.value.internalNumberPassword;
        this.userData.departments = this.createForm.value.departments;
        
        // this.userData = this.createForm.getRawValue();

        this.userService.updateUser(this.userData).subscribe(res => {
            this.toastr.success(this.translateService.instant("common.confirm.update.success"));
            this.dialogRef.close();
        }, error => {
            this.toastr.error(this.translateService.instant("common.notify.update.error"));
            this.toastr.error(error.error.title);
            console.error("At updateUser of create-dialog.component.ts: ");
            console.error(error.error.title);
            console.log(error);
        })

    }
}
