import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { DepartmentService } from '@app/core/services/department/department.service';
import { ProfileService } from '@app/core/services/profile/profile.service';
import { UserService } from '@app/core/services/user/user.service';
import { ConfirmDialogComponent, ConfirmDialogModel } from '@app/shared/components/confirm-dialog/confirm-dialog.component';
import { MtxGridColumn } from '@ng-matero/extensions';
import { TranslateService } from '@ngx-translate/core';
import { ToastrService } from 'ngx-toastr';
import { AddAuthorizeDialogComponent } from '../add-authorize-dialog/add-authorize-dialog.component';
import { ChangePasswordDialog } from '../change-password-dialog/change-password-dialog.conponent';
import { CreateDialog } from '../create-dialog/create-dialog.component';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit {
  @Input() active: boolean;
  data: any = []
  columns: MtxGridColumn[] = [];
  pageSizeList = [10, 50, 100];
  pagesize = 10;
  searchText: any = "";
  offset = 0;
  total: any = 0;
  searchForm: FormGroup = this.fb.group({
    login: [null],
    name: [null],
    email: [null],
    departmentId: [null],
    type: [null],
    internalNumber: [null],
    serverVoice: [null],
    connectionChannel: [null],
  });
  typeList: any = [
    { name: "Tổng đài viên", value: '1' },
    { name: "Trưởng nhóm", value: '2' },
    { name: "Admin", value: '3' }
  ];
  departmentList: any = [];

  constructor(
    private fb: FormBuilder,
    private userService: UserService,
    protected translateService: TranslateService,
    private dialog: MatDialog,
    private toastr: ToastrService,
    private departmentService: DepartmentService,
    private profileService: ProfileService
  ) { }

  ngOnInit(): void {
    console.log("active: " + this.active)
    this.init();
    this.columns = [
      { i18n: 'user.management.column.no', field: 'index', width: '40px' },
      { i18n: 'user.management.column.fullName', field: 'firstName', width: '180px' },
      { i18n: 'user.management.column.login', field: 'login', width: '150px' },
      { i18n: 'user.management.column.email', field: 'email', width: '150px' },
      { i18n: 'user.management.column.serverVoice', field: 'serverVoice', width: '150px' },
      { i18n: 'user.management.column.connectionChannel', field: 'connectionChannel', width: '150px' },
      { i18n: 'user.management.column.internalNumber', field: 'internalNumber', width: '150px' },
      { i18n: 'user.management.column.internalNumberPassword', field: 'internalNumberPassword', width: '150px' },
      
      {
        i18n: 'common.action',
        field: 'options',
        width: '200px',
        pinned: 'right',
        right: '0px',
        type: 'button',
        buttons: this.active ? [
          {
            type: 'icon',
            text: 'edit',
            icon: 'edit',
            tooltip: this.translateService.instant('common.button.edit'),
            click: record => this.openDialog({ username: record.login, title: this.translateService.instant('user.management.updateUserTitle') }),
          },
          {
            type: 'icon',
            text: 'setAuthorize',
            icon: 'build',
            tooltip: this.translateService.instant('common.setAuthorize'),
            click: record => this.openPermissionDialog({ username: record.login }),
          },
          {
            type: 'icon',
            text: 'lock',
            icon: 'toggle_off',
            color: 'warn',
            tooltip: this.translateService.instant('common.lock'),
            click: record => this.confirmLock(record),
          },
          {
            type: 'icon',
            text: 'changePassword',
            icon: 'vpn_key',
            color: 'accent',
            tooltip: this.translateService.instant('common.changePassword'),
            click: record => this.confirmChangePassword(record.login),
          },
          // {
          //   type: 'icon',
          //   text: 'delete',
          //   icon: 'delete',
          //   color: 'warn',
          //   tooltip: this.translateService.instant('common.button.delete'),
          //   click: record => this.confirmDelete(record),
          // }
        ] : [
          {
            type: 'icon',
            text: 'active',
            icon: 'toggle_on',
            color: 'primary',
            tooltip: this.translateService.instant('common.enableUser'),
            click: record => this.confirmEnableUser(record),
          },
        ]
      }

    ];
  }

  init() {
    this.getAllUsers();
    this.getAllDepartMent();
  }

  getAllDepartMent() {
    this.departmentService.getDepartments().subscribe(res => {
      this.departmentList = res;
    })
  }

  getAllUsers(search?: any) {
    this.userService.getAllUsers({ page: this.offset, size: this.pagesize, ...search, activated: this.active }).subscribe(res => {
      this.total = res.headers.get('X-Total-Count');
      this.data = res.body;
    }, error => {
      this.toastr.error(error.error.detail);
    })
  }

  search() {
    let searchObj = this.searchForm.value;
    searchObj.login === null ? "" : searchObj.login;
    this.offset = 0;
    this.getAllUsers(searchObj);
  }

  onPageChangeScript(event) {
    this.offset = event.pageIndex;
    this.pagesize = event.pageSize;
    this.getAllUsers();
  }

  openDialog(data?: any) { // data:{username: number, title:string}
    if (typeof data.title === 'undefined') {
      data.title = this.translateService.instant('user.management.createUserTitle')
    }
    const dialogRef = this.dialog.open(CreateDialog, {
      data: data
    });
    dialogRef.afterClosed().subscribe(result => {
      this.search();
    });
  }

  // openChangePasswordDialog(data?: any) { //data:{username:string}
  //   const dialogRef = this.dialog.open(ChangePasswordDialog, {
  //     data: data
  //   });
  //   dialogRef.afterClosed().subscribe(result => {

  //   });
  // }

  confirmChangePassword(login: string){
    const title = this.translateService.instant('common.confirm.reset-password');
    const content = this.translateService.instant('common.confirm');
    const dialogContent: ConfirmDialogModel = new ConfirmDialogModel(title, content);
    this.dialog.open(ConfirmDialogComponent, {
      width: '25vw',
      disableClose: true,
      hasBackdrop: true,
      data: dialogContent
    }).afterClosed().subscribe(result => {
      if (result.data === 'confirm') {
        // this.deleteUser(record.login);
        this.changePassword(login);
      }
    })
  }

  changePassword(login:string){
    this.profileService.retrieveMyPassword(login).subscribe(res =>{
      if (res.message === null) {
        this.toastr.success(this.translateService.instant("login.notify-send-email-retrieve-password"));
      } else {
        this.toastr.error(res.message);
        console.error(res.message);
      }
    })
  }

  deleteUser(login: string) {
    this.userService.deleteUser(login).subscribe(res => {
      this.init();
      this.toastr.success(this.translateService.instant("common.attachment.delete.success"))
    }, error => {
      this.toastr.error(error.error.detail);
    })
  }

  confirmLock(record) {
    const title = this.translateService.instant('common.confirm.lock');
    const content = this.translateService.instant('common.confirm');
    const dialogContent: ConfirmDialogModel = new ConfirmDialogModel(title, content);
    this.dialog.open(ConfirmDialogComponent, {
      width: '25vw',
      disableClose: true,
      hasBackdrop: true,
      data: dialogContent
    }).afterClosed().subscribe(result => {
      if (result.data === 'confirm') {
        this.lockUser(record.login);
      }
    })
  }

  confirmEnableUser(record) {
    const title = this.translateService.instant('common.confirm.enableUser');
    const content = this.translateService.instant('common.confirm');
    const dialogContent: ConfirmDialogModel = new ConfirmDialogModel(title, content);
    this.dialog.open(ConfirmDialogComponent, {
      width: '25vw',
      disableClose: true,
      hasBackdrop: true,
      data: dialogContent
    }).afterClosed().subscribe(result => {
      if (result.data === 'confirm') {
        // this.deleteUser(record.login);
        this.enableUser(record.login);
      }
    })
  }

  enableUser(login: string) {
    this.userService.updateUser({ login: login, activated: true }).subscribe(res => {
      this.init();
      this.toastr.success(this.translateService.instant("common.enable.success"))
    }, error => {
      this.toastr.error(error.error.detail);
    })
  }

  lockUser(login: string) {
    this.userService.updateUser({ login: login, activated: false }).subscribe(res => {
      this.init();
      this.toastr.success(this.translateService.instant("common.lock.success"))
    }, error => {
      this.toastr.error(error.error.detail);
    })
  }

  openPermissionDialog(data?:any){
    
    const dialogRef = this.dialog.open(AddAuthorizeDialogComponent, {
      data: data
    });
    dialogRef.afterClosed().subscribe(result => {
      this.search();
    });
  }

}
