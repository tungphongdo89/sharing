import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddAuthorizeDialogComponent } from './add-authorize-dialog.component';

describe('AddAuthorizeDialogComponent', () => {
  let component: AddAuthorizeDialogComponent;
  let fixture: ComponentFixture<AddAuthorizeDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddAuthorizeDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddAuthorizeDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
