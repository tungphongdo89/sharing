import { Component, Inject, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DepartmentService } from '@app/core/services/department/department.service';
import { UserService } from '@app/core/services/user/user.service';
import { TranslateService } from '@ngx-translate/core';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-add-authorize-dialog',
  templateUrl: './add-authorize-dialog.component.html',
  styleUrls: ['./add-authorize-dialog.component.scss']
})
export class AddAuthorizeDialogComponent implements OnInit {

  constructor(
    protected translateService: TranslateService,
    private fb: FormBuilder,
    private userService: UserService,
    private departmentService: DepartmentService,
    private toastr: ToastrService,
    public dialogRef: MatDialogRef<AddAuthorizeDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: { username: string }
  ) { }

  authorityList: any[] = [];
  authoritiesOfUser: any[] = [];

  ngOnInit(): void {
    this.getAllAuthorities();
    this.getUserAuthorities();
  }

  getAllAuthorities() {
    this.userService.getAllAuthorities().subscribe(res => {
      res.forEach((e: String) => {
        this.authorityList.push(e.replace('ROLE_', ''));
      });
    }, error => {
      this.toastr.error(this.translateService.instant('common.getAuthorities.failed'))
    })
  }

  getUserAuthorities() {
    this.userService.getByLogin(this.data.username).subscribe(res => {
      this.authoritiesOfUser = res.authorities;
    })
  }

  checkedValue(permission: string) {
    if (this.authoritiesOfUser.indexOf('ROLE_' + permission.toUpperCase()) !== -1) {
      return true;
    } else return false;
  }

  setAuthorities() {
    this.userService.updateUser({ login: this.data.username, authorities: this.authoritiesOfUser }).subscribe(res => {
      this.toastr.success(this.translateService.instant('common.setPermission.success'));;
      this.dialogRef.close();
    }, error => {
      this.toastr.error(this.translateService.instant('common.setPermission.failed'));;
    })

  }

  changeAuthority(check: boolean, authority: string) {
    let permission = 'ROLE_' + authority.toUpperCase();
    let index = this.authoritiesOfUser.indexOf(permission);
    if (check) {
      if (index === -1) {
        this.authoritiesOfUser.push(permission);
      }
    } else {
      if (index !== -1) this.authoritiesOfUser.splice(index, 1);
    }
  }

}
