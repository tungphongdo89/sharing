import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {CampaignGroupUserRoutingModule} from './campaign-group-user-routing.module';
import {
  CampaignGroupRestoreComponent,
  CampaignGroupUserComponent,
  CampaignResourceRestoreComponent
} from './campaign-group-user/campaign-group-user.component';
import {SharedModule} from '@shared';


@NgModule({
  declarations: [CampaignGroupUserComponent, CampaignGroupRestoreComponent, CampaignResourceRestoreComponent],
  imports: [
    CommonModule,
    CampaignGroupUserRoutingModule,
    SharedModule
  ]
})
export class CampaignGroupUserModule {
}
