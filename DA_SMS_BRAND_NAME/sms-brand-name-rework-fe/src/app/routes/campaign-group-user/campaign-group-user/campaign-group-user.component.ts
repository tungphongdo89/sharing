import {Component, Inject, OnInit} from '@angular/core';
import {MtxGridColumn} from '@ng-matero/extensions';
import {CampaignResourceService} from '@core/services/campaign-resource/campaign-resource.service';
import {ToastrService} from 'ngx-toastr';
import {TranslateService} from '@ngx-translate/core';
import {CampaignResultDetailService} from '@app/core/services/campaign-resource-detail/campaign-result-detail.service';
import {GroupUserService} from '@core/services/group-user.service';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-campaign-group-user',
  templateUrl: './campaign-group-user.component.html',
  styleUrls: ['./campaign-group-user.component.scss']
})
export class CampaignGroupUserComponent implements OnInit {

  // Danh sách chiến dịch
  campaignList: any = [];

  // Bảng tổng đài viên
  pageIndexTb2: any;
  pageSizeTb2: any;
  columns: MtxGridColumn[] = [];

  // Bảng nhóm
  isLoading = false;
  totalRecord: any;
  pageIndex: any;
  pageSize: any;
  pageSizeList = [5, 10, 50, 100];
  columnsGroup: MtxGridColumn[] = [];
  groupList: any = [];

  // Danh sách nhóm
  userGroupList: any = [];

  // Tìm kiếm DL chưa phân công
  campaignId = 0;
  groupId = 0;
  yetAssign = 0;

  // Phân công DL cho tổng đài viên
  formSave: FormGroup;
  userList: any = [];
  selectAll: boolean;

  // Danh sách tổng đài viên
  nameSearch: string;
  campaignUserList: any = [];

  constructor(
    private service: CampaignResourceService,
    private toastr: ToastrService,
    private translate: TranslateService,
    private campaignResultDetailService: CampaignResultDetailService,
    private groupUserService: GroupUserService,
    private dialog: MatDialog
  ) {
  }

  ngOnInit(): void {

    this.formSave = new FormGroup({});

    // Cột danh sách nhóm
    this.columnsGroup = [
      {i18n: 'campaign-group-user.group.columns.index', field: 'no', width: '50px'},
      {i18n: 'campaign-group-user.group.columns.group-name', field: 'groupName', width: '150px'},
      {i18n: 'campaign-group-user.group.columns.received', field: 'received', width: '100px'},
      {i18n: 'campaign-group-user.group.columns.called', field: 'called', width: '100px'},
      {i18n: 'campaign-group-user.group.columns.yet-call', field: 'yetCall', width: '100px'},
      {i18n: 'campaign-group-user.group.columns.yet-assign', field: 'yetAssign', width: '100px'},
      {i18n: 'campaign-group-user.group.columns.assigned', field: 'assigned', width: '100px'},
      {
        i18n: 'campaign-group-user.group.columns.data-yet-call-return',
        field: 'callReturn',
        width: '150px',
        pinned: 'right'
      }
    ];

    // Cột danh sách tổng đài viên
    this.columns = [
      {i18n: 'campaign-group-user.user.columns.index', field: 'no', width: '50px'},
      {i18n: 'campaign-group-user.user.columns.username', field: 'userName', width: '150px'},
      {i18n: 'campaign-group-user.user.columns.received', field: 'received', width: '100px'},
      {i18n: 'campaign-group-user.user.columns.called', field: 'called', width: '100px'},
      {i18n: 'campaign-group-user.user.columns.yet-call', field: 'yetCall', width: '100px'},
      {
        i18n: 'campaign-group-user.user.columns.data-yet-call-return',
        field: 'callReturn',
        width: '150px',
        pinned: 'right'
      }
    ];

    this.pageIndex = 0;
    this.pageSize = 5;

    this.pageIndexTb2 = 0;
    this.pageSizeTb2 = 10;

    this.getCampaignList();
    this.getGroupList();

  }

  getGroupList(): void {
    this.campaignResultDetailService.getGroupList().subscribe(res => this.groupList = res);
  }

  getCampaignList(): void {
    this.service.getLstCampaign().subscribe(res => this.campaignList = res);
  }

  onGetGroupUser(campaignId: any): void {
    this.userList = [];
    this.campaignUserList = [];
    this.groupId = null;
    this.groupUserService.getAllGroupUsersByCampaignId(campaignId).subscribe(res => this.userGroupList = res);
  }

  onGetCampaignData(): void {
    if (this.campaignId && this.groupId) {
      this.selectAll = false;
      this.campaignResultDetailService.getDataCampaignGroup(this.campaignId, this.groupId).subscribe(
        res => {
          this.yetAssign = res.noCampaignGroupYetAssign ? res.noCampaignGroupYetAssign : 0;
          this.userList = res.lstUserDto ? res.lstUserDto : [];
          this.userList.forEach(item => {
            this.formSave.setControl(item.id.toString(), new FormControl(0, [Validators.min(0), Validators.max(this.yetAssign)]));
            this.formSave.get(item.id.toString()).disable();
          });
        });

      const data = {
        campaignId: this.campaignId,
        groupId: this.groupId,
        search: this.nameSearch ? this.nameSearch.trim() : null
      };
      this.campaignResultDetailService.getCampaignUser(data).subscribe(
        res => this.campaignUserList = res);
    }
  }

  doAssign(): void {
    if (this.campaignId && this.groupId && this.yetAssign > 0 && this.userList.length > 0) {
      this.userList.forEach(item => item.numberData = this.formSave.get(item.id.toString()).value);
      const data = {
        groupId: this.groupId,
        campaignId: this.campaignId,
        lstUserDto: this.userList
      };
      if (this.userListIsValid()) {
        this.campaignResultDetailService.assignCampaignUser(data).subscribe(
          _ => {
            this.toastr.success(this.translate.instant('campaign-group-user.response.success-assign'));
            this.onGetCampaignData();
            this.getGroupList();
          });
      }
    }
  }

  /**
   * Mở modal trả dữ liệu về nhóm
   */
  openRestoreCampaignGroupDialog(user: any): void {
    const dialogRef = this.dialog.open(CampaignGroupRestoreComponent, {
      width: '30vw',
      disableClose: true,
      hasBackdrop: true,
      data: user
    });

    dialogRef.afterClosed().subscribe(async result => {
      if (result === undefined || !result) {
        return;
      }
      if (result.event !== 'cancel') {
        await this.doRestoreCampaignGroup(result);
        this.getGroupList();
        this.onGetCampaignData();
      }
    });
  }

  /**
   * Trả dữ liệu về nhóm
   */
  async doRestoreCampaignGroup(user: any): Promise<void> {
    const data = {
      groupId: this.groupId,
      campaignId: this.campaignId,
      userId: user.assignUserId,
      userDTO: {
        numberData: user.numberData ? user.numberData : 0
      }
    };
    await this.campaignResultDetailService.restoreCampaignGroup(data).toPromise().then(
      _ => {
        this.toastr.success(this.translate.instant('campaign-group-user.response.success-return'));
      });
  }

  /**
   * Mở modal trả dữ liệu về nguồn chiến dịch
   */
  openRestoreCampaignResourceDialog(group: any): void {
    const dialogRef = this.dialog.open(CampaignResourceRestoreComponent, {
      width: '30vw',
      disableClose: true,
      hasBackdrop: true,
      data: group
    });

    dialogRef.afterClosed().subscribe(async result => {
      if (result === undefined || !result) {
        return;
      }
      if (result.event !== 'cancel') {
        await this.doRestoreCampaignResource(result);
        this.getGroupList();
        this.onGetCampaignData();
      }
    });
  }

  /**
   * Trả dữ liệu về nguồn chiến dịch
   */
  async doRestoreCampaignResource(group: any): Promise<void> {
    const data = {
      groupId: group.groupId,
      groupUserDTO: {
        numberData: group.numberData
      }
    };
    await this.campaignResultDetailService.restoreCampaignResource(data).toPromise().then(
      _ => this.toastr.success(this.translate.instant('campaign-group-user.response.success-return')));
  }

  onPageChange(event) {
    this.pageIndex = event.pageIndex;
    this.pageSize = event.pageSize;
  }

  onUserPageChange(event) {
    this.pageIndexTb2 = event.pageIndex;
    this.pageSizeTb2 = event.pageSize;
  }

  onSelectAllUserChange(): void {
    if (this.selectAll === true) {
      this.userList.forEach(item => item.checked = true);
      this.formSave.enable();
    } else {
      this.userList.forEach(
        item => {
          item.checked = false;
          this.formSave.setControl(item.id.toString(), new FormControl(0, [Validators.min(0), Validators.max(this.yetAssign)]));
          this.formSave.get(item.id.toString()).disable();
        }
      );
    }
  }

  onSelectUserChange(user: any): void {
    const uncheckedList = this.userList.filter(item => !item.checked);
    if (uncheckedList && uncheckedList.length > 0) {
      this.selectAll = false;
    } else {
      console.log(this.selectAll);
      this.selectAll = true;
    }
    if (user.checked) {
      this.formSave.get(user.id.toString()).enable();
    } else {
      this.formSave.setControl(user.id.toString(), new FormControl(0, [Validators.min(0), Validators.max(this.yetAssign)]));
      this.formSave.get(user.id.toString()).disable();
    }
  }

  doEqualAssign(): void {
    const userCheckedList = this.userList.filter(user => user.checked);
    if (this.yetAssign && this.yetAssign > 0 && userCheckedList.length > 0) {
      let count = 1;
      let max = 0;
      while (max < this.yetAssign) {
        for (const user of userCheckedList) {
          this.formSave.setControl(user.id.toString(), new FormControl(count, [Validators.min(0), Validators.max(this.yetAssign)]));
          max += 1;
          if (max >= this.yetAssign) break;
        }
        count++;
      }
    }
  }

  onSearch(): void {
    this.isLoading = true;
    const data = {
      campaignId: this.campaignId,
      groupId: this.groupId,
      search: this.nameSearch ? this.nameSearch.trim() : null
    };
    if (this.campaignId && this.groupId) {
      this.campaignResultDetailService.getCampaignUser(data).subscribe(
        res => {
          this.campaignUserList = res;
          this.isLoading = false;
        }, () => {
          this.isLoading = false;
        }
      );
    }
  }

  userListIsValid(): boolean {
    let sum = 0;
    this.userList.forEach(user => {
      if (user.checked && user.numberData) {
        sum += user.numberData;
      }
    });
    if (sum > this.yetAssign) {
      this.toastr.error(this.translate.instant('campaign-group-user.response.over-limit'));
      return false;
    }
    return true;
  }
}

/**
 * Component con dialog trả dl về nhóm
 */
@Component({
  selector: 'campaign-group-restore',
  templateUrl: 'campaign-group-restore.html',
  styleUrls: ['./campaign-group-user.component.scss']
})
export class CampaignGroupRestoreComponent {

  formSave: FormGroup;

  constructor(
    public dialogRef: MatDialogRef<CampaignGroupRestoreComponent>,
    @Inject(MAT_DIALOG_DATA) public input: any
  ) {
    this.formSave = new FormGroup({
      numberData: new FormControl(0, [Validators.min(0), Validators.max(input.yetCall)])
    });
  }

  onReturnData(): void {
    this.input.numberData = this.formSave.value.numberData ? this.formSave.value.numberData : 0;
    this.dialogRef.close(this.input);
  }

}

/**
 * Component con dialog trả dl về nguồn chiến dịch
 */
@Component({
  selector: 'campaign-resource-restore',
  templateUrl: 'campaign-resource-restore.html',
  styleUrls: ['./campaign-group-user.component.scss']
})
export class CampaignResourceRestoreComponent {

  formSave: FormGroup;

  constructor(
    public dialogRef: MatDialogRef<CampaignGroupRestoreComponent>,
    @Inject(MAT_DIALOG_DATA) public input: any
  ) {
    this.formSave = new FormGroup({
      numberData: new FormControl(0, [Validators.min(0), Validators.max(input.yetCall)])
    });
  }

  onReturnData(): void {
    this.input.numberData = this.formSave.value.numberData ? this.formSave.value.numberData : 0;
    this.dialogRef.close(this.input);
  }

}
