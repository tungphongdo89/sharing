import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CampaignGroupUserComponent } from './campaign-group-user.component';

describe('CampaignGroupUserComponent', () => {
  let component: CampaignGroupUserComponent;
  let fixture: ComponentFixture<CampaignGroupUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CampaignGroupUserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CampaignGroupUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
