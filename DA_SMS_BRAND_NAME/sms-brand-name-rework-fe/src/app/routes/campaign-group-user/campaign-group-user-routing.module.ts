import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {CampaignGroupUserComponent} from '@app/routes/campaign-group-user/campaign-group-user/campaign-group-user.component';


const routes: Routes = [
  {
    path: '',
    component: CampaignGroupUserComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CampaignGroupUserRoutingModule { }
