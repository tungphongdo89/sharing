import {Component, Input, OnInit} from '@angular/core';
import {FormControl} from '@angular/forms';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-inline-message',
  templateUrl: './inline-message.component.html',
  styleUrls: ['./inline-message.component.scss']
})
export class InlineMessageComponent implements OnInit {
  @Input() formName: FormControl;
  @Input() message: any;
  @Input() customMessage: any;
  constructor(private translate: TranslateService) { }

  ngOnInit() {
  }

  getMessage(error: any) {
    if (error) {
      const key = Object.keys(error);
      if (key) {
        if (this.customMessage && this.customMessage[key[0]]) {
          return this.customMessage[key[0]];
        } else return this.translate.instant( 'validation.' + key[0], {field: this.message , value: error[key[0]] ? error[key[0]].requiredLength : ''});
      }
    }
  }
}
