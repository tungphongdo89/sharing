import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportCampaignEmailMarketingComponent } from './report-campaign-email-marketing.component';

describe('ReportCampaignEmailMarketingComponent', () => {
  let component: ReportCampaignEmailMarketingComponent;
  let fixture: ComponentFixture<ReportCampaignEmailMarketingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportCampaignEmailMarketingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportCampaignEmailMarketingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
