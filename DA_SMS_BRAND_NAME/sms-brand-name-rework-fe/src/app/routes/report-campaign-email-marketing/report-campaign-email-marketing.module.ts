import { NgModule } from '@angular/core';
import { ReportCampaignEmailMarketingComponent } from '@app/routes/report-campaign-email-marketing/report-campaign-email-marketing/report-campaign-email-marketing.component';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@shared';
import { ReportCampaignEmailMarketingRouting } from '@app/routes/report-campaign-email-marketing/report-campaign-email-marketing.routing';

@NgModule({
  declarations: [ReportCampaignEmailMarketingComponent],
  imports: [
    CommonModule,
    SharedModule,
    ReportCampaignEmailMarketingRouting
  ]
})
export class ReportCampaignEmailMarketingModule {}
