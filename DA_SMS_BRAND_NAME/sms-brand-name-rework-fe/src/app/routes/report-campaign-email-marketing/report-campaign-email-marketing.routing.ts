import { RouterModule, Routes } from '@angular/router';
import { ReportCampaignEmailMarketingComponent } from '@app/routes/report-campaign-email-marketing/report-campaign-email-marketing/report-campaign-email-marketing.component';
import { NgModule } from '@angular/core';

const routes: Routes = [
  {
    path: '',
    component: ReportCampaignEmailMarketingComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ReportCampaignEmailMarketingRouting {
}
