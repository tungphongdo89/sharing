import { NgModule } from '@angular/core';
import { ReportCampaignSmsMarketingComponent } from '@app/routes/report-campaign-sms-marketing/report-campaign-sms-marketing/report-campaign-sms-marketing.component';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@shared';
import { ReportCampaignSmsMarketingRouting } from '@app/routes/report-campaign-sms-marketing/report-campaign-sms-marketing.routing';

@NgModule({
  declarations: [ReportCampaignSmsMarketingComponent],
  imports: [
    CommonModule,
    SharedModule,
    ReportCampaignSmsMarketingRouting
  ]
})
export class ReportCampaignSmsMarketingModule {}
