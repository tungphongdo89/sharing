import { RouterModule, Routes } from '@angular/router';
import { ReportCampaignSmsMarketingComponent } from '@app/routes/report-campaign-sms-marketing/report-campaign-sms-marketing/report-campaign-sms-marketing.component';
import { NgModule } from '@angular/core';

const routes: Routes = [
  {
    path: '',
    component: ReportCampaignSmsMarketingComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ReportCampaignSmsMarketingRouting {
}
