import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportCampaignSmsMarketingComponent } from './report-campaign-sms-marketing.component';

describe('ReportCampaignSmsMarketingComponent', () => {
  let component: ReportCampaignSmsMarketingComponent;
  let fixture: ComponentFixture<ReportCampaignSmsMarketingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportCampaignSmsMarketingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportCampaignSmsMarketingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
