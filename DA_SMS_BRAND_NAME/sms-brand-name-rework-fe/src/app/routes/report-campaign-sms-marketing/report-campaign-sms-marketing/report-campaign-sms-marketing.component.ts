import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MtxGridColumn } from '@ng-matero/extensions';
import { CampaignSmsMarketingService } from '@core/services/campaign-sms-marketing/campaign-sms-marketing.service';
import { ReportCampaignSmsMarketingService } from '@core/services/report-campaign-sms-marketing/report-campaign-sms-marketing.service';
import { DatePipe } from '@angular/common';
import { finalize } from 'rxjs/operators';
import * as fileSaver from 'file-saver';

@Component({
  selector: 'app-report-campaign-sms-marketing',
  templateUrl: './report-campaign-sms-marketing.component.html',
  styleUrls: ['./report-campaign-sms-marketing.component.scss'],
})
export class ReportCampaignSmsMarketingComponent implements OnInit {
  formSearch: FormGroup;
  columns: MtxGridColumn[] = [];
  lstCampaign = [];
  pageIndex: any;
  pageSize: any;
  dataSearch = {
    page: null,
    size: null,
  };
  totalRecord: any;
  lst: any;
  pageSizeList = [10, 50, 100];
  isLoading = false;
  minToDate;
  maxFromDate;

  constructor(
    private fb: FormBuilder,
    private campaignSmsMarketingService: CampaignSmsMarketingService,
    private service: ReportCampaignSmsMarketingService,
  ) {
  }

  ngOnInit(): void {
    this.columns = [
      { i18n: 'report-campaign-sms-marketing.columns.index', width: '50px', field: 'no' },
      { i18n: 'report-campaign-sms-marketing.columns.campaign-name', field: 'campaignName' },
      { i18n: 'report-campaign-sms-marketing.columns.total-sms', field: 'totalSms' },
      { i18n: 'report-campaign-sms-marketing.columns.sent', field: 'sent' },
      { i18n: 'report-campaign-sms-marketing.columns.unsent', field: 'unsent' },
      { i18n: 'report-campaign-sms-marketing.columns.sent-success', field: 'sentSuccess' },
      { i18n: 'report-campaign-sms-marketing.columns.sent-error', field: 'sentError' },
    ];
    this.formSearch = this.fb.group({
      campaign: '',
      fromDate: null,
      toDate: null,
    });
    this.campaignSmsMarketingService.getAllSearch().subscribe(res => {
      this.lstCampaign = res.body;
    });
    this.pageIndex = 0;
    this.pageSize = 10;
    this.onSearch();
  }

  onSearch() {
    this.isLoading = true;
    this.dataSearch.page = this.pageIndex;
    this.dataSearch.size = this.pageSize;
    let fromDate;
    let toDate;
    let name;
    if (this.formSearch.get('fromDate').value) {
      fromDate = this.setDateTime(this.formSearch.get('fromDate').value);
    } else {
      fromDate = null;
    }
    if (this.formSearch.get('toDate').value) {
      toDate = this.setDateTime(this.formSearch.get('toDate').value);
    } else {
      toDate = null;
    }
    if (this.formSearch.get('campaign').value !== '') {
      name = this.formSearch.get('campaign').value;
    } else {
      name = -1;
    }
    this.service.doSearch(this.dataSearch, name, fromDate, toDate).pipe(finalize(() => {
      this.isLoading = false;
    })).subscribe(res => {
      if (res) {
        this.lst = res.body;
        this.totalRecord = Number(res.headers.get('X-Total-Count'));
      }
    });
  }

  onPageChange(event) {
    this.pageIndex = event.pageIndex;
    this.pageSize = event.pageSize;
    this.onSearch();
  }

  search() {
    if (this.formSearch.invalid) {
      return;
    }
    this.pageIndex = 0;
    this.onSearch();
  }

  setDateTime(dateTime) {
    const pipe = new DatePipe('en-US');
    const date = pipe.transform(dateTime, 'dd/MM/yyyy', 'GMT+7');
    return date;
  }

  export() {
    if (this.formSearch.invalid) {
      return;
    }
    let fromDate;
    let toDate;
    let name;
    if (this.formSearch.get('fromDate').value) {
      fromDate = this.setDateTime(this.formSearch.get('fromDate').value);
    } else {
      fromDate = null;
    }
    if (this.formSearch.get('toDate').value) {
      toDate = this.setDateTime(this.formSearch.get('toDate').value);
    } else {
      toDate = null;
    }
    if (this.formSearch.get('campaign').value !== '') {
      name = this.formSearch.get('campaign').value;
    } else {
      name = -1;
    }
    this.service.export(name, fromDate, toDate).subscribe(res => {
      if (res) {
        const fileName = res.headers.get('filename');
        this.saveFile(res.body, fileName);
      }
    });
  }

  private saveFile(data: ArrayBuffer, fileName: string) {
    const blob = new Blob([data], { type: 'application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet; charset=UTF-8' });
    fileSaver.saveAs(blob, fileName);
  }

  onChangeFromDate() {
    this.minToDate = this.formSearch.get('fromDate').value;
  }

  onChangeToDate() {
    this.maxFromDate = this.formSearch.get('toDate').value;
  }

}
