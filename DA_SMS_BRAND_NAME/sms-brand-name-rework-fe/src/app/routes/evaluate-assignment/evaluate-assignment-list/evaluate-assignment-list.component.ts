import {Component, Inject, OnInit} from '@angular/core';
import {MtxGridColumn} from '@ng-matero/extensions';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {ToastrService} from 'ngx-toastr';
import {TranslateService} from '@ngx-translate/core';
import {EvaluateAssignmentComponent} from '@app/routes/evaluate-assignment/evaluate-assignment/evaluate-assignment.component';
import {EvaluateAssignmentService} from '@core/services/evaluate-assignment/evaluate-assignment.service';

@Component({
  selector: 'app-evaluate-assignment-list',
  templateUrl: './evaluate-assignment-list.component.html',
  styleUrls: ['./evaluate-assignment-list.component.scss']
})
export class EvaluateAssignmentListComponent implements OnInit {

  // Tìm kiếm
  channelId: any;      // Kênh thông tin
  evaluaterId: any;    // Người đánh giá
  userId: any;         // Tổng đài viên
  startDate: Date;       // Ngày phân công từ
  endDate: Date;         // Ngày phân công đến
  assignList: any = []; // Danh sách phân công
  channelList: any = [
    {
      label: this.translate.instant('evaluate-assignment.in-call'),
      value: 1
    }, {
      label: this.translate.instant('evaluate-assignment.out-call'),
      value: 2
    }
  ];
  evaluaterList: any = [];
  userList: any = [];

  // Bảng
  isLoading: boolean;
  pageSize: number;
  pageIndex: number;
  pageSizeList = [5, 10, 20, 50];
  columns: MtxGridColumn[] = [];
  totalRecord = 0;

  constructor(
    private dialog: MatDialog,
    private toastr: ToastrService,
    private translate: TranslateService,
    private evaluateAssignmentService: EvaluateAssignmentService,
  ) {
  }

  ngOnInit(): void {
    this.createTable();
    this.getUserList();
    this.getEvaluatorList();
    this.getData();
  }

  createTable(): void {
    this.pageIndex = 0;
    this.pageSize = 10;

    this.columns = [
      {i18n: 'STT', field: 'no', width: '50px'},
      {i18n: 'evaluate-assignment.columns.channel', field: 'channelName', width: '200px',},
      {i18n: 'evaluate-assignment.columns.evaluater', field: 'evaluaterName', width: '200px'},
      {i18n: 'evaluate-assignment.columns.total-call', field: 'totalCall', width: '150px'},
      {i18n: 'evaluate-assignment.columns.total-user', field: 'totalUserId', width: '150px'},
      {i18n: 'evaluate-assignment.columns.user', field: 'names', width: '200px'},
      {i18n: 'evaluate-assignment.columns.call-time', field: 'callTime', width: '250px'},
      {i18n: 'evaluate-assignment.columns.assign-time', field: 'createDatetime', width: '150px'},
      {i18n: 'evaluate-assignment.columns.assigner', field: 'createUserName', width: '200px'},
      {
        i18n: 'evaluate-assignment.tool',
        field: 'tools',
        width: '150px',
        pinned: 'right'
      }
    ];
  }

  getAssignList(page: any, size: any): void {
    const data = {
      channelId: this.channelId,
      evaluaterId: this.evaluaterId,
      userId: this.userId,
      startDate: this.startDate,
      endDate: this.endDate
    };
    const pageable = {page, size};
    this.evaluateAssignmentService.getAllEvaluateAssignments(data, pageable).subscribe(
      res => {
        res.body.forEach(item => item.channelName = this.getChannelName(Number(item.channelId)));
        this.assignList = res.body;
        this.totalRecord = Number(res.headers.get('X-Total-Count'));
      }
    );
  }

  getData(): void {
    this.pageIndex = 0;
    this.endDate = new Date();
    this.startDate = this.addDay(new Date(), -7);
    this.getAssignList(this.pageIndex, this.pageSize);
  }

  onSearch(): void {
    if (this.beforeSearch()) {
      this.pageIndex = 0;
      this.getAssignList(this.pageIndex, this.pageSize);
    }
  }

  beforeSearch(): boolean {
    if (!this.channelId) {
      this.toastr.error(this.translate.instant('evaluate-assignment.validate.search-required', {field: this.translate.instant('evaluate-assignment.channel')}));
      document.getElementById('channelId').focus();
      return false;
    }
    if (!this.startDate) {
      this.toastr.error(this.translate.instant('evaluate-assignment.validate.search-required', {field: this.translate.instant('evaluate-assignment.start-date')}));
      document.getElementById('startDate').focus();
      return false;
    }
    if (!this.endDate) {
      this.toastr.error(this.translate.instant('evaluate-assignment.validate.search-required', {field: this.translate.instant('evaluate-assignment.end-date')}));
      document.getElementById('endDate').focus();
      return false;
    }
    if (this.endDate < this.startDate) {
      this.toastr.error(this.translate.instant('evaluate-assignment.validate.search-date-invalid'));
      document.getElementById('endDate').focus();
      return false;
    }
    return true;
  }

  getEvaluatorList(): void {
    this.evaluateAssignmentService.getUsersByType(2).subscribe(res => this.evaluaterList = res);
  }

  getUserList(): void {
    this.evaluateAssignmentService.getUsersByType(1).subscribe(res => this.userList = res);
  }

  openEvaluateAssignmentDialog(): void {
    const data = {
      userList: this.userList,
      evaluaterList: this.evaluaterList
    };
    const dialogRef = this.dialog.open(EvaluateAssignmentComponent, {
      width: '90vw',
      disableClose: true,
      hasBackdrop: true,
      data
    });
    dialogRef.afterClosed().subscribe(async result => {
      if (result === undefined || !result) {
        return;
      }
      if (result.event !== 'cancel') {
        await this.doEvaluateAssignment(result);
        this.getData();
      }
    });
  }

  async doEvaluateAssignment(sendData: any): Promise<void> {
    await this.evaluateAssignmentService.createEvaluateAssignment(sendData).toPromise().then(_ => this.toastr.success(this.translate.instant('evaluate-assignment.response.success-assign')));
  }

  async getDetail(id: any): Promise<void> {
    await this.evaluateAssignmentService.getEvaluateAssignmentsDetail(id).subscribe(res => {
      const userIds = [];
      res.evaluateAssignmentDetailsDTO.forEach(item => userIds.push(item.userId));
      this.userList.forEach(user => {
        if (userIds.includes(user.id)) {
          user.checked = true;
          user.disabled = false;
        } else {
          user.disabled = true;
          user.checked = false;
        }
      });
    });
  }

  async openEvaluateAssignmentUpdateDialog(data: any): Promise<void> {
    await this.getDetail(data.id);
    data.userList = this.userList;
    data.evaluaterList = this.evaluaterList;
    const dialogRef = this.dialog.open(EvaluateAssignmentComponent, {
      width: '90vw',
      disableClose: true,
      hasBackdrop: true,
      data
    });
    dialogRef.afterClosed().subscribe(async result => {
      if (result === undefined || !result) {
        return;
      }
      if (result.event !== 'cancel') {
        await this.doEvaluateAssignmentUpdate(result);
        this.getData();
      }
    });
  }

  async doEvaluateAssignmentUpdate(sendData: any): Promise<void> {
    await this.evaluateAssignmentService.updateEvaluateAssignment(sendData).toPromise().then(_ => this.toastr.success(this.translate.instant('evaluate-assignment.response.success-update')));
  }

  openEvaluateAssignmentDeleteDialog(id: any): void {
    const dialogRef = this.dialog.open(EvaluateAssignmentDeleteComponent, {
      width: '40vw',
      disableClose: true,
      hasBackdrop: true
    });
    dialogRef.afterClosed().subscribe(async result => {
      if (result === undefined || !result) {
        return;
      }
      if (result.event !== 'cancel') {
        await this.doEvaluateAssignmentDelete(id);
        this.getData();
      }
    });
  }

  async doEvaluateAssignmentDelete(id: any): Promise<void> {
    await this.evaluateAssignmentService.deleteEvaluateAssignment(id).toPromise().then(_ => this.toastr.success(this.translate.instant('evaluate-assignment.response.success-delete')));
  }

  onPageChange(event) {
    this.pageIndex = event.pageIndex;
    this.pageSize = event.pageSize;
    this.getAssignList(this.pageIndex, this.pageSize);
  }

  getChannelName(channelId: any): string {
    if (channelId === 1) {
      return this.translate.instant('evaluate-assignment.in-call');
    } else {
      return this.translate.instant('evaluate-assignment.out-call');
    }
  }

  addDay(date: any, day: any): Date {
    const tmp = new Date(date);
    tmp.setTime(tmp.getTime() + (day * 24 * 60 * 60 * 1000));
    return tmp;
  }
}

@Component({
  selector: 'app-evaluate-assignment-delete',
  templateUrl: './evaluate-assignment-delete.html'
})
export class EvaluateAssignmentDeleteComponent {

  constructor(
    private translate: TranslateService,
    @Inject(MAT_DIALOG_DATA) private data: any,
    private dialogRef: MatDialogRef<EvaluateAssignmentDeleteComponent>) {
  }

  onConfirmClick(): void {
    this.dialogRef.close(true);
  }
}
