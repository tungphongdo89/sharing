import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EvaluateAssignmentListComponent } from './evaluate-assignment-list.component';

describe('EvaluateAssignmentComponent', () => {
  let component: EvaluateAssignmentListComponent;
  let fixture: ComponentFixture<EvaluateAssignmentListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EvaluateAssignmentListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EvaluateAssignmentListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
