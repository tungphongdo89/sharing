import {Component, Inject, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {MtxGridColumn} from '@ng-matero/extensions';
import {TranslateService} from '@ngx-translate/core';
import {EvaluateAssignmentService} from '@core/services/evaluate-assignment/evaluate-assignment.service';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-evaluate-assignment',
  templateUrl: './evaluate-assignment.component.html',
  styleUrls: ['./evaluate-assignment.component.scss']
})
export class EvaluateAssignmentComponent implements OnInit {

  formSave: FormGroup;

  // Bảng
  isLoading: boolean;
  pageSize: number;
  pageIndex: number;
  pageSizeList = [5, 10, 20, 50];
  columns: MtxGridColumn[] = [];

  // Danh sách tổng đài viên
  userList: any = [];

  // Form input data
  channelList: any = [
    {
      label: this.translate.instant('evaluate-assignment.in-call'),
      value: 1
    }, {
      label: this.translate.instant('evaluate-assignment.out-call'),
      value: 2
    }
  ];
  evaluaterList: any = [];
  fromDate: Date;
  toDate: Date;

  constructor(
    public dialogRef: MatDialogRef<EvaluateAssignmentComponent>,
    private translate: TranslateService,
    private evaluateAssignmentService: EvaluateAssignmentService,
    private toastr: ToastrService,
    @Inject(MAT_DIALOG_DATA) public input: any
  ) {
    this.createTable();
    this.evaluaterList = input.evaluaterList;
    this.userList = input.userList;
    this.createForm();
    if (this.input.id) {
      this.setValue();
    }
  }

  ngOnInit(): void {
  }

  onReturnData(): void {
    this.formSave.value.lstUserDTO = this.userList.filter(user => user.checked === true);
    if (this.input.id) {
      this.formSave.value.id = this.input.id;
    }
    if (this.beforeSave()) {
      if (this.formSave.value.totalUserId === this.formSave.value.lstUserDTO.length && this.formSave.valid) {
        this.dialogRef.close(this.formSave.value);
      } else {
        this.toastr.error(this.translate.instant('evaluate-assignment.validate.MSG0044'));
      }
    }
  }

  createForm(): void {
    this.formSave = new FormGroup({
      evaluaterId: new FormControl(null, [Validators.required]),
      channelId: new FormControl(null, [Validators.required]),
      totalCall: new FormControl(null, [Validators.required, Validators.min(1)]),
      totalUserId: new FormControl(null, [Validators.required, Validators.min(1), Validators.max(this.userList.length)]),
      startDate: new FormControl(null, [Validators.required]),
      endDate: new FormControl(null, [Validators.required]),
    });
  }

  setValue(): void {
    this.formSave.patchValue(
      {
        evaluaterId: this.input.evaluaterId,
        channelId: Number(this.input.channelId),
        totalCall: this.input.totalCall,
        totalUserId: this.input.totalUserId,
        startDate: new Date(this.input.startDate),
        endDate: new Date(this.input.endDate)
      }
    );
    this.formSave.setControl('evaluaterId', new FormControl({
      value: this.input.evaluaterId,
      disabled: true
    }, [Validators.required]));
    this.formSave.setControl('channelId', new FormControl({
      value: Number(this.input.channelId),
      disabled: true
    }, [Validators.required]));
  }

  createTable(): void {
    this.columns = [
      {i18n: 'evaluate-assignment.user-list.username', field: 'login'},
      {i18n: 'evaluate-assignment.user-list.full-name', field: 'fullName'},
      {i18n: 'evaluate-assignment.user-list.number', field: 'number'}
    ];
  }

  getEvaluatorList(): void {
    this.evaluateAssignmentService.getUsersByType(2).subscribe(res => this.evaluaterList = res);
  }

  validateStartDate(): void {
    if (new Date(this.formSave.value.startDate) > new Date(this.formSave.value.endDate) && this.formSave.value.startDate && this.formSave.value.endDate) {
      this.formSave.get('startDate').setErrors({less: true, error: true});
    } else if (
      new Date(this.formSave.value.startDate) <= new Date(this.formSave.value.endDate) &&
      this.formSave.value.startDate &&
      this.formSave.value.endDate
    ) {
      this.formSave.get('startDate').setErrors(null);
    }
    if (!this.formSave.value.endDate) {
      this.formSave.get('endDate').setErrors({required: true, error: true});
    }
  }

  validateEndDate(): void {
    if (new Date(this.formSave.value.startDate) > new Date(this.formSave.value.endDate) && this.formSave.value.startDate && this.formSave.value.endDate) {
      this.formSave.get('endDate').setErrors({less: true, error: true});
    } else if (
      new Date(this.formSave.value.startDate) <= new Date(this.formSave.value.endDate) &&
      this.formSave.value.startDate &&
      this.formSave.value.endDate
    ) {
      this.formSave.get('endDate').setErrors(null);
    }
    if (!this.formSave.value.startDate) {
      this.formSave.get('startDate').setErrors({required: true, error: true});
    }
  }

  checkIsInteger(): void {
    if (this.formSave.value.totalCall === null || this.formSave.value.totalCall === undefined) {
      this.formSave.get('totalCall').setErrors({required: true, error: true});
    } else if (!Number.isInteger(this.formSave.value.totalCall) || Number(this.formSave.value.totalCall) <= 0) {
      this.formSave.get('totalCall').setErrors({notInt: true, error: true});
    } else if (this.formSave.value.totalCall && Number.isInteger(this.formSave.value.totalCall) && Number(this.formSave.value.totalCall) > 0) {
      this.formSave.get('totalCall').setErrors(null);
    }
  }

  onCheckedReachLimit(): void {
    const tmpList = this.userList.filter(item => item.checked);
    if (tmpList && tmpList.length >= this.formSave.value.totalUserId) {
      this.userList.forEach(item => {
        if (!item.checked) {
          item.disabled = true;
        }
      });
    } else {
      this.userList.forEach(item => {
        if (!item.checked) {
          item.disabled = false;
        }
      });
    }
  }

  onChangUserQuantity(): void {
    if (this.formSave.value.totalUserId === null || this.formSave.value.totalUserId === undefined) {
      this.formSave.get('totalUserId').setErrors({required: true, error: true});
    } else if (!Number.isInteger(this.formSave.value.totalUserId) || Number(this.formSave.value.totalUserId) <= 0) {
      this.formSave.get('totalUserId').setErrors({notInt: true, error: true});
    } else if (this.formSave.value.totalUserId && Number.isInteger(this.formSave.value.totalUserId) && Number(this.formSave.value.totalUserId) > 0) {
      this.formSave.get('totalUserId').setErrors(null);
    }

    this.userList.forEach(item => {
      item.checked = false;
      item.disabled = false;
    });
  }

  beforeSave(): boolean {
    if (!this.formSave.value.evaluaterId) {
      document.getElementById('evaluaterId').focus();
      this.formSave.get('evaluaterId').setErrors({required: true, error: true});
      return false;
    }
    if (!this.formSave.value.channelId) {
      document.getElementById('channelId').focus();
      this.formSave.get('channelId').setErrors({required: true, error: true});
      return false;
    }
    if (!this.formSave.value.totalCall) {
      document.getElementById('totalCall').focus();
      this.formSave.get('totalCall').setErrors({required: true, error: true});
      return false;
    }
    if (!this.formSave.value.totalUserId) {
      document.getElementById('totalUserId').focus();
      this.formSave.get('totalUserId').setErrors({required: true, error: true});
      return false;
    }
    if (!this.formSave.value.startDate) {
      document.getElementById('startDate').focus();
      this.formSave.get('startDate').setErrors({required: true, error: true});
      return false;
    }
    if (!this.formSave.value.endDate) {
      document.getElementById('endDate').focus();
      this.formSave.get('endDate').setErrors({required: true, error: true});
      return false;
    }
    return true;
  }
}
