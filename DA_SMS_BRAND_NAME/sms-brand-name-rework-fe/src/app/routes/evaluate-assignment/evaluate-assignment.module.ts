import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EvaluateAssignmentRoutingModule } from './evaluate-assignment-routing.module';
import {
  EvaluateAssignmentDeleteComponent,
  EvaluateAssignmentListComponent
} from './evaluate-assignment-list/evaluate-assignment-list.component';
import { SharedModule } from '@shared';
import { EvaluateAssignmentComponent } from './evaluate-assignment/evaluate-assignment.component';


@NgModule({
  declarations: [EvaluateAssignmentListComponent, EvaluateAssignmentComponent, EvaluateAssignmentDeleteComponent],
  imports: [
    CommonModule,
    EvaluateAssignmentRoutingModule,
    SharedModule
  ]
})
export class EvaluateAssignmentModule { }
