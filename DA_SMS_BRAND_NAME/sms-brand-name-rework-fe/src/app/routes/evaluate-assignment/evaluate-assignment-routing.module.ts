import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {EvaluateAssignmentListComponent} from '@app/routes/evaluate-assignment/evaluate-assignment-list/evaluate-assignment-list.component';


const routes: Routes = [
  {
    path: '',
    component: EvaluateAssignmentListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EvaluateAssignmentRoutingModule { }
