import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {MtxDialog, MtxGridColumn} from '@ng-matero/extensions';
import {ToastrService} from 'ngx-toastr';
import {TranslateService} from '@ngx-translate/core';
import {CriteriaGroupService} from '@core/services/criteria-group/criteria-group.service';
import {CriteriaGroupModule} from '@app/routes/criteria-group/criteria-group.module';
import {CriteriaGroupModel} from '@core/models/criteria-group-model';
import {PopupDeleteComponent} from '@app/routes/directory-management/popup-delete/popup-delete.component';
import {MatDialog} from '@angular/material/dialog';
import {BaseComponent} from '@shared/components/base-component/base-component.component';
import {RESOURCE} from '@core';
import {ActivatedRoute, Router} from '@angular/router';
import {ConfirmDialogComponent, ConfirmDialogModel} from '@shared/components/confirm-dialog/confirm-dialog.component';
import {Constant} from '@shared/Constant';

@Component({
  selector: 'app-criteria-group',
  templateUrl: './criteria-group.component.html',
  styleUrls: ['./criteria-group.component.scss']
})
export class CriteriaGroupComponent extends BaseComponent implements OnInit {

  formSearch: FormGroup;
  criteriaGroupForm: FormGroup;
  data: any = [];
  searchText: any = '';
  columns: MtxGridColumn[] = [];
  pageSizeList = [10, 50, 100];
  isLoading = false;
  offset: any = 0;
  pagesize = 10;
  total: any = 0;
  buttonTitle: string;
  buttonTitle2: string;
  backUpDatasource = [];
  filterInput = new FormControl(null);

  constructor(
    private fb: FormBuilder,
    private criteriaService: CriteriaGroupService,
    private toast: ToastrService,
    public dialog2: MtxDialog,
    public route: ActivatedRoute,
    protected translateService: TranslateService
  ) {
    super(route, criteriaService, RESOURCE.CUSTOMER, toast, translateService, dialog2);
  }

  ngOnInit(): void {


    this.getAllCriteriaGroup(true);
    this.criteriaGroupForm = this.fb.group({
      id: new FormControl(null),
      name: new FormControl(null, [Validators.required]),
      scores: new FormControl(null, [Validators.required, Validators.min(1), Validators.max(9999)]),
      createDatetime: new FormControl(null),
      createUser: new FormControl(null),
      updateDatetime: new FormControl(null),
      updateUser: new FormControl(null),
    });
    this.columns = [
      {i18n: 'criteria_group.no', field: 'index', width: '70px',},
      {i18n: 'criteria_group.name_criteria', field: 'name'},
      {i18n: 'criteria_group.score_criteria', field: 'scores'},
      {
        i18n: 'common.action',
        field: 'options',
        width: '120px',
        pinned: 'right',
        right: '0px',
        type: 'button',
        buttons: [
          {
            type: 'icon',
            text: 'edit',
            icon: 'edit',
            click: record => this.updateCriteriaGroup(record),
          },
          {
            type: 'icon',
            text: 'delete',
            icon: 'delete',
            click: record => this.deleteCriteriaGroup(record),
          }
        ]
      }
    ];
    this.checkButton();
    this.onFilterTable();
  }

  checkButton() {
    console.log('id: ', this.criteriaGroupForm.get('id').value);
    if (this.criteriaGroupForm.get('id').value !== null) {
      this.buttonTitle = this.translateService.instant('criteria_group.btn_update');
      this.buttonTitle2 = this.translateService.instant('criteria_group.btn_back');
    } else {
      this.buttonTitle = this.translateService.instant('criteria_group.btn_create');
      this.buttonTitle2 = this.translateService.instant('criteria_group.btn_back');
    }
    console.log('buton: ', this.buttonTitle);
  }

  updateCriteriaGroup(criteriaModel: CriteriaGroupModel){
    console.log('logggg ', criteriaModel);
    this.criteriaGroupForm.patchValue(criteriaModel);
    this.checkButton();
  }


  getAllCriteriaGroup(reset?: boolean) {
    if (reset) {
      this.offset = 0;
      this.pagesize = 10;
    }
    // const data = {...this.criteriaGroupForm.value, queryType: 1};
    this.criteriaService.getAllCriteriaGroup({size: this.pagesize, page: this.offset}).subscribe(res => {
      this.dataModel.dataSource = res.body;
      this.backUpDatasource = res.body;
      this.data = res.body;
      console.log(res.headers.get('X-Total-Count'));
      this.total = Number(res.headers.get('X-Total-Count'));
    });
  }

  btnBack(){
    this.criteriaGroupForm.reset();
    this.checkButton();
  }

  deleteCriteriaGroup(criteriaGroupDele: CriteriaGroupModel){
    const title = this.translateService.instant('common.confirm.title.delete');
    const content = this.translateService.instant('common.MSG0017');
    this.criteriaGroupForm.patchValue(criteriaGroupDele);
    const dialogContent: ConfirmDialogModel = new ConfirmDialogModel(title, content);
    this.dialog.originalOpen(ConfirmDialogComponent, {
      width: '25vw',
      disableClose: true,
      hasBackdrop: true,
      data: dialogContent
    }).afterClosed().subscribe((next: any) => {
      if (next.data === Constant.DIALOG_CONFIRM) {
        this.criteriaService.deleteCriteriaGroup(this.criteriaGroupForm.value).subscribe(rs => {
          if (rs.body.message === 'ok') {
            this.toast.success(this.translateService.instant('criteria_group.msg_delete_success'));
            this.criteriaGroupForm.reset();
            this.getAllCriteriaGroup(false);
          } else {
            this.toast.error(this.translateService.instant('criteria_group.msg_delete_error'));
          }
        });
      }else {
        this.criteriaGroupForm.reset();
      }
    });

  }

  addCriteriaGroup() {
    if (this.criteriaGroupForm.get('id').value){
      this.criteriaService.updateCriteriaGroup(this.criteriaGroupForm.value).subscribe(res => {
        console.log('update ', res.body.message);
        if (res.body.message === 'ok'){
          this.toast.success(this.translateService.instant('criteria_group.meg_update_success'));
          this.getAllCriteriaGroup(true);
          this.criteriaGroupForm.reset();
          this.checkButton();
        } else {
            this.toast.error(this.translateService.instant('criteria_group.msg_exist'));
        }
      });
    } else {
      this.criteriaService.addCriteriaGroup(this.criteriaGroupForm.value).subscribe(res => {
        console.log('create ', res);
        if (res.body.message === 'ok'){
          this.toast.success(this.translateService.instant('criteria_group.msg_create_success'));
          this.getAllCriteriaGroup(true);
          this.criteriaGroupForm.reset();
        } else {
            this.toast.error(this.translateService.instant('criteria_group.msg_exist'));
        }
      });
    }
  }

  onFilterTable() {
    this.filterInput.valueChanges.subscribe(next => {
      this.dataModel.dataSource = this.backUpDatasource.filter(value =>
        value.name?.toLowerCase().includes(next?.toLowerCase())
        || value.scores?.toString().toLowerCase().includes(next?.toLowerCase())
      );
      console.log('data model ', this.dataModel);
    });
  }


  onPageChange(event) {
    this.offset = event.pageIndex;
    this.pagesize = event.pageSize;
    this.getAllCriteriaGroup();
  }
}
