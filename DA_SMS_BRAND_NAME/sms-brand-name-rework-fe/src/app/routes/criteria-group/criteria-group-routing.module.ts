import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {CriteriaGroupComponent} from '@app/routes/criteria-group/criteria-group/criteria-group.component';


const routes: Routes = [
  {
    path: '',
    component: CriteriaGroupComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CriteriaGroupRoutingModule { }
