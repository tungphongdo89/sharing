import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CriteriaGroupRoutingModule } from './criteria-group-routing.module';
import { CriteriaGroupComponent } from './criteria-group/criteria-group.component';
import {SharedModule} from '@shared';
import {Ng2SearchPipeModule} from 'ng2-search-filter';


@NgModule({
  declarations: [CriteriaGroupComponent],
    imports: [
        CommonModule,
        SharedModule,
        CriteriaGroupRoutingModule,
        Ng2SearchPipeModule
    ]
})
export class CriteriaGroupModule { }
