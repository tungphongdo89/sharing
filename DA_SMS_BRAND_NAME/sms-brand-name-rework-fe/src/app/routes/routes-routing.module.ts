import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {environment} from '@env/environment';
import { AdminLayoutComponent } from '@theme/admin-layout/admin-layout.component';
import { DashboardComponent } from '@app/routes/dashboard/dashboard.component';
import { UserRouteAccessService } from '@core/auth/user-route-access.service';
import { LoginComponent } from '@core/login/login.component';
import { CpBrandManagementComponent } from '@app/routes/cp-management/cp-brand-management/cp-brand-management.component';
import { EventManagementModule } from '@app/routes/event-management/event-management.module';
import { DirectoryManagementModule } from '@app/routes/directory-management/directory-management.module';
import { ConfigurationModule } from '@app/routes/configuration-authorization/authorization-information/configuration.module';
import { ReportComplaintDetailComponent } from '@app/routes/report-complaint-detail/report-complaint-detail.component';
import { CampaignBlacklistModule } from '@app/routes/campaign-blacklist/campaign-blacklist.module';
import { ForgetPasswordComponent } from '@app/core/login/forget-password/forget-password.component';
import { ProfileComponent } from '@app/theme/header/profile/profile.component';
import { ChangePasswordComponent } from '@app/theme/header/change-password/change-password.component';
import {GuardMenuGuard} from '@core/guards/guard-menu.guard';

const routes: Routes = [
  {
    path: '',
    component: AdminLayoutComponent,
    canActivate: [UserRouteAccessService],
    children: [
      {path: '', redirectTo: 'dashboard', pathMatch: 'full'},
      {
        path: 'dashboard',
        component: DashboardComponent,
        data: {title: 'menu.dashboard'},
      },
      {
        path: 'cp-management',
        component: CpBrandManagementComponent,
      },
      {
        path: 'profile',
        component: ProfileComponent,
      },
      {
        path: 'change-password',
        component: ChangePasswordComponent,
      },
      {
        path: 'config-authorziration',
        loadChildren: () => import('./configuration-authorization/authorization-information/configuration.module').then(m => ConfigurationModule),
      },
      {
        path: 'event-receive',
        loadChildren: () => import('./event-management/event-management.module').then(m => EventManagementModule),
        // canActivate: [GuardMenuGuard],
      },
      {
        path: 'general-directory-management',
        loadChildren: () => import('./directory-management/directory-management.module').then(m => DirectoryManagementModule),
        data: { title: 'menu.general-directory-management' },
      },
      {
        path: 'campaign-blacklist',
        loadChildren: () => import('./campaign-blacklist/campaign-blacklist.module').then(m => CampaignBlacklistModule),
        data: {title: 'menu.campaign-blacklist'},
      },
      {
        path: 'kms-management',
        loadChildren: () => import('./kms-management/kms-management.module').then(m => m.KmsManagementModule),
      },
      {
        path: 'callout-campaign-management',
        loadChildren: () => import('./callout-campaign-management/callout-campaign-management.module').then(m => m.CalloutCampaignManagementModule),
        data: {
          title: 'menu.callout-campaign',
        },
      },
      {
        path: 'kms-management',
        loadChildren: () => import('./kms-management/kms-management.module').then(m => m.KmsManagementModule),
      },
      {
        path: 'campain-script',
        loadChildren: () => import('./campain-script/campain-script.module').then(m => m.CampainScriptModule),
      },
      {
        path: 'campaign',
        loadChildren: () => import('./campaign/campaign.module').then(m => m.CampaignModule),
      },
      {
        path: 'campaign-email-blacklist',
        loadChildren: () => import('./campaign-email-black-list/email-blacklist.module').then(m => m.EmailBlacklistModule),
        data: {title: 'menu.campaign-email-blacklist'},
      },
      {
        path: 'campaign-email-resource',
        loadChildren: () => import('./campaign-email-resource/campaign-email-resource.module').then(m => m.CampaignEmailResourceModule),
        data: {title: 'menu.campaign-email-resource'},
      },
      {
        path: 'campaign-sms-resource',
        loadChildren: () => import('./campaign-sms-resource/campaign-sms-resource.module').then(m => m.CampaignSmsResourceModule),
        data: {title: 'menu.campaign-sms-resource'},
      },
      {
        path: 'send-email-one',
        loadChildren: () => import('./send-email-one/send-email-one.module').then(m => m.SendEmailOneModule),
        data: {title: 'menu.send-email-one'},
      },
      {
        path: 'email-config',
        loadChildren: () => import('./email-config/email-config.module').then(m => m.EmailConfigModule),
        data: {title: 'menu.email-config'},
      },
      {
        path: 'kms-management',
        loadChildren: () => import('./kms-management/kms-management.module').then(m => m.KmsManagementModule),
      },
      {
        path: 'search-ticket', loadChildren: () => import('./cp-tiket/cp-tiket.module').then(m => m.CpTiketModule),
        data: {title: 'menu.search-ticket'},
      },
      {
        path: 'report-process-time',
        loadChildren: () => import('./report-process-time/report-process-time.module').then(m => m.ReportProcessTimeModule),
        data: {title: 'menu.report-process-time'},
      },
      {
        path: 'report-contact-center',
        loadChildren: () => import('./report-contact-center/report-contact-center.module').then(m => m.ReportContactCenterModule),
        data: {title: 'menu.report-contact-center'},
      },
      {
        path: 'report-complaint',
        loadChildren: () => import('./report-complaint/report-complaint.module').then(m => m.ReportComplaintModule),
        data: {title: 'menu.report-complaint'},
      },
      {
        path: 'report-complaint-detail',
        component: ReportComplaintDetailComponent,
        data: {title: 'menu.report-complaint-detail'},
      },
      {
        path: 'campaign-resource',
        loadChildren: () => import('./campaign-resource/campaign-resource.module').then(m => m.CampaignResourceModule),
        data: {title: 'menu.campaign-resource'},
      },
      {
        path: 'campaign-sms-black-list',
        loadChildren: () => import('./campaign-sms-black-list/campaign-sms-black-list.module').then(m => m.CampaignSmsBlackListModule),
        data: {title: 'menu.campaign-sms-black-list'},
      },
      {
        path: 'campaign-email-marketing',
        loadChildren: () => import('./campaign-email-marketing/campaign-email-marketing.module').then(m => m.CampaignEmailMarketingModule),
        data: {title: 'menu.campaign-email-marketing'},
      },
      {
        path: 'record-call-results',
        loadChildren: () => import('./record-call-results/record-call-results.module').then(m => m.RecordCallResultsModule),
        data: {title: 'menu.record-call-results'},
      },
      {
        path: 'campaign-sms-marketing',
        loadChildren: () => import('./campaign-sms-marketing/campaign-sms-marketing.module').then(m => m.CampaignSmsMarketingModule),
        data: {title: 'menu.campaign-sms-marketing'},
      },
      {
        path: 'config-auto-email',
        loadChildren: () => import('./config-auto-email/config-auto-email.module').then(m => m.ConfigAutoEmailModule),
        data: {title: 'menu.config-auto-email'},
      },
      {
        path: 'config-auto-sms',
        loadChildren: () => import('./config-auto-sms/config-auto-sms.module').then(m => m.ConfigAutoSmsModule),
        data: {title: 'menu.config-auto-sms'},
      },
      {
        path: 'report-productivity-callout-campaign',
        loadChildren: () => import('./report-productivity-callout-campaign/report-productivity-callout-campaign.module').then(m => m.ReportProductivityCalloutCampaignModule),
        data: {title: 'menu.report-productivity-callout-campaign'},
      },
      {
        path: 'report-campaign-sms-marketing',
        loadChildren: () => import('./report-campaign-sms-marketing/report-campaign-sms-marketing.module').then(m => m.ReportCampaignSmsMarketingModule),
        data: {title: 'menu.report-campaign-sms-marketing'},
      },
      {
        path: 'report-sent-sms-marketing',
        loadChildren: () => import('./report-sent-sms-marketing/report-sent-sms-marketing.module').then(m => m.ReportSentSmsMarketingModule),
        data: {title: 'menu.report-sent-sms-marketing'},
      },
      {
        path: 'report-campaign-email-marketing',
        loadChildren: () => import('./report-campaign-email-marketing/report-campaign-email-marketing.module').then(m => m.ReportCampaignEmailMarketingModule),
        data: {title: 'menu.report-campaign-email-marketing'},
      },
      {
        path: 'report-sent-email-marketing',
        loadChildren: () => import('./report-sent-email-marketing/report-sent-email-marketing.module').then(m => m.ReportSentEmailMarketingModule),
        data: {title: 'menu.report-sent-email-marketing'},
      },
      {
        path: 'send-sms-one',
        loadChildren: () => import('./send-sms-one/send-sms-one.module').then(m => m.SendSmsOneModule),
        data: {title: 'menu.send-sms-one'},
      },
      {
        path: 'campaign-group-user',
        loadChildren: () => import('./campaign-group-user/campaign-group-user.module').then(m => m.CampaignGroupUserModule),
        data: {title: 'menu.campaign-group-user'}
      },
      {
        path: 'campaign-perform-information',
        loadChildren: () => import('./campaign-perform-information/campaign-perform-information.module').then(m => m.CampaignPerformInformationModule),
        data: {title: 'menu.campaign-perform-information'},
      },
      {
        path: 'call-information-for-evaluation',
        loadChildren: () => import('./call-information-for-evaluation/call-information-for-evaluation.module').then(m => m.CallInformationForEvaluationModule),
        data: {title: 'menu.call-information-for-evaluation'},
      },
      {
        path: 'users',
        loadChildren: () => import('./user/user.module').then(m => m.UserModule),
        data: {title: 'menu.users'},
      },
      {
        path: 'criteria-group',
        loadChildren: () => import('./criteria-group/criteria-group.module').then(m => m.CriteriaGroupModule),
        data: {title: 'menu.criteria-group'},
      },
      {
        path: 'criteria',
        loadChildren: () => import('./criteria/criteria.module').then(m => m.CriteriaModule),
        data: {title: 'menu.criteria'},
      },
      {
        path: 'criteria-rating',
        loadChildren: () => import('./criteria-rating/criteria-rating.module').then(m => m.CriteriaRatingModule),
        data: {title: 'menu.criteria-rating'},
      },
      {
        path: 'search-work-list',
        loadChildren: () => import('./search-work-list/search-work-list.module').then(m => m.SearchWorkListModule),
        data: {title: 'menu.search-work-list'},
      },
      {
        path: 'evaluate-assignment',
        loadChildren: () => import('./evaluate-assignment/evaluate-assignment.module').then(m => m.EvaluateAssignmentModule),
        data: {title: 'menu.campaign-group-user'},
      },
      {
        path: 'evaluate',
        loadChildren: () => import('./evaluate-call/evaluate-call.module').then(m => m.EvaluateCallModule)
      },
      {
        path: 'evaluate-list',
        loadChildren: () => import('./evaluate-list/evaluate-list.module').then(m => m.EvaluateListModule),
        data: {title: 'menu.evaluate-list'},
      },
      {
        path: 'report-evaluate-score',
        loadChildren: () => import('./report-evaluate-score/report-evaluate-score.module').then(m => m.ReportEvaluateScoreModule),
        data: {title: 'menu.report-evaluate-score'},
      },
      {
        path: 'report-evaluate-rating',
        loadChildren: () => import('./report-evaluate-rating/report-evaluate-rating.module').then(m => m.ReportEvaluateRatingModule),
        data: {title: 'menu.report-evaluate-rating'},
      },
      {
        path: 'report-evaluate-criteria',
        loadChildren: () => import('./report-evaluate-criteria/report-evaluate-criteria.module').then(m => m.ReportEvaluateCriteriaModule),
        data: {title: 'menu.report-evaluate-criteria'},
      },
      {
        path: 'roles-management',
        loadChildren: () => import('./roles-management/roles-management.module').then(m => m.RolesManagementModule),
        data: {title: 'menu.roles-management'}
      },
      {
        path: 'group-call-out',
        loadChildren: () => import('./group-call-out/group-call-out.module').then(m => m.GroupCallOutModule),
        data: {title: 'menu.group-call-out'},
      },
      { path: 'report-render-call',
        loadChildren: () => import('./report-render-call/report-render-call.module').then(m => m.ReportRenderCallModule),
        data: {title: 'menu.report-render-call'},
      },
      { path: 'report-render-detail-output-call',
        loadChildren: () => import('./report-render-detail-output-call/report-render-detail-output-call.module').then(m => m.ReportRenderDetailOutputCallModule),
        data: {title: 'menu.report-render-detail-output-call'},
      },
      { path: 'report-render-statistics-question',
        loadChildren: () => import('./report-render-statistics-question/report-render-statistics-question.module').then(m => m.ReportRenderStatisticsQuestionModule),
        data: {title: 'menu.report-render-statistics-question'},
      },
    ],
  },
  {
    path: 'login',
    component: LoginComponent,
  },
  {
    path: 'forget-password',
    component: ForgetPasswordComponent,
  },
  { path: '**', redirectTo: 'dashboard' },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      useHash: environment.useHash,
    }),
  ],
  exports: [RouterModule],
})
export class RoutesRoutingModule {
}
