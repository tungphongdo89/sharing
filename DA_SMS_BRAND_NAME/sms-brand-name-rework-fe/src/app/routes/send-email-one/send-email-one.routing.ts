import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {SendEmailOneComponent} from '@app/routes/send-email-one/send-email-one/send-email-one.component';

const routes: Routes = [
  {
    path: '',
    component: SendEmailOneComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SendEmailOneRouting {
}
