import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SendEmailOneComponent} from './send-email-one/send-email-one.component';
import {SendEmailOneRouting} from '@app/routes/send-email-one/send-email-one.routing';
import {SharedModule} from '@shared';


@NgModule({
  declarations: [SendEmailOneComponent],
  imports: [
    CommonModule,
    SharedModule,
    SendEmailOneRouting
  ]
})
export class SendEmailOneModule {
}
