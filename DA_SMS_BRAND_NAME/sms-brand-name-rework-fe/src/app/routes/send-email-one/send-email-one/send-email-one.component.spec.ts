import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SendEmailOneComponent } from './send-email-one.component';

describe('SendEmailOneComponent', () => {
  let component: SendEmailOneComponent;
  let fixture: ComponentFixture<SendEmailOneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SendEmailOneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SendEmailOneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
