import {Component, OnInit} from '@angular/core';
import {SendEmailOneService} from '@core/services/send-email-one/send-email-one.service';
import {TranslateService} from '@ngx-translate/core';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ToastrService} from 'ngx-toastr';
import {CampaignEmailResourceService} from '@core/services/campaign-email-resource/campaign-email-resource.service';
import {COMMOM_CONFIG} from '@env/environment.uat';

@Component({
  selector: 'app-send-email-one',
  templateUrl: './send-email-one.component.html',
  styleUrls: ['./send-email-one.component.scss']
})
export class SendEmailOneComponent implements OnInit {
  public editor = ClassicEditor;
  sendEmailForm: FormGroup = this.fb.group({
    title: [null, [Validators.required,Validators.maxLength(3000)]],
    email: [null, [Validators.required,Validators.pattern(COMMOM_CONFIG.EMAIL_FORMAT),Validators.maxLength(3000)]],
    ccEmail: [null,Validators.maxLength(3000)],
    bccEmail: [null,Validators.maxLength(3000)],
    content: [null, [Validators.required,Validators.maxLength(3000)]],
  });

  constructor(
    private sendEmailOneService: SendEmailOneService,
    private translate: TranslateService,
    private fb: FormBuilder,
    private toastr: ToastrService,
    private campaignEmailResourceService: CampaignEmailResourceService
  ) {
  }

  ngOnInit(): void {
  }

  sendEmail() {
    this.sendEmailForm.markAllAsTouched();
    if (this.sendEmailForm.invalid) {
      return;
    }
    const data = this.sendEmailForm.getRawValue();
    this.campaignEmailResourceService.createEmailOne(data).subscribe(res => {
      this.showNotifySuccess();
      this.sendEmailForm.reset();
    }, error => {
      console.error(error);
      this.showUnknownErr();
    });
  }

  showNotifySuccess() {
    this.toastr.success(this.translate.instant('send-sms-one.send-success'));
  }

  showUnknownErr() {
    this.toastr.error(this.translate.instant('send-sms-one.send-error'));
  }
}
