import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SearchTicketComponent } from './search-ticket/search-ticket.component';
import {SharedModule} from '@shared';
import {CpTicketRoutingModule} from '@app/routes/cp-tiket/cp-ticket-routing.module';
import { NoProcessAndProcessingTicketComponent } from './search-ticket/no-process-and-processing-ticket/no-process-and-processing-ticket.component';
import { ProcessedAndConfirmingTicketComponent } from './search-ticket/processed-and-confirming-ticket/processed-and-confirming-ticket.component';
import { FinishTicketComponent } from './search-ticket/finish-ticket/finish-ticket.component';

@NgModule({
  declarations: [SearchTicketComponent, NoProcessAndProcessingTicketComponent, ProcessedAndConfirmingTicketComponent, FinishTicketComponent],
  imports: [
    CommonModule,
    SharedModule,
    CpTicketRoutingModule
  ]
})
export class CpTiketModule { }
