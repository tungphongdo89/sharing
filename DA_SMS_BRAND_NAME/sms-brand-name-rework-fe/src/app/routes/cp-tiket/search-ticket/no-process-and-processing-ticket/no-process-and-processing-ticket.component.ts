import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MtxGridColumn} from '@ng-matero/extensions';
import {ApDomainService} from '@core/services/ap-domain/ap-domain.service';
import {DepartmentService} from '@core/services/department/department.service';
import {BussinessTypeService} from '@core/services/bussiness-type/bussiness-type.service';
import {ReportProcessTimeService} from '@core/services/report/report-process-time.service';
import {CommonService} from '@shared/services/common.service';
import {map} from 'rxjs/operators';
import {CpTicketService} from '@core/services/cp/cp-ticket.service';
import {COMMOM_CONFIG} from '@env/environment';
import {ActivatedRoute, Router} from '@angular/router';
import * as moment from 'moment';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-no-process-and-processing-ticket',
  templateUrl: './no-process-and-processing-ticket.component.html',
  styleUrls: ['./no-process-and-processing-ticket.component.scss']
})
export class NoProcessAndProcessingTicketComponent implements OnInit {
  today = new Date();
  @Output() pageChange: EventEmitter<any> = new EventEmitter<any>();
  @Input() tabType: number;
  chanelTypes: any = [];
  requestTypes: any = [];
  priorities: any = [];
  businessTypes: any = [];
  ticketRequestStatus: any = [];
  ticketStatus: any = [];
  ticketStatusCombobox:any = [];
  departments: any = [];
  offset: any = 0;
  reportDetailForm: FormGroup = this.fb.group({
    channel: null,
    phone: [null],
    emails: [null],
    priority: null,
    idCode: [null],
    receiveUser: null,
    fromDate: moment().subtract(7, 'days').toISOString(),
    toDate: moment().toISOString(),
    status: null
  });
  data: any = [];
  columns: MtxGridColumn[] = [
    {i18n: 'cp-ticket.no-process-and-processing-ticket.column.no', field: 'index'},
    {i18n: 'cp-ticket.no-process-and-processing-ticket.column.ticketCode', field: 'ticketCode'},
    {i18n: 'cp-ticket.no-process-and-processing-ticket.column.chanelType', field: 'channelName'},
    {i18n: 'cp-ticket.no-process-and-processing-ticket.column.customerName', field: 'customerName'},
    {i18n: 'cp-ticket.no-process-and-processing-ticket.column.idCode', field: 'cid'},
    {i18n: 'cp-ticket.no-process-and-processing-ticket.column.companyName', field: 'companyName'},
    {i18n: 'cp-ticket.no-process-and-processing-ticket.column.phoneNumber', field: 'phoneNumber'},
    {i18n: 'cp-ticket.no-process-and-processing-ticket.column.email', field: 'email'},
    {i18n: 'cp-ticket.no-process-and-processing-ticket.column.processStatus', field: 'ticketRequestStatus'},
    {i18n: 'cp-ticket.no-process-and-processing-ticket.column.userName', field: 'processUser'},
    {i18n: 'cp-ticket.no-process-and-processing-ticket.column.receiveDate', field: 'receiveDateTime'},
  ];
  pageSizeList = [10, 50, 100];
  pagesize = 10;
  isLoading = false;
  total: any = 0;

  constructor(private fb: FormBuilder,
              private apDomainService: ApDomainService,
              private departmentService: DepartmentService,
              private bussinessTypeService: BussinessTypeService,
              private cpTicketService: CpTicketService,
              private commonService: CommonService,
              private router: Router,
              private route: ActivatedRoute,
              private translateService: TranslateService
  ) {
  }

  ngOnInit(): void {
    this.initData();
    this.search();
  }

  initData() {
    // Kênh tiếp nhận
    this.apDomainService.getDomainByType('CHANEL_TYPE').subscribe(res => {
      this.chanelTypes = res;
    });
    // Loại yêu cầu
    this.apDomainService.getDomainByType('REQUEST_TYPE').subscribe(res => {
      this.requestTypes = res;
    });
    // Trạng thái yêu cầu
    this.apDomainService.getDomainByType('TICKET_REQUEST_STATUS').subscribe(res => {
      this.ticketRequestStatus = res;
    });
    // Trạng thái sự vụ
    this.apDomainService.getDomainByType('TICKET_STATUS').subscribe(res => {
      this.ticketStatus = res;
      this.ticketStatusCombobox = this.ticketStatus.filter((x:any) => x.code==="1" || x.code==="2");
    });
    // Phòng ban
    this.departmentService.getLstDepartment().subscribe(res => {
      this.departments = res;
    });
    // Loại nghiệp vụ
    this.bussinessTypeService.getBussinessType().subscribe(res => {
      this.businessTypes = res;
    });
  }

  search(reset?: boolean) {
    if (reset) {
      this.offset = 0;
      this.pagesize = 10;
    }
    const data = {
      ...this.reportDetailForm.value,
      queryType: 1,
      size: this.pagesize,
      page: this.offset,
      tabType: this.tabType
    };
    this.cpTicketService.query(data, {size: this.pagesize, page: this.offset}).pipe(map(res => {
      return res;
    })).subscribe(res => {
      this.data = res.body;
      this.total = Number(res.headers.get('X-Total-Count'));
    });
  }

  export() {
    const data = {...this.reportDetailForm.value, tabType: this.tabType};
    this.cpTicketService.export(data);
  }

  onPageChange(event) {
    this.offset = event.pageIndex;
    this.pagesize = event.pageSize;
    this.search();
  }

  navigate(ticketId: any) {
    if (ticketId)
      this.router.navigate(['/event-receive/', ticketId]);
  }
}

