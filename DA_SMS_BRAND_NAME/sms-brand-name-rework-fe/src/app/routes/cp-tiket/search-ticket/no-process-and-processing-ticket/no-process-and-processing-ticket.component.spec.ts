import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NoProcessAndProcessingTicketComponent } from './no-process-and-processing-ticket.component';

describe('NoProcessAndProcessingTicketComponent', () => {
  let component: NoProcessAndProcessingTicketComponent;
  let fixture: ComponentFixture<NoProcessAndProcessingTicketComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NoProcessAndProcessingTicketComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NoProcessAndProcessingTicketComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
