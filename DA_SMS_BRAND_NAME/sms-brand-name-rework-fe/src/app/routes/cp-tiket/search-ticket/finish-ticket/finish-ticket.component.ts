import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MtxGridColumn} from '@ng-matero/extensions';
import {ApDomainService} from '@core/services/ap-domain/ap-domain.service';
import {DepartmentService} from '@core/services/department/department.service';
import {BussinessTypeService} from '@core/services/bussiness-type/bussiness-type.service';
import {CpTicketService} from '@core/services/cp/cp-ticket.service';
import {CommonService} from '@shared/services/common.service';
import {map} from 'rxjs/operators';
import {COMMOM_CONFIG} from '@env/environment';
import {Router} from '@angular/router';
import * as moment from 'moment';

@Component({
  selector: 'app-finish-ticket',
  templateUrl: './finish-ticket.component.html',
  styleUrls: ['./finish-ticket.component.scss']
})
export class FinishTicketComponent implements OnInit {
  today = new Date();
  @Output() pageChange: EventEmitter<any> = new EventEmitter<any>();
  @Input() tabType: number;
  chanelTypes: any = [];
  requestTypes: any = [];
  priorities: any = [];
  businessTypes:  any = [];
  ticketRequestStatus: any = [];
  ticketStatus: any = [];
  departments: any = [];
  offset: any = 0;
  reportDetailForm: FormGroup = this.fb.group({
    channel: null,
    phone: [null],
    emails: [null],
    priority: null,
    idCode: [null],
    receiveUser: null,
    fromDate: moment().subtract(7, 'days').toISOString(),
    toDate: moment().toISOString(),
  });
  data: any = [];
  columns: MtxGridColumn[] = [
    {i18n: 'cp-ticket.finish-ticket.column.no', field: 'index'},
    {i18n: 'cp-ticket.finish-ticket.column.ticketCode', field: 'ticketCode'},
    {i18n: 'cp-ticket.finish-ticket.column.chanelType', field: 'channelName'},
    {i18n: 'cp-ticket.finish-ticket.column.customerName', field: 'customerName'},
    {i18n: 'cp-ticket.finish-ticket.column.idCode', field: 'cid'},
    {i18n: 'cp-ticket.finish-ticket.column.companyName', field: 'companyName'},
    {i18n: 'cp-ticket.finish-ticket.column.phoneNumber', field: 'phoneNumber'},
    {i18n: 'cp-ticket.finish-ticket.column.email', field: 'email'},
    {i18n: 'cp-ticket.finish-ticket.column.processStatus', field: 'processStatus'},
    {i18n: 'cp-ticket.finish-ticket.column.userName', field: 'processUser'},
    {i18n: 'cp-ticket.finish-ticket.column.receiveDateTime', field: 'receiveDateTime'},
  ];
  pageSizeList = [10, 50, 100];
  pagesize = 10;
  isLoading = false;
  total: any = 0;
  constructor(private fb: FormBuilder,
              private apDomainService: ApDomainService,
              private departmentService: DepartmentService,
              private bussinessTypeService: BussinessTypeService,
              private cpTicketService: CpTicketService,
              private commonService: CommonService,
              private router: Router
  ) { }

  ngOnInit(): void {
    this.initData();
    this.search();
  }
  initData() {
    // Kênh tiếp nhận
    this.apDomainService.getDomainByType('CHANEL_TYPE').subscribe(res => {
      this.chanelTypes = res;
    });
    // Loại yêu cầu
    this.apDomainService.getDomainByType('REQUEST_TYPE').subscribe(res => {
      this.requestTypes = res;
    });
    // Trạng thái yêu cầu
    this.apDomainService.getDomainByType('TICKET_REQUEST_STATUS').subscribe(res => {
      this.ticketRequestStatus = res;
    });
    // Trạng thái sự vụ
    this.apDomainService.getDomainByType('TICKET_STATUS').subscribe(res => {
      this.ticketStatus = res;
    });
    // Phòng ban
    this.departmentService.getLstDepartment().subscribe(res => {
      this.departments = res;
    });
    // Loại nghiệp vụ
    this.bussinessTypeService.getBussinessType().subscribe(res => {
      this.businessTypes = res;
    });
  }

  search(reset?: boolean) {
    if (reset) {
      this.offset = 0;
      this.pagesize = 10;
    }
    const data = {...this.reportDetailForm.value, queryType: 1, size: this.pagesize, page: this.offset, tabType: this.tabType};
    this.cpTicketService.query(data, { size: this.pagesize, page: this.offset}).pipe(map(res => {
      return res;
    })).subscribe(res => {
      this.data = res.body;
      this.total = Number(res.headers.get('X-Total-Count'));
    });
  }
  export() {
    const data = {...this.reportDetailForm.value, tabType: this.tabType};
    this.cpTicketService.export(data);
  }
  onPageChange(event) {
    this.offset = event.pageIndex;
    this.pagesize = event.pageSize;
    this.search();
  }
  navigate(ticketId: any) {
    if (ticketId)
      this.router.navigate(['/event-receive/' , ticketId ]);
  }
}

