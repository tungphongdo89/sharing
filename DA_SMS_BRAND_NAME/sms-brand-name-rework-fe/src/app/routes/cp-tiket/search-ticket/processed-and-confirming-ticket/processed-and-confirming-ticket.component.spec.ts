import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProcessedAndConfirmingTicketComponent } from './processed-and-confirming-ticket.component';

describe('ProcessedAndConfirmingTicketComponent', () => {
  let component: ProcessedAndConfirmingTicketComponent;
  let fixture: ComponentFixture<ProcessedAndConfirmingTicketComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProcessedAndConfirmingTicketComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProcessedAndConfirmingTicketComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
