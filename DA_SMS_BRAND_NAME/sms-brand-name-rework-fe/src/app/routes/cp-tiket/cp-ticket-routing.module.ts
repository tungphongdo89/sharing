import {Routes, RouterModule} from '@angular/router';
import {NgModule} from '@angular/core';
import {SearchTicketComponent} from '@app/routes/cp-tiket/search-ticket/search-ticket.component';

const routes: Routes = [
  {
    path: '',
    component: SearchTicketComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CpTicketRoutingModule {
}
