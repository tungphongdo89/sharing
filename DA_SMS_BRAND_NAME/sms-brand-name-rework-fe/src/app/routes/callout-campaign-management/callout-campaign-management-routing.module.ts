import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CalloutCampaignManagementComponent } from './callout-campaign-management.component';

const routes: Routes = [{ path: '', component: CalloutCampaignManagementComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CalloutCampaignManagementRoutingModule { }
