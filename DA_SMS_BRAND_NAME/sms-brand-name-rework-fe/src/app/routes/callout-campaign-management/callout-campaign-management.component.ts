import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {BaseComponent} from '@shared/components/base-component/base-component.component';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {MtxDialog} from '@ng-matero/extensions';
import {ToastrService} from 'ngx-toastr';
import {TranslateService} from '@ngx-translate/core';
import {RESOURCE} from '@core';
import {SampleCampaignFormComponent} from '@app/routes/callout-campaign-management/sample-campaign-form/sample-campaign-form.component';
import {CalloutCampaignService} from '@core/services/callout-campaign-management/callout-campaign.service';
import {ApDomainService} from '@core/services/ap-domain/ap-domain.service';
import {Constant} from '@shared/Constant';
import {forkJoin} from 'rxjs';
import {ConfirmDialogComponent, ConfirmDialogModel} from '@shared/components/confirm-dialog/confirm-dialog.component';
import {finalize} from 'rxjs/operators';

@Component({
  selector: 'app-callout-campaign-management',
  templateUrl: './callout-campaign-management.component.html',
  styleUrls: ['./callout-campaign-management.component.scss']
})
export class CalloutCampaignManagementComponent extends BaseComponent implements OnInit {
  listAction = [];
  templates = [];
  templatesType = [];
  ord = [];
  infoCode = [];
  isShow = false;
  isUpdate = false;
  isUpdateDetail = false;
  selectedTemp: any;
  isDisable = true;
  processing = false;
  field = {value: 'id', text: 'campaignName'};
  backUpDatasource = [];
  beforeUpdate: any;
  filterInput = new FormControl(null);
  templateForm: FormGroup = this.fb.group({
    template: new FormControl(null)
  });

  updateTemplateForm: FormGroup = this.fb.group({
    campaignName: new FormControl(null, [Validators.required, Validators.maxLength(100)])
  });
  detailForm: FormGroup = this.fb.group({
    id: new FormControl(null),
    type: new FormControl(null, [Validators.required]),
    name: new FormControl(null, [Validators.required, Validators.maxLength(100)]),
    code: new FormControl(null, [Validators.required]),
    ord: new FormControl(null, [Validators.required]),
    defaultValue: new FormControl(null, [Validators.maxLength(250)]),
    exportExcel: new FormControl(true),
    display: new FormControl(true),
    editable: new FormControl(true),
    campaignId: new FormControl(null)
  });

  constructor(public route: ActivatedRoute,
              private campaignService: CalloutCampaignService,
              private fb: FormBuilder,
              public dialog: MtxDialog,
              protected toastr: ToastrService,
              protected translateService: TranslateService,
              private router: Router,
              private cdr: ChangeDetectorRef,
              private domainService: ApDomainService,
  ) {
    super(route, campaignService, RESOURCE.CUSTOMER, toastr, translateService, dialog);
  }

  ngOnInit(): void {
    this.prepareData();
  }

  prepareData() {
    this.isLoading = false;
    this.setColumns();
    this.generateOrd();
    this.loadComboBox();
    this.onTemplateChange();
    this.onFilterTable();
  }

  add() {
    this.dialog.open({
      width: '30vw',
      disableClose: true,
      hasBackdrop: true,
      data: {
        isSaving: true
      }
    }, SampleCampaignFormComponent).afterClosed().subscribe((next: any) => {
      if (next?.campaignId) {
        this.loadComboBox(next.campaignId);
        this.findAllTemplateDetailByCampaignId(next.campaignId);
      }
    });
  }

  copy() {
    this.dialog.open({
      width: '30vw',
      disableClose: true,
      hasBackdrop: true,
      data: {
        isSaving: false,
        templates: this.templates,
        selectedTemp: this.selectedTemp
      }
    }, SampleCampaignFormComponent).afterClosed().subscribe((next: any) => {
      if (next?.campaignId) {
        this.findAllTemplateDetailByCampaignId(next.campaignId);
        this.loadComboBox(next.campaignId);
      }
    });
  }

  saveTempDetail() {
    const rawValues = this.detailForm.getRawValue();
    if (this.isShow) rawValues[`optionValues`] = this.toArrayOptions(rawValues.optionValue);
    this.detailForm.markAllAsTouched();
    console.log(this.detailForm.controls);
    if (this.detailForm.invalid) return;
    this.processing = true;
    this.isUpdateDetail ? this.doUpdateTemplateDetail() : this.doInsertTemplateDetail();
  }

  doInsertTemplateDetail() {
    const arrCheckbox = ['exportExcel', 'display', 'editable'];
    const rawValues = this.detailForm.getRawValue();
    rawValues[`campaignId`] = this.selectedTemp.id;
    arrCheckbox.forEach(control => rawValues[`${control}`] = rawValues[`${control}`] ? 1 : 0);
    if (this.isShow) rawValues[`optionValues`] = this.toArrayOptions(rawValues.optionValue);
    console.log('rawValues', rawValues);
    this.campaignService.insertTemplateDetail(rawValues).pipe(finalize(() => this.processing = false)).subscribe((next: any) => {
      console.log('next', next);
      if (next?.status?.code === '400') {
        this.showErrMess(next.status.message);
      } else {
        this.onSaveSuccess();
        this.findAllTemplateDetailByCampaignId(next.campaignId);
      }
    }, () => this.showUnknownErr());
  }

  doUpdateTemplateDetail() {
    const arrCheckbox = ['exportExcel', 'display', 'editable'];
    const rawValues = this.detailForm.getRawValue();
    rawValues[`campaignId`] = this.selectedTemp.id;
    arrCheckbox.forEach(control => rawValues[`${control}`] = rawValues[`${control}`] ? 1 : 0);
    if (this.isShow) rawValues[`optionValues`] = this.toArrayOptions(rawValues.optionValue);
    console.log('rawValues update', rawValues);
    this.campaignService.updateTemplateDetail(rawValues).pipe(finalize(() => this.processing = false)).subscribe((next: any) => {
      console.log('next update', next);
      if (next?.status?.code === '400') {
        this.showErrMess(next.status.message);
      } else {
        this.onSaveSuccess();
        this.findAllTemplateDetailByCampaignId(next.campaignId);
      }
    }, () => this.showUnknownErr());
  }

  editTemp() {
    this.disableForm();
    this.updateTemplateForm.get('campaignName').setValue(this.selectedTemp.campaignName);
  }

  deleteTemp() {
    const title = this.translateService.instant('common.confirm.title.delete');
    const content = this.translateService.instant('common.MSG0017');
    const dialogContent: ConfirmDialogModel = new ConfirmDialogModel(title, content);
    this.dialog.originalOpen(ConfirmDialogComponent, {
      width: '25vw',
      disableClose: true,
      hasBackdrop: true,
      data: dialogContent
    }).afterClosed().subscribe((next: any) => {
      console.log(next);
      if (next.data === Constant.DIALOG_CONFIRM) {
        this.deleteTemplateById(this.selectedTemp.id);
      }
    });
  }

  deleteTemplateById(id: any) {
    if (!id) return;
    this.campaignService.deleteTemplate(id).subscribe(() => {
      this.dataModel.dataSource = [];
      this.selectedTemp = null;
      this.onDeleteSuccess();
      this.loadComboBox();
      this.checkDisableBtn(this.selectedTemp);
      this.resetForm();
    }, () => this.showUnknownErr());
  }

  findAllTemplate() {
    return this.campaignService.findAllTemplate();
  }

  findTemplateType() {
    return this.domainService.getDomainByType(Constant.TEMPLATE_TYPE);
  }

  showUnknownErr() {
    this.toastr.error(this.translateService.instant('common.notify.fail'));
  }

  onDeleteSuccess() {
    this.toastr.success(this.translateService.instant('common.MSG0022'));
  }

  toArrayOptions(str: any) {
    const arrObject = [];
    const arr = str.split(';');
    arr.forEach(obj => {
      try {
        const props = obj.split(':');
        if (props.length === 0 || props.length > 2) {
          console.log('key value are malformed');
          this.detailForm.get('optionValue').setErrors({pattern: true});
          return;
        }
        if (!props[0] || !props[1]) {
          console.log('key or value must not be empty');
          this.detailForm.get('optionValue').setErrors({pattern: true});
          return;
        }
        const key = this.replaceQuotes(props[0]);
        const value = this.replaceQuotes(props[1]);
        if (!key || !value) {
          console.log('key value must not be empty');
          this.detailForm.get('optionValue').setErrors({pattern: true});
          return;
        }
        console.log(`{"code":"${key}","name":"${value}"}`);
        const keyValue = JSON.parse(`{"code":"${key}","name":"${value}"}`);
        arrObject.push(keyValue);
      } catch (e) {
        this.detailForm.get('optionValue').setErrors({pattern: true});
        console.error(e);
      }
    });
    return arrObject;
  }

  replaceQuotes(str: any) {
    return str.replace(/"/g, '').replace(/'/g, '').trim();
  }

  generateOrd() {
    for (let i = 0; i <= 30; i++) {
      this.ord.push(i);
    }
  }

  onTemplateTypeChange(event: any) {
    if (!event) return;
    if (Constant.CBX_TYPE_ID === event.value.toString()) {
      this.detailForm.addControl('optionValue', new FormControl(null, [Validators.required]));
      this.isShow = true;
    } else {
      this.isShow = false;
      this.detailForm.removeControl('optionValue');
    }
    console.log('form', this.detailForm);
  }

  loadComboBox(justAdded?: any) {
    forkJoin([this.findAllTemplate(), this.findTemplateType(), this.findAllInfoCode()]).subscribe((object: any) => {
      this.templates = object[0];
      this.templatesType = object[1] && object[1].length > 0 ? object[1].sort((a, b) => a.name > b.name ? 1 : -1) : [];
      this.infoCode = object[2];
      if (justAdded) this.templateForm.get('template').setValue(justAdded);
      console.log('this.templatesType', this.templatesType);
      console.log('this.templates', this.templates);
    }, () => this.showUnknownErr());
  }

  onTemplateChange() {
    this.templateForm.get('template').valueChanges.subscribe((next: any) => {
      if (!next) return;
      this.selectedTemp = this.templates.find(value => value.id === next);
      this.checkDisableBtn(this.selectedTemp);
      this.findAllTemplateDetailByCampaignId(next);
      this.resetForm();
      this.isShow = false;
      console.log('temp', next);
      console.log(' this.selectedTemp', this.selectedTemp);
    });
  }

  setColumns() {
    this.columns = [
      {i18n: 'common.orderNumber', field: 'no', width: '50px'},
      {i18n: 'campaignTemplate.propName', field: 'name'},
      {i18n: 'campaignTemplate.propCode', field: 'code'},
      {i18n: 'campaignTemplate.ord', field: 'ord'},
      {i18n: 'campaignTemplate.optionValue', field: 'optionValue', width: '250px'},
      {i18n: 'campaignTemplate.defaultValue', field: 'defaultValue'},
      {i18n: 'campaignTemplate.propType', field: 'typeName'},
      {i18n: 'campaignTemplate.exportExcel', field: 'exportExcel'},
      {i18n: 'campaignTemplate.display', field: 'display'},
      {i18n: 'campaignTemplate.editable', field: 'editable'},
      {i18n: 'Template', field: 'campaignName', width: '250px'},
      {i18n: 'campaignTemplate.tools', field: 'action', pinned: 'right', width: '120px'}
    ];
  }

  checkDisableBtn(template: any) {
    if (template && template.isUsed === '0') this.isDisable = false;
    else this.isDisable = true;
  }

  resetForm() {
    this.isShow = false;
    this.isUpdateDetail = false;
    this.detailForm.reset();
    this.detailForm.enable();
    const arrCheckbox = ['exportExcel', 'display', 'editable'];
    arrCheckbox.forEach(value => this.detailForm.get(value).setValue(true));
  }

  findAllInfoCode() {
    return this.campaignService.getAllInfoCode();
  }

  onSaveSuccess() {
    this.toastr.success(this.translateService.instant('common.MSG0021'));
  }

  findAllTemplateDetailByCampaignId(id: any) {
    if (!id) return;
    this.campaignService.findAllTemplateDetailByCampaignId(id).subscribe((next: any) => {
      console.log(next);
      this.dataModel.dataSource = next;
      this.backUpDatasource = next;
    }, () => this.showUnknownErr());
  }

  update() {
    const beforeCampaignName = this.selectedTemp.campaignName;
    this.beforeUpdate = this.selectedTemp;
    console.log(this.updateTemplateForm.getRawValue());
    this.selectedTemp.campaignName = this.updateTemplateForm.get('campaignName').value;
    console.log(' this.selectedTemp', this.selectedTemp);
    this.campaignService.updateTemplate(this.selectedTemp).subscribe((next: any) => {
      console.log('next update temp', next);
      this.onSaveSuccess();
      this.findAllTemplateDetailByCampaignId(this.selectedTemp.id);
      this.loadComboBox(this.selectedTemp.id);
      this.enableForm();
    }, (error: any) => {
      console.log(error);
      if (error?.error?.message === 'error.existed') {
        this.showErrMess(this.translateService.instant('validation.MSG0019'));
      }
    });
    this.beforeUpdate[`campaignName`] = beforeCampaignName;
  }

  back() {
    this.enableForm();
  }

  enableForm() {
    this.isUpdate = false;
    this.detailForm.enable();
    this.beforeUpdate = null;
  }

  disableForm() {
    this.isUpdate = true;
    this.detailForm.disable();
  }

  showErrMess(message: any) {
    if (message === 'existedTemplateDetail') {
      message = this.translateService.instant('validation.existCheck', {field: this.translateService.instant('campaignTemplate.propCode')});
    }
    this.toastr.error(message);
  }

  updateTemplateDetail(row: any) {
    this.isUpdateDetail = true;
    console.log(row);
    const arrCheckbox = ['exportExcel', 'display', 'editable'];
    this.onTemplateTypeChange({value: Number(row.type)});
    this.detailForm.patchValue(row);
    this.detailForm.get('type').setValue(Number(row.type));
    arrCheckbox.forEach(value => {
      this.detailForm.get(value).setValue(row[value] === '1');
    });
    if (row?.isDefault === '1') {
      this.detailForm.get('type').disable();
      this.detailForm.get('code').disable();
    } else {
      this.detailForm.get('type').enable();
      this.detailForm.get('code').enable();
    }
  }

  deleteTemplateDetail(row: any) {
    const title = this.translateService.instant('common.confirm.title.delete');
    const content = this.translateService.instant('common.MSG0017');
    const dialogContent: ConfirmDialogModel = new ConfirmDialogModel(title, content);
    this.dialog.originalOpen(ConfirmDialogComponent, {
      width: '25vw',
      disableClose: true,
      hasBackdrop: true,
      data: dialogContent
    }).afterClosed().subscribe((next: any) => {
      if (next.data === Constant.DIALOG_CONFIRM) {
        this.doDeleteTemplateDetail(row.id);
      }
    });
  }

  doDeleteTemplateDetail(id: any) {
    this.campaignService.deleteTemplateDetail(id).subscribe(() => {
      this.onDeleteSuccess();
      this.findAllTemplateDetailByCampaignId(this.selectedTemp.id);
    }, () => this.showUnknownErr());
  }

  onFilterTable() {
    this.filterInput.valueChanges.subscribe(next => {
      this.dataModel.dataSource = this.backUpDatasource.filter(value =>
        value.name?.toLowerCase().includes(next?.toLowerCase())
        || value.code?.toLowerCase().includes(next?.toLowerCase())
        || value.ord?.toString().toLowerCase().includes(next?.toLowerCase())
        || value.typeName?.toLowerCase().includes(next?.toLowerCase())
        || value.optionValue?.toLowerCase().includes(next?.toLowerCase())
        || value.defaultValue?.toLowerCase().includes(next?.toLowerCase())
      );
    });
  }

  pageChange(event: any) {
    this.pageIndex = event.pageIndex;
  }
}
