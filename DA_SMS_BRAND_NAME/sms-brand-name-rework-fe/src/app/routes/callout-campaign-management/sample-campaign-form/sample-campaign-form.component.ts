import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {TranslateService} from '@ngx-translate/core';
import {ToastrService} from 'ngx-toastr';
import {CalloutCampaignService} from '@core/services/callout-campaign-management/callout-campaign.service';

@Component({
  selector: 'app-sample-campaign-form',
  templateUrl: './sample-campaign-form.component.html',
  styleUrls: ['./sample-campaign-form.component.scss']
})
export class SampleCampaignFormComponent implements OnInit {
  isSaving: boolean;
  templates = [];
  selectedTemp: any;
  field = {value: 'id', text: 'campaignName'};
  formGroup: FormGroup = this.fb.group({
    id: new FormControl(null, [Validators.required]),
    campaignName: new FormControl(null, [Validators.required, Validators.maxLength(100)])
  });

  constructor(
    private dialogRef: MatDialogRef<SampleCampaignFormComponent>,
    private fb: FormBuilder,
    protected translateService: TranslateService,
    protected toastr: ToastrService,
    private campaignService: CalloutCampaignService,
    @Inject(MAT_DIALOG_DATA) public dataDialog?: any) {
    this.isSaving = this.dataDialog.data.isSaving;
  }

  ngOnInit(): void {
    if (this.isSaving) this.formGroup.removeControl('id');
    else this.formGroup.addControl('id', new FormControl(null, [Validators.required]));
    if (this.dataDialog.data.templates) this.templates = this.dataDialog.data.templates;
    if (this.dataDialog.data.selectedTemp && !this.isSaving) {
      this.selectedTemp = this.dataDialog.data.selectedTemp;
      this.formGroup.get('id').setValue(this.selectedTemp.id);
    }
  }

  save() {
    this.formGroup.markAllAsTouched();
    console.log(this.formGroup);
    if (this.formGroup.invalid) return;
    this.isSaving ? this.doInsert() : this.doCopy();
  }

  dismiss() {
    this.dialogRef.close();
  }

  doInsert() {
    const rawValues = this.formGroup.getRawValue();
    this.campaignService.insertTemplate(rawValues).subscribe((next: any) => {
      console.log(next);
      if (next?.errMessage) {
        this.toastr.error(next?.errMessage);
      } else {
        this.toastr.success(this.translateService.instant('common.MSG0021'));
        this.dialogRef.close({campaignId: next.id});
      }
    }, () => this.showUnknownErr());
  }

  doCopy() {
    const rawValues = this.formGroup.getRawValue();
    const currentTempId = this.formGroup.get('id').value;
    const currentTemp = this.templates.find((temp: any) => temp.id === currentTempId) ? this.templates.find((temp: any) => temp.id === currentTempId) : null;
    rawValues[`status`] = this.selectedTemp ? this.selectedTemp.status : currentTemp[`status`];
    console.log(rawValues);
    this.campaignService.copyTemplate(rawValues).subscribe(next => {
      console.log(next);
      this.toastr.success(this.translateService.instant('common.MSG0021'));
      this.dialogRef.close({campaignId: next.id});
    }, (err: any) => {
      if (err?.error?.message === 'error.existed') {
        this.showErrMess(this.translateService.instant('validation.MSG0019'));
        return;
      }
      this.showUnknownErr();
    });
  }

  showUnknownErr() {
    this.toastr.error(this.translateService.instant('common.notify.fail'));
  }

  showErrMess(message: any) {
    if (message === 'existedTemplateDetail') {
      message = this.translateService.instant('validation.existCheck', {field: this.translateService.instant('campaignTemplate.propCode')});
    }
    this.toastr.error(message);
  }
}
