import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CalloutCampaignManagementRoutingModule } from './callout-campaign-management-routing.module';
import { CalloutCampaignManagementComponent } from './callout-campaign-management.component';
import {SharedModule} from '@shared';
import { SampleCampaignFormComponent } from './sample-campaign-form/sample-campaign-form.component';


@NgModule({
  declarations: [CalloutCampaignManagementComponent, SampleCampaignFormComponent],
  imports: [
    CommonModule,
    CalloutCampaignManagementRoutingModule,
    SharedModule
  ]
})
export class CalloutCampaignManagementModule { }
