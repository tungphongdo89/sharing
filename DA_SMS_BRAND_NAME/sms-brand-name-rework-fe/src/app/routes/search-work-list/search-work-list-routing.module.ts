import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {SearchWorkListComponent} from '@app/routes/search-work-list/search-work-list/search-work-list.component';


const routes: Routes = [
  {
    path: '',
    component: SearchWorkListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SearchWorkListRoutingModule { }
