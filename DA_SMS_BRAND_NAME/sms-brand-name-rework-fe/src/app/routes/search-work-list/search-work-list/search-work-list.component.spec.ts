import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchWorkListComponent } from './search-work-list.component';

describe('SearchWorkListComponent', () => {
  let component: SearchWorkListComponent;
  let fixture: ComponentFixture<SearchWorkListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchWorkListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchWorkListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
