import { Component, OnInit } from '@angular/core';
import {MtxGridColumn} from '@ng-matero/extensions';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ToastrService} from 'ngx-toastr';
import {TranslateService} from '@ngx-translate/core';
import {EvaluateAssignmentService} from '@core/services/evaluate-assignment/evaluate-assignment.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-search-work-list',
  templateUrl: './search-work-list.component.html',
  styleUrls: ['./search-work-list.component.scss']
})
export class SearchWorkListComponent implements OnInit {


  inputSearch: FormGroup;
  data: any = [];
  columns: MtxGridColumn[] = [];
  pageSizeList = [10, 50, 100];
  isLoading = false;
  offset: any = 0;
  pagesize = 10;
  total: any = 0;
  pageIndex: number;
  evaluaterList: any = [];
  userList: any = [];

  channelId: any;      // Kênh thông tin
  evaluaterId: any;    // Người đánh giá
  userId: any;         // Tổng đài viên
  evaluateStatus: any;         // Trạng thái đánh giá
  startDate: Date;       // Ngày phân công từ
  endDate: Date;         // Ngày phân công đến
  channelList: any = [
    {
      label: this.translateService.instant('evaluate-assignment.in-call'),
      value: 1
    }, {
      label: this.translateService.instant('evaluate-assignment.out-call'),
      value: 2
    }
  ];
  listStatus: any = [
    {
      label: this.translateService.instant('search_work_list.rating_status_done'),
      value: 1
    }, {
      label: this.translateService.instant('search_work_list.rating_status_not_done'),
      value: 2
    }
  ];

  constructor(
    private fb: FormBuilder,
    private toast: ToastrService,
    private evaluateService: EvaluateAssignmentService,
    protected translateService: TranslateService,
    private router: Router,
  ) {
  }

  ngOnInit(): void {

    this.getEvaluatorList();
    this.getUserList();

    this.columns = [
      {i18n: 'search_work_list.column.no', field: 'index', width: '70px',},
      {i18n: 'search_work_list.column.reception', field: 'channelName'},
      {i18n: 'search_work_list.column.operator', field: 'names'},
      {i18n: 'search_work_list.column.taskmaster', field: 'evaluaterName'},
      {i18n: 'search_work_list.column.assignment_date', field: 'createDatetime', width: '100px'},
      {i18n: 'search_work_list.column.total_amount', field: 'totalCall' , width: '100px'},
      {i18n: 'search_work_list.column.amount_not_yet', field: 'totalCalledNot', width: '100px'},
      {i18n: 'search_work_list.column.amount_yet', field: 'totalCalled', width: '100px'},
      {i18n: 'search_work_list.column.time_call', field: 'callTime',width: '200px'},
      {
        i18n: 'search_work_list.column.detail',
        field: 'options',
        width: '120px',
        pinned: 'right',
        right: '0px',
        type: 'button',
        buttons: [
          {
            type: 'icon',
            text: 'edit',
            icon: 'edit',
            click: record => this.updateCriteriaGroup(record),
          }
        ]
      }
    ];
    this.pageIndex = 0;
    this.getAssignList(this.pageIndex, this.pagesize);
  }

  onSearch(): void {
    if (this.beforeSearch()){
      this.pageIndex = 0;
      this.getAssignList(this.pageIndex, this.pagesize);
    }

  }

  beforeSearch(): boolean {
    if (!this.channelId) {
      this.toast.error(this.translateService.instant('evaluate-list.validator.required', {field: this.translateService.instant('evaluate-assignment.channel')}));
      document.getElementById('channelId').focus();
      return false;
    }
    if (!this.startDate) {
      this.toast.error(this.translateService.instant('evaluate-list.validator.required', {field: this.translateService.instant('evaluate-assignment.start-date')}));
      document.getElementById('startDate').focus();
      return false;
    }
    if (!this.endDate) {
      this.toast.error(this.translateService.instant('evaluate-list.validator.required', {field: this.translateService.instant('evaluate-assignment.end-date')}));
      document.getElementById('endDate').focus();
      return false;
    }
    if (this.endDate < this.startDate) {
      console.log('aaaa');
      this.toast.error(this.translateService.instant('evaluate-list.validator.invalid-date'));
      document.getElementById('endDate').focus();
      return false;
    }
    return true;
  }


  getAssignList(page: any, size: any): void {
    const data = {
      channelId: this.channelId,
      evaluaterId: this.evaluaterId,
      evaluateStatus: this.evaluateStatus,
      userId: this.userId,
      startDate: this.startDate,
      endDate: this.endDate
    };
    const pageable = {page, size};
    this.evaluateService.onSearch(data, pageable).subscribe(
      res => {
        res.body.forEach(item => item.channelName = this.getChannelName(Number(item.channelId)));
        this.data = res.body;
        console.log('res body ', res.body);
        this.total = Number(res.headers.get('X-Total-Count'));
      }
    );
  }


  getChannelName(channelId: any): string {
    if (channelId === 1) {
      return this.translateService.instant('evaluate-assignment.in-call');
    } else {
      return this.translateService.instant('evaluate-assignment.out-call');
    }
  }

  getEvaluatorList(): void {
    this.evaluateService.getUsersByType(2).subscribe(res => this.evaluaterList = res);
  }
  getUserList(): void {
    this.evaluateService.getUsersByType(1).subscribe(res => this.userList = res);
  }

  onPageChange(event) {
    this.offset = event.pageIndex;
    this.pagesize = event.pageSize;
  }

  updateCriteriaGroup(record: any) {
    if (record.id){
      this.router.navigate(['/call-information-for-evaluation/', record.ids]);
    }
  }
}
