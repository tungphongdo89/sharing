import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SearchWorkListRoutingModule } from './search-work-list-routing.module';
import { SearchWorkListComponent } from './search-work-list/search-work-list.component';
import {SharedModule} from '@shared';


@NgModule({
  declarations: [SearchWorkListComponent],
  imports: [
    CommonModule,
    SharedModule,
    SearchWorkListRoutingModule
  ]
})
export class SearchWorkListModule { }
