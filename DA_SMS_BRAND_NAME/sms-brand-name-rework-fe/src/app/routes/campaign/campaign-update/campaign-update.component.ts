import {Component, Inject, OnInit, Optional} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {CampaignModel} from '@app/core/models/campaign';
import {CampaignService} from '@app/core/services/campaign/campaign.service';
import {TranslateService} from '@ngx-translate/core';
import {ToastrService} from 'ngx-toastr';
import {CampaignScriptService} from '@core/services/campaign-script/campaign-script.service';
import {CampaignTemplateService} from '@core/services/campaign/campaign-template.service';
import {GroupUserService} from '@core/services/group-user.service';
import {formatDate} from '@angular/common';
import {ValidationService} from '@shared/common/validation.service';

@Component({
  selector: 'app-campaign-update',
  templateUrl: './campaign-update.component.html',
  styleUrls: ['./campaign-update.component.scss']
})
export class CampaignUpdateComponent implements OnInit {

  campaignTemplate: any = [];
  campaignScript: any = [];
  groupUsers: any = [];
  groupUseres: any = [];
  campaignUpdateForm: FormGroup;
  today = new Date();
  jstoday = '';
  optionSet: any = {};
  action: string;

  constructor(public dialogRef: MatDialogRef<CampaignUpdateComponent>,
              private campaignScriptService: CampaignScriptService,
              private campaignTemplateService: CampaignTemplateService,
              private groupUserService: GroupUserService,
              @Optional() @Inject(MAT_DIALOG_DATA) public data: CampaignModel,
              private campaignService: CampaignService,
              private toast: ToastrService,
              protected translateService: TranslateService) {
    this.action = 'edit';
    this.optionSet = data;
  }

  ngOnInit(): void {
    this.jstoday = formatDate(this.today, 'yyyy-MM-dd', 'en-US', '+0530');
    this.initData();
    this.campaignUpdateForm = new FormGroup({
        name: new FormControl({value: '', disabled: true}),
        campaignScriptId: new FormControl({value: '', disabled: true}),
        campaignTemplateId: new FormControl({value: '', disabled: true}),
        groupUser: new FormControl(),
        lstGroupUser: new FormControl('', [Validators.required]),
        startDate: new FormControl('', ),
        endDate: new FormControl('', [Validators.required]),
        displayPhone: new FormControl('2', [Validators.required]),
        status: new FormControl('1', [Validators.required])
      },
      {
        validators: ValidationService.notBefore('endDate', 'startDate', 'notBefore'),
      });
  }

  loadData() {
    if (this.action === 'edit') {
      // tslint:disable-next-line:forin
      // console.log(this.optionSet);
      this.optionSet.lstGroupUser = Array.from(this.optionSet.groupUser.split(','), Number);
      this.campaignUpdateForm.patchValue(this.optionSet);
    }
  }

  update() {
    // tslint:disable-next-line:forin
    for (const controlName in this.campaignUpdateForm.controls) {
      this.optionSet[controlName] = this.campaignUpdateForm.controls[controlName].value;
      // console.log('cho xin cai log nao: ' + this.campaignUpdateForm.controls[controlName].value);
    }
    this.dialogRef.close(this.optionSet);
  }

  showNotifySuccess() {
    this.toast.success(this.translateService.instant('common.notify.save.success'));
  }

  showUnknownErr() {
    this.toast.error(this.translateService.instant('common.notify.save.error'));
  }

  closeDialog() {
    this.dialogRef.close({event: 'cancel'});
  }

  initData() {
    // Mẫu chiến dịch
    this.campaignTemplateService.getLstTemplateScript().subscribe(res => {
      this.campaignTemplate = res;
    });
    // Kịch bản
    this.campaignScriptService.getLstCampaignScript().subscribe(res => {
      this.campaignScript = res;
    });
    // nhóm chiến dịch
    this.groupUserService.getLstGroupUser().subscribe(res => {
      this.groupUsers = res;
      this.loadData();
      console.log(this.groupUsers);
    });
  }

}
