import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CampaignRoutingModule } from './campaign-routing.module';
import { CampaignComponent } from './campaign/campaign.component';
import { SharedModule } from '@app/shared';
import { CampaignCreateComponent } from './campaign-create/campaign-create.component';
import { CampaignUpdateComponent } from './campaign-update/campaign-update.component';
import {Ng2SearchPipeModule} from 'ng2-search-filter';


@NgModule({
  declarations: [CampaignComponent, CampaignCreateComponent, CampaignUpdateComponent],
    imports: [
        CommonModule,
        SharedModule,
        CampaignRoutingModule,
        Ng2SearchPipeModule
    ]
})
export class CampaignModule { }
