import {Component, EventEmitter, Input, OnInit, Output, TemplateRef, ViewChild} from '@angular/core';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {MtxGridColumn} from '@ng-matero/extensions';
import {BaseComponent} from '@shared/components/base-component/base-component.component';
import {CampaignService} from '@app/core/services/campaign/campaign.service';
import {CampaignModel} from '@app/core/models/campaign';
import {CampaignCreateComponent} from '../campaign-create/campaign-create.component';
import {MatDialog} from '@angular/material/dialog';
import {Constant} from '@app/shared/Constant';
import {ToastrService} from 'ngx-toastr';
import {TranslateService} from '@ngx-translate/core';
import {PopupDeleteComponent} from '@app/routes/directory-management/popup-delete/popup-delete.component';
import {CampaignUpdateComponent} from '../campaign-update/campaign-update.component';
import {CampaignScriptService} from '@app/core/services/campaign-script/campaign-script.service';
import {CampaignTemplateService} from '@core/services/campaign/campaign-template.service';
import * as moment from 'moment';
import {ConfigScheduleModel} from '@core/models/config-schedule-model';
import {finalize} from 'rxjs/operators';

@Component({
  selector: 'app-campaign',
  templateUrl: './campaign.component.html',
  styleUrls: ['./campaign.component.scss'],
})
export class CampaignComponent implements OnInit {

  @ViewChild('button', {static: true}) button: TemplateRef<any>;
  @Input() campaignModel: CampaignModel[] = [];
  @Output() pageChange: EventEmitter<any> = new EventEmitter<any>();

  searchText: any = '';
  isLoading = false;
  create = 201;
  pageSizeList = [10, 50, 100];
  pagesize = 10;
  formInput: FormGroup;
  filterInput = new FormControl(null);
  total: any = 0;
  columns: MtxGridColumn[] = [];
  isAdvSearch = false;
  backUpDatasource = [];
  campaignModelFinter: CampaignModel = new CampaignModel();

  campaignTemplate: any = [];
  campaignScript: any = [];
  gruopUser: any = [];
  listAction = [];
  dataModel: any = {};
  offset: any = 0;
  currentPage: any;

  campaignForm: FormGroup = this.fb.group({
    id: null,
    name: null,
    campaignTemplateId: null,
    campaignScriptId: null,
    startDate: null,
    endDate: null,
    disPlayPhone: null,
    status: null,
  });
  data: any = [];

  constructor(
    private fb: FormBuilder,
    private campaignService: CampaignService,
    private campaignScriptService: CampaignScriptService,
    private campaignTemplateService: CampaignTemplateService,
    private dialog: MatDialog,
    private toast: ToastrService,
    protected translateService: TranslateService
  ) {
  }

  ngOnInit(): void {
    this.initData();
    this.formInput = new FormGroup({
      keyword: new FormControl(),
    });
    this.getAllCampaign(true);

    this.columns = [
      {i18n: 'campaign.management.column.no', field: 'index',width: '70px',},
      {i18n: 'campaign.management.column.id', field: 'id'},
      {i18n: 'campaign.management.column.name', field: 'name'},
      {i18n: 'campaign.management.column.campaign-template', field: 'campaignTemplateName'},
      {i18n: 'campaign.management.column.campaign-script', field: 'campaignScriptName'},
      {
        i18n: 'campaign.management.column.start-date', field: 'startDate', formatter: (rowData) => {
          if (rowData?.startDate) return `<span>${moment(rowData?.startDate).format('DD/MM/YYYY')}</span>`;
        }
      },
      {i18n: 'campaign.management.column.end-date', field: 'endDate', width: '90px'},
      {i18n: 'campaign.management.column.status', field: 'status'},
      {i18n: 'campaign.management.column.create-date', field: 'createDatetime'},
      {
        i18n: 'common.action',
        field: 'options',
        width: '100px',
        pinned: 'right',
        right: '0px',
        type: 'button',
        buttons: [
          {
            type: 'icon',
            text: 'edit',
            icon: 'edit',
            click: record => this.updateCampaign(record, 'edit'),
          },
          // {
          //   type: 'icon',
          //   text: 'delete',
          //   icon: 'delete',
          //   click: record => this.deleteStatus(record, 'change'),
          // }
        ]
      }
    ];
  }

  showNotifyDeleteSuccess() {
    this.toast.success(this.translateService.instant('common.confirm.delete.success'));
  }

  showNotifySuccess() {
    this.toast.success(this.translateService.instant('common.notify.save.success2'));
  }

  showUnknownErr() {
    this.toast.error(this.translateService.instant('common.notify.save.error'));
  }


  initData() {
    // Mẫu chiến dịch
    this.campaignTemplateService.getLstTemplateScript().subscribe(res => {
      this.campaignTemplate = res;
    });
    // Kịch bản
    this.campaignScriptService.getLstCampaignScript().subscribe(res => {
      this.campaignScript = res;
    });
    this.onFilterTable();
  }

  updateCampaign(campaignModel1: CampaignModel, action: string) {
    const dialogRef = this.dialog.open(CampaignUpdateComponent, {data:campaignModel1});
    // update
    // campaignModel1.lstGroupUser = campaignModel1.groupUser;
    // console.log('log cam paign 1   '+campaignModel1.groupUser);
    // console.log('log cam paign 1   '+campaignModel1.lstGroupUser);
    dialogRef.afterClosed().subscribe(result => {
      if (result === undefined) {
        return;
      }
      if (result.event !== 'cancel') {
        this.campaignService.updateCampaign(result).subscribe(rs => {
          // console.log('rs day ne '+rs);
          if (rs.msgCode === Constant.OK) {
            this.showNotifySuccess();
            this.getAllCampaign(false);
          } else {
            this.showUnknownErr();
          }
        });
      }
    });
  }

  deleteStatus(u: any, action: string) {
    // const dialogRef = null;
    u.action = action;
    // this.optionService.getOptionSetById(u.optionSetId).subscribe(rs=>{
    //   console.log(rs);
    const dialogRef = this.dialog.open(PopupDeleteComponent, {
      data: u
    });
    // });
    dialogRef.afterClosed().subscribe(result => {
      if (result === undefined) {
        return;
      }
      if (result.event === 'change') {
        this.campaignService.deleteCampaign(u).subscribe(rs => {
          if (rs.msgCode === Constant.OK) {
            this.showNotifyDeleteSuccess();
            this.getListOptionSet();
          } else {
            this.showUnknownErr();
          }
        });
      }
    });
  }
  onFilterTable() {
    this.filterInput.valueChanges.subscribe(next => {
      this.dataModel.dataSource = this.backUpDatasource.filter(value =>
        value.name?.toLowerCase().includes(next?.toLowerCase())
        // || value.ord?.toString().toLowerCase().includes(next?.toLowerCase())
      );
    });
  }

  // onSearch() {
  //   this.isAdvSearch = true;
  //   this.isLoading = true;
  //   const keyword = this.formInput.get('keyword').value;
  //   if (keyword == null || keyword === '') {
  //     this.getConfigSchedule();
  //     this.isLoading = false;
  //     this.isAdvSearch = false;
  //     return;
  //   }
  //   this.campaignModelFinter.keySearch = keyword;
  //   this.campaignModelFinter.page = 1;
  //   this.campaignModelFinter.size = this.pagesize;
  //   this.onSearchCampaign({offset: 0});
  //
  // }

  // onSearchCampaign(pageInfo) {
  //   const pageToLoad: number = pageInfo.offset;
  //   this.configSchedule.searchText(this.configModel, {
  //     page: pageToLoad,
  //     size: this.pagesize
  //   }).pipe(finalize(() => {
  //     this.isLoading = false;
  //     this.isAdvSearch = false;
  //   })).subscribe(res => {
  //     this.data = res.body;
  //     this.totalRecord = Number(res.headers.get('X-Total-Count'));
  //     if (this.data.length === 0) {
  //       this.showNotifyNotFound();
  //     }
  //   }, () => this.showUnknownErr());
  // }


  getConfigSchedule() {
    this.campaignService.getAllCampaigns().subscribe(res => {
      this.data = res;
    });
  }

  search(reset?: boolean) {
    if (reset) {
      this.offset = 0;
      this.pagesize = 10;
    }
    const data = {...this.campaignForm.value, queryType: 1};
    this.campaignService.query(data, {size: this.pagesize, page: this.offset}).subscribe(res => {
      this.data = res.body;
      console.log(this.data);
      console.log(res);
      console.log(res.headers.get('X-Total-Count'));
      this.total = Number(res.headers.get('X-Total-Count'));
    });
  }

  getAllCampaign(reset?: boolean) {
    if (reset) {
      this.offset = 0;
      this.pagesize = 10;
    }
    const data = {...this.campaignForm.value, queryType: 1};
    this.campaignService.getAllCampaign(data, {size: this.pagesize, page: this.offset}).subscribe(res => {
      this.data = res.body;
      console.log(this.data);
      console.log(res.headers.get('X-Total-Count'));
      this.total = Number(res.headers.get('X-Total-Count'));
    });
  }

  getListOptionSet(reset?: boolean) {
    if (reset) {
      this.offset = 0;
      this.pagesize = 10;
    }
    const data = {...this.campaignForm.value, queryType: 1, pageSize: this.pagesize, offset: this.offset};
    this.campaignService.query(data, {pageSize: this.pagesize, offset: this.offset}).subscribe(res => {
      this.campaignModel = res.body;
    });
  }

  addCampaign(action: string) {
    const newCampaign: any = {};
    newCampaign.action = action;
    const dialogRef = this.dialog.open(CampaignCreateComponent, {
      data: newCampaign
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result === undefined) {
        return;
      }
      if (result.event !== 'cancel') {
        this.campaignService.addCampaign(result).subscribe(data => {
          console.log(data);
          if (Constant.OK === data.body.msgCode) {
            this.showNotifySuccess();
            this.getAllCampaign();
          } else {
            this.showUnknownErr();
          }
        });
      }
    });
  }

  onPageChange(event) {
    this.offset = event.pageIndex;
    this.pagesize = event.pageSize;
    this.getAllCampaign();
  }

}
