import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { CampaignScriptService } from '@app/core/services/campaign-script/campaign-script.service';
import { GroupUserService } from '@app/core/services/group-user.service';
import { OptionSetCreateComponent } from '@app/routes/directory-management/option-set-create/option-set-create.component';
import {CampaignTemplateService} from '@core/services/campaign/campaign-template.service';
import {ValidationService} from '@shared/common/validation.service';
import {formatDate} from '@angular/common';

@Component({
  selector: 'app-campaign-create',
  templateUrl: './campaign-create.component.html',
  styleUrls: ['./campaign-create.component.scss']
})
export class CampaignCreateComponent implements OnInit {


  campaignTemplate: any = [];
  campaignScript: any = [];
  groupUsers: any = [];
  today= new Date();
  jstoday = '';
  formInput: FormGroup;
  constructor(
    private fb: FormBuilder,
    private campaignScriptService: CampaignScriptService,
    private campaignTemplateService: CampaignTemplateService,
    private groupUserService: GroupUserService,
    public dialogRef: MatDialogRef<CampaignCreateComponent>) { }
    private action: string;
    private campaign: any = {};

  ngOnInit(): void {
    this.jstoday = formatDate(this.today, 'yyyy-MM-dd', 'en-US', '+0700');
    this.initData();
    // @ts-ignore
    this.formInput = new FormGroup({
      name: new FormControl('', [Validators.required, Validators.maxLength(255)]),
      campaignScriptId: new FormControl('',[Validators.required]),
      campaignTemplateId: new FormControl('',[Validators.required]),
      lstGroupUser: new FormControl('',[Validators.required]),
      startDate: new FormControl('',[Validators.required]),
      endDate: new FormControl('',[Validators.required]),
      displayPhone: new FormControl('2', [Validators.required]),
      status: new FormControl('1', [Validators.required])
    },
      {
        validators: ValidationService.notBefore('endDate','startDate','notBefore'),
      });
  }


  initData() {
    // Mẫu chiến dịch
    this.campaignTemplateService.getLstTemplateScript().subscribe(res => {
          this.campaignTemplate = res;
        });
    // Kịch bản
    this.campaignScriptService.getLstCampaignScript().subscribe(res => {
      this.campaignScript = res;
    });
    // nhóm chiến dịch
    this.groupUserService.getLstGroupUser().subscribe(res => {
      this.groupUsers = res;
    });
  }

  submit(){}

  addCampaign() {
    // tslint:disable-next-line:forin
    for (const controlName in this.formInput.controls) {
      this.campaign[controlName] = this.formInput.controls[controlName].value;
    }
    this.dialogRef.close(this.campaign);
  }
  // saveCampaign() {
  //   // tslint:disable-next-line:forin
  //   for (const a in this.formInput.controls) {
  //     this.optionSet[a] = this.formInput.controls[a].value;
  //   }
  //   this.dialogRef.close(this.optionSet);
  // }
  closeDialog() {
    this.dialogRef.close({event: 'cancel'});
  }

}
