import {Component, OnInit} from '@angular/core';
import {CampaignScriptService} from '@core/services/campaign-script/campaign-script.service';
import {MatDialog} from '@angular/material/dialog';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ToastrService} from 'ngx-toastr';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-copy-campaign-script',
  templateUrl: './copy-campaign-script.component.html',
  styleUrls: ['./copy-campaign-script.component.scss']
})
export class CopyCampaignScriptComponent implements OnInit {


  campaignScript: any = [];
  formInput: FormGroup;

  constructor(
    private campaignScriptService: CampaignScriptService,
    private dialog: MatDialog,
    private fb: FormBuilder,
    protected toast: ToastrService,
    protected translateService: TranslateService,
  ) {
  }

  ngOnInit(): void {
    this.formInput = this.fb.group({
      id: new FormControl(null, [Validators.required]),
      newName: new FormControl(null, [Validators.required]),
    });
    this.onLoadScript();
  }


  checkCopy() {
    this.campaignScriptService.getCheckCopyScript(this.formInput.value).subscribe(res => {
      console.log('resss', res);
      if (res.body.message === 'ok') {
        this.toast.success(this.translateService.instant('campain-script.copy.copy_success'));
        this.closeDialof();
      } else {
        this.toast.error(this.translateService.instant('campain-script.copy.copy_error'));
      }
    });
  }

  onLoadScript() {
    this.campaignScriptService.getLstCampaignScript().subscribe(res => {
      this.campaignScript = res;
    });
  }

  closeDialof() {
    this.dialog.closeAll();
  }
}
