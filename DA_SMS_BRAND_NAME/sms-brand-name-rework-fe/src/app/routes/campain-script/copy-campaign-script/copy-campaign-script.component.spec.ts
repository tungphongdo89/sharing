import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CopyCampaignScriptComponent } from './copy-campaign-script.component';

describe('CopyCampaignScriptComponent', () => {
  let component: CopyCampaignScriptComponent;
  let fixture: ComponentFixture<CopyCampaignScriptComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CopyCampaignScriptComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CopyCampaignScriptComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
