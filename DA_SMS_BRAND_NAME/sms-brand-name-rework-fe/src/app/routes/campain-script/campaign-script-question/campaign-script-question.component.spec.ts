import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CampaignScriptQuestionComponent } from './campaign-script-question.component';

describe('CampaignScriptQuestionComponent', () => {
  let component: CampaignScriptQuestionComponent;
  let fixture: ComponentFixture<CampaignScriptQuestionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CampaignScriptQuestionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CampaignScriptQuestionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
