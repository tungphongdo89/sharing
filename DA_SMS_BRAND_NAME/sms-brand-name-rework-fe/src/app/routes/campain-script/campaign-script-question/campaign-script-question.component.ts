import {Component, Inject, Input, OnInit, Optional, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ToastrService} from 'ngx-toastr';
import {TranslateService} from '@ngx-translate/core';
import {CampaignScriptService} from '@core/services/campaign-script/campaign-script.service';
import {
  HtmlEditorService,
  ImageService,
  LinkService,
  TableService,
  ToolbarService
} from '@syncfusion/ej2-angular-richtexteditor';
import {SampleCampaignFormComponent} from '@app/routes/callout-campaign-management/sample-campaign-form/sample-campaign-form.component';
import {CampaignModel} from '@core/models/campaign';
import {CampaignScriptQuestionResponseModel} from '@core/models/campaign-script-question-response-model';

@Component({
  selector: 'app-campaign-script-question',
  templateUrl: './campaign-script-question.component.html',
  styleUrls: ['./campaign-script-question.component.scss'],
  providers: [ToolbarService, LinkService, ImageService, HtmlEditorService, TableService]
})
export class CampaignScriptQuestionComponent implements OnInit {

  checkbox: boolean;
  action: string;
  scriptQuestion: any = {};
  public tools: object = {
    items: [
      'Bold', 'Italic', 'Underline', 'StrikeThrough', '|',
      'FontName', 'FontSize', 'FontColor', 'BackgroundColor', '|',
      'LowerCase', 'UpperCase', '|', 'Undo', 'Redo', '|',
      'Formats', 'Alignments', '|', 'OrderedList', 'UnorderedList', '|',
      'Indent', 'Outdent', '|', 'CreateLink', 'CreateTable',
      '|', 'ClearFormat', 'Print', 'SourceCode', '|', 'FullScreen'],
    type: 'Scrollable',
    enableFloating: false
  };

  @Input() tabType: number;
  @Input() data: CampaignScriptQuestionResponseModel;
  @Input() scriptIdss: number;
  formInput: FormGroup;
  positionDef: any ;
  checkContent: boolean;

  constructor(
    private fb: FormBuilder,
    private dialog: MatDialog,
    private campaignScriptService: CampaignScriptService,
    private toast: ToastrService,
    protected translateService: TranslateService,
    @Optional() @Inject(MAT_DIALOG_DATA) public dataS,
    public dialogRef: MatDialogRef<CampaignScriptQuestionComponent>
  ) {
    this.action = 'edit';
  }


  ngOnInit(): void {

    this.checkContent = true;
    // console.log('this dat', this.data);
    // console.log('this scriptIdss', this.dataS.scriptIdss);
    // console.log('pooo ', this.positionDef);

    this.formInput = new FormGroup({
      code: new FormControl({value: '', disabled: true}),
      position: new FormControl(null , [Validators.required]),
      checkbox: new FormControl(true, ),
      display: new FormControl('', ),
      content: new FormControl(null),
      status: new FormControl('', ),
      action: new FormControl(),
    });
    this.loadPosition();
    this.loadData();
    this.checkbox = true;
  }

  loadData() {
      if (this.data) {
      this.scriptQuestion = this.data;
      this.formInput.get('code').setValue(this.scriptQuestion.code);
      this.formInput.get('position').setValue(this.scriptQuestion.position);
      this.formInput.get('checkbox').setValue(this.scriptQuestion.checkbox);
      this.formInput.get('display').setValue(this.scriptQuestion.display);
      this.formInput.get('content').setValue(this.scriptQuestion.content);
      this.formInput.get('status').setValue(this.scriptQuestion.status);
    }
  }

  checkValidate(){
    if (this.formInput.get('content').value === null){
      this.toast.error(this.translateService.instant('common.notify.save.error'));
      this.checkContent = true;
    } else {
      console.log('không');
    }
  }

  loadPosition(){
    this.campaignScriptService.getPositionQuestion(this.dataS.scriptIdss).subscribe(res => {
      this.positionDef = res.body;
      this.formInput.get('position').setValue(Number(this.positionDef + 1));
    });
  }

  addScriptQuestion() {
    // tslint:disable-next-line:forin
    if (this.formInput.get('content').value === null){
      this.checkContent = false;
    } else {
      this.formInput.markAllAsTouched();
      if (this.formInput.valid){
        this.formInput.get('action').setValue('addQuestion');
        for (const controlName in this.formInput.controls) {
          this.scriptQuestion[controlName] = this.formInput.controls[controlName].value;
        }
        this.dialogRef.close(this.scriptQuestion);
      } else {
        this.toast.error(this.translateService.instant('common.notify.save.error'));
      }
    }
  }

  closeDialog() {
    this.dialogRef.close({event: 'cancel'});
  }


  showNotifyDeleteSuccess() {
    this.toast.success(this.translateService.instant('common.confirm.delete.success'));
  }

  showNotifySuccess() {
    this.toast.success(this.translateService.instant('common.notify.save.success2'));
  }

  showUnknownErr() {
    this.toast.error(this.translateService.instant('common.notify.save.error'));
  }

}
