import {Component, Inject, Input, OnInit, Optional} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Location} from '@angular/common';
import {
  HtmlEditorService,
  ImageService,
  LinkService,
  TableService,
  ToolbarService
} from '@syncfusion/ej2-angular-richtexteditor';
import * as moment from 'moment';
import {MtxGridColumn} from '@ng-matero/extensions';
import {Router} from '@angular/router';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {CampaignScriptQuestionResponseModel} from '@core/models/campaign-script-question-response-model';
import {CampaignScriptAnwserReponseModel} from '@core/models/campaign-script-anwser-reponse-model';
import {PopupDeleteComponent} from '@app/routes/directory-management/popup-delete/popup-delete.component';
import {CampaignScriptAnwserServiceService} from '@core/services/campaign-script/campaign-script-anwser.service';
import {Constant} from '@shared/Constant';
import {ToastrService} from 'ngx-toastr';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-campaign-script-anwser',
  templateUrl: './campaign-script-anwser.component.html',
  styleUrls: ['./campaign-script-anwser.component.scss'],
  providers: [ToolbarService, LinkService, ImageService, HtmlEditorService, TableService]
})
export class CampaignScriptAnwserComponent implements OnInit {

  @Input() tabType: number;
  @Input() questionId: number;
  @Input() dataAnwser: CampaignScriptAnwserReponseModel;
  formInput: FormGroup;
  checkND: boolean;
  checkType: boolean;
  isLoading = false;
  create = 201;
  positionAnwser: number;
  pageSizeList = [10, 50, 100];
  pagesize = 10;
  total: any = 0;
  columns: MtxGridColumn[] = [];
  data: any = [];
  campaignTemplate: any = [];
  campaignScript: any = [];
  gruopUser: any = [];
  listAction = [];
  scriptAnwser: any = {};
  addAnwser: any = {};
  checkBTN: boolean = true;

  offset: any = 0;
  public tools: object = {
    items: [
      'Bold', 'Italic', 'Underline', 'StrikeThrough', '|',
      'FontName', 'FontSize', 'FontColor', 'BackgroundColor', '|',
      'LowerCase', 'UpperCase', '|', 'Undo', 'Redo', '|',
      'Formats', 'Alignments', '|', 'OrderedList', 'UnorderedList', '|',
      'Indent', 'Outdent', '|', 'CreateLink', 'CreateTable',
      '|', 'ClearFormat', 'Print', 'SourceCode', '|', 'FullScreen'],
    type: 'Scrollable',
    enable: false
  };


  campaignScriptAnwser: FormGroup = this.fb.group({
    id: new FormControl({value: null, disabled: true} ),
    campaignScriptQuestionId: new FormControl({value: null, disabled: true} ),
    code: new FormControl({value: null, disabled: true}, ),
    questionCode: new FormControl({value: null, disabled: true}, ),
    type: new FormControl(null, [Validators.required]),
    position: new FormControl(null,[Validators.required] ),
    display: new FormControl(null ),
    content: new FormControl(null),
    min: new FormControl(null),
    max: new FormControl(null),
    status: new FormControl(null ),
    action: new FormControl(),
  });

  constructor(
    private fb: FormBuilder,
    @Optional() @Inject(MAT_DIALOG_DATA) public dataA,
    public dialogRef: MatDialogRef<CampaignScriptAnwserComponent>,
    private dialog: MatDialog,
    protected toast: ToastrService,
    protected translateService: TranslateService,
    private campaignAnwser: CampaignScriptAnwserServiceService,
    private router: Router
  ) {
  }

  ngOnInit(): void {

    this.checkND = true;
    this.columns = [
      {i18n: 'campain-script.anwsers.column.no', field: 'index', width: '70px'},
      {i18n: 'campain-script.anwsers.column.codeAnwser', field: 'code', width: '150px'},
      {i18n: 'campain-script.anwsers.column.content', field: 'content'},
      {i18n: 'campain-script.anwsers.column.codeQuestion', field: 'questionCode', width: '150px'},
      {i18n: 'campain-script.anwsers.column.type', field: 'type' , width: '70px'},
      {
        i18n: 'common.action',
        field: 'options',
        width: '120px',
        pinned: 'right',
        right: '0px',
        type: 'button',
        buttons: [
          {
            type: 'icon',
            text: 'edit',
            icon: 'edit',
            click: record => this.updateAnwser(record),
          },
          {
            type: 'icon',
            text: 'delete',
            icon: 'delete',
            click: record => this.deleteAnwser(record),
          }
        ]
      }
    ];
    this.formInput = new FormGroup({
      keyword: new FormControl(),
    });
    this.loadData();
    this.loadPositionAnwser();
  }

  getAllAnwser() {
  }


  loadData() {
    this.checkTypes();
    if (this.dataAnwser !== null) {
      // tslint:disable-next-line:forin
      this.scriptAnwser = this.dataAnwser;
      this.total = Number(this.scriptAnwser.length);

      // this.campaignScriptAnwser.get('code').setValue(this.scriptAnwser.code);
      // this.campaignScriptAnwser.get('type').setValue(this.scriptAnwser.type);
      // this.campaignScriptAnwser.get('position').setValue(this.scriptAnwser.position);
      // this.campaignScriptAnwser.get('display').setValue(this.scriptAnwser.display);
      // this.campaignScriptAnwser.get('content').setValue(this.scriptAnwser.content);
      // this.campaignScriptAnwser.get('min').setValue(this.scriptAnwser.min);
      // this.campaignScriptAnwser.get('max').setValue(this.scriptAnwser.max);
      // this.campaignScriptAnwser.get('status').setValue(this.scriptAnwser.status);
    }
  }

  checkTypes() {
    // tslint:disable-next-line:no-unused-expression
    const typeCheck = this.campaignScriptAnwser.get('type').value;
    if (typeCheck !== '4') {
      this.checkType = false;
    } else {
      this.checkType = true;
    }

  }

  reset(){
    this.campaignScriptAnwser.reset();
    this.loadPositionAnwser();
  }

  addScriptAnwser() {
    // tslint:disable-next-line:forin
    if (this.campaignScriptAnwser.get('content').value === null){
      this.checkND = false;
    }else {
      if (this.campaignScriptAnwser.get('action').value === null){
        this.campaignScriptAnwser.get('action').setValue('add');
      }
      for (const controlName in this.campaignScriptAnwser.controls) {
        this.addAnwser[controlName] = this.campaignScriptAnwser.controls[controlName].value;
      }
      this.dialogRef.close(this.addAnwser);
    }
  }

  updateAnwser(campaignScriptAnwser: CampaignScriptAnwserReponseModel) {
    this.campaignScriptAnwser.patchValue(campaignScriptAnwser);
    this.campaignScriptAnwser.get('action').setValue('updateAnwser');
    this.checkTypes();
  }

  deleteAnwser(campaignScriptAnwser: CampaignScriptAnwserReponseModel) {
    this.campaignScriptAnwser.patchValue(campaignScriptAnwser);
    this.campaignScriptAnwser.get('action').setValue('deleteAnwser');
    if (this.campaignScriptAnwser.get('action').value === 'deleteAnwser'){
      const questionId = this.campaignScriptAnwser.get('campaignScriptQuestionId').value;
      const dialogRef2 = this.dialog.open(PopupDeleteComponent, {
        data: this.campaignScriptAnwser.getRawValue()
      });
      dialogRef2.afterClosed().subscribe(result => {
        if (result === undefined) {
          return;
        }
        if (result.event === 'deleteAnwser') {
          this.campaignAnwser.deleteAnwser(questionId, this.campaignScriptAnwser.get('id').value).subscribe(rs => {
            if (rs.status === 200) {
              this.showNotifyDeleteSuccess();
              this.closeDialog();
            } else {
              this.showUnknownErr();
            }
          });
        }
      });
    }
  }

  loadPositionAnwser(){
    this.campaignAnwser.getPositionAnwser(this.dataA.questionId).subscribe(res =>{
      this.positionAnwser = res.body;
      this.campaignScriptAnwser.get('position').setValue(Number(this.positionAnwser +1));
    });
  }

  checkContent(){
    if (this.campaignScriptAnwser.get('content').value === null){
      this.checkBTN = true;
    } else {
      this.checkBTN = false;
    }
  }

  closeDialog() {
    this.dialogRef.close({event: 'cancel'});
  }

  onPageChange(event) {
    this.offset = event.pageIndex;
    this.pagesize = event.pageSize;
    // this.getAllCampaign();
  }

  showNotifyDeleteSuccess() {
    this.toast.success(this.translateService.instant('common.confirm.delete.success'));
  }

  showNotifySuccess() {
    this.toast.success(this.translateService.instant('common.notify.save.success2'));
  }

  showUnknownErr() {
    this.toast.error(this.translateService.instant('common.notify.fail'));
  }

}
