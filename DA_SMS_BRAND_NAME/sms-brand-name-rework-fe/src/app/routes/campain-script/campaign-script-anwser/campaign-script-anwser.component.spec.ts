import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CampaignScriptAnwserComponent } from './campaign-script-anwser.component';

describe('CampaignScriptAnwserComponent', () => {
  let component: CampaignScriptAnwserComponent;
  let fixture: ComponentFixture<CampaignScriptAnwserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CampaignScriptAnwserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CampaignScriptAnwserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
