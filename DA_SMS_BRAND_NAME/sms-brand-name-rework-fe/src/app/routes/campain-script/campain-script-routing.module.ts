import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CampainScriptComponent } from './campain-script/campain-script.component';

const routes: Routes = [
  {
    path: '',
    component: CampainScriptComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CampainScriptRoutingModule { }
