import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CampainScriptComponent } from './campain-script.component';

describe('CampainScriptComponent', () => {
  let component: CampainScriptComponent;
  let fixture: ComponentFixture<CampainScriptComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CampainScriptComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CampainScriptComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
