import {Component, EventEmitter, Inject, OnInit, Output} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialog} from '@angular/material/dialog';
import {CampaignScriptService} from '@app/core/services/campaign-script/campaign-script.service';
import {MtxDialog, MtxGridColumn} from '@ng-matero/extensions';
import {TranslateService} from '@ngx-translate/core';
import {ToastrService} from 'ngx-toastr';
import {Constant} from '@shared/Constant';
import * as fileSaver from 'file-saver';
import {CampaignScriptQuestionComponent} from '@app/routes/campain-script/campaign-script-question/campaign-script-question.component';
import {DialogScriptComponent} from '@app/routes/campain-script/dialog-script/dialog-script.component';
import {SampleCampaignFormComponent} from '@app/routes/callout-campaign-management/sample-campaign-form/sample-campaign-form.component';
import {CampaignUpdateComponent} from '@app/routes/campaign/campaign-update/campaign-update.component';
import {CampaignScriptModel} from '@core/models/campaign-script';
import {formatDate} from '@angular/common';
import {PopupDeleteComponent} from '@app/routes/directory-management/popup-delete/popup-delete.component';
import {CampaignScriptQuestionResponseModel} from '@core/models/campaign-script-question-response-model';
import {
  HtmlEditorService,
  ImageService,
  LinkService,
  TableService,
  ToolbarService
} from '@syncfusion/ej2-angular-richtexteditor';
import {CampaignScriptAnwserServiceService} from '@core/services/campaign-script/campaign-script-anwser.service';
import {CampaignScriptAnwserReponseModel} from '@core/models/campaign-script-anwser-reponse-model';
import {ConfirmDialogComponent, ConfirmDialogModel} from '@shared/components/confirm-dialog/confirm-dialog.component';
import {CopyCampaignScriptComponent} from '@app/routes/campain-script/copy-campaign-script/copy-campaign-script.component';

@Component({
  selector: 'app-campain-script',
  templateUrl: './campain-script.component.html',
  styleUrls: ['./campain-script.component.scss'],
  providers: [ToolbarService, LinkService, ImageService, HtmlEditorService, TableService]
})
export class CampainScriptComponent implements OnInit {
  @Output() pageChange: EventEmitter<any> = new EventEmitter<any>();

  public tools: object = {
    items: [
      'Bold', 'Italic', 'Underline', 'StrikeThrough', '|',
      'FontName', 'FontSize', 'FontColor', 'BackgroundColor', '|',
      'LowerCase', 'UpperCase', '|', 'Undo', 'Redo', '|',
      'Formats', 'Alignments', '|', 'OrderedList', 'UnorderedList', '|',
      'Indent', 'Outdent', '|', 'CreateLink', 'CreateTable',
      'Image', '|', 'ClearFormat', 'Print', 'SourceCode', '|', 'FullScreen'],
    type: 'Scrollable',
    enable: false,
    enableFloating: true
  };
  file: any = null;
  checkDeleteQuestion: number;
  campainScriptForm: FormGroup;
  formImport: FormGroup;
  listAction = [];
  data: any = [];
  dataUpdate: any = [];
  dataQuestion: any = [];
  dataAnwser: any = [];
  dataScript: any = [];
  total: any = 0;
  total2: any = 0;
  dataCampaignScript: any = [];
  today = new Date();
  jstoday = '';
  iconStatus1: boolean;
  iconStatus2: boolean;

  columns: MtxGridColumn[] = [
    {i18n: 'campain-script.question.column.no', field: 'index' , width: '70px'},
    {i18n: 'campain-script.question.column.code', field: 'code'},
    {i18n: 'campain-script.question.column.content', field: 'content'},
    {
      i18n: 'common.action',
      field: 'options',
      width: '120px',
      pinned: 'right',
      right: '0px',
      type: 'button',
      buttons: [
        {
          type: 'icon',
          text: 'edit',
          icon: 'edit',
          click: record => this.updateCampaignScriptQuestion(record, 'edit'),
        },
        {
          type: 'icon',
          text: 'delete',
          icon: 'delete',
          click: record => this.deleteCampaignScriptQuestion(record, 'delete'),
        },
      ]
    },
  ];
  columnsScript: MtxGridColumn[] = [
    {i18n: 'campain-script.management.column.no', field: 'index', width: '70px'},
    {i18n: 'campain-script.management.column.code', field: 'code'},
    {i18n: 'campain-script.management.column.name', field: 'name'},
    {i18n: 'campain-script.management.column.create-user', field: 'createUsername'},
    {i18n: 'campain-script.management.column.create-date', field: 'createDatetime', width: '100px'},
    {i18n: 'campain-script.management.column.update-user', field: 'updateUsername'},
    {i18n: 'campain-script.management.column.update-date', field: 'updateDatetime' , width: '100px'},
    // {i18n: 'campain-script.management.column.status', field: 'status' , width: '100px'},
    {
      i18n: 'common.action',
      field: 'options',
      width: '120px',
      pinned: 'right',
      right: '0px',
      type: 'button',

      // buttons: [
      //   {
      //     type: 'icon',
      //     text: 'edit',
      //     icon: 'edit',
      //     disabled: this.iconStatus1,
      //     click: record => this.updateCampaignScript(record),
      //   },
      //   {
      //     type: 'icon',
      //     text: 'delete',
      //     icon: 'delete',
      //     disabled: this.iconStatus1,
      //     click: record => this.deleteCampaignScript(record, 'confirm'),
      //   }
      // ]
    }
  ];
  searchText: any = '';
  offset = null;
  offset2 = null;
  buttonTitle: string;
  buttonTitle2: string;
  isLoading = false;
  checkQuesttion = true;
  pageSizeList = [10, 50, 100];
  pagesize = 10;
  pagesize2 = 10;
  formInput: FormGroup;
  formSearch: FormGroup;


  constructor(
    private fb: FormBuilder,
    protected translateService: TranslateService,
    private dialog: MatDialog,
    public dialog2: MtxDialog,
    private campaignScriptService: CampaignScriptService,
    private campaignAnwser: CampaignScriptAnwserServiceService,
    protected toast: ToastrService,
  ) {
  }

  ngOnInit(): void {
    this.offset = 0;
    this.pagesize = 10;
    this.jstoday = formatDate(this.today, 'dd-MM-yyyy', 'en-US', '+0530');
    this.listAction.push({
      icon: 'edit',
      tooltip: this.translateService.instant('common.button.edit'),
      color: 'primary',
      type: 'icon',
    });

    this.formInput = this.fb.group({
      id: [{value: null}],
      code: [{value: null, disabled: true}],
      position: [null, [Validators.required]],
      checkbox: [true, [Validators.required]],
      display: [null, [Validators.required]],
      content: [null, [Validators.required]],
      status: [null, [Validators.required]],
    });

    this.formImport = this.fb.group({
      checkDetele: true,
      file: null,
    });

    this.campainScriptForm = this.fb.group({
      id: new FormControl(null),
      code: new FormControl({value: null, disabled: true}, [Validators.required]),
      name: new FormControl('', [Validators.required]),
      startContent: new FormControl('', [Validators.required]),
      endContent: new FormControl('', [Validators.required]),
      status: new FormControl('',),
      createDatetime: new FormControl(null),
      createUserId: new FormControl(null),
      updateDatetime: new FormControl(null),
      updateUserId: new FormControl(null),
      createUsername: new FormControl(null),
      updateUsername: new FormControl(null),
      createFullName: new FormControl(null),
      updateFullName: new FormControl(null),
      errorCodeConfig: new FormControl(null),
    });

    this.formSearch = new FormGroup({
      keyword: new FormControl(),
    });
    this.getAllCampaignScript(true);
    this.checkButton();
  }

  checkButton() {
    if (this.campainScriptForm.get('id').value !== null) {
      this.buttonTitle = this.translateService.instant('common.button.update');
      this.buttonTitle2 = this.translateService.instant('common.button.back');
      this.iconStatus1 = true;
      this.iconStatus2 = true;
      this.checkQuesttion = false;
    } else {
      this.buttonTitle = this.translateService.instant('common.button.insert');
      this.buttonTitle2 = this.translateService.instant('common.button.copy');
      this.iconStatus1 = false;
      this.iconStatus2 = false;
      this.checkQuesttion = true;
    }
  }

  getQuestionInScript(reset?: boolean) {
    if (reset) {
      this.offset2 = 0;
      this.pagesize2 = 10;
    }
    const scriptId = this.campainScriptForm.get('id').value;
    this.campaignScriptService.getQuestionInScript(scriptId,{size: this.pagesize2, page: this.offset2}).subscribe(res => {
      console.log('.. res', res);
      this.dataQuestion = res.body;
      this.total2 = Number(res.headers.get('X-Total-Count'));
    });
  }

  getAnwserInScript(reset?: boolean) {
  }

  getAllCampaignScript(reset?: boolean) {
    if (reset) {
      this.offset = 0;
      this.pagesize = 10;
    }
    const data = {...this.campainScriptForm.value, queryType: 1};
    this.campaignScriptService.getAllCampaignScript(data, {size: this.pagesize, page: this.offset}).subscribe(res => {
      this.dataScript = res.body;
      console.log(res.headers.get('X-Total-Count'));
      this.total = Number(res.headers.get('X-Total-Count'));
    });
  }

  deleteCampaignScript(campaignScriptModel: CampaignScriptModel, action: string) {
    this.campainScriptForm.patchValue(campaignScriptModel);
    this.campainScriptForm.get('status').setValue('2');
    const scriptId = this.campainScriptForm.get('id').value;
      const title = this.translateService.instant('common.confirm.title.delete');
      const content = this.translateService.instant('common.MSG0017');
      const dialogContent: ConfirmDialogModel = new ConfirmDialogModel(title, content);
      this.dialog2.originalOpen(ConfirmDialogComponent, {
        width: '25vw',
        disableClose: true,
        hasBackdrop: true,
        data: dialogContent
      }).afterClosed().subscribe((next: any) => {
        if (next.data === Constant.DIALOG_CONFIRM) {
          this.campaignScriptService.updateStatusCampaignScript(scriptId, this.campainScriptForm.getRawValue()).subscribe(rs => {
            console.log('eee ' ,rs);
            if (rs.body.message === 'ok') {
              this.toast.success(this.translateService.instant('criteria_detail.msg_delete_success'));
              this.campainScriptForm.reset();
              this.getAllCampaignScript(false);
            } else {
              this.toast.error(this.translateService.instant('criteria_detail.msg_delete_error'));
            }
          });
        } else {
          this.campainScriptForm.reset();
        }
      });
  }

  updateCampaignScript(campaignScriptModel: CampaignScriptModel) {
    this.campainScriptForm.setValue(campaignScriptModel);
    this.checkButton();
    this.getQuestionInScript();
  }

  deleteCampaignScriptQuestion(
    campaignScriptQuestion: CampaignScriptQuestionResponseModel,
    action: string){
    const scriptId = this.campainScriptForm.get('id').value;
    this.campainScriptForm.get('status').setValue('2');
    const title = this.translateService.instant('common.confirm.title.delete');
    const content = this.translateService.instant('common.MSG0017');
    const dialogContent: ConfirmDialogModel = new ConfirmDialogModel(title, content);
    this.dialog2.originalOpen(ConfirmDialogComponent, {
      width: '25vw',
      disableClose: true,
      hasBackdrop: true,
      data: dialogContent
    }).afterClosed().subscribe((next: any) => {
      if (next.data === Constant.DIALOG_CONFIRM) {
        this.campaignScriptService.deleteQuestion(scriptId,campaignScriptQuestion.id).subscribe(rs => {
          if (rs.status !== 200) {
            this.toast.error(this.translateService.instant('criteria_detail.msg_delete_error'));
          } else {
            this.toast.success(this.translateService.instant('criteria_detail.msg_delete_success'));
            this.getQuestionInScript();
          }
        });
      }
    });
  }


  updateCampaignScriptQuestion(
    campaignScriptQuestion: CampaignScriptQuestionResponseModel,
    action: string) {
    // @ts-ignore
    const questionId2 = campaignScriptQuestion.id;
    this.campaignAnwser.getAnwserInQuestion(questionId2).subscribe(res => {
      this.dataAnwser = res;
      this.onOpenAnswersQuestion(campaignScriptQuestion);
    });
    this.offset = 0;
    this.pagesize = 10;
  }

  onOpenAnswersQuestion(campaignScriptQuestion: CampaignScriptQuestionResponseModel) {
    const dialogRef = this.dialog.open(DialogScriptComponent, {
      data: {
        data: campaignScriptQuestion,
        dataAnswer: this.dataAnwser,
        questionId: campaignScriptQuestion.id,
      },
      width: '80%',
      height: '700px',
      disableClose: true
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === undefined) {
        return;
      }
      if (result.event !== 'cancel') {
        if (result.action === 'addQuestion'){
          const scriptId = this.campainScriptForm.get('id').value;
          const questionId = campaignScriptQuestion.id;
          this.campaignScriptService.updateQuestion(scriptId, questionId, result).subscribe(rs => {
            if (rs.status !== 200) {
              this.showUnknownErr();
            } else {
              this.showNotifySuccess();
              this.getQuestionInScript(true);
            }
          });
        }
        if (result.action === 'updateAnwser'){
          const questionId = campaignScriptQuestion.id;
          const anwserId = result.id;
          if (result.type === '4'){
            if (result.min < '0' || result.max < '0'){
              this.showUnknownErr();
            }
            if (result.min >= result.max){
              this.showUnknownErr();
            }
          }
          this.campaignAnwser.updateAnwser(questionId,anwserId, result).subscribe(rs => {
            if (rs.body.message === 'ok') {
              this.showNotifySuccess();
              this.getQuestionInScript(true);
            } else {
              this.showUnknownErr();
            }
          });
        }
        if (result.action === 'add'){
          const questionId = campaignScriptQuestion.id;
          if (result.type === '4'){
            if (result.min < '0' || result.max < '0'){
              this.showUnknownErr();
            }
            if (result.min >= result.max){
              this.showUnknownErr();
            }
          }
          this.campaignAnwser.addAnwser(questionId, result).subscribe(rs => {
            if (rs.body.message === 'ok') {
              this.showNotifySuccess();
              this.getQuestionInScript(true);
              this.updateCampaignScriptQuestion(campaignScriptQuestion,result.action);
            } else {
              this.showUnknownErr();
            }
          });
        }
      }
    });
  }

  add() {
    if (this.campainScriptForm.get('id').value !== null) {
      const scriptId = this.campainScriptForm.get('id').value;
      this.campaignScriptService.updateCampaignScript(scriptId, this.campainScriptForm.value).subscribe((response: any) => {
        console.log('msg  ',response );
        if (response.body.message === 'ok') {
          this.toast.success(this.translateService.instant('common.notify.save.success'));
          this.getAllCampaignScript(true);
        } else {
          if(response.body.message === 'nameExist'){
            this.toast.error(this.translateService.instant('validation.MSG0019'));
          } else {
            this.toast.error(this.translateService.instant(response.statusText));
          }
        }
      }, () => {
        this.toast.warning(this.translateService.instant('validation.MSG0019'));
      });
    } else {
      this.campaignScriptService.addScript(this.campainScriptForm.value).subscribe((response: any) => {
        if (response.body.msgCode === Constant.OK) {
          this.toast.success(this.translateService.instant('common.notify.save.success2'));
          this.getAllCampaignScript(true);
          this.campainScriptForm.reset();
        } else {
          console.log('log aaa', response);
          if(response.body.msgCode === '401'){
            this.toast.error(this.translateService.instant('validation.MSG0019'));
          } else {
            this.toast.error(this.translateService.instant(response.body.message));
          }
        }
      }, () => {
        this.toast.warning(this.translateService.instant('validation.MSG0019'));
      });
    }
  }

  addQuestion(action: string) {
    const scriptId = this.campainScriptForm.get('id').value;
    console.log('111111 ', scriptId);
    const dialogRef = this.dialog.open(DialogScriptComponent, {
      data: {
        dataAnswer: null,
        data: null,
        scriptIdss: scriptId,
        checkTab: true,
      },
      width: '80%', height: '700px', disableClose: true,
      panelClass: 'trend-dialog',
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result === undefined) {
        return;
      }
      if (result.event !== 'cancel') {
        if (scriptId !== null) {
          console.log('check box ', result);
          if (result.checkbox === true) {
            result.display = '1';
          } else {
            result.display = '2';
          }
          result.status = '1';
          result.code = null;
          this.campaignScriptService.addQuestion(scriptId, result).subscribe(data => {
            if (data.status !== 201) {
              this.showUnknownErr();
            } else {
              this.showNotifySuccess();
              this.getQuestionInScript();
            }
          });
        } else {
          this.showUnknownErr();
        }
      }
    });
  }

  downloadTemplate() {
    this.campaignScriptService.downloadTemplate().subscribe(res => {
      if (res) {
        const fileName = res.headers.get('filename');
        this.saveFile(res.body, fileName);
      }
    }, error => {
      this.toast.error(this.translateService.instant('campaign-resource.error'));
    });
  }

  private saveFile(data: ArrayBuffer, fileName: string) {
    const blob = new Blob([data], {type: 'application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet; charset=UTF-8'});
    fileSaver.saveAs(blob, fileName);
  }


  copy() {
    if (this.campainScriptForm.get('id').value !== null){
      this.campainScriptForm.reset();
      this.dataQuestion = [];
      this.checkButton();
    }else {
      const dialogCopy = this.dialog.open(CopyCampaignScriptComponent,{
        data: null,
        width: '50%', disableClose: true,
        panelClass: 'trend-dialog',
      });
      dialogCopy.afterClosed().subscribe(result => {
        this.getAllCampaignScript(true);
      });
    }
  }

  import() {
    this.formImport.markAllAsTouched();
    const scriptId = this.campainScriptForm.get('id').value;
    if(this.formImport.get('checkDetele').value === true){
      this.checkDeleteQuestion = 1;
    } else {
      this.checkDeleteQuestion = 2;
    }
    if (this.formImport.get('file').value === null) {
      this.toast.warning(this.translateService.instant('campaign-email-marketing.file-required'));
      return;
    }
    this.file = this.formImport.get('file').value;
    this.campaignScriptService.importFile(scriptId, this.checkDeleteQuestion, this.file._files[0]).subscribe(res => {
      this.toast.success(this.translateService.instant('campain-script.import-excel'));
      this.getQuestionInScript();
      this.formImport.get('file').reset();
    }, error => {
      this.toast.error(this.translateService.instant('campain-script.import-excel-error'));
    });

  }

  search() {
  }

  uploadFile(event) {
    // const scriptId = this.campainScriptForm.get('id').value;
    // this.checkDeleteQuestion = 0;
    // if (this.file) {
    //   this.campaignScriptService.uploadFile(scriptId, this.checkDeleteQuestion,this.file._files[0]).subscribe(res => {
    //     if (res) {
    //       this.toast.success(this.translateService.instant('campaign-resource.upload-success'));
    //     }
    //   }, error => {
    //     this.toast.error(error.error.title);
    //   });
    // } else {
    //   this.toast.warning(this.translateService.instant('campaign-resource.must-select-file'));
    //   return;
    // }
  }

  closeDialog() {
    this.dialog.closeAll();
  }

  onPageChange(event) {
    this.offset2 = event.pageIndex;
    this.pagesize2 = event.pageSize;
    this.getQuestionInScript();
  }

  onPageChangeScript(event) {
    this.offset = event.pageIndex;
    this.pagesize = event.pageSize;
    this.getAllCampaignScript();
  }

  showNotifyDeleteSuccess() {
    this.toast.success(this.translateService.instant('common.confirm.delete.success'));
  }

  showNotifySuccess() {
    this.toast.success(this.translateService.instant('common.notify.save.success2'));
  }

  showUnknownErr() {
    this.toast.error(this.translateService.instant('common.notify.fail'));
  }
}
