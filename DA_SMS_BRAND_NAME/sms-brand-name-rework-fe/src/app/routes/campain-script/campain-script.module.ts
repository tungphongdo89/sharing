import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CampainScriptRoutingModule } from './campain-script-routing.module';
import { CampainScriptComponent } from './campain-script/campain-script.component';
import { SharedModule } from '@app/shared';
import { CampaignScriptQuestionComponent } from './campaign-script-question/campaign-script-question.component';
import { DialogScriptComponent } from './dialog-script/dialog-script.component';
import { CampaignScriptAnwserComponent } from './campaign-script-anwser/campaign-script-anwser.component';
import {RichTextEditorModule} from '@syncfusion/ej2-angular-richtexteditor';
import {Ng2SearchPipeModule} from 'ng2-search-filter';
import { CopyCampaignScriptComponent } from './copy-campaign-script/copy-campaign-script.component';

@NgModule({
  declarations: [CampainScriptComponent, CampaignScriptQuestionComponent, DialogScriptComponent, CampaignScriptAnwserComponent, CopyCampaignScriptComponent],
    imports: [
        CommonModule,
        SharedModule,
        CampainScriptRoutingModule,
        RichTextEditorModule,
        Ng2SearchPipeModule
    ]
})
export class CampainScriptModule { }
