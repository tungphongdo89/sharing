import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogScriptComponent } from './dialog-script.component';

describe('DialogScriptComponent', () => {
  let component: DialogScriptComponent;
  let fixture: ComponentFixture<DialogScriptComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogScriptComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogScriptComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
