import {Component, Inject, Input, OnInit, Optional} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {CampaignScriptQuestionResponseModel} from '@core/models/campaign-script-question-response-model';
import {CampaignScriptAnwserReponseModel} from '@core/models/campaign-script-anwser-reponse-model';
import {TranslateService} from '@ngx-translate/core';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-dialog-script',
  templateUrl: './dialog-script.component.html',
  styleUrls: ['./dialog-script.component.scss']
})
export class DialogScriptComponent implements OnInit {
  @Input() dataAnswer: CampaignScriptAnwserReponseModel;
  @Input() data: CampaignScriptQuestionResponseModel;
  @Input() scriptIdss: number;
  @Input() questionId: number;
  @Input() checkTab: boolean;
  // checkTab: boolean = true;

  constructor(
    public dialogRef: MatDialogRef<DialogScriptComponent>,
    protected translateService: TranslateService,
    protected toast: ToastrService,
    @Optional() @Inject(MAT_DIALOG_DATA) public d,
  ) {
  }

  ngOnInit(): void {
    // console.log('question ', this.d.data);
    // console.log('answer ', this.d.dataAnswer);
    // console.log('tab boolean ', this.d.checkTab);
    // console.log('id queâtion  ', this.d.questionId);
  }

  closeDialog() {
    this.dialogRef.close({event: 'cancel'});
  }

  showUnknownErr() {
    this.toast.error(this.translateService.instant('common.notify.fail'));
  }

}
