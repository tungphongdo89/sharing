import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CampaignBlacklistComponent } from './campaign-blacklist/campaign-blacklist.component';
import {SharedModule} from '@shared';
import {CampaignBlacklistRouting} from '@app/routes/campaign-blacklist/campaign-blacklist-routing';


@NgModule({
  declarations: [CampaignBlacklistComponent],
  imports: [
    CommonModule,
    SharedModule,
    CampaignBlacklistRouting
  ]
})
export class CampaignBlacklistModule { }
