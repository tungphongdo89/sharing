import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CampaignBlacklistComponent } from './campaign-blacklist.component';

describe('CampaignBlacklistComponent', () => {
  let component: CampaignBlacklistComponent;
  let fixture: ComponentFixture<CampaignBlacklistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CampaignBlacklistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CampaignBlacklistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
