import {Component, EventEmitter, OnInit, Output, TemplateRef, ViewChild} from '@angular/core';
import {CampaignBlacklistService} from '@core/services/campaign-blacklist/campaign-blacklist.service';
import {AbstractControl, FormBuilder, FormControl, FormGroup, ValidationErrors, Validators} from '@angular/forms';
import {CampaignBlacklistInterface} from '@core/models/campaign-blacklist.interface';
import {MtxDialog, MtxGridColumn} from '@ng-matero/extensions';
import {TranslateService} from '@ngx-translate/core';
import {ToastrService} from 'ngx-toastr';
import {ConfirmDialogComponent, ConfirmDialogModel} from '@shared/components/confirm-dialog/confirm-dialog.component';
import {Constant} from '@shared/Constant';

@Component({
  selector: 'app-campaign-blacklist',
  templateUrl: './campaign-blacklist.component.html',
  styleUrls: ['./campaign-blacklist.component.scss']
})
export class CampaignBlacklistComponent implements OnInit {
  @ViewChild('button', {static: true}) button: TemplateRef<any>;
  @Output() pageChange: EventEmitter<any> = new EventEmitter<any>();
  @ViewChild('createDate', {static: true}) createDate: TemplateRef<any>;
  @ViewChild('campaignId', {static: true}) campaignId: TemplateRef<any>;
  action: 'ADD' | 'SEARCH';
  lstCampaignBlackList: CampaignBlacklistInterface[] = [];
  campaignBlackListTable: MtxGridColumn[] = [];
  lstCampaign: any[] = [];
  lstRowSelect: CampaignBlacklistInterface[] = [];
  pagesize = 10;
  pageSizeList = [10, 50, 100];
  isLoading: true;
  campaignBlackListForm: FormGroup = this.fb.group({
    campaignId: new FormControl(null),
    phoneNumber: new FormControl(null),
  });
  listAction = [];
  displayedColumns = ['phoneNumber', 'campaignId', 'createUserName', 'createDate'];
  offset = 0;
  total = 0;

  constructor(
    private campaignBlackListService: CampaignBlacklistService,
    private fb: FormBuilder,
    private translateService: TranslateService,
    private toastr: ToastrService,
    private dialog: MtxDialog
  ) {
  }

  ngOnInit(): void {
    this.campaignBlackListService.getAllCampaign().subscribe(res => {
      this.lstCampaign = res;
      console.log(this.lstCampaign);
    });
    this.search();
    this.campaignBlackListTable = [
      {i18n: 'stt', field: 'no', width: '30px'},
      {i18n: 'campaign_blacklist.phoneNumber', field: 'phoneNumber'},
      {i18n: 'campaign_blacklist.campaignId', field: 'campaignId', cellTemplate: this.campaignId},
      {i18n: 'campaign_blacklist.createUser', field: 'createUserName',},
      {i18n: 'campaign_blacklist.createDate', field: 'createDate', cellTemplate: this.createDate},
    ];
  }

  onPageChange(event) {
    // this.pageChange.emit(event);
    this.offset = event.pageIndex;
    this.pagesize = event.pageSize;
    this.search();
  }

  search() {
    this.campaignBlackListForm.get('campaignId').setErrors(null);
    this.campaignBlackListForm.get('phoneNumber').setErrors(null);
    const formVal = {...this.campaignBlackListForm.getRawValue(), size: this.pagesize, page: this.offset};
    this.campaignBlackListService.search(formVal).subscribe(res => {
      this.lstCampaignBlackList = res.body;
      this.lstRowSelect=[];
      this.total = Number(res.headers.get('X-Total-Count'));
    });
  }

  save() {
    const campaignIdControl = this.campaignBlackListForm.get('campaignId');
    const phoneNumberControl = this.campaignBlackListForm.get('phoneNumber');
    const campaignId = campaignIdControl.value;
    const phoneNumber = phoneNumberControl.value;
    this.campaignBlackListForm.markAllAsTouched();
    if (!campaignId) campaignIdControl.setErrors({required: true});
    if (!phoneNumber) {
      phoneNumberControl.setErrors({required: true});
      return;
    }
    if (!phoneNumber.match(/^(?:[0-9]{9,15},)*[0-9]{9,15}$/)) {
      phoneNumberControl.setErrors({pattern: true});
    }
    // this.campaignBlackListForm.get('phoneNumber').setErrors(this.validatePassword);
    if (this.campaignBlackListForm.invalid) {
      return;
    }
    const formVal = this.campaignBlackListForm.getRawValue();
    this.campaignBlackListService.create(formVal).subscribe( res  => {
      console.log('res',res);
      // if (res.status?.code==='200'){
        this.campaignBlackListForm.reset();
        this.search();
        this.showNotifySuccess();
      // }
    }, (error) => {
      this.toastr.error(error?.error.message);
    });
  }

  onRowSelect(event: any[]) {
    console.log('row', event);
    this.lstRowSelect = event;
  }

  deleteAll() {
    if (this.lstRowSelect.length < 1) {
      this.toastr.warning(this.translateService.instant('Chưa có bản ghi nào được chọn'));
      return;
    }
    const title = this.translateService.instant('common.confirm.title.delete');
    const content = this.translateService.instant('common.MSG0017');
    const dialogContent: ConfirmDialogModel = new ConfirmDialogModel(title, content);
    this.dialog.originalOpen(ConfirmDialogComponent, {
      width: '25vw',
      disableClose: true,
      hasBackdrop: true,
      data: dialogContent
    }).afterClosed().subscribe((next: any) => {
      console.log(next);
      if (next.data === Constant.DIALOG_CONFIRM) {
        this.campaignBlackListService.deleteListPhoneNumber(this.lstRowSelect).subscribe(res => {
          this.showNotifyDeleteSuccess();
          this.search();
        });
      }
    });
  }

  validatePassword(control: AbstractControl): ValidationErrors {
    if (!control.value) {
      return null;
    }
    if (/^(?:[0-9]{9,15},)*[0-9]{9,15}$/.test(control.value)) {
      return null;
    }
    return {pattern: true};
  }

  showNotifyDeleteSuccess() {
    this.toastr.success(this.translateService.instant('common.attachment.delete.success'));
  }

  showNotifySuccess() {
    this.toastr.success(this.translateService.instant('common.notify.save.success2'));
  }

  showUnknownErr() {
    this.toastr.error(this.translateService.instant('common.notify.save.error'));
  }

  checkExistPhoneNumber() {
    this.toastr.error(this.translateService.instant('common.notify.save.error'));
  }
}
