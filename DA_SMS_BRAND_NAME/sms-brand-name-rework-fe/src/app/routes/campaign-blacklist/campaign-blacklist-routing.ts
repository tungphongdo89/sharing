import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {CampaignBlacklistComponent} from '@app/routes/campaign-blacklist/campaign-blacklist/campaign-blacklist.component';

const routes: Routes = [
  {
    path: '',
    component: CampaignBlacklistComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CampaignBlacklistRouting {
}
