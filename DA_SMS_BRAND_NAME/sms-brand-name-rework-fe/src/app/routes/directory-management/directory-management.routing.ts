import {Routes, RouterModule} from '@angular/router';
import {NgModule} from '@angular/core';
import {DirectoryManagementComponent} from '@app/routes/directory-management/directory-management.component';

const routes: Routes = [
  {
    path: '',
    component: DirectoryManagementComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DirectoryManagementRouting {
}
