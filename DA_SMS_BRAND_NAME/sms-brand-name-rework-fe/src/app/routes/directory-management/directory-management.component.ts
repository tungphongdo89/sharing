import {TranslateService} from '@ngx-translate/core';
import {OptionSetValueCreateComponent} from './option-set-value-create/option-set-value-create.component';
import {MatDialog} from '@angular/material/dialog';
import {Component, EventEmitter, Input, OnInit, Output, TemplateRef, ViewChild} from '@angular/core';
import {MtxDialog, MtxGridColumn} from '@ng-matero/extensions';
import {OptionSetModel} from '@core/models/option-set-model';
import {OptionSetService} from '@core/services/option/option-set';
import {OptionSetValueModel} from '@core/models/option-set-value';
import {OptionSetValueService} from '@core/services/option/option-set-value';
import {ToastrService} from 'ngx-toastr';
import {Constant} from '@app/shared/Constant';
import {OptionSetCreateComponent} from '@app/routes/directory-management/option-set-create/option-set-create.component';
import {FormBuilder, FormGroup} from '@angular/forms';
import {OptionSetUpdateComponent} from './option-set-update/option-set-update.component';
import {OptionSetValueUpdateComponent} from './option-set-value-update/option-set-value-update.component';
import {OptionSetValueDeleteComponent} from './option-set-value-delete/option-set-value-delete.component';
import {PopupDeleteComponent} from '@app/routes/directory-management/popup-delete/popup-delete.component';
import {ConfirmDialogComponent, ConfirmDialogModel} from '@shared/components/confirm-dialog/confirm-dialog.component';

@Component({
  selector: 'app-directory-management',
  templateUrl: './directory-management.component.html',
  styleUrls: ['./directory-management.component.scss']
})
export class DirectoryManagementComponent implements OnInit {
  @Input() dataDirectoryType: OptionSetModel[] = [];
  @Input() dataDirectory: OptionSetValueModel[] = [];
  @Output() pageChange: EventEmitter<any> = new EventEmitter<any>();
  @ViewChild('button', {static: true}) button: TemplateRef<any>;
  @ViewChild('buttondelete', {static: true}) buttondelete: TemplateRef<any>;
  @ViewChild('effectDate', {static: true}) effectDate: TemplateRef<any>;
  @ViewChild('expiredDate', {static: true}) expiredDate: TemplateRef<any>;
  @ViewChild('createDate', {static: true}) createDate: TemplateRef<any>;
  @ViewChild('updateDate', {static: true}) updateDate: TemplateRef<any>;
  @ViewChild('effectDate1', {static: true}) effectDate1: TemplateRef<any>;
  @ViewChild('expiredDate1', {static: true}) expiredDate1: TemplateRef<any>;
  @ViewChild('createDate1', {static: true}) createDate1: TemplateRef<any>;
  @ViewChild('updateDate1', {static: true}) updateDate1: TemplateRef<any>;
  limit = 10;
  start = 0;
  REGEX = /^[-.a-zA-Z0-9_]{1,}$/;
  columns: MtxGridColumn[] = [];
  columnsDirectory: MtxGridColumn[] = [];
  pagesizeOptionSet = 10;
  offsetOptionSet: any = 0;
  pagesize = 10;
  offset: any = 0;
  pageSizeList = [10, 50, 100];
  listAction = [];
  isLoading = true;
  confirmDialog: boolean;
  isAdvSearch = false;
  searchParam: FormGroup = this.fb.group({
    name: null,
    code: null,
    status: '1',
  });
  searchSubParam: FormGroup = this.fb.group({
    name: null,
    code: null,
    status: '1',
    optionSetId: 0
  });

  listStatus: any = [];
  total: any = 0;
  totalOptionSet: any = 0;
  fileToUpload: File | null = null;
  optionValue: any;

  constructor(private optionService: OptionSetService,
              private optionValueService: OptionSetValueService,
              private dialog: MatDialog,
              private mtxDialog: MtxDialog,
              private translate: TranslateService,
              private fb: FormBuilder,
              private toast: ToastrService,
              protected translateService: TranslateService) {
  }

  ngOnInit(): void {
    this.columns = [
      {i18n: 'code', field: 'code'},
      {i18n: 'name', field: 'name'},
      {i18n: 'effectDate', field: 'fromDate', cellTemplate: this.effectDate},
      {i18n: 'expiredDate', field: 'endDate', cellTemplate: this.expiredDate},
      {i18n: 'status', field: 'status',},
      {i18n: 'createDate', field: 'createDate', cellTemplate: this.createDate},
      {i18n: 'userCreate', field: 'createUserName'},
      {i18n: 'updateDate', field: 'updateDate', cellTemplate: this.updateDate},
      {i18n: 'updateBy', field: 'updateUserName'},
      {
        i18n: 'common.action',
        field: 'options',
        width: '200px',
        pinned: 'right',
        right: '0px',
        type: 'button',
        buttons: [
          {
            type: 'icon',
            text: 'edit',
            icon: 'edit',
            click: record => this.updateOptionSet(record, 'edit'),
          },
          // {
          //   type: 'icon',
          //   text: 'delete',
          //   icon: 'delete',
          //   click: record => this.deleteOptionSet(record, 'delete'),
          // },
          {
            type: 'icon',
            text: 'delete',
            icon: 'delete',
            click: record => this.deleteStatus(record),
          }
        ]
      }
    ];
    this.columnsDirectory = [
      {i18n: 'code', field: 'code'},
      {i18n: 'name', field: 'name'},
      {i18n: 'index', field: 'ord'},
      {i18n: 'group', field: 'groupName'},
      {i18n: 'effectDate', field: 'fromDateView', cellTemplate: this.effectDate1},
      {i18n: 'expiredDate', field: 'endDateView', cellTemplate: this.expiredDate1},
      {i18n: 'status', field: 'status'},
      {i18n: 'createDate', field: 'createDateView', cellTemplate: this.createDate1},
      {i18n: 'userCreate', field: 'createUserName'},
      {i18n: 'updateDate', field: 'updateDateView', cellTemplate: this.updateDate1},
      {i18n: 'updateBy', field: 'updateUserName'},
      {
        i18n: 'common.action',
        field: 'options',
        width: '200px',
        pinned: 'right',
        right: '0px',
        type: 'button',
        buttons: [
          {
            type: 'icon',
            text: 'edit',
            icon: 'edit',
            click: record => this.updateOptionSetValue(record, 'edit'),
          }, {
            type: 'icon',
            text: 'delete',
            icon: 'delete',
            click: record => this.deleteOptionSetValue(record),
          }
        ]
        // cellTemplate: this.button
      }
    ];
    this.offset = 0;
    this.pagesize = 10;
    this.offsetOptionSet = 0;
    this.pagesizeOptionSet = 10;
    this.isLoading = false;
    this.getListOptionSet(true);
    this.search(true);
    this.initLstStatus();
  }

  getListOptionSet(reset?: boolean) {
    if (reset === true) {
      this.offsetOptionSet = 0;
      this.pagesizeOptionSet = 10;
    }
    const data = {...this.searchParam.value, queryType: 1};
    this.optionService.query(data, {size: this.pagesizeOptionSet, page: this.offsetOptionSet}).subscribe(res => {
      this.dataDirectoryType = res.body;
      this.totalOptionSet = Number(res.headers.get('X-Total-Count'));
    });
  }

  initLstStatus() {
    this.listStatus = [{title: this.translate.instant('label.active'), value: '1'}, {
      title: this.translate.instant('label.inActive'), value: '0'
    }];
  }

  /*  getListOptionSetValue() {
      this.optionValueService.getOptionSetValueList().subscribe(res => {
        this.dataDirectory = res;
      });
    }*/

  search(reset?: boolean, id?: number) {
    if (reset === true) {
      this.offset = 0;
      this.pagesize = 10;
    }
    if(!id){
      this.searchSubParam.patchValue({ optionSetId: null });
      const data = { ...this.searchSubParam.value };
      this.optionValueService.query(data, {size: this.pagesize, page: this.offset}).subscribe(res => {
        this.dataDirectory = res.body;
        this.total = Number(res.headers.get('X-Total-Count'));
        return;
      });
    } else {
      this.searchSubParam.patchValue({ optionSetId: id });
      const data = { ...this.searchSubParam.value, queryType: 1 };
      console.log('data: ', data);
      this.optionValueService.query(data, { size: this.pagesize, page: this.offset }).subscribe(res => {
        this.dataDirectory = res.body;
        this.total = Number(res.headers.get('X-Total-Count'));
      });
    }
  }

  onPageChangeOptionSet(event) {
    this.offsetOptionSet = event.pageIndex;
    this.pagesizeOptionSet = event.pageSize;
    this.getListOptionSet(false);
  }
  onPageChangeOptionSetValue(event) {
    this.offset = event.pageIndex;
    this.pagesize = event.pageSize;
    this.search(false);
  }

  createOptionSetValue(action: string) {
    const optionSetValue: any = {};
    optionSetValue.action = action;
    optionSetValue.ord = this.dataDirectory.length + 1;
    optionSetValue.optionSets = this.dataDirectoryType;
    const dialogRef = this.mtxDialog.open( {
      disableClose: true,
      hasBackdrop: true,
      width: '70vw',
      data: optionSetValue
    }, OptionSetValueCreateComponent);

    dialogRef.afterClosed().subscribe(result => {
      if (result === undefined) {
        return;
      }
      if (result.event === 'add') {
        console.log(result.data);
        this.optionValueService.createOptionSetValue(result.data).subscribe(res => {
          console.log(res);
          if (res.msgCode === Constant.OK) {
            this.showNotifySuccess();
            this.search(true);
          } else {
            this.showUnknownErr();
          }
        });
      }
    });
  }

  addOptionSet(action: string) {
    const newOptionSet: any = {};
    newOptionSet.action = action;
    const dialogRef = this.dialog.open(OptionSetCreateComponent, {
      data: newOptionSet
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result === undefined) {
        return;
      }
      if (result.event !== 'cancel') {
        this.optionService.addOptionSet(result).subscribe(data => {
          console.log(data);
          if (Constant.OK === data.msgCode) {
            this.showNotifySuccess();
            this.getListOptionSet();
          } else {
            this.showUnknownErr();
          }
        });
      }
    });
  }

  deleteStatus(u: any) {
    const title = this.translateService.instant('common.confirm.title.delete');
    const content = this.translateService.instant('common.MSG0017');
    const dialogContent: ConfirmDialogModel = new ConfirmDialogModel(title, content);
    this.mtxDialog.originalOpen(ConfirmDialogComponent, {
      width: '25vw',
      disableClose: true,
      hasBackdrop: true,
      data: dialogContent,
    }).afterClosed().subscribe((next: any) => {
      if (next.data === 'confirm') {
        this.optionService.deleteOptionSetStatus(u).subscribe(rs => {
          if (rs.msgCode === Constant.OK) {
            this.showNotifyDeleteSuccess();
            this.getListOptionSet();
          } else {
            this.showUnknownErr();
          }
        });
      }
    });
  }

  showNotifyDeleteSuccess() {
    this.toast.success(this.translateService.instant('common.notify.change.status.success'));
  }

  showNotifyUpdateSuccess() {
    this.toast.success(this.translateService.instant('common.confirm.update.success'));
  }

  showNotifySuccess() {
    this.toast.success(this.translateService.instant('common.notify.save.success2'));
  }

  showUnknownErr() {
    this.toast.error(this.translateService.instant('common.notify.save.error'));
  }

  export() {
    const data = {...this.searchSubParam.value};
    this.optionValueService.export(data);
  }

  updateOptionSet(optionset: OptionSetModel, action: string) {
    const dialogRef = this.dialog.open(OptionSetUpdateComponent, {data: optionset});
    // update
    dialogRef.afterClosed().subscribe(result => {
      if (result === undefined) {
        return;
      }
      if (result.event !== 'cancel') {
        this.optionService.updateOptionSet(result).subscribe(rs => {
          if (rs.msgCode === Constant.OK) {
            this.showNotifySuccess();
            this.getListOptionSet();
          } else {
            this.showUnknownErr();
          }
        });
      }
    });
  }

  updateOptionSetValue(optionSetValue: any, action: string) {
    optionSetValue.listOptionSet = this.dataDirectoryType;
    const dialogRef = this.dialog.open(OptionSetValueUpdateComponent, {data: optionSetValue});
    // update
    dialogRef.afterClosed().subscribe(result => {
      if (result === undefined) {
        return;
      }
      if (result.event !== 'cancel') {
        this.optionValueService.updateOptionSetValue(result.data).subscribe(rs => {
          if (rs.msgCode === Constant.OK) {
            this.showNotifyUpdateSuccess();
            this.search(true);
          } else {
            this.showUnknownErr();
          }
        });
      }
    });
  }

  deleteOptionSetValue(u: OptionSetValueModel) {
    const title = this.translateService.instant('common.confirm.title.delete');
    const content = this.translateService.instant('common.MSG0017');
    const dialogContent: ConfirmDialogModel = new ConfirmDialogModel(title, content);
    this.mtxDialog.originalOpen(ConfirmDialogComponent, {
      width: '25vw',
      disableClose: true,
      hasBackdrop: true,
      data: dialogContent,
    }).afterClosed().subscribe((next: any) => {
      if (next.data === 'confirm') {
        this.optionValueService.updateStatusOptionSetValue(u).subscribe(res => {
          console.log(res);
          if (res.msgCode === Constant.OK) {
            this.showNotifyDeleteSuccess();
            this.search(true);
          } else {
            this.showUnknownErr();
          }
        });
      }
    });
  }

  handleFileInput(file: FileList) {
    this.fileToUpload = file.item(0);
    this.optionService.importOption(this.fileToUpload);
  }

  clickShowFileUpload() {
    return document.getElementById('fileUpload').click();
  }
  getOptionSetId(id: any){
    // if(!id){
    //   console.log('khoog co id');
    // } else {
    //   this.search(true);
    // }
  }
}
