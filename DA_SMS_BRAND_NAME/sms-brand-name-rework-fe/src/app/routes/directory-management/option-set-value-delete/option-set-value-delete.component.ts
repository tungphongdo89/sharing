import { Component, Inject, OnInit, Optional } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { OptionSetValueModel } from '@app/core/models/option-set-value';

@Component({
  selector: 'app-option-set-value-delete',
  templateUrl: './option-set-value-delete.component.html',
  styleUrls: ['./option-set-value-delete.component.scss']
})
export class OptionSetValueDeleteComponent implements OnInit {

  private action:string;
  optionSetValue: any = {}
  constructor(public dialogRef: MatDialogRef<OptionSetValueDeleteComponent>,
    @Optional() @Inject(MAT_DIALOG_DATA) public data: any
) {
  this.action = 'delete';
  this.optionSetValue = data;
  }

  ngOnInit(): void {
  }

  close() {
    this.dialogRef.close({ event: 'cancel' });
  }

  delete(){
    this.dialogRef.close({event: this.action, data: this.optionSetValue});
  }
}
