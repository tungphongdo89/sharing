import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OptionSetValueDeleteComponent } from './option-set-value-delete.component';

describe('OptionSetValueDeleteComponent', () => {
  let component: OptionSetValueDeleteComponent;
  let fixture: ComponentFixture<OptionSetValueDeleteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OptionSetValueDeleteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OptionSetValueDeleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
