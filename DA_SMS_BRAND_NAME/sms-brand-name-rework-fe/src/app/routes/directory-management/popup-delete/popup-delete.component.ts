
import { Component, OnInit, Optional, Inject } from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';


@Component({
  selector: 'app-popup-delete',
  templateUrl: './popup-delete.component.html',
  styleUrls: ['./popup-delete.component.scss']
})
export class PopupDeleteComponent implements OnInit {

  action: string;
  localData:any;

  constructor(
    public dialogRef: MatDialogRef<PopupDeleteComponent>,
    @Optional() @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.action = data.action;
   }

  ngOnInit() {
  }

  //Click vào xóa
  doAction(){
    this.dialogRef.close({event:this.action});
  }

  //Click vào bỏ qua
  closeDialog(){
    this.dialogRef.close({event:'Cancel'});
  }

}
