import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OptionSetCreateComponent } from './option-set-create.component';

describe('OptionSetCreateComponent', () => {
  let component: OptionSetCreateComponent;
  let fixture: ComponentFixture<OptionSetCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OptionSetCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OptionSetCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
