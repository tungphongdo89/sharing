import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {MatDialogRef} from "@angular/material/dialog";
import {ValidationService} from '@shared/common/validation.service';

@Component({
  selector: 'app-option-set-create',
  templateUrl: './option-set-create.component.html',
  styleUrls: ['./option-set-create.component.scss']
})
export class OptionSetCreateComponent implements OnInit {
  formInput: FormGroup;
  constructor(
    private fb: FormBuilder,
    public dialogRef: MatDialogRef<OptionSetCreateComponent>) { }
    private action: string;
    private optionSet: any = {};

  ngOnInit(): void {
  //   this.formInput = new FormGroup({
  //     keyword: new FormControl(),
  //     code: new FormControl(),
  //     name: new FormControl(),
  //     engName: new FormControl(),
  //     fromDate: new FormControl(),
  //     endDate: new FormControl()
  //   });
  // }
    this.formInput = this.fb.group({
      code: ['', [Validators.required]],
      name: ['', [Validators.required]],
      engName: ['', [Validators.required]],
      fromDate: ['', [Validators.required]],
      endDate: ['', [Validators.required]],
      status: [{value: '', disabled: true}, [Validators.maxLength(255)]],
    }, {validator: ValidationService.notBefore("endDate", "fromDate", 'notBefore')});
  }
  submit(){}

  saveOptionSet() {
    // tslint:disable-next-line:forin
    for (const a in this.formInput.controls) {
      this.optionSet[a] = this.formInput.controls[a].value;
    }
    this.dialogRef.close(this.optionSet);
  }
  closeDialog() {
    this.dialogRef.close({event: 'cancel'});
  }
}
