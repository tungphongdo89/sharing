import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OptionSetValueUpdateComponent } from './option-set-value-update.component';

describe('OptionSetValueUpdateComponent', () => {
  let component: OptionSetValueUpdateComponent;
  let fixture: ComponentFixture<OptionSetValueUpdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OptionSetValueUpdateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OptionSetValueUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
