import { Component, Inject, OnInit, Optional } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { OptionSetValueModel } from '@app/core/models/option-set-value';
import { OptionSetService } from '@app/core/services/option/option-set';
import { OptionSetValueService } from '@app/core/services/option/option-set-value';

@Component({
  selector: 'app-option-set-value-update',
  templateUrl: './option-set-value-update.component.html',
  styleUrls: ['./option-set-value-update.component.scss']
})
export class OptionSetValueUpdateComponent implements OnInit {

  public optionsSetValueForm: FormGroup;
  optionSetValue: any = {}
  private action: string;
  listStatus: any[]= [{value: '1', name: 'Có hiệu lực'}, {value: '0', name: 'Hết hiệu lực'}];
  listOptionSet:any;


  constructor(public dialogRef: MatDialogRef<OptionSetValueUpdateComponent>,
    @Optional() @Inject(MAT_DIALOG_DATA) public data: any,
    private fb: FormBuilder, private optionValueService: OptionSetValueService,
    private optionSetService: OptionSetService) {
    this.optionSetValue = data,
    this.action = 'edit';
    this.listOptionSet = data.listOptionSet;
  }

  ngOnInit(): void {
    this.optionsSetValueForm = this.fb.group({
      optionSetName: [{value: '', disabled: true}, [Validators.required]],
      code: ['', [Validators.required, Validators.maxLength(255)]],
      name: ['', [Validators.required, Validators.maxLength(255)]],
      englishName: ['', [Validators.required, Validators.maxLength(255)]],
      ord: ['', [Validators.required]],
      groupName: ['', [Validators.maxLength(255)]],
      fromDateView: ['', [Validators.required]],
      endDateView: [''],
      status: [''],
    }, {validator: this.notBefore('endDateView', 'fromDateView', 'notBefore')})
    this.loadData();
  }

  loadData() {
    this.optionValueService.getOptionSetValueById(this.optionSetValue.id).subscribe(res => {
      this.optionSetValue.optionSetId = res.optionSetId;
      this.optionSetService.getOptionSetById(res.optionSetId).subscribe(data => {
        console.log(data);
        this.optionSetValue.optionSetName = data.name;
        this.optionsSetValueForm.controls['optionSetName'].setValue(this.optionSetValue['optionSetName']);
      })
    })
    for (let controlName in this.optionsSetValueForm.controls) {
      this.optionsSetValueForm.controls[controlName].setValue(this.optionSetValue[controlName]);
    }
    if(this.optionSetValue.endDateView){
      let fromDate = new Date(this.optionSetValue.fromDateView);
      this.optionsSetValueForm.controls['fromDateView'].setValue(fromDate.toISOString());
    }
    if(this.optionSetValue.endDateView){
      let endDate = new Date(this.optionSetValue.endDateView);
      this.optionsSetValueForm.controls['endDateView'].setValue(endDate.toISOString());
    }
  }

  close() {
    this.dialogRef.close({ event: 'cancel' });
  }

  changeOptionStatus(event){
    if (event.value) {
      this.optionsSetValueForm.controls['status'].setValue(event.value);
    }
  }

  update() {
    for(let controlName in this.optionsSetValueForm.controls){
      this.optionSetValue[controlName] = this.optionsSetValueForm.controls[controlName].value;
    }
    this.optionSetValue.endDate = this.optionSetValue.endDateView;
    this.dialogRef.close({event: this.action, data: this.optionSetValue});
  }

  public notBefore(targetKey: string, toMatchKey: string, labelMatchCode: string): ValidatorFn {
    return (group: FormGroup): { [key: string]: any } => {
      const target = group.controls[targetKey];
      const toMatch = group.controls[toMatchKey];

      if(target.value && !toMatch.value){
        target.setValue(null);
        target.setErrors({dateNotBefore: {dateNotBefore: 'notSelectBefore'}});
        target.markAsTouched();
      }

      if(!target.value && toMatch.value){
        target.setErrors(null);
        target.markAsTouched();
      }

      if (target.value && toMatch.value) {
        const isCheck = target.value >= toMatch.value;
        // set equal value error on dirty controls
        if (!isCheck && target.valid && toMatch.valid) {
          target.setErrors({dateNotBefore: {dateNotBefore: labelMatchCode}});
          target.markAsTouched();
        }
      }
      return null;
    };
  }

}
