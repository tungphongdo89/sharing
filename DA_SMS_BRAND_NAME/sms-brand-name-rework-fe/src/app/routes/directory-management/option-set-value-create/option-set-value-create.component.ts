import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormControl, FormBuilder, Validators, AbstractControl, ValidatorFn } from '@angular/forms';
import { Component, OnInit, Optional, Inject } from '@angular/core';
import { ValidationService } from '@app/shared/common/validation.service';

@Component({
  selector: 'app-option-set-value-create',
  templateUrl: './option-set-value-create.component.html',
  styleUrls: ['./option-set-value-create.component.scss'],
})
export class OptionSetValueCreateComponent implements OnInit {

  public optionsSetValueForm: FormGroup;

  private action: string;
  private optionSetValue: any = {};
  optionSets: any[] = [];

  constructor(
    public dialogRef: MatDialogRef<OptionSetValueCreateComponent>,
    @Optional() @Inject(MAT_DIALOG_DATA) public data: any,
    private fb: FormBuilder) {
    this.action = data.data.action;
    this.optionSetValue = data.data;
    this.optionSets = data.data.optionSets;
  }

  ngOnInit(): void {
    console.log('data: ', this.data);
    this.loadData();
  }

  //Set value and validate form
  loadData(): void {
    this.optionsSetValueForm = this.fb.group({
      optionSetId: [{ value: '', disabled: false }, [Validators.required]],
      code: ['', [Validators.required, Validators.maxLength(255)]],
      name: ['', [Validators.required, Validators.maxLength(255)]],
      englishName: ['', [Validators.required, Validators.maxLength(255)]],
      ord: ['', [Validators.required]],
      groupName: ['', [Validators.maxLength(255)]],
      fromDate: ['', [Validators.required]],
      endDate: [''],
      status: ['', [Validators.maxLength(255)]],
    }, { validator: this.notBefore('endDate', 'fromDate', 'notBefore') });
    this.optionsSetValueForm.controls['ord'].setValue(this.optionSetValue.ord);
  }

  save() {
    //Lấy giá trị từ các FormControl
    for (let controlName in this.optionsSetValueForm.controls) {
      this.optionSetValue[controlName] = this.optionsSetValueForm.controls[controlName].value;
    }
    this.dialogRef.close({ event: this.action, data: this.optionSetValue });
  }

  close() {
    this.dialogRef.close({ event: 'cancel' });
  }

  //Selete option set
  changeOptionSet(event) {
    console.log('chon: ', event.value);
    if (event.value) {
      this.optionsSetValueForm.controls['optionSetId'].setValue(event.value);
    }
  }

  /**
   * Validate date not before date
   * @param targetKey: any
   * @param toMatchKey: any
   */
  public notBefore(targetKey: string, toMatchKey: string, labelMatchCode: string): ValidatorFn {
    return (group: FormGroup): { [key: string]: any } => {
      const target = group.controls[targetKey];
      const toMatch = group.controls[toMatchKey];

      if (target.value && !toMatch.value) {
        target.setValue(null);
        target.setErrors({ dateNotBefore: { dateNotBefore: 'notSelectBefore' } });
        target.markAsTouched();
      }

      if (!target.value && toMatch.value) {
        target.setErrors(null);
        target.markAsTouched();
      }

      if (target.value && toMatch.value) {
        const isCheck = target.value >= toMatch.value;
        // set equal value error on dirty controls
        if (!isCheck && target.valid && toMatch.valid) {
          target.setErrors({ dateNotBefore: { dateNotBefore: labelMatchCode } });
          target.markAsTouched();
        }
      }
      return null;
    };
  }

}
