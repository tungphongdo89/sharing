import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OptionSetValueCreateComponent } from './option-set-value-create.component';

describe('OptionSetValueCreateComponent', () => {
  let component: OptionSetValueCreateComponent;
  let fixture: ComponentFixture<OptionSetValueCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OptionSetValueCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OptionSetValueCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
