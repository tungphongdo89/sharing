import { Component, Inject, OnInit, Optional } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { OptionSetModel } from '@app/core/models/option-set-model';
import { OptionSetService } from '@app/core/services/option/option-set';
import { Constant } from '@app/shared/Constant';
import { TranslateService } from '@ngx-translate/core';
import { ToastrService } from 'ngx-toastr';
import { OptionSetValueCreateComponent } from '../option-set-value-create/option-set-value-create.component';

@Component({
  selector: 'app-option-set-update',
  templateUrl: './option-set-update.component.html',
  styleUrls: ['./option-set-update.component.scss']
})
export class OptionSetUpdateComponent implements OnInit {
  optionSetUpdateForm: FormGroup;
  optionSet: any = {}
  action: string;
  constructor(public dialogRef: MatDialogRef<OptionSetUpdateComponent>, @Optional() @Inject(MAT_DIALOG_DATA) public data: OptionSetModel, private optionService: OptionSetService, private toast: ToastrService, protected translateService: TranslateService) {
    this.action = 'edit';
    this.optionSet = data;
  }

  ngOnInit(): void {
    this.optionSetUpdateForm = new FormGroup({
      code: new FormControl(''),
      name: new FormControl(''),
      fromDate: new FormControl(''),
      endDate: new FormControl(''),
      status: new FormControl(''),
    });
    this.loadData();
  }

  loadData() {
    if (this.action === 'edit') {
      for (let controlName in this.optionSetUpdateForm.controls) {
        this.optionSetUpdateForm.controls[controlName].setValue(this.optionSet[controlName]);
      }
    }
  }
  update() {
    for (let controlName in this.optionSetUpdateForm.controls) {
      this.optionSet[controlName] = this.optionSetUpdateForm.controls[controlName].value;
    }
    this.dialogRef.close(this.optionSet);
  }

  showNotifySuccess() {
    this.toast.success(this.translateService.instant('common.notify.save.success'));
  }
  showUnknownErr() {
    this.toast.error(this.translateService.instant('common.notify.save.error'));
  }

  closeDialog(){
    this.dialogRef.close({event:'cancel'});
  }

}
