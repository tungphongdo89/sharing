import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SharedModule} from '@shared/shared.module';
import {EventManagementRouting} from '@app/routes/event-management/event-management.routing';
import {ConfirmCustomerComponent} from '@app/routes/event-management/confirm-customer/confirm-customer.component';
import {EventReceiveComponent} from '@app/routes/event-management/event-receive/event-receive.component';
import {EventCloseComponent} from '@app/routes/event-management/event-close/event-close.component';
import {EventHandlingComponent} from '@app/routes/event-management/event-handling/event-handling.component';
import {EventManagementComponent} from '@app/routes/event-management/event-management.component';
import {DirectoryManagementComponent} from '@app/routes/directory-management/directory-management.component';
import {OptionSetValueCreateComponent} from '@app/routes/directory-management/option-set-value-create/option-set-value-create.component';
import {DirectoryManagementRouting} from '@app/routes/directory-management/directory-management.routing';
import {OptionSetCreateComponent} from '@app/routes/directory-management/option-set-create/option-set-create.component';
import { OptionSetUpdateComponent } from './option-set-update/option-set-update.component';
import { OptionSetValueUpdateComponent } from './option-set-value-update/option-set-value-update.component';
import { OptionSetValueDeleteComponent } from './option-set-value-delete/option-set-value-delete.component';
import {PopupDeleteComponent} from '@app/routes/directory-management/popup-delete/popup-delete.component';


@NgModule({
  declarations: [
   DirectoryManagementComponent,
    OptionSetValueCreateComponent,
    OptionSetCreateComponent,
    OptionSetUpdateComponent,
    OptionSetValueUpdateComponent,
    OptionSetValueDeleteComponent,
    PopupDeleteComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    DirectoryManagementRouting
  ],
  entryComponents: []
})
export class DirectoryManagementModule {
}
