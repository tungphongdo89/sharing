import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReportEvaluateRatingRoutingModule } from './report-evaluate-rating-routing.module';
import { ReportEvaluateRatingComponent } from './report-evaluate-rating/report-evaluate-rating.component';
import {SharedModule} from "@shared";


@NgModule({
  declarations: [ReportEvaluateRatingComponent],
  imports: [
    CommonModule,
    ReportEvaluateRatingRoutingModule,
    SharedModule
  ]
})
export class ReportEvaluateRatingModule { }
