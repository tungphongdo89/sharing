import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportEvaluateRatingComponent } from './report-evaluate-rating.component';

describe('ReportEvaluateRatingComponent', () => {
  let component: ReportEvaluateRatingComponent;
  let fixture: ComponentFixture<ReportEvaluateRatingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportEvaluateRatingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportEvaluateRatingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
