import {Component, OnInit} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {ToastrService} from 'ngx-toastr';
import {TranslateService} from '@ngx-translate/core';
import {MtxGridColumn} from '@ng-matero/extensions';
import {EvaluateAssignmentService} from '@core/services/evaluate-assignment/evaluate-assignment.service';
import {CriteriaRatingService} from '@core/services/criteria-rating/criteria-rating.service';
import {EvaluateResultService} from '@core/services/evaluate-result/evaluate-result.service';
import * as fileSaver from 'file-saver';
import {DatePipe} from '@angular/common';

@Component({
  selector: 'app-report-evaluate-rating',
  templateUrl: './report-evaluate-rating.component.html',
  styleUrls: ['./report-evaluate-rating.component.scss']
})
export class ReportEvaluateRatingComponent implements OnInit {

  constructor(
    private dialog: MatDialog,
    private toastr: ToastrService,
    private translate: TranslateService,
    private evaluateAssignmentService: EvaluateAssignmentService,
    private criteriaRatingService: CriteriaRatingService,
    private evaluateResultService: EvaluateResultService
  ) {
  }

  // Ngày đánh giá từ
  startDate: Date = null;

  // Ngày đánh giá đến
  endDate: Date = null;

  // Xếp loại
  criteriaRatingList: any = [];
  criteriaRatingId: any;

  // Bảng
  isLoading: boolean;
  pageSize: number;
  pageIndex: number;
  pageSizeList = [5, 10, 20, 50];
  columns: MtxGridColumn[] = [];
  totalRecord = 0;
  dataList: any = [];
  isExport = false;

  private static saveFile(data: ArrayBuffer, fileName: string) {
    const blob = new Blob([data], {type: 'application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet; charset=UTF-8'});
    fileSaver.saveAs(blob, fileName);
  }

  ngOnInit(): void {
    this.createTable();
    this.getCriteriaRatingList();
    this.pageIndex = 0;
    this.pageSize = 10;
    this.getDataList(this.pageIndex, this.pageSize);
  }

  onSearch(): void {
    if (this.beforeSearch()) {
      this.pageIndex = 0;
      this.getDataList(this.pageIndex, this.pageSize);
    }
  }

  getDataList(pageIndex: any, pageSize: any): void {
    this.isLoading = true;
    const data = {
      startDate: this.startDate ? this.startDate : null,
      endDate: this.endDate ? this.endDate : null,
      criteriaRatingId: this.criteriaRatingId ? this.criteriaRatingId : null
    };
    const pageable = {page: pageIndex, size: pageSize};
    this.evaluateResultService.getRatingCall(data, pageable).subscribe(res => {
        this.dataList = res.body;
        this.totalRecord = Number(res.headers.get('X-Total-Count'));
        this.isLoading = false;
      }, _ => this.isLoading = false
    );
  }

  getCriteriaRatingList(): void {
    this.criteriaRatingService.findAllCriteriaRating().subscribe(res => this.criteriaRatingList = res);
  }

  onExportExcel(): void {
    if (this.beforeSearch()) {
      this.isExport = true;
      const data = {
        startDate: this.startDate ? this.startDate : null,
        endDate: this.endDate ? this.endDate : null,
        criteriaRatingId: this.criteriaRatingId ? this.criteriaRatingId : null
      };
      this.evaluateResultService.ratingCallReport(data).subscribe(
        res => {
          if (res) {
            this.toastr.success(this.translate.instant('evaluate-list.response.success-export'));
            const pipe = new DatePipe('vi-VN');
            const fileName = this.translate.instant('report-evaluate-rating.title') + ' - ' + pipe.transform(new Date(), 'ddMMyyyyHHmmss');
            ReportEvaluateRatingComponent.saveFile(res.body, fileName);
          }
          this.isExport = false;
        }, _ => this.isExport = false
      );
    }
  }

  createTable(): void {
    this.pageIndex = 0;
    this.pageSize = 10;

    this.columns = [
      {i18n: 'STT', field: 'no', width: '50px'},
      {i18n: 'report-evaluate-rating.column.rating', field: 'criteriaRatingName', width: '200px'},
      {i18n: 'report-evaluate-rating.column.quantity', field: 'times', width: '100px'},
      {i18n: 'report-evaluate-rating.column.total-call', field: 'totalCall', width: '100px'},
      {i18n: 'report-evaluate-rating.column.rate', field: 'rate', width: '100px'}
    ];
  }

  onPageChange(event) {
    this.pageIndex = event.pageIndex;
    this.pageSize = event.pageSize;
    this.getDataList(this.pageIndex, this.pageSize);
  }

  beforeSearch(): boolean {
    if (this.startDate && this.endDate && this.endDate < this.startDate) {
      this.toastr.error(this.translate.instant('report-evaluate-criteria.validator.invalid-date'));
      document.getElementById('endDate').focus();
      return false;
    }
    return true;
  }

}
