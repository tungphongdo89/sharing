import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ReportEvaluateRatingComponent} from '@app/routes/report-evaluate-rating/report-evaluate-rating/report-evaluate-rating.component';


const routes: Routes = [
  {
    path: '',
    component: ReportEvaluateRatingComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportEvaluateRatingRoutingModule {
}
