import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReportConplaintComponent } from './report-conplaint/report-conplaint.component';
import {SharedModule} from '@shared';
import {ReportComplaintRoutingModule} from '@app/routes/report-complaint/report-complaint-routing.module';



@NgModule({
  declarations: [ReportConplaintComponent],
  imports: [
    CommonModule,
    SharedModule,
    ReportComplaintRoutingModule
  ]
})
export class ReportComplaintModule { }
