import {Routes, RouterModule} from '@angular/router';
import {NgModule} from '@angular/core';
import {ReportConplaintComponent} from '@app/routes/report-complaint/report-conplaint/report-conplaint.component';

const routes: Routes = [
  {
    path: '',
    component: ReportConplaintComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportComplaintRoutingModule {
}
