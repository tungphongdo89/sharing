import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MtxGridColumn } from '@ng-matero/extensions';
import { ApDomainService } from '@core/services/ap-domain/ap-domain.service';
import { DepartmentService } from '@core/services/department/department.service';
import { BussinessTypeService } from '@core/services/bussiness-type/bussiness-type.service';
import { ReportContactCenterService } from '@core/services/report/report-contact-center.service';
import { CommonService } from '@shared/services/common.service';
import { map } from 'rxjs/operators';
import { ReportComplaintService } from '@core/services/report/report-complaint.service';

@Component({
  selector: 'app-report-conplaint',
  templateUrl: './report-conplaint.component.html',
  styleUrls: ['./report-conplaint.component.scss'],
})
export class ReportConplaintComponent implements OnInit {
  today = new Date();
  @Output() pageChange: EventEmitter<any> = new EventEmitter<any>();
  chanelTypes: any = [];
  requestTypes: any = [];
  priorities: any = [];
  businessTypes: any = [];
  ticketRequestStatus: any = [];
  ticketStatus: any = [];
  departments: any = [];
  offset: any = 0;
  offsetDetail: any = 0;
  selectStaff: any = null;
  reportDetailForm: FormGroup = this.fb.group({
    channelType: null,
    receiveUser: null,
    fromDate: [null, [Validators.required]],
    toDate: [null, [Validators.required]],
    bussinessType: null,
    requestType: null,
  });
  data: any = [];
  dataDetail: any = [];
  columns: MtxGridColumn[] = [
    { i18n: 'report-complaint.management.column.no', field: 'index' },
    { i18n: 'report-complaint.management.column.staffName', field: 'staffName' },
    { i18n: 'report-complaint.management.column.notProcess', field: 'notProcess' },
    { i18n: 'report-complaint.management.column.processing', field: 'processing' },
    { i18n: 'report-complaint.management.column.processed', field: 'processed' },
    { i18n: 'report-complaint.management.column.confirming', field: 'confirming' },
    { i18n: 'report-complaint.management.column.complete', field: 'complete' },
  ];
  columns2: MtxGridColumn[] = [
    { i18n: 'report-contact-center.management.column.no', field: 'index' },
    { i18n: 'report-contact-center.management.column.ticketCode', field: 'ticketCode' },
    { i18n: 'report-contact-center.management.column.ticketRequestCode', field: 'ticketRequestCode' },
    { i18n: 'report-contact-center.management.column.chanelType', field: 'channelReceive' },
    { i18n: 'report-contact-center.management.column.requestStatus', field: 'requestStatus' },
    { i18n: 'report-contact-center.management.column.receiveDate', field: 'createDateTime' },
  ];
  pageSizeList = [10, 50, 100];
  pagesize = 10;
  isLoading = false;
  total: any = 0;
  pageSizeListDetail = [10, 50, 100];
  pagesizeDetail = 10;
  isLoadingDetail = false;
  totalDetail: any = 0;
  checkMonth: boolean = true;

  constructor(private fb: FormBuilder,
              private apDomainService: ApDomainService,
              private departmentService: DepartmentService,
              private bussinessTypeService: BussinessTypeService,
              private reportComplaintService: ReportComplaintService,
              private commonService: CommonService,
  ) {
  }

  ngOnInit(): void {
    this.initData();
    // this.search();
  }

  initData() {
    // Kênh tiếp nhận
    this.apDomainService.getDomainByType('CHANEL_TYPE').subscribe(res => {
      this.chanelTypes = res;
    });
    // Loại yêu cầu
    this.apDomainService.getDomainByType('REQUEST_TYPE').subscribe(res => {
      this.requestTypes = res;
    });
    // Trạng thái yêu cầu
    this.apDomainService.getDomainByType('TICKET_REQUEST_STATUS').subscribe(res => {
      this.ticketRequestStatus = res;
    });
    // Trạng thái sự vụ
    this.apDomainService.getDomainByType('TICKET_STATUS').subscribe(res => {
      this.ticketStatus = res;
    });
    // Phòng ban
    this.departmentService.getLstDepartment().subscribe(res => {
      this.departments = res;
    });
    // Loại nghiệp vụ
    this.bussinessTypeService.getBussinessType().subscribe(res => {
      this.businessTypes = res;
    });
  }

  search(reset?: boolean) {
    this.reportDetailForm.markAllAsTouched();
    if (this.reportDetailForm.invalid) return;
    if (reset) {
      this.offset = 0;
      this.pagesize = 10;
    }
    let data = { ...this.reportDetailForm.value, queryType: 1, size: this.pagesize, page: this.offset };
    if (this.selectStaff) {
      data = { ...data, status: this.selectStaff.status, staffId: this.selectStaff.staffId };
    }
    this.reportComplaintService.query(data, { size: this.pagesize, page: this.offset }).pipe(map(res => {
      return res;
    })).subscribe(res => {
      this.data = res.body;
      this.total = Number(res.headers.get('X-Total-Count'));
      this.selectStaff = null;
    });
  }

  searchDetail(reset?: boolean) {
    if (reset) {
      this.offsetDetail = 0;
      this.pagesizeDetail = 10;
    }
    const data = {
      ...this.selectStaff, ...this.reportDetailForm.value,
      queryType: 1,
      size: this.pagesizeDetail,
      page: this.offsetDetail,
    };
    this.reportComplaintService.queryDetail(data, {
      size: this.pagesizeDetail,
      page: this.offsetDetail,
    }).pipe(map(res => {
      return res;
    })).subscribe(res => {
      this.dataDetail = res.body;
      this.totalDetail = Number(res.headers.get('X-Total-Count'));
    });
  }

  export() {
    this.reportDetailForm.markAllAsTouched();
    if (this.reportDetailForm.invalid) return;
    let data = { ...this.reportDetailForm.value };
    if (this.selectStaff) {
      data = { ...data, status: this.selectStaff.status, staffId: this.selectStaff.staffId };
    }
    this.reportComplaintService.export(data);
  }

  onPageChange(event) {
    this.offset = event.pageIndex;
    this.pagesize = event.pageSize;
    this.search();
  }

  onPageChangeDetail(event) {
    this.offsetDetail = event.pageIndex;
    this.pagesizeDetail = event.pageSize;
    this.searchDetail();
  }

  showDetail(row: any, index: number) {
    this.selectStaff = { ...row, status: index };
    this.searchDetail(true);
  }

  onChangeFromDate() {
    if (this.reportDetailForm.get('toDate').value) {
      if (this.dateDiffInMonths(this.reportDetailForm.get('fromDate').value.toDate(), this.reportDetailForm.get('toDate').value.toDate()) > 12) {
        this.checkMonth = false;
      } else {
        this.checkMonth = true;
      }
    } else {
      this.checkMonth = true;
    }
  }

  onChangeToDate() {
    if (this.reportDetailForm.get('fromDate').value) {
      if (this.dateDiffInMonths(this.reportDetailForm.get('fromDate').value.toDate(), this.reportDetailForm.get('toDate').value.toDate()) > 12) {
        this.checkMonth = false;
      } else {
        this.checkMonth = true;
      }
    } else {
      this.checkMonth = true;
    }
  }

  dateDiffInMonths(d1, d2) {
    const d1Y = d1.getFullYear();
    const d2Y = d2.getFullYear();
    const d1M = d1.getMonth();
    const d2M = d2.getMonth();

    return (d2M + 12 * d2Y) - (d1M + 12 * d1Y);
  }


}
