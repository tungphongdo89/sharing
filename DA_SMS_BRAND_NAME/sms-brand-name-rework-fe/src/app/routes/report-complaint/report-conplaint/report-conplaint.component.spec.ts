import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportConplaintComponent } from './report-conplaint.component';

describe('ReportConplaintComponent', () => {
  let component: ReportConplaintComponent;
  let fixture: ComponentFixture<ReportConplaintComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportConplaintComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportConplaintComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
