import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportSentSmsMarketingDialogComponent } from './report-sent-sms-marketing-dialog.component';

describe('ReportSentSmsMarketingDialogComponent', () => {
  let component: ReportSentSmsMarketingDialogComponent;
  let fixture: ComponentFixture<ReportSentSmsMarketingDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportSentSmsMarketingDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportSentSmsMarketingDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
