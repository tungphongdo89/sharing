import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormBuilder, FormGroup } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-report-sent-sms-marketing-dialog',
  templateUrl: './report-sent-sms-marketing-dialog.component.html',
  styleUrls: ['./report-sent-sms-marketing-dialog.component.scss']
})
export class ReportSentSmsMarketingDialogComponent implements OnInit {
  formGroup: FormGroup;

  constructor(
    private fb: FormBuilder,
    private translate: TranslateService,
    @Inject(MAT_DIALOG_DATA) public dataDialog?: any
  ) { }

  ngOnInit(): void {
    this.formGroup = this.fb.group({
      campaignName: this.dataDialog.data.campaignName,
      title: this.dataDialog.data.title,
      phoneNumber: this.dataDialog.data.phoneNumber,
      sentUser: this.dataDialog.data.sentUser,
      sentDate: this.dataDialog.data.sentDate,
      status: this.dataDialog.data.status === '1' ? this.translate.instant('report-sent-sms-marketing.success') : this.translate.instant('report-sent-sms-marketing.error'),
      content: this.dataDialog.data.content
    });
  }

}
