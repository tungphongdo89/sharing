import { RouterModule, Routes } from '@angular/router';
import { ReportSentSmsMarketingComponent } from '@app/routes/report-sent-sms-marketing/report-sent-sms-marketing/report-sent-sms-marketing.component';
import { NgModule } from '@angular/core';

const routes: Routes = [
  {
    path: '',
    component: ReportSentSmsMarketingComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportSentSmsMarketingRouting {}
