import { NgModule } from '@angular/core';
import { ReportSentSmsMarketingComponent } from '@app/routes/report-sent-sms-marketing/report-sent-sms-marketing/report-sent-sms-marketing.component';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@shared';
import { ReportSentSmsMarketingRouting } from '@app/routes/report-sent-sms-marketing/report-sent-sms-marketing.routing';
import { ReportSentSmsMarketingDialogComponent } from '@app/routes/report-sent-sms-marketing/report-sent-sms-marketing-dialog/report-sent-sms-marketing-dialog.component';

@NgModule({
  declarations: [ReportSentSmsMarketingComponent, ReportSentSmsMarketingDialogComponent],
  imports: [
    CommonModule,
    SharedModule,
    ReportSentSmsMarketingRouting
  ]
})
export class ReportSentSmsMarketingModule {}
