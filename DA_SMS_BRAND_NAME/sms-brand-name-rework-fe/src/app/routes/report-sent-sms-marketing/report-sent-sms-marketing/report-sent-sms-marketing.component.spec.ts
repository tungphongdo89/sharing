import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportSentSmsMarketingComponent } from './report-sent-sms-marketing.component';

describe('ReportSentSmsMarketingComponent', () => {
  let component: ReportSentSmsMarketingComponent;
  let fixture: ComponentFixture<ReportSentSmsMarketingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportSentSmsMarketingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportSentSmsMarketingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
