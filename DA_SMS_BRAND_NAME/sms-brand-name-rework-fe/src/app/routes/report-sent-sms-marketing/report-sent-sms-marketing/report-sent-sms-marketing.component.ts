import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MtxDialog, MtxGridColumn } from '@ng-matero/extensions';
import { CampaignSmsMarketingService } from '@core/services/campaign-sms-marketing/campaign-sms-marketing.service';
import { TranslateService } from '@ngx-translate/core';
import { ReportSentSmsMarketingService } from '@core/services/report-sent-sms-marketing/report-sent-sms-marketing.service';
import { DatePipe } from '@angular/common';
import { finalize } from 'rxjs/operators';
import { ReportSentSmsMarketingDialogComponent } from '@app/routes/report-sent-sms-marketing/report-sent-sms-marketing-dialog/report-sent-sms-marketing-dialog.component';
import * as fileSaver from 'file-saver';

@Component({
  selector: 'app-report-sent-sms-marketing',
  templateUrl: './report-sent-sms-marketing.component.html',
  styleUrls: ['./report-sent-sms-marketing.component.scss'],
})
export class ReportSentSmsMarketingComponent implements OnInit {
  formSearch: FormGroup;
  columns: MtxGridColumn[] = [];
  lstCampaign = [];
  statusLst = [
    {
      label: this.translate.instant('report-sent-sms-marketing.error'),
      value: 0,
    },
    {
      label: this.translate.instant('report-sent-sms-marketing.success'),
      value: 1,
    },
  ];
  pageIndex: any;
  pageSize: any;
  dataSearch = {
    page: null,
    size: null,
  };
  totalRecord: any;
  lst: any;
  pageSizeList = [10, 50, 100];
  isLoading = false;
  minToDate;
  maxFromDate;

  constructor(
    private fb: FormBuilder,
    private campaignSmsMarketingService: CampaignSmsMarketingService,
    private translate: TranslateService,
    private service: ReportSentSmsMarketingService,
    private dialog: MtxDialog,
  ) {
  }

  ngOnInit(): void {
    this.columns = [
      { i18n: 'report-sent-sms-marketing.columns.index', width: '50px', field: 'no' },
      { i18n: 'report-sent-sms-marketing.columns.campaign-name', field: 'campaignName' },
      { i18n: 'report-sent-sms-marketing.columns.title', field: 'title' },
      { i18n: 'report-sent-sms-marketing.columns.phone-number', width: '150px', field: 'phoneNumber' },
      { i18n: 'report-sent-sms-marketing.columns.sent-user', field: 'sentUser' },
      { i18n: 'report-sent-sms-marketing.columns.sent-date', width: '150px', field: 'sentDate' },
      { i18n: 'report-sent-sms-marketing.columns.status', width: '150px', field: 'status' },
      { i18n: 'report-sent-sms-marketing.columns.content', width: '200px', field: 'content' },
    ];
    this.formSearch = this.fb.group({
      campaign: '',
      fromDate: null,
      toDate: null,
      phoneNumber: null,
      status: '',
    });
    this.campaignSmsMarketingService.getAllSearch().subscribe(res => {
      this.lstCampaign = res.body;
    });
    this.pageIndex = 0;
    this.pageSize = 10;
    this.onSearch();
  }

  onSearch() {
    this.isLoading = true;
    this.dataSearch.page = this.pageIndex;
    this.dataSearch.size = this.pageSize;
    let fromDate;
    let toDate;
    let name;
    let phoneNumber;
    let status;
    if (this.formSearch.get('fromDate').value) {
      fromDate = this.setDateTime(this.formSearch.get('fromDate').value);
    } else {
      fromDate = null;
    }
    if (this.formSearch.get('toDate').value) {
      toDate = this.setDateTime(this.formSearch.get('toDate').value);
    } else {
      toDate = null;
    }
    if (this.formSearch.get('campaign').value !== '') {
      name = this.formSearch.get('campaign').value;
    } else {
      name = -1;
    }
    if (this.formSearch.get('phoneNumber').value !== '') {
      phoneNumber = this.formSearch.get('phoneNumber').value;
    } else {
      phoneNumber = null;
    }
    if (this.formSearch.get('status').value !== '') {
      status = this.formSearch.get('status').value;
    } else {
      status = null;
    }
    this.service.doSearch(this.dataSearch, name, fromDate, toDate, phoneNumber, status).pipe(finalize(() => {
      this.isLoading = false;
    })).subscribe(res => {
      if (res) {
        this.lst = res.body;
        this.totalRecord = Number(res.headers.get('X-Total-Count'));
      }
    });
  }

  export() {
    if (this.formSearch.invalid) {
      return;
    }
    let fromDate;
    let toDate;
    let name;
    let phoneNumber;
    let status;
    if (this.formSearch.get('fromDate').value) {
      fromDate = this.setDateTime(this.formSearch.get('fromDate').value);
    } else {
      fromDate = null;
    }
    if (this.formSearch.get('toDate').value) {
      toDate = this.setDateTime(this.formSearch.get('toDate').value);
    } else {
      toDate = null;
    }
    if (this.formSearch.get('campaign').value !== '') {
      name = this.formSearch.get('campaign').value;
    } else {
      name = -1;
    }
    if (this.formSearch.get('phoneNumber').value !== '') {
      phoneNumber = this.formSearch.get('phoneNumber').value;
    } else {
      phoneNumber = null;
    }
    if (this.formSearch.get('status').value !== '') {
      status = this.formSearch.get('status').value;
    } else {
      status = null;
    }
    this.service.export(name, fromDate, toDate, phoneNumber, status).subscribe(res => {
      if (res) {
        const fileName = res.headers.get('filename');
        this.saveFile(res.body, fileName);
      }
    });
  }

  private saveFile(data: ArrayBuffer, fileName: string) {
    const blob = new Blob([data], { type: 'application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet; charset=UTF-8' });
    fileSaver.saveAs(blob, fileName);
  }

  onPageChange(event) {
    this.pageIndex = event.pageIndex;
    this.pageSize = event.pageSize;
    this.onSearch();
  }

  search() {
    if (this.formSearch.invalid) {
      return;
    }
    this.pageIndex = 0;
    this.onSearch();
  }

  setDateTime(dateTime) {
    const pipe = new DatePipe('en-US');
    const date = pipe.transform(dateTime, 'dd/MM/yyyy', 'GMT+7');
    return date;
  }

  openDialog(row: any) {
    this.dialog.open({
      disableClose: true,
      hasBackdrop: true,
      width: '50vw',
      data: row,
    }, ReportSentSmsMarketingDialogComponent);
  }

  onChangeFromDate() {
    this.minToDate = this.formSearch.get('fromDate').value;
  }

  onChangeToDate() {
    this.maxFromDate = this.formSearch.get('toDate').value;
  }
}
