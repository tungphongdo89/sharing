import {Component, OnInit} from '@angular/core';
import {CriteriaGroupService} from '@core/services/criteria-group/criteria-group.service';
import {CriteriaRatingService} from '@core/services/criteria-rating/criteria-rating.service';
import {OptionSetValueService} from '@core/services/option/option-set-value';
import {CpTicketService} from '@core/services/cp/cp-ticket.service';
import {environment} from '@env/environment';
import {EvaluateResultService} from '@core/services/evaluate-result/evaluate-result.service';
import {MatDialog} from '@angular/material/dialog';
import {ToastrService} from 'ngx-toastr';
import {TranslateService} from '@ngx-translate/core';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-evaluate-call',
  templateUrl: './evaluate-call.component.html',
  styleUrls: ['./evaluate-call.component.scss']
})
export class EvaluateCallComponent implements OnInit {

  // Chi tiết cuộc gọi
  callDetail: any;
  evaluateResultId: any;

  // Quản lý tiêu chí
  totalScoreLimit = 0;
  totalScore = 0;
  criteriaGroupList: any = [];
  criteriaDetailDTOS: any = [];

  // Quản lý xếp hạng
  criteriaRatingList: any = [];
  criteriaRatingId: any = null;

  // Quản lý lỗi
  optionSetValue: any = [];
  specializeList: any = [];
  specializeId: any = null;
  majorList: any = [];
  majorId: any = null;
  content = '';
  suggest = '';

  constructor(
    private criteriaGroupService: CriteriaGroupService,
    private criteriaRatingService: CriteriaRatingService,
    private optionSetValueService: OptionSetValueService,
    private cpTicketService: CpTicketService,
    private evaluateResultService: EvaluateResultService,
    private dialog: MatDialog,
    private toastr: ToastrService,
    private translate: TranslateService,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) {
  }

  ngOnInit(): void {
    this.getCallDetail();
    this.getCriteriaGroupDetail();
    this.getCriteriaRating();
    this.getOptionSetValue();
  }

  getCallDetail(): void {
    this.evaluateResultId = Number(this.activatedRoute.snapshot.params.id);
    this.evaluateResultService.getTicketForEvaluate(this.evaluateResultId).subscribe(
      res => {
        res.channelName = this.getChannelName(res.channelName);
        this.callDetail = res;
        // Build audio
        const audioSource = document.createElement('source');
        audioSource.setAttribute('src', environment.serverUrl.api + res.recordLink);
        document.getElementById('audio').appendChild(audioSource);
      }
    );
  }

  getCriteriaGroupDetail(): void {
    this.criteriaGroupService.findCriteriaGroupDetail().subscribe(res => {
      if (res) {
        res.forEach(
          item => {
            if (item.scores) {
              this.totalScoreLimit += item.scores;
            }
          }
        );
      }
      this.criteriaGroupList = res;
    });
  }

  getCriteriaRating(): void {
    this.criteriaRatingService.findAllCriteriaRating().subscribe(res => this.criteriaRatingList = res);
  }

  getOptionSetValue(): void {
    this.optionSetValueService.findOptSetValueByOptionSetCode('LOI_CHUYEN_MON').subscribe(res => this.specializeList = res);
    this.optionSetValueService.findOptSetValueByOptionSetCode('LOI_NGHIEP_VU').subscribe(res => this.majorList = res);
  }

  scrollToCriteria(id: any): void {
    document.getElementById(`${id}`).scrollIntoView(true);
  }

  onCheckCriteria(data: any, criteria: any, group: any): void {
    criteria.criteriaDetailDTOList.forEach(item => {
        if (item.id !== data.id) {
          item.checked = false;
        }
      }
    );

    criteria.score = data.checked ? data.scores : 0;
    group.score = 0;
    group.lstCriteriaDTO.forEach(item => {
        group.score = group.score + (item.score ? item.score : 0);
      }
    );

    this.totalScore = 0;
    this.criteriaGroupList.forEach(item => {
      this.totalScore = this.totalScore + (item.score ? item.score : 0);
    });
    if (this.criteriaRatingList) {
      const rate = this.criteriaRatingList.filter(item => item.fromScores <= this.totalScore && item.toScores >= this.totalScore);
      if (rate && rate.length > 0) {
        this.criteriaRatingId = rate[0].id;
      } else {
        this.criteriaRatingId = null;
      }
    }
  }

  doEvaluate(): void {
    this.handleListBeforeSend();
    const data = {
      id: this.callDetail.id,
      criteriaRatingId: this.criteriaRatingId ? this.criteriaRatingId : null,
      totalScores: this.totalScore ? this.totalScore : 0,
      error1: this.specializeId ? this.specializeId.trim() : '',
      error2: this.majorId ? this.majorId.trim() : '',
      content: this.content ? this.content.trim() : '',
      suggest: this.suggest ? this.suggest.trim() : '',
      criteriaDetailDTOS: this.criteriaDetailDTOS
    };
    this.evaluateResultService.createEvaluateResult(data).subscribe(_ => this.toastr.success(this.translate.instant('evaluate-call.response.success')));
  }

  handleListBeforeSend(): void {
    this.criteriaDetailDTOS = [];
    for (const group of this.criteriaGroupList) {
      for (const criteria of group.lstCriteriaDTO) {
        for (const detail of criteria.criteriaDetailDTOList) {
          if (detail.checked) {
            this.criteriaDetailDTOS.push(detail);
          }
        }
      }
    }
  }

  getChannelName(channelId: any): string {
    if (Number(channelId) === 1) {
      return this.translate.instant('evaluate-assignment.in-call');
    } else {
      return this.translate.instant('evaluate-assignment.out-call');
    }
  }

}
