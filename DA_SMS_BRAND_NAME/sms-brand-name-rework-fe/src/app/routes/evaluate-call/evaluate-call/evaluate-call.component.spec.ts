import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EvaluateCallComponent } from './evaluate-call.component';

describe('EvaluateCallComponent', () => {
  let component: EvaluateCallComponent;
  let fixture: ComponentFixture<EvaluateCallComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EvaluateCallComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EvaluateCallComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
