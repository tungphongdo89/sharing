import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {EvaluateCallComponent} from '@app/routes/evaluate-call/evaluate-call/evaluate-call.component';

const routes: Routes = [
  {
    path: ':id',
    component: EvaluateCallComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EvaluateCallRoutingModule {
}
