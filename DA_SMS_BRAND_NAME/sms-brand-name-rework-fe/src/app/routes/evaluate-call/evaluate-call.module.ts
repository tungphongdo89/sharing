import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EvaluateCallRoutingModule } from './evaluate-call-routing.module';
import { EvaluateCallComponent } from './evaluate-call/evaluate-call.component';
import {SharedModule} from '@shared';


@NgModule({
  declarations: [EvaluateCallComponent],
    imports: [
        CommonModule,
        EvaluateCallRoutingModule,
        SharedModule
    ]
})
export class EvaluateCallModule { }
