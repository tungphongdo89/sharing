import {Component, OnInit} from '@angular/core';
import {MatDialogRef} from '@angular/material/dialog';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {COMMOM_CONFIG} from '@env/environment';

@Component({
  selector: 'app-enter-outer-call',
  templateUrl: './enter-outer-call.component.html',
  styleUrls: ['./enter-outer-call.component.scss']
})
export class EnterOuterCallComponent implements OnInit {
  formGroup: FormGroup = this.fb.group({
    phoneNumber: new FormControl(null, [Validators.required, Validators.pattern(COMMOM_CONFIG.NUMBER_PHONE_FORMAT)]),
    cid: new FormControl(null, Validators.required),
  });

  constructor(
    private dialogRef: MatDialogRef<EnterOuterCallComponent>,
    private fb: FormBuilder
  ) {
  }

  ngOnInit(): void {
  }

  save() {
    this.formGroup.markAllAsTouched();
    if (this.formGroup.invalid) return;
    const rawValues = this.formGroup.getRawValue();
    this.dialogRef.close(rawValues);
  }

  dismiss() {
    this.dialogRef.close(false);
  }
}
