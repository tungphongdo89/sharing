import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {Constant} from '@shared/Constant';
import {MtxDialog} from '@ng-matero/extensions';
import {RecordAudioPlayComponent} from '@app/routes/record-call-results/record-audio-play/record-audio-play.component';
import {BaseComponent} from '@shared/components/base-component/base-component.component';
import {ActivatedRoute} from '@angular/router';
import {ToastrService} from 'ngx-toastr';
import {TranslateService} from '@ngx-translate/core';
import {RESOURCE} from '@core';
import {EnterOuterCallComponent} from '@app/routes/record-call-results/enter-outer-call/enter-outer-call.component';
import {RecordCallResultsService} from '@core/services/record-call-results/record-call-results.service';
import {ValidatorsCb} from '@shared/directives/required-cb';
import * as  moment from 'moment';
import {FORMATS_DATE, FORMATS_DATETIME} from '@shared/common/format-date';
import {Time} from '@shared/components/time-picker/time-picker.component';
import {concatMap} from 'rxjs/operators';
import {coerceBooleanProperty} from '@angular/cdk/coercion';

@Component({
  selector: 'app-enter-callout-information',
  templateUrl: './enter-callout-information.component.html',
  styleUrls: ['./enter-callout-information.component.scss']
})
export class EnterCalloutInformationComponent extends BaseComponent implements OnInit {
  campaign = [];
  scenarios = [];
  field = {value: 'id', text: 'name'};
  campaignId: FormControl = new FormControl(null, ValidatorsCb.required());
  Constant = Constant;
  columnsScenario = [];
  arrControls = [];
  formGroup: FormGroup = this.fb.group({});
  isRenderTable = false;
  isRenderQuestion = false;
  campaignName = '';
  questionAndAnswers = {};
  arrRadiosId = [];
  campaignResourceDetailId: any;

  constructor(private recordCallResultService: RecordCallResultsService,
              private fb: FormBuilder,
              private mtxDialog: MtxDialog,
              public route: ActivatedRoute,
              public dialog: MtxDialog,
              protected toastr: ToastrService,
              protected translateService: TranslateService,
  ) {
    super(route, recordCallResultService, RESOURCE.CUSTOMER, toastr, translateService, dialog);
  }

  ngOnInit(): void {
    this.prepareData();
  }

  prepareData() {
    this.isLoading = false;
    this.loadAllCampaigns();
    this.setColumns();
    this.onCampaignChange();
    this.getIdFromParam();
  }

  getIdFromParam(){
    this.route.paramMap.subscribe(async paramMap => {
      const idParam = paramMap.get('id');
      if (idParam){
        this.getCampaignIDFromCID(idParam);
      }
    });
  }

  getCampaignIDFromCID(cid){
    this.recordCallResultService.getCampaignIdFromCID(cid).subscribe((res:any) => {
      console.log(res);
      if (res){
        const campaignId = res.campaignId;
        this.campaignId.patchValue(campaignId);
      }
    });
  }

  registerControls() {
    const object = {};
    // gen dynamic table
    const dataRows = [];
    this.columns = [];
    if (this.arrControls.length > 0) {
      this.arrControls.forEach(control => {
        if (control.type === Constant.CBX_TYPE_ID) {
          const cbxValue = Array.from(control.options).find((opt: any) => opt.code === control.value) ?
            Array.from(control.options).find((opt: any) => opt.code === control.value)[`name`] : control.value;
          object[control.id] = cbxValue;
        } else {
          object[control.id] = control.value;
        }
        this.columns.push({i18n: control.name, field: control.id.toString()});
        if (control.type === Constant.DATE_TYPE_ID) {
          this.formGroup.addControl(control.id, new FormControl({
            value: moment(control.value, FORMATS_DATE.parse.dateInput, true).toDate(),
            disabled: control.editable === Constant.EDITABLE
          }));
        } else if (control.type === Constant.DATETIME_TYPE_ID) {
          this.formGroup.addControl(control.id, new FormControl({
            value: moment(control.value, FORMATS_DATETIME.parse.dateInput, true).toDate(),
            disabled: control.editable === Constant.EDITABLE
          }));
        } else if (control.type === Constant.TIME_TYPE_ID) {
          this.formGroup.addControl(control.id, new FormControl({
            value: this.getTimeFromString(control.value),
            disabled: control.editable === Constant.EDITABLE
          }));
        } else this.formGroup.addControl(control.id, new FormControl({
          value: control.value,
          disabled: control.editable === Constant.EDITABLE
        }));
      });
      this.columns.push({i18n: 'Play', field: 'action', pinned: 'right', width: '100px'});
      this.columns.unshift({i18n: 'common.orderNumber', field: 'no', width: '50px'});
      // gen dynamic table
      dataRows.push(object);
      this.dataModel.dataSource = dataRows;
      this.isRenderTable = true;
    } else {
      this.isRenderTable = false;
      this.columns = [];
    }
    console.log(object);
    console.log('this.column', this.columns);
    console.log('this.data', this.dataModel.dataSource);
  }

  loadAllCampaigns() {
    this.recordCallResultService.getAllCampaigns().subscribe((next: any) => {
      console.log(next);
      this.campaign = next;
    });
  }

  getTimeFromString(strTime: any) {
    if (!strTime) return;
    const arrTime = strTime.split(':');
    if (arrTime.length !== 2) {
      console.error('Invalid time type (recommend HH:mm)');
    }
    return new Time(arrTime[0], arrTime[1]);
  }

  getStringFromTime(time: Time) {
    if (!time) return;
    return `${time.hour}:${time.minute}`;
  }

  handleTime(control: any) {
    console.log(this.formGroup.controls);
    const time = this.formGroup.controls[`${control}`];
    if (!time.value?.hour || !time.value?.minute) {
      console.error('Time must not be empty');
    }
    if (Number(time.value?.hour) > 23 || Number(time.value?.minute) > 59) {
      time.reset();
    }
  }

  onOpenAudioPlayer() {
    const audio = [
      {
        title: 'Lạc Trôi - Sơn Tùng M-TP (TripleD Remix) ',
        link: './assets/images/lac-troi.mp3',
        artist: 'Son Tung M-TP'
      }
    ];
    this.mtxDialog.open({
      hasBackdrop: true,
      width: '20vw',
      data: {playLists: audio}
    }, RecordAudioPlayComponent);
  }

  onOpenEnterOuterCall() {
    this.campaignId.markAllAsTouched();
    if (this.campaignId.invalid) return;
    const dialogRef = this.mtxDialog.open({
      hasBackdrop: true,
      width: '30vw',
      disableClose: true
    }, EnterOuterCallComponent);
    dialogRef.afterClosed().subscribe(value => {
      if (value) {
        value[`campaignId`] = this.campaignId.value;
        this.recordCallResultService.addOuterCall(value).subscribe((next: any) => {
          console.log('outer call', next);
          if (next) {
            if (next.errorCode === '001') {
              this.toastr.error(this.translateService.instant('record-call-result.cid-notfound'));
              return;
            } else {
              this.getInformationOfOuterCall(next.campaignResourceDetailId);
            }
          }
        }, () => this.showUnknownErr());
      }
    });
  }

  getInformationOfOuterCall(campaignResourceDetailId: any) {
    const options = {};
    options[`campaignResourceDetailId`] = campaignResourceDetailId;
    options[`campaignId`] = this.campaignId.value;
    this.recordCallResultService.getTheNextPhoneNumber(this.campaignId.value).pipe(
      concatMap((next: any) => {
        console.log('next', next);
        this.arrControls = next;
        this.registerControls();
        if (next && next.length > 0) {
          this.campaignResourceDetailId = next[0].campaignResourceDetailId;
        } else {
          this.toastr.error(this.translateService.instant('record-call-result.phoneNumber.notFound'));
        }
        return this.recordCallResultService.getQuestionsAndAnswers(options);
      })).subscribe((scenarios: any) => {
      console.log('question', scenarios);
      this.scenarios = scenarios;
      this.registerNgModel(this.scenarios);
    }, () => this.showUnknownErr());
  }

  pageChange(event: any) {
    console.log('event', event);
  }

  setColumns() {
    this.columnsScenario = [
      {i18n: this.translateService.instant('record-call-result.question'), field: 'questions', width: '60vw'},
      {i18n: this.translateService.instant('record-call-result.answer'), field: 'answers'}
    ];
  }

  onCampaignChange() {
    this.campaignId.valueChanges.subscribe(value => {
      if (value) {
        this.campaignName = this.campaign.find((campaign: any) => campaign.id === value)
          ? this.campaign.find((campaign: any) => campaign.id === value).name : '';
        this.getTheNextPhoneNumber(value);
      }
    });
  }

  getTheNextPhoneNumber(campaignId: any) {
    this.campaignId.markAllAsTouched();
    if (this.campaignId.invalid) return;
    const options = {};
    this.recordCallResultService.getTheNextPhoneNumber(campaignId).pipe(
      concatMap((next: any) => {
        console.log('next', next);
        this.arrControls = next;
        this.registerControls();
        if (next && next.length > 0) {
          this.campaignResourceDetailId = next[0].campaignResourceDetailId;
        } else {
          this.toastr.error(this.translateService.instant('record-call-result.phoneNumber.notFound'));
        }
        options[`campaignId`] = campaignId;
        options[`campaignResourceDetailId`] = this.campaignResourceDetailId ? this.campaignResourceDetailId : 0;
        return this.recordCallResultService.getQuestionsAndAnswers(options);
      })).subscribe((scenarios: any) => {
      console.log('question', scenarios);
      this.scenarios = scenarios;
      this.registerNgModel(this.scenarios);
    }, () => this.showUnknownErr());
  }

  registerNgModel(scenario: any[]) {
    this.arrRadiosId = [];
    this.questionAndAnswers = {};
    this.isRenderQuestion = false;
    console.log('scenario', scenario);
    if (scenario?.length <= 0) return;
    scenario.forEach(object => {
      if (object.answers.length > 0 && object.answers[0][`type`] === Constant.ANSWER_TYPE_TEXT) {
        Array.from(object.answers).forEach((text: any) => {
          this.questionAndAnswers[text.id] = text.value;
        });
      } else if (object.answers.length > 0 && object.answers[0][`type`] === Constant.ANSWER_TYPE_RADIO && object.answers[0][`radioValues`].length > 0) {
        Array.from(object.answers[0][`radioValues`]).forEach((radio: any) => {
          this.arrRadiosId.push(radio.id);
          if (radio.value) this.questionAndAnswers[object.answers[0]?.radioValues[0]?.id] = `${radio.id}:${radio.value}`;
        });
      } else if (object.answers.length > 0 && object.answers[0][`type`] === Constant.ANSWER_TYPE_CHECKBOX) {
        Array.from(object.answers).forEach((checkbox: any) => {
          this.questionAndAnswers[checkbox.id] = coerceBooleanProperty(checkbox.value);
        });
      } else if (object.answers.length > 0 && object.answers[0][`type`] === Constant.ANSWER_TYPE_RANKING) {
        this.questionAndAnswers[object.answers[0]?.radioValues[0]?.id] = object.answers[0]?.radioValues[0]?.value;
      }
    });
    console.log('this.arrRadiosId', this.arrRadiosId);
    this.isRenderQuestion = true;
  }

  onSave() {
    const body = this.prepareRawDataToSave();
    const campaignResource = {};
    campaignResource[`campaignResourceDetailId`] = this.campaignResourceDetailId;
    campaignResource[`campaignId`] = this.campaignId.value;
    this.recordCallResultService.saveResult(body, campaignResource).subscribe(() => {
      this.onSaveSuccess();
      this.campaignResourceDetailId = 0;
    }, () => this.showUnknownErr());
  }

  prepareRawDataToSave() {
    const body = {};
    const arrControlValue = [];
    const answersValue = [];
    const rawValues = this.formGroup.getRawValue();
    const keyQuestions = Object.keys(this.questionAndAnswers).length > 0 ? Array.from(Object.keys(this.questionAndAnswers), Number) : [];
    this.arrControls.forEach((control: any) => {
      const campaignResourceTemp = {};
      const controlValue = rawValues[control.id];
      campaignResourceTemp[`id`] = control.id;
      if (control.type === Constant.DATE_TYPE_ID) {
        campaignResourceTemp[`value`] = moment(controlValue).format(FORMATS_DATE.parse.dateInput);
      } else if (control.type === Constant.DATETIME_TYPE_ID) {
        campaignResourceTemp[`value`] = moment(controlValue).format(FORMATS_DATETIME.parse.dateInput);
      } else if (control.type === Constant.TIME_TYPE_ID) {
        campaignResourceTemp[`value`] = this.getStringFromTime(controlValue);
      } else {
        campaignResourceTemp[`value`] = controlValue;
      }
      arrControlValue.push(campaignResourceTemp);
    });
    console.log('questionAndAnswers', this.questionAndAnswers);
    keyQuestions.forEach(key => {
      if (this.arrRadiosId.includes(key)) {
        const radioValues = this.questionAndAnswers[key].split(':');
        const objRadioValue = {};
        objRadioValue[`id`] = Number(radioValues[0]);
        objRadioValue[`value`] = radioValues[1];
        answersValue.push(objRadioValue);
      } else {
        const otherValue = {};
        otherValue[`id`] = key;
        otherValue[`value`] = this.questionAndAnswers[key];
        answersValue.push(otherValue);
      }
    });
    body[`lstResourceTemplate`] = arrControlValue;
    body[`lstCampaignCallResult`] = answersValue;
    console.log('body', body);
    return body;
  }

  onSaveSuccess() {
    this.toastr.success(this.translateService.instant('common.MSG0021'));
  }

  showUnknownErr() {
    this.toastr.error(this.translateService.instant('common.notify.fail'));
  }
}
