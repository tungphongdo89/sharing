import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {EnterCalloutInformationComponent} from '@app/routes/record-call-results/enter-callout-information/enter-callout-information.component';

const routes: Routes = [
  {path: '', component: EnterCalloutInformationComponent},
  {path: ':id', component: EnterCalloutInformationComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RecordCallResultsRoutingModule {
}
