import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RecordCallResultsRoutingModule} from './record-call-results-routing.module';
import {EnterCalloutInformationComponent} from './enter-callout-information/enter-callout-information.component';
import {SharedModule} from '@shared';
import {RecordAudioPlayComponent} from './record-audio-play/record-audio-play.component';
import {EnterOuterCallComponent} from './enter-outer-call/enter-outer-call.component';


@NgModule({
  declarations: [EnterCalloutInformationComponent, RecordAudioPlayComponent, EnterOuterCallComponent],
  imports: [
    CommonModule,
    RecordCallResultsRoutingModule,
    SharedModule
  ]
})
export class RecordCallResultsModule {
}
