import {Component, Inject, OnInit} from '@angular/core';
import {Track} from 'ngx-audio-player';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'app-record-audio-play',
  templateUrl: './record-audio-play.component.html',
  styleUrls: ['./record-audio-play.component.scss']
})
export class RecordAudioPlayComponent implements OnInit {
  displayTitle = true;
  displayPlaylist = false;
  pageSizeOptions = [2, 4, 6];
  displayVolumeControls = true;
  displayRepeatControls = false;
  displayArtist = false;
  displayDuration = false;
  disablePositionSlider = false;
  autoPlay = true;


  playLists: Track[] = [];

  constructor(private dialogRef: MatDialogRef<RecordAudioPlayComponent>,
              @Inject(MAT_DIALOG_DATA) public dataDialog?: any) {
  }

  ngOnInit(): void {
    if (this.dataDialog.data.playLists) this.playLists = this.dataDialog.data.playLists;
  }

  onEnded(event: any) {
  }
}
