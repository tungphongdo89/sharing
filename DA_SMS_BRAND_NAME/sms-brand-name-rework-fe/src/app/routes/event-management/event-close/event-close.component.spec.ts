import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventCloseComponent } from './event-close.component';

describe('EventCloseComponent', () => {
  let component: EventCloseComponent;
  let fixture: ComponentFixture<EventCloseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventCloseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventCloseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
