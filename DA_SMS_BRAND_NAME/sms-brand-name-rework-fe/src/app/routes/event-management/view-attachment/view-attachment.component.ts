import {Component, ElementRef, Inject, OnInit, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {ConfirmDialogModel} from '@shared/components/confirm-dialog/confirm-dialog.component';

@Component({
  selector: 'app-view-attachment',
  templateUrl: './view-attachment.component.html',
  styleUrls: ['./view-attachment.component.scss']
})
export class ViewAttachmentComponent implements OnInit {
  fileUrl:any;
  check: any;
  type: any;
  @ViewChild('pdfView', { static: true }) pdfViewer: ElementRef;
  constructor(public dialogRef: MatDialogRef<ViewAttachmentComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any) {
    this.fileUrl = data.fileURL;
    this.check = data.check;
  }

  ngOnInit(): void {
    this.pdfViewer.nativeElement.data = this.fileUrl;
    if(this.check === 1)
      this.type = 'application/pdf';
    else
      this.type = 'image/jpg';
  }

}
