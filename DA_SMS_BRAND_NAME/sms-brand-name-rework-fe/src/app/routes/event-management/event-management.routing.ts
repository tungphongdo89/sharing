import {Routes, RouterModule} from '@angular/router';
import {NgModule} from '@angular/core';
import {EventManagementComponent} from '@app/routes/event-management/event-management.component';

const routes: Routes = [
  {
    path: '',
    component: EventManagementComponent
  },
  {
    path: ':id',
    component: EventManagementComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EventManagementRouting {
}
