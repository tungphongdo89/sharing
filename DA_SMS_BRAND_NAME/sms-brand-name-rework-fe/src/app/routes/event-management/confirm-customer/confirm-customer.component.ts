import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {TranslateService} from '@ngx-translate/core';
import {ToastrService} from 'ngx-toastr';
import {EventService} from '@core/services/envent-handling/event-service';
import {MtxDialog, MtxGridColumn} from '@ng-matero/extensions';
import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
  TemplateRef,
  ViewChild
} from '@angular/core';
import {ConfirmTicketService} from '@core/services/envent-handling/confirm-ticket-service';
import {ConfirmTicketModel} from '@core/models/confirm-ticket-model';
import {ApDomainService} from '@core/services/ap-domain/ap-domain.service';
import {ActivatedRoute} from '@angular/router';
import {ConfirmDialogComponent, ConfirmDialogModel} from '@shared/components/confirm-dialog/confirm-dialog.component';
import {MatDialog} from '@angular/material/dialog';
import {ViewAttachmentComponent} from '@app/routes/event-management/view-attachment/view-attachment.component';
import {DatePipe} from "@angular/common";

@Component({
  selector: 'app-confirm-customer',
  templateUrl: './confirm-customer.component.html',
  styleUrls: ['./confirm-customer.component.scss']
})
export class ConfirmCustomerComponent implements OnInit, OnChanges {
  @ViewChild('timeReceive', {static: true}) timeReceive: TemplateRef<any>;
  @Input() dataGetHistory: ConfirmTicketModel[] = [];
  @Input() confirmTicketDTO: ConfirmTicketModel = new ConfirmTicketModel();
  @Input() eventStatus: string;
  @Input() isOverride: boolean;
  @Input() isPermission: boolean;
  columnsConfirmAttachment: MtxGridColumn[] = [];
  fileNameConfirm: any[] = [];
  listConfirmTicketAttachmentDTOs: any[] = [];
  isDisabled: boolean = false;
  idTicket: string;
  pipe: any;
  @Output() disableToParent = new EventEmitter();
  pagesize = 5;
  pageSizeList = [5, 10, 50, 100];
  totalAttachmentConfirm: number = 0;
  dataListAttachmentConfirmPagible: any = [];
  showValidate:boolean = true;

  constructor(private confirmTicketService: ConfirmTicketService,
              private ticketService: EventService,
              protected toastr: ToastrService,
              protected translateService: TranslateService,
              private fb: FormBuilder, private apDomainService: ApDomainService,
              private route: ActivatedRoute,
              private dialog: MtxDialog,
              private matDialog: MatDialog) {
  }

  @Input() level: any[];
  form: FormGroup = this.fb.group({
    content: ['', Validators.required],
    level: ['', Validators.required],
    file: [''],
    confirmDeadline: ['']
  });

  ngOnInit(): void {
    // this.form.get('confirmDeadline').patchValue(this.setDateTime(this.confirmTicketDTO.confirmDeadline));
    this.columnsConfirmAttachment = [
      {i18n: 'fileName', field: 'fileName'},
      {i18n: 'timeUpload', field: 'createDatetime', cellTemplate: this.timeReceive},
      {
        i18n: 'tool',
        field: 'options',
        width: '200px',
        pinned: 'right',
        right: '0px',
        type: 'button',
        buttons: [
          {
            type: 'icon',
            text: 'view',
            icon: 'visibility',
            click: record => this.viewConfirm(record),
          },
          {
            type: 'icon',
            text: 'delete',
            icon: 'delete',
            click: record => this.deleteConfirmAttachment(record),
          }
        ]
      },
    ];
    this.confirmTicketDTO.content = '';
    // channel
    this.apDomainService.getDomainByType('SATISFIED_TYPE').subscribe(res => {
      this.level = res;
    });
    this.getListConfirmTicketAttachment();
  }

  ngOnChanges(changes: SimpleChanges) {
    console.log('chane', changes);
    this.form.patchValue(this.confirmTicketDTO);
  }

  setDateTime(dateTime) {
    const pipe = new DatePipe('en-US');
    const date = pipe.transform(dateTime, 'dd/MM/yyyy HH:mm', 'GMT+7');
    return date;
  }

  getListHistoryConfirmTicket(confirmTicketDTO) {
    this.confirmTicketService.getListHistoryConfirmTickets(confirmTicketDTO).subscribe(res => {
      if (res === null) {
        this.dataGetHistory = [];
      } else {
        this.dataGetHistory = res.filter(item => item.content!=='');
      }
    });
  }

  getListConfirmTicketAttachment() {
    this.route.paramMap.subscribe(paramMap => {
      this.idTicket = paramMap.get('id');
      if (this.idTicket) {
        this.confirmTicketService.getListConfirmTicketAttachment(this.idTicket).subscribe(res => {
          this.listConfirmTicketAttachmentDTOs = res;
          this.totalAttachmentConfirm = res.length;
          this.dataListAttachmentConfirmPagible = [...this.listConfirmTicketAttachmentDTOs].splice(0, this.pagesize);
        });
      }

    });
  }

  onChangeFile(event) {
    const a = event.target.files.length;
    for (let i = 0; i < a; i++) {
      this.fileNameConfirm.push(event.target.files[i]);
    }
  }

  insertFile() {
    this.confirmTicketService.uploadFile(this.fileNameConfirm).subscribe(
      (res) => {
        console.log(res);
        this.listConfirmTicketAttachmentDTOs = [...this.listConfirmTicketAttachmentDTOs, ...res];
        this.totalAttachmentConfirm = this.listConfirmTicketAttachmentDTOs.length;
        this.dataListAttachmentConfirmPagible = [...this.listConfirmTicketAttachmentDTOs].splice(0, this.pagesize);
        this.showNotifySuccess();
        this.fileNameConfirm = [];
      },
      (error) => {
        alert(error.error.message);
      }
    );
  }

  // Thay dổi mức độ hài lòng
  changeLevel(event) {
    this.confirmTicketDTO.satisfied = event.value;
  }

  // Lưu xác nhận
  createConfirmTicket() {
    if(this.showValidate){
      this.confirmTicketDTO.listConfirmTicketAttachmentDTOs = this.listConfirmTicketAttachmentDTOs;
      this.confirmTicketDTO.status = '4';
      this.confirmTicketDTO.content = this.form.controls.content.value;
      console.log(this.confirmTicketDTO);
      this.confirmTicketService.createConfirmTicket(this.confirmTicketDTO).subscribe(res => {
        console.log(res);
        if (res.status === 'SUCCESS') {
          this.confirmTicketDTO.satisfied = '';
          this.getListHistoryConfirmTicket(this.confirmTicketDTO);
          this.form.reset();
          this.form.controls.level.setErrors(null);
          this.form.controls.content.setErrors(null);
          this.showValidate = false;
          this.getListConfirmTicketAttachment();
          this.showNotifySuccess();
        } else {
          this.showUnknownErr();
        }
      });
    }else{
      this.initFormGroup();
    }
  }

  closeEvent() {
    this.confirmTicketDTO.listConfirmTicketAttachmentDTOs = this.listConfirmTicketAttachmentDTOs;
    this.confirmTicketDTO.status = '5';
    this.confirmTicketDTO.content = this.form.controls.content.value;
    console.log('dong su vu: ', this.confirmTicketDTO);
    this.confirmTicketService.createConfirmTicket(this.confirmTicketDTO).subscribe(res => {
      if (res.status === 'SUCCESS') {
        // disable
        // tslint:disable-next-line:forin
        for (const controlName in this.form.controls) {
          this.form.get(controlName).disable();
        }
        this.isDisabled = true;
        // disable toàn bộ MH08
        this.disableToParent.emit(this.isDisabled);
        this.getListHistoryConfirmTicket(this.confirmTicketDTO);
        this.showNotifySuccess();
      } else {
        this.showUnknownErr();
      }
    });
  }

  viewConfirm(record) {
    const data: any = {};
    if (record.fillNameEncrypt.indexOf('.pdf') !== -1) {
      this.confirmTicketService.downloadFile(record.fillNameEncrypt).subscribe((responseMessage) => {
        const file = new Blob([responseMessage], {type: 'application/pdf'});
        data.fileURL = URL.createObjectURL(file);
        data.check = 1;
        this.matDialog.open(ViewAttachmentComponent, {
          width: '80vw',
          hasBackdrop: true,
          data: data
        });
      });
    } else if (record.fillNameEncrypt.indexOf('.xls') !== -1 ||
      record.fillNameEncrypt.indexOf('.xlsx') !== -1) {
      this.confirmTicketService.downloadFile(record.fillNameEncrypt).subscribe((responseMessage) => {
        const file = new Blob([responseMessage], {type: 'application/vnd.ms-excel'});
        const fileURL = URL.createObjectURL(file);
        window.open(fileURL, '_blank');
      });
    } else if (record.fillNameEncrypt.indexOf('.doc') !== -1 ||
      record.fillNameEncrypt.indexOf('.docx') !== -1) {
      this.confirmTicketService.downloadFile(record.fillNameEncrypt).subscribe((responseMessage) => {
        const file = new Blob([responseMessage], {type: 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'});
        const fileURL = URL.createObjectURL(file);
        window.open(fileURL, '_blank');
      });
    } else if (record.fillNameEncrypt.indexOf('.jpg') !== -1 || record.fillNameEncrypt.toUpperCase().indexOf('.PNG') !== -1) {
      this.confirmTicketService.downloadFile(record.fillNameEncrypt).subscribe((responseMessage) => {
        const file = new Blob([responseMessage], {type: 'image/jpg'});
        data.fileURL = URL.createObjectURL(file);
        data.check = 2;
        this.matDialog.open(ViewAttachmentComponent, {
          width: '80vw',
          hasBackdrop: true,
          data: data
        });
      });
    } else {
      this.confirmTicketService.downloadFile(record.fillNameEncrypt).subscribe((responseMessage) => {
        const file = new Blob([responseMessage], {type: 'blod'});
        const fileURL = URL.createObjectURL(file);
        window.open(fileURL, '_blank');
      });
    }
  }

  deleteConfirmAttachment(record) {
    const title = this.translateService.instant('common.confirm.title.delete');
    const content = this.translateService.instant('common.MSG0017');
    const dialogContent: ConfirmDialogModel = new ConfirmDialogModel(title, content);
    this.dialog.originalOpen(ConfirmDialogComponent, {
      width: '25vw',
      disableClose: true,
      hasBackdrop: true,
      data: dialogContent,
    }).afterClosed().subscribe((next: any) => {
      if (next.data === 'confirm') {
        this.confirmTicketService.deleteConfirmAttachment(record).subscribe(data => {
          this.listConfirmTicketAttachmentDTOs = this.listConfirmTicketAttachmentDTOs
            .filter(item => item.fillNameEncrypt !== record.fillNameEncrypt);
          this.dataListAttachmentConfirmPagible = [...this.listConfirmTicketAttachmentDTOs].splice(0, this.pagesize);
          this.toastr.success(this.translateService.instant('common.attachment.delete.success'));
        });
      }
    });
  }

  isDisableForm() {
    return !this.eventStatus || this.eventStatus === '1' || this.eventStatus === '2' || this.eventStatus === '5' || this.isDisabled || this.isPermission;
  }

  onPageChangeConfirm(event) {
    const position = event.pageIndex * event.pageSize;
    this.dataListAttachmentConfirmPagible = [...this.listConfirmTicketAttachmentDTOs].splice(position, event.pageSize);
  }

  showNotifySuccess() {
    this.toastr.success(this.translateService.instant('common.notify.save.success2'));
  }

  showUnknownErr() {
    this.toastr.error(this.translateService.instant('common.notify.fail'));
  }

  initFormGroup(){
    this.form = this.fb.group({
      content: ['', Validators.required],
      level: ['', Validators.required],
      file: [''],
      confirmDeadline: ['']
    });
    this.showValidate = true;
  }
}
