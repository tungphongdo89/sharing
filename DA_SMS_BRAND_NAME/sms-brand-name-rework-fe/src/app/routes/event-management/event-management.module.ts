import {NgModule} from '@angular/core';
import {CommonModule, DatePipe} from '@angular/common';
import {SharedModule} from '@shared/shared.module';
import {EventManagementRouting} from '@app/routes/event-management/event-management.routing';
import {ConfirmCustomerComponent} from '@app/routes/event-management/confirm-customer/confirm-customer.component';
import {EventReceiveComponent} from '@app/routes/event-management/event-receive/event-receive.component';
import {EventCloseComponent} from '@app/routes/event-management/event-close/event-close.component';
import {EventHandlingComponent} from '@app/routes/event-management/event-handling/event-handling.component';
import {EventManagementComponent} from '@app/routes/event-management/event-management.component';
import { TableEmailPhoneComponent } from './event-receive/table-email-phone/table-email-phone.component';
import { ViewAttachmentComponent } from './view-attachment/view-attachment.component';

@NgModule({
  declarations: [
    EventManagementComponent,
    ConfirmCustomerComponent,
    EventReceiveComponent,
    EventCloseComponent,
    EventHandlingComponent,
    TableEmailPhoneComponent,
    ViewAttachmentComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    EventManagementRouting,
  ],
  entryComponents: [],
  providers: [
    DatePipe
  ]
})
export class EventManagementModule {
}
