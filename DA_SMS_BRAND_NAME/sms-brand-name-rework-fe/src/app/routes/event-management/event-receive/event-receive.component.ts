import {Title} from '@angular/platform-browser';
import {
  AfterViewInit,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnInit,
  Output,
  TemplateRef,
  ViewChild,
} from '@angular/core';
import {MtxDialog, MtxGridColumn} from '@ng-matero/extensions';
import {FeeStatus} from '@core/models/fee-status';
import {CertificateModel} from '@core/models/certificate-model';
import {FormArray, FormBuilder, FormControl, FormGroup, ValidatorFn, Validators} from '@angular/forms';
import {EventService} from '@core/services/envent-handling/event-service';
import {TicketModel} from '@core/models/ticket-model';
import {EventModel} from '@core/models/event-model';
import {DepartmentModel} from '@core/models/department-model';
import {DepartmentService} from '@core/services/envent-handling/department-service';
import {CustomerModel} from '@core/models/customer-model';
import {CustomerService} from '@core/services/envent-handling/customer-service';
import {ToastrService} from 'ngx-toastr';
import {TranslateService} from '@ngx-translate/core';
import {ListTicketRequestAttachment} from '@core/models/list-ticket-request-attachment';
import {DatePipe} from '@angular/common';
import {ProcessTicketModel} from '@core/models/process-ticket-model';
import {ProcessTicketService} from '@core/services/envent-handling/process-ticket-service';
import {ActivatedRoute, Router} from '@angular/router';
import {ConfigSchedule} from '@core/services/config-author/config-schedule';
import {ConfigScheduleModel} from '@core/models/config-schedule-model';
import * as moment from 'moment';
import {TicketRequestService} from '@core/services/envent-handling/ticket-request-service';
import {ConfirmTicketModel} from '@app/core/models/confirm-ticket-model';
import {ApDomainService} from '@core/services/ap-domain/ap-domain.service';
import {BussinessTypeService} from '@core/services/bussiness-type/bussiness-type.service';
import {TicketRequestAttachmentService} from '@core/services/envent-handling/ticket-request-attachment-service';
import {ConfirmDialogComponent, ConfirmDialogModel} from '@shared/components/confirm-dialog/confirm-dialog.component';
import {MatDialog} from '@angular/material/dialog';
import {TableEmailPhoneComponent} from '@app/routes/event-management/event-receive/table-email-phone/table-email-phone.component';
import {ViewAttachmentComponent} from '../view-attachment/view-attachment.component';
import {ConfirmCustomerComponent} from '@app/routes/event-management/confirm-customer/confirm-customer.component';
import {COMMOM_CONFIG} from '@env/environment';

@Component({
  selector: 'app-event-receive',
  templateUrl: './event-receive.component.html',
  styleUrls: ['./event-receive.component.scss'],
})
export class EventReceiveComponent implements OnInit, AfterViewInit {
  @ViewChild('inputElement', {static: false}) fileInput: ElementRef;
  @ViewChild('timeReceive', {static: true}) timeReceive: TemplateRef<any>;
  @ViewChild('channelTypeView', {static: true}) channelTypeView: TemplateRef<any>;
  @ViewChild('status', {static: true}) status: TemplateRef<any>;
  @ViewChild('createdUser', {static: true}) createdUser: TemplateRef<any>;
  @ViewChild('inputStatus') inputStatusTicket;
  @ViewChild('inputTicketRequest') inputTicketRequest;
  @ViewChild('createUser') createUser;
  @ViewChild('createUser') horizontalStepper: any;
  @ViewChild('expiredDate', {static: true}) expiredDate: TemplateRef<any>;
  @ViewChild(ConfirmCustomerComponent, {static: true}) ccc: ConfirmCustomerComponent;
  @Input() dataHistorySupport: TicketModel[] = [];
  @Input() dataFeeStatus: FeeStatus[] = [];
  @Input() dataCertificate: CertificateModel[] = [];
  @Input() dataDepartments: DepartmentModel[] = [];
  @Input() dataCustomers: CustomerModel[] = [];
  @Input() dataListRequestAttachment: any[] = [];
  dataListRequestAttachmentPagible: any[] = [];
  @Input() dataListAttachment: any[] = [];
  dataListAttachmentPagible: any[] = [];
  @Input() dataHistoryProcessTicket: any[] = [];
  @Output() pageChange: EventEmitter<any> = new EventEmitter<any>();
  @Output() dataTicket: EventEmitter<EventModel> = new EventEmitter<EventModel>();
  @Input() channel: any[];
  selectRequest: any[];
  selectBusiness: any[];
  prioritize: any[];
  notify: any[];
  index = 2;
  idTab = 2;
  lstPhoneAndEmail: any[] = [];
  listRequest: any[] = [
    {id: 1, label: 'Yêu cầu 1'},
  ];
  latestAttachment: any[] = [];
  selected = new FormControl(0);
  columnsHistorySupp: MtxGridColumn[] = [];
  columnsFeeStatus: MtxGridColumn[] = [];
  columnsCertificate: MtxGridColumn[] = [];
  columnsRequestAttachment: MtxGridColumn[] = [];
  columnsAttachment: MtxGridColumn[] = [];
  isLoading: true;
  pagesize = 5;
  pageSizeList = [5, 10, 50, 100];
  totalAttachmentProcess: number = 0;
  totalAttachmentRequest: number = 0;
  eventDTO: EventModel = new EventModel();
  formRequest: FormGroup;
  fileNameRequest: any[] = [];
  listTicketRequestAttachment: ListTicketRequestAttachment = new ListTicketRequestAttachment();
  pipe: any;
  fcr: boolean;
  eventStatus: number;
  processTicketDTO: ProcessTicketModel = new ProcessTicketModel();
  confirmTicketDTO: ConfirmTicketModel = new ConfirmTicketModel();
  ticketId: any;
  ticketRequestId: any[] = [];
  departmentId: any;
  configModel: ConfigScheduleModel = new ConfigScheduleModel();
  feedBackTime: Date;
  isShow = true;
  offset: any = 0;
  total: any = 0;
  cid: string;
  ticketModel: TicketModel = new TicketModel();
  ticketStatus: any;
  ticketPermission: any;
  // HieuNT
  checkAddRequest: boolean;
  checkDisable: boolean;
  checkStatusRequest: any[] = [];
  checkPermission: any[] = [];
  isDisabled: boolean = false;
  disableForm: FormGroup;
  selectIndex: any;
  fileSave: any;
  selectTab: any;
  showHandlingForm: boolean = false;
  idTicket: any;

  dataGetHistory: ConfirmTicketModel[] = [];
  fileNameProcess: any[] = [];
  x: number;
  arrBuffer: any[] = [];
  arrBufferProcess: any[] = [];
  currentTabIndex: number;

  isOverride:boolean;

  constructor(private eventService: EventService, private departmentService: DepartmentService,
              private customerService: CustomerService, private fb: FormBuilder,
              private ticketService: EventService,
              protected toastr: ToastrService,
              protected translateService: TranslateService,
              private router: Router,
              private processTicketService: ProcessTicketService,
              private route: ActivatedRoute, private configService: ConfigSchedule,
              private ticketRequestService: TicketRequestService,
              private ticketRequestAttachmentService: TicketRequestAttachmentService,
              private apDomainService: ApDomainService,
              private bussinessTypeService: BussinessTypeService,
              private dialog: MtxDialog,
              private matDialog: MatDialog,
              private titleService: Title,
              private datePipe: DatePipe) {
    this.getTicketbyId();
  }

  ngOnInit(): void {
    if(this.route.snapshot.queryParamMap.get('viewOnly') === 'true'){
      this.isOverride = true;
    }else {
      this.isOverride = false;
    }
    this.route.paramMap.subscribe(paramMap => {
      this.idTicket = paramMap.get('id');
      if (this.idTicket) {
        this.titleService.setTitle(this.translateService.instant('menu.update-ticket-title'));
      } else {
        this.titleService.setTitle(this.translateService.instant('menu.ticket-title-normal'));
      }
    });
    // this.getTicketbyId();
    this.columnsHistorySupp = [
      {i18n: 'eventCode', field: 'ticketId'},
      {i18n: 'eventStatus', field: 'status', cellTemplate: this.status},
      {i18n: 'channel', field: 'channelType', cellTemplate: this.channelTypeView},
      {i18n: 'timeReceive', field: 'createDatetime', cellTemplate: this.timeReceive},
      {i18n: 'userReceive', field: 'customerName', cellTemplate: this.createdUser},
    ];
    this.columnsFeeStatus = [
      {i18n: 'feeType', field: 'feeType'},
      {i18n: 'feeStatus', field: 'status'},
      {i18n: 'amount', field: 'feeTotal'},
      {i18n: 'deadline', field: 'paymentExpDate'},
    ];
    this.columnsCertificate = [
      {i18n: 'userName', field: 'userName'},
      {i18n: 'user', field: 'fullName'},
      {i18n: 'ctsStatus', field: 'status'},
      {i18n: 'expiredDate', field: 'caExpDate'},
    ];
    this.columnsRequestAttachment = [
      {i18n: 'fileName', field: 'fileName'},
      {i18n: 'timeUpload', field: 'createDatetime', cellTemplate: this.timeReceive},
      {
        i18n: 'tool',
        field: 'options',
        width: '200px',
        pinned: 'right',
        right: '0px',
        type: 'button',
        buttons: [
          {
            type: 'icon',
            text: 'view',
            icon: 'visibility',
            click: record => this.viewFileRequest(record, 'view'),
          },
          {
            type: 'icon',
            text: 'delete',
            icon: 'delete',
            click: record => this.deleteRequestAttachment(record),
          },
        ],
      },
    ];
    this.columnsAttachment = [
      {i18n: 'fileName', field: 'fileName'},
      {i18n: 'timeUpload', field: 'createDatetime', cellTemplate: this.timeReceive},
      {
        i18n: 'tool',
        field: 'options',
        width: '200px',
        pinned: 'right',
        right: '0px',
        type: 'button',
        buttons: [
          {
            type: 'icon',
            text: 'view',
            icon: 'visibility',
            click: record => this.viewFileProcess(record, 'view'),
          },
          {
            type: 'icon',
            text: 'delete',
            icon: 'delete',
            click: record => this.deleteProcessAttachment(record),
          },
        ],
      },
    ];
    this.disableForm = this.fb.group({
      name: [null, [Validators.required]],
    });
    this.formRequest = this.fb.group({
      channelType: ['', [Validators.required]],
      ticketCode: '',
      phoneNumber: [null, Validators.pattern(COMMOM_CONFIG.NUMBER_PHONE_FORMAT)],
      name: '',
      email: ['', Validators.pattern(COMMOM_CONFIG.EMAIL_FORMAT)],
      contactPhone: '',
      status: '',
      fcr: '',
      cid: '',
      taxCode: '',
      companyName: '',
      address: '',
      listTicketRequestDTOS: this.fb.array([this.createTab()]),
    }, {
      validator: [this.checkValidateHotline('phoneNumber', 'channelType', ''),
        this.checkValidateEmail('email', 'channelType', '')],
    });
    this.getHistorySupport();
    this.getListDepartmentsForCombobox();
    this.getListCustomersForCombobox();
    this.confirmTicketDTO.createUser = Number(localStorage.getItem('user'));
    // request_type
    this.apDomainService.getDomainByType('REQUEST_TYPE').subscribe(res => {
      this.selectRequest = res.map(e => {
        e.code = Number(e.code);
        return e;
      });
    });
    // channel
    this.apDomainService.getDomainByType('CHANEL_TYPE').subscribe(res => {
      this.channel = res;
    });
    // business_type
    this.bussinessTypeService.getBussinessType().subscribe(res => {
      this.selectBusiness = res;
    });
    // ticket priority
    this.apDomainService.getDomainByType('TICKET_PRIORITY').subscribe(res => {
      this.prioritize = res;
    });
    // time notify
    this.apDomainService.getDomainByType('TIME_NOTIFY').subscribe(res => {
      this.notify = res;
    });
  }

  getTicketbyId() {
    this.route.params.subscribe(data => {
      if (data && data.id) this.patchValueForm({id: data.id});
      this.showHandlingForm = true;
    });
  }

  onPageChange(event) {
    this.offset = event.pageIndex;
    this.pagesize = event.pageSize;
    this.getHistorySupport();
  }

  onChangeEvent(event) {
    this.fcr = event.checked;
  }

  async getHistorySupport(reset?: boolean): Promise<void> {
    if (reset) {
      this.offset = 0;
      this.pagesize = 5;
    }
    this.ticketModel.cid = this.formRequest.get('cid').value;
    console.log('ticketModel', this.ticketModel);
    // @ts-ignore
    const res = await this.eventService.getHistorySupport<any>(this.ticketModel, {
      page: this.offset,
      size: this.pagesize
    }).toPromise();
    console.log('resevent', res);
    this.dataHistorySupport = res.body;
    this.total = Number(res.headers.get('X-Total-Count'));
    // this.eventService.getHistorySupport(this.ticketModel, {page: this.offset, size: this.pagesize}).subscribe(res => {
    //   console.log('history', res);
    //   this.dataHistorySupport = res.body;
    //   this.total = Number(res.headers.get('X-Total-Count'));
    // });
    const data = {
      orgCode: this.formRequest.get('cid').value,
    };
    // tslint:disable-next-line:no-shadowed-variable
    const info = await this.ticketService.getPhoneAndEmailDetail(data).toPromise().then(res => {
      console.log('data', res);
      if (res?.body) {
        this.formRequest.get('cid').patchValue(res.body?.orgInfo?.orgCode);
        this.formRequest.get('taxCode').patchValue(res.body?.orgInfo?.taxCode);
        this.formRequest.get('companyName').patchValue(res.body?.orgInfo?.orgFullName);
        this.formRequest.get('address').patchValue(res.body?.orgInfo?.officeAdd);
        const feeData = res.body?.orgInfo?.feeDebtInfos;
        const certificate = res.body?.orgInfo?.caInfos;
        this.dataFeeStatus = feeData;
        this.dataCertificate = certificate;
      } else {
        this.formRequest.get('cid').setValue('');
        this.formRequest.get('taxCode').setValue('');
        this.formRequest.get('companyName').setValue('');
        this.formRequest.get('address').setValue('');
        this.dataFeeStatus = [];
        this.dataCertificate = [];
      }
    });
  }

  submit() {
    this.formRequest.markAllAsTouched();
    if (this.formRequest.invalid) {
      this.showRequiredErr();
      return;
    }
    if (this.formRequest.controls.channelType.value === '1'
      && (this.formRequest.controls.phoneNumber.value === '' ||
        this.formRequest.controls.phoneNumber.value === null)
    ) {
      this.toastr.error(this.translateService.instant('common.notify.handling.error.hotline'));
      return;
    }

    if (this.formRequest.controls.channelType.value === '3'
      && (this.formRequest.controls.email.value.trim() === '' ||
        this.formRequest.controls.email.value === null)
    ) {
      this.toastr.error(this.translateService.instant('common.notify.handling.error.email'));
      return;
    }
    if (this.formRequest.controls.channelType.value === '3' &&
      this.formRequest.controls.email.value !== null &&
      !this.validateEmail(this.formRequest.controls.email.value.trim())) {
      this.toastr.error(this.translateService.instant('common.notify.handling.error.email2'));
      return;
    }

    this.pipe = new DatePipe('en-US');
    this.eventDTO = this.formRequest.value;
    let idx = 0;
    for (const list of this.eventDTO.listTicketRequestDTOS) {
      if (list.deadline) {
        const deadline = this.pipe.transform(list.deadline, 'yyyy-MM-dd\'T\'HH:mm:ss\'Z\'');
        list.deadline = deadline;
      }
      if (list.confirmDate) {
        const confirmDate = this.pipe.transform(list.confirmDate, 'yyyy-MM-dd\'T\'HH:mm:ss\'Z\'');
        list.confirmDate = confirmDate;
      }
      list.createDatetime = null;
      this.departmentId = list.departmentId;
      const createUser = Number(localStorage.getItem('user'));
      list.createUser = createUser;
      list.listTicketRequestAttachmentDTOS = this.dataListRequestAttachment[idx];
      idx = idx + 1;
    }
    if (this.fcr === true) {
      this.eventDTO.fcr = '' + 1;
    } else this.eventDTO.fcr = '' + 0;
    this.eventDTO.createUser = Number(localStorage.getItem('user'));
    this.eventDTO.customerId = 0;
    this.eventDTO.ticketId = this.route.snapshot.params.id;
    this.ticketService.insertEvent(this.eventDTO).subscribe(res => {
        if ('SUCCESS' === res.body.status) {
          debugger;
          this.patchValueForm({id: res.body.ticketId});
          this.showNotifySuccess();
          // this.formRequest.reset();
          res.body.listTicketRequestIds.forEach((ticket, index) => {
            this.eventDTO.listTicketRequestDTOS[index][`ticketRequestId`] = ticket;
          });
          this.formRequest.markAsUntouched();
          this.router.navigate(['/event-receive/', res.body.ticketId]);
        } else {
          this.showUnknownErr();
        }
      },
      (error) => {
        if (error.status === 500) {
          this.showUnknownErr();
        } else if (error.status === 400) {
          console.log(error);
          if (error.error.errorKey === 'user.has.no.right.to.create.ticket') {
            this.toastr.error(this.translateService.instant('common.You do not have the right to create cases'));
          } else {
            this.toastr.error(this.translateService.instant('common.notify.handling.error'));
          }
        }
      });
  }

  onChangeFileProcess(event, j: number) {
    const len = event.target.files.length;
    for (let i = 0; i < len; i++) {
      this.arrBufferProcess.push(event.target.files[i]);
    }
    this.fileNameProcess[j] = this.arrBufferProcess;
  }

  onChangeFileRequest(event, j: number): void {
    console.log('event.target.files', event.target.files);
    const len = event.target.files.length;
    for (let i = 0; i < len; i++) {
      this.arrBuffer.push(event.target.files[i]);
    }
    this.fileNameRequest[j] = this.arrBuffer;
  }

  insertFileProcess(i: number) {
    this.currentTabIndex = i;
    this.processTicketService.uploadFile(this.fileNameProcess[i]).subscribe(
      (res) => {
        this.latestAttachment[i] = res;
        if (typeof this.dataListAttachment[i] !== 'undefined') {
          this.dataListAttachment[i] = [...this.dataListAttachment[i], ...res];
          this.totalAttachmentProcess = this.dataListAttachment[i].length;
          this.dataListAttachmentPagible[i] = [...this.dataListAttachment[i]].splice(0, this.pagesize);
        } else {
          this.dataListAttachment[i] = res;
          this.totalAttachmentProcess = this.dataListAttachment[i].length;
          this.dataListAttachmentPagible[i] = [...this.dataListAttachment[i]].splice(0, this.pagesize);
        }
        this.arrBufferProcess = [];
        this.showNotifySuccess();
      },
      (error) => {
        alert(error.error.message);
      },
    );
  }

  insertFileRequest(i: number) {
    this.currentTabIndex = i;
    this.ticketService.uploadFile(this.fileNameRequest[i]).subscribe(
      (res) => {
        this.latestAttachment[i] = res.listRequestAttachmentDTOList;
        this.dataListRequestAttachment[i] = res.listRequestAttachmentDTOList;
        this.totalAttachmentRequest = this.dataListRequestAttachment[i].length;
        this.dataListRequestAttachmentPagible[i] = [...this.dataListRequestAttachment[i]].splice(0, this.pagesize);
        this.showNotifySuccess();
      },
      (error) => {
        alert(error.error.message);
      },
    );
  }

  showNotifySuccess() {
    this.toastr.success(this.translateService.instant('common.notify.save.success2'));
  }

  showUnknownErr() {
    this.toastr.error(this.translateService.instant('common.notify.fail'));
  }

  showRequiredErr() {
    this.toastr.error(this.translateService.instant('validation.MSG0006'));
  }

  getListDepartmentsForCombobox() {
    this.departmentService.getListDepartmentsForCombobox().subscribe(res => {
      this.dataDepartments = res;
    });
  }

  getListCustomersForCombobox() {
    this.customerService.getListCustomersForCombobox().subscribe(res => {
      this.dataCustomers = res;
    });
  }

  createTab(): FormGroup {
    this.arrBuffer = [];
    return this.fb.group({
      ticketRequestCode: '',
      requestType: ['', [Validators.required]],
      bussinessType: ['', [Validators.required]],
      priority: ['', [Validators.required]],
      departmentId: '',
      status: '',
      content: ['', [Validators.required]],
      deadline: ['', [Validators.required]],
      confirmDate: [''],
      timeNotify: '',
      createDatetime: moment().format('DD/MM/YYYY, h:mm:ss a'),
      createUser: '',
      createUserName: '',
      contentHandling: '',
      deadlineHandling: '',
      ticketRequestId: null,
      listTicketRequestAttachmentDTOS: this.fb.array([this.createFile()]),
    });
  }

  createFile(): FormGroup {
    return this.fb.group({
      fileName: '',
      createDatetime: '',
    });
  }

  addTab() {
    const items = this.formRequest.get('listTicketRequestDTOS') as FormArray;
    items.push(this.createTab());
    this.selectTab = items.length;
  }

  get f() {
    return this.formRequest.controls;
  }

  getListHistoryProcessTicket(processTicket: ProcessTicketModel, index) {
    this.processTicketService.getListHistoryProcessTickets(processTicket).subscribe(res => {
      this.dataHistoryProcessTicket[index] = res.filter(item => item.content !== '');
    });
  }

  createTicket(formGroupName) {
    const data: any = this.formRequest.controls.listTicketRequestDTOS;
    data.controls[formGroupName].get('contentHandling').setValidators(Validators.required);
    data.controls[formGroupName].get('contentHandling').updateValueAndValidity();
    data.controls[formGroupName]?.markAllAsTouched();
    if (data.controls[formGroupName].invalid) return;
    this.processTicketDTO.ticketRequestId = data.controls[formGroupName].get('ticketRequestId').value;
    this.processTicketDTO.content = data.controls[formGroupName].get('contentHandling').value;
    if (typeof this.currentTabIndex !== 'undefined') {
      this.processTicketDTO.listProcessTicketAttachmentDTOS = this.latestAttachment[this.currentTabIndex];
    }
    this.processTicketDTO.status = '2';
    this.processTicketDTO.createUser = Number(localStorage.getItem('user'));
    this.processTicketDTO.check = 1;
    this.processTicketService.createProcessTicket(this.processTicketDTO).subscribe(res => {
      if ('SUCCESS' === res.status) {
        this.getListHistoryProcessTicket(this.processTicketDTO, formGroupName);
        this.showNotifySuccess();
      } else {
        this.toastr.error(this.translateService.instant('common.notify.handling.error'));
      }
    });
  }

  closeTicket(formGroupName) {
    const data: any = this.formRequest.controls.listTicketRequestDTOS;
    data.controls[formGroupName].get('contentHandling').setValidators(Validators.required);
    data.controls[formGroupName].get('contentHandling').updateValueAndValidity();
    data.controls[formGroupName]?.markAllAsTouched();
    if (data.controls[formGroupName].invalid) return;
    this.processTicketDTO.ticketRequestId = data.controls[formGroupName].get('ticketRequestId').value;
    this.processTicketDTO.content = data.controls[formGroupName].get('contentHandling').value;
    this.processTicketDTO.listProcessTicketAttachmentDTOS = [];
    this.processTicketDTO.status = '3';
    this.processTicketDTO.createUser = Number(localStorage.getItem('user'));
    this.processTicketDTO.check = 2;
    this.processTicketService.createProcessTicket(this.processTicketDTO).subscribe(res => {
      if ('SUCCESS' === res.status) {
        this.showNotifySuccess();
        this.getListHistoryProcessTicket(this.processTicketDTO, formGroupName);
        this.ticketService.getTicketById(this.idTicket)
          .subscribe((resNewTicket: any) => {
            console.log('resSub', resNewTicket);
            console.log('old', this.confirmTicketDTO);
            this.confirmTicketDTO = {...this.confirmTicketDTO, ...resNewTicket.confirmDeadline};
            this.getTicketReqByTicketId();
          });
      } else {
        this.toastr.error(this.translateService.instant('common.notify.handling.error'));
      }
    });
  }

  getTicketReqByTicketId() {
    this.ticketRequestService.getTicketRequestByTicketId(this.ticketModel).subscribe((next: any) => {
      for (let i = 0; i < next.length; i++) {
        console.log('next resTicketRq >>> ', next);
        this.checkStatusRequest[i] = next[i].status;
        this.checkPermission[i] = next[i].disableCloseSave;
      }
    });
  }

  getProcessTime(formGroupName) {
    const data: any = this.formRequest.controls.listTicketRequestDTOS;
    this.eventDTO = this.formRequest.value;
    for (const item of this.eventDTO.listTicketRequestDTOS) {
      this.configModel.requestType = item.requestType;
      this.configModel.bussinessType = item.bussinessType;
    }
    this.configService.getProcessTime(this.configModel).subscribe(res => {
      if ('SUCCESS' === res.status) {
        this.pipe = new DatePipe('en-US');
        console.log('ressssssss', res);
        data.controls[formGroupName].patchValue({
          deadline: moment(res.processDate).toDate(),
        });
      } else {
        data.controls[formGroupName].patchValue({
          deadline: null,
        });
      }
    });
  }

  changeSteep(event: any) {
    const id = 'step' + (event + 1);
    document.getElementById(id).scrollIntoView();
    document.getElementById(id).focus();
    // window.location.hash ='steep';
  }

  disableAll(event) {
    if(event === true){
      this.checkAddRequest = true;
    }
  }

  getStep(ticketStatus: string) {
    switch (ticketStatus) {
      case '1':
        return 0;
        break;
      case '2':
        return 1;
        break;
      case '3':
        return 2;
        break;
      case '4':
        return 2;
        break;
      default:
        return 0;
    }
  }

  ngAfterViewInit(): void {
  }

  removeItem(i) {
    if (i === 0) {
      return;
    }
    const title = this.translateService.instant('common.confirm.title.delete');
    const content = this.translateService.instant('common.MSG0017');
    const dialogContent: ConfirmDialogModel = new ConfirmDialogModel(title, content);
    this.dialog.originalOpen(ConfirmDialogComponent, {
      width: '25vw',
      disableClose: true,
      hasBackdrop: true,
      data: dialogContent,
    }).afterClosed().subscribe(res => {
        if (res.data === 'confirm') {
          (this.formRequest.controls.listTicketRequestDTOS as FormArray).removeAt(i);
        }
      },
    );
  }

  onTabChanged(event) {
    this.arrBufferProcess = [];
  }

  viewFileProcess(record, action: string) {
    const data: any = {};
    if (record.fillNameEncrypt.indexOf('.pdf') !== -1) {
      this.processTicketService.downloadFile(record.fillNameEncrypt).subscribe((responseMessage) => {
        const file = new Blob([responseMessage], {type: 'application/pdf'});
        data.fileURL = URL.createObjectURL(file);
        data.check = 1;
        this.matDialog.open(ViewAttachmentComponent, {
          width: '80vw',
          hasBackdrop: true,
          data
        });
      });
    } else if (record.fillNameEncrypt.indexOf('.xls') !== -1 ||
      record.fillNameEncrypt.indexOf('.xlsx') !== -1) {
      this.processTicketService.downloadFile(record.fillNameEncrypt).subscribe((responseMessage) => {
        const file = new Blob([responseMessage], {type: 'application/vnd.ms-excel'});
        const fileURL = URL.createObjectURL(file);
        window.open(fileURL, '_blank');
      });
    } else if (record.fillNameEncrypt.indexOf('.doc') !== -1 ||
      record.fillNameEncrypt.indexOf('.docx') !== -1) {
      this.processTicketService.downloadFile(record.fillNameEncrypt).subscribe((responseMessage) => {
        const file = new Blob([responseMessage], {type: 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'});
        const fileURL = URL.createObjectURL(file);
        window.open(fileURL, '_blank');
      });
    } else if (record.fillNameEncrypt.indexOf('.jpg') !== -1 || record.fillNameEncrypt.toUpperCase().indexOf('.PNG') !== -1) {
      this.processTicketService.downloadFile(record.fillNameEncrypt).subscribe((responseMessage) => {
        const file = new Blob([responseMessage], {type: 'image/jpg'});
        data.fileURL = URL.createObjectURL(file);
        data.check = 2;
        this.matDialog.open(ViewAttachmentComponent, {
          width: '80vw',
          hasBackdrop: true,
          data
        });
      });
    } else {
      this.processTicketService.downloadFile(record.fillNameEncrypt).subscribe((responseMessage) => {
        const file = new Blob([responseMessage], {type: 'blod'});
        const fileURL = URL.createObjectURL(file);
        window.open(fileURL, '_blank');
      });
    }
  }

  viewFileRequest(record, action: string) {
    const data: any = {};
    if (record.fillNameEncrypt.indexOf('.pdf') !== -1) {
      this.eventService.downloadFile(record.fillNameEncrypt).subscribe((responseMessage) => {
        const file = new Blob([responseMessage], {type: 'application/pdf'});
        data.fileURL = URL.createObjectURL(file);
        data.check = 1;
        this.matDialog.open(ViewAttachmentComponent, {
          width: '80vw',
          hasBackdrop: true,
          data
        });
      });
    } else if (record.fillNameEncrypt.indexOf('.xls') !== -1 ||
      record.fillNameEncrypt.indexOf('.xlsx') !== -1) {
      this.eventService.downloadFile(record.fillNameEncrypt).subscribe((responseMessage) => {
        const file = new Blob([responseMessage], {type: 'application/vnd.ms-excel'});
        const fileURL = URL.createObjectURL(file);
        window.open(fileURL, '_blank');
      });
    } else if (record.fillNameEncrypt.indexOf('.doc') !== -1 ||
      record.fillNameEncrypt.indexOf('.docx') !== -1) {
      this.eventService.downloadFile(record.fillNameEncrypt).subscribe((responseMessage) => {
        const file = new Blob([responseMessage], {type: 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'});
        const fileURL = URL.createObjectURL(file);
        window.open(fileURL, '_blank');
      });
    } else if (record.fillNameEncrypt.indexOf('.jpg') !== -1 || record.fillNameEncrypt.toUpperCase().indexOf('.PNG') !== -1) {
      this.eventService.downloadFile(record.fillNameEncrypt).subscribe((responseMessage) => {
        const file = new Blob([responseMessage], {type: 'image/jpg'});
        data.fileURL = URL.createObjectURL(file);
        data.check = 2;
        this.matDialog.open(ViewAttachmentComponent, {
          width: '80vw',
          hasBackdrop: true,
          data
        });
      });
    } else {
      this.eventService.downloadFile(record.fillNameEncrypt).subscribe((responseMessage) => {
        const file = new Blob([responseMessage], {type: 'blod'});
        const fileURL = URL.createObjectURL(file);
        window.open(fileURL, '_blank');
      });
    }
  }

  deleteRequestAttachment(record: any) {
    const title = this.translateService.instant('common.confirm.title.delete');
    const content = this.translateService.instant('common.MSG0017');
    const dialogContent: ConfirmDialogModel = new ConfirmDialogModel(title, content);
    this.dialog.originalOpen(ConfirmDialogComponent, {
      width: '25vw',
      disableClose: true,
      hasBackdrop: true,
      data: dialogContent,
    }).afterClosed().subscribe((next: any) => {
      if (next.data === 'confirm') {
        this.ticketRequestAttachmentService.deleteRequestAttachment(record).subscribe(data => {
          // reload list request attachment
          this.ticketRequestAttachmentService.getTicketRequestAttachmentById(record.ticketRequestId).subscribe(response => {
            for (let i = 0; i < this.dataListRequestAttachment.length; i++) {
              for (let j = 0; j < this.dataListRequestAttachment[i].length; j++) {
                if (this.dataListRequestAttachment[i][j].ticketRequestId === record.ticketRequestId) {
                  this.dataListRequestAttachment[i] = response;
                }
              }
            }
            this.toastr.success(this.translateService.instant('common.attachment.delete.success'));
          });
        });
      }
    });
  }

  deleteProcessAttachment(record: any) {
    const title = this.translateService.instant('common.confirm.title.delete');
    const content = this.translateService.instant('common.MSG0017');
    const dialogContent: ConfirmDialogModel = new ConfirmDialogModel(title, content);
    this.dialog.originalOpen(ConfirmDialogComponent, {
      width: '25vw',
      disableClose: true,
      hasBackdrop: true,
      data: dialogContent,
    }).afterClosed().subscribe((next: any) => {
      if (next.data === 'confirm') {
        this.processTicketService.deleteProcessAttachment(record).subscribe(data => {
          if (record.ticketRequestId !== null) {
            this.processTicketService.getTicketProcessAttachment(record.ticketRequestId).subscribe(response => {
              for (let i = 0; i < this.dataListAttachment.length; i++) {
                for (let j = 0; j < this.dataListAttachment[i].length; j++) {
                  if (this.dataListAttachment[i][j].ticketRequestId === record.ticketRequestId) {
                    this.dataListAttachment[i] = response;
                    this.totalAttachmentProcess = this.dataListAttachment[i].length;
                    this.dataListAttachmentPagible[i] = [...this.dataListAttachment[i]].splice(0, this.pagesize);
                    break;
                  }
                }
              }
            });
          } else {
            for (let i = 0; i < this.dataListAttachment.length; i++) {
              for (let j = 0; j < this.dataListAttachment[i].length; j++) {
                this.dataListAttachment[i] = this.dataListAttachment[i]
                  .filter(item => this.dataListAttachment[i][j].fillNameEncrypt !== item.fillNameEncrypt);
                this.totalAttachmentProcess = this.dataListAttachment[i].length;
                this.dataListAttachmentPagible[i] = [...this.dataListAttachment[i]].splice(0, this.pagesize);
                break;
              }
            }
          }
          this.toastr.success(this.translateService.instant('common.attachment.delete.success'));
        });
      }
    });
  }

  validateEmail(email) {
    const regularExpression = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return regularExpression.test(String(email).toLowerCase());
  }

  public checkValidateHotline(targetKey: string, toMatchKey: string, labelMatchCode: string): ValidatorFn {
    return (group: FormGroup): { [key: string]: any } => {
      const target = group.controls[targetKey];
      const toMatch = group.controls[toMatchKey];
      if ((target.value === '' || target.value === null) && toMatch.value === '1') {
        target.setErrors({errorPhone: {errorPhone: labelMatchCode}});
        target.markAsTouched();
      }
      if (toMatch.value !== '1') {
        target.setErrors(null);
      }
      return null;
    };
  }

  public checkValidateEmail(targetKey: string, toMatchKey: string, labelMatchCode: string): ValidatorFn {
    return (group: FormGroup): { [key: string]: any } => {
      const target = group.controls[targetKey];
      const toMatch = group.controls[toMatchKey];
      if ((target.value === '' || target.value === null) && toMatch.value === '3') {
        target.setErrors({errorEmail: {errorEmail: labelMatchCode}});
        target.markAsTouched();
      }

      if (toMatch.value !== '3') {
        target.setErrors(null);
        target.markAsTouched();
      }
      return null;
    };
  }

  onPageChangeProcess(event, i) {
    const position = event.pageIndex * event.pageSize;
    this.dataListAttachmentPagible[i] = [...this.dataListAttachment[i]].splice(position, event.pageSize);
  }

  onPageChangeRequest(event, i) {
    const position = event.pageIndex * event.pageSize;
    this.dataListRequestAttachmentPagible[i] = [...this.dataListRequestAttachment[i]].splice(position, event.pageSize);
  }

  navigate(ticketId: any) {
    if (ticketId) {
      this.router.navigate(['/event-receive/', ticketId]);
      const baseUrl = window.location.href.replace(this.router.url, '');
      const url = this.router.serializeUrl(
        this.router.createUrlTree([`event-receive/${ticketId}`])
      );
      window.open(baseUrl + url, '_blank');
    }
  }

  async viewTablePhone(): Promise<void> {
    if (!this.formRequest.get('phoneNumber').value) {
      return;
    }
    const data = {
      crmId: '',
      recEmail: this.formRequest.get('email').value,
      recPhone: this.formRequest.get('phoneNumber').value,
      orgCode: '',
    };
    const res = await this.eventService.getPhoneAndEmail(data).toPromise();
    this.lstPhoneAndEmail = res.body.infos;
    if (this.lstPhoneAndEmail.length < 1) {
      this.toastr.warning('Không có dữ liệu trả về');
      return;
    }
    const dialog = this.dialog.originalOpen(TableEmailPhoneComponent, {
      width: '50vw',
      // disableClose: true,
      // hasBackdrop: true,
      data: {
        lstPhoneEmail: this.lstPhoneAndEmail,
      }
    });
    dialog.afterClosed().subscribe((next: any) => {
      console.log('next', next);
      if (next[1]?.body) {
        this.formRequest.get('cid').patchValue(next[1]?.body?.orgInfo?.orgCode);
        this.formRequest.get('taxCode').patchValue(next[1]?.body?.orgInfo?.taxCode);
        this.formRequest.get('companyName').patchValue(next[1]?.body?.orgInfo?.orgFullName);
        this.formRequest.get('address').patchValue(next[1]?.body?.orgInfo?.officeAdd);
        const feeData = next[1]?.body?.orgInfo?.feeDebtInfos;
        const certificate = next[1]?.body?.orgInfo?.caInfos;
        this.dataFeeStatus = feeData;
        this.dataCertificate = certificate;
      } else {
        this.formRequest.get('cid').setValue('');
        this.formRequest.get('taxCode').setValue('');
        this.formRequest.get('companyName').setValue('');
        this.formRequest.get('address').setValue('');
        this.dataFeeStatus = [];
        this.dataCertificate = [];
      }
      const items = next[0];
      if (items) {
        this.formRequest.get('name').patchValue(items?.recName);
        this.formRequest.get('email').patchValue(items?.recEmail);
        this.formRequest.get('contactPhone').patchValue(items?.recPhone);
      } else {
        this.formRequest.get('name').setValue('');
        this.formRequest.get('email').setValue('');
        this.formRequest.get('contactPhone').setValue('');
      }
      console.log('data status', this.dataFeeStatus);
    });
  }

  async viewTableEmail(): Promise<void> {
    if (!this.formRequest.get('email').value) {
      return;
    }
    const data = {
      crmId: '',
      recEmail: this.formRequest.get('email').value,
      recPhone: this.formRequest.get('phoneNumber').value,
      orgCode: '',
    };
    const res = await this.eventService.getPhoneAndEmail(data).toPromise();
    this.lstPhoneAndEmail = res.body.infos;
    if (this.lstPhoneAndEmail.length < 1) {
      this.toastr.warning('Không có dữ liệu trả về');
      return;
    }
    const dialog = this.dialog.originalOpen(TableEmailPhoneComponent, {
      width: '50vw',
      // disableClose: true,
      // hasBackdrop: true,
      data: {
        lstPhoneEmail: this.lstPhoneAndEmail,
      }
    });
    dialog.afterClosed().subscribe((next: any) => {
      console.log('next', next);
      if (next[1]?.body) {
        this.formRequest.get('cid').patchValue(next[1]?.body?.orgInfo?.orgCode);
        this.formRequest.get('taxCode').patchValue(next[1]?.body?.orgInfo?.taxCode);
        this.formRequest.get('companyName').patchValue(next[1]?.body?.orgInfo?.orgFullName);
        this.formRequest.get('address').patchValue(next[1]?.body?.orgInfo?.officeAdd);
        const feeData = next[1]?.body?.orgInfo?.feeDebtInfos;
        const certificate = next[1]?.body?.orgInfo?.caInfos;
        this.dataFeeStatus = feeData;
        this.dataCertificate = certificate;
      } else {
        this.formRequest.get('cid').setValue('');
        this.formRequest.get('taxCode').setValue('');
        this.formRequest.get('companyName').setValue('');
        this.formRequest.get('address').setValue('');
        this.dataFeeStatus = [];
        this.dataCertificate = [];
      }
      const items = next[0];
      if (items) {
        this.formRequest.get('name').patchValue(items?.recName);
        this.formRequest.get('email').patchValue(items?.recEmail);
        this.formRequest.get('contactPhone').patchValue(items?.recPhone);
      } else {
        this.formRequest.get('name').setValue('');
        this.formRequest.get('email').setValue('');
        this.formRequest.get('contactPhone').setValue('');
      }
      console.log('data status', this.dataFeeStatus);
    });
  }

  patchValueForm(data: any) {
    this.ticketService.getTicketById(data.id).subscribe((res: any) => {
      console.log('patch value', res);
      if (!res.listTicketRequestDTOS) {
        res.listTicketRequestDTOS = [];
      }
      const status = res.status;
      this.ticketStatus = status;
      this.ticketPermission = res.disableCloseSave;
      res.confirmDeadline = this.datePipe.transform(res.confirmDeadline, 'dd/MM/yyyy HH:mm');
      this.formRequest.patchValue(res);
      this.formRequest.patchValue({fcr: (res.fcr === '1')});
      const statusInput = this.formRequest.get('status').value;
      const a = this.inputStatusTicket.nativeElement;
      if (statusInput === '1') {
        a.value = this.translateService.instant('noprocess');
      } else if (statusInput === '2') {
        a.value = this.translateService.instant('processing');
      } else if (statusInput === '3') {
        a.value = this.translateService.instant('processed');
      } else if (statusInput === '4') {
        a.value = this.translateService.instant('confirming');
      } else if (statusInput === '5') {
        a.value = this.translateService.instant('done');
      }
      this.customerService.getCustomerById(res.customerId).subscribe(dataCust => {
        this.formRequest.patchValue(dataCust);
      });

      this.ticketModel.ticketId = data.id;
      this.processTicketDTO.ticketId = data.id;
      this.processTicketDTO.departmentId = res.departmentId;
      this.confirmTicketDTO.ticketId = data.id;
      this.confirmTicketDTO.departmentId = res.departmentId;
      this.confirmTicketDTO.confirmDeadline = res.confirmDeadline;
      this.dataGetHistory = res.confirmTicketDTOS.filter(item => item.content!=='');
      this.ticketRequestService.getTicketRequestByTicketId(this.ticketModel).subscribe(resTicketRq => {
        for (let i = 0; i < resTicketRq.length; i++) {
          console.log('resTicketRq >>> ', resTicketRq);
          this.checkStatusRequest[i] = resTicketRq[i].status;
          this.checkPermission[i] = resTicketRq[i].disableCloseSave;
          const dataFormControl: any = this.formRequest.controls.listTicketRequestDTOS;
          if (!dataFormControl.get('' + i)) {
            dataFormControl.push(this.createTab());
          }
          if (resTicketRq[i].deadline) {
            resTicketRq[i].deadline = moment(resTicketRq[i].deadline).format('YYYY-MM-DDTHH:mm');
            dataFormControl.controls[i].patchValue({
              deadlineHandling: moment(resTicketRq[i].deadline).format('YYYY-MM-DDTHH:mm'),
              // createUser: resTicketRq[i].createUserName
            });
          }
          if (resTicketRq[i].confirmDate) {
            resTicketRq[i].confirmDate = moment(resTicketRq[i].confirmDate).format('YYYY-MM-DDTHH:mm');
          }
          if (resTicketRq[i].createDatetime) {
            resTicketRq[i].createDatetime = moment(resTicketRq[i].createDatetime).format('YYYY-MM-DDTHH:mm');
          }
          dataFormControl.patchValue(resTicketRq);
          const requestStatus = dataFormControl.controls[i].get('status').value;
          const requestCode = dataFormControl.controls[i].get('ticketRequestCode').value;
          if (requestCode === localStorage.getItem('ticketRequestCode')) {
            this.selectTab = i + 1;
          }
          if (dataFormControl.controls[i].get('ticketRequestCode').value) {
            this.checkDisable = false;
          }
          const rqStatus = this.inputTicketRequest.nativeElement;
          if (requestStatus === '1') {
            rqStatus.value = this.translateService.instant('noprocess');
          } else if (requestStatus === '2') {
            rqStatus.value = this.translateService.instant('processing');
          } else if (requestStatus === '3') {
            rqStatus.value = this.translateService.instant('processed');
          } else if (requestStatus === '4') {
            rqStatus.value = this.translateService.instant('confirming');
          } else if (requestStatus === '5') {
            rqStatus.value = this.translateService.instant('done');
          }
          this.processTicketDTO.ticketRequestId = resTicketRq[i].ticketRequestId;
          this.processTicketService.getListHistoryProcessTickets(this.processTicketDTO).subscribe(hisProcess => {
            console.log('phong ban', hisProcess);
            this.dataHistoryProcessTicket[i] = hisProcess.filter(item => item.content !== '');
          });
          this.processTicketService.getTicketProcessAttachment(resTicketRq[i].ticketRequestId).subscribe(processFile => {
            this.dataListAttachment[i] = processFile;
            this.dataListAttachmentPagible[i] = processFile.splice(0, this.pagesize);
          });
          // this.getListHistoryProcessTicket(this.processTicketDTO);
          this.processTicketService.getListFileTicketProcessAttachment(this.processTicketDTO).subscribe(resp => {
            this.dataListAttachment[i] = resp;
            this.dataListAttachmentPagible[i] = resp.splice(0, this.pagesize);
            if (typeof resp !== 'undefined') {
              this.totalAttachmentProcess = resp.length;
            }
          });
          this.ticketRequestAttachmentService.getTicketRequestAttachmentById(resTicketRq[i].ticketRequestId).subscribe(resFile => {
            this.dataListRequestAttachment[i] = resFile;
            this.totalAttachmentRequest = resFile.length;
            this.dataListRequestAttachmentPagible[i] = resFile.splice(0, this.pagesize);
          });
        }
        this.getHistorySupport();
      });
      if (res.status === '5') {
        this.checkAddRequest = true;
      } else {
        this.checkAddRequest = false;
      }
    });
  }

  navigateTo(ticketId: any) {
    if (ticketId) {
      const baseUrl = window.location.href.replace(this.router.url, '');
      const url = this.router.serializeUrl(
        this.router.createUrlTree([`event-receive/${ticketId}`], {queryParams: { viewOnly: 'true'}})
      );
      window.open(baseUrl + url, '_blank');
    }
  }

}
