import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventReceiveComponent } from './event-receive.component';

describe('EventReceiveComponent', () => {
  let component: EventReceiveComponent;
  let fixture: ComponentFixture<EventReceiveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventReceiveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventReceiveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
