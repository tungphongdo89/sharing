import {Component, Inject, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {MtxGridColumn} from '@ng-matero/extensions';
import {EventService} from '@core/services/envent-handling/event-service';
import {PageEvent} from '@angular/material/paginator';

@Component({
  selector: 'app-table-email-phone',
  templateUrl: './table-email-phone.component.html',
  styleUrls: ['./table-email-phone.component.scss']
})
export class TableEmailPhoneComponent implements OnInit {
  @ViewChild('index', {static: true}) index: TemplateRef<any>;
  @ViewChild('orgCode', {static: true}) orgCode: TemplateRef<any>;
  phoneEmail: MtxGridColumn[] = [];
  pagesize = 10;
  pageSizeList = [10, 50, 100];
  offset = 0;
  total = 0;
  pageIndex: any;
  rowSelects: any[] = [];

  constructor(private translate: TranslateService,
              public dialogRef: MatDialogRef<TableEmailPhoneComponent>,
              @Inject(MAT_DIALOG_DATA) public data,
              private ticketService: EventService,
  ) {
  }

  ngOnInit(): void {
    this.pageIndex = 0;
    this.pagesize = 10;
    console.log('input', this.data.lstPhoneEmail);
    this.phoneEmail = [
      {i18n: 'campaign_email_resource.index', field: 'no', width: '15px', cellTemplate: this.index},
      {i18n: 'orgCode', field: 'orgCode'},
      {i18n: 'recEmail', field: 'recEmail'},
      {i18n: 'recName', field: 'recName',},
      {i18n: 'recPhone', field: 'recPhone'},
    ];
  }

  onPageChange(event: PageEvent) {
    console.log('event', event);
    this.offset = event.pageIndex;
    this.pagesize = event.pageSize;
  }

  closeData(row) {
    const data = {
      orgCode: row.orgCode,
    };
    this.ticketService.getPhoneAndEmailDetail(data).subscribe(res => {
      console.log('data', res);
      const next = [row, res];
      this.dialogRef.close(next);
    });
  }
}
