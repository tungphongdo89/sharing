import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TableEmailPhoneComponent } from './table-email-phone.component';

describe('TableEmailPhoneComponent', () => {
  let component: TableEmailPhoneComponent;
  let fixture: ComponentFixture<TableEmailPhoneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TableEmailPhoneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableEmailPhoneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
