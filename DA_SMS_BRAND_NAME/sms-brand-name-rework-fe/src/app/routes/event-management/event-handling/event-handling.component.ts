import {Component, Input, OnInit} from '@angular/core';
import {ProcessTicketService} from '@core/services/envent-handling/process-ticket-service';
import {ProcessTicketModel} from '@core/models/process-ticket-model';
import {EventModel} from '@core/models/event-model';
import {ToastrService} from 'ngx-toastr';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-event-handling',
  templateUrl: './event-handling.component.html',
  styleUrls: ['./event-handling.component.scss']
})
export class EventHandlingComponent implements OnInit {
  processTicketDTO: ProcessTicketModel = new ProcessTicketModel();
  @Input() dataHistoryProcessTicket: ProcessTicketModel[] = [];
  @Input() data: EventModel = new EventModel();
  constructor(private processTicketService: ProcessTicketService,
              protected toastr: ToastrService,
              protected translateService: TranslateService) { }

  ngOnInit(): void {
    this.getListHistoryProcessTicket();
  }
  createTicket(){
    const ticketId = Number(localStorage.getItem('ticketId'));
    const ticketRequestId = Number(localStorage.getItem('ticketRequestId'));
    const departmentId = Number(localStorage.getItem('departmentId'));
    this.processTicketDTO.ticketId = ticketId;
    this.processTicketDTO.ticketRequestId = ticketRequestId;
    this.processTicketDTO.departmentId = departmentId;
    this.processTicketDTO.listProcessTicketAttachmentDTOS = [];
    this.processTicketDTO.status = '2';
    // this.processTicketDTO.createUser = '1';
    this.processTicketService.createProcessTicket(this.processTicketDTO).subscribe(res => {
     if('SUCCESS' === res.status){
       this.showNotifySuccess();
       this.getListHistoryProcessTicket();
       localStorage.clear();
     }
     else {
       this.showUnknownErr();
     }
    });
  }
  getListHistoryProcessTicket(){
    // this.processTicketService.getListHistoryProcessTickets().subscribe(res => {
    //   this.dataHistoryProcessTicket = res;
    // });
  }
  showNotifySuccess() {
    this.toastr.success(this.translateService.instant('common.notify.save.success2'));
  }
  showUnknownErr() {
    this.toastr.error(this.translateService.instant('common.notify.fail'));
  }
}
