import {NgModule} from '@angular/core';
import {SharedModule} from '@shared/shared.module';
import {RoutesRoutingModule} from './routes-routing.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {DashboardComponent} from './dashboard/dashboard.component';
import {AccountSearchComponent} from '@app/routes/cp-management/account-search/account-search.component';
import {CpBrandManagementComponent} from '@app/routes/cp-management/cp-brand-management/cp-brand-management.component';
import {CpBrandManagementSearchComponent} from '@app/routes/cp-management/cp-brand-management/cp-brand-management-search/cp-brand-management-search.component';
import {EventManagementModule} from '@app/routes/event-management/event-management.module';
import {DirectoryManagementModule} from '@app/routes/directory-management/directory-management.module';
import {ConfigurationModule} from '@app/routes/configuration-authorization/authorization-information/configuration.module';
import { ReportComplaintDetailComponent } from './report-complaint-detail/report-complaint-detail.component';
import {InlineMessageComponent} from '@app/routes/inline-message/inline-message.component';
import { Ng2SearchPipeModule } from 'ng2-search-filter';

const COMPONENTS = [DashboardComponent,AccountSearchComponent,CpBrandManagementComponent,CpBrandManagementSearchComponent];
const COMPONENTS_DYNAMIC = [];

@NgModule({
  imports: [SharedModule, RoutesRoutingModule, FormsModule, ReactiveFormsModule, EventManagementModule, DirectoryManagementModule, ConfigurationModule,Ng2SearchPipeModule],
  declarations: [...COMPONENTS, ...COMPONENTS_DYNAMIC, ReportComplaintDetailComponent, InlineMessageComponent],
  entryComponents: COMPONENTS_DYNAMIC,
})
export class RoutesModule {
}
