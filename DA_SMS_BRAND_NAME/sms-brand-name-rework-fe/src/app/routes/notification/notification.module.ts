import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NotificationComponent } from './notification/notification.component';
import {SharedModule} from '@shared';



@NgModule({
  declarations: [NotificationComponent],
  exports: [
    NotificationComponent
  ],
  imports: [
    CommonModule,
    SharedModule
  ]
})
export class NotificationModule { }
