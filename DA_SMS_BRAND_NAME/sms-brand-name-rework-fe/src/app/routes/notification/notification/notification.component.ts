import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {NotificationService} from '@core/services/notification/notification.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-notification-custom',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.scss']
})
export class NotificationComponent implements OnInit {
  messages: any = [];
  total: number = 0;

  constructor(private notificationService: NotificationService, private cdk: ChangeDetectorRef, private router: Router) {
    this.search();
  }

  ngOnInit(): void {

  }

  search() {
    this.notificationService.query().subscribe(res => {
      this.messages = res.body;
      this.total = Number(res.headers.get('X-Total-Count'));
      this.cdk.detectChanges();
      console.log(this.total);
    });
  }

  navigate(message: any) {
    if (message.objectCode === 'TICKET') {
      localStorage.setItem('ticketRequestCode', message.ticketRequestCode);
      const data = JSON.parse(JSON.stringify(message));
      data.isRead = 1;
      this.notificationService.update(message.id, data).subscribe(res => {
        this.router.navigate(['/event-receive/', message.refId]);
        this.search();
      });
    }
  }
}
