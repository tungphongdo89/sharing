import {Component, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {MtxGridColumn} from '@ng-matero/extensions';
import {FormBuilder, FormGroup, NgForm, Validators} from '@angular/forms';
import {ToastrService} from 'ngx-toastr';
import {TranslateService} from '@ngx-translate/core';
import {MatDialog} from '@angular/material/dialog';
import {CommonService} from '@shared/common/common.service';
import {DatePipe} from '@angular/common';
import {ConfirmDialogComponent, ConfirmDialogModel} from '@shared/components/confirm-dialog/confirm-dialog.component';
import {environment} from '@env/environment';
import {CampaignSmsBlackListService} from '@core/services/campaign-sms-black-list/campaign-sms-black-list.service';
import {finalize} from 'rxjs/operators';

@Component({
  selector: 'app-campaign-sms-black-list',
  templateUrl: './campaign-sms-black-list.component.html',
  styleUrls: ['./campaign-sms-black-list.component.scss']
})
export class CampaignSmsBlackListComponent implements OnInit {

  @ViewChild('button', {static: true}) button: TemplateRef<any>;
  @ViewChild('index', {static: true}) index: TemplateRef<any>;
  @ViewChild('createTime', {static: true}) createTime: TemplateRef<any>;
  columns: MtxGridColumn[] = [
    {i18n: 'No', field: 'no', width: '5%', cellTemplate: this.index},
    {i18n: 'campaign_sms_black_list.phone_number', field: 'phoneNumber'},
    {i18n: 'email_blacklist.Full_name', field: 'fullName'},
    {i18n: 'email_blacklist.Creator', field: 'createUserName'},
    {i18n: 'email_blacklist.Date_created', field: 'time'},
    {
      i18n: 'common.action',
      field: 'options',
      width: '50px',
      cellTemplate: this.button,
    }
  ];
  searchForm: FormGroup = this.fb.group({
    campaignSMSId: [null, Validators.required],
    phoneNumber: [null, [Validators.required,
      Validators.pattern('^(84|0[3|5|7|8|9])([0-9]{8,14})$')]],
    fullName: [null]
  });
  pageSizeList = [10, 50, 100];
  pageSize = 10;
  totalPage = 0;
  page: number = 0;
  isLoading = true;
  file: File;
  lstCampaignSMSBlackList: any = [];
  lstCampaignSMSMarketing: any = [];
  importResult: any;
  isImportFalse: boolean = false;
  @ViewChild('form') form: NgForm;

  constructor(private fb: FormBuilder,
              private campaignSmsBlackListService: CampaignSmsBlackListService,
              private toastrService: ToastrService,
              private translate: TranslateService,
              private dialog: MatDialog,
              private commonService: CommonService,
              private datePipe: DatePipe) {
  }

  ngOnInit(): void {
    this.init();
  }

  init() {
    this.campaignSmsBlackListService.findAllCampaignSMSMarketing().subscribe(res => {
      this.lstCampaignSMSMarketing = res;
      this.searchForm.get('campaignSMSId').setValue(null);
      this.search();
    });

  }

  search(reset?: boolean) {
    if (reset) {
      this.page = 0;
      this.pageSize = 10;
    }
    this.form.resetForm(this.form.value);
    const data: any = this.searchForm.value;
    data.size = this.pageSize;
    data.page = this.page;
    this.campaignSmsBlackListService.search(data)
      .pipe(finalize(() => this.isLoading = false))
      .subscribe(res => {
        this.lstCampaignSMSBlackList = res.body;
        this.totalPage = res.headers.get('X-Total-Count');
      }, error => this.toastrService.error('', error));
  }

  onPageChange(event) {
    this.page = event.pageIndex;
    this.pageSize = event.pageSize;
    this.search();
  }

  create() {
    if (this.searchForm.invalid) {
      return;
    }
    this.campaignSmsBlackListService.create(this.searchForm.value).subscribe(res => {
        this.toastrService.success('', this.translate.instant('successfully_added_new'));
        this.form.resetForm();
        this.search(true);
      },
      error => {
        this.toastrService.error('', this.translate.instant('phone_number_exists'));
      });
  }

  delete(row) {
    const title = this.translate.instant('common.confirm.title.delete');
    const content = this.translate.instant('common.MSG0017');
    const dialogContent: ConfirmDialogModel = new ConfirmDialogModel(title, content);
    this.dialog.open(ConfirmDialogComponent, {
      width: '25vw',
      disableClose: true,
      hasBackdrop: true,
      data: dialogContent
    }).afterClosed().subscribe(result => {
      if (result.data === 'confirm') {
        this.campaignSmsBlackListService.delete(row.id).subscribe(res => {
            this.toastrService.success('', this.translate.instant('delete_successfully'));
            this.search();
          }, error => this.toastrService.error('', error)
        );
      }
    });
  }


  export() {
    this.campaignSmsBlackListService.export(this.searchForm.value,
      'Danh sách đen gửi sms' + this.datePipe.transform(new Date(), 'ddMMyyyyHHmm'));
  }

  changeFileUpload(event) {
    this.file = event.target.files[0];
  }

  uploadFile() {
    if (!this.file) {
      this.toastrService.warning('', this.translate.instant('select_file_upload'));
      return;
    }
    this.campaignSmsBlackListService.upLoad(this.file).subscribe(res => {
      if (res.status === 202) {
        this.toastrService.success(this.translate.instant('common.notify.import.success'));
        this.isImportFalse = false;
        this.importResult = res.body;
        this.searchForm.get('campaignSMSId').setValue(null);
        this.search(true);
      }
    }, error => {
      this.toastrService.error(error.error.errorCodeConfig);
    });
  }

  downloadTemplate() {
    this.commonService.downloadFile(
      environment.serverUrl.api + '/campaign-sms-black-list/getSampleFile',
      null, null, ('Danh_sach_den_gui_sms_' + this.datePipe.transform(new Date(), 'yyyyMMddHHmm')));
  }

}
