import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {CampaignSmsBlackListComponent} from '@app/routes/campaign-sms-black-list/campaign-sms-black-list/campaign-sms-black-list.component';

const routes: Routes = [
  {
    path: '',
    component: CampaignSmsBlackListComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CampaignSmsBlackListRoutingModule {
}
