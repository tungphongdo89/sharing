import {NgModule} from '@angular/core';
import {CommonModule, DatePipe} from '@angular/common';
import {SharedModule} from '@shared';
import {CampaignSmsBlackListRoutingModule} from '@app/routes/campaign-sms-black-list/campaign-sms-black-list-routing.module';
import {CampaignSmsBlackListComponent} from '@app/routes/campaign-sms-black-list/campaign-sms-black-list/campaign-sms-black-list.component';


@NgModule({
  declarations: [CampaignSmsBlackListComponent],
  imports: [
    CommonModule,
    SharedModule,
    CampaignSmsBlackListRoutingModule
  ],
  providers: [
    DatePipe
  ]
})
export class CampaignSmsBlackListModule {
}
