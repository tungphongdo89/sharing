import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CriteriaRatingComponent } from './criteria-rating.component';

describe('CriteriaRatingComponent', () => {
  let component: CriteriaRatingComponent;
  let fixture: ComponentFixture<CriteriaRatingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CriteriaRatingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CriteriaRatingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
