import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {MtxDialog, MtxGridColumn} from '@ng-matero/extensions';
import {CriteriaGroupService} from '@core/services/criteria-group/criteria-group.service';
import {ToastrService} from 'ngx-toastr';
import {MatDialog} from '@angular/material/dialog';
import {TranslateService} from '@ngx-translate/core';
import {CriteriaRatingService} from '@core/services/criteria-rating/criteria-rating.service';
import {CriteriaRatingModel} from '@core/models/criteria-rating-model';
import {PopupDeleteComponent} from '@app/routes/directory-management/popup-delete/popup-delete.component';
import {BaseComponent} from '@shared/components/base-component/base-component.component';
import {RESOURCE} from '@core';
import {ActivatedRoute} from '@angular/router';
import {ConfirmDialogComponent, ConfirmDialogModel} from '@shared/components/confirm-dialog/confirm-dialog.component';
import {Constant} from '@shared/Constant';

@Component({
  selector: 'app-criteria-rating',
  templateUrl: './criteria-rating.component.html',
  styleUrls: ['./criteria-rating.component.scss']
})
export class CriteriaRatingComponent extends BaseComponent implements OnInit {

  formSearch: FormGroup;
  criteriaRatingForm: FormGroup;
  data: any = [];
  searchText: any = '';
  columns: MtxGridColumn[] = [];
  pageSizeList = [10, 50, 100];
  isLoading = false;
  offset: any = 0;
  pagesize = 10;
  total: any = 0;
  buttonTitle: string;
  buttonTitle2: string;
  filterInput = new FormControl(null);
  backUpDatasource = [];

  constructor(
    private fb: FormBuilder,
    public route: ActivatedRoute,
    private criteriaRatingService: CriteriaRatingService,
    private toast: ToastrService,
    public dialog2: MtxDialog,
    protected translateService: TranslateService
  ) {
    super(route, criteriaRatingService, RESOURCE.CUSTOMER, toast, translateService, dialog2);
  }

  ngOnInit(): void {
    this.getAllCriteriaRating(true);
    this.criteriaRatingForm = this.fb.group({
      id: new FormControl(null),
      name: new FormControl(null, [Validators.required]),
      fromScores: new FormControl(null, [Validators.required, Validators.min(0), Validators.max(9999)]),
      toScores: new FormControl(null, [Validators.required, Validators.min(0), Validators.max(9999)]),
      createDatetime: new FormControl(null),
      createUser: new FormControl(null),
      updateDatetime: new FormControl(null),
      updateUser: new FormControl(null),
    });
    this.columns = [
      {i18n: 'criteria_rating.no', field: 'index', width: '70px',},
      {i18n: 'criteria_rating.name_rating', field: 'name'},
      {i18n: 'criteria_rating.to_scores', field: 'toScores'},
      {i18n: 'criteria_rating.form_scores', field: 'fromScores'},
      {
        i18n: 'common.action',
        field: 'options',
        width: '120px',
        pinned: 'right',
        right: '0px',
        type: 'button',
        buttons: [
          {
            type: 'icon',
            text: 'edit',
            icon: 'edit',
            click: record => this.updateCriteriaRating(record),
          },
          {
            type: 'icon',
            text: 'delete',
            icon: 'delete',
            click: record => this.deleteCriteriaRating(record),
          }
        ]
      }
    ];
    this.checkButton();
    this.onFilterTable();
  }

  checkButton() {
    console.log('id: ', this.criteriaRatingForm.get('id').value);
    if (this.criteriaRatingForm.get('id').value !== null) {
      this.buttonTitle = this.translateService.instant('criteria_rating.btn_update');
      this.buttonTitle2 = this.translateService.instant('criteria_rating.btn_back');
    } else {
      this.buttonTitle = this.translateService.instant('criteria_rating.btn_create');
      this.buttonTitle2 = this.translateService.instant('criteria_rating.btn_back');
    }
    console.log('buton: ', this.buttonTitle);
  }

  updateCriteriaRating(criteriaRatingModel: CriteriaRatingModel) {
    this.criteriaRatingForm.patchValue(criteriaRatingModel);
    this.checkButton();
  }

  getAllCriteriaRating(reset?: boolean) {
    if (reset) {
      this.offset = 0;
      this.pagesize = 10;
    }
    // const data = {...this.criteriaGroupForm.value, queryType: 1};
    this.criteriaRatingService.getAllCriteriaRating({size: this.pagesize, page: this.offset}).subscribe(res => {
      console.log('áđâsd ', res);
      this.dataModel.dataSource = res.body;
      this.backUpDatasource = res.body;
      this.data = res.body;
      console.log(res.headers.get('X-Total-Count'));
      this.total = Number(res.headers.get('X-Total-Count'));
    });
  }

  btnBack() {
    this.criteriaRatingForm.reset();
    this.checkButton();
  }

  deleteCriteriaRating(criteriaRatingModel: CriteriaRatingModel){
    this.criteriaRatingForm.patchValue(criteriaRatingModel);
    const title = this.translateService.instant('common.confirm.title.delete');
    const content = this.translateService.instant('common.MSG0017');
    const dialogContent: ConfirmDialogModel = new ConfirmDialogModel(title, content);
    this.dialog.originalOpen(ConfirmDialogComponent, {
      width: '25vw',
      disableClose: true,
      hasBackdrop: true,
      data: dialogContent
    }).afterClosed().subscribe((next: any) => {
      if (next.data === Constant.DIALOG_CONFIRM) {
        this.criteriaRatingService.deleteCriteriaRating(this.criteriaRatingForm.value).subscribe(rs => {
          if (rs.body.message === 'ok') {
            this.toast.success(this.translateService.instant('criteria_rating.msg_delete_success'));
            this.criteriaRatingForm.reset();
            this.getAllCriteriaRating(false);
          } else {
            this.toast.error(this.translateService.instant('criteria_rating.msg_delete_error'));
          }
        });
      }else {
        this.criteriaRatingForm.reset();
      }
    });
  }

  addCriteriaRating(){
    if (this.criteriaRatingForm.get('id').value){
      this.criteriaRatingService.updateCriteriaRating(this.criteriaRatingForm.value).subscribe(res => {
        console.log('exxx ', res);
        if (res.body.message === 'ok'){
          this.toast.success(this.translateService.instant('criteria_rating.msg_update_success'));
          this.getAllCriteriaRating(true);
          this.criteriaRatingForm.reset();
          this.checkButton();
        } else {
          if(res.body.message === 'exist'){
            this.toast.error(this.translateService.instant('criteria_rating.msg_exist'));
          } else {
            this.toast.error(this.translateService.instant('criteria_rating.msg_exist_scores'));
          }
        }
      });
    } else {
      this.criteriaRatingService.addCriteriaRating(this.criteriaRatingForm.value).subscribe(res => {
        if (res.body.message === 'ok'){
          this.toast.success(this.translateService.instant('criteria_rating.msg_create_success'));
          this.getAllCriteriaRating(true);
          this.criteriaRatingForm.reset();
          this.checkButton();
        } else {
          if(res.body.message === 'exist'){
            this.toast.error(this.translateService.instant('criteria_rating.msg_exist'));
          } else {
            this.toast.error(this.translateService.instant('criteria_rating.msg_exist_scores'));
          }
        }
      });
    }
  }

  onFilterTable() {
    this.filterInput.valueChanges.subscribe(next => {
      this.dataModel.dataSource = this.backUpDatasource.filter(value =>
        value.name?.toLowerCase().includes(next?.toLowerCase())
        || value.fromScores?.toString().toLowerCase().includes(next?.toLowerCase())
        || value.toScores?.toString().toLowerCase().includes(next?.toLowerCase())
      );
      console.log('data model ', this.dataModel);
    });
  }

  onPageChange(event) {
    this.offset = event.pageIndex;
    this.pagesize = event.pageSize;
    this.getAllCriteriaRating();
  }

}
