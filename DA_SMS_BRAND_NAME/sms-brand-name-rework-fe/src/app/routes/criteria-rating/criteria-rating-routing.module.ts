import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {CriteriaRatingComponent} from '@app/routes/criteria-rating/criteria-rating/criteria-rating.component';


const routes: Routes = [
  {
    path: '',
    component: CriteriaRatingComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CriteriaRatingRoutingModule { }
