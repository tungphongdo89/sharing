import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CriteriaRatingRoutingModule } from './criteria-rating-routing.module';
import { CriteriaRatingComponent } from './criteria-rating/criteria-rating.component';
import {SharedModule} from '@shared';
import {Ng2SearchPipeModule} from 'ng2-search-filter';


@NgModule({
  declarations: [CriteriaRatingComponent],
  imports: [
    CommonModule,
    SharedModule,
    CriteriaRatingRoutingModule,
    Ng2SearchPipeModule
  ]
})
export class CriteriaRatingModule { }
