import { NgModule } from '@angular/core';
import { CampaignEmailMarketingConfigComponent } from '@app/routes/campaign-email-marketing/campaign-email-marketing-config/campaign-email-marketing-config.component';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@shared';
import { CampaignEmailMarketingRouting } from '@app/routes/campaign-email-marketing/campaign-email-marketing.routing';
import { CampaignEmailMarketingDialogComponent } from '@app/routes/campaign-email-marketing/campaign-email-marketing-dialog/campaign-email-marketing-dialog.component';

@NgModule({
  declarations: [CampaignEmailMarketingConfigComponent, CampaignEmailMarketingDialogComponent],
  imports: [
    CommonModule,
    SharedModule,
    CampaignEmailMarketingRouting
  ]
})
export class CampaignEmailMarketingModule {}
