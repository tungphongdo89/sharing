import { RouterModule, Routes } from '@angular/router';
import { CampaignEmailMarketingConfigComponent } from '@app/routes/campaign-email-marketing/campaign-email-marketing-config/campaign-email-marketing-config.component';
import { NgModule } from '@angular/core';
import { CampaignEmailMarketingDialogComponent } from '@app/routes/campaign-email-marketing/campaign-email-marketing-dialog/campaign-email-marketing-dialog.component';

const routes: Routes = [
  {
    path: '',
    component: CampaignEmailMarketingConfigComponent
  },
  {
    path: 'new',
    component: CampaignEmailMarketingDialogComponent
  },
  {
    path: 'edit/:id',
    component: CampaignEmailMarketingDialogComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CampaignEmailMarketingRouting {}
