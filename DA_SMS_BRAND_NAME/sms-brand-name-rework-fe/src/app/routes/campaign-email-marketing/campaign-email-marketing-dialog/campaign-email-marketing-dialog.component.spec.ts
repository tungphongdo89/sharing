import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CampaignEmailMarketingDialogComponent } from './campaign-email-marketing-dialog.component';

describe('CampaignEmailMarketingDialogComponent', () => {
  let component: CampaignEmailMarketingDialogComponent;
  let fixture: ComponentFixture<CampaignEmailMarketingDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CampaignEmailMarketingDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CampaignEmailMarketingDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
