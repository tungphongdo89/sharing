import { Component, OnInit } from '@angular/core';
import { HtmlEditorService, ImageService, LinkService, ToolbarService } from '@syncfusion/ej2-angular-richtexteditor';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';
import { MtxDialog } from '@ng-matero/extensions';
import { CampaignEmailMarketingService } from '@core/services/campaign-email-marketing/campaign-email-marketing.service';
import { ActivatedRoute, Router } from '@angular/router';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-campaign-email-marketing-dialog',
  templateUrl: './campaign-email-marketing-dialog.component.html',
  styleUrls: ['./campaign-email-marketing-dialog.component.scss'],
  providers: [ToolbarService, LinkService, ImageService, HtmlEditorService],
})
export class CampaignEmailMarketingDialogComponent implements OnInit {
  public tools: object = {
    items: ['Undo', 'Redo', '|',
      'Bold', 'Italic', 'Underline', 'StrikeThrough', '|',
      'FontName', 'FontSize', 'FontColor', 'BackgroundColor', '|',
      'SubScript', 'SuperScript', '|',
      'LowerCase', 'UpperCase', '|',
      'Formats', 'Alignments', '|', 'OrderedList', 'UnorderedList', '|',
      'Indent', 'Outdent', '|', 'CreateLink',
      'Image', '|', 'ClearFormat', 'Print', 'SourceCode', '|', 'FullScreen'],
  };
  currentYear = new Date().getFullYear();
  currentMonth = new Date().getMonth();
  currentDay = new Date().getDate();
  minStartDate: Date = new Date(this.currentYear, this.currentMonth, this.currentDay + 1);
  minEndDate: Date = new Date(this.currentYear, this.currentMonth, this.currentDay + 1);
  maxStartDate = null;
  isUpdate: boolean = false;
  id: any = null;
  formControl: FormGroup;
  checkStartDate: boolean = true;
  checkEndDate: boolean = true;
  button: string;
  title: string;
  REGEX_DATE = /^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$/;

  constructor(
    private fb: FormBuilder,
    private toastr: ToastrService,
    private translate: TranslateService,
    private service: CampaignEmailMarketingService,
    private router: Router,
    private route: ActivatedRoute,
  ) {
  }

  ngOnInit(): void {
    this.formControl = this.fb.group({
      name: [null, [Validators.required, Validators.maxLength(200)]],
      startDate: [null, [Validators.required]],
      endDate: [null, [Validators.required]],
      title: [null, [Validators.required, Validators.maxLength(500)]],
      content: [null, [Validators.required]],
      startDateView: null,
      endDateView: null,
    });

    if (this.route.snapshot.routeConfig.path === 'edit/:id') {
      this.isUpdate = true;
    }
    if (this.isUpdate) {
      this.button = this.translate.instant('campaign-email-marketing.update');
      this.title = this.translate.instant('campaign-email-marketing.update-title');
      this.id = this.route.snapshot.params.id;
      this.service.getOne(this.id).subscribe(res => {
        this.formControl.patchValue(res.body);
        this.formControl.markAllAsTouched();
        const sd = this.formControl.get('startDateView').value.split('/');
        const startDate = new Date(sd[2], sd[1] - 1, sd[0]);
        this.minStartDate = null;
        // if (startDate <= this.minStartDate && !this.formControl.get('startDate').hasError('required')) {
        //   this.checkStartDate = false;
        // }
        this.minEndDate = startDate;
      });
    } else {
      this.button = this.translate.instant('campaign-email-marketing.save');
      this.title = this.translate.instant('campaign-email-marketing.add-title');
    }
  }

  onSave() {
    if (this.formControl.invalid) {
      this.formControl.markAllAsTouched();
      return;
    }
    const data = this.formControl.getRawValue();
    if (this.isUpdate) {
      data.id = this.id;
    }
    this.service.save(data).subscribe(res => {
      if (res) {
        if (!this.isUpdate) {
          this.toastr.success(this.translate.instant('campaign-email-marketing.create-success'));
        } else {
          this.toastr.success(this.translate.instant('campaign-email-marketing.update-success'));
        }
        this.close();
      }
    }, error => {
      this.toastr.error(error.error.title);
    });
  }

  setDateTime(dateTime) {
    const pipe = new DatePipe('en-US');
    const date = pipe.transform(dateTime, 'dd/MM/yyyy', 'GMT+7');
    return date;
  }

  close() {
    this.router.navigate(['/campaign-email-marketing']);
  }

  onChangeStartDate() {
    this.checkStartDate = true;
    if (this.formControl.get('startDate').value) {
      this.minEndDate = this.formControl.get('startDate').value;
    } else {
      this.minEndDate = this.minStartDate;
    }
    if (this.isUpdate) {
      const ed = this.formControl.get('endDateView').value.split('/');
      const endDate = new Date(ed[2], ed[1] - 1, ed[0]);
      if (endDate <= this.minEndDate && !this.formControl.get('endDate').hasError('required')) {
        this.checkEndDate = false;
      }
    }
  }

  onChangeEndDate() {
    if (this.formControl.get('endDate').value) {
      this.maxStartDate = this.formControl.get('endDate').value;
    } else {
      this.maxStartDate = null;
    }
  }

  validateDate(formControl, event) {
    if (!this.REGEX_DATE.test(event.target.value)) {
      this.formControl.get(formControl).setValue(null);
    }
  }

  checkUnicode(text: string) {
    const unicode: string[] = ['à', 'ả', 'ã', 'á', 'ạ', 'ă', 'ằ', 'ẳ', 'ẵ', 'ắ', 'ặ', 'â', 'ầ', 'ẩ', 'ẫ', 'ấ', 'ậ',
      'À', 'Ả', 'Ã', 'Á', 'Ạ', 'Ă', 'Ằ', 'Ẳ', 'Ẵ', 'Ắ', 'Ặ', 'Â', 'Ầ', 'Ẩ', 'Ẫ', 'Ấ', 'Ậ',
      'đ', 'Đ',
      'è', 'ẻ', 'ẽ', 'é', 'ẹ', 'ê', 'ề', 'ể', 'ễ', 'ế', 'ệ',
      'È', 'Ẻ', 'Ẽ', 'É', 'Ẹ', 'Ê', 'Ề', 'Ể', 'Ễ', 'Ế', 'Ệ',
      'ì', 'ỉ', 'ĩ', 'í', 'ị',
      'Ì', 'Ỉ', 'Ĩ', 'Í', 'Ị',
      'ò', 'ỏ', 'õ', 'ó', 'ọ', 'ô', 'ồ', 'ổ', 'ỗ', 'ố', 'ộ', 'ơ', 'ờ', 'ở', 'ỡ', 'ớ', 'ợ',
      'Ò', 'Ỏ', 'Õ', 'Ó', 'Ọ', 'Ô', 'Ồ', 'Ổ', 'Ỗ', 'Ố', 'Ộ', 'Ơ', 'Ờ', 'Ở', 'Ỡ', 'Ớ', 'Ợ',
      'ù', 'ủ', 'ũ', 'ú', 'ụ', 'ư', 'ừ', 'ử', 'ữ', 'ứ', 'ự',
      'Ù', 'Ủ', 'Ũ', 'Ú', 'Ụ', 'Ư', 'Ừ', 'Ử', 'Ữ', 'Ứ', 'Ự',
      'ỳ', 'ỷ', 'ỹ', 'ý', 'ỵ',
      'Ỳ', 'Ỷ', 'Ỹ', 'Ý', 'Ỵ'];
    if (text) {
      for (const a of text.split('')) {
        if (unicode.includes(a)) {
          return true;
        }
      }
    }
    return false;
  }

  onInputName() {
    if (this.checkUnicode(this.formControl.get('name').value)) {
      this.formControl.get('name').setValidators([Validators.required, Validators.maxLength(94)]);
      this.formControl.updateValueAndValidity();
    } else {
      this.formControl.get('name').setValidators([Validators.required, Validators.maxLength(200)]);
      this.formControl.updateValueAndValidity();
    }
  }

  onInputTitle() {
    if (this.checkUnicode(this.formControl.get('title').value)) {
      this.formControl.get('title').setValidators([Validators.required, Validators.maxLength(235)]);
      this.formControl.updateValueAndValidity();
    } else {
      this.formControl.get('title').setValidators([Validators.required, Validators.maxLength(500)]);
      this.formControl.updateValueAndValidity();
    }
  }

}
