import { Component, OnInit } from '@angular/core';
import { MtxDialog, MtxGridColumn } from '@ng-matero/extensions';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';
import { CampaignEmailMarketingService } from '@core/services/campaign-email-marketing/campaign-email-marketing.service';
import * as fileSaver from 'file-saver';
import { HtmlEditorService, ImageService, LinkService, ToolbarService } from '@syncfusion/ej2-angular-richtexteditor';
import { Router } from '@angular/router';
import { DatePipe } from '@angular/common';
import { finalize } from 'rxjs/operators';
import { ConfirmDialogComponent, ConfirmDialogModel } from '@shared/components/confirm-dialog/confirm-dialog.component';
import { Constant } from '@shared/Constant';

@Component({
  selector: 'app-campaign-email-marketing-config',
  templateUrl: './campaign-email-marketing-config.component.html',
  styleUrls: ['./campaign-email-marketing-config.component.scss'],
  providers: [ToolbarService, LinkService, ImageService, HtmlEditorService],
})
export class CampaignEmailMarketingConfigComponent implements OnInit {
  columns: MtxGridColumn[] = [];
  pageIndex: any;
  pageSize: any;
  dataSearch = {
    page: null,
    size: null,
  };
  formSearch: FormGroup;
  formImport: FormGroup;
  totalRecord: any;
  lst: any;
  lstCampaignEmailName: any;
  lstCampaignEmailNameSearch: any;
  pageSizeList = [10, 50, 100];
  isLoading = false;
  file: any = null;
  distinct = [
    {
      value: 0,
      label: this.translate.instant('campaign-email-marketing.no-duplicate-filter'),
    },
    {
      value: 1,
      label: this.translate.instant('campaign-email-marketing.duplicate-filter-campaign'),
    },
    {
      value: 2,
      label: this.translate.instant('campaign-email-marketing.duplicate-filter-all-campaign'),
    },
  ];
  minToDate;
  maxFromDate;
  REGEX_DATE = /^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$/;

  constructor(
    private service: CampaignEmailMarketingService,
    private fb: FormBuilder,
    private toastr: ToastrService,
    private translate: TranslateService,
    private router: Router,
    private dialog: MtxDialog,
  ) {
  }

  ngOnInit(): void {
    this.getActiveCampaign();
    this.getAllCampaign();

    this.columns = [
      { i18n: 'campaign-email-marketing.columns.index', width: '50px', field: 'no' },
      { i18n: 'campaign-email-marketing.columns.campaign-name', field: 'name' },
      { i18n: 'campaign-email-marketing.columns.title', field: 'title' },
      { i18n: 'campaign-email-marketing.columns.amount-data', field: 'totalData' },
      { i18n: 'campaign-email-marketing.columns.create-user', field: 'createUserName' },
      { i18n: 'campaign-email-marketing.columns.create-date', field: 'createDateTimeView' },
      { i18n: 'common.action', field: 'options', width: '150px', pinned: 'right', type: 'button' },
    ];

    this.formSearch = this.fb.group({
      name: '',
      fromDate: null,
      toDate: null,
    });

    this.formImport = this.fb.group({
      name: ['', Validators.required],
      duplicateFilter: 0,
      file: [null, Validators.required],
    });

    this.pageIndex = 0;
    this.pageSize = 10;
    this.onSearch();
  }

  getActiveCampaign() {
    this.service.getAll().subscribe(res => {
      if (res) {
        this.lstCampaignEmailName = res.body;
      }
    });
  }

  getAllCampaign() {
    this.service.getAllSearch().subscribe(res => {
      if (res) {
        this.lstCampaignEmailNameSearch = res.body;
      }
    });
  }

  onSearch() {
    this.isLoading = true;
    this.dataSearch.page = this.pageIndex;
    this.dataSearch.size = this.pageSize;
    let fromDate;
    let toDate;
    let name;
    if (this.formSearch.get('fromDate').value) {
      fromDate = this.setDateTime(this.formSearch.get('fromDate').value);
    } else {
      fromDate = null;
    }
    if (this.formSearch.get('toDate').value) {
      toDate = this.setDateTime(this.formSearch.get('toDate').value);
    } else {
      toDate = null;
    }
    if (this.formSearch.get('name').value !== '') {
      name = this.formSearch.get('name').value;
    } else {
      name = -1;
    }
    this.service.doSearch(this.dataSearch, name, fromDate, toDate).pipe(finalize(() => {
      this.isLoading = false;
    })).subscribe(res => {
      if (res) {
        if (res.body.length === 0) {
          this.toastr.error(this.translate.instant('campaign-resource.no-record-found'));
        }
        this.lst = res.body;
        this.totalRecord = Number(res.headers.get('X-Total-Count'));
      }
    });
  }

  onPageChange(event) {
    this.pageIndex = event.pageIndex;
    this.pageSize = event.pageSize;
    this.onSearch();
  }

  search() {
    this.formSearch.markAllAsTouched();
    if (this.formSearch.invalid) {
      return;
    }
    this.pageIndex = 0;
    this.onSearch();
  }

  setDateTime(dateTime) {
    const pipe = new DatePipe('en-US');
    const date = pipe.transform(dateTime, 'dd/MM/yyyy', 'GMT+7');
    return date;
  }

  downloadTemplate() {
    const controls = this.formImport.controls;
    controls.file.setErrors(null);
    controls.name.setErrors(null);
    this.service.downloadTemplate().subscribe(res => {
      if (res) {
        const fileName = res.headers.get('filename');
        this.saveFile(res.body, fileName);
      }
    }, error => {
      this.toastr.error(error.error.title);
    });
  }

  private saveFile(data: ArrayBuffer, fileName: string) {
    const blob = new Blob([data], { type: 'application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet; charset=UTF-8' });
    fileSaver.saveAs(blob, fileName);
  }

  new() {
    this.router.navigate(['/campaign-email-marketing/new']);
  }

  edit(row: any) {
    this.router.navigate(['/campaign-email-marketing/edit/' + row.id]);
  }

  delete(row: any) {
    const title = this.translate.instant('common.confirm.title.delete');
    const content = this.translate.instant('common.MSG0017');
    const dialogContent: ConfirmDialogModel = new ConfirmDialogModel(title, content);
    this.dialog.originalOpen(ConfirmDialogComponent, {
      width: '25vw',
      disableClose: true,
      hasBackdrop: true,
      data: dialogContent,
    }).afterClosed().subscribe((next: any) => {
      if (next.data === 'confirm') {
        this.service.delete(row.id).subscribe(res => {
          if (res) {
            this.toastr.success(this.translate.instant('campaign-email-marketing.delete-success'));
            this.onSearch();
            this.getActiveCampaign();
            this.getAllCampaign();
          }
        }, error => {
          this.toastr.success(this.translate.instant('campaign-email-marketing.delete-fail'));
        });
      }
    });
  }

  doImport() {
    this.formImport.markAllAsTouched();
    if (this.formImport.invalid) {
      return;
    } else {
      if (this.formImport.get('name').value === null || this.formImport.get('name').value === '') {
        this.formImport.get('name').setErrors(Validators.required);
        this.formImport.get('name').updateValueAndValidity();
      }
      if (this.formImport.get('file').value === null) {
        this.formImport.get('file').setErrors(Validators.required);
        this.formImport.get('file').updateValueAndValidity();
      }
      if (this.formImport.invalid) {
        return;
      }
    }
    const id = this.formImport.get('name').value;
    const distinct = this.formImport.get('duplicateFilter').value;
    this.service.importFile(this.formImport.get('file').value._files[0], id, distinct).subscribe((res: any) => {
      console.log(res);
      if (res.body.status.code === Constant.OK) {
        const rs = res.body.data.split('-');
        this.toastr.success(this.translate.instant('campaign-email-marketing.import-result.total') + rs[0]
          + '<br/>' + this.translate.instant('campaign-email-marketing.import-result.success') + rs[1]
          + '<br/>' + this.translate.instant('campaign-email-marketing.import-result.error') + rs[2]
          + '<br/>' + this.translate.instant('campaign-email-marketing.import-result.blackList') + rs[3], '', {
          timeOut: 5000,
          enableHtml: true,
        });
        this.search();
      } else if (res.body.status.code === '102') {
        this.toastr.error(this.translate.instant('campaign-email-marketing.error.max-file-size'));
      }
    }, error => {
      this.toastr.error(error.error.title);
    });
  }

  doCancel() {
    const controls = this.formImport.controls;
    controls.file.reset();
    controls.file.setErrors(null);
    controls.name.reset('');
    controls.name.setErrors(null);
    controls.uplicateFilter.reset(0);
    controls.duplicateFilter.setErrors(null);
  }

  fileChange() {
    const button = document.getElementById('button');
    if (this.formImport.get('file').value === null) {
      button.style.marginTop = '-4px';
    } else if (this.formImport.get('file').value !== null || this.formImport.get('file').value) {
      button.style.marginTop = '-12px';
    }
  }

  onChangeFromDate(event) {
    if (!this.REGEX_DATE.test(event.target.value)) {
      this.formSearch.get('fromDate').setValue(null);
    }
    this.minToDate = this.formSearch.get('fromDate').value;
  }

  onChangeToDate(event) {
    if (!this.REGEX_DATE.test(event.target.value)) {
      this.formSearch.get('toDate').setValue(null);
    }
    this.maxFromDate = this.formSearch.get('toDate').value;
  }
}
