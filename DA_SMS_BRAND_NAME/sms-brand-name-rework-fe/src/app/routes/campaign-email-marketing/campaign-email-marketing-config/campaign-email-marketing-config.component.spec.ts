import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CampaignEmailMarketingConfigComponent } from './campaign-email-marketing-config.component';

describe('CampaignEmailMarketingConfigComponent', () => {
  let component: CampaignEmailMarketingConfigComponent;
  let fixture: ComponentFixture<CampaignEmailMarketingConfigComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CampaignEmailMarketingConfigComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CampaignEmailMarketingConfigComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
