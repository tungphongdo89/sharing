import {Component, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {CampaignPerformInformationService} from '@core/services/campaign-perform-information/campaign-perform-information.service';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {TranslateService} from '@ngx-translate/core';
import {ToastrService} from 'ngx-toastr';
import {DatePipe} from '@angular/common';
import {BaseComponent} from '@shared/components/base-component/base-component.component';
import {Constant} from '@shared/Constant';
import * as moment from 'moment';
import {FORMATS_DATE, FORMATS_DATETIME} from '@shared/common/format-date';
import {Time} from '@shared/components/time-picker/time-picker.component';
import {Router} from '@angular/router';

@Component({
  selector: 'app-campaign-perform-information',
  templateUrl: './campaign-perform-information.component.html',
  styleUrls: ['./campaign-perform-information.component.scss']
})
export class CampaignPerformInformationComponent extends BaseComponent implements OnInit {
  @ViewChild('index', {static: true}) index: TemplateRef<any>;
  @ViewChild('CID', {static: true}) CID: TemplateRef<any>;
  lstCampaigns = [];
  lstCampaignResource = [];
  lstAccountTDV = [];
  lstDataInformation = [];
  lstCallStatus = [];
  lstSurveyStatus = [];
  arrControls = [];
  formGroup: FormGroup = this.fb.group({});
  pageSizeList = [10, 50, 100];
  pageSize = 10;
  page: number = 0;
  filterInput = new FormControl(null);
  form: FormGroup = this.fb.group({
    campaign_id: [null, Validators.required],
    campaign_resource_id: [null],
    assign_user_id: [null],
    call_time_from: [null, Validators.required],
    call_time_to: [null, Validators.required]
  });
  backUpDatasource = [];

  constructor(private campaignPerformInformationService: CampaignPerformInformationService,
              private fb: FormBuilder,
              protected translateService: TranslateService,
              private toast: ToastrService,
              private datePipe: DatePipe,
              protected toastr: ToastrService,
              private router: Router) {
    super(null, null, null, toastr, translateService, null);
  }

  ngOnInit(): void {
    this.init();
  }

  init() {
    const startDay = new Date();
    startDay.setDate(startDay.getDate() - 7);
    this.form.patchValue({call_time_from: startDay});
    this.form.patchValue({call_time_to: new Date()});
    this.campaignPerformInformationService.findAllCampaigns()
      .subscribe(res => {
        this.lstCampaigns = res;
      });
    this.campaignPerformInformationService.findAllAccountTDV()
      .subscribe(res => {
        this.lstAccountTDV = res;
      });
    this.onFilterTable();
  }

  search() {
    if (this.form.invalid) {
      return;
    }
    this.columns = [];
    this.dataModel.dataSource = [];
    const data = this.form.value;
    data.call_time_from = this.datePipe.transform(data.call_time_from, 'yyyy-MM-dd');
    data.call_time_to = this.datePipe.transform(data.call_time_to, 'yyyy-MM-dd');
    this.campaignPerformInformationService.getDataInformation(data)
      .subscribe(res => {
        this.lstDataInformation = res;
      });
    this.campaignPerformInformationService.getCallStatus(data.campaign_id)
      .subscribe(res => {
        this.lstCallStatus = res;
      });
    this.campaignPerformInformationService.getSurveyStatus(data.campaign_id)
      .subscribe(res => {
        this.lstSurveyStatus = res;
      });
  }

  export() {
    if (this.form.invalid){
      return;
    }
    const data: any = this.form.value;
    data.type = 1;
    this.campaignPerformInformationService.export(data, 'Thong_tin_thuc_hien_chien_dich');
  }

  changeCampaign() {
    this.campaignPerformInformationService.findAllCampaignResource(this.form.get('campaign_id').value)
      .subscribe(res => {
        this.lstCampaignResource = res;
      });
  }

  onPageChange(event) {
    this.page = event.pageIndex;
    this.pageSize = event.pageSize;
    this.search();
  }

  typeClick(value){
    this.getDataTable({type: value});
  }

  callStatusClick(value) {
    console.log('call status', value);
    this.getDataTable({status: value?.code});
  }

  surveyStatusClick(value) {
    console.log('survey status', value);
    this.getDataTable({saleStatus: value?.code});
  }

  getDataTable(value?) {
    this.columns = [];
    this.dataModel.dataSource = [];
    let data = this.form.value;
    data = {...data, ...value};
    this.campaignPerformInformationService.getDataTable(data).subscribe((next: any) => {
      if (next) {
        this.arrControls = next;
        this.registerControls();
      }
    });
  }

  registerControls() {
    const dataRows = [];
    this.columns = [];
    if (this.arrControls.length > 0) {
      this.arrControls[0].forEach(control => {
        if (control.code === 'CID'){
          this.columns.push({i18n: control.name, field: 'cidx', width: '150px'});
        } else {
          this.columns.push({i18n: control.name, field: control.code?.toString()});
        }
      });
      this.arrControls.forEach(lstControl => {
        const object = {};
        lstControl.forEach(control => {
          if (control.type === Constant.CBX_TYPE_ID) {
            const cbxValue = Array.from(control.options).find((opt: any) => opt.code === control.value) ?
              Array.from(control.options).find((opt: any) => opt.code === control.value)[`name`] : control.value;
            Object.defineProperty(object, control.code, {value: cbxValue});
          } else {
            Object.defineProperty(object, control.code, {value: control.value});
          }
          if (control.type === Constant.DATE_TYPE_ID) {
            this.formGroup.addControl(control.code, new FormControl(moment(control.value, FORMATS_DATE.parse.dateInput, true).toDate()));
          } else if (control.type === Constant.DATETIME_TYPE_ID) {
            this.formGroup.addControl(control.code, new FormControl(moment(control.value, FORMATS_DATETIME.parse.dateInput, true).toDate()));
          } else if (control.type === Constant.TIME_TYPE_ID) {
            this.formGroup.addControl(control.code, new FormControl(this.getTimeFromString(control.value)));
          } else this.formGroup.addControl(control.code, new FormControl(control.value));
        });
        dataRows.push(object);
      });
      this.columns.unshift({i18n: 'common.orderNumber', field: 'no', width: '50px'});
      this.dataModel.dataSource = dataRows;
      this.backUpDatasource = dataRows;
      console.log('dataRows',dataRows);
    } else {
      this.columns = [];
    }
  }
  getTimeFromString(strTime: any) {
    if (!strTime) return;
    const arrTime = strTime.split(':');
    if (arrTime.length !== 2) {
      console.error('Invalid time type (recommend HH:mm)');
    }
    return new Time(arrTime[0], arrTime[1]);
  }

  openDetail(row) {
    this.router.navigate(['/record-call-results/' + row['CID']]);
  }
  onFilterTable() {
    this.filterInput.valueChanges.subscribe(next => {
      this.dataModel.dataSource = this.backUpDatasource.filter((value:any) =>
        value.CID?.toLowerCase().includes(next?.toLowerCase())
        || value.CustomerName?.toLowerCase().includes(next?.toLowerCase())
        || value.MobilePhone?.toString().toLowerCase().includes(next?.toLowerCase())
        || value.SalesStatus?.toLowerCase().includes(next?.toLowerCase())
        || value.Status?.toLowerCase().includes(next?.toLowerCase())
      );
    });
  }
}
