import {NgModule} from '@angular/core';
import {CommonModule, DatePipe} from '@angular/common';
import {SharedModule} from '@shared';
import {CampaignPerformInformationComponent} from './campaign-perform-information/campaign-perform-information.component';
import {CampaignPerformInformationRoutingModule} from '@app/routes/campaign-perform-information/campaign-perform-information-routing.module';


@NgModule({
  declarations: [CampaignPerformInformationComponent],
  imports: [
    CommonModule,
    SharedModule,
    CampaignPerformInformationRoutingModule
  ],
  providers: [
    DatePipe
  ]
})
export class CampaignPerformInformationModule {
}
