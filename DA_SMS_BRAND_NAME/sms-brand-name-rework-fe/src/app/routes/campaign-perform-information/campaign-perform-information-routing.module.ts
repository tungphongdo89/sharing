import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {CampaignPerformInformationComponent} from '@app/routes/campaign-perform-information/campaign-perform-information/campaign-perform-information.component';

const routes: Routes = [
  {
    path: '',
    component: CampaignPerformInformationComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CampaignPerformInformationRoutingModule {
}
