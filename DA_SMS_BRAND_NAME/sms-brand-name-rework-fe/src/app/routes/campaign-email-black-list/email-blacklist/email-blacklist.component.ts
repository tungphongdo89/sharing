import {Component, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {MtxDialog, MtxGridColumn} from '@ng-matero/extensions';
import {FormBuilder, FormGroup, NgForm, Validators} from '@angular/forms';
import {CampaignEmailBlackListService} from '@core/services/campaign-email-black-list/campaign-email-black-list.service';
import {ToastrService} from 'ngx-toastr';
import {TranslateService} from '@ngx-translate/core';
import {ConfirmDialogComponent, ConfirmDialogModel} from '@shared/components/confirm-dialog/confirm-dialog.component';
import {environment} from '@env/environment';
import {CommonService} from '@shared/common/common.service';
import {DatePipe} from '@angular/common';
import {finalize} from 'rxjs/operators';

@Component({
  selector: 'app-email-blacklist',
  templateUrl: './email-blacklist.component.html',
  styleUrls: ['./email-blacklist.component.scss']
})
export class EmailBlacklistComponent implements OnInit {
  @ViewChild('button', {static: true}) button: TemplateRef<any>;
  @ViewChild('index', {static: true}) index: TemplateRef<any>;
  @ViewChild('createTime', {static: true}) createTime: TemplateRef<any>;
  columns: MtxGridColumn[] = [
    {
      i18n: 'No',
      width: '5%',
      field: 'no',
      cellTemplate: this.index
    },
    {i18n: 'Email', field: 'email'},
    {i18n: 'email_blacklist.Full_name', field: 'fullName'},
    {i18n: 'email_blacklist.Creator', field: 'createUserName'},
    {i18n: 'email_blacklist.Date_created', field: 'time'},
    {
      i18n: 'common.action',
      field: 'options',
      width: '50px',
      cellTemplate: this.button,
    }
  ];
  searchForm: FormGroup = this.fb.group({
    campaignEmailId: [null, Validators.required],
    email: [null, [Validators.required, Validators.email]],
    fullName: [null],
    file: [null]
  });
  pageSizeList = [10, 50, 100];
  pageSize = 10;
  totalPage = 0;
  page: number = 0;
  isLoading = true;
  file: File;
  lstCampaignEmailBlackList: any = [];
  lstCampaignEmailMarketing: any = [];
  importResult: any;
  isImportFalse: boolean = false;
  @ViewChild('form') form: NgForm;

  constructor(private fb: FormBuilder,
              private campaignEmailBlackListService: CampaignEmailBlackListService,
              private toastrService: ToastrService,
              private translate: TranslateService,
              private dialog: MtxDialog,
              private commonService: CommonService,
              private datePipe: DatePipe) {
  }

  ngOnInit(): void {
    this.init();
  }

  init() {
    this.campaignEmailBlackListService.findAllCampaignEmailMarketing().subscribe(res => {
      this.lstCampaignEmailMarketing = res;
      this.searchForm.get('campaignEmailId').setValue(null);
      this.search();
    });

  }

  search(reset?: boolean) {
    if (reset) {
      this.page = 0;
      this.pageSize = 10;
    }
    this.form.resetForm(this.form.value);
    const data: any = this.searchForm.value;
    data.size = this.pageSize;
    data.page = this.page;
    this.campaignEmailBlackListService.search(data)
      .pipe(finalize(() => this.isLoading = false))
      .subscribe(res => {
        this.lstCampaignEmailBlackList = res.body;
        this.totalPage = res.headers.get('X-Total-Count');
      }, error => this.toastrService.error('', error));

  }

  onPageChange(event) {
    this.page = event.pageIndex;
    this.pageSize = event.pageSize;
    this.search();
  }

  create() {
    if (this.searchForm.invalid) {
      return;
    }
    const data = this.searchForm.value;
    data.email = data.email?.toLowerCase();
    this.campaignEmailBlackListService.create(data).subscribe(res => {
        this.toastrService.success('', this.translate.instant('successfully_added_new'));
        this.form.resetForm();
        this.search(true);
      },
      error => {
        this.toastrService.error('', this.translate.instant('email_already_exists'));
      });
  }

  delete(row) {
    const title = this.translate.instant('common.confirm.title.delete');
    const content = this.translate.instant('common.MSG0017');
    const dialogContent: ConfirmDialogModel = new ConfirmDialogModel(title, content);
    this.dialog.originalOpen(ConfirmDialogComponent, {
      width: '25vw',
      disableClose: true,
      hasBackdrop: true,
      data: dialogContent
    }).afterClosed().subscribe(result => {
      if (result.data === 'confirm') {
        this.campaignEmailBlackListService.delete(row.id).subscribe(res => {
            this.toastrService.success('', this.translate.instant('delete_successfully'));
            this.search();
          }, error => this.toastrService.error('', error)
        );
      }
    });
  }


  export() {
    this.campaignEmailBlackListService.export(this.searchForm.value,
      'Danh sách đen gửi email' + this.datePipe.transform(new Date(), 'ddMMyyyyHHmm'));
  }

  changeFileUpload(event) {
    this.file = event.target.files[0];
  }

  uploadFile() {
    if (!this.file) {
      this.toastrService.warning('', this.translate.instant('select_file_upload'));
      return;
    }
    this.campaignEmailBlackListService.upLoad(this.file).subscribe(res => {
      if (res.status === 202) {
        this.toastrService.success(this.translate.instant('common.notify.import.success'));
        this.isImportFalse = false;
        this.importResult = res.body;
        this.search(true);
      }
    }, error => {
      this.toastrService.error(error.error.errorCodeConfig);
    });
  }

  downloadTemplate() {
    this.commonService.downloadFile(
      environment.serverUrl.api + '/campaign-email-blacklist/getSampleFile',
      null, null, ('Danh_sach_den_gui_email_' + this.datePipe.transform(new Date(), 'yyyyMMddHHmm')));
  }
}
