import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {EmailBlacklistComponent} from '@app/routes/campaign-email-black-list/email-blacklist/email-blacklist.component';

const routes: Routes = [
  {
    path: '',
    component: EmailBlacklistComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EmailBlacklistRoutingModule {
}
