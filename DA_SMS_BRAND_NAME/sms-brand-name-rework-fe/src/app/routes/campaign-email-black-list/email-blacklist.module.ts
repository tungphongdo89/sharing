import {NgModule} from '@angular/core';
import {CommonModule, DatePipe} from '@angular/common';
import {SharedModule} from '@shared';
import {EmailBlacklistRoutingModule} from '@app/routes/campaign-email-black-list/email-blacklist-routing.module';
import {EmailBlacklistComponent} from './email-blacklist/email-blacklist.component';


@NgModule({
  declarations: [EmailBlacklistComponent],
  imports: [
    CommonModule,
    SharedModule,
    EmailBlacklistRoutingModule
  ],
  providers: [
    DatePipe
  ]
})
export class EmailBlacklistModule {
}
