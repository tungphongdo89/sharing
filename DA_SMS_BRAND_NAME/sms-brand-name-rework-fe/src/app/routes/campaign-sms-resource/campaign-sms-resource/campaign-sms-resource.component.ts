import {Component, EventEmitter, OnInit, Output, TemplateRef, ViewChild} from '@angular/core';
import {TranslateService} from "@ngx-translate/core";
import {CampaignSmsResourceService} from "@core/services/campaign-sms-resource/campaign-sms-resource.service";
import {CampaignEmailBatchModel} from "@core/models/campaign-email-batch.interface";
import {MtxGridColumn} from "@ng-matero/extensions";
import {FormBuilder, FormGroup} from "@angular/forms";
import {CampaignSmsResourceModel} from "@core/models/campaign-sms-resource.model";
import {CampaignSmsBatchInterface, CampaignSmsBatchModel} from "@core/models/campaign-sms-batch.interface";
import {CampaignSmsMarketingService} from "@core/services/campaign-sms-marketing/campaign-sms-marketing.service";
import {DatePipe} from "@angular/common";
import {ToastrService} from "ngx-toastr";

@Component({
  selector: 'app-campaign-sms-resource',
  templateUrl: './campaign-sms-resource.component.html',
  styleUrls: ['./campaign-sms-resource.component.scss']
})
export class CampaignSmsResourceComponent implements OnInit {

  @ViewChild('button', {static: true}) button: TemplateRef<any>;
  @Output() pageChange: EventEmitter<any> = new EventEmitter<any>();
  @ViewChild('sendDate', {static: true}) sendDate: TemplateRef<any>;
  lstSmsMarketing: any[] = [];
  lstCampaignSmsResource: CampaignSmsResourceModel[] = [];
  lstCampaignSmsBatch: CampaignSmsBatchModel[] = [];
  lstStatus: { label: string, value: string }[] = [
    {label: this.translate.instant('campaign_email_resource.noSend'), value: '1'},
    {label: this.translate.instant('campaign_email_resource.sent'), value: '2'},
  ];
  campaignSmsResourceTable: MtxGridColumn[] = [];
  pageSizeList = [10, 50, 100];
  isLoading: boolean;
  total: number;
  offset = 0;
  searchForm: FormGroup = this.fb.group({
    campaignSmsMarketingId: [null],
    status: [null],
    sendDate: [null],
  });
  pageIndex: any;
  pageSize: any;
  dataSearch = {
    page: null,
    size: null,
  };

  constructor(private translate: TranslateService,
              private campaignSmsResourceService: CampaignSmsResourceService,
              private fb: FormBuilder,
              private campaignSmsMarketingService: CampaignSmsMarketingService,
              private toastr: ToastrService
  ) {
  }

  ngOnInit(): void {

    this.pageIndex = 0;
    this.pageSize = 10;
    this.campaignSmsMarketingService.getAll().subscribe(res => {
      this.lstSmsMarketing = res.body;
      console.log('abc',this.lstSmsMarketing)
    });
    this.search();
    this.campaignSmsResourceTable = [
      {i18n: 'campaign_sms_resource.index', field: 'no', width: '15px'},
      {i18n: 'campaign_sms_resource.phoneNumber', field: 'phoneNumber'},
      {i18n: 'campaign_sms_resource.c1', field: 'c1'},
      {i18n: 'campaign_sms_resource.c2', field: 'c2',},
      {i18n: 'campaign_sms_resource.c3', field: 'c3'},
      {i18n: 'campaign_sms_resource.c4', field: 'c4'},
      {i18n: 'campaign_sms_resource.c5', field: 'c5'},
      {i18n: 'campaign_sms_resource.c6', field: 'c6'},
      {i18n: 'campaign_sms_resource.c7', field: 'c7'},
      {i18n: 'campaign_sms_resource.c8', field: 'c8'},
      {i18n: 'campaign_sms_resource.c9', field: 'c9'},
      {i18n: 'campaign_sms_resource.c10', field: 'c10'},
      {i18n: 'campaign_sms_resource.sendStatus', field: 'sendStatus'},
      {i18n: 'campaign_sms_resource.sendUserId', field: 'sendUserName'},
      {i18n: 'campaign_sms_resource.sendDate', field: 'sendDate', cellTemplate: this.sendDate},
      {
        i18n: 'common.action',
        field: 'options',
        width: '50px',
        cellTemplate: this.button,
        pinned: 'right'
      }
    ];
  }

  onRowSelect(event: any[]) {
    this.lstCampaignSmsBatch = event.map(e => {
      const campaign = new CampaignSmsBatchModel();
      campaign.phoneNumber = e.phoneNumber;
      campaign.campaignSmsMarketingId = e.campaignSmsMarketingId;
      campaign.status = e.sendStatus;
      campaign.sendDate = e.sendDate;
      return campaign;
    });
  }

  onSearch() {
    this.pageIndex = 0;
    this.search();
  }

  search() {
    this.dataSearch.page = this.pageIndex;
    this.dataSearch.size = this.pageSize;
    this.isLoading = true;
    let status;
    let sendDate;
    let campaignSmsMarketingId;
    console.log(this.searchForm.get('status').value);
    if (this.searchForm.get('sendDate').value) {
      sendDate = this.setDateTime(this.searchForm.get('sendDate').value);
    } else {
      sendDate = null;
    }
    if (this.searchForm.get('status').value) {
      status = this.searchForm.get('status').value;
    } else {
      status = null;
    }
    if (this.searchForm.get('campaignSmsMarketingId').value !== '') {
      campaignSmsMarketingId = this.searchForm.get('campaignSmsMarketingId').value;
    } else {
      campaignSmsMarketingId = -1;
    }
    this.campaignSmsResourceService.doSearch(this.dataSearch, campaignSmsMarketingId, status, sendDate).subscribe(res => {
      this.lstCampaignSmsResource = res.body;
      this.total = Number(res.headers.get('X-Total-Count'));
      this.lstCampaignSmsBatch = [];
      this.isLoading = false;
    });
  }

  onPageChange(event) {
    this.pageIndex = event.pageIndex;
    this.pageSize = event.pageSize;
    this.search();
  }

  setDateTime(dateTime) {
    const pipe = new DatePipe('en-US');
    const date = pipe.transform(dateTime, 'dd/MM/yyyy', 'GMT+7');
    return date;
  }

  save() {
    if (this.lstCampaignSmsBatch.length < 1) {
      this.toastr.warning(this.translate.instant('Chưa có bản ghi nào được chọn'));
      return;
    }
    this.campaignSmsResourceService.createCampaignSmsBatch(this.lstCampaignSmsBatch).subscribe(res => {
      this.search();
      this.showNotifySuccess();
    }, error => {
      console.error(error);
      this.showUnknownErr();
    });
  }

  delete(row: any) {
    this.lstCampaignSmsResource = this.lstCampaignSmsResource.filter(e => {
      return e.id !== row.id;
    });
  }

  showNotifySuccess() {
    this.toastr.success(this.translate.instant('send_email_one.sendSuccess'));
  }

  showUnknownErr() {
    this.toastr.error(this.translate.instant('send_email_one.sendError'));
  }
}
