import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CampaignSmsResourceComponent } from './campaign-sms-resource.component';

describe('CampaignSmsResourceComponent', () => {
  let component: CampaignSmsResourceComponent;
  let fixture: ComponentFixture<CampaignSmsResourceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CampaignSmsResourceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CampaignSmsResourceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
