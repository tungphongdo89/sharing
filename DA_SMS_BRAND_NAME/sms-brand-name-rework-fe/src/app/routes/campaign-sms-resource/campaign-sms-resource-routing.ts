import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {CampaignSmsResourceComponent} from '@app/routes/campaign-sms-resource/campaign-sms-resource/campaign-sms-resource.component';

const routes: Routes = [
  {
    path: '',
    component: CampaignSmsResourceComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CampaignSmsResourceRouting {
}
