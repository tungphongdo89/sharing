import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SharedModule} from "@shared";
import {CampaignSmsResourceComponent} from './campaign-sms-resource/campaign-sms-resource.component';
import {CampaignSmsResourceRouting} from "@app/routes/campaign-sms-resource/campaign-sms-resource-routing";


@NgModule({
  declarations: [CampaignSmsResourceComponent],
  imports: [
    CommonModule,
    SharedModule,
    CampaignSmsResourceRouting
  ]
})
export class CampaignSmsResourceModule {
}
