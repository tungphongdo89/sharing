import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportRenderCallComponent } from './report-render-call.component';

describe('ReportRenderCallComponent', () => {
  let component: ReportRenderCallComponent;
  let fixture: ComponentFixture<ReportRenderCallComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportRenderCallComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportRenderCallComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
