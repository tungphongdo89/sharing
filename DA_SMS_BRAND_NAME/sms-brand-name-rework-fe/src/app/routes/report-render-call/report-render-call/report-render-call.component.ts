import { Component, OnInit } from '@angular/core';
import {MtxGridColumn} from '@ng-matero/extensions';

@Component({
  selector: 'app-report-render-call',
  templateUrl: './report-render-call.component.html',
  styleUrls: ['./report-render-call.component.scss']
})
export class ReportRenderCallComponent implements OnInit {

  columns: MtxGridColumn[] = [];
  pageSizeList = [10, 50, 100];
  isLoading = false;
  offset: any = 0;
  pagesize = 10;
  total: any = 0;
  campaign: any = [];

  constructor() { }

  ngOnInit(): void {

    this.columns = [
      {i18n: 'report_render_call.column.no', field: 'index', width: '70px',},
      {i18n: 'report_render_call.column.cid', field: 'cid'},
      {i18n: 'report_render_call.phone_number', field: 'phone_number'},
      {i18n: 'report_render_call.aa', field: 'aa'},
      {i18n: 'report_render_call.column.date_call_no1', field: 'date_call_no1'},
      {i18n: 'report_render_call.column.status_connect_date_call_no1', field: 'status_connect_date_call_no1'},
      {i18n: 'report_render_call.column.status_date_call_no1', field: 'status_date_call_no1'},
      {i18n: 'report_render_call.column.date_call_no2', field: 'date_call_no2'},
      {i18n: 'report_render_call.column.status_connect_date_call_no2', field: 'status_connect_date_call_no2'},
      {i18n: 'report_render_call.column.status_date_call_no2', field: 'status_date_call_no2'},
      {i18n: 'report_render_call.column.date_call_no3', field: 'date_call_no3'},
      {i18n: 'report_render_call.column.status_connect_date_call_no3', field: 'status_connect_date_call_no3'},
      {i18n: 'report_render_call.column.status_date_call_no3', field: 'status_date_call_no3'},
      {i18n: 'report_render_call.evaluater', field: 'evaluater'},
    ];
  }

  onPageChange(event) {
    this.offset = event.pageIndex;
    this.pagesize = event.pageSize;
  }

}
