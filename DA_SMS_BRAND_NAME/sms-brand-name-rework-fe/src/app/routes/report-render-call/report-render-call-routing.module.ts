import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ReportRenderCallComponent} from '@app/routes/report-render-call/report-render-call/report-render-call.component';


const routes: Routes = [
  {
    path:'',
    component: ReportRenderCallComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportRenderCallRoutingModule { }
