import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReportRenderCallRoutingModule } from './report-render-call-routing.module';
import { ReportRenderCallComponent } from './report-render-call/report-render-call.component';
import {SharedModule} from '@shared';


@NgModule({
  declarations: [ReportRenderCallComponent],
  imports: [
    CommonModule,
    SharedModule,
    ReportRenderCallRoutingModule
  ]
})
export class ReportRenderCallModule { }
