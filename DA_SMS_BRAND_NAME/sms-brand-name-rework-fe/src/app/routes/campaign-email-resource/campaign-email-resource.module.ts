import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CampaignEmailResourceComponent} from './campaign-email-resource/campaign-email-resource.component';
import {SharedModule} from '@shared';
import {CampaignEmailResourceRouting} from '@app/routes/campaign-email-resource/campaign-email-resource-routing';


@NgModule({
  declarations: [CampaignEmailResourceComponent],
  imports: [
    CommonModule,
    SharedModule,
    CampaignEmailResourceRouting
  ]
})
export class CampaignEmailResourceModule {
}
