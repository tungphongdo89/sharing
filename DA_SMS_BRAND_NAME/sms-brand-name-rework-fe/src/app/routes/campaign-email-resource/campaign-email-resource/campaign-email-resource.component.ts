import {Component, EventEmitter, OnInit, Output, TemplateRef, ViewChild} from '@angular/core';
import {CampaignEmailMarketingService} from '@core/services/campaign-email-marketing/campaign-email-marketing.service';
import {MtxGridColumn} from '@ng-matero/extensions';
import {CampaignBlacklistInterface} from '@core/models/campaign-blacklist.interface';
import {CampaignEmailResourceModel} from '@core/models/campaign-email-resource.model';
import {CampaignEmailResourceService} from '@core/services/campaign-email-resource/campaign-email-resource.service';
import {FormBuilder, FormGroup} from '@angular/forms';
import {TranslateService} from '@ngx-translate/core';
import {DatePipe} from '@angular/common';
import {CampaignEmailBatchModel} from '@core/models/campaign-email-batch.interface';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-campaign-email-marketing',
  templateUrl: './campaign-email-resource.component.html',
  styleUrls: ['./campaign-email-resource.component.scss']
})
export class CampaignEmailResourceComponent implements OnInit {
  @ViewChild('button', {static: true}) button: TemplateRef<any>;
  @Output() pageChange: EventEmitter<any> = new EventEmitter<any>();
  @ViewChild('sendDate', {static: true}) sendDate: TemplateRef<any>;
  lstEmailMarketing: any[] = [];
  lstCampaignEmailResource: CampaignEmailResourceModel[] = [];
  lstCampaignEmailBatch: CampaignEmailBatchModel[] = [];
  lstStatus: { label: string, value: string }[] = [
    {label: this.translate.instant('campaign_email_resource.noSend'), value: '1'},
    {label: this.translate.instant('campaign_email_resource.sent'), value: '2'},
  ];
  campaignEmailResourceTable: MtxGridColumn[] = [];
  pageSizeList = [10, 50, 100];
  isLoading: boolean;
  total: number;
  offset = 0;
  searchForm: FormGroup = this.fb.group({
    campaignEmailMarketingId: [null],
    status: [null],
    sendDate: [null],
  });
  pageIndex: any;
  pageSize: any;
  dataSearch = {
    page: null,
    size: null,
  };

  constructor(private campaignEmailMarketingService: CampaignEmailMarketingService,
              private campaignEmailResourceService: CampaignEmailResourceService,
              private fb: FormBuilder,
              private translate: TranslateService,
              private toastr: ToastrService) {
  }

  ngOnInit(): void {
    this.pageIndex = 0;
    this.pageSize = 10;
    this.campaignEmailMarketingService.getAll().subscribe(res => {
      this.lstEmailMarketing = res.body;
      console.log('email', this.lstEmailMarketing);
    });
    this.search();
    this.campaignEmailResourceTable = [
      {i18n: 'campaign_email_resource.index', field: 'no', width: '15px'},
      {i18n: 'campaign_email_resource.email', field: 'email'},
      {i18n: 'campaign_email_resource.c1', field: 'c1'},
      {i18n: 'campaign_email_resource.c2', field: 'c2',},
      {i18n: 'campaign_email_resource.c3', field: 'c3'},
      {i18n: 'campaign_email_resource.c4', field: 'c4'},
      {i18n: 'campaign_email_resource.c5', field: 'c5',},
      {i18n: 'campaign_email_resource.c6', field: 'c6'},
      {i18n: 'campaign_email_resource.c7', field: 'c7'},
      {i18n: 'campaign_email_resource.c8', field: 'c8',},
      {i18n: 'campaign_email_resource.c9', field: 'c9'},
      {i18n: 'campaign_email_resource.c10', field: 'c10'},
      {i18n: 'campaign_email_resource.sendStatus', field: 'sendStatus'},
      {i18n: 'campaign_email_resource.sendUserId', field: 'sendUserName'},
      {i18n: 'campaign_email_resource.sendDate', field: 'sendDate', cellTemplate: this.sendDate},
      {
        i18n: 'common.action',
        field: 'options',
        width: '50px',
        cellTemplate: this.button,
        pinned:'right'
      }
    ];
  }

  onRowSelect(event: any[]) {
    this.lstCampaignEmailBatch = event.map(e => {
      const campaign = new CampaignEmailBatchModel();
      campaign.campaignEmailResourceId = e.id;
      campaign.email = e.email;
      campaign.campaignEmailMarketingId = e.campaignEmailMarketingId;
      campaign.status = e.sendStatus;
      campaign.sendDate = e.sendDate;
      return campaign;
    });
    console.log('list', this.lstCampaignEmailBatch);
  }

  onSearch() {
    this.pageIndex = 0;
    this.search();
  }

  search() {
    this.dataSearch.page = this.pageIndex;
    this.dataSearch.size = this.pageSize;
    this.isLoading = true;
    let status;
    let sendDate;
    let campaignEmailMarketingId;
    console.log(this.searchForm.get('status').value);
    if (this.searchForm.get('sendDate').value) {
      sendDate = this.setDateTime(this.searchForm.get('sendDate').value);
    } else {
      sendDate = null;
    }
    if (this.searchForm.get('status').value) {
      status = this.searchForm.get('status').value;
    } else {
      status = null;
    }
    if (this.searchForm.get('campaignEmailMarketingId').value !== '') {
      campaignEmailMarketingId = this.searchForm.get('campaignEmailMarketingId').value;
    } else {
      campaignEmailMarketingId = -1;
    }
    this.campaignEmailResourceService.doSearch(this.dataSearch, campaignEmailMarketingId, status, sendDate).subscribe(res => {
      this.lstCampaignEmailResource = res.body;
      this.total = Number(res.headers.get('X-Total-Count'));
      this.lstCampaignEmailBatch = [];
      this.isLoading = false;
    });
  }

  onPageChange(event) {
    this.pageIndex = event.pageIndex;
    this.pageSize = event.pageSize;
    this.search();
  }

  setDateTime(dateTime) {
    const pipe = new DatePipe('en-US');
    const date = pipe.transform(dateTime, 'dd/MM/yyyy', 'GMT+7');
    return date;
  }

  save() {
    if (this.lstCampaignEmailBatch.length < 1) {
      this.toastr.warning(this.translate.instant('Chưa có bản ghi nào được chọn'));
      return;
    }
    this.campaignEmailResourceService.createCampaignEmailBatch(this.lstCampaignEmailBatch).subscribe(res => {
      this.search();
      this.showNotifySuccess();
    }, error => {
      console.error(error);
      this.showUnknownErr();
    });
  }

  delete(row: any) {
    console.log('campaignResource', this.lstCampaignEmailResource);
    this.lstCampaignEmailResource = this.lstCampaignEmailResource.filter(e => {
      return e.id !== row.id;
    });
  }

  showNotifySuccess() {
    this.toastr.success(this.translate.instant('send_email_one.sendSuccess'));
  }

  showUnknownErr() {
    this.toastr.error(this.translate.instant('send_email_one.sendError'));
  }
}
