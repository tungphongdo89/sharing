import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {CampaignEmailResourceComponent} from '@app/routes/campaign-email-resource/campaign-email-resource/campaign-email-resource.component';

const routes: Routes = [
  {
    path: '',
    component: CampaignEmailResourceComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CampaignEmailResourceRouting {
}
