import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuthorizationVocComponent } from './authorization-voc.component';

describe('AuthorizationVocComponent', () => {
  let component: AuthorizationVocComponent;
  let fixture: ComponentFixture<AuthorizationVocComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AuthorizationVocComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuthorizationVocComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
