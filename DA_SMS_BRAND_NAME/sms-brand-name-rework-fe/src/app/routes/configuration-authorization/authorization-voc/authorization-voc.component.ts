import {Component, ElementRef, EventEmitter, Input, OnInit, Output, TemplateRef, ViewChild} from '@angular/core';
import {AuthorizationInfo} from '@core/models/authorization-info';
import {FormControl, FormGroup} from '@angular/forms';
import {TelcosSelectBoxModel} from '@core/models/telco_model';
import {MtxDialog, MtxGridColumn} from '@ng-matero/extensions';
import {AuthorzirationVoc} from '@core/services/config-author/authorziration-voc';
import {Constant} from '@shared/Constant';
import {ConfirmDialogComponent} from '@shared/components/confirm-dialog/confirm-dialog.component';
import {ToastrService} from 'ngx-toastr';
import {TranslateService} from '@ngx-translate/core';
import {finalize} from 'rxjs/operators';

@Component({
  selector: 'app-authorization-voc',
  templateUrl: './authorization-voc.component.html',
  styleUrls: ['./authorization-voc.component.scss']
})
export class AuthorizationVocComponent implements OnInit {
  @ViewChild('keyword') keyword: ElementRef;
  @ViewChild('createTicket', { static: true }) createTicket: TemplateRef<any>;
  @ViewChild('processTicket', { static: true }) processTicket: TemplateRef<any>;
  @ViewChild('confirmTicket', { static: true }) confirmTicket: TemplateRef<any>;
  @ViewChild('button', { static: true }) button: TemplateRef<any>;
  @ViewChild('index', { static: true }) index: TemplateRef<any>;
  @Input() data: AuthorizationInfo[] = [];
  @Output() pageChange: EventEmitter<any> = new EventEmitter<any>();
  @Output() dataSearch: EventEmitter<AuthorizationInfo> = new EventEmitter<AuthorizationInfo>();
  formSearch: FormGroup;
  telcos: TelcosSelectBoxModel[];
  limit = 10;
  start = 0;
  REGEX = /^[-.a-zA-Z0-9_]{1,}$/;
  columns: MtxGridColumn[] = [] ;
  pagesize = 10;
  offset: any = 0;
  total: any = 0;
  pageSizeList = [10, 50, 100];
  listAction = [];
  isLoading = true;
  confirmDialog : boolean;
  isAdvSearch = false;
  buttonStatus: boolean;
  userRequest: AuthorizationInfo = new AuthorizationInfo();
  constructor(private authorVOCService: AuthorzirationVoc,
              public dialog: MtxDialog, protected toastr: ToastrService,
              protected translateService: TranslateService,) { }

  ngOnInit(): void {
    this.formSearch = new FormGroup({
      keyword: new FormControl()
    });
    this.columns = [
      {i18n: 'stt', field: 'id', cellTemplate: this.index},
      {i18n: 'userName', field: 'login'},
      {i18n: 'fullName', field: 'fullName'},
      {i18n: 'receive', field: 'createTicket', cellTemplate: this.createTicket},
      {i18n: 'handling', field: 'processTicket', cellTemplate: this.processTicket},
      {i18n: 'confirm', field: 'confirmTicket', cellTemplate: this.confirmTicket},
      {i18n: 'common.action',
        field: 'options',
        width: '200px',
        pinned: 'right',
        right: '0px',
        type: 'button',
        cellTemplate: this.button
      }
    ];
    this.isLoading = false;
    this.getAllUsers();
    this.onSearch();
  }
  getAllUsers(){
    this.authorVOCService.getAllUsers().subscribe(res =>{
      this.data = res;
    });
  }
  updateAuthorVOC(author : AuthorizationInfo){
    this.authorVOCService.updateAuthorzirationVOC(author).subscribe(res => {
      this.getAllUsers();
      if (res.msgCode === Constant.OK) {
        this.showNotifySuccess();
      }
      else {
        this.showUnknownErr();
      }
    });
  }
  onPageChange(event) {
    this.offset = event.pageIndex;
    this.pagesize = event.pageSize;
    this.onSearch();
  }
  onChangeEvent(event) {
    this.buttonStatus = true;
  }
  openDialogConfirm(author : AuthorizationInfo) {
    const dialogRef = this.dialog.open({
      maxWidth: '45vw',
      maxHeight: '45vh',
      width: '45vw',
      autoFocus: false,
      // data: {cpCode, message},
    }, ConfirmDialogComponent);
    dialogRef.afterClosed().subscribe(result => {
      if (result.data === 'confirm') {
        this.updateAuthorVOC(author);
      }
      if (result.data === 'cancel') {
        this.getAllUsers();
      }
    });
  }
  checkValidateForm(): boolean {
    let check = true;
    if(this.formSearch.invalid){
      check = false;
      Object.keys((this.formSearch.controls)).forEach(key => {
        this.formSearch.get(key).markAllAsTouched();
      });
    }
    return check;
  }

  onSearch(reset?: boolean){
    if (reset) {
      this.offset = 0;
      this.pagesize = 10;
    }
    const keyword = this.formSearch.get('keyword').value;
    if(keyword == null) {
      this.getAllUsers();
      this.isLoading = false;
      this.isAdvSearch = false;
      return;
    }
    this.userRequest.login = keyword;
    this.authorVOCService.searchUserByKeyword(this.userRequest, {page: this.offset, size: this.pagesize}).
    pipe(finalize(() => {
      this.isLoading = false;
      this.isAdvSearch = false;
    })).subscribe(res => {
      this.data = res.body;
      if(this.data.length === 0){
        this.showNotifyNotFound();
      }
      this.total = Number(res.headers.get('X-Total-Count'));
    }, () => this.showUnknownErr());
  }

  trimSpace(event, controlName: string) {
    if(event) {
      const key = event.trim();
      this.formSearch.controls[controlName].setValue(key);
    }
  }
  showUnknownErr() {
    this.toastr.error(this.translateService.instant('common.notify.fail'));
  }
  showNotifySuccess() {
    this.toastr.success(this.translateService.instant('common.notify.save.success'));
  }
  showNotifyNotFound() {
    this.toastr.error(this.translateService.instant('common.notify.save.notfound'));
  }
}
