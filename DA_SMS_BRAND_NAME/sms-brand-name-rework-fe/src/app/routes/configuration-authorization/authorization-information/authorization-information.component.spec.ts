import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuthorizationInformationComponent } from './authorization-information.component';

describe('AuthorizationInformationComponent', () => {
  let component: AuthorizationInformationComponent;
  let fixture: ComponentFixture<AuthorizationInformationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AuthorizationInformationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuthorizationInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
