import {Routes, RouterModule} from '@angular/router';
import {NgModule} from '@angular/core';
import {AuthorizationInformationComponent} from '@app/routes/configuration-authorization/authorization-information/authorization-information.component';

const routes: Routes = [
  {
    path: '',
    component: AuthorizationInformationComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ConfigurationRouting {
}
