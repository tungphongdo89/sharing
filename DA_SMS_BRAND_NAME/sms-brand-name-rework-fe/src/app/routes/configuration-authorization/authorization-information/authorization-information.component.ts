import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {TelcosSelectBoxModel} from '@core/models/telco_model';
import {MtxGridColumn} from '@ng-matero/extensions';
import {AuthorizationInfo} from '@core/models/authorization-info';

@Component({
  selector: 'app-authorization-information',
  templateUrl: './authorization-information.component.html',
  styleUrls: ['./authorization-information.component.scss']
})
export class AuthorizationInformationComponent implements OnInit {
  @Output() pageChange: EventEmitter<any> = new EventEmitter<any>();
  formSearch: FormGroup;
  telcos: TelcosSelectBoxModel[];
  limit = 10;
  start = 0;
  REGEX = /^[-.a-zA-Z0-9_]{1,}$/;
  columns: MtxGridColumn[] = [];
  pagesize = 10;
  pageSizeList = [10, 50, 100];
  listAction = [];
  isLoading = true;
  constructor() { }

  ngOnInit(): void {
  }
  search(){}
  onPageChange(event) {
    this.pageChange.emit(event);
  }
}
