import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SharedModule} from '@shared/shared.module';
import {EventManagementRouting} from '@app/routes/event-management/event-management.routing';
import {AuthorizationInformationComponent} from '@app/routes/configuration-authorization/authorization-information/authorization-information.component';
import {ConfigurationProcessingTermComponent} from '@app/routes/configuration-authorization/configuration-schedule/configuration-processing-term.component';
import {AuthorizationVocComponent} from '@app/routes/configuration-authorization/authorization-voc/authorization-voc.component';
import {ConfigurationRouting} from '@app/routes/configuration-authorization/authorization-information/configuration.routing';


@NgModule({
  declarations: [
    AuthorizationInformationComponent,
    ConfigurationProcessingTermComponent,
    AuthorizationVocComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    ConfigurationRouting
  ],
  entryComponents: []
})
export class ConfigurationModule {
}
