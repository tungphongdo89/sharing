import {Component, ElementRef, EventEmitter, Input, OnInit, Output, TemplateRef, ViewChild} from '@angular/core';
import {AuthorizationInfo} from '@core/models/authorization-info';
import {AbstractControl, FormBuilder, FormControl, FormGroup, NgModel, ValidatorFn, Validators} from '@angular/forms';
import {TelcosSelectBoxModel} from '@core/models/telco_model';
import {MtxDialog, MtxGridColumn} from '@ng-matero/extensions';
import {ConfigSchedule} from '@core/services/config-author/config-schedule';
import {ConfigScheduleModel} from '@core/models/config-schedule-model';
import {ToastrService} from 'ngx-toastr';
import {TranslateService} from '@ngx-translate/core';
import {Constant} from '@shared/Constant';
import {Pipe, PipeTransform} from '@angular/core';
import {finalize} from 'rxjs/operators';
import {CommonService} from '@shared/services/common.service';
import {DatePipe} from '@angular/common';
import {environment} from '@env/environment';
import {ApDomainService} from '@core/services/ap-domain/ap-domain.service';
import {BussinessTypeService} from '@core/services/bussiness-type/bussiness-type.service';
import { ThemePalette } from '@angular/material/core';
import { ConfirmDialogComponent, ConfirmDialogModel } from '@app/shared/components/confirm-dialog/confirm-dialog.component';
import * as moment from 'moment';
export class CustomeDateValidators {
  static fromToDate(fromDateField: string, toDateField: string, errorName: string = 'fromToDate'): ValidatorFn {
    return (formGroup: AbstractControl): { [key: string]: boolean } | null => {
      const fromDate = formGroup.get(fromDateField).value;
      const toDate = formGroup.get(toDateField).value;
      const fromDateType = formGroup.get('processTimeType').value;
      const toDateType = formGroup.get('confirmTimeType').value;
      let fromFormat = 1;
      let toFormat = 1;
      switch (fromDateType) {
        case '1' :
          fromFormat = 1;
          break;
        case '2' :
          fromFormat = 60;
          break;
        case '3' :
          fromFormat = 1440;
          break;
      }
      switch (toDateType) {
        case '1' :
          toFormat = 1;
          break;
        case '2' :
          toFormat = 60;
          break;
        case '3' :
          toFormat = 1440;
          break;
      }
      // Ausing the fromDate and toDate are numbers. In not convert them first after null check
      if ((fromDate && toDate  && fromDateType && toDateType) ) {
        if (((fromDate * fromFormat) > (toDate * toFormat))) {
          formGroup.get(toDateField).setErrors({toFromDate: true});
          formGroup.get(fromDateField).setErrors({fromToDate: true});
        } else {
          formGroup.get(fromDateField).setErrors(null);
          formGroup.get(toDateField).setErrors(null);
        }

        return null;
      }
      return null;
    };
  }
}
@Component({
  selector: 'app-configuration-processing-term',
  templateUrl: './configuration-processing-term.component.html',
  styleUrls: ['./configuration-processing-term.component.scss'],
  providers: [DatePipe]
})
export class ConfigurationProcessingTermComponent implements OnInit, PipeTransform {
  @ViewChild('button', {static: true}) button: TemplateRef<any>;
  @ViewChild('reqType', {static: true}) reqType: TemplateRef<any>;
  @ViewChild('processTime', {static: true}) proTime: TemplateRef<any>;
  @ViewChild('confirmTime', {static: true}) confTime: TemplateRef<any>;
  @ViewChild('busType', {static: true}) busType: TemplateRef<any>;
  @ViewChild('index', {static: true}) index: TemplateRef<any>;
  @ViewChild('inputElement', {static: false}) fileInput: ElementRef;
  @Pipe({name: 'appFilter'})
  @Input() data: ConfigScheduleModel[] = [];
  selectRequest: any[];
  selectBusiness: any[];
  @Input() timer: any[] = [
    {code: '1', time: 'Phút'},
    {code: '2', time: 'Giờ'},
    {code: '3', time: 'Ngày'}
  ];
  @Output() pageChange: EventEmitter<any> = new EventEmitter<any>();
  formInput: FormGroup;
  telcos: TelcosSelectBoxModel[];
  limit = 10;
  start = 0;
  demo1;
  REGEX = /^[-.a-zA-Z0-9_]{1,}$/;
  columns: MtxGridColumn[] = [];
  pagesize = 10;
  offset: any = 0;
  totalRecord = 0;
  pageSizeList = [10, 50, 100];
  listAction = [];
  isLoading = true;
  keyword: string;
  configModel: ConfigScheduleModel = new ConfigScheduleModel();
  isAdvSearch = false;
  idConfig: number;
  file: File;
  importComplete = false;
  importResult: any;
  color: ThemePalette = 'primary';
  formGroup: FormGroup = this.fb.group({
    requestType: null,
    bussinessType: null,
    processTime: null,
    processTimeType: '1',
    confirmTime: null,
    confirmTimeType: '1',
    bussinessTypeStr: null,
    confirmTimeStr: null,
    createDate: null,
    createUser: null,
    error: null,
    errorCodeConfig: null,
    id: null,
    importType: null,
    keySearch: null,
    page: null,
    processTimeStr: null,
    requestTypeStr: null,
    size: null,
    status: null,
    updateDate: null,
    updateUser: null,
  } ,{ validator: [
    CustomeDateValidators.fromToDate('processTime', 'confirmTime')
]});
  constructor(private configSchedule: ConfigSchedule, protected toastr: ToastrService,
              protected translateService: TranslateService,
              private commonService: CommonService,
              private fb: FormBuilder,
              private apDomainService: ApDomainService,
              private bussinessTypeService: BussinessTypeService,
              private datePipe: DatePipe,
              private dialog: MtxDialog) {
  }

  transform(data: any[], keyword: string): any[] {
    this.data = data;
    if (!data) {
      return [];
    }
    if (!keyword) {
      return data;
    }
    keyword = keyword.toLocaleLowerCase();

    return data.filter(it => {
      return it.toLocaleLowerCase().includes(keyword);
    });
  }

  ngOnInit(): void {
    this.formInput = new FormGroup({
      keyword: new FormControl()
    });
    this.formGroup.valueChanges.subscribe(res => {
      this.configModel = this.formGroup.value;
    });
    this.columns = [
      {i18n: 'stt', field: 'id', cellTemplate: this.index},
      {i18n: 'requestType', field: 'requestType', cellTemplate: this.reqType},
      {i18n: 'businessType', field: 'bussinessType', cellTemplate: this.busType},
      {i18n: 'timeHandling', field: 'processTime',cellTemplate:this.proTime},
      {i18n: 'timeConfirm', field: 'confirmTime',cellTemplate:this.confTime},
      {
        i18n: 'common.action',
        field: 'options',
        width: '200px',
        pinned: 'right',
        right: '0px',
        type: 'button',
        cellTemplate: this.button,
        buttons: [
          {
            type: 'icon',
            text: 'edit',
            icon: 'edit',
            click: record => this.getConfigScheduleById(record.id),
          },
          {
            type: 'icon',
            text: 'delete',
            icon: 'delete',
            click: record => this.deleteConfigScheduleById(record.id),
          }
        ]
      },
    ];
    // this.configModel = new ConfigScheduleModel();
    this.isLoading = false;
    // this.getConfigSchedule();
    // request_type
    this.apDomainService.getDomainByType('REQUEST_TYPE').pipe().subscribe(res => {
      this.selectRequest = res.map(e => {
        e.code = Number(e.code);
        return e;
      });
    });
    // business_type
    this.bussinessTypeService.getBussinessType().subscribe(res => {
      this.selectBusiness = res;
    });
    this.formInput = this.fb.group({
      requestType: ['', [Validators.required]],
      bussinessType: ['', [Validators.required]],
      processTime: ['', [Validators.required]],
      processTimeType: ['', [Validators.required]],
      confirmTime: ['', [Validators.required]],
      confirmTimeType: ['', [Validators.required]],
    });
    this.formInput = new FormGroup({
      keyword: new FormControl()
    });
    this.onSearch();
    this.configModel.processTimeType = '1';
    this.configModel.confirmTimeType = '1';
  }

  get f() {
    return this.formInput.controls;
  }

  submit() {
    if (this.formGroup.invalid) {
      this.formGroup.markAllAsTouched();
    } else if (!this.idConfig) {
      this.configSchedule.addConfigSchedule(this.configModel).subscribe(res => {
        // if (Constant.OK === res.msgCode) {
          this.showNotifySuccess();
          this.getConfigSchedule();
        // } else {
        //   this.showUnknownErr();
        //   return;
        // }
      }, error => {
        this.toastr.error(error.error.title);
      });
    } else {
      this.configSchedule.updateConfigSchedule(this.configModel).subscribe(res => {
        // if (Constant.OK === res.msgCode) {
          this.showNotifyUpdateSuccess();
          this.getConfigSchedule();
          // this.idConfig = 0;
        // } else {
        //   this.showUnknownErr();
        //   return;
        // }
      }, error => {
        this.toastr.error(error.error.title);
      });
    }
  }

  onSearch() {
    this.isAdvSearch = true;
    this.isLoading = true;
    const keyword = this.formInput.get('keyword').value;
    if (keyword == null || keyword === '') {
      this.getConfigSchedule();
      this.isLoading = false;
      this.isAdvSearch = false;
      return;
    }
    this.configModel.keySearch = keyword;
    this.configModel.page = 1;
    this.configModel.size = this.pagesize;
    this.onSearchConfigSchedule({offset: 0});

  }

  onSearchConfigSchedule(pageInfo) {
    const pageToLoad: number = pageInfo.offset;
    this.configSchedule.searchText(this.configModel, {
      page: pageToLoad,
      size: this.pagesize
    }).pipe(finalize(() => {
      this.isLoading = false;
      this.isAdvSearch = false;
    })).subscribe(res => {
      console.log('datares',res.body);
      this.data = res.body;
      this.totalRecord = Number(res.headers.get('X-Total-Count'));
      if (this.data.length === 0) {
        this.showNotifyNotFound();
      }
    }, () => this.showUnknownErr());
  }

  onPageChange(event) {
    this.offset = event.pageIndex;
    this.pagesize = event.pageSize;
    this.onSearchConfigSchedule({offset: this.offset});
    this.pageChange.emit(event);
  }

  getConfigSchedule() {
    this.configSchedule.getAllConfigSchedule().subscribe(res => {
      this.data = res;
    });
  }

  showNotifySuccess() {
    this.toastr.success(this.translateService.instant('common.notify.save.success2'));
  }

  showNotifyNotFound() {
    this.toastr.error(this.translateService.instant('common.notify.save.notfound'));
  }

  showUnknownErr() {
    this.toastr.error(this.translateService.instant('common.notify.save.error2'));
  }

  showNotifyUpdateSuccess() {
    this.toastr.success(this.translateService.instant('common.notify.save.success'));
  }

  getConfigScheduleById(id: any) {
    this.configSchedule.searchConfigSchedule(id).subscribe(res => {
      this.configModel = res;
      this.formGroup.patchValue(res);
      this.idConfig = id;
    });
  }

  reset() {
    this.formGroup.patchValue({
      requestType: null,
      bussinessType: null,
      processTime: null,
      processTimeType: '1',
      confirmTime: null,
      confirmTimeType: '1',
      bussinessTypeStr: null,
      confirmTimeStr: null,
      createDate: null,
      createUser: null,
      error: null,
      errorCodeConfig: null,
      id: null,
      importType: null,
      keySearch: null,
      page: null,
      processTimeStr: null,
      requestTypeStr: null,
      size: null,
      status: null,
      updateDate: null,
      updateUser: null
    });
    this.idConfig = null;
  }

  downloadFile() {
    this.commonService.downloadFile(
      environment.serverUrl.api + '/config-schedules/getSampleFile',
      null, null, ('report_cpAliasTmp_' + this.datePipe.transform(new Date(), 'yyyyMMddHHmmss')));
  }

  uploadFile(event) {
    this.file = event.target.files[0];
  }

  submitUploadFile() {
    // this.configSchedule.uploadFile(this.file).subscribe(res =>{
    //   if(Constant.OK === res.msgCode){
    //     this.showNotifySuccess();
    //     this.getConfigSchedule();
    //   }
    //   else {
    //     this.showUnknownErr();
    //   }
    // });
  }

  // handleFileInput(file: FileList) {
  //   this.file = file.item(0);
  //   this.configSchedule.uploadFile(this.file).subscribe(res => {
  //     this.importComplete = true;
  //     this.importResult = res.body;
  //     this.onSearchConfigSchedule({ offset: 0 });
  //     this.toastr.success(this.translateService.instant('common.notify.import.success'));
  //   }, error => {
  //     this.importComplete = true;
  //     this.importResult = error.error;
  //     this.toastr.error(this.translateService.instant('common.notify.import.fail'));
  //   });
  //   this.fileInput.nativeElement.value = '';
  // }
  downloadResult() {
    this.commonService.saveFile(this.importResult, 'file-result', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;');
  }

  clickShowFileUpload() {
    if(typeof this.file !== 'undefined' && this.file !== null){
      this.configSchedule.uploadFile(this.file).subscribe(res => {
        this.importComplete = true;
        this.importResult = res.body;
        this.onSearchConfigSchedule({ offset: 0 });
        this.file = null;
        this.toastr.success(this.translateService.instant('common.notify.import.success'));
      }, error => {
        this.importComplete = true;
        this.importResult = error.error;
        this.file = null;
        this.toastr.error(this.translateService.instant('common.notify.import.fail'));
      });
    }
  }

  deleteConfigScheduleById(id: number) {
    const title = this.translateService.instant('common.confirm.title.delete');
    const content = this.translateService.instant('common.MSG0017');
    const dialogContent: ConfirmDialogModel = new ConfirmDialogModel(title, content);
    this.dialog.originalOpen(ConfirmDialogComponent, {
      width: '25vw',
      disableClose: true,
      hasBackdrop: true,
      data: dialogContent,
    }).afterClosed().subscribe((next: any) => {
      if (next.data === 'confirm') {
        this.configSchedule.deleteConfigSchedule(id).subscribe(data => {
          this.onSearchConfigSchedule({ offset: 0 });
          this.toastr.success(this.translateService.instant('common.attachment.delete.success'));
        });
      }
    });
  }

  changeDate(processTime: any, confirmTime: any) {
    if (processTime.value > confirmTime.value) {
      processTime.errors = {
        requireds: true
      };
      confirmTime.errors = {
        requireds: true
      };
      console.log(processTime.errors);
      console.log(confirmTime.errors);
    }
  }

  exportExcel(){
    this.configSchedule.export(this.configModel);
  }
}
