import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfigurationProcessingTermComponent } from './configuration-processing-term.component';

describe('ConfigurationProcessingTermComponent', () => {
  let component: ConfigurationProcessingTermComponent;
  let fixture: ComponentFixture<ConfigurationProcessingTermComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfigurationProcessingTermComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfigurationProcessingTermComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
