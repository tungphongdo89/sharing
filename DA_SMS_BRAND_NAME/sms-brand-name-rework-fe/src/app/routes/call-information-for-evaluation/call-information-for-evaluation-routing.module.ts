import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {CallInformationForEvaluationComponent} from '@app/routes/call-information-for-evaluation/call-information-for-evaluation/call-information-for-evaluation.component';

const routes: Routes = [
  {
    path: '',
    component: CallInformationForEvaluationComponent
  },
  {
    path: ':id',
    component: CallInformationForEvaluationComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CallInformationForEvaluationRoutingModule {
}
