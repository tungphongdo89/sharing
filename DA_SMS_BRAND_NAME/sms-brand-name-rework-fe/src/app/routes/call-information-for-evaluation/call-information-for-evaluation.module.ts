import {NgModule} from '@angular/core';
import {CommonModule, DatePipe} from '@angular/common';
import {SharedModule} from '@shared';
import {CallInformationForEvaluationComponent} from './call-information-for-evaluation/call-information-for-evaluation.component';
import {CallInformationForEvaluationRoutingModule} from '@app/routes/call-information-for-evaluation/call-information-for-evaluation-routing.module';
import {NgxMaterialTimepickerModule} from 'ngx-material-timepicker';


@NgModule({
  declarations: [CallInformationForEvaluationComponent],
    imports: [
        CommonModule,
        SharedModule,
        CallInformationForEvaluationRoutingModule,
        NgxMaterialTimepickerModule
    ],
  providers: [
    DatePipe
  ]
})
export class CallInformationForEvaluationModule {
}
