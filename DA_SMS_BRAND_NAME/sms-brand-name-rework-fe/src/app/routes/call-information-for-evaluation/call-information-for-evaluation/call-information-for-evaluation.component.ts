import {Component, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {CallInformationForEvaluationService} from '@core/services/call-information-for-evaluation/call-information-for-evaluation.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {TranslateService} from '@ngx-translate/core';
import {ToastrService} from 'ngx-toastr';
import {MtxGridColumn} from '@ng-matero/extensions';
import {ActivatedRoute, Router} from '@angular/router';
import {DatePipe} from '@angular/common';
import {NotificationService} from '@core/services/notification/notification.service';

@Component({
  selector: 'app-call-information-for-evaluation',
  templateUrl: './call-information-for-evaluation.component.html',
  styleUrls: ['./call-information-for-evaluation.component.scss']
})
export class CallInformationForEvaluationComponent implements OnInit {
  @ViewChild('index', {static: true}) index: TemplateRef<any>;
  columns: MtxGridColumn[] = [
    {
      i18n: 'No',
      width: '30px',
      field: 'no',
      cellTemplate: this.index
    },
    {i18n: 'search_work_list.column.reception', width: '150px', field: 'channelType'},
    {i18n: 'campaign_perform_information.Operator', width: '150px', field: 'operatorName'},
    {i18n: 'call_info_evaluation.call_date', width: '150px', field: 'createDate'},
    {i18n: 'campaign_perform_information.phone_number', width: '120px', field: 'phoneNumber'},
    {
      i18n: 'call_info_evaluation.call_duration', width: '100px', field: 'callDuration',
    },
    {i18n: 'call_info_evaluation.make_an_assessment', width: '120px', field: 'evaluateStatus'},
  ];
  lstSource = [];
  pageSizeList = [10, 50, 100];
  pageSize = 10;
  totalPage = 0;
  page: number = 0;
  idParam: any;
  lstAccountTDV = [];
  callInformation: any;
  form: FormGroup = this.fb.group({
    channelId: [{value: null, disabled: true}, [Validators.required]],
    operator: [{value: null, disabled: true}],
    phoneNumber: [null],
    callDuration: [null],
    startDate: [{value: null, disabled: true}],
    endDate: [{value: null, disabled: true}]
  });

  constructor(
    private callInformationForEvaluationService: CallInformationForEvaluationService,
    private fb: FormBuilder,
    private translateService: TranslateService,
    private toast: ToastrService,
    private route: ActivatedRoute,
    private datePipe: DatePipe,
    private router: Router
  ) {
  }

  ngOnInit(): void {
    this.init();
  }

  init() {
    this.route.paramMap.subscribe(async paramMap => {
      this.idParam = paramMap.get('id');
      this.callInformation = await this.getDataFromId();
      this.lstAccountTDV = await this.getLstTDV();
      this.form.patchValue(this.callInformation);
      this.search();
    });
  }

  getDataFromId() {
    return this.callInformationForEvaluationService.findCallInformationById(this.idParam).toPromise();
  }

  getLstTDV() {
    return this.callInformationForEvaluationService.findAllAccountTDV().toPromise();
  }

  search(reset?) {
    if (reset) {
      this.page = 0;
      this.pageSize = 10;
    }
    const data = this.form.getRawValue();
    data.size = this.pageSize;
    data.page = this.page;
    if (data.channelId === 1) { // goi vao
      data.create_date_from = this.datePipe.transform(data.startDate, 'yyyy-MM-dd');
      data.create_date_to = this.datePipe.transform(data.endDate, 'yyyy-MM-dd');
      this.callInformationForEvaluationService.searchInComingCall(data)
        .subscribe(res => {
          this.lstSource = res?.body;
          this.totalPage = res?.headers.get('X-Total-Count');
        });
    } else if (data.channelId === 2) { // goi ra
      data.call_time_from = this.datePipe.transform(data.startDate, 'yyyy-MM-dd');
      data.call_time_to = this.datePipe.transform(data.endDate, 'yyyy-MM-dd');
      this.callInformationForEvaluationService.searchOutGoingCall(data)
        .subscribe(res => {
          this.lstSource = res?.body;
          this.totalPage = res?.headers.get('X-Total-Count');
        });
    }
  }

  onPageChange(event) {
    this.page = event.pageIndex;
    this.pageSize = event.pageSize;
    this.search();
  }

  /**
   * Chuyển đến màn đánh giá
   */
  doEvaluate(row: any): void {
    const data: any = {};
    data.id_call = row.id;
    data.channel_id = this.callInformation.channelId;
    data.evaluater_id = this.callInformation.assign_user_id;
    data.evaluated_id = this.callInformation.operator;
    data.evaluate_detail_id = this.idParam;
    console.log(data);
    this.callInformationForEvaluationService.getIdFromProcess(data).subscribe(res => {
      if (res === -1){
        this.toast.error('',this.translateService.instant('not_enough_condition'));
      } else {
        this.router.navigate(['/evaluate/', res]);
      }
    });
  }
}
