import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {GroupCallOutComponent} from '@app/routes/group-call-out/group-call-out/group-call-out.component';
import {ListGroupMemberComponent} from '@app/routes/group-call-out/list-group-member/list-group-member.component';

const routes: Routes = [
  {
    path: '',
    component: GroupCallOutComponent
  },
  {
    path: ':id',
    component: ListGroupMemberComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GroupCallOutRoutingModule {
}
