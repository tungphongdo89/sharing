import {Component, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {FormBuilder, FormGroup, NgForm, Validators} from '@angular/forms';
import {MtxDialog, MtxGridColumn} from '@ng-matero/extensions';
import {GroupCallOutService} from '@core/services/group-call-out/group-call-out.service';
import {finalize} from 'rxjs/operators';
import {ToastrService} from 'ngx-toastr';
import {TranslateService} from '@ngx-translate/core';
import {ConfirmDialogComponent, ConfirmDialogModel} from '@shared/components/confirm-dialog/confirm-dialog.component';

@Component({
  selector: 'app-list-group-member',
  templateUrl: './list-group-member.component.html',
  styleUrls: ['./list-group-member.component.scss']
})
export class ListGroupMemberComponent implements OnInit {
  @ViewChild('index', {static: true}) index: TemplateRef<any>;
  @ViewChild('button', {static: true}) button: TemplateRef<any>;
  columns: MtxGridColumn[] = [
    {
      i18n: 'No',
      field: 'no',
      width: '7%',
      cellTemplate: this.index
    },
    {i18n: 'group_call_out.username', width: '28%', field: 'login'},
    {i18n: 'group_call_out.fullName', width: '28%', field: 'fullName'},
    {i18n: 'group_call_out.number', field: 'internalNumber'},
    {
      i18n: 'common.action',
      width: '10%',
      field: 'options',
      cellTemplate: this.button,
    }
  ];
  idParam: any;
  groupUserInfo: any;
  lstUserNotInThisGroup = [];
  lstUserInThisGroup = [];
  dataSource = [];
  pageSizeList = [10, 50, 100];
  pageSize = 10;
  totalPage = 0;
  page: number = 0;
  @ViewChild('ngForm') ngForm: NgForm;
  isLoading: boolean = true;
  isDisabled: boolean = true;
  form: FormGroup = this.fb.group({
    user: [null, [Validators.required]]
  });

  constructor(private activatedRoute: ActivatedRoute,
              private groupCallOutService: GroupCallOutService,
              private fb: FormBuilder,
              private toast: ToastrService,
              private translate: TranslateService,
              private dialog: MtxDialog) {
  }

  ngOnInit(): void {
    this.init();
  }

  init() {
    this.activatedRoute.paramMap.subscribe(async paramMap => {
      this.idParam = paramMap.get('id');
      this.groupCallOutService.getGroupUser(this.idParam).subscribe(res => {
        this.groupUserInfo = res;
      });
      this.fillData();
    });
  }

  fillData() {
    this.groupCallOutService.findAllUserNotInGroup(this.idParam)
      .subscribe(res => {
        this.lstUserNotInThisGroup = res;
      });
    this.groupCallOutService.findAllUserInGroup(this.idParam)
      .pipe(finalize(() => this.isLoading = false))
      .subscribe(res => {
        this.lstUserInThisGroup = this.dataSource = res;
      });
  }

  create() {
    if (this.form.invalid) {
      return;
    }
    const dataUser = this.form.get('user').value;
    const data = {groupId: this.idParam, userIds: dataUser};
    this.groupCallOutService.addUserToGroup(data)
      .subscribe(res => {
        this.toast.success(this.translate.instant('group_call_out.add_user_to_group_successfully'));
        this.fillData();
      });

  }


  onPageChange(event: any) {
    this.page = event.pageIndex;
    this.pageSize = event.pageSize;
  }

  delete(row) {
    const title = this.translate.instant('common.confirm.title.delete');
    const content = this.translate.instant('common.MSG0017') + ' ' + row.fullName;
    const dialogContent: ConfirmDialogModel = new ConfirmDialogModel(title, content);
    this.dialog.originalOpen(ConfirmDialogComponent, {
      width: '25vw',
      disableClose: true,
      hasBackdrop: true,
      data: dialogContent
    }).afterClosed().subscribe(result => {
      if (result.data === 'confirm') {
        this.groupCallOutService.deleteUserFromGroup({userId: row.id, groupId: this.idParam})
          .subscribe(res => {
            this.toast.success('', this.translate.instant('delete_successfully'));
            this.fillData();
          }, error => this.toast.error('', error.error.title));
      }
    });
  }

  filterSearch(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource = this.lstUserInThisGroup.filter((value: any) =>
      value.login?.toLowerCase().includes(filterValue.toLowerCase())
      || value.fullName?.toString().toLowerCase().includes(filterValue.toLowerCase()));
  }

}
