import {Component, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, NgForm, Validators} from '@angular/forms';
import {GroupCallOutService} from '@core/services/group-call-out/group-call-out.service';
import {TranslateService} from '@ngx-translate/core';
import {ToastrService} from 'ngx-toastr';
import {MtxDialog, MtxGridColumn} from '@ng-matero/extensions';
import {Router} from '@angular/router';
import {finalize} from 'rxjs/operators';
import {ConfirmDialogComponent, ConfirmDialogModel} from '@shared/components/confirm-dialog/confirm-dialog.component';

@Component({
  selector: 'app-group-call-out',
  templateUrl: './group-call-out.component.html',
  styleUrls: ['./group-call-out.component.scss']
})
export class GroupCallOutComponent implements OnInit {
  @ViewChild('index', {static: true}) index: TemplateRef<any>;
  @ViewChild('button', {static: true}) button: TemplateRef<any>;
  columns: MtxGridColumn[] = [
    {
      i18n: 'No',
      field: 'no',
      width: '100px',
      cellTemplate: this.index
    },
    {i18n: 'group_call_out.group_name', width: '60%', field: 'name'},
    {i18n: 'group_call_out.number_members', width: '30%', field: 'numberData'},
    {
      i18n: 'common.action',
      width: '250px',
      right: '0px',
      field: 'options',
      cellTemplate: this.button,
    }
  ];
  form: FormGroup = this.fb.group({
    name: [null, [Validators.required, Validators.maxLength(250)]],
  });
  dataSource = [];
  allDataSource = [];
  pageSizeList = [10, 50, 100];
  pageSize = 10;
  totalPage = 0;
  page: number = 0;
  @ViewChild('ngForm') ngForm: NgForm;
  isLoading: boolean = true;
  isDisabled: boolean = true;
  searchWord = '';
  selectedItem: any;

  constructor(private fb: FormBuilder,
              private groupCallOutService: GroupCallOutService,
              private translate: TranslateService,
              private toast: ToastrService,
              private router: Router,
              private dialog: MtxDialog) {
  }

  ngOnInit(): void {
    this.search();
  }

  search(reset?) {
    if (reset) {
      this.page = 0;
      this.pageSize = 10;
    }
    this.groupCallOutService.getAllGroupUser({page: this.page, size: this.pageSize})
      .pipe(finalize(() => this.isLoading = false))
      .subscribe(res => {
        this.dataSource = this.allDataSource = res.body;
        this.totalPage = res.headers.get('X-Total-Count');
      }, error => this.toast.error('', error));
  }

  onPageChange(event) {
    this.page = event.pageIndex;
    this.pageSize = event.pageSize;
    this.search();
  }

  filterSearch() {
    this.dataSource = this.allDataSource.filter((value: any) =>
      value.name?.toLowerCase().includes(this.searchWord.toLowerCase())
      || value.numberData?.toString().toLowerCase().includes(this.searchWord.toLowerCase()));
  }

  create() {
    this.groupCallOutService.createGroupUser(this.form.value)
      .subscribe(res => {
          this.toast.success('', this.translate.instant('successfully_added_new'));
          this.ngForm.resetForm();
          this.search();
        }, error => this.toast.error('', error.error.title)
      );
  }

  detail(row) {
    this.router.navigate(['/group-call-out/', row.id]);
  }

  update(row) {
    this.selectedItem = row;
    this.form.patchValue({name: row.name});
    this.isDisabled = false;
  }

  submit() {
    if (this.selectedItem?.name === this.form.get('name').value) {
      this.toast.success('', this.translate.instant('group_call_out.group_name_not_changed'));
      this.isDisabled = true;
      this.ngForm.resetForm();
      return;
    }
    const title = this.translate.instant('group_call_out.sure_update');
    const content = this.translate.instant('group_call_out.do_you_want_to_change_group_name')+' '+ this.selectedItem?.name
      +' '+ this.translate.instant('group_call_out.to') +' '+ this.form.get('name').value + '?';
    const dialogContent: ConfirmDialogModel = new ConfirmDialogModel(title, content);
    this.dialog.originalOpen(ConfirmDialogComponent, {
      width: '25vw',
      disableClose: true,
      hasBackdrop: true,
      data: dialogContent
    }).afterClosed().subscribe(result => {
      if (result.data === 'confirm') {
        this.groupCallOutService.updateGroupUser(this.form.value, this.selectedItem.id)
          .subscribe(res => {
            this.toast.success('', this.translate.instant('group_call_out.rename_successful'));
            this.isDisabled = true;
            this.ngForm.resetForm();
            this.search();
          }, error => this.toast.error('', error.error.title));
      }
    });
  }

  delete(row) {
    this.isDisabled = true;
    const title = this.translate.instant('common.confirm.title.delete');
    const content = this.translate.instant('common.MSG0017') + ' ' + row.name;
    const dialogContent: ConfirmDialogModel = new ConfirmDialogModel(title, content);
    this.dialog.originalOpen(ConfirmDialogComponent, {
      width: '25vw',
      disableClose: true,
      hasBackdrop: true,
      data: dialogContent
    }).afterClosed().subscribe(result => {
      if (result.data === 'confirm') {
        this.groupCallOutService.deleteGroupUser(row.id).subscribe(res => {
          this.toast.success('', this.translate.instant('delete_successfully'));
          this.search();
        }, error => this.toast.error('', error.error.title));
      }
    });
  }


}
