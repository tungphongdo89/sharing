import {NgModule} from '@angular/core';
import {CommonModule, DatePipe} from '@angular/common';
import {SharedModule} from '@shared';
import {NgxMaterialTimepickerModule} from 'ngx-material-timepicker';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {GroupCallOutComponent} from './group-call-out/group-call-out.component';
import {GroupCallOutRoutingModule} from '@app/routes/group-call-out/group-call-out-routing.module';
import { ListGroupMemberComponent } from './list-group-member/list-group-member.component';


@NgModule({
  declarations: [GroupCallOutComponent, ListGroupMemberComponent],
  imports: [
    CommonModule,
    SharedModule,
    GroupCallOutRoutingModule,
    NgxMaterialTimepickerModule,
    MatFormFieldModule,
    MatInputModule
  ],
  providers: [
    DatePipe
  ]
})
export class GroupCallOutModule {
}
