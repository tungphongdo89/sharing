import {Routes, RouterModule} from '@angular/router';
import {NgModule} from '@angular/core';
import {CpIndexComponent} from '@app/routes/cp-management/cp-index/cp-index.component';
import {CpBrandManagementComponent} from '@app/routes/cp-management/cp-brand-management/cp-brand-management.component';
import {CpBrandManagementCreateComponent} from '@app/routes/cp-management/cp-brand-management/cp-brand-management-create/cp-brand-management-create.component';
import {CpFormComponent} from '@app/routes/cp-management/cp-form/cp-form.component';
import {CpBrandManagementEditComponent} from '@app/routes/cp-management/cp-brand-management/cp-brand-management-edit/cp-brand-management-edit.component';

const routes: Routes = [
  {
    path: '',
    component: CpIndexComponent
  },
  {
    path: 'brand',
    component: CpBrandManagementComponent,
  },
  {
    path: 'brand/create',
    component: CpBrandManagementCreateComponent
  },
  {
    path: 'brand/edit',
    component: CpBrandManagementEditComponent
  },
  {
    path: 'new',
    component: CpFormComponent,
    data: {title: 'customer.addTitle'}
  },
  {
    path: 'edit/:id',
    component: CpFormComponent,
    data: {title: 'customer.editTitle'}
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CpManagementRouting {
}
