import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SharedModule} from '@shared/shared.module';
import {CpManagementRouting} from '@app/routes/cp-management/cp-management.routing';
import {CpFormComponent} from '@app/routes/cp-management/cp-form/cp-form.component';
import {CpIndexComponent} from '@app/routes/cp-management/cp-index/cp-index.component';
import {CpSearchComponent} from '@app/routes/cp-management/cp-search/cp-search.component';
import {CpBrandManagementComponent} from './cp-brand-management/cp-brand-management.component';
import {CpBrandManagementSearchComponent} from './cp-brand-management/cp-brand-management-search/cp-brand-management-search.component';
import {CpBrandManagementResultComponent} from './cp-brand-management/cp-brand-management-result/cp-brand-management-result.component';
import {CpBrandManagementCreateComponent} from './cp-brand-management/cp-brand-management-create/cp-brand-management-create.component';
import {ConfirmBusinessComponent} from './cp-brand-management/comfirm-business/confirm-business.component';
import {CpBrandManagementEditComponent} from './cp-brand-management/cp-brand-management-edit/cp-brand-management-edit.component';
import {AccountSearchComponent} from '@app/routes/cp-management/account-search/account-search.component';
import {TelesaleSearchComponent} from '@app/routes/cp-management/telesale-search/telesale-search.component';
import {DevSearchComponent} from '@app/routes/cp-management/dev-search/dev-search.component';
import {AdjustCommissionHisComponent} from './adjust-commission-his/adjust-commission-his.component';

@NgModule({
  declarations: [
    CpFormComponent,
    CpIndexComponent,
    CpSearchComponent,
    AccountSearchComponent,
    TelesaleSearchComponent,
    DevSearchComponent,
    CpBrandManagementComponent,
    CpBrandManagementSearchComponent,
    CpBrandManagementResultComponent,
    CpBrandManagementCreateComponent,
    ConfirmBusinessComponent,
    CpBrandManagementEditComponent,
    AdjustCommissionHisComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    CpManagementRouting,
  ],
  entryComponents: []
})
export class CpManagementModule {
}
