import {AfterViewInit, ChangeDetectorRef, Component, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {ToastrService} from 'ngx-toastr';
import {MtxDialog} from '@ng-matero/extensions';
import {CpService} from '@core/services/cp/cp.service';
import {TranslateService} from '@ngx-translate/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {COMMOM_CONFIG} from '@env/environment';
import {
  cpCodeValid,
  onlyCharacterValidator, passwordValid,
  phoneNumberValid,
  userBrandNameValid
} from '@shared/directives/onlyCharacters.directive';
import {ActivatedRoute} from '@angular/router';
import * as moment from 'moment';
import {FileValidator} from 'ngx-material-file-input';
import {AccountSearchComponent} from '@app/routes/cp-management/account-search/account-search.component';
import {DevSearchComponent} from '@app/routes/cp-management/dev-search/dev-search.component';
import {TelesaleSearchComponent} from '@app/routes/cp-management/telesale-search/telesale-search.component';
import {Observable} from 'rxjs';
import {Constant} from '@shared/Constant';
import {ConfirmDialogComponent, ConfirmDialogModel} from '@shared/components/confirm-dialog/confirm-dialog.component';
import {FileModel} from '@core/models/file-model.model';

@Component({
  selector: 'app-cp-form',
  templateUrl: './cp-form.component.html',
  styleUrls: ['./cp-form.component.scss']
})
export class CpFormComponent implements OnInit, AfterViewInit {
  cp: any;
  cpId: any;
  titlePopup: any;
  isSaving: any;
  attachFile: any;
  confirmExistedCp: any;
  monthCommissionAttachFile: any;
  currentCommissionCode: any;
  searchAccount = {id: null, userName: null};
  searchTeleSale = {staffCode: null, name: null};
  searchDev = {staffCode: null, name: null};
  currentWsPassword: any;
  currentPassSmpp: any;
  isAdvInfo = false;
  lstProvince = [];
  lstDistrict = [];
  lstCpParent = [];
  lstProject = [];
  lstCommissionCode = [];
  DEFAULT_SMS_BRAND = 'smsbrand_';
  CODE_REGEX = /^[a-zA-Z0-9_]{1,}$/;
  PHONE_REGEX = /^84\d{9,10}$/;
  SMS_BRAND_REGEX = /^smsbrand_+[a-zA-Z0-9_]{1,}$/;
  PASSWORD_REGEX = /^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&* ])[a-zA-Z0-9!@#$%^&* ]{8,50}$/;
  SMPP_PASSWORD_REGEX = /^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&* ])[a-zA-Z0-9!@#$%^&* ]{8,9}$/;
  CP_CODE_REGEX = /^[a-zA-Z0-9_]{1,}$/;
  IP_REGEX = /^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$/g;
  EMAIL_REGEX = /[A-Za-z0-9._%-]+@[A-Za-z0-9._%-]+\.[a-z]{2,3}$/;
  MAX_TRANSFER_LIMIT = 5000000;
  ALLOW_FILE_TYPE = '.7z,.rar,.zip,.txt,.ppt,.pptx,.doc,.docx,.xls,.xlsx,.pdf,.jpg,.jpeg,.png,.bmp,.gif';
  lstStatus = [
    {value: 1, label: 'customer.status1'},
    {value: 0, label: 'customer.status0'},
  ];
  lstChargeType = [
    {value: 'PRE', label: 'customer.pre'},
    {value: 'POS', label: 'customer.pos'},
  ];
  lstCurrency = [
    {value: 'VND', label: 'VND'},
    {value: 'USD', label: 'USD'},
  ];
  lstAccountType = [
    {value: 0, label: 'Basic'},
    {value: 1, label: 'Limit'},
  ];
  lstLbsPr = [
    {value: 0, label: 'customer.offLbs'},
    {value: 1, label: 'customer.onLbs'},
  ];
  formGroup: FormGroup = this.fb.group({
    chargeType: ['PRE', Validators.required],
    currency: ['VND', Validators.required],
    limitedDays: [0, [Validators.max(999), Validators.maxLength(3)]],
    email: [null, [Validators.required, Validators.maxLength(100), Validators.pattern(COMMOM_CONFIG.EMAIL_FORMAT)]],
    cpCode: [null, [Validators.required, cpCodeValid(this.CP_CODE_REGEX)]],
    cpName: [null, [Validators.required, Validators.maxLength(200)]],
    taxCode: [null, [Validators.required, Validators.maxLength(50)]],
    representative: [null, [Validators.required, Validators.maxLength(100)]],
    provinceId: ['-1', Validators.required],
    districtId: [null],
    address: [null, [Validators.required, Validators.maxLength(500)]],
    parentCpId: ['-1'],
    warnContact: [null, [Validators.required, Validators.maxLength(12), phoneNumberValid(this.PHONE_REGEX)]],
    orderNo: [null, [Validators.required, Validators.maxLength(100)]],
    beginDate: [moment().toDate(), [Validators.required]],
    expireDate: [null],
    userName: [this.DEFAULT_SMS_BRAND, [Validators.required, Validators.maxLength(50), userBrandNameValid(this.SMS_BRAND_REGEX)]],
    password: [null, [passwordValid(this.PASSWORD_REGEX), Validators.maxLength(50)]],
    wsUsername: [null, [Validators.maxLength(50), onlyCharacterValidator(this.CODE_REGEX)]],
    wsPassword: [null, [Validators.maxLength(50), passwordValid(this.PASSWORD_REGEX)]],
    cpSysid: [null, [Validators.maxLength(50), onlyCharacterValidator(this.CODE_REGEX)]],
    passSmpp: [null, [Validators.maxLength(9), passwordValid(this.SMPP_PASSWORD_REGEX)]],
    tps: [20, [Validators.max(100), Validators.min(20), Validators.maxLength(3)]],
    ip: [null, [Validators.maxLength(500), Validators.pattern(this.IP_REGEX)]],
    accountType: [0],
    status: [1],
    freeSms: [0, [Validators.maxLength(10), Validators.min(0)]],
    topupLimit: [10000000, [Validators.maxLength(20), Validators.min(0)]],
    warnBalance: [150000, [Validators.maxLength(20), Validators.min(0)]],
    adWarnBalance: [0, [Validators.maxLength(20), Validators.min(0)]],
    alertSmstotal: [0, [Validators.maxLength(18), Validators.min(0)]],
    alertEmail: [null, [Validators.maxLength(2000), Validators.pattern(this.EMAIL_REGEX)]],
    alertTel: [null, [Validators.maxLength(2000)]],
    realtimePermit: [0],
    transferLimit: [this.MAX_TRANSFER_LIMIT, [Validators.min(0), Validators.maxLength(17)]],
    projectCode: ['-1'],
    projectName: [null],
    approveType: [null],
    sendWithOther: [null],
    allowBiProg: [null],
    notFilter: [null],
    filterVip: [null],
    filter1313: [null],
    syncRevenueBccs: [null],
    cpinfo: [null, [Validators.required, Validators.maxLength(500)]],
    monthCommission: [12, [Validators.required, Validators.maxLength(2), Validators.min(0), Validators.max(99)]],
    monthCommissionNote: [null, Validators.maxLength(450)],
    monthCommissionAttachFile: [null],
    commissionPercentCode: ['D0'],
    attachFile: [null],
    staffEmail: [null, [Validators.maxLength(100), Validators.pattern(COMMOM_CONFIG.EMAIL_FORMAT), Validators.required]],
    amPercent: [100, [Validators.maxLength(3), Validators.min(0), Validators.max(100)]],
    reason: null
  });

  constructor(
    protected toastr: ToastrService,
    public dialog: MtxDialog,
    private cpService: CpService,
    private fb: FormBuilder,
    protected translateService: TranslateService,
    private route: ActivatedRoute,
    private cdr: ChangeDetectorRef
  ) {
  }

  ngAfterViewInit(): void {
    if (!this.cpId) {
      document.getElementById('PRE').focus();
      this.cdr.detectChanges();
    }
  }

  ngOnInit(): void {
    this.prepareData();
  }

  prepareData() {
    this.cpId = this.route.snapshot.params.id;
    if (this.cpId) {
      this.titlePopup = 'customer.editTitle';
      this.isSaving = false;
      this.findCpById(this.cpId);
      this.disableControls();
    } else {
      this.titlePopup = 'customer.addTitle';
      this.isSaving = true;
      this.formGroup.get('limitedDays').disable();
      this.setFormValidators();
    }
    this.findAllProvince();
    this.findAllCpParent();
    this.findCommissionCode();
    this.findBccsProject();
  }

  save() {
    this.formGroup.markAllAsTouched();
    this.validProvince();
    if (this.formGroup.invalid) {
      this.scrollToErrorControls();
      return;
    }
    if (!this.attachFile) {
      this.toastr.warning(this.translateService.instant('customer.attachFileRequired'));
      const element = document.getElementById('file-control-2').getElementsByClassName('uploadFile')[0];
      (element as HTMLAnchorElement).focus();
      return;
    }
    this.checkExistedOrderNoOrCpName(this.isSaving).subscribe((res: any) => {
      if (Constant.OK === res.status.code && res.data) {
        this.openDialogConfirmExistedCp();
      } else {
        this.isSaving ? this.doAdd() : this.doUpdate();
      }
    });

  }

  doAdd() {
    const rawValues = this.formGroup.getRawValue();
    const attachFile = this.attachFile;
    const monthCommissionAttachFile = this.monthCommissionAttachFile;
    const cpEntity = this.setValueCheckBoxAndFile(rawValues);
    if (this.confirmExistedCp) {
      cpEntity.commissionPercentCode = 'C0';
      cpEntity.confirmExistedCp = true;
    }
    console.log('cpEntity', cpEntity);
    this.cpService.add(cpEntity, attachFile, monthCommissionAttachFile).subscribe((res: any) => {
        console.log('res', res);
        if (Constant.OK !== res.status.code) {
          this.setControlsErr(res.status);
        } else this.toastr.success(this.translateService.instant('customer.addSuccess'));
      }, () => this.showUnknownErr()
    );
  }

  doUpdate() {
    const rawValues = this.formGroup.getRawValue();
    console.log('rawValues', rawValues);
    if (rawValues.wsPassword) rawValues.isWsPassChange = true;
    else {
      rawValues.wsPassword = this.currentWsPassword;
      rawValues.isWsPassChange = false;
    }
    rawValues.passSmpp ? rawValues.passSmpp = rawValues.passSmpp : rawValues.passSmpp = this.currentPassSmpp;
    const attachFile = this.attachFile;
    const monthCommissionAttachFile = this.monthCommissionAttachFile;
    const cpEntity = this.setValueCheckBoxAndFile(rawValues);
    if (this.confirmExistedCp) {
      cpEntity.commissionPercentCode = 'C0';
      cpEntity.confirmExistedCp = true;
    }
    if (cpEntity.commissionPercentCode !== this.currentCommissionCode) cpEntity.oldValueCommissionPercentCode = this.currentCommissionCode;
    rawValues.cpId = this.cpId;
    console.log('cpEntity', cpEntity);
    this.cpService.update(cpEntity, attachFile, monthCommissionAttachFile).subscribe((res: any) => {
        console.log('res', res);
        if (Constant.OK !== res.status.code) {
          this.setControlsErr(res.status);
        } else this.toastr.success(this.translateService.instant('customer.editSuccess'));
      }
      ,
      () => this.showUnknownErr()
    );
  }

  advInfoClick() {
    this.isAdvInfo = !this.isAdvInfo;
  }

  onProvinceChange(event: any) {
    this.resetAfterChangeProvince();
    this.validProvince();
    if (!event) return;
    this.cpService.findDistrictByProvinceId(event.value).subscribe((res: any) => {
        this.lstDistrict = res;
      }, () => this.showUnknownErr()
    );
  }

  resetAfterChangeProvince() {
    this.lstDistrict = [];
    this.formGroup.get('districtId').reset();
  }

  findAllProvince() {
    this.cpService.findAllProvince().subscribe((res: any) => {
        if (res && res.length > 0) {
          this.lstProvince = res.sort((a, b) => a.provinceName > b.provinceName ? 1 : -1);
        }
      }, () => this.showUnknownErr()
    );
  }

  findCommissionCode() {
    this.cpService.findCommissionPercentCode().subscribe((res: any) => {
        if (res && res.length > 0) {
          this.lstCommissionCode = res;
        }
      }, () => this.showUnknownErr()
    );
  }

  findAllCpParent() {
    const options = {
      page: 0,
      pageSize: 99999,
      sendWithOther: 1
    };
    this.cpService.search(options).subscribe((res: any) => {
      if (res && Constant.OK === res.status.code) {
        this.lstCpParent = res.data.listData;
      }
    }, () => this.showUnknownErr());
  }

  setControlsErr(data: any) {
    if (data.code === '104' || data.code === '105') {
      this.setFileErrors();
    } else {
      const err = {};
      err[`${data.code}`] = true;
      const formControl = data.message;
      this.formGroup.get(formControl).setErrors(err);
      this.scrollToErrorControls();
    }
  }

  setFileErrors() {
    const arrFileType = this.ALLOW_FILE_TYPE.split(',');
    console.log('arrFileType', arrFileType);
    if (typeof this.attachFile === 'object') {
      const fileName = this.attachFile.name;
      const fileType = fileName.substring(fileName.lastIndexOf('.'));
      const isValid = arrFileType.includes(fileType);
      if (!isValid) {
        this.toastr.warning(this.translateService.instant('validation.pattern', {field: this.translateService.instant('customer.attachDocx')}));
        const element = document.getElementById('file-control-2').getElementsByClassName('uploadFile')[0];
        (element as HTMLAnchorElement).focus();
      }
    }
    if (typeof this.monthCommissionAttachFile === 'object') {
      const fileName = this.monthCommissionAttachFile.name;
      const fileType = fileName.substring(fileName.lastIndexOf('.'));
      const isValid = arrFileType.includes(fileType);
      if (!isValid) {
        this.toastr.warning(this.translateService.instant('validation.pattern', {field: this.translateService.instant('customer.attachDocxHH')}));
        const element = document.getElementById('file-control-1').getElementsByClassName('uploadFile')[0];
        (element as HTMLAnchorElement).focus();
      }
    }
  }

  validDate() {
    const today = moment().format('YYYY-MM-DD').toString();
    const expireDate = this.formGroup.get('expireDate').value ? moment(this.formGroup.get('expireDate').value).format('YYYY-MM-DD').toString() : null;
    const beginDate = this.formGroup.get('beginDate').value ? moment(this.formGroup.get('beginDate').value).format('YYYY-MM-DD').toString() : null;
    if (moment(today).isSameOrAfter(expireDate)) this.formGroup.get('expireDate').setErrors({isAfterToday: true});
    if (moment(beginDate).isSameOrAfter(expireDate)) this.formGroup.get('expireDate').setErrors({isSmallThanBegin: true});
  }

  validUserName() {
    const userNameControl = this.formGroup.get('userName');
    if (userNameControl.value !== null && userNameControl.value.length < 10) userNameControl.setErrors({isMinMax: true});
    if (userNameControl.value) {
      const arrSplit = userNameControl.value.split('smsbrand_');
      console.log('arrSplit', arrSplit);
      console.log('formGroup', this.formGroup);
    }
  }

  validAlertSmstotal() {
    const alertSmstotal = this.formGroup.get('alertSmstotal').value ? Number(this.formGroup.get('alertSmstotal').value) : null;
    const alertEmail = this.formGroup.get('alertEmail').value;
    const alertTel = this.formGroup.get('alertTel').value;
    if (alertSmstotal !== null && alertSmstotal > 0) {
      this.formGroup.get('alertEmail').setErrors(!alertEmail ? {required: true} : null);
      this.formGroup.get('alertTel').setErrors(!alertTel ? {required: true} : null);
      this.formGroup.markAllAsTouched();
      console.log('formGroup', this.formGroup);
    } else {
      this.formGroup.get('alertEmail').setErrors(null);
      this.formGroup.get('alertTel').setErrors(null);
      this.formGroup.markAllAsTouched();
    }
  }

  validAlertEmail() {
    const alertEmail = this.formGroup.get('alertEmail').value;
    const arrEmail = alertEmail ? alertEmail.split(',') : [];
    console.log('arrEmail', arrEmail);
    if (arrEmail.length > 0) {
      arrEmail.forEach(obj => {
        const flag = this.EMAIL_REGEX.test(obj);
        console.log('flag', flag);
        if (!flag) this.formGroup.get('alertEmail').setErrors({pattern: true});
      });
    }
  }

  validAlertTel() {
    const alertTel = this.formGroup.get('alertTel').value;
    const arrTel = alertTel ? alertTel.split(',') : [];
    if (arrTel.length > 0) {
      arrTel.forEach(obj => {
        const flag = this.PHONE_REGEX.test(obj);
        if (!flag) this.formGroup.get('alertTel').setErrors({pattern: true});
      });
    }
  }

  validMonthCommission() {
    const monthCommissionControl = this.formGroup.get('monthCommission');
    const monthCommissionNoteControl = this.formGroup.get('monthCommissionNote');
    const monthCommissionValue = monthCommissionControl.value ? Number(monthCommissionControl.value) : null;
    if (monthCommissionValue !== 12) {
      monthCommissionNoteControl.setErrors({required: true});
      this.formGroup.markAllAsTouched();
    } else {
      monthCommissionNoteControl.setErrors(null);
    }
  }

  onChangeCurrency(event: any) {
    if (!event) return;
    const currency = event.value;
    currency === 'USD' ? this.formGroup.get('transferLimit').setValue(250) : this.formGroup.get('transferLimit').setValue(this.MAX_TRANSFER_LIMIT);
  }

  setValueCheckBoxAndFile(data: any) {
    const arrControls = ['approveType', 'sendWithOther', 'allowBiProg', 'notFilter', 'filterVip', 'filter1313', 'syncRevenueBccs'];
    arrControls.forEach(obj => {
      const value = data[`${obj}`];
      !value ? data[`${obj}`] = 0 : data[`${obj}`] = 1;
    });
    if (!this.isSaving) {
      data.monthCommissionAttachFile = typeof this.monthCommissionAttachFile === 'object' && this.monthCommissionAttachFile ? this.monthCommissionAttachFile.name : this.monthCommissionAttachFile;
      data.attachFile = typeof this.attachFile === 'object' && this.attachFile ? this.attachFile.name : this.attachFile;
    } else {
      data.monthCommissionAttachFile = null;
      data.attachFile = null;
    }
    data.provinceUserId = this.searchAccount.id ? this.searchAccount.id : null;
    data.staffCode = this.searchDev.staffCode ? this.searchDev.staffCode : null;
    data.staffName = this.searchDev.name ? this.searchDev.name : null;
    data.telesaleName = this.searchTeleSale.name ? this.searchTeleSale.name : null;
    data.telesaleCode = this.searchTeleSale.staffCode ? this.searchTeleSale.staffCode : null;
    data.cpCode = data.cpCode ? data.cpCode.toUpperCase() : null;
    data.wsUsername = data.wsUsername ? data.wsUsername.toLowerCase() : null;
    data.userName = data.userName ? data.userName.toLowerCase() : null;
    data.cpSysid = data.cpSysid ? data.cpSysid.toLowerCase() : null;
    return data;
  }

  findCpById(id: any) {
    if (!id) return;
    this.cpService.findCpById(id).subscribe((res: any) => {
      console.log('res', res);
      if (Constant.OK === res.status.code) {
        this.cp = res.data;
        this.patchValueForm(this.cp);
      } else {
        this.showErrMessage(res.status.message);
      }
    }, () => this.showUnknownErr());
  }

  patchValueForm(cp: any) {
    const formGroupControls = this.formGroup.controls;
    this.isAdvInfo = true;
    const data = this.convertCheckBoxValue(cp);
    this.formGroup.get('attachFile').setValue(FileModel.setFile([data.attachFile] || []));
    this.formGroup.get('monthCommissionAttachFile').setValue(FileModel.setFile([data.monthCommissionAttachFile] || []));
    if (data.attachFile) delete formGroupControls.attachFile;
    if (data.monthCommissionAttachFile) delete formGroupControls.monthCommissionAttachFile;
    this.formGroup.patchValue(data);
    this.currentWsPassword = data.wsPassword;
    this.currentPassSmpp = data.passSmpp;
    this.formGroup.get('wsPassword').reset();
    this.formGroup.get('passSmpp').reset();
    this.searchAccount.userName = data.provinceUserName;
    this.searchAccount.id = data.provinceUserId;
    this.searchDev.name = data.staffName;
    this.searchDev.staffCode = data.staffCode;
    this.searchTeleSale.name = data.telesaleName;
    this.searchTeleSale.staffCode = data.telesaleCode;
    this.attachFile = data.attachFile;
    this.monthCommissionAttachFile = data.monthCommissionAttachFile;
    this.currentCommissionCode = data.commissionPercentCode;
    Object.assign(formGroupControls, {attachFile: new FormControl(null, FileValidator.maxContentSize(20971520))});
    Object.assign(formGroupControls, {monthCommissionAttachFile: new FormControl(null, FileValidator.maxContentSize(20971520))});
    if (data.districtId) {
      this.cpService.findDistrictByProvinceId(data.provinceId).subscribe((res: any) => {
          this.lstDistrict = res;
          this.formGroup.get('districtId').setValue(data.districtId);
        }, () => this.showUnknownErr()
      );
    }
    if (data.chargeType) {
      if (data.chargeType === 'PRE') {
        this.formGroup.get('limitedDays').disable();
        this.formGroup.get('syncRevenueBccs').enable();
      } else {
        this.formGroup.get('syncRevenueBccs').disable();
      }
      document.getElementById(`${data.chargeType}`).focus();
      this.cdr.detectChanges();
    }
  }

  convertCheckBoxValue(data: any) {
    const arrControls = ['approveType', 'sendWithOther', 'allowBiProg', 'notFilter', 'filterVip', 'filter1313', 'syncRevenueBccs'];
    arrControls.forEach(obj => {
      const value = data[`${obj}`];
      value && value === 1 ? data[`${obj}`] = true : data[`${obj}`] = false;
    });
    return data;
  }

  showErrMessage(err: any) {
    this.toastr.warning(err);
  }

  showUnknownErr() {
    this.toastr.error(this.translateService.instant('common.notify.fail'));
  }

  setFormValidators() {
    const formControls = this.formGroup.controls;
    delete formControls.password;
    this.formGroup.get('attachFile').setValidators(FileValidator.maxContentSize(20971520));
    this.formGroup.get('monthCommissionAttachFile').setValidators(FileValidator.maxContentSize(20971520));
    Object.assign(formControls, {password: new FormControl(null, [Validators.required, passwordValid(this.PASSWORD_REGEX), Validators.maxLength(50)])});
  }

  scrollToErrorControls() {
    const element = document.getElementById('errorControls');
    element.scrollIntoView({block: 'center'});
  }

  openSearchAccountDialog() {
    const ref = this.dialog.open({
      disableClose: true,
      hasBackdrop: true,
      width: '60vw',
      height: '80vh',
      maxWidth: '60vw',
      data: {lstProvince: this.lstProvince}
    }, AccountSearchComponent);
    ref.afterClosed().subscribe((res: any) => {
      console.log('res close', res);
      if (res) this.searchAccount = res;
    });
  }

  openSearchDevDialog() {
    const ref = this.dialog.open({
      disableClose: true,
      hasBackdrop: true,
      width: '60vw',
      height: '80vh',
      maxWidth: '60vw',
    }, DevSearchComponent);
    ref.afterClosed().subscribe((res: any) => {
      console.log('res close', res);
      if (res) this.searchDev = res;
    });
  }

  openSearchTeleSaleDialog() {
    const ref = this.dialog.open({
      disableClose: true,
      hasBackdrop: true,
      width: '60vw',
      height: '80vh',
      maxWidth: '60vw',
    }, TelesaleSearchComponent);
    ref.afterClosed().subscribe((res: any) => {
      console.log('res close', res);
      if (res) this.searchTeleSale = res;
    });
  }

  cancelChoose(idx: any) {
    if (idx === 1) this.searchAccount = null;
    else if (idx === 2) this.searchDev = null;
    else this.searchTeleSale = null;
  }

  downloadFiles(fileName: string) {
    if (!fileName) return;
    return this.cpService.downloadCpFiles(fileName, fileName);
  }

  onFileChange(event: any, idx: number) {
    console.log('event', event);
    if (!event) return;
    if (idx === 1) {
      this.attachFile = event.length > 0 ? event[0] : null;
      if (!this.attachFile) this.toastr.warning(this.translateService.instant('customer.attachFileRequired'));
    } else {
      this.monthCommissionAttachFile = event.length > 0 ? event[0] : null;
    }
    // idx === 1 ? this.attachFile = null : this.monthCommissionAttachFile = null;
  }

  validApiUserAndSmppUser(idx: number) {
    if (idx === 1) {
      const apiUsername = this.formGroup.get('wsUsername').value;
      const wsPasswordControl = this.formGroup.get('wsPassword');
      if (apiUsername && !wsPasswordControl.value) wsPasswordControl.setErrors({required: true});
      else {
        const length = wsPasswordControl.errors ? Object.keys(wsPasswordControl.errors).length : 0;
        if (length <= 1 && wsPasswordControl.errors && wsPasswordControl.errors.required) wsPasswordControl.setErrors(null);
        else delete wsPasswordControl.errors.required;
      }
    } else {
      const cpSysid = this.formGroup.get('cpSysid').value;
      const passSmppControl = this.formGroup.get('passSmpp');
      if (cpSysid && !passSmppControl.value) passSmppControl.setErrors({required: true});
      else {
        const length = passSmppControl.errors ? Object.keys(passSmppControl.errors).length : 0;
        if (length <= 1 && passSmppControl.errors && passSmppControl.errors.required) passSmppControl.setErrors(null);
        else delete passSmppControl.errors.required;
      }
    }
    this.formGroup.markAllAsTouched();
  }

  checkExistedOrderNoOrCpName(flag: any): Observable<any> {
    const options = {
      taxCode: this.formGroup.get('taxCode').value ? this.formGroup.get('taxCode').value : null,
      cpName: this.formGroup.get('cpName').value ? this.formGroup.get('cpName').value : null,
      isSaving: flag,
      cpId: null
    };
    options.cpId = !this.isSaving ? this.cpId : null;
    return this.cpService.checkExistedOrderNoOrCpName(options);
  }

  openDialogConfirmExistedCp() {
    const content = this.translateService.instant('customer.existedConfirmContent');
    const title = this.translateService.instant('common.notification');
    const dialogData = new ConfirmDialogModel(title, content);
    const ref = this.dialog.originalOpen(ConfirmDialogComponent, {
      disableClose: true,
      hasBackdrop: true,
      data: dialogData
    });
    ref.afterClosed().subscribe((res: any) => {
      if (res) {
        this.confirmExistedCp = res;
        this.isSaving ? this.doAdd() : this.doUpdate();
      }
    });
  }

  findBccsProject() {
    this.cpService.findBccsProjects().subscribe((res: any) => {
      console.log('res', res);
      if (Constant.OK === res.status.code) {
        this.lstProject = res.data;
      }
    });
  }

  onProjectChange(event: any) {
    if (!event) return;
    const projectName = this.lstProject.length > 0 && this.lstProject.find(obj => obj.value === event.value) ? this.lstProject.find(obj => obj.value === event.value).label : null;
    this.formGroup.get('projectName').setValue(projectName);
  }

  onChargeTypeChange(event: any) {
    if (!event) return;
    const chargeTypeControl = this.formGroup.get('limitedDays');
    const syncRevenueBccsControl = this.formGroup.get('syncRevenueBccs');
    if (event.value === 'PRE') {
      chargeTypeControl.disable();
      chargeTypeControl.setValue('0');
      syncRevenueBccsControl.enable();
    } else {
      chargeTypeControl.enable();
      syncRevenueBccsControl.disable();
    }
  }

  validProvince() {
    const provinceControl = this.formGroup.get('provinceId');
    if (provinceControl.value === '-1') {
      provinceControl.setErrors({required: true});
    }
  }

  disableControls() {
    const arrControls = ['chargeType', 'currency', 'cpCode', 'userName', 'wsUsername', 'cpSysid'];
    arrControls.forEach(obj => {
      this.formGroup.get(obj).disable();
    });
  }
}
