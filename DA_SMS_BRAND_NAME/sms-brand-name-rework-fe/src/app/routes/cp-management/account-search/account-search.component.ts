import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MtxGridColumn} from '@ng-matero/extensions';
import {CpService} from '@core/services/cp/cp.service';
import {finalize} from 'rxjs/operators';
import {TranslateService} from '@ngx-translate/core';
import {ToastrService} from 'ngx-toastr';
import {Constant} from '@shared/Constant';

@Component({
  selector: 'app-account-search',
  templateUrl: './account-search.component.html',
  styleUrls: ['./account-search.component.scss']
})
export class AccountSearchComponent implements OnInit {
  data = [];
  listAction = [];
  lstProvince = [];
  columns: MtxGridColumn[] = [];
  isLoading = false;
  pageSize = 10;
  startRecord = 0;
  totalRecord = 0;
  pageSizeList = [10, 50, 100];
  pageIndex = 0;
  isAdvSearch = false;

  formSearch: FormGroup = this.fb.group({
    provinceId: '-1',
    userName: [null, Validators.maxLength(50)]
  });

  constructor(
    private cpService: CpService,
    private dialogRef: MatDialogRef<AccountSearchComponent>,
    private fb: FormBuilder,
    protected translateService: TranslateService,
    protected toastr: ToastrService,
    @Inject(MAT_DIALOG_DATA) public dataDialog?: any,
  ) {
    this.initAction();
  }

  ngOnInit(): void {
    if (this.dataDialog.data.lstProvince !== null) this.lstProvince = this.dataDialog.data.lstProvince;
    this.columns = [
      {i18n: 'common.orderNumber', field: 'no', width: '80px'},
      {i18n: 'Username', field: 'userName'},
      {
        i18n: 'common.action',
        field: 'option',
        width: '100px',
        pinned: 'right',
        right: '0px',
        type: 'button',
        buttons: this.listAction
      },
    ];
    this.search();
  }

  search() {
    this.isAdvSearch = true;
    this.onSearch();
  }

  pageChange(event: any) {
    this.pageIndex = event.pageIndex;
    this.startRecord = event.pageIndex === 0 ? event.pageIndex : ((event.pageIndex * event.pageSize));
    this.pageSize = event.pageSize;
    this.onSearch();
  }

  initAction() {
    this.listAction.push({
      icon: 'person_add',
      tooltip: 'Choose',
      color: 'basic',
      type: 'icon',
      click: record => this.onChooseAccount(record)
    });
  }

  showUnknownErr() {
    this.toastr.error(this.translateService.instant('common.notify.fail'));
  }

  onSearch() {
    const options = {};
    this.isLoading = true;
    const provinceId = this.formSearch.get('provinceId').value;
    const userName = this.formSearch.get('userName').value;
    this.isAdvSearch ? this.pageIndex = 0 : null;
    options['provinceId'] = provinceId && provinceId !== '-1' ? provinceId : null;
    options['userName'] = userName ? userName : null;
    options['page'] = !this.isAdvSearch ? this.startRecord : 0;
    options['pageSize'] = this.pageSize;
    console.log('options', options);
    this.cpService.searchProvinceUser(options).pipe(finalize(() => {
      this.isLoading = false;
      this.isAdvSearch = false;
    })).subscribe((res: any) => {
      console.log('res', res);
      if (Constant.OK === res.status.code) {
        this.data = res.data.listData;
        this.totalRecord = res.data.count;
      } else {
        this.data = [];
        this.totalRecord = 0;
      }
    }, () => this.showUnknownErr());
  }

  onChooseAccount(data: any) {
    this.dialogRef.close(data);
  }
}
