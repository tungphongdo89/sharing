import {Component, Inject, OnInit} from '@angular/core';
import {MtxGridColumn} from '@ng-matero/extensions';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {CpService} from '@core/services/cp/cp.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {TranslateService} from '@ngx-translate/core';
import {ToastrService} from 'ngx-toastr';
import {finalize} from 'rxjs/operators';
import {Constant} from '@shared/Constant';

@Component({
  selector: 'app-dev-search',
  templateUrl: './dev-search.component.html',
  styleUrls: ['./dev-search.component.scss']
})
export class DevSearchComponent implements OnInit {

  data = [];
  listAction = [];
  columns: MtxGridColumn[] = [];
  isLoading = false;
  pageSize = 10;
  startRecord = 0;
  totalRecord = 0;
  pageSizeList = [10, 50, 100];
  pageIndex = 0;
  isAdvSearch = false;

  formSearch: FormGroup = this.fb.group({
    staffCode: [null, Validators.maxLength(40)],
    name: [null, Validators.maxLength(100)]
  });

  constructor(
    private cpService: CpService,
    private dialogRef: MatDialogRef<DevSearchComponent>,
    private fb: FormBuilder,
    protected translateService: TranslateService,
    protected toastr: ToastrService,
    @Inject(MAT_DIALOG_DATA) public dataDialog?: any,
  ) {
    this.initAction();
  }

  ngOnInit(): void {
    this.columns = [
      {i18n: 'common.orderNumber', field: 'no', width: '80px'},
      {i18n: 'customer.staffCode', field: 'staffCode'},
      {i18n: 'customer.staffName', field: 'name'},
      {
        i18n: 'common.action',
        field: 'option',
        width: '100px',
        pinned: 'right',
        right: '0px',
        type: 'button',
        buttons: this.listAction
      },
    ];
    this.search();
  }

  pageChange(event: any) {
    this.pageIndex = event.pageIndex;
    this.startRecord = event.pageIndex === 0 ? event.pageIndex : ((event.pageIndex * event.pageSize));
    this.pageSize = event.pageSize;
    this.onSearch();
  }

  onSearch() {
    const options = {};
    this.isLoading = true;
    this.isAdvSearch ? this.pageIndex = 0 : null;
    const staffCode = this.formSearch.get('staffCode').value;
    const name = this.formSearch.get('name').value;
    options['page'] = !this.isAdvSearch ? this.startRecord : 0;
    options['pageSize'] = this.pageSize;
    options['staffCode'] = staffCode ? staffCode : null;
    options['name'] = name ? name : null;
    console.log('options', options);
    this.cpService.searchStaff(options).pipe(finalize(() => {
      this.isLoading = false;
      this.isAdvSearch = false;
    })).subscribe((res: any) => {
      console.log('res', res);
      if (Constant.OK === res.status.code) {
        this.data = res.data.listData;
        this.totalRecord = res.data.count;
      } else {
        this.data = [];
        this.totalRecord = 0;
      }
    }, () => this.showUnknownErr());
  }

  search() {
    this.isAdvSearch = true;
    this.onSearch();
  }

  showUnknownErr() {
    this.toastr.error(this.translateService.instant('common.notify.fail'));
  }

  initAction() {
    this.listAction.push({
      icon: 'person_add',
      tooltip: 'Choose',
      color: 'basic',
      type: 'icon',
      click: record => this.onChooseDev(record)
    });
  }

  onChooseDev(data: any) {
    this.dialogRef.close(data);
  }
}
