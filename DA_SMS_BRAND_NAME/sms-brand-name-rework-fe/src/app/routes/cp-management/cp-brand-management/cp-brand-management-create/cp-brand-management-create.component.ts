import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {CpBrandManagementService} from '@core/services/cp/cp-brand-management.service';
import {Constant} from '@shared/Constant';
import {ComboboxModel} from '@core/models/combobox.model';
import {TelcosSelectBoxModel} from '@core/models/cp-brand-management/telcos-select-box.model';
import {ValidationService} from '@shared/common/validation.service';
import {Router} from '@angular/router';
import {DateAdapter, ErrorStateMatcher, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';
import {MomentDateAdapter} from '@angular/material-moment-adapter';
import {CpAliasModel} from '@core/models/cp-brand-management/cp-alias.model';
import {MtxDialog} from '@ng-matero/extensions';
import {ConfirmBusinessComponent} from '@app/routes/cp-management/cp-brand-management/comfirm-business/confirm-business.component';
import {ToastrService} from 'ngx-toastr';
import {TranslateService} from '@ngx-translate/core';
import {DatePipe} from '@angular/common';


export const MY_FORMATS_DATE = {
  parse: {
    dateInput: 'DD/MM/YYYY',
  },
  display: {
    dateInput: 'DD/MM/YYYY',
    monthYearLabel: 'DD MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'DD MMMM YYYY',
  },
};

@Component({
  selector: 'app-cp-brand-management-create',
  templateUrl: './cp-brand-management-create.component.html',
  styleUrls: ['./cp-brand-management-create.component.scss'],
  providers: [DatePipe,
    {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
    {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS_DATE,},

  ],
})


export class CpBrandManagementCreateComponent implements OnInit {

  @ViewChild('brandName') brandName: ElementRef;
  disableCreate: boolean;
  cpData: any;
  formCreate: FormGroup;
  aliasGroups: ComboboxModel[] = [];
  mainWSs: ComboboxModel[] = [];
  extraWSs: ComboboxModel[] = [];
  keepFees: ComboboxModel[] = [];
  telcos: TelcosSelectBoxModel[];

  sameBrandName: boolean;

  REGEX = /^[-.a-zA-Z0-9_]{1,}$/;
  private readonly listEndFile = '.7z, .rar, .zip, .txt, .ppt, .pptx, .doc, .docx, .xls, .xlsx, .pdf, .jpg,.jpeg, .png, .bmp, .gif';
  private readonly maxSize = 20971520;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private cpBrandManagementService: CpBrandManagementService,
    public dialog: MtxDialog,
    protected toastr: ToastrService,
    private datePipe: DatePipe,
    private translateService: TranslateService) {
  }

  ngOnInit(): void {
    this.disableCreate = false;
    this.getCpData();
    this.getTelcos();
    this.sameBrandName = false;
    this.getKeepFee();
    this.getMainWS();
    this.getExtraWS();
    this.getAliasGroup(null);
    this.createForm();
    setTimeout(() => {
      this.brandName.nativeElement.focus();
    }, 0);
  }

  getCpData() {
    this.cpBrandManagementService.cpData$.subscribe((data) => {
      this.cpData = data;
      if (!data) {
        this.router.navigate(['cp-management']);
      }
    });
  }

  createForm() {
    if (this.cpData) {
      this.formCreate = this.fb.group(
        {
          brandName: ['', [ValidationService.validateNoFullSpace,
            Validators.maxLength(11),
            Validators.pattern(this.REGEX)]],
          state: [1],
          companyName: [this.cpData.cpName, [ValidationService.validateNoFullSpace,
            Validators.maxLength(500)]],
          taxCode: [this.cpData.taxCode, [ValidationService.validateNoFullSpace, Validators.maxLength(50)]],
          aliasType: [null, [Validators.required]],
          groupId: ['', ValidationService.validateNoFullSpace],
          telco: ['VT'],
          mainWS: ['', ValidationService.validateNoFullSpace],
          extraWS: [{value: '', disabled: true}],
          timeRepeat: [300, [Validators.required, Validators.maxLength(5), Validators.min(0), Validators.max(86400)]],
          expirationDate: [null, [ValidationService.checkDateAndCurrentDate]],
          checkSpam: [false],
          feeRegister: [false],
          blockSMPP: [false],
          superiorsCp: [false],
          keepFee: [{value: '', disabled: true}],
          numberSpam: [{value: '', disabled: true}, [Validators.min(0), Validators.max(9999999999999999999)]],
          feeMonth: [null, [Validators.min(0), Validators.max(999)]],
          attachFile: [null, [ValidationService.fileType(this.listEndFile), ValidationService.checkFileSize(this.maxSize)]]
        }
      );
    }
  }

  getAliasGroup(type) {
    if (type || type === 0) {
      this.cpBrandManagementService.getAliasGroup(type).subscribe((data: any) => {
        if (data.status.code === Constant.OK) {
          this.aliasGroups = data.data;
        }
      });
    }
  }


  getMainWS() {
    this.cpBrandManagementService.getMainWS(0).subscribe((data: any) => {
      if (data.status.code === Constant.OK) {
        this.mainWSs = data.data;
      }
    });
  }

  getExtraWS() {
    this.cpBrandManagementService.getMainWS(1).subscribe((data: any) => {
      if (data.status.code === Constant.OK) {
        this.extraWSs = data.data;
      }
    });
  }

  getKeepFee() {
    this.cpBrandManagementService.getKeepFee().subscribe((data: any) => {
      if (data.status.code === Constant.OK) {
        this.keepFees = data.data;
      }
    });
  }

  getTelcos() {
    this.cpBrandManagementService.getTelcos().subscribe((data: any) => {
      if (data.status.code === Constant.OK) {
        this.telcos = data.data;
      }
    });
  }

  changeMainWS(event) {
    this.formCreate.get('extraWS').disable();
    this.formCreate.get('extraWS').setValue('');
    if (event.value) {
      this.formCreate.get('extraWS').enable();
    }
  }

  hasError(controlName: string, errorName: string) {
    return this.formCreate.controls[controlName].hasError(errorName);
  }

  checkSameBrandName() {
    if (this.formCreate.get('brandName').valid) {
      this.formCreate.get('brandName').setErrors(null);
      const brandName = this.formCreate.get('brandName').value.trim();
      this.telcos.forEach(data => {
        if (brandName.toLowerCase() === data.telcoName.trim().toLowerCase()) {
          this.formCreate.get('brandName').setErrors({sameBrandName: true});
          return;
        }
      });
    }
  }

  onChangeCustomFee(event) {
    if (event.checked) {
      this.formCreate.get('keepFee').enable();
      this.formCreate.get('keepFee').setValidators([Validators.required]);
      this.formCreate.get('keepFee').updateValueAndValidity();
      this.formCreate.get('keepFee').setValue('');
    } else {
      this.formCreate.get('keepFee').clearValidators();
      this.formCreate.get('keepFee').disable();
    }
  }

  onChangeBlockSmppp(event) {
    if (event.checked) {
      this.formCreate.get('numberSpam').setValue(500000);
      this.formCreate.get('numberSpam').enable();
      this.formCreate.get('numberSpam').setValidators([Validators.required, Validators.min(0), Validators.max(9999999999999999999)]);
    } else {
      this.formCreate.get('numberSpam').setValue(null);
      this.formCreate.get('numberSpam').disable();
      this.formCreate.get('numberSpam').clearValidators();
    }
    this.formCreate.get('numberSpam').updateValueAndValidity();
  }

  checkValidForm(): boolean {
    let check = true;
    if (this.formCreate.invalid) {
      check = false;
      Object.keys(this.formCreate.controls).forEach(key => {
        this.formCreate.get(key).markAllAsTouched();
      });
    }

    return check;
  }

  setDataToObject(): CpAliasModel {
    const cpAlias = new CpAliasModel();
    cpAlias.aliasType = this.formCreate.get('aliasType').value;
    if (this.formCreate.get('attachFile').value && this.formCreate.get('attachFile').value.length > 0) {
      cpAlias.attachFile = this.formCreate.get('attachFile').value[0].name;
    }
    cpAlias.checkDuplicate = this.formCreate.get('blockSMPP').value ? 1 : 0;
    cpAlias.cpAlias = this.formCreate.get('brandName').value.trim();
    cpAlias.checkBlockSpam = this.formCreate.get('checkSpam').value ? '1' : '0';
    cpAlias.companyName = this.formCreate.get('companyName').value.trim();
    cpAlias.inactiveDateStr = this.formCreate.get('expirationDate').value ? this.datePipe.transform(this.formCreate.get('expirationDate').value, 'dd/MM/yyyy') + ' 23:59:59' : null;
    cpAlias.webserviceBackup = this.formCreate.get('extraWS').value ? this.formCreate.get('extraWS').value.toString().toUpperCase() : null;
    cpAlias.monthKeepFee = this.formCreate.get('feeMonth').value ? this.formCreate.get('feeMonth').value : null;
    cpAlias.optionalKeepFee = this.formCreate.get('feeRegister').value ? 1 : 0;
    cpAlias.groupType = this.formCreate.get('groupId').value;
    cpAlias.keepFee = this.formCreate.get('keepFee').value ? this.formCreate.get('keepFee').value : null;
    cpAlias.webservice = this.formCreate.get('mainWS').value.toString().toUpperCase();
    cpAlias.numberSmsCheckSpam = this.formCreate.get('numberSpam').value === '' ? null : this.formCreate.get('numberSpam').value;
    cpAlias.status = this.formCreate.get('state').value;
    cpAlias.acceptParentCpSend = this.formCreate.get('superiorsCp').value ? 1 : 0;
    cpAlias.taxCode = this.formCreate.get('taxCode').value;
    cpAlias.telco = this.formCreate.get('telco').value;
    cpAlias.timeRepeat = this.formCreate.get('timeRepeat').value;
    cpAlias.cpId = this.cpData.cpId;

    return cpAlias;
  }

  create(check: string, commissionDefault: string) {
    if (!this.checkValidForm() || this.disableCreate) {
      return;
    }
    const cpAlias = this.setDataToObject();
    let file = null;
    if (this.formCreate.get('attachFile').value) {
      file = this.formCreate.get('attachFile').value[0];
    }
    this.disableCreate = true;
    this.cpBrandManagementService.createCpAlias(cpAlias, file, commissionDefault, check, false)
      .subscribe((response: any) => {
        if (response.status.code === Constant.OK) {
          if (response.data.code === 112) {
            this.openDialogConfirmBusiness(response.data.data, response.data.errors);
          } else {
            this.toastr.success(this.translateService.instant('cp-brand-mn.create.success'));
            this.goBack();
          }
          this.disableCreate = false;
        } else {
          this.toastr.error(this.translateService.instant(response.status.message));
          if (response.status.code === '101') {
            setTimeout(() => {
              this.brandName.nativeElement.focus();
            }, 0);
          }
          this.disableCreate = false;
        }
      }, () => {
        this.toastr.warning(this.translateService.instant('common.notify.fail'));
        this.disableCreate = false;
      });
  }

  openDialogConfirmBusiness(cpCode, message) {
    const dialogRef = this.dialog.open({
      maxWidth: '90vw',
      maxHeight: '90vh',
      width: '70vw',
      autoFocus: false,
      data: {cpCode, message},
    }, ConfirmBusinessComponent);
    dialogRef.afterClosed().subscribe(result => {
      if (result.data === 'normal') {
        this.create('1', 'DEFAULT');
      }

      if (result.data === 'commission') {
        this.create('1', '0');
      }

    });
  }

  goBack() {
    this.router.navigate(['cp-management/brand']);
    this.cpBrandManagementService.setCpData(this.cpData);
  }

  changeAliasType(event) {
    this.getAliasGroup(event.value);
    this.formCreate.get('groupId').setValue('');
  }

}
