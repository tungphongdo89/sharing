import {Component, OnInit} from '@angular/core';
import {CpBrandManagementService} from '@core/services/cp/cp-brand-management.service';
import {Constant} from '@shared/Constant';
import {ActivatedRoute, Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {ToastrService} from 'ngx-toastr';
import {environment} from '@env/environment';
import {CommonService} from '@shared/services/common.service';
import {DatePipe} from '@angular/common';
import {ConfirmDialogComponent, ConfirmDialogModel} from '@shared/components/confirm-dialog/confirm-dialog.component';
import {MtxDialog} from '@ng-matero/extensions';
import {CpBrandResultModel} from '@core/models/cp-brand_result_model';
import {CpBrandSearchModel} from '@core/models/cp-brand_search_model';

@Component({
  selector: 'app-cp-brand-management',
  templateUrl: './cp-brand-management.component.html',
  styleUrls: ['./cp-brand-management.component.scss'],
  providers: [DatePipe]
})
export class CpBrandManagementComponent implements OnInit {

  cpBrandResult: CpBrandResultModel[];
  totalRow: number;
  dataSearch: CpBrandSearchModel;
  limit = 10;
  start = 0;
  cpData: any;
  pageIndex: number;

  constructor(
    private cpBrandManagementService: CpBrandManagementService,
    private route: ActivatedRoute,
    protected toastr: ToastrService,
    private translateService: TranslateService,
    private commonService: CommonService,
    private datePipe: DatePipe,
    private router: Router,
    public dialog: MtxDialog) {
  }

  ngOnInit(): void {
    this.getCpData();
  }

  getCpData() {
    this.cpBrandManagementService.cpData$.subscribe((data) => {
      this.cpData = data;
      if (!data) {
        this.router.navigate(['cp-management']);
      }
    });
  }

  search(data: CpBrandSearchModel) {
    // if (this.cpData) {
    //   this.pageIndex = data.pageIndex;
    //   // data.cpId = this.cpData.cpId;
    //   // this.cpBrandManagementService.onSearch(
    //   //   data.cpId, data.aliasType, data.brandName, data.telco,
    //   //   data.start, data.limit).subscribe((response: any) => {
    //   //   if (Constant.OK === response.status.code) {
    //   //     this.cpBrandResult = response.data.data;
    //   //     this.totalRow = response.data.totalRow;
    //   //     this.dataSearch = data;
    //   //   }
    //   // }, () => {
    //   //   this.toastr.warning(this.translateService.instant('common.notify.fail'));
    //   // });
    // }
  }

  changePage(event) {
    this.dataSearch.pageIndex = event.pageIndex;
    // this.dataSearch.start = event.pageIndex * event.pageSize;
    // this.dataSearch.limit = event.pageSize;
    this.search(this.dataSearch);
  }

  export(data: CpBrandSearchModel) {
    // data.cpId = this.cpData.cpId;
    // data.limit = 0;
    // data.start = 0;
    // this.exportData(data);
  }

  exportData(data) {
    if (data) {
      this.commonService.downloadFile(
        environment.serverUrl.api + '/cp-management/brand/export-brands',
        data, null, ('report_cpAliasTmp_' + this.datePipe.transform(new Date(), 'yyyyMMddHHmmss')));
    }
  }

  activeOrInactiveBrand(event) {
    this.cpBrandManagementService.activeOrInactive(event)
      .subscribe((response) => {
        if (response.status.code === Constant.OK) {
          this.search(this.dataSearch);
          const i18n = event.action === 1 ? 'cp-brand-mn.column.lock.sucess' : 'cp-brand-mn.column.unlock.sucess';
          this.toastr.success(this.translateService.instant(i18n));
        } else {
          this.toastr.warning(this.translateService.instant(response.status.message));
        }
      }, () => {
        this.toastr.warning(this.translateService.instant('common.notify.fail'));
      });
  }

  openDialogConfirm(event) {
    const mess = this.translateService.instant('cp-brand-mn.column.delete.confirm.message');
    const title = this.translateService.instant('cp-brand-mn.column.delete.confirm');
    const dialogData = new ConfirmDialogModel(title, mess);
    const dialogRef = this.dialog.originalOpen(ConfirmDialogComponent, {
      maxWidth: '400px',
      data: dialogData
    });

    dialogRef.afterClosed().subscribe(dialogResult => {
      if (dialogResult) {
        this.deleteBrand(event);
      }
    });
  }

  deleteBrand(event) {
    this.cpBrandManagementService.deleteCpBrand(event)
      .subscribe((response) => {
        if (response.status.code === Constant.OK) {
          this.search(this.dataSearch);
          let messageTranslate = '';
          this.translateService.get('cp-brand-mn.column.delete.success', { brandname: event.userName }).subscribe((s: string) => {
            messageTranslate = s;
          });
          this.toastr.success(messageTranslate);
        } else {
          this.toastr.warning(this.translateService.instant(response.status.message));
        }
      }, () => {
        this.toastr.warning(this.translateService.instant('common.notify.fail'));
      });
  }

  viewManagementBrandCreate(event) {
    if (event) {
      this.router.navigate(['cp-management/brand/create']);
    }
  }

  viewManagementBrandEdit(event) {
    if (event) {
      this.router.navigate(['cp-management/brand/edit']);
      this.cpBrandManagementService.setCpAliasData(event);
    }
  }

  downloadFile(event) {
    if (event) {
      return this.cpBrandManagementService.downloadCpAttach(event);
    }
  }

  goBack() {
    this.router.navigate(['cp-management']);
  }

}
