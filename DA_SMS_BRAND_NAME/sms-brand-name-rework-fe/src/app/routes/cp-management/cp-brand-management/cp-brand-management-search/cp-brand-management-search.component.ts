import {Component, ElementRef, EventEmitter, OnInit, Output, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {CpBrandManagementService} from '@core/services/cp/cp-brand-management.service';
import {Constant} from '@shared/Constant';
import {CpBrandSearchModel} from '@core/models/cp-brand_search_model';
import {TelcosSelectBoxModel} from '@core/models/telco_model';

@Component({
  selector: 'app-cp-brand-management-search',
  templateUrl: './cp-brand-management-search.component.html',
  styleUrls: ['./cp-brand-management-search.component.scss']
})
export class CpBrandManagementSearchComponent implements OnInit {

  @ViewChild('brandName') brandName: ElementRef;
  // @Output() dataSearch: EventEmitter<CpBrandSearchModel> = new EventEmitter<CpBrandSearchModel>();
  // @Output() exportFile: EventEmitter<CpBrandSearchModel> = new EventEmitter<CpBrandSearchModel>();
  formSearch: FormGroup;
  telcos: TelcosSelectBoxModel[];
  limit = 10;
  start = 0;
  REGEX = /^[-.a-zA-Z0-9_]{1,}$/;
  private exportFile: any;

  constructor(
    private fb: FormBuilder,
    private cpBrandManagementService: CpBrandManagementService) {
  }

  ngOnInit(): void {
    this.getTelcos();
    this.createForm();
    this.search();
    setTimeout(() => {
      this.brandName.nativeElement.focus();
    }, 0);
  }

  createForm() {
    this.formSearch = this.fb.group(
      {
        aliasType: [-1],
        telco: [''],
        brandName: ['', [Validators.maxLength(20), Validators.pattern(this.REGEX)]]
      }
    );
  }

  getTelcos() {
    this.cpBrandManagementService.getTelcos().subscribe((data: any) => {
      if (data.status.code === Constant.OK) {
        this.telcos = data.data;
      }
    });
  }

  hasError(controlName: string, errorName: string) {
    return this.formSearch.controls[controlName].hasError(errorName);
  }

  checkValidForm(): boolean {
    let check = true;
    if (this.formSearch.invalid) {
      check = false;
      Object.keys(this.formSearch.controls).forEach(key => {
        this.formSearch.get(key).markAllAsTouched();
      });
    }

    return check;
  }

  search() {
    this.trimSpace(this.formSearch.get('brandName').value, 'brandName');

    if (!this.checkValidForm()) {
      return;
    }

    const cpBrandSearch = new CpBrandSearchModel();
    // cpBrandSearch.aliasType = this.formSearch.get('aliasType').value;
    // cpBrandSearch.telco = this.formSearch.get('telco').value;
    // cpBrandSearch.brandName = this.formSearch.get('brandName').value.trim();
    // cpBrandSearch.pageIndex = 0;
    // cpBrandSearch.limit = this.limit;
    // cpBrandSearch.start = this.start;

    // this.dataSearch.emit(cpBrandSearch);
  }

  export() {
    if (!this.checkValidForm()) {
      return;
    }
    const cpBrandSearch = new CpBrandSearchModel();
    // cpBrandSearch.aliasType = this.formSearch.get('aliasType').value;
    // cpBrandSearch.telco = this.formSearch.get('telco').value;
    // cpBrandSearch.brandName = this.formSearch.get('brandName').value.trim();

    // this.exportFile.emit(cpBrandSearch);
  }

  trimSpace(event, controlName: string) {
    // this.formSearch.controls[controlaNme].setValue(event.trim());
  }

}
