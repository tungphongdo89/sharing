import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {TranslateService} from "@ngx-translate/core";

@Component({
  selector: 'app-confirm-business',
  templateUrl: './confirm-business.component.html',
  styleUrls: ['./confirm-business.component.scss']
})
export class ConfirmBusinessComponent implements OnInit {
  title: string;
  message: string;
  constructor(public dialogRef: MatDialogRef<ConfirmBusinessComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private translateService: TranslateService) { }

  ngOnInit(): void {
    this.message = this.data.data.message + ' ' + this.data.data.cpCode;
    this.title = this.translateService.instant('cp-brand-mn-confirm-exist');
  }

  onSaveNormal(): void {
    // Close the dialog, return true
    this.dialogRef.close({ data: 'normal' });
  }

  onDismiss(): void {
    this.dialogRef.close({ data: 'cancel' });
  }

  onSaveCommission(): void {
    this.dialogRef.close({ data: 'commission' });
  }

}
