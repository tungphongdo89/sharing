import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {MtxDialog, MtxGridColumn} from '@ng-matero/extensions';
import {TranslateService} from '@ngx-translate/core';
import {ConfirmDialogComponent, ConfirmDialogModel} from '@shared/components/confirm-dialog/confirm-dialog.component';
import {CpBrandResultModel} from '@core/models/cp-brand_result_model';
import {CpBrandActionModel} from '@core/models/cp-brand_action_model';

@Component({
  selector: 'app-cp-brand-management-result',
  templateUrl: './cp-brand-management-result.component.html',
  styleUrls: ['./cp-brand-management-result.component.scss']
})
export class CpBrandManagementResultComponent implements OnInit {
  @Input() data: CpBrandResultModel[];
  // @Input() totalRow: number;
  @Output() pageChange: EventEmitter<any> = new EventEmitter<any>();
  @Output() activeOrInactiveBrand: EventEmitter<CpBrandActionModel> = new EventEmitter<CpBrandActionModel>();
  @Output() deleteBrand: EventEmitter<CpBrandActionModel> = new EventEmitter<CpBrandActionModel>();
  @Output() viewCreate: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() viewEdit: EventEmitter<any> = new EventEmitter<any>();
  @Output() downloadFile: EventEmitter<string> = new EventEmitter<string>();
  // @Input() pageIndex: number;

  columns: MtxGridColumn[] = [];
  pagesize = 10;
  pageSizeList = [10, 50, 100];
  listAction = [];
  isLoading = true;

  constructor(private translateService: TranslateService,
              public dialog: MtxDialog) {
  }

  ngOnInit(): void {
    this.prepareData();
    // this.totalRow = 0;
  }

  onPageChange(event) {
    this.pageChange.emit(event);
  }

  prepareData() {
    this.listAction.push(
      {
        icon: 'edit',
        tooltip: `${this.translateService.instant('common.button.edit')}`,
        type: 'icon',
        click: record => this.editBrand(record)
      },
      {
        icon: 'delete',
        tooltip: `${this.translateService.instant('common.button.delete')}`,
        type: 'icon',
        color: 'warn',
        click: record => this.delete(record.cpAliasId, 'label.delete', record.alias),
      },
      {
        icon: 'check_circle',
        tooltip: `${this.translateService.instant('filter.active')}`,
        type: 'icon',
        iif: record => record && record.status === 1,
        click: record => this.openDialogConfirm(record.cpAliasId, 'label.active', 0, record.cpAliasTmpId,
          'cp-brand-mn.column.lock.confirm', 'cp-brand-mn.column.lock.confirm.title', record.alias)
      },
      {
        icon: 'check_circle_outline',
        tooltip: `${this.translateService.instant('common.do-inactive')}`,
        type: 'icon',
        iif: record => record && record.status === 0 || record.status === 3,
        click: record => this.openDialogConfirm(record.cpAliasId, 'label.inactive', 1, record.cpAliasTmpId,
          'cp-brand-mn.column.unlock.confirm', 'cp-brand-mn.column.unlock.confirm.title', record.alias)
      },
      {
        icon: 'get_app',
        color: 'basic',
        tooltip: this.translateService.instant('common.button.download'),
        type: 'icon',
        iif: record => record.attachFile,
        click: (record) => this.downloadAttach(record.attachFile),
      }
    );
    this.columns = [
      {i18n: 'common.orderNumber', field: 'rn', width: '50px'},
      {i18n: 'cp-brand-mn.brandType', field: 'aliasTypeName'},
      {i18n: 'cp-brand-mn.brandName', field: 'alias'},
      {i18n: 'cp-brand-mn.create.group', field: 'groupTypeName'},
      {i18n: 'cp-brand-mn.Telco', field: 'telcoName'},
      {i18n: 'cp-brand-mn.create.date', field: 'createDateStr'},
      {i18n: 'common.status', field: 'statusName'},
      {i18n: 'cp-brand-mn.create.time.repeat', field: 'timeRepeat'},
      {
        i18n: 'common.action',
        field: 'option',
        width: '200px',
        pinned: 'right',
        right: '0px',
        type: 'button',
        buttons: this.listAction
      },
    ];
    this.isLoading = false;
  }

  openDialogConfirm(cpAliasId, actionKeyword, action, cpAliasTmpIdTmp, message: string, title: string, alias: string) {
    title = this.translateService.instant(title);
    const dialogData = new ConfirmDialogModel(this.translateService.instant(title),
      this.translateService.instant(message, {brandname: alias}));

    const dialogRef = this.dialog.originalOpen(ConfirmDialogComponent, {
      maxWidth: '400px',
      data: dialogData
    });

    dialogRef.afterClosed().subscribe(dialogResult => {
      if (dialogResult) {
        this.activeOrInactive(cpAliasId, actionKeyword, action, cpAliasTmpIdTmp,);
      }
    });
  }

  activeOrInactive(cpAliasId, actionKeyword, action, cpAliasTmpIdTmp) {
    const cpBrandActionModel = new CpBrandActionModel();
    // cpBrandActionModel.cpAliasID = cpAliasId;
    // cpBrandActionModel.actionKeyword = actionKeyword;
    // cpBrandActionModel.action = action;
    // cpBrandActionModel.cpAliasTmpID = cpAliasTmpIdTmp;
    this.activeOrInactiveBrand.emit(cpBrandActionModel);
  }

  delete(cpAliasId, actionKeyword, alias) {
    const cpBrandActionModel = new CpBrandActionModel();
    // cpBrandActionModel.cpAliasID = cpAliasId;
    // cpBrandActionModel.actionKeyword = actionKeyword;
    // cpBrandActionModel.action = null;
    // cpBrandActionModel.cpAliasTmpID = null;
    // cpBrandActionModel.userName = alias;
    this.deleteBrand.emit(cpBrandActionModel);
  }

  createBrand() {
    this.viewCreate.emit(true);
  }

  editBrand(record) {
    this.viewEdit.emit(record);
  }

  downloadAttach(fileName) {
    this.downloadFile.emit(fileName);
  }
}
