import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CpIndexComponent } from './cp-index.component';

describe('CpIndexComponent', () => {
  let component: CpIndexComponent;
  let fixture: ComponentFixture<CpIndexComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CpIndexComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CpIndexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
