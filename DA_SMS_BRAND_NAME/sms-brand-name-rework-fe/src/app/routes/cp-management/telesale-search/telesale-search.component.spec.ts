import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TelesaleSearchComponent } from './telesale-search.component';

describe('TelesaleSearchComponent', () => {
  let component: TelesaleSearchComponent;
  let fixture: ComponentFixture<TelesaleSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TelesaleSearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TelesaleSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
