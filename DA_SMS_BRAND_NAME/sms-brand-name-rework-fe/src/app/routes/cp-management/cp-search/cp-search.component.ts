import {AfterViewInit, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {BaseComponent} from '@shared/components/base-component/base-component.component';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MtxDialog} from '@ng-matero/extensions';
import {ToastrService} from 'ngx-toastr';
import {TranslateService} from '@ngx-translate/core';
import {RESOURCE} from '@core';
import {CpService} from '@core/services/cp/cp.service';
import {CpBrandManagementService} from '@core/services/cp/cp-brand-management.service';
import {ConfirmDialogComponent, ConfirmDialogModel} from '@shared/components/confirm-dialog/confirm-dialog.component';
import {AdjustCommissionHisComponent} from '@app/routes/cp-management/adjust-commission-his/adjust-commission-his.component';
import {Constant} from '@shared/Constant';
import * as moment from 'moment';

@Component({
  selector: 'app-cp-search',
  templateUrl: './cp-search.component.html',
  styleUrls: ['./cp-search.component.scss']
})
export class CpSearchComponent extends BaseComponent implements OnInit, AfterViewInit {
  checked = false;
  lstProvince = [];
  listAction = [];
  lstCpParent = [];
  lstCommissionCode = [];
  CBX_DEFAULT_VALUE = '-1';
  CODE_REGEX = /^[a-zA-Z0-9_]{1,}$/;

  status = [
    {value: this.CBX_DEFAULT_VALUE, label: 'customer.allOptions'},
    {value: 0, label: 'customer.status0'},
    {value: 1, label: 'customer.status1'},
  ];
  lstChargeType = [
    {value: this.CBX_DEFAULT_VALUE, label: 'customer.allOptions'},
    {value: 'PRE', label: 'customer.pre'},
    {value: 'POS', label: 'customer.pos'},
  ];
  lstCurrency = [
    {value: this.CBX_DEFAULT_VALUE, label: 'customer.allOptions'},
    {value: 'VND', label: 'VND'},
    {value: 'USD', label: 'USD'},
  ];
  formSearch: FormGroup = this.fb.group({
    cpCode: [null, Validators.maxLength(50)],
    cpName: [null, Validators.maxLength(100)],
    userName: [null, Validators.maxLength(50)],
    chargeType: this.CBX_DEFAULT_VALUE,
    currency: this.CBX_DEFAULT_VALUE,
    status: this.CBX_DEFAULT_VALUE,
    cpAlias: [null, Validators.maxLength(20)],
    provinceId: this.CBX_DEFAULT_VALUE
  });

  constructor(public route: ActivatedRoute,
              private fb: FormBuilder,
              private cpService: CpService,
              public dialog: MtxDialog,
              protected toastr: ToastrService,
              protected translateService: TranslateService,
              private router: Router,
              private cpBrandManagementService: CpBrandManagementService,
              private cdr: ChangeDetectorRef) {

    super(route, cpService, RESOURCE.CUSTOMER, toastr, translateService, dialog);
    this.initAction();
  }

  ngAfterViewInit(): void {
    document.getElementById('cpCode').focus();
    this.cdr.detectChanges();
  }

  ngOnInit(): void {
    this.prepareData();
  }

  search() {
    this.formSearch.markAllAsTouched();
    if (this.formSearch.invalid) return;
    this.isAdvSearch = true;
    this.onSearch();
  }

  onSearch() {
    const options = this.formSearch.getRawValue();
    this.isLoading = true;
    this.isAdvSearch ? this.pageIndex = 0 : null;
    this.formSearch.get('chargeType').value === this.CBX_DEFAULT_VALUE ? options.chargeType = null : null;
    this.formSearch.get('currency').value === this.CBX_DEFAULT_VALUE ? options.currency = null : null;
    this.formSearch.get('status').value === this.CBX_DEFAULT_VALUE ? options.status = null : null;
    this.formSearch.get('provinceId').value === this.CBX_DEFAULT_VALUE ? options.provinceId = null : null;
    options.page = !this.isAdvSearch ? this.startrecord : 0;
    options.pageSize = this.pagesize;
    console.log('options', options);
    this.cpService.search(options).subscribe((res: any) => {
      console.log('res', res);
      if (res && Constant.OK === res.status.code) {
        this.dataModel.dataSource = res.data.listData;
        this.totalRecord = res.data.count;
        this.isLoading = false;
        console.log('this.dataModel', this.dataModel);
      } else {
        this.dataModel.dataSource = [];
        this.isLoading = false;
        this.totalRecord = 0;
      }
    }, () => {
      this.isLoading = false;
      this.showUnknownErr();
    });
    this.isAdvSearch = false;
  }

  prepareSaveOrUpdate() {
    this.router.navigate(['/cp-management/new']);
  }

  edit(record?, componentTemplate?) {
    const ref = this.dialog.open({
      width: '1440px',
      data: {
        record,
        lstProvince: this.lstProvince,
        lstCpParent: this.lstCpParent,
        lstCommissionCode: this.lstCommissionCode
      },
      disableClose: true,
      hasBackdrop: true,
    }, componentTemplate);
    ref.afterClosed().subscribe((res: any) => {
      console.log(res);
      if (res) this.search();
    });
  }

  onCheckboxChange(event: any) {
    if (!event) return;
    this.checked = event.checked;
  }

  prepareData() {
    this.columns = [
      {i18n: 'common.orderNumber', field: 'no', width: '50px'},
      {i18n: 'customer.code', field: 'cpCode'},
      {i18n: 'customer.name', field: 'cpName'},
      {i18n: 'customer.channel', field: 'channel'},
      {i18n: 'customer.username', field: 'userName'},
      {i18n: 'customer.approveType', field: 'approveType'},
      {i18n: 'customer.status', field: 'status'},
      {i18n: 'customer.type', field: 'chargeType'},
      {i18n: 'customer.curType', field: 'currency'},
      {i18n: 'customer.cpBalance', field: 'balance', type: 'number'},
      {i18n: 'customer.adBalance', field: 'adBalance', type: 'number'},
      {i18n: 'customer.province', field: 'provinceName'},
      {i18n: 'customer.district', field: 'districtName'},
      {i18n: 'customer.address', field: 'address'},
      {
        i18n: 'common.action',
        field: 'option',
        width: '280px',
        pinned: 'right',
        right: '0px',
        type: 'button',
        buttons: this.listAction
      },
    ];
    this.isLoading = false;
    this.findAllProvince();
    this.findAllCpParent();
    this.findCommissionCode();
    this.search();
  }

  pageChange(event) {
    console.log('event', event);
    this.pageIndex = event.pageIndex;
    this.startrecord = event.pageIndex === 0 ? event.pageIndex : ((event.pageIndex * event.pageSize));
    this.pagesize = event.pageSize;
    this.onSearch();
  }

  findAllProvince() {
    this.cpService.findAllProvince().subscribe((res: any) => {
        if (res && res.length > 0) {
          this.lstProvince = res.sort((a, b) => a.provinceName > b.provinceName ? 1 : -1);
          this.lstProvince.unshift({provinceId: this.CBX_DEFAULT_VALUE, provinceName: 'customer.allOptions'});
        }
      }, () => this.showUnknownErr()
    );
  }

  findCommissionCode() {
    this.cpService.findCommissionPercentCode().subscribe((res: any) => {
        if (res && res.length > 0) {
          this.lstCommissionCode = res;
        }
      }, () => this.showUnknownErr()
    );
  }

  findAllCpParent() {
    const options = {
      page: 0,
      pageSize: 99999,
      sendWithOther: 1
    };
    this.cpService.search(options).subscribe((res: any) => {
      if (res && res.listData) {
        this.lstCpParent = res.listData;
      }
    }, () => this.showUnknownErr());
  }

  initAction() {
    this.listAction.push({
      icon: 'edit',
      tooltip: this.translateService.instant('common.button.edit'),
      color: 'primary',
      type: 'icon',
      click: record => this.router.navigate([`/cp-management/edit/${record.cpId}`])
    });
    this.listAction.push({
      icon: 'delete',
      tooltip: this.translateService.instant('common.button.delete'),
      color: 'warn',
      type: 'icon',
      click: record => this.deleteCp(record),
    });
    this.listAction.push({
      icon: 'check_circle_outline',
      color: 'primary',
      type: 'icon',
      tooltip: this.translateService.instant('filter.inactive'),
      iif: (record: any) => record.status === 0 ? true : false,
      click: record => {
        this.changeCpStatus(record);
      },
    });
    this.listAction.push({
      icon: 'check_circle',
      color: 'primary',
      type: 'icon',
      tooltip: this.translateService.instant('filter.active'),
      iif: (record: any) => record.status === 1 ? true : false,
      click: record => {
        this.changeCpStatus(record);
      },
    });
    this.listAction.push({
      icon: 'settings',
      tooltip: this.translateService.instant('customer.brand-management', {date: moment().format('DD/MM/YYYY hh:mm:ss A')}),
      type: 'icon',
      color: 'primary',
      click: record => this.viewManagementBrand(record),
    });
    this.listAction.push({
      icon: 'alarm',
      tooltip: this.translateService.instant('customer.actionHis'),
      color: 'primary',
      type: 'icon',
      click: record => this.openAdjustCommissionHis(record.cpId)
    });
    this.listAction.push({
      icon: 'get_app',
      tooltip: this.translateService.instant('customer.downloadFile'),
      color: 'primary',
      type: 'icon',
      iif: record => record.attachFile && record.attachFile !== 'none' ? true : false,
      click: record => this.downloadCpProfile(record.attachFile)
    });
  }

  viewManagementBrand(record) {
    this.router.navigate(['cp-management/brand']);
    this.cpBrandManagementService.setCpData(record);
  }

  changeCpStatus(data: any) {
    const title = data.status === 0 ? this.translateService.instant('customer.confirmActiveStatusTitle') : this.translateService.instant('customer.confirmInactiveStatusTitle');
    const content = data.status === 0 ? this.translateService.instant('customer.confirmActiveStatusMsg', {cpCode: data.cpCode}) : this.translateService.instant('customer.confirmInactiveStatusMsg', {cpCode: data.cpCode});
    const successMsg = data.status === 0 ? this.translateService.instant('customer.activeSuccess') : this.translateService.instant('customer.inactiveSuccess');
    const dialogData = new ConfirmDialogModel(title, content);
    const confirmDialog = this.dialog.originalOpen(ConfirmDialogComponent, {
      disableClose: true,
      hasBackdrop: true,
      data: dialogData
    });
    confirmDialog.afterClosed().subscribe((flag: any) => {
      if (flag) {
        data.status === 1 ? data.status = 0 : data.status = 1;
        this.cpService.update(data, null, null).subscribe((res: any) => {
          this.search();
          this.toastr.success(successMsg);
        }, () => this.showUnknownErr());
      }
    });
  }

  deleteCp(data: any) {
    const title = this.translateService.instant('customer.confirmDeleteTitle', {cpCode: data.cpCode});
    const content = this.translateService.instant('customer.confirmDeleteMsg', {cpCode: data.cpCode});
    const successMsg = this.translateService.instant('customer.deleteSuccess');
    const dialogData = new ConfirmDialogModel(title, content);
    const confirmDialog = this.dialog.originalOpen(ConfirmDialogComponent, {
      disableClose: true,
      hasBackdrop: true,
      data: dialogData
    });
    confirmDialog.afterClosed().subscribe((flag: any) => {
      if (flag) {
        this.cpService.deleteCpById(data.cpId).subscribe((res: any) => {
          this.search();
          this.toastr.success(successMsg);
        }, () => this.showUnknownErr());
      }
    });
  }

  openAdjustCommissionHis(id: any) {
    const ref = this.dialog.open({
      hasBackdrop: true,
      disableClose: true,
      width: '60vw',
      maxHeight: '80vh',
      maxWidth: '60vw',
      data: {cpId: id}
    }, AdjustCommissionHisComponent);
  }

  downloadCpProfile(fileName) {
    if (!fileName) return;
    return this.cpService.downloadCpFiles(fileName, fileName);
  }

  showUnknownErr() {
    this.toastr.error(this.translateService.instant('common.notify.fail'));
  }

  export() {
    const options = this.formSearch.getRawValue();
    this.formSearch.get('chargeType').value === this.CBX_DEFAULT_VALUE ? options.chargeType = null : null;
    this.formSearch.get('currency').value === this.CBX_DEFAULT_VALUE ? options.currency = null : null;
    this.formSearch.get('status').value === this.CBX_DEFAULT_VALUE ? options.status = null : null;
    this.formSearch.get('provinceId').value === this.CBX_DEFAULT_VALUE ? options.provinceId = null : null;
    this.cpService.search(options).subscribe((res: any) => {
      if (res && Constant.OK === res.status.code) {
        if (res.data.count === 0) this.toastr.warning(this.translateService.instant('common.notify.no-record-result'));
        else this.cpService.exportCp(options, `${moment().format('YYYYMMDD').toString()}_report_cp`);
      }
    });
  }

  validSpecialChar(formControlName) {
    const control = this.formSearch.get(formControlName);
    if (control.value) {
      const isValid = this.CODE_REGEX.test(control.value);
      console.log(isValid);
      if (!isValid) control.setErrors({invalidChar: true});
    }
  }
}
