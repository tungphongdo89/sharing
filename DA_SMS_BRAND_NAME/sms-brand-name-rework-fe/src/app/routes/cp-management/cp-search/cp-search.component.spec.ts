import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CpSearchComponent } from './cp-search.component';

describe('CpSearchComponent', () => {
  let component: CpSearchComponent;
  let fixture: ComponentFixture<CpSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CpSearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CpSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
