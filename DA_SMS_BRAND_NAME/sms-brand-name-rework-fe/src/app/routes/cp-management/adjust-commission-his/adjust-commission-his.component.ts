import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {MtxGridColumn} from '@ng-matero/extensions';
import {CpService} from '@core/services/cp/cp.service';
import {ToastrService} from 'ngx-toastr';
import {TranslateService} from '@ngx-translate/core';
import {Constant} from "@shared/Constant";

@Component({
  selector: 'app-adjust-commission-his',
  templateUrl: './adjust-commission-his.component.html',
  styleUrls: ['./adjust-commission-his.component.scss']
})
export class AdjustCommissionHisComponent implements OnInit {

  data = [];
  listAction = [];
  columns: MtxGridColumn[] = [];
  pageSize = 10;
  pageSizeList = [10, 50, 100];
  pageIndex = 0;
  cpId: any;

  constructor(
    private dialogRef: MatDialogRef<AdjustCommissionHisComponent>,
    private cpService: CpService,
    protected toastr: ToastrService,
    protected translateService: TranslateService,
    @Inject(MAT_DIALOG_DATA) public dataDialog?: any,
  ) {
  }

  ngOnInit(): void {
    this.prepareData();
  }

  prepareData() {
    console.log('this.dataDialog', this.dataDialog);
    if (this.dataDialog.data.cpId) this.cpId = this.dataDialog.data.cpId;
    this.columns = [
      {i18n: 'common.orderNumber', field: 'no', width: '80px'},
      {i18n: 'customer.modifyDate', field: 'updatedDate'},
      {i18n: 'customer.modifyUser', field: 'userName'},
      {i18n: 'customer.adjustContent', field: 'content'},
      {i18n: 'customer.adjustReason', field: 'reason'},
    ];
    this.searchAdjustCommissionHis(this.cpId);
  }

  searchAdjustCommissionHis(id: any) {
    this.cpService.searchAdjustCommissionHis(id).subscribe((res: any) => {
      console.log('res', res);
      if (Constant.OK === res.status.code) {
        this.data = res.data;
      }
    }, () => this.showUnknownErr());
  }

  showUnknownErr() {
    this.toastr.error(this.translateService.instant('common.notify.fail'));
  }

  pageChange(event) {
    this.pageIndex = event.pageIndex;
  }
}
