import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdjustCommissionHisComponent } from './adjust-commission-his.component';

describe('AdjustCommissionHisComponent', () => {
  let component: AdjustCommissionHisComponent;
  let fixture: ComponentFixture<AdjustCommissionHisComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdjustCommissionHisComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdjustCommissionHisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
