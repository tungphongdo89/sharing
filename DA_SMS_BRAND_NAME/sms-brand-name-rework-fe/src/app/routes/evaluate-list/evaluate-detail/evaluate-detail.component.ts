import {Component, OnInit} from '@angular/core';
import {CriteriaGroupService} from '@core/services/criteria-group/criteria-group.service';
import {CriteriaRatingService} from '@core/services/criteria-rating/criteria-rating.service';
import {OptionSetValueService} from '@core/services/option/option-set-value';
import {CpTicketService} from '@core/services/cp/cp-ticket.service';
import {EvaluateResultService} from '@core/services/evaluate-result/evaluate-result.service';
import {MatDialog} from '@angular/material/dialog';
import {ToastrService} from 'ngx-toastr';
import {TranslateService} from '@ngx-translate/core';
import {environment} from '@env/environment';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-evaluate-detail',
  templateUrl: './evaluate-detail.component.html',
  styleUrls: ['./evaluate-detail.component.scss']
})
export class EvaluateDetailComponent implements OnInit {

  // Chi tiết cuộc gọi
  evaluateResultId: any;
  evaluateResult: any = Object;
  checkedList = [];

  // Quản lý tiêu chí
  totalScoreLimit = 0;
  totalScore = 0;
  criteriaGroupList: any = [];
  criteriaDetailDTOS: any = [];

  // Quản lý xếp hạng
  criteriaRatingList: any = [];

  // Quản lý lỗi
  optionSetValue: any = [];
  specializeList: any = [];
  majorList: any = [];

  constructor(
    private criteriaGroupService: CriteriaGroupService,
    private criteriaRatingService: CriteriaRatingService,
    private optionSetValueService: OptionSetValueService,
    private cpTicketService: CpTicketService,
    private evaluateResultService: EvaluateResultService,
    private dialog: MatDialog,
    private toastr: ToastrService,
    private translate: TranslateService,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) {
  }

  ngOnInit(): void {
    this.getData();
  }

  getData(): void {
    this.getCriteriaRating().then();
    this.getOptionSetValue().then();
    this.getEvaluateResult().then();
    this.getCriteriaGroupDetail().then();
  }

  async getEvaluateResult(): Promise<void> {
    this.evaluateResultId = Number(this.activatedRoute.snapshot.params.id);
    await this.evaluateResultService.getEvaluateResult(this.evaluateResultId, 0).subscribe(
      res => {
        res.channelName = this.getChannelName(res.channelName);
        this.evaluateResult = res;
        // Build audio
        const audioSource = document.createElement('source');
        audioSource.setAttribute('src', environment.serverUrl.api + res.recordLink);
        document.getElementById('audio').appendChild(audioSource);

        if (this.evaluateResult.evaluateResultDetailDTOS) {
          for (const item of this.evaluateResult.evaluateResultDetailDTOS) {
            this.checkedList.push(item.criteriaDetailId);
          }
        }
      }
    );
  }

  async getCriteriaGroupDetail(): Promise<void> {
    await this.criteriaGroupService.findCriteriaGroupDetail().subscribe(res => {
      if (res) {
        for (const item of res) {
          if (item.scores) {
            this.totalScoreLimit += item.scores;
          }
          for (const criteria of item.lstCriteriaDTO) {
            for (const detail of criteria.criteriaDetailDTOList) {
              if (this.checkedList.includes(detail.id)) {
                detail.checked = true;
                detail.note = this.evaluateResult.evaluateResultDetailDTOS.find(tmp => tmp.criteriaDetailId === detail.id).note;
                this.onCheckCriteria(detail, criteria, item);
              }
            }
          }
        }
      }
      this.criteriaGroupList = res;
    });
  }

  async getCriteriaRating(): Promise<void> {
    await this.criteriaRatingService.findAllCriteriaRating().subscribe(res => this.criteriaRatingList = res);
  }

  async getOptionSetValue(): Promise<void> {
    await this.optionSetValueService.findOptSetValueByOptionSetCode('LOI_CHUYEN_MON').subscribe(res => this.specializeList = res);
    await this.optionSetValueService.findOptSetValueByOptionSetCode('LOI_NGHIEP_VU').subscribe(res => this.majorList = res);
  }

  scrollToCriteria(id: any): void {
    document.getElementById(`${id}`).scrollIntoView(true);
  }

  onCheckCriteria(data: any, criteria: any, group: any): void {
    criteria.criteriaDetailDTOList.forEach(item => {
        if (item.id !== data.id) {
          item.checked = false;
        }
      }
    );

    criteria.score = data.checked ? data.scores : 0;
    group.score = 0;
    group.lstCriteriaDTO.forEach(item => {
        group.score = group.score + (item.score ? item.score : 0);
      }
    );

    this.totalScore = 0;
    this.criteriaGroupList.forEach(item => {
      this.totalScore = this.totalScore + (item.score ? item.score : 0);
    });
  }

  onGoBack(): void {
    this.router.navigate([`/evaluate-list`]).then();
  }

  getChannelName(channelId: any): string {
    if (Number(channelId) === 1) {
      return this.translate.instant('evaluate-assignment.in-call');
    } else {
      return this.translate.instant('evaluate-assignment.out-call');
    }
  }

}
