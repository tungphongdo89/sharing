import {Component, OnInit} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {ToastrService} from 'ngx-toastr';
import {TranslateService} from '@ngx-translate/core';
import {MtxGridColumn} from '@ng-matero/extensions';
import {EvaluateAssignmentService} from '@core/services/evaluate-assignment/evaluate-assignment.service';
import {CriteriaRatingService} from '@core/services/criteria-rating/criteria-rating.service';
import {EvaluateResultService} from '@core/services/evaluate-result/evaluate-result.service';
import * as fileSaver from 'file-saver';
import {DatePipe} from '@angular/common';
import {Router} from '@angular/router';

@Component({
  selector: 'app-evaluate-list',
  templateUrl: './evaluate-list.component.html',
  styleUrls: ['./evaluate-list.component.scss']
})
export class EvaluateListComponent implements OnInit {

  constructor(
    private dialog: MatDialog,
    private toastr: ToastrService,
    private translate: TranslateService,
    private evaluateAssignmentService: EvaluateAssignmentService,
    private criteriaRatingService: CriteriaRatingService,
    private evaluateResultService: EvaluateResultService,
    private router: Router
  ) {
  }

  // Kênh tiếp nhận
  channelList: any = [
    {
      label: this.translate.instant('evaluate-assignment.in-call'),
      value: 1
    }, {
      label: this.translate.instant('evaluate-assignment.out-call'),
      value: 2
    }
  ];
  channelId: any;

  // Danh sách người giám sát
  evaluaterList: any = [];
  evaluaterId: any;

  // Danh sách tổng đài viên
  evaluatedList: any = [];
  evaluatedId: any;

  // Số điện thoại
  phoneNumber: any = '';

  // Ngày đánh giá từ
  startDate: Date = null;

  // Ngày đánh giá đến
  endDate: Date = null;

  // Xếp loại
  criteriaRatingList: any = [];
  criteriaRatingId: any;

  // Bảng
  isLoading: boolean;
  pageSize: number;
  pageIndex: number;
  pageSizeList = [5, 10, 20, 50];
  columns: MtxGridColumn[] = [];
  totalRecord = 0;
  dataList: any = [];
  isExport = false;

  private static saveFile(data: ArrayBuffer, fileName: string) {
    const blob = new Blob([data], {type: 'application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet; charset=UTF-8'});
    fileSaver.saveAs(blob, fileName);
  }

  ngOnInit(): void {
    this.createTable();
    this.getEvaluaterList();
    this.getEvaluatedList();
    this.getCriteriaRatingList();
    this.getDataList(this.pageIndex, this.pageSize);
  }

  onSearch(): void {
    if (this.beforeSearch()) {
      this.pageIndex = 0;
      this.getDataList(this.pageIndex, this.pageSize);
    }
  }

  getDataList(pageIndex: any, pageSize: any): void {
    this.isLoading = true;
    const data = {
      channelId: this.channelId ? this.channelId : null,
      evaluaterId: this.evaluaterId ? this.evaluaterId : null,
      evaluatedId: this.evaluatedId ? this.evaluatedId : null,
      startDate: this.startDate ? this.startDate : null,
      endDate: this.endDate ? this.endDate : null,
      phoneNumber: this.phoneNumber ? this.phoneNumber : null,
      criteriaRatingId: this.criteriaRatingId ? this.criteriaRatingId : null
    };
    const pageable = {page: pageIndex, size: pageSize};
    this.evaluateResultService.getAllEvaluateResult(data, pageable).subscribe(res => {
        this.dataList = res.body;
        this.totalRecord = Number(res.headers.get('X-Total-Count'));
        this.isLoading = false;
      }, _ => this.isLoading = false
    );
  }

  getEvaluaterList(): void {
    this.evaluateAssignmentService.getUsersByType(2).subscribe(res => this.evaluaterList = res);
  }

  getEvaluatedList(): void {
    this.evaluateAssignmentService.getUsersByType(1).subscribe(res => this.evaluatedList = res);
  }

  getCriteriaRatingList(): void {
    this.criteriaRatingService.findAllCriteriaRating().subscribe(res => this.criteriaRatingList = res);
  }

  onExportExcel(): void {
    this.isExport = true;
    const data = {
      channelId: this.channelId ? this.channelId : null,
      evaluaterId: this.evaluaterId ? this.evaluaterId : null,
      evaluatedId: this.evaluatedId ? this.evaluatedId : null,
      startDate: this.startDate ? this.startDate : null,
      endDate: this.endDate ? this.endDate : null,
      phoneNumber: this.phoneNumber ? this.phoneNumber : null,
      criteriaRatingId: this.criteriaRatingId ? this.criteriaRatingId : null
    };
    this.evaluateResultService.exportExcel(data).subscribe(
      res => {
        if (res) {
          this.toastr.success(this.translate.instant('evaluate-list.response.success-export'));
          const pipe = new DatePipe('vi-VN');
          const fileName = this.translate.instant('evaluate-list.file-name') + ' - ' + pipe.transform(new Date(), 'ddMMyyyyHHmmss');
          EvaluateListComponent.saveFile(res.body, fileName);
        }
        this.isExport = false;
      }, _ => this.isExport = false
    );
  }

  onViewEvaluateDetail(id: number): void {
    this.router.navigate([`/evaluate-list/evaluate-detail/${id}`]).then();
  }

  onRedoEvaluateDetail(id: number): void {
    this.router.navigate([`/evaluate-list/evaluate-redo/${id}`]).then();
  }

  onReplyEvaluateDetail(id: number): void {
    this.router.navigate([`/evaluate-list/evaluate-reply/${id}`]).then();
  }

  createTable(): void {
    this.pageIndex = 0;
    this.pageSize = 10;

    this.columns = [
      {i18n: 'STT', field: 'no', width: '50px'},
      {i18n: 'evaluate-list.column.channel', field: 'channelTypeName', width: '150px',},
      {i18n: 'evaluate-list.column.phone-number', field: 'phoneNumber', width: '150px'},
      {i18n: 'evaluate-list.column.evaluater', field: 'evaluaterName', width: '150px'},
      {i18n: 'evaluate-list.column.evaluate-date', field: 'evaluateDate', width: '150px'},
      {i18n: 'evaluate-list.column.evaluated', field: 'evaluatedName', width: '150px'},
      {i18n: 'evaluate-list.column.record-date', field: 'recordDate', width: '150px'},
      {i18n: 'evaluate-list.column.content', field: 'content', width: '200px'},
      {i18n: 'evaluate-list.column.suggest', field: 'suggest', width: '200px'},
      {i18n: 'evaluate-list.column.score', field: 'totalScores', width: '150px'},
      {i18n: 'evaluate-list.column.score-new', field: 'totalScoresNew', width: '150px'},
      {i18n: 'evaluate-list.column.rating', field: 'criteriaRatingName', width: '150px'},
      {i18n: 'evaluate-list.column.error-1', field: 'error1Name', width: '150px'},
      {i18n: 'evaluate-list.column.error-2', field: 'error2Name', width: '150px'},
      {
        i18n: 'evaluate-list.column.tool',
        field: 'tools',
        width: '200px',
        pinned: 'right'
      }
    ];
  }

  onPageChange(event) {
    this.pageIndex = event.pageIndex;
    this.pageSize = event.pageSize;
    this.getDataList(this.pageIndex, this.pageSize);
  }

  beforeSearch(): boolean {
    if (!this.channelId) {
      this.toastr.error(this.translate.instant('evaluate-list.validator.required', {field: this.translate.instant('evaluate-assignment.channel')}));
      document.getElementById('channelId').focus();
      return false;
    }
    if (!this.startDate) {
      this.toastr.error(this.translate.instant('evaluate-list.validator.required', {field: this.translate.instant('evaluate-assignment.start-date')}));
      document.getElementById('startDate').focus();
      return false;
    }
    if (!this.endDate) {
      this.toastr.error(this.translate.instant('evaluate-list.validator.required', {field: this.translate.instant('evaluate-assignment.end-date')}));
      document.getElementById('endDate').focus();
      return false;
    }
    if (this.endDate < this.startDate) {
      this.toastr.error(this.translate.instant('evaluate-list.validator.invalid-date'));
      document.getElementById('endDate').focus();
      return false;
    }
    if (!this.criteriaRatingId) {
      this.toastr.error(this.translate.instant('evaluate-list.validator.required', {field: this.translate.instant('evaluate-list.search.rating')}));
      document.getElementById('criteriaRatingId').focus();
      return false;
    }
    return true;
  }
}
