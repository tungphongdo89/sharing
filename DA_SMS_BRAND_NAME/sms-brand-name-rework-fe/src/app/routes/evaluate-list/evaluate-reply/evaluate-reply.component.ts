import {Component, OnInit} from '@angular/core';
import {CriteriaGroupService} from '@core/services/criteria-group/criteria-group.service';
import {CriteriaRatingService} from '@core/services/criteria-rating/criteria-rating.service';
import {OptionSetValueService} from '@core/services/option/option-set-value';
import {CpTicketService} from '@core/services/cp/cp-ticket.service';
import {EvaluateResultService} from '@core/services/evaluate-result/evaluate-result.service';
import {MatDialog} from '@angular/material/dialog';
import {ToastrService} from 'ngx-toastr';
import {TranslateService} from '@ngx-translate/core';
import {ActivatedRoute, Router} from '@angular/router';
import {environment} from '@env/environment';
import {EvaluateResultResponseService} from '@core/services/evaluate-result-response/evaluate-result-response.service';
import {MtxGridColumn} from '@ng-matero/extensions';

@Component({
  selector: 'app-evaluate-reply',
  templateUrl: './evaluate-reply.component.html',
  styleUrls: ['./evaluate-reply.component.scss']
})
export class EvaluateReplyComponent implements OnInit {

  // Chi tiết cuộc gọi
  evaluateResultId: any;
  evaluateResult: any = Object;
  checkedList = [];

  // Phản hồi
  reply = '';

  // Quản lý tiêu chí
  totalScoreLimit = 0;
  totalScore = 0;
  criteriaGroupList: any = [];
  criteriaDetailDTOS: any = [];

  // Quản lý xếp hạng
  criteriaRatingList: any = [];

  // Quản lý lỗi
  optionSetValue: any = [];
  specializeList: any = [];
  majorList: any = [];

  // Lịch sử phản hồi
  historyList: any = [];
  isLoading: boolean;
  pageSize: number;
  pageIndex: number;
  pageSizeList = [5, 10, 20, 50];
  columns: MtxGridColumn[] = [];
  totalRecord = 0;


  constructor(
    private evaluateResultResponseService: EvaluateResultResponseService,
    private criteriaGroupService: CriteriaGroupService,
    private criteriaRatingService: CriteriaRatingService,
    private optionSetValueService: OptionSetValueService,
    private cpTicketService: CpTicketService,
    private evaluateResultService: EvaluateResultService,
    private dialog: MatDialog,
    private toastr: ToastrService,
    private translate: TranslateService,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) {
  }

  ngOnInit(): void {
    this.createTable();
    this.getData();
  }

  getData(): void {
    this.getCriteriaRating().then();
    this.getOptionSetValue().then();
    this.getEvaluateResult().then();
    this.getCriteriaGroupDetail().then();
    this.pageIndex = 0;
    this.pageSize = 10;
    this.getHistory(this.pageIndex, this.pageSize);
  }

  async getEvaluateResult(): Promise<void> {
    this.evaluateResultId = Number(this.activatedRoute.snapshot.params.id);
    await this.evaluateResultService.getEvaluateResult(this.evaluateResultId, 2).subscribe(
      res => {
        res.channelName = this.getChannelName(res.channelName);
        this.evaluateResult = res;
        // Build audio
        const audioSource = document.createElement('source');
        audioSource.setAttribute('src', environment.serverUrl.api + res.recordLink);
        document.getElementById('audio').appendChild(audioSource);

        if (this.evaluateResult.evaluateResultDetailDTOS) {
          for (const item of this.evaluateResult.evaluateResultDetailDTOS) {
            this.checkedList.push(item.criteriaDetailId);
          }
        }
      }
    );
  }

  async getCriteriaGroupDetail(): Promise<void> {
    await this.criteriaGroupService.findCriteriaGroupDetail().subscribe(res => {
      if (res) {
        for (const item of res) {
          if (item.scores) {
            this.totalScoreLimit += item.scores;
          }
          for (const criteria of item.lstCriteriaDTO) {
            for (const detail of criteria.criteriaDetailDTOList) {
              if (this.checkedList.includes(detail.id)) {
                detail.checked = true;
                detail.note = this.evaluateResult.evaluateResultDetailDTOS.find(tmp => tmp.criteriaDetailId === detail.id).note;
                this.onCheckCriteria(detail, criteria, item);
              }
            }
          }
        }
      }
      this.criteriaGroupList = res;
    });
  }

  async getCriteriaRating(): Promise<void> {
    await this.criteriaRatingService.findAllCriteriaRating().subscribe(res => this.criteriaRatingList = res);
  }

  async getOptionSetValue(): Promise<void> {
    await this.optionSetValueService.findOptSetValueByOptionSetCode('LOI_CHUYEN_MON').subscribe(res => this.specializeList = res);
    await this.optionSetValueService.findOptSetValueByOptionSetCode('LOI_NGHIEP_VU').subscribe(res => this.majorList = res);
  }

  getHistory(page: any, size: any): void {
    this.isLoading = true;
    const pageable = {page, size};
    this.evaluateResultResponseService.getEvaluateResultResponseByEvaluateResult(this.evaluateResultId, pageable).subscribe(
      res => {
        this.historyList = res.body;
        this.totalRecord = Number(res.headers.get('X-Total-Count'));
        this.isLoading = false;
      }, () => this.isLoading = false
    );
  }

  onPageChange(event) {
    this.pageIndex = event.pageIndex;
    this.pageSize = event.pageSize;
    this.getHistory(this.pageIndex, this.pageSize);
  }

  scrollToCriteria(id: any): void {
    document.getElementById(`${id}`).scrollIntoView(true);
  }

  async doReply(): Promise<void> {
    const data = {
      evaluateResultId: this.evaluateResultId,
      content: this.reply ? this.reply.trim() : ''
    };
    await this.evaluateResultResponseService.createEvaluateResultResponse(data).subscribe(
      res => {
        this.toastr.success(this.translate.instant('evaluate-list.response.success-feedback'));
        if (res) {
          this.onGoBack();
        }
      }
    );
  }

  onCheckCriteria(data: any, criteria: any, group: any): void {
    criteria.criteriaDetailDTOList.forEach(item => {
        if (item.id !== data.id) {
          item.checked = false;
        }
      }
    );

    criteria.score = data.checked ? data.scores : 0;
    group.score = 0;
    group.lstCriteriaDTO.forEach(item => {
        group.score = group.score + (item.score ? item.score : 0);
      }
    );

    this.totalScore = 0;
    this.criteriaGroupList.forEach(item => {
      this.totalScore = this.totalScore + (item.score ? item.score : 0);
    });
  }

  onGoBack(): void {
    this.router.navigate([`/evaluate-list`]).then();
  }

  getChannelName(channelId: any): string {
    if (Number(channelId) === 1) {
      return this.translate.instant('evaluate-assignment.in-call');
    } else {
      return this.translate.instant('evaluate-assignment.out-call');
    }
  }

  createTable(): void {
    this.pageIndex = 0;
    this.pageSize = 10;

    this.columns = [
      {i18n: 'STT', field: 'no', width: '50px'},
      {i18n: 'evaluate-list.history.column.create-date', field: 'createDatetime'},
      {i18n: 'evaluate-list.history.column.create-by', field: 'createBy'},
      {i18n: 'evaluate-list.history.column.content', field: 'content'}
    ];
  }
}
