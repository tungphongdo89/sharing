import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EvaluateReplyComponent } from './evaluate-reply.component';

describe('EvaluateReplyComponent', () => {
  let component: EvaluateReplyComponent;
  let fixture: ComponentFixture<EvaluateReplyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EvaluateReplyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EvaluateReplyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
