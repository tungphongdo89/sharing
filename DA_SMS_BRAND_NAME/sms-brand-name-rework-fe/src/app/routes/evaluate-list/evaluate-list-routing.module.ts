import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {EvaluateListComponent} from '@app/routes/evaluate-list/evaluate-list/evaluate-list.component';
import {EvaluateDetailComponent} from '@app/routes/evaluate-list/evaluate-detail/evaluate-detail.component';
import {EvaluateRedoComponent} from '@app/routes/evaluate-list/evaluate-redo/evaluate-redo.component';
import {EvaluateReplyComponent} from '@app/routes/evaluate-list/evaluate-reply/evaluate-reply.component';

const routes: Routes = [
  {
    path: '',
    component: EvaluateListComponent
  },
  {
    path: 'evaluate-detail/:id',
    component: EvaluateDetailComponent
  },
  {
    path: 'evaluate-redo/:id',
    component: EvaluateRedoComponent
  },
  {
    path: 'evaluate-reply/:id',
    component: EvaluateReplyComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EvaluateListRoutingModule {
}
