import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EvaluateRedoComponent } from './evaluate-redo.component';

describe('EvaluateRedoComponent', () => {
  let component: EvaluateRedoComponent;
  let fixture: ComponentFixture<EvaluateRedoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EvaluateRedoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EvaluateRedoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
