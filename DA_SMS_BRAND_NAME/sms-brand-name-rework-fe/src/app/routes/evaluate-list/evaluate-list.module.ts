import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EvaluateListRoutingModule } from './evaluate-list-routing.module';
import { EvaluateListComponent } from './evaluate-list/evaluate-list.component';
import {SharedModule} from "@shared";
import { EvaluateDetailComponent } from './evaluate-detail/evaluate-detail.component';
import { EvaluateReplyComponent } from './evaluate-reply/evaluate-reply.component';
import { EvaluateRedoComponent } from './evaluate-redo/evaluate-redo.component';


@NgModule({
  declarations: [EvaluateListComponent, EvaluateDetailComponent, EvaluateReplyComponent, EvaluateRedoComponent],
  imports: [
    CommonModule,
    EvaluateListRoutingModule,
    SharedModule
  ]
})
export class EvaluateListModule { }
