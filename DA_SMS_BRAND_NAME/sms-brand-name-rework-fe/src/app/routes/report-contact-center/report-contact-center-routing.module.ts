import {Routes, RouterModule} from '@angular/router';
import {NgModule} from '@angular/core';
import {ReportContactCenterComponent} from '@app/routes/report-contact-center/report-contact-center/report-contact-center.component';

const routes: Routes = [
  {
    path: '',
    component: ReportContactCenterComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportContactCenterRoutingModule {
}
