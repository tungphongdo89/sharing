import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReportContactCenterComponent } from './report-contact-center/report-contact-center.component';
import {SharedModule} from '@shared';
import {ReportContactCenterRoutingModule} from '@app/routes/report-contact-center/report-contact-center-routing.module';



@NgModule({
  declarations: [ReportContactCenterComponent],
  imports: [
    CommonModule,
    SharedModule,
    ReportContactCenterRoutingModule
  ]
})
export class ReportContactCenterModule { }
