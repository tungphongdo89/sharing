import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportContactCenterComponent } from './report-contact-center.component';

describe('ReportContactCenterComponent', () => {
  let component: ReportContactCenterComponent;
  let fixture: ComponentFixture<ReportContactCenterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportContactCenterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportContactCenterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
