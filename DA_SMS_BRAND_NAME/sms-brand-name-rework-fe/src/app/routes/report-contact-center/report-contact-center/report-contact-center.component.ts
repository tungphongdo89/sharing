import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {MtxGridColumn} from '@ng-matero/extensions';
import {ApDomainService} from '@core/services/ap-domain/ap-domain.service';
import {DepartmentService} from '@core/services/department/department.service';
import {BussinessTypeService} from '@core/services/bussiness-type/bussiness-type.service';
import {ReportContactCenterService} from '@core/services/report/report-contact-center.service';
import {CommonService} from '@shared/services/common.service';
import {map} from 'rxjs/operators';

@Component({
  selector: 'app-report-contact-center',
  templateUrl: './report-contact-center.component.html',
  styleUrls: ['./report-contact-center.component.scss']
})
export class ReportContactCenterComponent implements OnInit {
  today = new Date();
  @Output() pageChange: EventEmitter<any> = new EventEmitter<any>();
  chanelTypes: any = [];
  requestTypes: any = [];
  priorities: any = [];
  businessTypes:  any = [];
  ticketRequestStatus: any = [];
  ticketStatus: any = [];
  departments: any = [];
  offset: any = 0;
  reportDetailForm: FormGroup = this.fb.group({
    channelType: null,
    receiveUser: null,
    fromDate: null,
    toDate: null
  });
  data: any = [];
  columns: MtxGridColumn[] = [
    {i18n: 'report-contact-center.management.column.no', field: 'index'},
    {i18n: 'report-contact-center.management.column.ticketCode', field: 'ticketCode'},
    {i18n: 'report-contact-center.management.column.ticketRequestCode', field: 'ticketRequestCode'},
    {i18n: 'report-contact-center.management.column.chanelType', field: 'chanelType'},
    {i18n: 'report-contact-center.management.column.businessType', field: 'bussinessType'},
    {i18n: 'report-contact-center.management.column.requestContent', field: 'requestContent'},
    {i18n: 'report-contact-center.management.column.requestProcess', field: 'requestProcess'},
    {i18n: 'report-contact-center.management.column.requestStatus', field: 'requestStatus'},
    {i18n: 'report-contact-center.management.column.satisfactionLevel', field: 'satisfactionLevel'},
    {i18n: 'report-contact-center.management.column.receiveDate', field: 'receiveDate'},
    {i18n: 'report-contact-center.management.column.receiveUser2', field: 'receiveUser'}
  ];
  pageSizeList = [10, 50, 100];
  pagesize = 10;
  isLoading = false;
  total: any = 0;
  constructor(private fb: FormBuilder,
              private apDomainService: ApDomainService,
              private departmentService: DepartmentService,
              private bussinessTypeService: BussinessTypeService,
              private reportContactCenterService: ReportContactCenterService,
              private commonService: CommonService
  ) { }

  ngOnInit(): void {
    this.initData();
    // this.search();
  }
  initData() {
    // Kênh tiếp nhận
    this.apDomainService.getDomainByType('CHANEL_TYPE').subscribe(res => {
      this.chanelTypes = res;
    });
    // Loại yêu cầu
    this.apDomainService.getDomainByType('REQUEST_TYPE').subscribe(res => {
      this.requestTypes = res;
    });
    // Trạng thái yêu cầu
    this.apDomainService.getDomainByType('TICKET_REQUEST_STATUS').subscribe(res => {
      this.ticketRequestStatus = res;
    });
    // Trạng thái sự vụ
    this.apDomainService.getDomainByType('TICKET_STATUS').subscribe(res => {
      this.ticketStatus = res;
    });
    // Phòng ban
    this.departmentService.getLstDepartment().subscribe(res => {
      this.departments = res;
    });
    // Loại nghiệp vụ
    this.bussinessTypeService.getBussinessType().subscribe(res => {
      this.businessTypes = res;
    });
  }

  search(reset?: boolean) {
    if (reset) {
      this.offset = 0;
      this.pagesize = 10;
    }
    const data = {...this.reportDetailForm.value, queryType: 1, size: this.pagesize, page: this.offset};
    this.reportContactCenterService.query(data, { size: this.pagesize, page: this.offset}).pipe(map(res => {
      return res;
    })).subscribe(res => {
      this.data = res.body;
      this.total = Number(res.headers.get('X-Total-Count'));
    });
  }
  export() {
    const data = {...this.reportDetailForm.value};
    this.reportContactCenterService.export(data);
  }
  onPageChange(event) {
    this.offset = event.pageIndex;
    this.pagesize = event.pageSize;
    this.search();
  }
}
