import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReportRenderStatisticsQuestionRoutingModule } from './report-render-statistics-question-routing.module';
import { ReportRenderStatisticsQuestionComponent } from './report-render-statistics-question/report-render-statistics-question.component';
import {SharedModule} from '@shared';


@NgModule({
  declarations: [ReportRenderStatisticsQuestionComponent],
  imports: [
    CommonModule,
    SharedModule,
    ReportRenderStatisticsQuestionRoutingModule
  ]
})
export class ReportRenderStatisticsQuestionModule { }
