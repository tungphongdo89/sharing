import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ReportRenderStatisticsQuestionComponent} from '@app/routes/report-render-statistics-question/report-render-statistics-question/report-render-statistics-question.component';


const routes: Routes = [
  {
    path:'',
    component: ReportRenderStatisticsQuestionComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportRenderStatisticsQuestionRoutingModule { }
