import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportRenderStatisticsQuestionComponent } from './report-render-statistics-question.component';

describe('ReportRenderStatisticsQuestionComponent', () => {
  let component: ReportRenderStatisticsQuestionComponent;
  let fixture: ComponentFixture<ReportRenderStatisticsQuestionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportRenderStatisticsQuestionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportRenderStatisticsQuestionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
