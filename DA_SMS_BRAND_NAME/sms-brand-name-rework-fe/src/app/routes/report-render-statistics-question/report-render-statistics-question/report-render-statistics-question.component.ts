import { Component, OnInit } from '@angular/core';
import {MtxGridColumn} from '@ng-matero/extensions';

@Component({
  selector: 'app-report-render-statistics-question',
  templateUrl: './report-render-statistics-question.component.html',
  styleUrls: ['./report-render-statistics-question.component.scss']
})
export class ReportRenderStatisticsQuestionComponent implements OnInit {


  columns: MtxGridColumn[] = [];
  pageSizeList = [10, 50, 100];
  isLoading = false;
  offset: any = 0;
  pagesize = 10;
  total: any = 0;
  campaign: any = [];

  constructor() { }

  ngOnInit(): void {


    this.columns = [
      {i18n: 'report_render_statistics.column.no', field: 'index', width: '70px',},
      {i18n: 'report_render_statistics.aa', field: 'aa'},
      {i18n: 'report_render_statistics.column.count', field: 'count'},
    ];
  }




  onPageChange(event) {
    this.offset = event.pageIndex;
    this.pagesize = event.pageSize;
  }

}
