import {AfterViewInit, Component, OnInit} from '@angular/core';
import {PreloaderService} from '@core';
import {MatIconRegistry} from '@angular/material/icon';
import {DomSanitizer, Title} from '@angular/platform-browser';
import {ActivatedRoute, NavigationEnd, Router} from '@angular/router';
import {filter, map} from 'rxjs/operators';
import {TranslateService} from '@ngx-translate/core';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-root',
  template: '<router-outlet></router-outlet>',
})
export class AppComponent implements OnInit, AfterViewInit {
  constructor(
    private preloader: PreloaderService,
    private matIconRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private titleService: Title,
    private translateService: TranslateService,
    private http: HttpClient,
  ) {

    this.router.events.pipe(
      filter(event => event instanceof NavigationEnd),
      map(() => {
        let child = this.activatedRoute.firstChild;
        while (child) {
          if (child.firstChild) {
            child = child.firstChild;
          } else if (child.snapshot.data && child.snapshot.data.title) {
            return child.snapshot.data.title;
          } else {
            return null;
          }
        }
        return null;
      })
    ).subscribe((data: any) => {
      if (data) {
        this.titleService.setTitle(this.translateService.instant(data));
      }
    });
    this.matIconRegistry.addSvgIcon('ic_arrow_select', this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/icons/ic_arrow_select.svg'));
    this.matIconRegistry.addSvgIcon('ic_Calendar', this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/icons/ic_Calendar.svg'));
    this.matIconRegistry.addSvgIcon('ic_CapNhatTrangThaiThe', this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/icons/ic_CapNhatTrangThaiThe.svg'));
    this.matIconRegistry.addSvgIcon('ic_Change', this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/icons/ic_Change.svg'));
    this.matIconRegistry.addSvgIcon('ic_checkbox', this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/icons/ic_checkbox.svg'));
    this.matIconRegistry.addSvgIcon('ic_Delete', this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/icons/ic_Delete.svg'));
    this.matIconRegistry.addSvgIcon('ic_download', this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/icons/ic_download.svg'));
    this.matIconRegistry.addSvgIcon('ic_dropdown', this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/icons/ic_dropdown.svg'));
    this.matIconRegistry.addSvgIcon('ic_Duyet', this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/icons/ic_Duyet.svg'));
    this.matIconRegistry.addSvgIcon('ic_edit', this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/icons/ic_edit.svg'));
    this.matIconRegistry.addSvgIcon('ic_Lock', this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/icons/ic_Lock.svg'));
    this.matIconRegistry.addSvgIcon('ic_menu', this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/icons/ic_menu.svg'));
    this.matIconRegistry.addSvgIcon('ic_minus', this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/icons/ic_minus.svg'));
    this.matIconRegistry.addSvgIcon('ic_next', this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/icons/ic_next.svg'));
    this.matIconRegistry.addSvgIcon('ic_plus', this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/icons/ic_plus.svg'));
    this.matIconRegistry.addSvgIcon('ic_QlyChinhSachGia', this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/icons/ic_QlyChinhSachGia.svg'));
    this.matIconRegistry.addSvgIcon('ic_QlyDanhMuc', this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/icons/ic_QlyDanhMuc.svg'));
    this.matIconRegistry.addSvgIcon('ic_QlyHopDong', this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/icons/ic_QlyHopDong.svg'));
    this.matIconRegistry.addSvgIcon('ic_QlyHoSo', this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/icons/ic_QlyHoSo.svg'));
    this.matIconRegistry.addSvgIcon('ic_QlyKhachHang', this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/icons/ic_QlyKhachHang.svg'));
    this.matIconRegistry.addSvgIcon('ic_QlyPhuongTienDacBiet', this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/icons/ic_QlyPhuongTienDacBiet.svg'));
    this.matIconRegistry.addSvgIcon('ic_radiobutton', this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/icons/ic_radiobutton.svg'));
    this.matIconRegistry.addSvgIcon('ic_radiobutton_Select', this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/icons/ic_radiobutton_Select.svg'));
    this.matIconRegistry.addSvgIcon('ic_settings', this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/icons/ic_settings.svg'));
    this.matIconRegistry.addSvgIcon('ic_themphuluc', this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/icons/ic_themphuluc.svg'));
    this.matIconRegistry.addSvgIcon('ic_uncheck', this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/icons/ic_uncheck.svg'));
    this.matIconRegistry.addSvgIcon('ic_unlink', this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/icons/ic_unlink.svg'));
    this.matIconRegistry.addSvgIcon('ic_user', this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/icons/ic_user.svg'));
    this.matIconRegistry.addSvgIcon('ic_xem', this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/icons/ic_xem.svg'));
    this.matIconRegistry.addSvgIcon('ic_link', this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/icons/ic_link.svg'));
    this.matIconRegistry.addSvgIcon('ic_file', this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/icons/ic_file.svg'));
    this.matIconRegistry.addSvgIcon('ic_unLock', this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/icons/ic_unLock.svg'));
    this.matIconRegistry.addSvgIcon('ic_TiengAnh', this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/icons/ic_TiengAnh.svg'));
    this.matIconRegistry.addSvgIcon('ic_TiengViet', this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/icons/ic_TiengViet.svg'));
  }

  ngOnInit() {
  }

  ngAfterViewInit() {
    this.preloader.hide();
  }
}
