import {NgModule, Optional, SkipSelf} from '@angular/core';
import {CommonModule} from '@angular/common';
import {throwIfAlreadyLoaded} from './module-import-guard';
import {LoginComponent} from './login/login.component';
import {SharedModule} from '@shared';
import { ForgetPasswordComponent } from './login/forget-password/forget-password.component';

@NgModule({
  declarations: [LoginComponent, ForgetPasswordComponent],
  imports: [CommonModule, SharedModule],
})
export class CoreModule {
  constructor(@Optional() @SkipSelf() parentModule: CoreModule) {
    throwIfAlreadyLoaded(parentModule, 'CoreModule');
  }
}
