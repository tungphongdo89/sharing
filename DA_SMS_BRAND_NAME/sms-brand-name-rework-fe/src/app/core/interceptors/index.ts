import {HTTP_INTERCEPTORS} from '@angular/common/http';
import {AuthInterceptor} from '@core/interceptors/auth.interceptor';
import {AuthExpiredInterceptor} from '@core/interceptors/auth-expired.interceptor';

export const httpInterceptorProviders = [
  {
    provide: HTTP_INTERCEPTORS,
    useClass: AuthInterceptor,
    multi: true,
  },
  {
    provide: HTTP_INTERCEPTORS,
    useClass: AuthExpiredInterceptor,
    multi: true,
  }
];
