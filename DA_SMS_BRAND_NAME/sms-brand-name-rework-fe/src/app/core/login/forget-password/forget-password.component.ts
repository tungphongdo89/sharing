import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ProfileService } from '@app/core/services/profile/profile.service';
import { TranslateService } from '@ngx-translate/core';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-forget-password',
  templateUrl: './forget-password.component.html',
  styleUrls: ['./forget-password.component.scss']
})
export class ForgetPasswordComponent implements OnInit {

  constructor(
    protected translateService: TranslateService,
    private toastr: ToastrService,
    private fb: FormBuilder,
    private profileService: ProfileService,
    private router: Router
  ) { }

  passwordRetrievalForm: FormGroup = this.fb.group({
    username: [null, [Validators.required]],
  })

  ngOnInit(): void {
  }

  submit() {
    if (this.passwordRetrievalForm.invalid) {
      this.toastr.error('Username is required')
    } else {
      this.profileService.retrieveMyPassword(this.passwordRetrievalForm.value.username).subscribe(res => {
        if (res.message === null) {
          this.toastr.success(this.translateService.instant("login.notify-send-email-retrieve-password"));
          this.router.navigate(['/login']);
        } else {
          this.toastr.error(res.message);
          console.error(res.message);
        }
      });
    }
  }

}
