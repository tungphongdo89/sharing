import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LoginService } from '@core/auth/login.service';
import { AccountService } from '@core/auth/account.service';
import { ProfileService } from '../services/profile/profile.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  authenticationError: boolean = false;
  loginForm = this.fb.group({
    username: [null, [Validators.required]],
    password: [null, [Validators.required]],
    rememberMe: [true],
  });

  constructor(
    private accountService: AccountService,
    private loginService: LoginService,
    private router: Router,
    private fb: FormBuilder,
    private profileService: ProfileService,
    private toastr: ToastrService
  ) {
  }

  ngOnInit(): void {
    this.accountService.identity().subscribe(() => {
      if (this.accountService.isAuthenticated()) {
        void this.router.navigate(['']);
      }
    });
  }

  login(): void {

    const data = {
      username: this.loginForm.get('username')!.value,
      password: this.loginForm.get('password')!.value,
      rememberMe: this.loginForm.get('rememberMe')!.value,
    };

    this.profileService.checkLoginError(data).subscribe(res => {
      if (res.body.data === 0) {
        this.loginService
          .login(data)
          .subscribe(
            (res) => {
              this.authenticationError = false;
              if (res.activeDatetime === null) {
                this.router.navigate(['/change-password'])
              } else {
                void this.router.navigate(['']);
              }

            },
            (error) => {
              (this.authenticationError = true);
              console.log(error.error.detail);
              this.toastr.error(error.error.detail);
            }
          );
      } else {
        // res.body.data == 0 -> ok
        // res.body.data == 1 -> sai username
        // res.body.data == 1 -> sai password
        this.toastr.error(res.body.message);
      }
    })

  }
}
