import {Injectable, Injector} from '@angular/core';
import {LOCATION_INITIALIZED} from '@angular/common';
import {TranslateService} from '@ngx-translate/core';
import {SettingsService} from './settings.service';
import {environment} from '@env/environment';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class TranslateLangService {
  constructor(
    private _injector: Injector,
    private _translate: TranslateService,
    private _settings: SettingsService,
    private http: HttpClient,
  ) {
  }

  load() {
    return new Promise<any>((resolve: any) => {
      this.dynamicEnv();
      const locationInitialized = this._injector.get(LOCATION_INITIALIZED, Promise.resolve(null));
      locationInitialized.then(() => {
        let browserLang = localStorage.getItem('lang');
        if (!browserLang)
          browserLang = 'vi-VN';
        const defaultLang = browserLang.match(/vi-VN|en-US/) ? browserLang : 'vi-VN';
        this._settings.setLanguage(defaultLang);
        this._translate.setDefaultLang(defaultLang);
        this._translate.use(defaultLang).subscribe(
          () => {
            console.log(`Successfully initialized '${defaultLang}' language.'`);
          },
          () => {
            console.error(`Problem with '${defaultLang}' language initialization.'`);
          },
          () => {
            resolve(null);
          }
        );
      });
    });
  }

  dynamicEnv() {
    this.http.get('assets/conf/env.json').subscribe((value: any) => {
      if (value && environment.production) {
        environment.serverUrl.api = value.CRM_SERVICE;
      }
      console.log(environment.serverUrl.api);
    });
  }
}
