import {Injectable} from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree} from '@angular/router';
import {Observable} from 'rxjs';
import {MenuService} from '@core';

@Injectable({
  providedIn: 'root'
})
export class GuardMenuGuard implements CanActivate {

  constructor(
    private menuService: MenuService
  ) {
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

    if (
      state.url.indexOf('/event-receive') >= 0 &&
      this.menuService.codeList.includes('event-receive') === true
    ) {
      return true;
    }
    return false;
  }

}
