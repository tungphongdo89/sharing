export const PERMISSION_CODE: any = {
  // action tac dong
  'action.view': 'VIEW',
  'action.insert': 'INSERT',
  'action.update': 'UPDATE',
  'action.delete': 'DELETE',
  'action.import': 'IMPORT',
  'action.export': 'EXPORT',
};

export enum RESOURCE {
  CUSTOMER = 'CUSTOMER',
  CP_GROUP = 'CP_GROUP',
  CP_GROUP_SUB = 'CP_GROUP_SUB',
  CONTRACT = 'CONTRACT',
}

export enum PERMISSION {
  VIEW = 'action.view',
  INSERT = 'action.insert',
  DELETE = 'action.delete',
  UPDATE = 'action.update',
}
