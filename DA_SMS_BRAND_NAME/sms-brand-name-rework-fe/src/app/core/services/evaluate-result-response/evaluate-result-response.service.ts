import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {HelperService} from '@shared/services/helper.service';
import {environment} from '@env/environment';
import {BasicService} from '@core/services/basic.service';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EvaluateResultResponseService extends BasicService {

  constructor(
    public httpClient: HttpClient,
    public helperService: HelperService,
  ) {
    super(environment.serverUrl.api, '/evaluate-result-responses', httpClient, helperService);
  }

  createEvaluateResultResponse(data: any): Observable<any> {
    const url = `${this.serviceUrl}`;
    return this.postRequest(url, data);
  }

  getEvaluateResultResponseByEvaluateResult(id: any, pageable: any): Observable<any> {
    const url = `${this.serviceUrl}/${id}`;
    return this.postRequestFullResponse(url, pageable);
  }
}
