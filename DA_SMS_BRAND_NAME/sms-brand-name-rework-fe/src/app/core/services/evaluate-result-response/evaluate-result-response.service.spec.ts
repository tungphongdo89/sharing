import { TestBed } from '@angular/core/testing';

import { EvaluateResultResponseService } from './evaluate-result-response.service';

describe('EvaluateResultResponseService', () => {
  let service: EvaluateResultResponseService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EvaluateResultResponseService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
