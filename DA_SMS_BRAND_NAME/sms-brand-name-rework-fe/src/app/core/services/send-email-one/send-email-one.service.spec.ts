import { TestBed } from '@angular/core/testing';

import { SendEmailOneService } from './send-email-one.service';

describe('SendEmailOneService', () => {
  let service: SendEmailOneService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SendEmailOneService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
