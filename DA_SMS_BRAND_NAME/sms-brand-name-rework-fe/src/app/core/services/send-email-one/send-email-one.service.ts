import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {HelperService} from '@shared/services/helper.service';
import {CommonService} from '@shared/common/common.service';
import {environment} from '@env/environment';
import {SendEmailOneModel} from '@core/models/send-email-one-model';

@Injectable({
  providedIn: 'root'
})
export class SendEmailOneService {

  constructor(
    public httpClient: HttpClient,
    public helperService: HelperService,
    private commonService: CommonService
  ) {
  }

  create(objData: SendEmailOneModel) {
    return this.httpClient.post<SendEmailOneModel>(`${environment.serverUrl.api}/send-email-one`, objData, {observe: 'response'});
  }
}
