import { Injectable } from '@angular/core';
import {BasicService} from '@core/services/basic.service';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {Observable} from 'rxjs/Observable';
import {HttpClient} from '@angular/common/http';
import {HelperService} from '@shared/services/helper.service';
import {CommonService} from '@shared/common/common.service';
import {environment} from '@env/environment';

@Injectable({
  providedIn: 'root'
})
export class NotificationService extends BasicService {
  configData: BehaviorSubject<any> = new BehaviorSubject<any>(null);
  configData$: Observable<any> = this.configData.asObservable();

  constructor(public httpClient: HttpClient, public helperService: HelperService, private commonService: CommonService) {
    super(environment.serverUrl.api, '/notifications', httpClient, helperService);
  }

  query(reqDTO?: any, page?: any): Observable<any> {
    const url = `${this.serviceUrl}`;
    return this.getRequest(url, { observe: 'response'});
  }
  update(id: any, reqDTO?: any): Observable<any> {
    const url = `${this.serviceUrl}/${id}`;
    return this.putRequest(url, reqDTO);
  }
}
