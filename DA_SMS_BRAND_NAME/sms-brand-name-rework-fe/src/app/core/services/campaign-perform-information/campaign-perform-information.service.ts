import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {environment} from '@env/environment';
import {createRequestOption} from '@core/utils/request-util';
import {CommonService} from '@shared/common/common.service';

@Injectable({
  providedIn: 'root'
})
export class CampaignPerformInformationService {

  constructor(private http: HttpClient,
              private commonService: CommonService) {
  }

  findAllCampaigns(): Observable<any> {
    const url = `${environment.serverUrl.api}/campaigns`;
    return this.http.get<any>(url);
  }

  findAllCampaignResource(campaignId): Observable<any> {
    const url = `${environment.serverUrl.api}/campaign-resource`;
    return this.http.get<any>(url, {
      params: {
        campaign_id: campaignId,
      }
    });
  }

  findAllAccountTDV(): Observable<any> {
    const url = `${environment.serverUrl.api}/account/by-type-and-activated`;
    return this.http.get<any>(url);
  }

  getDataInformation(searchObj?: any): Observable<any> {
    const url = `${environment.serverUrl.api}/campaign-perform-information/data-information`;
    return this.http.get<any>(url, {
      params: createRequestOption(searchObj),
    });
  }

  getCallStatus(campaignId): Observable<any> {
    const url = `${environment.serverUrl.api}/campaign-perform-information/call-status`;
    return this.http.get<any>(url, {params: {campaign_id: campaignId}});
  }

  getSurveyStatus(campaignId): Observable<any> {
    const url = `${environment.serverUrl.api}/campaign-perform-information/survey-status`;
    return this.http.get<any>(url, {params: {campaign_id: campaignId}});
  }

  getDataTable(data): Observable<any> {
    const url = `${environment.serverUrl.api}/campaign-perform-information/next-phone-number`;
    return this.http.get<any>(url, {params: createRequestOption(data)});
  }

  export(data, fileName) {
    const url = `${environment.serverUrl.api}/campaign-perform-information/export`;
    this.commonService.downloadFile(url, data, null,
      fileName,
      'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;');

  }
}
