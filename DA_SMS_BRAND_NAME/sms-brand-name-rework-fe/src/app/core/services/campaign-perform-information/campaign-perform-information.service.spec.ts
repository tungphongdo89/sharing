import { TestBed } from '@angular/core/testing';

import { CampaignPerformInformationService } from './campaign-perform-information.service';

describe('CampaignPerformInformationService', () => {
  let service: CampaignPerformInformationService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CampaignPerformInformationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
