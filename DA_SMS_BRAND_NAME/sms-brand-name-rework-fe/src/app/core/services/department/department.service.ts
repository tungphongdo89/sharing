import { Injectable } from '@angular/core';
import {BasicService} from '@core/services/basic.service';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {Observable} from 'rxjs/Observable';
import {HttpClient} from '@angular/common/http';
import {HelperService} from '@shared/services/helper.service';
import {CommonService} from '@shared/common/common.service';
import {environment} from '@env/environment';

@Injectable({
  providedIn: 'root'
})
export class DepartmentService extends BasicService {
  configData: BehaviorSubject<any> = new BehaviorSubject<any>(null);
  configData$: Observable<any> = this.configData.asObservable();
  constructor(public httpClient: HttpClient, public helperService: HelperService, private commonService: CommonService) {
    super(environment.serverUrl.api, '/departments', httpClient, helperService);
  }
  getLstDepartment(): Observable<any> {
    const url = `${this.serviceUrl}/getListDepartmentsForCombobox`;
    return this.getRequest(url);
  }

  getDepartments():Observable<any>{
    const url = this.serviceUrl + "/getListDepartmentsForCombobox";
    return this.httpClient.get(url);
  }
}
