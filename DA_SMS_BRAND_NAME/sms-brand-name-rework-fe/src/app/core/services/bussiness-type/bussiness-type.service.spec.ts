import { TestBed } from '@angular/core/testing';

import { BussinessTypeService } from './bussiness-type.service';

describe('BussinessTypeService', () => {
  let service: BussinessTypeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BussinessTypeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
