import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {HelperService} from '@app/shared/services/helper.service';
import {environment} from '@env/environment';
import {Observable} from 'rxjs';
import {BasicService} from '@core/services/basic.service';

@Injectable({
  providedIn: 'root'
})
export class CampaignResultDetailService extends BasicService {

  constructor(
    public httpClient: HttpClient,
    public helperService: HelperService,
  ) {
    super(environment.serverUrl.api, '/campaign-resource-detail', httpClient, helperService);
  }

  getCampaignResourceDetailsNotGroup(campaignResourceId: any): Observable<any> {
    const url = `${this.serviceUrl}/getNumberCampaignGroup/` + campaignResourceId;
    return this.httpClient.get<any>(url);
  }

  groupCampaignResourceDetails(data: any): Observable<any> {
    const url = `${this.serviceUrl}/group`;
    return this.httpClient.post(url, data, {
      observe: 'response'
    });
  }

  getGroupList(): Observable<any> {
    const url = `${this.serviceUrl}/getCampaignGroup`;
    return this.postRequest(url);
  }

  getDataCampaignGroup(campaignId: any, groupId: any): Observable<any> {
    const url = `${this.serviceUrl}/getDataCampaignGroup?campaignId=${campaignId}&groupId=${groupId}`;
    return this.postRequest(url);
  }

  getCampaignUser(data: any): Observable<any> {
    const url = `${this.serviceUrl}/getCampaignUser`;
    return this.postRequest(url, data);
  }

  assignCampaignUser(data: any): Observable<any> {
    const url = `${this.serviceUrl}/assignCampaignUser`;
    return this.postRequest(url, data);
  }

  restoreCampaignGroup(data: any): Observable<any> {
    const url = `${this.serviceUrl}/restoreCampaignGroup`;
    return this.postRequest(url, data);
  }

  restoreCampaignResource(data: any): Observable<any> {
    const url = `${this.serviceUrl}/restoreCampaignResource`;
    return this.postRequest(url, data);
  }
}
