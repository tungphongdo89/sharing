import { TestBed } from '@angular/core/testing';

import { CampaignResultDetailService } from './campaign-result-detail.service';

describe('CampaignResultDetailService', () => {
  let service: CampaignResultDetailService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CampaignResultDetailService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
