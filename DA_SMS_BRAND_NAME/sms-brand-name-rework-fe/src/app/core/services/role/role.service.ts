import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {HelperService} from '@shared/services/helper.service';
import {environment} from '@env/environment';
import {BasicService} from '@core/services/basic.service';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RoleService extends BasicService {

  constructor(
    public httpClient: HttpClient,
    public helperService: HelperService,
  ) {
    super(environment.serverUrl.api, '/role', httpClient, helperService);
  }

  getRole(data: any, pageable: any): Observable<any> {
    const url = `${this.serviceUrl}/search`;
    return this.postRequestFullResponse(url, data, pageable);
  }

  createRole(data: any): Observable<any> {
    const url = `${this.serviceUrl}`;
    return this.postRequest(url, data);
  }

  updateRole(data: any): Observable<any> {
    const url = `${this.serviceUrl}`;
    return this.putRequest(url, data);
  }

  actionRole(data: any): Observable<any> {
    const url = `${this.serviceUrl}/action`;
    return this.postRequest(url, data);
  }
}
