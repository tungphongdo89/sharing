import { TestBed } from '@angular/core/testing';

import { GroupCallOutService } from './group-call-out.service';

describe('GroupCallOutService', () => {
  let service: GroupCallOutService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GroupCallOutService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
