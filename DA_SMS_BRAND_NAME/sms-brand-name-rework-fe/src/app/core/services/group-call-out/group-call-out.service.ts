import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {createRequestOption} from '@core/utils/request-util';
import {environment} from '@env/environment';

@Injectable({
  providedIn: 'root'
})
export class GroupCallOutService {

  url = `${environment.serverUrl.api}/group-user-mapping`;

  constructor(private http: HttpClient) {
  }

  getAllGroupUser(searchObj): Observable<any> {
    return this.http.get(this.url + '/find-all-group-user', {
      params: createRequestOption(searchObj),
      observe: 'response'
    });
  }

  getGroupUser(groupId): Observable<any> {
    const url1 = `${environment.serverUrl.api}/group-users/${groupId}`;
    return this.http.get(url1);
  }

  createGroupUser(data: any): Observable<any> {
    return this.http.post(this.url + `/create-group-user`, data);
  }

  addUserToGroup(data): Observable<any> {
    return this.http.post(this.url, data);
  }

  updateGroupUser(data, id): Observable<any> {
    return this.http.put(this.url + `/update-group-user/${id}`, data);
  }

  deleteGroupUser(id): Observable<any> {
    const url1 = `${environment.serverUrl.api}/group-users/${id}`;
    return this.http.delete(url1);
  }

  findAllUserNotInGroup(groupId): Observable<any> {
    return this.http.get(this.url + '/find-user-not-in-group', {
      params: {
        groupId
      }
    });
  }

  findAllUserInGroup(groupId): Observable<any> {
    return this.http.get(this.url + '/find-user-in-group', {
      params: {
        groupId
      }
    });
  }

  deleteUserFromGroup(data): Observable<any> {
    return this.http.delete(this.url, {params: createRequestOption(data)});
  }
}
