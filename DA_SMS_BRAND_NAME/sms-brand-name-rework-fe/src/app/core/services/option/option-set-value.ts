import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {environment} from '@env/environment';
import {HelperService} from '@shared/services/helper.service';
import {BasicService} from '@core/services/basic.service';
import {CommonService} from '@shared/common/common.service';
import {OptionSetValueModel} from '@core/models/option-set-value';

@Injectable({
  providedIn: 'root'
})

export class OptionSetValueService extends BasicService {
  configData: BehaviorSubject<any> = new BehaviorSubject<any>(null);
  configData$: Observable<any> = this.configData.asObservable();
  constructor(public httpClient: HttpClient, public helperService: HelperService, private commonService: CommonService) {
    super(environment.serverUrl.api, '/option_set_value', httpClient, helperService);
  }

  getOptionSetValueList(): Observable<OptionSetValueModel[]>{
    const url = `${this.serviceUrl}`;
    return this.getRequest(url);
  }

  createOptionSetValue(optionSetValue: any): Observable<any>{
    const url = `${this.serviceUrl}`;
    return this.postRequest(url, optionSetValue);
  }
  export(reqDTO: any) {
    const url = `${this.serviceUrl}/export_option_set_value`;
    this.commonService.downloadFile(url, reqDTO, null, 'cate-item-export', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;');
  }
  query(reqDTO: any, page?: any): Observable<any> {
    const url = `${this.serviceUrl}/query`;
    return this.postRequestFullResponse(url, reqDTO, page);
  }
  getOptionSetValueById(optionSetValueId: any): Observable<any>{
    const url = `${this.serviceUrl}/`+ optionSetValueId;
    return this.getRequest(url);
  }
  updateOptionSetValue(optionSetValue: any): Observable<any>{
    const url = `${this.serviceUrl}`;
    return this.putRequest(url, optionSetValue);
  }

  updateStatusOptionSetValue(optionSetValue: any): Observable<any>{
    const url = `${this.serviceUrl}/change_status`;
    return this.putRequest(url, optionSetValue);
  }

  findOptSetValueByOptionSetCode(code: any): Observable<any>{
    const url = `${this.serviceUrl}/optionSetCode?code=${code}`;
    return this.getRequest(url);
  }
}
