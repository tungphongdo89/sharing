import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {environment} from '@env/environment';
import {HelperService} from '@shared/services/helper.service';
import {BasicService} from '@core/services/basic.service';
import {CommonService} from '@shared/common/common.service';
import {OptionSetModel} from '@core/models/option-set-model';
import {ConfigScheduleResponse} from "@core/models/config_schedule_response_code";

@Injectable({
  providedIn: 'root'
})

export class OptionSetService extends BasicService {
  configData: BehaviorSubject<any> = new BehaviorSubject<any>(null);
  configData$: Observable<any> = this.configData.asObservable();
  constructor(public httpClient: HttpClient, public helperService: HelperService, private commonService: CommonService) {
    super(environment.serverUrl.api, '/option_set', httpClient, helperService);
  }

  getOptionSetList(): Observable<OptionSetModel[]>{
    const url = `${this.serviceUrl}`;
    return this.getRequest(url);
  }
 updateOptionSet(optionSet: any): Observable<any>{
    const url = `${this.serviceUrl}`;
    return this.putRequest(url, optionSet);
  }
  query(obj: any, page?: any): Observable<any>{
    const url = `${this.serviceUrl}/query`;
    console.log('page', page);
    return this.postRequestFullResponse(url, obj, page);
  }

  addOptionSet(optionSet: any): Observable<any>{
    const url = `${this.serviceUrl}`;
    return this.postRequest(url, optionSet);
  }
  getOptionSetById(optionSetId: number): Observable<OptionSetModel>{
    const url = `${this.serviceUrl}`;
    return this.getRequest(url + "/" + optionSetId);
  }
  deleteOptionSet(id:any):Observable<any> {
    const url = `${this.serviceUrl}/${id}`;
    return this.deleteRequest(url);
  }

  deleteOptionSetStatus(optionSet:any):Observable<any> {
    const url = `${this.serviceUrl}/change_status`;
    return this.putRequest(url,optionSet);
  }

  importOption(file: File){
    const url = `${this.serviceUrl}/upload_file`;
    const formData = new FormData();
    formData.append('file', file);
     // return this.postRequest(url, formData);
     this.commonService.downloadFileImport(url, formData, null, 'error-import', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;');

  }

}
