import { Injectable } from '@angular/core';
import { BasicService } from '@core/services/basic.service';
import { HttpClient } from '@angular/common/http';
import { HelperService } from '@shared/services/helper.service';
import { CommonService } from '@shared/common/common.service';
import { environment } from '@env/environment';
import { CommonUtils } from '@shared/services';
import { Observable } from 'rxjs';
import { createRequestOption } from '@core/models/campaign-blacklist.interface';

@Injectable({
  providedIn: 'root',
})
export class CriteriaService extends BasicService {

  constructor(
    public httpClient: HttpClient,
    public helperService: HelperService,
    private commonService: CommonService,
  ) {
    super(environment.serverUrl.api, '/criteria', httpClient, helperService);
  }

  getOneCriteria(id?: any): Observable<any> {
    const url = `${this.serviceUrl}/${id}`;
    return this.httpClient.get(url, {
        observe: 'response',
      },
    );
  }

  loadCriteriaToCbx(): Observable<any> {
    const url = `${this.serviceUrl}/list`;
    return this.httpClient.get(url, {
        observe: 'response',
      },
    );
  }

  getCriteriaByCriteriaGroupId(criteriaId: any): Observable<any> {
    const url = `${this.serviceUrl}/criteriaGroup/${criteriaId}`;
    return this.httpClient.get(url, {
        observe: 'response',
      },
    );
  }

  doSearch(page: any, status: any, criteriaGroupId: any, keyWord: any) {
    const url = `${this.serviceUrl}/search`;
    const formData: FormData = new FormData();
    formData.append('status', status ? status : '');
    formData.append('criteriaGroupId', criteriaGroupId ? criteriaGroupId : null);
    formData.append('keyWord', keyWord ? keyWord : '');
    this.credentials = Object.assign({}, page);
    const searchData = CommonUtils.convertData(this.credentials);
    const buildParams = CommonUtils.buildParams(searchData);
    return this.httpClient.post<any>(url, formData, {
      params: buildParams,
      observe: 'response',
    });
  }

  save(data: any) {
    const url = `${this.serviceUrl}/save`;
    return this.httpClient.post(url, data, {
      observe: 'response',
    });
  }

  changeStatus(id: any, isDelete: any) {
    const url = `${this.serviceUrl}/change-status`;
    const formData: FormData = new FormData();
    formData.append('id', id);
    formData.append('isDelete', isDelete);
    return this.httpClient.post(url, formData, {
      observe: 'response',
    });
  }
}
