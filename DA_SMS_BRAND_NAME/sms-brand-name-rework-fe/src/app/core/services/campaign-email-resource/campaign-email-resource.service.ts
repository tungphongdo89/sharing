import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {HelperService} from '@shared/services/helper.service';
import {CommonService} from '@shared/common/common.service';
import {BasicService} from "@core/services/basic.service";
import {environment} from "@env/environment";
import {CommonUtils} from "@shared/services";
import {Observable} from "rxjs/Observable";

@Injectable({
  providedIn: 'root'
})
export class CampaignEmailResourceService extends BasicService {

  constructor(
    public httpClient: HttpClient,
    public helperService: HelperService,
    private commonService: CommonService,
  ) {
    super(environment.serverUrl.api, '/campaign-email-resource', httpClient, helperService);
  }

  doSearch(page: any, campaignEmailMarketingId: any, status: any, sendDate: any) {
    const url = `${this.serviceUrl}`;
    const formData: FormData = new FormData();
    formData.append('campaignEmailMarketingId', campaignEmailMarketingId ? campaignEmailMarketingId : -1);
    formData.append('status', status ? status : '');
    formData.append('sendDate', sendDate ? sendDate : '');
    this.credentials = Object.assign({}, page);
    const searchData = CommonUtils.convertData(this.credentials);
    const buildParams = CommonUtils.buildParams(searchData);
    return this.httpClient.post<any>(url, formData, {
      params: buildParams,
      observe: 'response',
    });
  }

  createCampaignEmailBatch(data): Observable<any> {
    const url = `${environment.serverUrl.api}/campaign-email-batch`;
    return this.httpClient.post(url, data);
  }

  createEmailOne(data): Observable<any> {
    const url = `${environment.serverUrl.api}/campaign-email-batch/one`;
    return this.httpClient.post(url, data);
  }
}
