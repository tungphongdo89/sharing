import { TestBed } from '@angular/core/testing';

import { CampaignSmsResourceService } from './campaign-sms-resource.service';

describe('CampaignSmsResourceService', () => {
  let service: CampaignSmsResourceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CampaignSmsResourceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
