import {Injectable} from '@angular/core';
import {BasicService} from '@core/services/basic.service';
import {HttpClient} from '@angular/common/http';
import {HelperService} from '@shared/services/helper.service';
import {CommonService} from '@shared/common/common.service';
import {environment} from '@env/environment';
import {CommonUtils} from '@shared/services';
import {Observable} from 'rxjs/Observable';

@Injectable({
  providedIn: 'root'
})
export class CampaignSmsResourceService extends BasicService {

  constructor(
    public httpClient: HttpClient,
    public helperService: HelperService,
    private commonService: CommonService,
  ) {
    super(environment.serverUrl.api, '/campaign-sms-resource', httpClient, helperService);
  }

  doSearch(page: any, campaignSmsMarketingId: any, status: any, sendDate: any) {
    const url = `${this.serviceUrl}`;
    const formData: FormData = new FormData();
    formData.append('campaignSmsMarketingId', campaignSmsMarketingId ? campaignSmsMarketingId : -1);
    formData.append('status', status ? status : '');
    formData.append('sendDate', sendDate ? sendDate : '');
    this.credentials = Object.assign({}, page);
    const searchData = CommonUtils.convertData(this.credentials);
    const buildParams = CommonUtils.buildParams(searchData);
    return this.httpClient.post<any>(url, formData, {
      params: buildParams,
      observe: 'response',
    });
  }

  createCampaignSmsBatch(data): Observable<any> {
    const url = `${environment.serverUrl.api}/campaign-sms-batch`;
    return this.httpClient.post(url, data);
  }
}
