import {Injectable} from '@angular/core';
import {CryptoService} from '@app/shared/services/crypto.service';
import {AccessToken} from '@core/models/access-token';

@Injectable({
  providedIn: 'root'
})
class StorageData {
  navState: boolean;
  navFlipState: boolean;
  searchState: any;
  listLang: any;
  listMarket: any;
  currentUrl: any;
  intro: any;
}
export class AppStorage {
  public static data: StorageData;
  private static expriteIn = '_expriteIn';
  private static instanceName = '_AppStorage';

  private static storage = localStorage;

  /**
   * init
   */
  public static init(): void {

  }
  /**
   * isExprited
   */
  public static isExprited(): boolean {
    return false;
  }
  /**
   * clear
   */
  public static clear(): void {
    this.storage.removeItem(this.instanceName);
  }
  /**
   * storedData
   */
  public static storedData(): StorageData {
    const storedData = this.storage.getItem(this.instanceName);
    if (this.isNullOrEmpty(storedData)) {
      return null;
    }
    return CryptoService.decr(storedData);
  }

  public static isNullOrEmpty(str: any): boolean {
    return !str || (str + '').trim() === '';
  }

  /**
   * get
   */
  public static get(key: string): any {
    if (this.isExprited()) {
      return null;
    }
    const storedData = this.storedData();
    if (storedData == null) {
      return null;
    }
    return storedData[key];
  }
  /**
   * get
   */
  public static set(key: string, val: any): any {
    let storedData = this.storedData();
    if (storedData == null) {
      storedData = new StorageData();
    }
    storedData[key] = val;

    this.storage.setItem(this.instanceName, CryptoService.encr(storedData));
  }


  /**
   * getAccessToken
   */
  public static getAccessToken(): AccessToken {
    return this.get('accessToken');
  }
  /**
   * setAccessToken
   */
  public static setAccessToken(accessToken) {
    return this.set('accessToken', accessToken);
  }

  /**
   * getUserLogin
   */
  public static getUserLogin(): string {
    return this.get('userLogin');
  }
  /**
   * setUserLogin
   */
  public static setUserLogin(userLogin) {
    return this.set('userLogin', userLogin);
  }
  /**
   * getCurrentUrl
   */
  public static getCurrentUrl(): any {
    return this.get('currentUrl');
  }
  /**
   * setCurrentUrl
   */
  public static setCurrentUrl(currentUrl: any): void {
    this.set('currentUrl', currentUrl);
  }

  /**
   * getLanguage
   */
  public static getLanguage(): any {
    return this.get('language');
  }
  /**
   * setLanguage
   */
  public static setLanguage(language: any): void {
    this.set('language', language);
  }
}
