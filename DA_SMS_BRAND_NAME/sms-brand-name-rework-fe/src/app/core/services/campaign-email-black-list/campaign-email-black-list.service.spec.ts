import { TestBed } from '@angular/core/testing';

import { CampaignEmailBlackListService } from './campaign-email-black-list.service';

describe('CampaignEmailBlackListService', () => {
  let service: CampaignEmailBlackListService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CampaignEmailBlackListService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
