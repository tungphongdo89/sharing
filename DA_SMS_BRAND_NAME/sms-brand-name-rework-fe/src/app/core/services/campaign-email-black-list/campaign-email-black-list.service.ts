import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {environment} from '@env/environment';
import {createRequestOption} from '@core/utils/request-util';
import {CommonService} from '@shared/common/common.service';
import {DatePipe} from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class CampaignEmailBlackListService {

  constructor(private http: HttpClient,
              private commonService: CommonService) {
  }

  findAllCampaignEmailMarketing(): Observable<any> {
    const url = `${environment.serverUrl.api}/campaign-email-marketing`;
    return this.http.get<any>(url);
  }

  search(searchObj?: any): Observable<any> {
    const url = `${environment.serverUrl.api}/campaign-email-blacklist/search`;
    return this.http.get<any>(url, {
      params: createRequestOption(searchObj),
      observe: 'response'
    });
  }


  create(data): Observable<any> {
    const url = `${environment.serverUrl.api}/campaign-email-blacklist/create`;
    return this.http.post(url, data);

  }

  delete(id): Observable<any> {
    const url = `${environment.serverUrl.api}/campaign-email-blacklist/delete/${id}`;
    return this.http.delete(url);
  }

  upLoad(file: File): Observable<any> {
    const url = `${environment.serverUrl.api}/campaign-email-blacklist/uploadFile`;
    const formData = new FormData();
    formData.append('file', file);
    return this.http.post(url, formData);
  }

  export(data, fileName) {
    const url = `${environment.serverUrl.api}/campaign-email-blacklist/export`;
    this.commonService.downloadFile(url, data, null,
      fileName,
      'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;');

  }
}
