import {CommonUtils} from '@app/shared/services/common-utils.service';
import {Injectable, Inject} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {never, Observable, throwError} from 'rxjs';
import {catchError, tap} from 'rxjs/operators';
import {HelperService} from '@app/shared/services/helper.service';
import {error} from '@angular/compiler/src/util';
import {AppModule} from '@app/app.module';

@Injectable()
export class BasicService {
  public serviceUrl: string;
  public module: string;
  public systemCode: string;
  credentials: any = {};
  // toastr = AppModule.Toastr;
  // translateService = AppModule.TranslateSV;

  /**
   * init service from system code and module
   * config value of app-config.ts
   * param systemCode
   * param module
   */
  constructor(
    @Inject(String) systemCode: string,
    @Inject(String) module: string,
    public httpClient: HttpClient,
    public helperService: HelperService,
  ) {

    this.systemCode = systemCode;
    this.module = module;
    const API_URL = this.systemCode;
    const API_PATH = this.module;
    if (!API_URL) {
      console.error(`Missing config system service config in src/environments/environment.ts => system: ${this.systemCode}`);
      return;
    }
    // if (!API_PATH) {
    //   console.error(`Missing config system service config in src/environments/environment.ts => module: ${this.module}`);
    //   return;
    // }
    this.serviceUrl = API_URL + API_PATH;
  }

  /**
   * set SystemCode
   * param systemCode
   */
  public setSystemCode(systemCode: string) {
    this.systemCode = systemCode;
    const API_URL = this.systemCode;
    const API_PATH = this.module;
    if (!API_URL) {
      console.error(`Missing config system service config in src/environments/environment.ts => system: ${this.systemCode}`);
      return;
    }
    // if (!API_PATH) {
    //   console.error(`Missing config system service config in src/environments/environment.ts => module: ${this.module}`);
    //   return;
    // }
    this.serviceUrl = API_URL + API_PATH;
  }

  public search(data?: any): Observable<any> {
    this.credentials = Object.assign({}, data);
    const searchData = CommonUtils.convertData(this.credentials);
    const buildParams = CommonUtils.buildParams(searchData);
    const url = `${this.serviceUrl}/search`;
    return this.getRequest(url, {params: buildParams});
  }

  /**
   * findAll
   */
  public findAll(): Observable<any> {
    const url = `${this.serviceUrl}`;
    return this.getRequest(url);
  }

  /**
   * findOne
   * param id
   */
  public findOne(id: number): Observable<any> {
    const url = `${this.serviceUrl}/${id}`;
    return this.getRequest(url);
  }

  /**
   * saveOrUpdate
   */
  public saveOrUpdate(item: any): Observable<any> {
    const url = `${this.serviceUrl}`;
    return this.postRequest(url, CommonUtils.convertData(item));
  }

  /**
   * saveOrUpdateFormFile
   */
  public saveOrUpdateFormFile(item: any): Observable<any> {
    const formdata = CommonUtils.convertFormFile(item);
    const url = `${this.serviceUrl}`;
    return this.postRequest(url, formdata);
  }

  /**
   * deleteById
   * param id
   */
  public deleteById(id: number): Observable<any> {
    const url = `${this.serviceUrl}/${id}`;
    this.helperService.isProcessing(true);
    return this.deleteRequest(url);
  }

  /*******************************/

  /**
   * handleError
   */
  public handleError(error: any) {
    const errorMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    return throwError(errorMsg);
  }

  /**
   * make get request
   */
  public getRequest(url: string, options?: any, func?): Observable<any> {
    this.helperService.isProcessing(true);
    return this.httpClient.get(url, options)
      .pipe(
        tap( // Log the result or error
          (res: any) => {
            this.helperService.APP_TOAST_MESSAGE.next(res);
            this.helperService.isProcessing(false);
            if (res?.status?.code && res?.status?.code !== '200') {
              this.helperService.showErrors(res.status.message);
              // tslint:disable-next-line:no-unused-expression
              if (func) func(res);
              throw error(res.status.message);
            }
          },
          errors => {
            this.helperService.APP_TOAST_MESSAGE.next(errors);
            this.helperService.isProcessing(false);
            this.helperService.showUnknowErrors();
          }
        ),
        catchError(this.customHandler)
      );
  }

  public putRequest(url: string, options?: any): Observable<any> {
    this.helperService.isProcessing(true);
    return this.httpClient.put(url, options)
      .pipe(
        tap( // Log the result or error
          (res: any) => {
            this.helperService.APP_TOAST_MESSAGE.next(res);
            this.helperService.isProcessing(false);
            if (res?.status?.code && res?.status?.code !== '200') {
              this.helperService.showErrors(res.status.message);
            }
          },
          err => {
            this.helperService.APP_TOAST_MESSAGE.next(err);
            this.helperService.isProcessing(false);
            this.helperService.showUnknowErrors();
          }
        ),
        catchError(this.handleError)
      );
  }

  /**
   * make post request
   */
  public postRequest(url: string, data?: any, param?: any, func?): Observable<any> {
    this.helperService.isProcessing(true);
    return this.httpClient.post(url, data, {params: param})
      .pipe(
        tap( // Log the result or error
          (res: any) => {
            this.helperService.APP_TOAST_MESSAGE.next(res);
            this.helperService.isProcessing(false);
            if (res?.status?.code && res?.status?.code !== '200') {
              this.helperService.showErrors(res.status.message);
              // tslint:disable-next-line:no-unused-expression
              if (func) func(res);
              throw error(res.status.message);
            }
          },
          errors => {
            this.helperService.APP_TOAST_MESSAGE.next(errors);
            this.helperService.isProcessing(false);
            this.helperService.showUnknowErrors();
          }
        ),
        catchError(this.customHandler)
      );
  }

  public postRequestFullResponse(url: string, data?: any, param?: any, func?): Observable<any> {
    this.helperService.isProcessing(true);
    return this.httpClient.post(url, data, {params: param, observe: 'response'})
      .pipe(
        tap( // Log the result or error
          (res: any) => {
            this.helperService.APP_TOAST_MESSAGE.next(res);
            this.helperService.isProcessing(false);
            if (res?.status?.code && res?.status?.code !== '200') {
              this.helperService.showErrors(res.status.message);
              // tslint:disable-next-line:no-unused-expression
              if (func) func(res);
              throw error(res.status.message);
            }
            if (res?.body?.status?.code && res?.body?.status?.code !== '200') {
              this.helperService.showErrors(res.body.status.message);
              // tslint:disable-next-line:no-unused-expression
              if (func) func(res);
              throw error(res.body.status.message);
            }
          },
          errors => {
            this.helperService.APP_TOAST_MESSAGE.next(errors);
            this.helperService.isProcessing(false);
            this.helperService.showUnknowErrors();
          }
        ),
        catchError(this.customHandler)
      );
  }

  /**
   * make post request for file
   */
  public postRequestFile(url: string, data?: any): Observable<any> {
    this.helperService.isProcessing(true);
    return this.httpClient.post(url, data, {responseType: 'blob'})
      .pipe(
        tap( // Log the result or error
          res => {
            this.helperService.APP_TOAST_MESSAGE.next(res);
            this.helperService.isProcessing(false);
          },
          error => {
            this.helperService.APP_TOAST_MESSAGE.next(error);
            this.helperService.isProcessing(false);
          }
        ),
        catchError(this.handleError)
      );
  }

  /**
   * make get request
   */
  public deleteRequest(url: string): Observable<any> {
    this.helperService.isProcessing(true);
    return this.httpClient.delete(url)
      .pipe(
        tap( // Log the result or error
          res => {
            this.helperService.APP_TOAST_MESSAGE.next(res);
            this.helperService.isProcessing(false);
          },
          error => {
            this.helperService.APP_TOAST_MESSAGE.next(error);
            this.helperService.isProcessing(false);
          }
        ),
        catchError(this.handleError)
      );
  }

  /**
   * processReturnMessage
   * param data
   */
  public processReturnMessage(data): void {
    this.helperService.APP_TOAST_MESSAGE.next(data);
  }

  /**
   * request is success
   */
  public requestIsSuccess(data: any): boolean {
    let isSuccess = false;
    if (!data) {
      isSuccess = false;
    }
    // if (data.type === 'SUCCESS' || data.type === 'success') {
    if (data.mess.code === 1) {
      isSuccess = true;
    } else {
      isSuccess = false;
    }
    return isSuccess;
  }

  /**
   * request is success
   */
  public requestIsConfirm(data: any): boolean {
    let isConfirm = false;
    if (!data) {
      isConfirm = false;
    }
    if (data.type === 'CONFIRM') {
      isConfirm = true;
    } else {
      isConfirm = false;
    }
    return isConfirm;
  }

  /**
   * confirmDelete
   */
  public confirmDelete(data): void {
    this.helperService.confirmDelete(data);
  }

  public customHandler(err?) {
    return never();
  }
}
