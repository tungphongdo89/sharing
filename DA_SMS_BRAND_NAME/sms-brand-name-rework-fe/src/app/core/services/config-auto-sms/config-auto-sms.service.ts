import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '@env/environment';
import {Observable} from 'rxjs/Observable';
import {createRequestOption} from '@core/utils/request-util';

@Injectable({
  providedIn: 'root'
})
export class ConfigAutoSmsService {
  url = `${environment.serverUrl.api}/config-auto-sms`;

  constructor(private http: HttpClient) {
  }

  findAllCampaignSMSMarketing(): Observable<any> {
    const url = `${environment.serverUrl.api}/campaign-sms-marketing`;
    return this.http.get<any>(url);
  }

  search(searchObj?: any): Observable<any> {
    return this.http.get<any>(this.url, {
      params: createRequestOption(searchObj),
      observe: 'response'
    });
  }

  create(data): Observable<any> {
    return this.http.post<any>(this.url, data);
  }

  update(data, id): Observable<any> {
    return this.http.put(this.url + `/${id}`, data);
  }

  delete(id): Observable<any> {
    return this.http.delete(this.url + `/${id}`);
  }
}
