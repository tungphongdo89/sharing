import { TestBed } from '@angular/core/testing';

import { CampaignEmailMarketingService } from './campaign-email-marketing.service';

describe('CampaignEmailMarketingService', () => {
  let service: CampaignEmailMarketingService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CampaignEmailMarketingService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
