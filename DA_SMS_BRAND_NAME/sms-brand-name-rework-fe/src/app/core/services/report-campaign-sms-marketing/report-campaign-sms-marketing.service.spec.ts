import { TestBed } from '@angular/core/testing';

import { ReportCampaignSmsMarketingService } from './report-campaign-sms-marketing.service';

describe('ReportCampaignSmsMarketingService', () => {
  let service: ReportCampaignSmsMarketingService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ReportCampaignSmsMarketingService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
