import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { createRequestOption } from '@app/core/utils/request-util';
import { CommonService } from '@app/shared/common/common.service';
import { HelperService } from '@app/shared/services/helper.service';
import { environment } from '@env/environment';
import { Observable } from 'rxjs';
import { BasicService } from '../basic.service';

@Injectable({
  providedIn: 'root'
})
export class UserService extends BasicService {

  constructor(
    public httpClient: HttpClient,
    public helperService: HelperService,
    private commonService: CommonService) {
    super(environment.serverUrl.api, '', httpClient, helperService);
  }

  getAllUsers(page?: any): Observable<any> {
    const url = this.serviceUrl + '/admin/users';
    const options = createRequestOption(page);
    return this.httpClient.get(url, {
      params: options,
      observe: 'response'
    });
  }

  createUser(userData: any): Observable<any> {
    const url = this.serviceUrl + '/admin/users';
    // let headers = new HttpHeaders();
    // headers = headers.set('Accept-Language', 'en-US');
    return this.httpClient.post(url, userData, {
      // headers: headers,
      observe: 'response'
    })
  }

  updateUser(userData: any): Observable<any> {
    const url = this.serviceUrl + '/admin/users' + `/${userData.login}`;
    return this.httpClient.put(url, userData, {
      observe: 'response',
    })
  }

  deleteUser(login: string): Observable<any> {
    const url = this.serviceUrl + '/admin/users' + `/${login}`;
    return this.httpClient.delete(url, {
      observe: 'response',
    })
  }

  getByLogin(login: string): Observable<any> {
    const url = this.serviceUrl + '/admin/users' + `/${login}`;
    return this.httpClient.get(url);
  }

  getAllAuthorities(): Observable<any> {
    const url = this.serviceUrl + '/authorities';
    return this.httpClient.get(url);
  }

  getAllUser(): Observable<any> {
    const url = `${this.serviceUrl}/admin/users/all`;
    return this.getRequest(url);
  }
}
