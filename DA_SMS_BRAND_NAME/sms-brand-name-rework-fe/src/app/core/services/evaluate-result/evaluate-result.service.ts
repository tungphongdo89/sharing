import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {HelperService} from '@shared/services/helper.service';
import {environment} from '@env/environment';
import {Observable} from 'rxjs';
import {BasicService} from '@core/services/basic.service';

@Injectable({
  providedIn: 'root'
})
export class EvaluateResultService extends BasicService {

  constructor(
    public httpClient: HttpClient,
    public helperService: HelperService,
  ) {
    super(environment.serverUrl.api, '/evaluate-results', httpClient, helperService);
  }

  createEvaluateResult(data: any): Observable<any> {
    const url = `${this.serviceUrl}`;
    return this.postRequest(url, data);
  }

  updateEvaluateResult(data: any): Observable<any> {
    const url = `${this.serviceUrl}`;
    return this.putRequest(url, data);
  }

  getTicketForEvaluate(id: any): Observable<any> {
    const url = `${this.serviceUrl}/get-for-evaluate?id=${id}`;
    return this.getRequest(url);
  }

  getEvaluateResult(id: any, type: any): Observable<any> {
    const url = `${this.serviceUrl}/${id}/${type}`;
    return this.getRequest(url);
  }

  getAllEvaluateResult(data: any, pageable: any): Observable<any> {
    const url = `${this.serviceUrl}/all`;
    return this.postRequestFullResponse(url, data, pageable);
  }

  exportExcel(data: any): Observable<any> {
    const url = `${this.serviceUrl}/export`;
    return this.httpClient.post(url, data, {
      observe: 'response',
      responseType: 'arraybuffer',
    });
  }

  reportEvaluateScore(data: any): Observable<any> {
    const url = `${this.serviceUrl}/reportDetail`;
    return this.httpClient.post(url, data, {
      observe: 'response',
      responseType: 'arraybuffer',
    });
  }

  getRatingCall(data: any, pageable: any): Observable<any> {
    const url = `${this.serviceUrl}/ratingCall`;
    return this.postRequestFullResponse(url, data, pageable);
  }

  ratingCallReport(data: any): Observable<any> {
    const url = `${this.serviceUrl}/ratingCallReport`;
    return this.httpClient.post(url, data, {
      observe: 'response',
      responseType: 'arraybuffer',
    });
  }

  getCriteriaCall(data: any, pageable: any): Observable<any> {
    const url = `${this.serviceUrl}/criteriaCall`;
    return this.postRequestFullResponse(url, data, pageable);
  }

  criteriaCallReport(data: any): Observable<any> {
    const url = `${this.serviceUrl}/criteriaCallReport`;
    return this.httpClient.post(url, data, {
      observe: 'response',
      responseType: 'arraybuffer',
    });
  }
}
