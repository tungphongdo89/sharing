import { TestBed } from '@angular/core/testing';

import { EvaluateResultService } from './evaluate-result.service';

describe('EvaluateResultService', () => {
  let service: EvaluateResultService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EvaluateResultService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
