import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '@env/environment';
import {createRequestOption} from '@core/utils/request-util';

@Injectable({
  providedIn: 'root'
})
export class RecordCallResultsService {

  constructor(public http: HttpClient) {
  }

  getTheNextPhoneNumber(campaignId: any): Observable<any> {
    return this.http.get(`${environment.serverUrl.api}/record-call-result/next-phone-number?campaignId=${campaignId}`);
  }

  getQuestionsAndAnswers(options: any): Observable<any> {
    const params = createRequestOption(options);
    return this.http.get(`${environment.serverUrl.api}/record-call-result/get-questions-and-answers`, {params});
  }

  saveResult(body: any, campaign: any): Observable<any> {
    const params = createRequestOption(campaign);
    return this.http.post(`${environment.serverUrl.api}/record-call-result/save-result`, body, {params});
  }

  addOuterCall(options: any): Observable<any> {
    const params = createRequestOption(options);
    return this.http.post(`${environment.serverUrl.api}/record-call-result/add-outer-call`, null, {params});
  }

  getAllCampaigns() {
    return this.http.get(`${environment.serverUrl.api}/record-call-result/get-all-campaign-for-tdv`);
  }

  getCampaignIdFromCID(CID){
    return this.http.get(`${environment.serverUrl.api}/campaign-resource-detail/get-from-cid/${CID}`);
  }

}
