import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {ConfigScheduleModel} from '@app/core/models/config-schedule-model';
import {CommonService} from '@app/shared/common/common.service';
import {HelperService} from '@app/shared/services/helper.service';
import {environment} from '@env/environment';
import {Observable} from 'rxjs';
import {BasicService} from './basic.service';

@Injectable({
  providedIn: 'root'
})
export class GroupUserService extends BasicService {

  constructor(public httpClient: HttpClient, public helperService: HelperService, private commonService: CommonService) {
    super(environment.serverUrl.api, '/group-users', httpClient, helperService);
  }

  addScript(entity: any): Observable<any> {
    return this.postRequestFullResponse(this.serviceUrl, entity);
  }

  getLstGroupUser(): Observable<any> {
    const url = `${this.serviceUrl}`;
    return this.getRequest(url);
  }

  getAllGroupUsersByCampaignId(capmpaignId: any): Observable<any> {
    const url = `${this.serviceUrl}/campaign/${capmpaignId}`;
    return this.getRequest(url);
  }

}
