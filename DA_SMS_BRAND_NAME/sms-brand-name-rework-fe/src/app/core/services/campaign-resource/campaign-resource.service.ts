import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { HelperService } from '@shared/services/helper.service';
import { CommonService } from '@shared/common/common.service';
import { environment } from '@env/environment';
import { BasicService } from '@core/services/basic.service';
import { Observable } from 'rxjs/Observable';
import { CommonUtils } from '@shared/services';

@Injectable({
  providedIn: 'root',
})
export class CampaignResourceService extends BasicService {

  credentials: any = {};

  constructor(
    public httpClient: HttpClient,
    public helperService: HelperService,
    private commonService: CommonService,
  ) {
    super(environment.serverUrl.api, '/campaign-resource', httpClient, helperService);
  }

  doSearch(page: any, campaignId: any, fromDate: any, toDate: any) {
    const url = `${this.serviceUrl}/search`;
    const formData: FormData = new FormData();
    formData.append('campaignId', campaignId ? campaignId : null);
    formData.append('fromDate', fromDate ? fromDate : '');
    formData.append('toDate', toDate ? toDate : '');
    this.credentials = Object.assign({}, page);
    const searchData = CommonUtils.convertData(this.credentials);
    const buildParams = CommonUtils.buildParams(searchData);
    return this.httpClient.post<any>(url, formData, {
      params: buildParams,
      observe: 'response',
    });
  }

  getLstCampaign(): Observable<any> {
    const url = `${this.serviceUrl}/get-campaign-combo`;
    return this.getRequest(url);
  }

  uploadFile(file: any, id: any) {
    const url = `${this.serviceUrl}/upload`;
    const formData: FormData = new FormData();
    const headers = new HttpHeaders();
    formData.append('file', file ? file : null);
    formData.append('id', id ? id : null);
    headers.append('Content-type', 'multipart/form-data');
    const options = { headers };
    return this.httpClient.post(url, formData, options);
  }

  importFile(file: any, campaignId: any, distinct: any) {
    const url = `${this.serviceUrl}/import`;
    const formData: FormData = new FormData();
    formData.append('file', file ? file : null);
    formData.append('campaignId', campaignId ? campaignId : null);
    formData.append('distinct', distinct);
    return this.httpClient.post(url, formData, {
      observe: 'response'
    });
  }

  downloadTemplate(id: any) {
    const url = `${this.serviceUrl}/download-template/${id}`;
    return this.httpClient.get(url, {
      observe: 'response',
      responseType: 'arraybuffer',
    });
  }

  changeStatus(id: any, status: any) {
    const url = `${this.serviceUrl}/change-status`;
    const formData: FormData = new FormData();
    formData.append('id', id);
    formData.append('status', status ? status : null);
    return this.httpClient.post(url, formData, {
      observe: 'response'
    });
  }

  exportDetail(id: any) {
    const url = `${this.serviceUrl}/export-campaign-resource-detail/${id}`;
    return this.httpClient.get(url, {
      observe: 'response',
      responseType: 'arraybuffer',
    });
  }

  exportError(id: any) {
    const url = `${this.serviceUrl}/export-campaign-resource-error/${id}`;
    return this.httpClient.get(url, {
      observe: 'response',
      responseType: 'arraybuffer',
    });
  }
}
