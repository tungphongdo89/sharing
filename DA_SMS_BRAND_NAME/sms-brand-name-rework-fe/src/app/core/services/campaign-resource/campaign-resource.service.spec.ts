import { TestBed } from '@angular/core/testing';

import { CampaignResourceService } from './campaign-resource.service';

describe('CampaignResourceService', () => {
  let service: CampaignResourceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CampaignResourceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
