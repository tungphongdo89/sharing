import { TestBed } from '@angular/core/testing';

import { CriteriaRatingService } from './criteria-rating.service';

describe('CriteriaRatingService', () => {
  let service: CriteriaRatingService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CriteriaRatingService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
