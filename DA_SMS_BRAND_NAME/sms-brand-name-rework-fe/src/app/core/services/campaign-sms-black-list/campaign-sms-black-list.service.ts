import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {CommonService} from '@shared/common/common.service';
import {environment} from '@env/environment';
import {Observable} from 'rxjs/Observable';
import {createRequestOption} from '@core/utils/request-util';

@Injectable({
  providedIn: 'root'
})
export class CampaignSmsBlackListService {

  constructor(private http: HttpClient,
              private commonService: CommonService) {
  }

  findAllCampaignSMSMarketing() {
    const url = `${environment.serverUrl.api}/campaign-sms-marketing`;
    return this.http.get<any>(url);
  }

  search(searchObj?: any): Observable<any> {
    const url = `${environment.serverUrl.api}/campaign-sms-black-list/search`;
    return this.http.get<any>(url, {
      params: createRequestOption(searchObj),
      observe: 'response'
    });
  }


  create(data): Observable<any> {
    const url = `${environment.serverUrl.api}/campaign-sms-black-list/create`;
    return this.http.post(url, data);

  }

  delete(id): Observable<any> {
    const url = `${environment.serverUrl.api}/campaign-sms-black-list/delete/${id}`;
    return this.http.delete(url);
  }

  upLoad(file: File): Observable<any> {
    const url = `${environment.serverUrl.api}/campaign-sms-black-list/uploadFile`;
    const formData = new FormData();
    formData.append('file', file);
    return this.http.post(url, formData);
  }

  export(data, fileName) {
    const url = `${environment.serverUrl.api}/campaign-sms-black-list/export`;
    this.commonService.downloadFile(url, data, null,
      fileName,
      'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;');

  }
}
