import { TestBed } from '@angular/core/testing';

import { CampaignSmsBlackListService } from './campaign-sms-black-list.service';

describe('CampaignSmsBlackListService', () => {
  let service: CampaignSmsBlackListService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CampaignSmsBlackListService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
