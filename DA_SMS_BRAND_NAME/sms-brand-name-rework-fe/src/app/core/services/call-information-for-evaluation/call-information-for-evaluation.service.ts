import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {environment} from '@env/environment';
import {createRequestOption} from '@core/utils/request-util';

@Injectable({
  providedIn: 'root'
})
export class CallInformationForEvaluationService {

  constructor(private http: HttpClient) {
  }

  findAllAccountTDV(): Observable<any> {
    const url = `${environment.serverUrl.api}/account/by-type-and-activated`;
    return this.http.get<any>(url);
  }

  findCallInformationById(id: any): Observable<any> {
    const url = `${environment.serverUrl.api}/call-information-evaluation/get-call-info-by-id/${id}`;
    return this.http.get<any>(url);
  }

  searchInComingCall(searchObj): Observable<any> {
    const url = `${environment.serverUrl.api}/call-information-evaluation/incoming-call`;
    return this.http.get<any>(url, {
      observe: 'response',
      params: createRequestOption(searchObj),
    });
  }

  searchOutGoingCall(searchObj): Observable<any> {
    const url = `${environment.serverUrl.api}/call-information-evaluation/outgoing-call`;
    return this.http.get<any>(url, {
      observe: 'response',
      params: createRequestOption(searchObj),
    });
  }

  getIdFromProcess(searchObj): Observable<any> {
    const url = `${environment.serverUrl.api}/call-information-evaluation/get-id-from-process`;
    return this.http.get<any>(url, {
      params: createRequestOption(searchObj),
    });
  }
}
