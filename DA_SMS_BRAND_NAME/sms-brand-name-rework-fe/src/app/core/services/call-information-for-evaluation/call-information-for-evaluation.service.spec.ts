import { TestBed } from '@angular/core/testing';

import { CallInformationForEvaluationService } from './call-information-for-evaluation.service';

describe('CallInformationForEvaluationService', () => {
  let service: CallInformationForEvaluationService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CallInformationForEvaluationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
