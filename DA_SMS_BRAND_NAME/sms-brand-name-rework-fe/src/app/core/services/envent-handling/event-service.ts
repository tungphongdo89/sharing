import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {environment} from '@env/environment';
import {HelperService} from '@shared/services/helper.service';
import {BasicService} from '@core/services/basic.service';
import {CommonService} from '@shared/common/common.service';
import {TicketModel} from '@core/models/ticket-model';
import {EventModel} from '@core/models/event-model';
import {ServiceResultModel} from '@core/models/service-result-model';
import {catchError} from 'rxjs/operators';
import {throwError} from "rxjs";

@Injectable({
  providedIn: 'root'
})

export class EventService extends BasicService {
  configData: BehaviorSubject<any> = new BehaviorSubject<any>(null);
  configData$: Observable<any> = this.configData.asObservable();

  constructor(public httpClient: HttpClient, public helperService: HelperService, private commonService: CommonService) {
    super(environment.serverUrl.api, '/tickets', httpClient, helperService);
  }

  getHistorySupport(data: TicketModel, page?: any): Observable<any> {
    const url = `${this.serviceUrl}/getListHistorySupports`;
    return this.postRequestFullResponse(url, data, page);

  }

  insertEvent(evevt: EventModel): Observable<any> {
    return this.httpClient.post<any>(`${this.serviceUrl}/create`, evevt, {
      observe: 'response'
    })
      .pipe(catchError(this.handleErrorResponse));
  }

  handleErrorResponse(error: HttpErrorResponse) {
    return throwError(error);
  }

  uploadFile(file: File[]): Observable<ServiceResultModel> {
    const url = `${this.serviceUrl}/upload-file`;
    const formData = new FormData();
    for (const f of file) {
      formData.append('files', f);
    }
    return this.postRequest(url, formData);
  }

  getTicketById(id: any): Observable<TicketModel> {
    const url = `${this.serviceUrl}/get-for-process/${id}`;
    return this.getRequest(url);
  }

  getPhoneAndEmail(data?: any): Observable<any> {
    const url = `${environment.serverUrl.api}/egp/crm-get-orgcode`;
    return this.httpClient.post(url, data, {observe: "body"});

  }

  getPhoneAndEmailDetail(data?: any): Observable<any> {
    const url = `${environment.serverUrl.api}/egp/crm-get-orginfo`;
    return this.httpClient.post(url, data, {observe: "body"});

  }

  downloadFile(fileName: string) {
    return this.httpClient
      .get<any>(`${this.serviceUrl}/download-file-attachment/${fileName}`, {responseType: 'arraybuffer' as 'json'});
  }
}
