import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {HelperService} from '@shared/services/helper.service';
import {BasicService} from '@core/services/basic.service';
import {CommonService} from '@shared/common/common.service';
import {environment} from '@env/environment';
import {ConfirmTicketModel} from '@core/models/confirm-ticket-model';
import {ServiceResultModel} from '@core/models/service-result-model';

@Injectable({
  providedIn: 'root'
})

export class ConfirmTicketService extends BasicService {
  configData: BehaviorSubject<any> = new BehaviorSubject<any>(null);
  configData$: Observable<any> = this.configData.asObservable();

  constructor(public httpClient: HttpClient, public helperService: HelperService, private commonService: CommonService) {
    super(environment.serverUrl.api, '/confirm-tickets', httpClient, helperService);
  }

  createConfirmTicket(confirmTicket: ConfirmTicketModel): Observable<any> {
    const url = `${this.serviceUrl}/create`;
    return this.postRequest(url, confirmTicket);
  }

  updateConfirmTicket(confirmTicket: ConfirmTicketModel): Observable<any> {
    const url = `${this.serviceUrl}/update`;
    return this.postRequest(url, confirmTicket);
  }

  uploadFile(file: File[]): Observable<any> {
    const url = `${this.serviceUrl}/upload-file`;
    const formData = new FormData();
    for (const f of file) {
      formData.append('files', f);
    }
    return this.postRequest(url, formData);
  }

  getListHistoryConfirmTickets(confirmTicket: ConfirmTicketModel): Observable<ConfirmTicketModel[]> {
    const url = `${this.serviceUrl}/getListHistoryConfirmTickets`;
    return this.postRequest(url, confirmTicket);
  }

  getListConfirmTicketAttachment(ticketId:any): Observable<any>{
    const url = `${this.serviceUrl}/get-list-confirm-ticket-attachment/${ticketId}`;
    return this.getRequest(url);
  }

  deleteConfirmAttachment(confirmTicketAttachment:any): Observable<any>{
    const url = `${this.serviceUrl}/delete-confirm-attachment`;
    return this.postRequestFile(url, confirmTicketAttachment);
  }

  downloadFile(fileName: string) {
    return this.httpClient
      .get<any>(`${this.serviceUrl}/download-file-attachment/${fileName}`, { responseType: 'arraybuffer' as 'json' });
  }
}
