import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {HelperService} from '@shared/services/helper.service';
import {BasicService} from '@core/services/basic.service';
import {CommonService} from '@shared/common/common.service';
import {DepartmentModel} from '@core/models/department-model';
import {environment} from '@env/environment';
import {CustomerModel} from "@core/models/customer-model";

@Injectable({
  providedIn: 'root'
})

export class CustomerService extends BasicService {
  configData: BehaviorSubject<any> = new BehaviorSubject<any>(null);
  configData$: Observable<any> = this.configData.asObservable();
  constructor(public httpClient: HttpClient, public helperService: HelperService, private commonService: CommonService) {
  super(environment.serverUrl.api, '/customers', httpClient, helperService);
}

  getListCustomersForCombobox(): Observable<CustomerModel[]> {
    const url = `${this.serviceUrl}/getListCustomersForCombobox`;
    return this.getRequest(url);
  }
  getCustomerById(id: any): Observable<CustomerModel>{
      const url = `${this.serviceUrl}/${id}`;
      return this.getRequest(url);
    }

}
