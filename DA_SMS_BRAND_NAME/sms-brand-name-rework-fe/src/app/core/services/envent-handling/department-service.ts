import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {HelperService} from '@shared/services/helper.service';
import {BasicService} from '@core/services/basic.service';
import {CommonService} from '@shared/common/common.service';
import {DepartmentModel} from '@core/models/department-model';
import {environment} from '@env/environment';

@Injectable({
  providedIn: 'root'
})

export class DepartmentService extends BasicService {
  configData: BehaviorSubject<any> = new BehaviorSubject<any>(null);
  configData$: Observable<any> = this.configData.asObservable();
  constructor(public httpClient: HttpClient, public helperService: HelperService, private commonService: CommonService) {
  super(environment.serverUrl.api, '/departments', httpClient, helperService);
}

getListDepartmentsForCombobox(): Observable<DepartmentModel[]>{
  const url = `${this.serviceUrl}/getListDepartmentsForCombobox`;
  return this.getRequest(url);
}

}
