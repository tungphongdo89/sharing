import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {HelperService} from '@shared/services/helper.service';
import {BasicService} from '@core/services/basic.service';
import {CommonService} from '@shared/common/common.service';
import {environment} from '@env/environment';
import {ProcessTicketModel} from '@core/models/process-ticket-model';
import {ServiceResultModel} from '@core/models/service-result-model';

@Injectable({
  providedIn: 'root'
})

export class ProcessTicketService extends BasicService {
  configData: BehaviorSubject<any> = new BehaviorSubject<any>(null);
  configData$: Observable<any> = this.configData.asObservable();

  constructor(public httpClient: HttpClient, public helperService: HelperService, private commonService: CommonService) {
    super(environment.serverUrl.api, '/process-tickets', httpClient, helperService);
  }

  createProcessTicket(processTicket: ProcessTicketModel): Observable<ServiceResultModel> {
    const url = `${this.serviceUrl}/create`;
    return this.postRequest(url, processTicket);
  }

  getListHistoryProcessTickets(processTicket: ProcessTicketModel): Observable<ProcessTicketModel[]> {
    const url = `${this.serviceUrl}/getListHistoryProcessTickets`;
    return this.postRequest(url, processTicket);
  }

  getTicketProcessAttachment(ticketRequestId: any):Observable<any>{
    const url = `${this.serviceUrl}/get-ticket-process-attachment/${ticketRequestId}`;
    return this.getRequest(url);
  }

  deleteProcessAttachment(processTicketAttachment:any): Observable<any>{
    const url = `${this.serviceUrl}/delete-process-ticket-attachment`;
    return this.postRequest(url, processTicketAttachment);
  }
  getListFileTicketProcessAttachment(processTicket: ProcessTicketModel): Observable<ProcessTicketModel[]> {
    const url = `${this.serviceUrl}/getListFileTicketProcessAttachment`;
    return this.postRequest(url, processTicket);
  }

  uploadFile(file: File[]): Observable<any> {
    const url = `${this.serviceUrl}/upload-file`;
    const formData = new FormData();
    for (const f of file) {
      formData.append('files', f);
    }
    return this.postRequest(url, formData);
  }

  downloadFile(fileName: string) {
    return this.httpClient
      .get<any>(`${this.serviceUrl}/download-file-attachment/${fileName}`, { responseType: 'arraybuffer' as 'json' });
  }

}
