import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {HelperService} from '@shared/services/helper.service';
import {BasicService} from '@core/services/basic.service';
import {CommonService} from '@shared/common/common.service';
import {environment} from '@env/environment';
import {TicketModel} from '@core/models/ticket-model';
import {TicketRequestModel} from '@core/models/ticket-request-model';

@Injectable({
  providedIn: 'root'
})

export class TicketRequestService extends BasicService {
  configData: BehaviorSubject<any> = new BehaviorSubject<any>(null);
  configData$: Observable<any> = this.configData.asObservable();
  constructor(public httpClient: HttpClient, public helperService: HelperService, private commonService: CommonService) {
  super(environment.serverUrl.api, '/ticket-resource', httpClient, helperService);
}

  getTicketRequestByTicketId(ticket: TicketModel): Observable<TicketRequestModel[]>{
      const url = `${this.serviceUrl}/getTicketRequestByTicketId`;
      return this.postRequest(url,ticket);
    }

}
