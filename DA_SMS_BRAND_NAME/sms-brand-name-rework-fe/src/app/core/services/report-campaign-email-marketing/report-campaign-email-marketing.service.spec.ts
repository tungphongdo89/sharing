import { TestBed } from '@angular/core/testing';

import { ReportCampaignEmailMarketingService } from './report-campaign-email-marketing.service';

describe('ReportCampaignEmailMarketingService', () => {
  let service: ReportCampaignEmailMarketingService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ReportCampaignEmailMarketingService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
