import { TestBed } from '@angular/core/testing';

import { ReportProductivityCalloutCampaignService } from './report-productivity-callout-campaign.service';

describe('ReportProductivityCalloutCampaignService', () => {
  let service: ReportProductivityCalloutCampaignService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ReportProductivityCalloutCampaignService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
