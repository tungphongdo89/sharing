import { Injectable } from '@angular/core';
import { BasicService } from '@core/services/basic.service';
import { HttpClient } from '@angular/common/http';
import { HelperService } from '@shared/services/helper.service';
import { environment } from '@env/environment';
import { CommonUtils } from '@shared/services';

@Injectable({
  providedIn: 'root'
})
export class ReportProductivityCalloutCampaignService extends BasicService {

  constructor(
    public httpClient: HttpClient,
    public helperService: HelperService,
  ) {
    super(environment.serverUrl.api, '/report-productivity-callout-campaign', httpClient, helperService);
  }

  doSearch(page: any, campaign: any, operator: any, fromDate: any, toDate: any) {
    const url = `${this.serviceUrl}/search`;
    const formData: FormData = new FormData();
    formData.append('campaign', campaign ? campaign : null);
    formData.append('operator', operator ? operator : null);
    formData.append('fromDate', fromDate ? fromDate : '');
    formData.append('toDate', toDate ? toDate : '');
    this.credentials = Object.assign({}, page);
    const searchData = CommonUtils.convertData(this.credentials);
    const buildParams = CommonUtils.buildParams(searchData);
    return this.httpClient.post<any>(url, formData, {
      params: buildParams,
      observe: 'response',
    });
  }

  export(campaign: any, operator: any, fromDate: any, toDate: any) {
    const url = `${this.serviceUrl}/export`;
    const formData: FormData = new FormData();
    formData.append('campaign', campaign ? campaign : null);
    formData.append('operator', operator ? operator : null);
    formData.append('fromDate', fromDate ? fromDate : '');
    formData.append('toDate', toDate ? toDate : '');
    return this.httpClient.post(url, formData,{
      observe: 'response',
      responseType: 'arraybuffer',
    });
  }
}
