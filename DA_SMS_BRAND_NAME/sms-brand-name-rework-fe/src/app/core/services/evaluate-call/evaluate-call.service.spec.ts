import { TestBed } from '@angular/core/testing';

import { EvaluateCallService } from './evaluate-call.service';

describe('EvaluateCallService', () => {
  let service: EvaluateCallService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EvaluateCallService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
