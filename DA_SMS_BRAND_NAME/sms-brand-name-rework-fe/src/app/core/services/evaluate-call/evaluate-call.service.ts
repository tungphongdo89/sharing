import { Injectable } from '@angular/core';
import {BasicService} from '@core/services/basic.service';
import {HttpClient} from '@angular/common/http';
import {HelperService} from '@shared/services/helper.service';
import {environment} from '@env/environment';

@Injectable({
  providedIn: 'root'
})
export class EvaluateCallService extends BasicService{

  constructor(
    public httpClient: HttpClient,
    public helperService: HelperService,
  ) {
    super(environment.serverUrl.api, '/evaluate-assignments', httpClient, helperService);
  }
}
