import { TestBed } from '@angular/core/testing';

import { ConfigAutoEmailService } from './config-auto-email.service';

describe('ConfigAutoEmailService', () => {
  let service: ConfigAutoEmailService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ConfigAutoEmailService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
