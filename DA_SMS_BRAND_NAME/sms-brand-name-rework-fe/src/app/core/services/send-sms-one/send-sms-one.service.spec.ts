import { TestBed } from '@angular/core/testing';

import { SendSmsOneService } from './send-sms-one.service';

describe('SendSmsOneService', () => {
  let service: SendSmsOneService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SendSmsOneService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
