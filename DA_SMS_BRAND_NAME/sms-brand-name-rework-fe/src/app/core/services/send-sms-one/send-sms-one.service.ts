import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {HelperService} from '@shared/services/helper.service';
import {CommonService} from '@shared/common/common.service';
import {environment} from '@env/environment';
import {Observable} from 'rxjs';
import {BasicService} from '@core/services/basic.service';

@Injectable({
  providedIn: 'root'
})
export class SendSmsOneService extends BasicService {

  constructor(public httpClient: HttpClient,
              public helperService: HelperService,
              private commonService: CommonService) {
    super(environment.serverUrl.api, '/campaign-sms-batches', httpClient, helperService);
  }


  addSMS(listSMS?: any):Observable<any>{
    const url = `${this.serviceUrl}/create`;
    return this.httpClient.post(url, listSMS, {
        observe:'response',
      }
    );
  }
}
