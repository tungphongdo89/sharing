import { TestBed } from '@angular/core/testing';

import { CriteriaDetailService } from './criteria-detail.service';

describe('CriteriaDetailService', () => {
  let service: CriteriaDetailService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CriteriaDetailService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
