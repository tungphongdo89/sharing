import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {HelperService} from '@shared/services/helper.service';
import {CommonService} from '@shared/common/common.service';
import {environment} from '@env/environment';
import {BasicService} from '@core/services/basic.service';
import {Observable} from 'rxjs';
import {createRequestOption} from '@core/models/campaign-blacklist.interface';

@Injectable({
  providedIn: 'root'
})
export class CriteriaDetailService extends BasicService {

  constructor(public httpClient: HttpClient,
              public helperService: HelperService,
              private commonService: CommonService) {
    super(environment.serverUrl.api, '/criteriaDetails', httpClient, helperService);
  }

  getAllCriteriaDetail(page?: any): Observable<any> {
    const options = createRequestOption(page);
    const url = `${this.serviceUrl}`;
    return this.httpClient.get(url, {
        params: options,
        observe: 'response',
      },
    );
  }

  addCriteriaDetail(criteriaDetail?: any): Observable<any> {
    const url = `${this.serviceUrl}`;
    return this.httpClient.post(url, criteriaDetail, {
        observe: 'response',
      }
    );
  }

  updateCriteriaDetail(criteriaDetail?: any): Observable<any> {
    const url = `${this.serviceUrl}`;
    return this.httpClient.put(url, criteriaDetail, {
        observe: 'response',
      }
    );
  }

  deleteCriteriaDetail(criteriaDetail?: any): Observable<any> {
    const url = `${this.serviceUrl}/delete`;
    return this.httpClient.put(url, criteriaDetail, {
        observe: 'response',
      }
    );
  }


}
