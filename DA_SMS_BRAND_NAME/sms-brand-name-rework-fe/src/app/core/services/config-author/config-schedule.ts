import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {environment} from '@env/environment';
import {HelperService} from '@shared/services/helper.service';
import {BasicService} from '@core/services/basic.service';
import {CommonService} from '@shared/common/common.service';
import {ConfigScheduleModel} from '@core/models/config-schedule-model';
import {ConfigScheduleResponse} from "@core/models/config_schedule_response_code";
import {AuthorizationInfo} from "@core/models/authorization-info";
import {ServiceResultModel} from "@core/models/service-result-model";
import {createRequestOption} from "@core/utils/request-util";
import {finalize} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})

export class ConfigSchedule extends BasicService {
  configData: BehaviorSubject<any> = new BehaviorSubject<any>(null);
  configData$: Observable<any> = this.configData.asObservable();
  constructor(public httpClient: HttpClient, public helperService: HelperService, private commonService: CommonService) {
    super(environment.serverUrl.api, '/config-schedules', httpClient, helperService);
  }

  getAllConfigSchedule(): Observable<ConfigScheduleModel[]>{
    const url = `${this.serviceUrl}`;
    return this.getRequest(url);
  }

  addConfigSchedule(configSchedule: ConfigScheduleModel): Observable<any>{
    const url = `${this.serviceUrl}`;
    // return this.postRequest(url, configSchedule);
    return this.httpClient.post<any>(url, configSchedule, {
      observe: 'response'
    });
  }

  searchText(configModel: ConfigScheduleModel, req?: any): Observable<any> {
    const options = createRequestOption(req);
    const url = `${this.serviceUrl}/search`;
    return this.postRequestFullResponse(url, configModel, options);
  }

  searchConfigSchedule(id: number): Observable<ConfigScheduleModel> {
    const url = `${this.serviceUrl}/${id}`;
    return this.getRequest(url);
  }

  updateConfigSchedule(configModel: ConfigScheduleModel): Observable<any>{
    const url = `${this.serviceUrl}`;
    return this.httpClient.put<any>(url, configModel, {
      observe: 'response'
    });
  }
  uploadFile(file: File){
    const url = `${this.serviceUrl}/uploadFile`;
    const formData = new FormData();
    formData.append('file', file);

    return this.httpClient.post(url, formData, {
      observe: 'response',
      responseType: 'arraybuffer'
    });
  }
  getProcessTime(configModel: ConfigScheduleModel): Observable<ServiceResultModel>{
    const url = `${this.serviceUrl}/getProcessTime`;
    return this.postRequest(url, configModel);
  }

  deleteConfigSchedule(id: number): Observable<any>{
    const url = `${this.serviceUrl}/${id}`;
    return this.deleteRequest(url);
  }

  export(configModel: any) {
    const url = `${this.serviceUrl}/export`;
    this.commonService.downloadFile(url, configModel, null, 'configuration-schedule', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;');
  }
}
