import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {environment} from '@env/environment';
import {HelperService} from '@shared/services/helper.service';
import {BasicService} from '@core/services/basic.service';
import {CommonService} from '@shared/common/common.service';
import {AuthorizationInfo} from '@core/models/authorization-info';
import {UpdateUserResponse} from '@core/models/UpdateUserResponse';

@Injectable({
  providedIn: 'root'
})
export class AuthorzirationVoc extends BasicService{
  authorData: BehaviorSubject<any> = new BehaviorSubject<any>(null);
  authorData$: Observable<any> = this.authorData.asObservable();
  constructor(public httpClient: HttpClient, public helperService: HelperService, private commonService: CommonService) {
    super(environment.serverUrl.api, '/users', httpClient, helperService);
  }

  getAllUsers(): Observable<AuthorizationInfo[]>{
    const url = `${this.serviceUrl}`;
    return this.getRequest(url);
  }
  updateAuthorzirationVOC(author: AuthorizationInfo): Observable<UpdateUserResponse> {
    const url = `${this.serviceUrl}?userId=${author.id}`;
    return this.putRequest(url, author);
  }
  searchUserByKeyword(data: AuthorizationInfo, page?: any): Observable<any> {
    const url = `${this.serviceUrl}/query`;
    return this.postRequestFullResponse(url, data, page);
  }
}
