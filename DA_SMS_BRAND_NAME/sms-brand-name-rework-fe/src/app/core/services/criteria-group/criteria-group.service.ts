import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {HelperService} from '@shared/services/helper.service';
import {CommonService} from '@shared/common/common.service';
import {environment} from '@env/environment';
import {BasicService} from '@core/services/basic.service';
import {Observable} from 'rxjs';
import {createRequestOption} from '@core/models/campaign-blacklist.interface';

@Injectable({
  providedIn: 'root'
})
export class CriteriaGroupService extends BasicService {

  constructor(public httpClient: HttpClient,
              public helperService: HelperService,
              private commonService: CommonService) {
    super(environment.serverUrl.api, '/criteriaGroup', httpClient, helperService);
  }

  getAllCriteriaGroup(page?: any): Observable<any> {
    const options = createRequestOption(page);
    const url = `${this.serviceUrl}`;
    return this.httpClient.get(url, {
        params: options,
        observe: 'response',
      },
    );
  }

  loadCriteriaGroupToCbx(): Observable<any> {
    const url = `${this.serviceUrl}/list`;
    return this.httpClient.get(url, {
        observe: 'response',
      },
    );
  }

  addCriteriaGroup(criteriaGroup?: any): Observable<any> {
    const url = `${this.serviceUrl}`;
    return this.httpClient.post(url, criteriaGroup, {
        observe: 'response',
      }
    );
  }

  updateCriteriaGroup(criteriaGroup?: any): Observable<any> {
    const url = `${this.serviceUrl}`;
    return this.httpClient.put(url, criteriaGroup, {
        observe: 'response',
      }
    );
  }

  deleteCriteriaGroup(criteriaGroup?: any): Observable<any> {
    const url = `${this.serviceUrl}/delete`;
    return this.httpClient.put(url, criteriaGroup, {
        observe: 'response',
      }
    );
  }

  getRemainingScore(criteriaGroupId: any, criteriaId: any) {
    const url = `${this.serviceUrl}/getRemainingScore`;
    const formData: FormData = new FormData();
    formData.append('criteriaGroupId', criteriaGroupId ? criteriaGroupId : -1);
    formData.append('criteriaId', criteriaId ? criteriaId : -1);
    return this.httpClient.post(url, formData, {
        observe: 'response',
      }
    );
  }

  findCriteriaGroupDetail(): Observable<any> {
    const url = `${this.serviceUrl}/detail`;
    return this.getRequest(url, {});
  }

  loadToCbx(): Observable<any> {
    const url = `${this.serviceUrl}/loadToCbx`;
    return this.httpClient.get(url, {
        observe: 'response',
      },
    );
  }
}
