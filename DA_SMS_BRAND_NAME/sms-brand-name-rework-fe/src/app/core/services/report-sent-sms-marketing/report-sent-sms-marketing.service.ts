import { Injectable } from '@angular/core';
import { BasicService } from '@core/services/basic.service';
import { HttpClient } from '@angular/common/http';
import { HelperService } from '@shared/services/helper.service';
import { environment } from '@env/environment';
import { CommonUtils } from '@shared/services';

@Injectable({
  providedIn: 'root',
})
export class ReportSentSmsMarketingService extends BasicService {

  constructor(
    public httpClient: HttpClient,
    public helperService: HelperService,
  ) {
    super(environment.serverUrl.api, '/report-sent-sms-marketing', httpClient, helperService);
  }

  doSearch(page: any, campaign: any, fromDate: any, toDate: any, phoneNumber: any, status: any) {
    const url = `${this.serviceUrl}/search`;
    const formData: FormData = new FormData();
    formData.append('campaign', campaign ? campaign : null);
    formData.append('fromDate', fromDate ? fromDate : '');
    formData.append('toDate', toDate ? toDate : '');
    formData.append('phoneNumber', phoneNumber ? phoneNumber : '');
    formData.append('status', status ? status : '');
    this.credentials = Object.assign({}, page);
    const searchData = CommonUtils.convertData(this.credentials);
    const buildParams = CommonUtils.buildParams(searchData);
    return this.httpClient.post<any>(url, formData, {
      params: buildParams,
      observe: 'response',
    });
  }

  export( campaign: any, fromDate: any, toDate: any, phoneNumber: any, status: any) {
    const url = `${this.serviceUrl}/export`;
    const formData: FormData = new FormData();
    formData.append('campaign', campaign ? campaign : null);
    formData.append('fromDate', fromDate ? fromDate : '');
    formData.append('toDate', toDate ? toDate : '');
    formData.append('phoneNumber', phoneNumber ? phoneNumber : '');
    formData.append('status', status ? status : '');
    return this.httpClient.post(url, formData,{
      observe: 'response',
      responseType: 'arraybuffer',
    });
  }

}
