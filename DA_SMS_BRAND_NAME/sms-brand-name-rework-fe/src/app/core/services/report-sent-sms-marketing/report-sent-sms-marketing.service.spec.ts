import { TestBed } from '@angular/core/testing';

import { ReportSentSmsMarketingService } from './report-sent-sms-marketing.service';

describe('ReportSentSmsMarketingService', () => {
  let service: ReportSentSmsMarketingService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ReportSentSmsMarketingService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
