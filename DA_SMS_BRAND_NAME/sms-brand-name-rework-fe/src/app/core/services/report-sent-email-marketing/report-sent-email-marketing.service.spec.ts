import { TestBed } from '@angular/core/testing';

import { ReportSentEmailMarketingService } from './report-sent-email-marketing.service';

describe('ReportSentEmailMarketingService', () => {
  let service: ReportSentEmailMarketingService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ReportSentEmailMarketingService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
