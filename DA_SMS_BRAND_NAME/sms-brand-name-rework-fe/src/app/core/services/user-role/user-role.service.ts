import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {HelperService} from '@shared/services/helper.service';
import {environment} from '@env/environment';
import {BasicService} from '@core/services/basic.service';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserRoleService extends BasicService {

  constructor(
    public httpClient: HttpClient,
    public helperService: HelperService,
  ) {
    super(environment.serverUrl.api, '/user-role', httpClient, helperService);
  }

  getUserRole(data: any, pageable: any): Observable<any> {
    const url = `${this.serviceUrl}/role`;
    return this.postRequestFullResponse(url, data, pageable);
  }

  createUserRole(data: any): Observable<any> {
    const url = `${this.serviceUrl}`;
    return this.postRequest(url, data);
  }

  removeUserRole(data: any): Observable<any> {
    const url = `${this.serviceUrl}`;
    return this.putRequest(url, data);
  }

}
