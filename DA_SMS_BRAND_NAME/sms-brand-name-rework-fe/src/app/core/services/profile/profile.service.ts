import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Login} from '@app/core/auth/login.service';
import {Observable} from 'rxjs';
import {environment} from '@env/environment';

@Injectable({
  providedIn: 'root'
})
export class ProfileService {
  constructor(
    public httpClient: HttpClient,
  ) {
  }

  getMyProfile(): Observable<any> {
    const url = environment.serverUrl.api + '/account';
    // const options = createRequestOption(page);
    return this.httpClient.get(url, {
      // params: options,
      observe: 'response'
    });
  }

  updateProfile(userData: any): Observable<any> {
    const url = environment.serverUrl.api + '/profile';

    return this.httpClient.put(url, userData, {
      observe: 'response'
    });
  }

  checkLoginError(credentials: Login): Observable<any> {
    const url = environment.serverUrl.api + '/checkLogin';
    return this.httpClient.post(url, credentials, {
      observe: 'response'
    });
  }

  changeMyPassword(data: any): Observable<any> {
    const url = environment.serverUrl.api + `/change-my-password`;
    return this.httpClient.post(url, data, {
      observe: 'response'
    });
  }

  retrieveMyPassword(login: string): Observable<any> {
    const url = environment.serverUrl.api + `/${login}/retrieve-my-password`;
    return this.httpClient.post(url, {
      observe: 'response'
    });
  }

}
