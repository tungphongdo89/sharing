import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {HelperService} from '@shared/services/helper.service';
import {environment} from '@env/environment';
import {BasicService} from '@core/services/basic.service';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ConfigMenuItemsService extends BasicService {

  constructor(
    public httpClient: HttpClient,
    public helperService: HelperService,
  ) {
    super(environment.serverUrl.api, '/config-menu-items', httpClient, helperService);
  }

  addFunctionMenu(data: any): Observable<any> {
    const url = `${this.serviceUrl}/addFunction`;
    return this.postRequest(url, data);
  }

  actionConfigMenuItem(data: any): Observable<any> {
    const url = `${this.serviceUrl}/action`;
    return this.postRequest(url, data);
  }

  createConfigMenuItem(data: any): Observable<any> {
    const url = `${this.serviceUrl}`;
    return this.postRequest(url, data);
  }

  updateConfigMenuItem(data: any): Observable<any> {
    const url = `${this.serviceUrl}`;
    return this.putRequest(url, data);
  }
}
