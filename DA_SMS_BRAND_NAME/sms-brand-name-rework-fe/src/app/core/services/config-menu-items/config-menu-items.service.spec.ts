import { TestBed } from '@angular/core/testing';

import { ConfigMenuItemsService } from './config-menu-items.service';

describe('ConfigMenuItemsService', () => {
  let service: ConfigMenuItemsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ConfigMenuItemsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
