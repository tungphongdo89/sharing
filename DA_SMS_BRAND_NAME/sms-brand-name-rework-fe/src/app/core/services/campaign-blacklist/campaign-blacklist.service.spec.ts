import { TestBed } from '@angular/core/testing';

import { CampaignBlacklistService } from './campaign-blacklist.service';

describe('CampaignBlacklistService', () => {
  let service: CampaignBlacklistService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CampaignBlacklistService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
