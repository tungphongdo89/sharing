import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {HelperService} from '@shared/services/helper.service';
import {CommonService} from '@shared/common/common.service';
import {environment} from '@env/environment';
import {Observable} from 'rxjs/Observable';
import {CampaignBlacklistInterface, createRequestOption} from '@core/models/campaign-blacklist.interface';

@Injectable({
  providedIn: 'root'
})
export class CampaignBlacklistService {

  constructor(
    public httpClient: HttpClient,
    public helperService: HelperService,
    private commonService: CommonService
  ) {
  }

  search(req?: any): Observable<any> {
    const options = createRequestOption(req);
    return this.httpClient.get<CampaignBlacklistInterface[]>(`${environment.serverUrl.api}/campaign-blacklist`, {
      params: options,
      observe: 'response',
    });
  }

  delete(id: any) {
    return this.httpClient.delete(`${environment.serverUrl.api}/campaign-blacklist/${id}`);
  }

  create(objData: CampaignBlacklistInterface) {
    return this.httpClient.post<CampaignBlacklistInterface>(`${environment.serverUrl.api}/campaign-blacklist`, objData);
  }
  getAllCampaign(req?: any): Observable<any> {
    return this.httpClient.get<CampaignBlacklistInterface[]>(`${environment.serverUrl.api}/campaigns`);
  }

  deleteListPhoneNumber(phoneNumberArr: CampaignBlacklistInterface[]) {
    return this.httpClient.put(`${environment.serverUrl.api}/campaign-blacklist`,phoneNumberArr);
  }
}
