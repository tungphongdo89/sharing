import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ConfigScheduleModel } from '@app/core/models/config-schedule-model';
import { CommonService } from '@app/shared/common/common.service';
import { HelperService } from '@app/shared/services/helper.service';
import { environment } from '@env/environment';
import { Observable } from 'rxjs';
import { BasicService } from '../basic.service';

@Injectable({
  providedIn: 'root'
})
export class CampaignScriptAnwserServiceService extends BasicService  {

  constructor(public httpClient: HttpClient, public helperService: HelperService, private commonService: CommonService) {
    super(environment.serverUrl.api, '/campaign-scripts-questions', httpClient, helperService);
  }

  getAnwserInQuestion(questionId?: number):Observable<any>{
    const url = `${this.serviceUrl}/${questionId}/answers`;
    return this.getRequest(url);
  }

  getPositionAnwser(questionId: number): Observable<any>{
    const url = `${this.serviceUrl}/${questionId}/answers/getMaxPosition`;
    return this.httpClient.get(url, {
        observe:'response',
      }
    );
  }

  addAnwser(questionId?: number,scriptQuestion?: any):Observable<any>{
    const url = `${this.serviceUrl}/${questionId}/answers`;
    return this.httpClient.post(url, scriptQuestion, {
        observe:'response',
      }
    );
  }

  updateAnwser(questionId?: number,anwserId?: number, scriptQuestion?: any):Observable<any>{
    const url = `${this.serviceUrl}/${questionId}/answers/${anwserId}`;
    return this.httpClient.put(url, scriptQuestion, {
        observe:'response',
      }
    );
  }

  deleteAnwser(questionId?: number,anwserId?: number):Observable<any>{
    const url = `${this.serviceUrl}/${questionId}/answers/${anwserId}`;
    return this.httpClient.delete(url, {
        observe:'response',
      }
    );
  }


}
