import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ConfigScheduleModel } from '@app/core/models/config-schedule-model';
import { CommonService } from '@app/shared/common/common.service';
import { HelperService } from '@app/shared/services/helper.service';
import { environment } from '@env/environment';
import { Observable } from 'rxjs';
import { BasicService } from '../basic.service';
import {createRequestOption} from '@core/models/campaign-blacklist.interface';

@Injectable({
  providedIn: 'root'
})
export class CampaignScriptService extends BasicService  {

  constructor(public httpClient: HttpClient, public helperService: HelperService, private commonService: CommonService) {
    super(environment.serverUrl.api, '/campaign-scripts', httpClient, helperService);
  }

  addScript(entity: any): Observable<any> {
    return this.postRequestFullResponse(this.serviceUrl, entity);
  }

  updateCampaignScript(scriptId: number, campaignSctipt: any): Observable<any>{
    const url = `${this.serviceUrl}/${scriptId}`;
    return this.httpClient.put(url, campaignSctipt, {
        observe:'response',
      }
    );
  }

  updateStatusCampaignScript(scriptId: number, campaignSctipt: any): Observable<any>{
    const url = `${this.serviceUrl}/${scriptId}/delete`;
    return this.httpClient.put(url, campaignSctipt, {
        observe:'response',
      }
    );
  }

  deleteCampaignScript(id: number): Observable<any>{
    const url = `${this.serviceUrl}/${id}`;
    return this.deleteRequest(url);
  }

  getQuestionInScript(reqDTO?: number,page?: any):Observable<any>{
    const options = createRequestOption(page);
    const url = `${this.serviceUrl}/${reqDTO}/questions`;
    return this.httpClient.get(url, {
        params: options,
        observe: 'response',
      },
    );
  }

  addQuestion(reqDTO?: number,scriptQuestion?: any):Observable<any>{
    const url = `${this.serviceUrl}/${reqDTO}/questions`;
    return this.httpClient.post(url, scriptQuestion, {
      observe:'response',
      }
    );
  }

  updateQuestion(scriptID?: number,questionId?: number, scriptQuestion?: any):Observable<any>{
    const url = `${this.serviceUrl}/${scriptID}/questions/${questionId}`;
    return this.httpClient.put(url, scriptQuestion, {
        observe:'response',
      }
    );
  }

  getLstCampaignScript(): Observable<any> {
    const url = `${this.serviceUrl}/getListCampaignScriptForCombobox`;
    return this.getRequest(url);
  }

  getCheckCopyScript(formInput?: any): Observable<any> {
    const url = `${this.serviceUrl}/getCheckCopyScript`;
    return this.httpClient.post(url, formInput,{
        observe:'response',
      }
    );
  }

  getAllCampaignScript(reqDTO?: any, page?: any): Observable<any> {
    const url = `${this.serviceUrl}/findAll`;
    return this.postRequestFullResponse(url, reqDTO, page);
  }

  deleteQuestion(scriptId?: number,questionId?: number):Observable<any>{
    const url = `${this.serviceUrl}/${scriptId}/questions/${questionId}`;
    return this.httpClient.delete(url, {
        observe:'response',
      }
    );
  }

  getPositionQuestion(scriptId: number): Observable<any>{
    const url = `${this.serviceUrl}/${scriptId}/questions/getMaxPosition`;
    return this.httpClient.get(url, {
        observe:'response',
      }
    );
  }

  uploadFile(scriptId: number,deleteQuestion: any,file: any) {
    const url = `${this.serviceUrl}/${scriptId}/uploadFile?deleteAllQuestion=${deleteQuestion}`;
    const formData: FormData = new FormData();
    const headers = new HttpHeaders();
    formData.append('file', file ? file : null);
    formData.append('campaignId', deleteQuestion ? deleteQuestion : null);
    headers.append('Content-type', 'multipart/form-data');
    const options = { headers };
    return this.httpClient.post(url, formData, options);
  }

  importFile(scriptId: number, deleteAllQuestion: any, file: any) {
    const url = `${this.serviceUrl}/${scriptId}/uploadFile`;
    const formData: FormData = new FormData();
    formData.append('file', file ? file : null);
    formData.append('deleteAllQuestion', deleteAllQuestion ? deleteAllQuestion : null);
    return this.httpClient.post(url, formData, {
      observe: 'response'
    });
  }

  downloadTemplate() {
    const url = `${this.serviceUrl}/download-template`;
    return this.httpClient.get(url, {
      observe: 'response',
      responseType: 'arraybuffer',
    });
  }

}
