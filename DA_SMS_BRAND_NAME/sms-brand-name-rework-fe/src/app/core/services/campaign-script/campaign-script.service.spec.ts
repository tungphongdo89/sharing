import { TestBed } from '@angular/core/testing';

import { CampaignScriptService } from './campaign-script.service';

describe('CampaignScriptService', () => {
  let service: CampaignScriptService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CampaignScriptService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
