import { Injectable } from '@angular/core';
import {BasicService} from '@core/services/basic.service';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {Observable} from 'rxjs/Observable';
import {HttpClient} from '@angular/common/http';
import {HelperService} from '@shared/services/helper.service';
import {CommonService} from '@shared/common/common.service';
import {environment} from '@env/environment';
import * as moment from 'moment';

@Injectable({
  providedIn: 'root'
})
export class CpTicketService extends BasicService {
  configData: BehaviorSubject<any> = new BehaviorSubject<any>(null);
  configData$: Observable<any> = this.configData.asObservable();
  constructor(public httpClient: HttpClient, public helperService: HelperService, private commonService: CommonService) {
    super(environment.serverUrl.api, '', httpClient, helperService);
  }
  query(reqDTO: any, page: any): Observable<any> {
    const url = `${this.serviceUrl}/querySearch`;
    return this.postRequestFullResponse(url, reqDTO, page);
  }
  export(reqDTO: any) {
    const url = `${this.serviceUrl}/export-ticket-not-process`;
    this.commonService.downloadFile(url, reqDTO, null, 'ThongTinSuVu_' + moment().format('DDMMyyyyhhmm'), 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;');
  }

  getTicketForEvaluate(id: any): Observable<any> {
    const url = `${this.serviceUrl}/tickets/get-for-evaluate/${id}`;
    return this.getRequest(url);
  }
}

