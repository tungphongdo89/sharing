import { TestBed } from '@angular/core/testing';

import { CpTicketService } from './cp-ticket.service';

describe('CpTicketService', () => {
  let service: CpTicketService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CpTicketService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
