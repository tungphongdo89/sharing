import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {environment} from '@env/environment';
import {HelperService} from '@shared/services/helper.service';
import {BasicService} from '@core/services/basic.service';
import {CommonService} from '@shared/common/common.service';

@Injectable({
  providedIn: 'root'
})
export class CpBrandManagementService extends BasicService {


  cpData: BehaviorSubject<any> = new BehaviorSubject<any>(null);
  cpData$: Observable<any> = this.cpData.asObservable();

  cpAliasData: BehaviorSubject<any> = new BehaviorSubject<any>(null);
  cpAliasData$: Observable<any> = this.cpAliasData.asObservable();

  constructor(public httpClient: HttpClient, public helperService: HelperService, private commonService: CommonService) {
    super(environment.serverUrl.api, '/cp-management/brand', httpClient, helperService);
  }

  setCpData(cpData: any) {
    this.cpData.next(cpData);
  }

  setCpAliasData(cpAliasData: any) {
    this.cpAliasData.next(cpAliasData);
  }

  getTelcos(): Observable<any> {
    const url = `${this.serviceUrl}/tecols`;
    return this.getRequest(url);
  }

  getAliasGroup(type): Observable<any> {
    const url = `${this.serviceUrl}/alias-group?type=${type}`;
    return this.getRequest(url);
  }

  getMainWS(status: number): Observable<any> {
    const url = `${this.serviceUrl}/ws?status=${status}`;
    return this.getRequest(url);
  }

  getKeepFee(): Observable<any> {
    const url = `${this.serviceUrl}/keep-fee`;
    return this.getRequest(url);
  }

  onSearch(cpId: number, aliasType: number, brandName: string, telco: string, start: number, limit: number): any {
    const url = `${this.serviceUrl}/search`;
    return this.getRequest(url, {params: {cpId, aliasType, brandName, telco, start, limit}});
  }

  activeOrInactive(cpBrandActionModel: any) {
    const url = `${this.serviceUrl}/active-or-inactive`;
    return this.postRequest(url, cpBrandActionModel);
  }

  deleteCpBrand(cpBrandApproveModel: any) {
    const url = `${this.serviceUrl}/delete`;
    return this.postRequest(url, cpBrandApproveModel);
  }

  createCpAlias(cpAlias: any, file: any, commissionDefault: string, checked: string, isUpdate: boolean, func?) {
    let url = `${this.serviceUrl}/create`;
    if (isUpdate) {
      url = `${this.serviceUrl}/edit`;
    }
    const formData: FormData = new FormData();
    if (file) {
      formData.append('file', file);
    }
    formData.append('data', JSON.stringify(cpAlias));
    formData.append('commissionDefault', commissionDefault);
    formData.append('checked', checked);
    const vobjHeaders = new HttpHeaders();
    vobjHeaders.append('Content-type', 'multipart/form-data');
    vobjHeaders.append('Accept', 'application/json');
    const options = {headers: vobjHeaders};
    return this.httpClient.post(url, formData, options);
  }

  downloadCpAttach(fileName: string, newFileName?: string) {
    return this.commonService.downloadFile(`${this.serviceUrl}/downloadAttach`, null, {fileName}, newFileName);
  }

}
