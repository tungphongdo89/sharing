import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {HelperService} from '@app/shared/services/helper.service';
import {environment} from '@env/environment';
import {Observable, of} from 'rxjs';
import {CommonUtils} from '@shared/services';
import {CommonService} from '@shared/common/common.service';
import {catchError, map} from 'rxjs/operators';
import {BasicService} from '@core/services/basic.service';

@Injectable({
  providedIn: 'root'
})
export class CpService extends BasicService {

  constructor(
    public httpClient: HttpClient,
    public helperService: HelperService,
    private commonService: CommonService
  ) {
    super(environment.serverUrl.api, '/cp', httpClient, helperService);
  }

  getCPforVirtual(status, currency) {
    const url = `${this.serviceUrl}/getCPPre`;
    return this.getRequest(url, {params: {status, currency}});
  }

  findAllProvince(): Observable<any> {
    const url = `${this.serviceUrl}/all-province`;
    return this.getRequest(url);
  }

  findDistrictByProvinceId(id: any): Observable<any> {
    const url = `${this.serviceUrl}/find-district-by-province/${id}`;
    return this.getRequest(url);
  }

  findCommissionPercentCode(): Observable<any> {
    const url = `${this.serviceUrl}/all-percent-code`;
    return this.getRequest(url);
  }

  add(entity: any, attachFile: any, monthCommissionAttachFile: any): Observable<any> {
    const url = `${this.serviceUrl}/add`;
    const formData: FormData = new FormData();
    const headers = new HttpHeaders();
    formData.append('attachFile', attachFile ? attachFile : null);
    formData.append('monthCommissionAttachFile', monthCommissionAttachFile ? monthCommissionAttachFile : null);
    formData.append('cpJson', JSON.stringify(entity));
    headers.append('Content-type', 'multipart/form-data');
    headers.append('Accept', 'application/json');
    const options = {headers};
    return this.httpClient.post(url, formData, options);
  }

  findCpById(id: number): Observable<any> {
    const url = `${this.serviceUrl}/find-by-id/${id}`;
    return this.getRequest(url);
  }

  update(entity: any, attachFile: any, monthCommissionAttachFile: any): Observable<any> {
    const url = `${this.serviceUrl}/update`;
    const formData: FormData = new FormData();
    const headers = new HttpHeaders();
    formData.append('attachFile', attachFile ? attachFile : null);
    formData.append('monthCommissionAttachFile', monthCommissionAttachFile ? monthCommissionAttachFile : null);
    formData.append('cpJson', JSON.stringify(entity));
    headers.append('Content-type', 'multipart/form-data');
    headers.append('Accept', 'application/json');
    const options = {headers};
    return this.httpClient.put(url, formData, options);
  }

  searchProvinceUser(options: any) {
    this.credentials = Object.assign({}, options);
    const searchData = CommonUtils.convertData(this.credentials);
    const buildParams = CommonUtils.buildParams(searchData);
    const url = `${this.serviceUrl}/search-province-user`;
    return this.getRequest(url, {params: buildParams});
  }

  searchStaff(options: any) {
    this.credentials = Object.assign({}, options);
    const searchData = CommonUtils.convertData(this.credentials);
    const buildParams = CommonUtils.buildParams(searchData);
    const url = `${this.serviceUrl}/search-staff`;
    return this.getRequest(url, {params: buildParams});
  }

  findCpByUser(): Observable<any> {
    const url = `${this.serviceUrl}/find-cp-by-user`;
    return this.getRequest(url);
  }

  deleteCpById(id: number): Observable<any> {
    const url = `${this.serviceUrl}/delete/${id}`;
    return this.deleteRequest(url);
  }

  searchAdjustCommissionHis(id: any): Observable<any> {
    const url = `${this.serviceUrl}/search-adjust-commission-his/${id}`;
    return this.getRequest(url);
  }

  downloadCpFiles(fileName: string, newFileName?: any) {
    return this.commonService.downloadFile(`${this.serviceUrl}/downloadCpFiles`, null, {fileName}, newFileName);
  }

  checkExistedOrderNoOrCpName(options: any): Observable<any> {
    const url = `${this.serviceUrl}/check-existed-order-or-name`;
    this.credentials = Object.assign({}, options);
    const searchData = CommonUtils.convertData(this.credentials);
    const buildParams = CommonUtils.buildParams(searchData);
    return this.getRequest(url, {params: buildParams});
  }

  findBccsProjects(): Observable<any> {
    const url = `${this.serviceUrl}/find-bccs-projects`;
    return this.getRequest(url);
  }

  getCustomerCodes(code: string): Observable<any> {
    const url = `${this.serviceUrl}/auto-complete?input=${code}`;
    return this.getRequest(url).pipe(
      catchError(() => of([])),
      map((res: any[]) => {
        return res.map(item => {
          return {
            label: item.cpCode,
            value: item.cpCode
          };
        });
      })
    );
  }

  exportCp(options: any, fileName: any) {
    const url = `${this.serviceUrl}/exportCp`;
    return this.commonService.downloadFile(url, options, null, fileName);
  }
}
