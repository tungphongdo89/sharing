import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {HelperService} from '@shared/services/helper.service';
import {CommonService} from '@shared/common/common.service';
import {Observable} from 'rxjs/Observable';
import {environment} from '@env/environment';
import {EmailConfigModel} from '@core/models/email-config.model';
import {EmailConfigHistoryModel} from "@core/models/email-config-history.model";

@Injectable({
  providedIn: 'root'
})
export class EmailConfigService {

  constructor(
    public httpClient: HttpClient,
    public helperService: HelperService,
    private commonService: CommonService
  ) {
  }

  findAll(): Observable<any> {
    return this.httpClient.get<EmailConfigModel>(`${environment.serverUrl.api}/email-config`);
  }

  updateEmailConfig(id: number, data: any) {
    return this.httpClient.put<EmailConfigModel>(`${environment.serverUrl.api}/email-config/${id}`, data);
  }
}
