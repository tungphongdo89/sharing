import { TestBed } from '@angular/core/testing';

import { CampaignSmsMarketingService } from './campaign-sms-marketing.service';

describe('CampaignSmsMarketingService', () => {
  let service: CampaignSmsMarketingService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CampaignSmsMarketingService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
