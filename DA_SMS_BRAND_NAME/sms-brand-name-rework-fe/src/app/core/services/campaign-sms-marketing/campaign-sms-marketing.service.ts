import { Injectable } from '@angular/core';
import { BasicService } from '@core/services/basic.service';
import { HttpClient } from '@angular/common/http';
import { HelperService } from '@shared/services/helper.service';
import { environment } from '@env/environment';
import { CommonUtils } from '@shared/services';

@Injectable({
  providedIn: 'root',
})
export class CampaignSmsMarketingService extends BasicService {

  constructor(
    public httpClient: HttpClient,
    public helperService: HelperService,
  ) {
    super(environment.serverUrl.api, '/campaign-sms-marketing', httpClient, helperService);
  }

  doSearch(page: any, name: any, startDate: any, endDate: any) {
    const url = `${this.serviceUrl}/search`;
    const formData: FormData = new FormData();
    formData.append('name', name ? name : null);
    formData.append('startDate', startDate ? startDate : '');
    formData.append('endDate', endDate ? endDate : '');
    this.credentials = Object.assign({}, page);
    const searchData = CommonUtils.convertData(this.credentials);
    const buildParams = CommonUtils.buildParams(searchData);
    return this.httpClient.post<any>(url, formData, {
      params: buildParams,
      observe: 'response',
    });
  }

  getAll() {
    const url = `${this.serviceUrl}/get-all`;
    return this.httpClient.get<any>(url, {
      observe: 'response',
    });
  }

  getAllSearch() {
    const url = `${this.serviceUrl}/get-all-search`;
    return this.httpClient.get<any>(url, {
      observe: 'response',
    });
  }

  getOne(id: any) {
    const url = `${this.serviceUrl}/get-one/${id}`;
    return this.httpClient.get(url, {
      observe: 'response',
    });
  }

  downloadTemplate() {
    const url = `${this.serviceUrl}/download-template`;
    return this.httpClient.get(url, {
      observe: 'response',
      responseType: 'arraybuffer',
    });
  }

  save(data: any) {
    const url = `${this.serviceUrl}/save`;
    return this.httpClient.post(url, data, {
      observe: 'response',
    });
  }

  delete(id: any) {
    const url = `${this.serviceUrl}/delete/${id}`;
    return this.httpClient.delete(url, {
      observe: 'response',
    });
  }

  importFile(file: any, id: any, distinct: any) {
    const url = `${this.serviceUrl}/import`;
    const formData: FormData = new FormData();
    formData.append('file', file ? file : null);
    formData.append('id', id ? id : null);
    formData.append('duplicateFilter', distinct);
    return this.httpClient.post(url, formData, {
      observe: 'response',
    });
  }
}
