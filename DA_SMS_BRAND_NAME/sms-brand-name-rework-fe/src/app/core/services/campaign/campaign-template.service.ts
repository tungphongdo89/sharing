import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CommonService } from '@app/shared/common/common.service';
import { HelperService } from '@app/shared/services/helper.service';
import { environment } from '@env/environment';
import { Observable } from 'rxjs';
import { BasicService } from '../basic.service';

@Injectable({
  providedIn: 'root'
})
export class CampaignTemplateService extends BasicService  {

  constructor(public httpClient: HttpClient, public helperService: HelperService, private commonService: CommonService) {
    super(environment.serverUrl.api, '/campaign-templates', httpClient, helperService);
  }

  getLstTemplateScript(): Observable<any> {
    const url = `${this.serviceUrl}/getListCampaignTemplateForCombobox`;
    return this.getRequest(url);
  }
}
