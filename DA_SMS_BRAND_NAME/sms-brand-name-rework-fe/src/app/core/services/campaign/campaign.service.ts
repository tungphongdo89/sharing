import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ConfigScheduleModel } from '@app/core/models/config-schedule-model';
import { CommonService } from '@app/shared/common/common.service';
import { HelperService } from '@app/shared/services/helper.service';
import { environment } from '@env/environment';
import { Observable } from 'rxjs';
import { BasicService } from '../basic.service';

@Injectable({
  providedIn: 'root'
})
export class CampaignService extends BasicService  {

  constructor(public httpClient: HttpClient,
    public helperService: HelperService,
    private commonService: CommonService) {
    super(environment.serverUrl.api, '/campaigns', httpClient, helperService);
  }

  addCampaign(entity: any): Observable<any> {
    return this.postRequestFullResponse(this.serviceUrl, entity);
  }

  getAllCampaigns(): Observable<ConfigScheduleModel[]>{
    const url = `${this.serviceUrl}`;
    return this.getRequest(url);
  }

  query(reqDTO: any, page: any): Observable<any> {
    const url = `${this.serviceUrl}/query`;
    return this.postRequestFullResponse(url, reqDTO, page);
  }

  getAllCampaign(reqDTO?: any, page?: any): Observable<any> {
    const url = `${this.serviceUrl}/findAll`;
    return this.postRequestFullResponse(url, reqDTO, page);
  }

  updateCampaign(campaignID: any): Observable<any>{
    const url = `${this.serviceUrl}`;
    return this.putRequest(url, campaignID);
  }

  deleteCampaign(id:any):Observable<any> {
    const url = `${this.serviceUrl}/${id}`;
    return this.deleteRequest(url);
  }
}
