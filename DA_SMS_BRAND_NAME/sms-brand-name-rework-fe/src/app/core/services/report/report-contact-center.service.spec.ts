import { TestBed } from '@angular/core/testing';

import { ReportContactCenterService } from './report-contact-center.service';

describe('ReportContactCenterService', () => {
  let service: ReportContactCenterService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ReportContactCenterService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
