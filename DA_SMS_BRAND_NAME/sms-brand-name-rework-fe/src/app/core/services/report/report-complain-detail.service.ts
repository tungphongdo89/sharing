import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {Observable} from 'rxjs/Observable';
import {HttpClient} from '@angular/common/http';
import {HelperService} from '@shared/services/helper.service';
import {CommonService} from '@shared/common/common.service';
import {environment} from '@env/environment';
import {BasicService} from '@core/services/basic.service';
import {createRequestOption} from '@core/utils/request-util';

@Injectable({
  providedIn: 'root'
})
export class ReportComplainDetailService extends BasicService {
  configData: BehaviorSubject<any> = new BehaviorSubject<any>(null);
  configData$: Observable<any> = this.configData.asObservable();

  constructor(public httpClient: HttpClient, public helperService: HelperService, private commonService: CommonService) {
    super(environment.serverUrl.api, '/export-report', httpClient, helperService);
  }

  query(reqDTO: any, page: any): Observable<any> {
    const url = `${this.serviceUrl}/query-complaint-detail`;
    return this.postRequestFullResponse(url, reqDTO, page);
  }
  export(reqDTO: any) {
    const url = `${this.serviceUrl}/export-complaint-detail`;
    this.commonService.downloadFile(url, reqDTO, null, 'report-complaint-detail', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;');
  }
}
