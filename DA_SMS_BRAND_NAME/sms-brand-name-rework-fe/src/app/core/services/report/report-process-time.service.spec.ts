import { TestBed } from '@angular/core/testing';

import { ReportProcessTimeService } from './report-process-time.service';

describe('ReportProcessTimeService', () => {
  let service: ReportProcessTimeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ReportProcessTimeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
