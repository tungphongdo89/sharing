import { Injectable } from '@angular/core';
import {BasicService} from '@core/services/basic.service';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {Observable} from 'rxjs/Observable';
import {HttpClient} from '@angular/common/http';
import {HelperService} from '@shared/services/helper.service';
import {CommonService} from '@shared/common/common.service';
import {environment} from '@env/environment';

@Injectable({
  providedIn: 'root'
})
export class ReportComplaintService extends BasicService {
  configData: BehaviorSubject<any> = new BehaviorSubject<any>(null);
  configData$: Observable<any> = this.configData.asObservable();

  constructor(public httpClient: HttpClient, public helperService: HelperService, private commonService: CommonService) {
    super(environment.serverUrl.api, '/export-report', httpClient, helperService);
  }

  query(reqDTO: any, page: any): Observable<any> {
    const url = `${this.serviceUrl}/query-productivity`;
    return this.postRequestFullResponse(url, reqDTO, page);
  }
  queryDetail(reqDTO: any, page: any): Observable<any> {
    const url = `${this.serviceUrl}/query-productivity-detail`;
    return this.postRequestFullResponse(url, reqDTO, page);
  }
  export(reqDTO: any) {
    const url = `${this.serviceUrl}/export-productivity`;
    this.commonService.downloadFile(url, reqDTO, null, 'Bao_cao_nang_suat_tdv', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;');
  }
}
