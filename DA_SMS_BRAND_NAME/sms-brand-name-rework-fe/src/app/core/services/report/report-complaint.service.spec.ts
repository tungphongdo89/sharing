import { TestBed } from '@angular/core/testing';

import { ReportComplaintService } from './report-complaint.service';

describe('ReportComplaintService', () => {
  let service: ReportComplaintService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ReportComplaintService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
