import { TestBed } from '@angular/core/testing';

import { ReportComplainDetailService } from './report-complain-detail.service';

describe('ReportComplainDetailService', () => {
  let service: ReportComplainDetailService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ReportComplainDetailService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
