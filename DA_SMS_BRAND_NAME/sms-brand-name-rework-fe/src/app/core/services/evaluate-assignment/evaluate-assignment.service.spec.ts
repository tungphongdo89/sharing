import { TestBed } from '@angular/core/testing';

import { EvaluateAssignmentService } from './evaluate-assignment.service';

describe('EvaluateAssignmentService', () => {
  let service: EvaluateAssignmentService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EvaluateAssignmentService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
