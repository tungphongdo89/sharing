import {Injectable} from '@angular/core';
import {BasicService} from '@core/services/basic.service';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {HelperService} from '@shared/services/helper.service';
import {environment} from '@env/environment';

@Injectable({
  providedIn: 'root'
})
export class EvaluateAssignmentService extends BasicService {

  constructor(
    public httpClient: HttpClient,
    public helperService: HelperService,
  ) {
    super(environment.serverUrl.api, '/evaluate-assignments', httpClient, helperService);
  }

  getUsersByType(type: any): Observable<any> {
    const url = `${environment.serverUrl.api}/admin/usersByType?type=${type}`;
    return this.postRequest(url, {});
  }

  createEvaluateAssignment(data: any): Observable<any> {
    const url = `${this.serviceUrl}`;
    return this.postRequest(url, data);
  }

  getAllEvaluateAssignments(data: any, pageable: any): Observable<any> {
    const url = `${this.serviceUrl}/all`;
    return this.postRequestFullResponse(url, data, pageable);
  }

  onSearch(data: any, pageable: any): Observable<any> {
    const url = `${this.serviceUrl}/search`;
    return this.postRequestFullResponse(url, data, pageable);
  }

  getEvaluateAssignmentsDetail(data: any): Observable<any> {
    const url = `${this.serviceUrl}/${data}`;
    return this.getRequest(url);
  }

  updateEvaluateAssignment(data: any): Observable<any> {
    const url = `${this.serviceUrl}`;
    return this.putRequest(url, data);
  }

  deleteEvaluateAssignment(id: any): Observable<any> {
    const url = `${this.serviceUrl}/${id}`;
    return this.deleteRequest(url);
  }
}
