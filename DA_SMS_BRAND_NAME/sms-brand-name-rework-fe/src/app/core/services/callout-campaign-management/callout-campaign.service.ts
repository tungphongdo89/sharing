import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '@env/environment';

@Injectable({
  providedIn: 'root'
})
export class CalloutCampaignService {

  constructor(private http: HttpClient) {
  }

  findAllTemplate(): Observable<any> {
    return this.http.get(`${environment.serverUrl.api}/campaign-templates`);
  }

  findAllTemplateDetailByCampaignId(id: any): Observable<any> {
    return this.http.get(`${environment.serverUrl.api}/campaign-template-detail/find-by-campaign-id/${id}`);
  }

  insertTemplate(template: any): Observable<any> {
    return this.http.post(`${environment.serverUrl.api}/campaign-templates`, template);
  }

  copyTemplate(template: any): Observable<any> {
    return this.http.post(`${environment.serverUrl.api}/campaign-templates/copy`, template);
  }

  updateTemplate(template: any): Observable<any> {
    return this.http.put(`${environment.serverUrl.api}/campaign-templates/${template.id}`, template, {observe: 'response'});
  }

  deleteTemplate(id: any): Observable<any> {
    return this.http.put(`${environment.serverUrl.api}/campaign-templates/delete/${id}`, null);
  }

  insertTemplateDetail(templateDetail: any) {
    return this.http.post(`${environment.serverUrl.api}/campaign-template-detail`, templateDetail);
  }

  updateTemplateDetail(templateDetail: any) {
    return this.http.put(`${environment.serverUrl.api}/campaign-template-detail/${templateDetail.id}`, templateDetail);
  }

  deleteTemplateDetail(templateDetailId: any) {
    return this.http.delete(`${environment.serverUrl.api}/campaign-template-detail/${templateDetailId}`);
  }

  getAllInfoCode(): Observable<any> {
    return this.http.get(`${environment.serverUrl.api}/campaign-templates/get-info-code`);
  }
}
