import { Injectable } from '@angular/core';
import {BasicService} from '@core/services/basic.service';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {Observable} from 'rxjs/Observable';
import {HttpClient} from '@angular/common/http';
import {HelperService} from '@shared/services/helper.service';
import {CommonService} from '@shared/common/common.service';
import {environment} from '@env/environment';

@Injectable({
  providedIn: 'root'
})
export class KmsDocumentPostService  extends BasicService {
  configData: BehaviorSubject<any> = new BehaviorSubject<any>(null);
  configData$: Observable<any> = this.configData.asObservable();

  constructor(public httpClient: HttpClient, public helperService: HelperService, private commonService: CommonService) {
    super(environment.serverUrl.api, '/document-posts', httpClient, helperService);
  }

  query(reqDTO: any, page: any): Observable<any> {
    const url = `${this.serviceUrl}`;
    return this.httpClient.get(url, {
      params: reqDTO,
      observe: 'response'
    });
  }
  queryUser(reqDTO: any, postId): Observable<any> {
    const url = `${environment.serverUrl.api}/document-post-views/search/${postId}`;
    return this.httpClient.get(url, {
      params: reqDTO,
      observe: 'response'
    });
  }
  approve(reqDTO: any): Observable<any> {
    const url = `${this.serviceUrl}/approve`;
    return this.httpClient.post(url,  reqDTO);
  }
  getBookMark(reqDTO: any, page: any): Observable<any> {
    const url = `${this.serviceUrl}/get-bookmarks`;
    return this.getRequest(url, {
      params: reqDTO,
    }, page);
  }
  getNewest(reqDTO: any, page: any): Observable<any> {
    const url = `${this.serviceUrl}/get-new-post`;
    return this.getRequest(url, {
      params: reqDTO,
    }, page);
  }
  getMostView(reqDTO: any): Observable<any> {
    const url = `${this.serviceUrl}/top-view`;
    return this.getRequest(url, {
      params: reqDTO,
    });
  }
  getViewPost(id): Observable<any> {
    const url = `${this.serviceUrl}/view-document/${id}`;
    return this.getRequest(url);
  }
  bookmarkViewPost(id): Observable<any> {
    const url = `${this.serviceUrl}/bookmark-document/${id}`;
    return this.httpClient.get(url);
  }
  unBookmarkViewPost(id): Observable<any> {
    const url = `${this.serviceUrl}/remove-bookmark/${id}`;
    return this.httpClient.get(url);
  }
  getNotApp(reqDTO: any): Observable<any> {
    const url = `${environment.serverUrl.api}/document-posts-not-approve`;
    return this.getRequest(url, {
      params: reqDTO,
      observe: 'response'
    });
  }
  move(reqDTO: any): Observable<any> {
    const url = `${this.serviceUrl}/move-document`;
    return this.httpClient.get(url, {
      params: reqDTO
    });
  }
  findNotChild(id): Observable<any> {
    const url = `${this.serviceUrl}/find-not-in/${id}`;
    return this.getRequest(url);
  }
  save(reqDTO: any): Observable<any> {
    const url = `${this.serviceUrl}`;
    return this.httpClient.post(url, reqDTO);
  }
  update(reqDTO: any): Observable<any> {
    const url = `${this.serviceUrl}`;
    return this.httpClient.put(url, reqDTO);
  }
  delete(id: any): Observable<any> {
    const url = `${this.serviceUrl}/${id}`;
    return this.httpClient.delete(url);
  }
}


