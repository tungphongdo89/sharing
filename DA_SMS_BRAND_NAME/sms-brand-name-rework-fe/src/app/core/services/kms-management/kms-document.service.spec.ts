import { TestBed } from '@angular/core/testing';

import { KmsDocumentService } from './kms-document.service';

describe('KmsDocumentService', () => {
  let service: KmsDocumentService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(KmsDocumentService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
