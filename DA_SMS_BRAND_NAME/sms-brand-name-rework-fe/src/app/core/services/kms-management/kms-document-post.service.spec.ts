import { TestBed } from '@angular/core/testing';

import { KmsDocumentPostService } from './kms-document-post.service';

describe('KmsDocumentPostService', () => {
  let service: KmsDocumentPostService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(KmsDocumentPostService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
