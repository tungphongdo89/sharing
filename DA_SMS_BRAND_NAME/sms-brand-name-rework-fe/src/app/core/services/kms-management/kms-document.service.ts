import { Injectable } from '@angular/core';
import {BasicService} from '@core/services/basic.service';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {Observable} from 'rxjs/Observable';
import {HttpClient} from '@angular/common/http';
import {HelperService} from '@shared/services/helper.service';
import {CommonService} from '@shared/common/common.service';
import {environment} from '@env/environment';

@Injectable({
  providedIn: 'root'
})
export class KmsDocumentService extends BasicService {
  configData: BehaviorSubject<any> = new BehaviorSubject<any>(null);
  configData$: Observable<any> = this.configData.asObservable();

  constructor(public httpClient: HttpClient, public helperService: HelperService, private commonService: CommonService) {
    super(environment.serverUrl.api, '/documents', httpClient, helperService);
  }

  query(reqDTO: any, page: any): Observable<any> {
    const url = `${this.serviceUrl}`;
    return this.httpClient.get(url, reqDTO);
  }
  move(reqDTO: any): Observable<any> {
    const url = `${this.serviceUrl}/move-document`;
    return this.httpClient.get(url, {
      params: reqDTO
    });
  }
  findNotChild(id): Observable<any> {
    const url = `${this.serviceUrl}/find-not-in/${id}`;
    return this.getRequest(url);
  }
  save(reqDTO: any): Observable<any> {
    const url = `${this.serviceUrl}`;
    return this.httpClient.post(url, reqDTO);
  }
  update(reqDTO: any): Observable<any> {
    const url = `${this.serviceUrl}`;
    return this.httpClient.put(url, reqDTO);
  }
  delete(id: any): Observable<any> {
    const url = `${this.serviceUrl}/${id}`;
    return this.httpClient.delete(url);
  }
}

