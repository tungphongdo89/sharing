import { TestBed } from '@angular/core/testing';

import { ApDomainService } from './ap-domain.service';

describe('ApDomainService', () => {
  let service: ApDomainService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ApDomainService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
