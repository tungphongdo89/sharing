import { TestBed } from '@angular/core/testing';

import { ConfigMenusService } from './config-menus.service';

describe('ConfigMenusService', () => {
  let service: ConfigMenusService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ConfigMenusService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
