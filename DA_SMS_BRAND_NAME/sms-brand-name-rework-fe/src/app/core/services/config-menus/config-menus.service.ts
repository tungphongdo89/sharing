import { Injectable } from '@angular/core';
import {BasicService} from '@core/services/basic.service';
import {HttpClient} from '@angular/common/http';
import {HelperService} from '@shared/services/helper.service';
import {environment} from '@env/environment';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ConfigMenusService extends BasicService{

  constructor(
    public httpClient: HttpClient,
    public helperService: HelperService,
  ) {
    super(environment.serverUrl.api, '/config-menus', httpClient, helperService);
  }

  getAllMenu(): Observable<any> {
    const url = `${this.serviceUrl}`;
    return this.getRequest(url);
  }

  getFunctionByMenuAndRole(roleId: any, menuId: any): Observable<any> {
    const url = `${this.serviceUrl}/get-function-by-menu/${roleId}/${menuId}`;
    return this.getRequest(url);
  }

  getFunctionByMenu(menuId: any): Observable<any> {
    const url = `${this.serviceUrl}/menu/${menuId}`;
    return this.getRequest(url);
  }

  getFunctionInActiveByMenu(menuId: any): Observable<any> {
    const url = `${this.serviceUrl}/getFunctionInActive/${menuId}`;
    return this.getRequest(url);
  }
}
