export class DepartmentModel{
  departmentId: number;

  departmentName: string;

  status: string;
}
