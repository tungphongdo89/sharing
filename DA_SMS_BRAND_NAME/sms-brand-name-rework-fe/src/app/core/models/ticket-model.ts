export class TicketModel {
  ticketId: number;

  channelType: string;

  ticketCode: string;

  customerId: number;

  status: string;

  fcr: string;

  departmentId: number;

  createUser: number;

  createDatetime: Date;

  updateDatetime: Date;

  cid: string;

  taxCode: string;

  companyName: string;

  address: string;

  customerName: string;

  createUserName: string;
}
