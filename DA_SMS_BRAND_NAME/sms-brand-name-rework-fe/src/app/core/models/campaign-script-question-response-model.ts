export class CampaignScriptQuestionResponseModel {
  id: number;
  campaignScriptId: number;
  campaignScriptName: string;
  code: string;
  position: number;
  display: string;
  content: string;
  status: string;
  createDatetime: Date;
  updateDatetime: Date;
  createUser: number;
  updateUser: number;
}
