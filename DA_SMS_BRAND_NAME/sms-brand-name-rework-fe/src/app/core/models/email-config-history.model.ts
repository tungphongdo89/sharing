export interface EmailConfigHistoryModel{
  id:number;

  host:number;

  port:number;

  email:number;

  password:string;

  ssl:string;
}
