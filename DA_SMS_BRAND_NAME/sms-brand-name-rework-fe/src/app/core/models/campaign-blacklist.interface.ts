import {HttpParams} from "@angular/common/http";

export class CampaignBlacklistInterface {
  id: number;
  campaignId?: number;
  phoneNumber?: string;
  status: string;
  createDate: string;
  createUser: number;
  createUserName: string;
}
export const createRequestOption = (req?: any): HttpParams => {
  let options: HttpParams = new HttpParams();

  if (req) {
    Object.keys(req).forEach(key => {
      if (key !== 'sort' && req[key] !== null && req[key] !== undefined) {
        options = options.set(key, req[key]);
      }
    });

    if (req.sort) {
      req.sort.forEach((val: string) => {
        options = options.append('sort', val);
      });
    }
  }

  return options;
};
