export interface CampaignEmailBatchInterface {
  id?: number;
  campaignEmailMarketingId?: number;

  batchId?: number;

  email?: string;

  content?: string;

  title?: string;

  status?: string;

  sendDate?: string;

  response?: string;

  createDate?: string;

  createUser?: number;
  campaignEmailResourceId?: number;
}

// @ts-ignore
export class CampaignEmailBatchModel implements CampaignEmailBatchInterface {
  constructor(
    public id?: string,
    public campaignEmailMarketingId?: number,
    public batchId?: number,
    public email?: string,
    public content?: string,
    public title?: string,
    public status?: string,
    public sendDate?: string,
    public response?: string,
    public createDate?: string,
    public createUser?: number,
    public campaignEmailResourceId?: number,
  ) {
  }
}
