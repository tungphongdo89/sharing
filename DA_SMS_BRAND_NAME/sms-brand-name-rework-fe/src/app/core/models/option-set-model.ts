export class OptionSetModel{
 optionSetId: number;

 code: string;

 name: string;

 fromDate: Date;

 endDate: Date;

 status: string;

 createUser: number;

 createDate: Date;

 updateUser: number;

 updateDate: Date;

 page: number;
}
