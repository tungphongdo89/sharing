export class CriteriaGroupModel{
  id: number;
  name: string;
  scores: number;
  createDatetime: Date;
  createUser: number;
  updateDatetime: Date;
  updateUser: number;
}
