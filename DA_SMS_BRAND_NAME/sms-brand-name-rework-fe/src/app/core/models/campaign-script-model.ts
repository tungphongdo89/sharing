export class CampaignScriptListModel {
    id: number;
    campaignScriptName: string;
    status: string;
}
