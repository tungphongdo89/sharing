export class OptionSetValueModel {
  id: number;

  optionSetId: number;

  code: string;

  ord: number;

  name: string;

  groupName: string;

  fromDate: Date;

  endDate: Date;

  status: string;

  createUser: number;

  createDate: Date;

  updateUser: number;

  updateDate: Date;

  c1: string;

  c2: string;

  page: number;
}
