export interface EmailConfigModel {
  id: number;
  host: string;
  port: string;
  email: string;
  password: string;
  ssl: string;
}
