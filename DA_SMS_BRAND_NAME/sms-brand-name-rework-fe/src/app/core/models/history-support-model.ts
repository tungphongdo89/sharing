export class HistorySupportModel{
  eventCode: number;
  eventStatus: string;
  channel: string;
  userReceive: string;
  timeReceive: Date;
}
