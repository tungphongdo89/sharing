import {ProcessTicketAttachmentModel} from '@core/models/process-ticket-attachment-model';

export class ProcessTicketModel{
  processTicketId: number;

  ticketId: number;

  ticketRequestId: number;

  fileName: string;

  content: string;

  departmentId: number;

  createDatetime: Date;

  createUser: number;

  status: string;

  check: number;

  listProcessTicketAttachmentDTOS: ProcessTicketAttachmentModel[];
}
