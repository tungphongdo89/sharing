export class CriteriaDetailModel{
  id: number;
  criteriaId: number;
  criteriaGroupId: number;
  content: string;
  description: string;
  scores: number;
  status: string;
  createDatetime: Date;
  createUser: number;
  updateDatetime: Date;
  updateUser: number;
}
