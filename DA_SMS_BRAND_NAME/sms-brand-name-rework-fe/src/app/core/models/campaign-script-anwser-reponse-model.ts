export class CampaignScriptAnwserReponseModel{
  id: number;
  campaignScriptId: number;
  campaignScriptQuestionId: number;
  campaignScriptName: string;
  questionCode: string;
  code: string;
  type: string;
  position: number;
  display: string;
  content: string;
  min: number;
  max: number;
  status: string;
  createDatetime: Date;
  updateDatetime: Date;
  createUserId: number;
  updateUserId: number;
  createUsername: string;
  updateUsername: string;
  createFullName: string;
  updateFullName: string;
}
