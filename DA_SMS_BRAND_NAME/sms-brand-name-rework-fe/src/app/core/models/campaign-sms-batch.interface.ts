export interface CampaignSmsBatchInterface {
  id?: number;
  campaignSmsMarketingId?: number;

  batchId?: number;

  phoneNumber?: number;

  content?: string;

  status?: string;

  sendDate?: string;

  response?: string;

  createDateTime?: string;

  createUser?: number;
}

// @ts-ignore
export class CampaignSmsBatchModel implements CampaignSmsBatchInterface {
  constructor(
    public id?: number,
    public campaignSmsMarketingId?: number,
    public batchId?: number,
    public phoneNumber?: string,
    public content?: string,
    public status?: string,
    public sendDate?: string,
    public response?: string,
    public createDateTime?: string,
    public createUser?: number,
  ) {
  }
}
