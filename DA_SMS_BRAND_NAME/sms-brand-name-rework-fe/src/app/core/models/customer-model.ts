export class CustomerModel {
  customerId: number;

  phoneNumber: string;

  email: string;

  name: string;

  contactPhone: string;

  createDatetime: Date;

  updateDatetime: Date;
}
