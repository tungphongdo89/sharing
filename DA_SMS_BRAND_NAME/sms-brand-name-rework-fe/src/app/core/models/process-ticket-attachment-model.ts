export class ProcessTicketAttachmentModel {
  processTicketAttachmentId: number;

  ticketRequestId: number;

  fileName: string;

  fillNameEncrypt: string;

  createDatetime: Date;

  createUser: number;

  status: string;
}
