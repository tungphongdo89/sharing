import {ListTicketRequestAttachment} from '@core/models/list-ticket-request-attachment';

export class ListTicketRequest{
  ticketRequestCode: string;
  requestType: number;
  bussinessType: number;
  priority: number;
  departmentId: number;
  status: string;
  content: string;
  deadline: any;
  confirmDate: any;
  timeNotify: number;
  createDatetime: Date;
  createUser: number;
  listTicketRequestAttachmentDTOS: ListTicketRequestAttachment[];
}
