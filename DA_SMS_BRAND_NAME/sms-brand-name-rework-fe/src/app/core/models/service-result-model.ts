import {ListTicketRequestAttachment} from '@core/models/list-ticket-request-attachment';
import {ListTicketRequest} from '@core/models/list-ticket-request';

export class ServiceResultModel{
  status: string;
  message: string;
  listRequestAttachmentDTOList: ListTicketRequestAttachment[];
  ticketId: number;
  listTicketRequestIds: any[];
  processDate: Date;
}
