export class UserModel {
    id: number;
    login: string;
    password: string;
    firstName: string;
    lastName: string;
    email: string;
    imageUrl: string;
    createdBy: string;
    lastModifiedBy: string;
    createdDate: Date;
    lastModifiedDate: Date;
}