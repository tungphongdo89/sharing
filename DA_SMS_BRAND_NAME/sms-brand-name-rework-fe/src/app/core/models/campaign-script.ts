export class CampaignScriptModel{
  id: number;
  code: string;
  name: string;
  startContent: string;
  endContent: string;
  status: string;
  createDatetime: Date;
  createUserId: number;
  updateDatetime: Date;
  updateUserId: number;
  errorCodeConfig: string;
  message: string;
}
