export class ConfigScheduleModel {
  id: number;

  requestType: number;

  bussinessType: number;

  processTime: number;

  processTimeType: string;

  confirmTime: number;

  confirmTimeType: string;

  status: string;

  createUser: number;

   createDate: Date;

  updateUser: number;

  updateDate: number;

  page: number;

  size: number;

  keySearch: string;
}
