export class CampaignTemplateListModel{
  id: number;
  campaignTemplateName: string;
  status: string;
}
