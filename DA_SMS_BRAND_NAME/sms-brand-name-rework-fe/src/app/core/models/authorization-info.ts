export class AuthorizationInfo {
  id: number;
  login: string;
  fullName: string;
  createTicket: boolean;
  processTicket: boolean;
  confirmTicket: boolean;
}
