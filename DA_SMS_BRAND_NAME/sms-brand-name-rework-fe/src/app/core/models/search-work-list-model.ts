export class SearchWorkListModel{
  channelId: string;
  channelName: string;
  createDatetime: Date;
  createUser: number;
  createUserName: string;
  endDate: Date;
  evaluaterId: number;
  evaluaterName: string;
  id: number;
  lstUserDTO: number;
  names: string;
  startDate: Date;
  totalCall: number;
  totalUserId: number;
  updateDatetime: Date;
  updateUser: number;
  userId: number;
  totalCalled: number;
  totalCalledNot: number;
}
