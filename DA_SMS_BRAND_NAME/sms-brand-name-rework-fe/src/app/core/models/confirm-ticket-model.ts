import {ConfirmTicketAttachmentModel} from '@core/models/confirm-ticket-attachment-model';

export class ConfirmTicketModel {
  confirmTicketId: number;

  ticketId: number;

  content: string;

  departmentId: number;

  createUser: number;

  createDatetime: Date;

  status: string;

  satisfied: string;

  userName: string;

  confirmDeadline: string;

  departmentName:string;

  listConfirmTicketAttachmentDTOs: ConfirmTicketAttachmentModel[];
}
