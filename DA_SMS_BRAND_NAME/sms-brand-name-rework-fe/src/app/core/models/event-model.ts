import {ListTicketRequest} from '@core/models/list-ticket-request';

export class EventModel{
  channelType: string;
  ticketCode: string;
  phoneNumber: string;
  customerId: number;
  createUser: number;
  name: string;
  email: string;
  contactPhone: string;
  status: string;
  fcr: string;
  cid: string;
  taxCode: string;
  companyName: string;
  address: string;
  listTicketRequestAttachmentDTOS: any[];
  listTicketRequestDTOS: ListTicketRequest[];
  ticketId: number;
}

