export interface CampaignEmailResourceModel {
  id: number;

  campaignEmailMarketingId: number;

  email: string;

  c1?: string;

  c2?: string;

  c3?: string;

  c4?: string;

  c5?: string;

  c6?: string;

  c7?: string;

  c8?: string;

  c9?: string;

  c10?: string;

  createDateTime?: string;

  createUser?: number;

  sendUserId?: number;

  sendDate?: string;

  sendStatus?: string;

  sendUserName?: string;
}
