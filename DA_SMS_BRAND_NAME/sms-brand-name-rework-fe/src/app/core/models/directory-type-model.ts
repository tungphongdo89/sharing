export class DirectoryTypeModel {
  code: string;
  name: string;
  effectDate: Date;
  expiredDate: Date;
  status: string;
  createDate: Date;
  createUser: string;
  updateDate: Date;
  updateBy: string;
}
