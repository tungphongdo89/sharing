export class CriteriaRatingModel{
  id: number;
  name: string;
  fromScores: number;
  toScores: number;
  createDatetime: Date;
  createUser: number;
  updateDatetime: Date;
  updateUser: number;
}
