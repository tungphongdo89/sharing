export interface SendEmailOneModel {
  title: string;

  toEmail: string;

  ccEmail: string;

  bccEmail: string;

  content: string;
}
