export class FeeStatus{
  feeType: string;
  feeStatus: string;
  amount: number;
  date: Date;
}
