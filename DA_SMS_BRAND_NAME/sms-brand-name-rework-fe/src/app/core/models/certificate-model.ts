export class CertificateModel{
  userName: string;
  user: string;
  ctsStatus: string;
  expiredDate: Date;
}
