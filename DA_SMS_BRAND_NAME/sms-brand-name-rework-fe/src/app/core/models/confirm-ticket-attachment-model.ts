export class ConfirmTicketAttachmentModel {
  confirmTicketAttachmentId: number;

  ticketRequestId: number;

  ticketId: number;

  fileName: string;

  fillNameEncrypt: string;

  createDatetime: Date;

  createUser: number;

  status: string;
}
