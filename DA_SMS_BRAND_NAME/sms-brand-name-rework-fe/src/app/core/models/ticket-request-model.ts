export class TicketRequestModel {
  ticketRequestId: number;

  ticketId: number;

  ticketRequestCode: string;

  requestType: number;

  bussinessType: number;

  priority: number;

  departmentId: number;

  status: string;

  deadline: string;

  content: string;

  confirmDate: string;

  timeNotify: number;

  createUser: number;

  updateUser: number;

  createDatetime: string;

  createUserName: string;

  updateDatetime: Date;

  disableCloseSave?: boolean;
}
