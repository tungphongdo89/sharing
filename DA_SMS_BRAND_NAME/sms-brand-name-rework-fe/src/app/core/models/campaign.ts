export class CampaignModel {

  id: number;
  name: string;
  campaignTemplateId: number;
  campaignScriptId: number;
  groupUser: string;
  displayPhone: string;
  startDate: Date;
  endDate: Date;
  status: string;
  createDatetime: Date;
  createUser: number;
  updateDatetime: Date;
  updateUser: number;
  lstGroupUser: Array<string>;
  campaignTemplateName: string;
  campaignScriptName: string;



}
