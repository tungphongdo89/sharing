import {Directive, Input, OnDestroy, OnInit, TemplateRef, ViewContainerRef} from '@angular/core';
import {Menu, MenuService} from '@core';

@Directive({
  selector: '[actionCode]'
})
export class ActionPermissionDirective implements OnInit, OnDestroy {

  @Input() actionCode: string;

  constructor(
    private templateRef: TemplateRef<any>,
    private viewContainer: ViewContainerRef,
    private menuService: MenuService
  ) {
  }

  ngOnDestroy(): void {
  }

  ngOnInit(): void {
    if (!this.menuService.codeList || this.menuService.codeList.length < 1) {
      this.getMenuByLogin();
    } else {
      if (this.actionCode && this.menuService.codeList.includes(this.actionCode)) {
        this.viewContainer.createEmbeddedView(this.templateRef);
      } else {
        this.viewContainer.clear();
      }
    }
  }

  getMenuByLogin(login?: any) {
    this.menuService.getMenuByLogin(login).subscribe((value: Menu[]) => {
      if (value) {
        this.menuService.setCodeList(value);
        if (this.actionCode && this.menuService.codeList.includes(this.actionCode)) {
          this.viewContainer.createEmbeddedView(this.templateRef);
        } else {
          this.viewContainer.clear();
        }
      }
    });
  }

}
