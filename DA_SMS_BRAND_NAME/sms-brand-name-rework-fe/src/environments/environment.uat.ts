// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const COMMOM_CONFIG = {
  DATE_FORMAT: 'DD/MM/YYYY',
  DATE_TIME_FORMAT: 'DD/MM/YYYY HH:mm:ss',
  EMAIL_FORMAT: '[A-Za-z0-9._%-]+@[A-Za-z0-9._%-]+\\.[a-z]{2,3}',
  NUMBER_PHONE_FORMAT: '^((\\84)|0)([0-9]{9,14})',
};

export const environment = {
  production: false,
  useHash: true,
  serverUrl: {
    api: 'http://27.118.22.7:4205/api',
  },
};


