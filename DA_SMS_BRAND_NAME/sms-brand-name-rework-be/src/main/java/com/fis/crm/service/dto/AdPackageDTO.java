package com.fis.crm.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link com.fis.crm.domain.AdPackage} entity.
 */
public class AdPackageDTO implements Serializable {

  private Long id;

  private String packageName;

  private Integer type;

  private Integer status;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getPackageName() {
    return packageName;
  }

  public void setPackageName(String packageName) {
    this.packageName = packageName;
  }

  public Integer getType() {
    return type;
  }

  public void setType(Integer type) {
    this.type = type;
  }

  public Integer getStatus() {
    return status;
  }

  public void setStatus(Integer status) {
    this.status = status;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof AdPackageDTO)) {
      return false;
    }

    AdPackageDTO adPackageDTO = (AdPackageDTO) o;
    if (this.id == null) {
      return false;
    }
    return Objects.equals(this.id, adPackageDTO.id);
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.id);
  }

  // prettier-ignore
    @Override
    public String toString() {
        return "AdPackageDTO{" +
            "id=" + getId() +
            ", packageName='" + getPackageName() + "'" +
            ", type=" + getType() +
            ", status=" + getStatus() +
            "}";
    }
}
