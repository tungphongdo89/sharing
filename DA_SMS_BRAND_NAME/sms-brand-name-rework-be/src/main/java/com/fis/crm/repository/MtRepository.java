package com.fis.crm.repository;

import com.fis.crm.domain.Mt;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the Mt entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MtRepository extends JpaRepository<Mt, Long> {}
