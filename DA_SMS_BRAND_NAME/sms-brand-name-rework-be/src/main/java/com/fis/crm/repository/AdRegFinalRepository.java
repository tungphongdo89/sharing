package com.fis.crm.repository;

import com.fis.crm.domain.AdRegFinal;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the AdRegFinal entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AdRegFinalRepository extends JpaRepository<AdRegFinal, Long> {}
