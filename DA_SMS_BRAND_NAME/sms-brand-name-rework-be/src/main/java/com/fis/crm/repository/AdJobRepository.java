package com.fis.crm.repository;

import com.fis.crm.domain.AdJob;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the AdJob entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AdJobRepository extends JpaRepository<AdJob, Long> {}
