package com.fis.crm.repository;

import com.fis.crm.domain.CpGroupSub;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the CpGroupSub entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CpGroupSubRepository extends JpaRepository<CpGroupSub, Long> {}
