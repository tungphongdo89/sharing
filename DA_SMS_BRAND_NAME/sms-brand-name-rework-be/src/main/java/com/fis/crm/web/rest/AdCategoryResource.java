package com.fis.crm.web.rest;

import com.fis.crm.repository.AdCategoryRepository;
import com.fis.crm.service.dto.AdCategoryDTO;
import com.fis.crm.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.fis.crm.domain.AdCategory}.
 */
@RestController
@RequestMapping("/api")
public class AdCategoryResource {

  private final Logger log = LoggerFactory.getLogger(AdCategoryResource.class);

  private static final String ENTITY_NAME = "adCategory";

  @Value("${jhipster.clientApp.name}")
  private String applicationName;

  private final AdCategoryService adCategoryService;

  private final AdCategoryRepository adCategoryRepository;

  public AdCategoryResource(AdCategoryService adCategoryService, AdCategoryRepository adCategoryRepository) {
    this.adCategoryService = adCategoryService;
    this.adCategoryRepository = adCategoryRepository;
  }

  /**
   * {@code POST  /ad-categories} : Create a new adCategory.
   *
   * @param adCategoryDTO the adCategoryDTO to create.
   * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new adCategoryDTO, or with status {@code 400 (Bad Request)} if the adCategory has already an ID.
   * @throws URISyntaxException if the Location URI syntax is incorrect.
   */
  @PostMapping("/ad-categories")
  public ResponseEntity<AdCategoryDTO> createAdCategory(@RequestBody AdCategoryDTO adCategoryDTO) throws URISyntaxException {
    log.debug("REST request to save AdCategory : {}", adCategoryDTO);
    if (adCategoryDTO.getId() != null) {
      throw new BadRequestAlertException("A new adCategory cannot already have an ID", ENTITY_NAME, "idexists");
    }
    AdCategoryDTO result = adCategoryService.save(adCategoryDTO);
    return ResponseEntity
      .created(new URI("/api/ad-categories/" + result.getId()))
      .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
      .body(result);
  }

  /**
   * {@code PUT  /ad-categories/:id} : Updates an existing adCategory.
   *
   * @param id the id of the adCategoryDTO to save.
   * @param adCategoryDTO the adCategoryDTO to update.
   * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated adCategoryDTO,
   * or with status {@code 400 (Bad Request)} if the adCategoryDTO is not valid,
   * or with status {@code 500 (Internal Server Error)} if the adCategoryDTO couldn't be updated.
   * @throws URISyntaxException if the Location URI syntax is incorrect.
   */
  @PutMapping("/ad-categories/{id}")
  public ResponseEntity<AdCategoryDTO> updateAdCategory(
    @PathVariable(value = "id", required = false) final Long id,
    @RequestBody AdCategoryDTO adCategoryDTO
  ) throws URISyntaxException {
    log.debug("REST request to update AdCategory : {}, {}", id, adCategoryDTO);
    if (adCategoryDTO.getId() == null) {
      throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
    }
    if (!Objects.equals(id, adCategoryDTO.getId())) {
      throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
    }

    if (!adCategoryRepository.existsById(id)) {
      throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
    }

    AdCategoryDTO result = adCategoryService.save(adCategoryDTO);
    return ResponseEntity
      .ok()
      .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, adCategoryDTO.getId().toString()))
      .body(result);
  }

  /**
   * {@code PATCH  /ad-categories/:id} : Partial updates given fields of an existing adCategory, field will ignore if it is null
   *
   * @param id the id of the adCategoryDTO to save.
   * @param adCategoryDTO the adCategoryDTO to update.
   * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated adCategoryDTO,
   * or with status {@code 400 (Bad Request)} if the adCategoryDTO is not valid,
   * or with status {@code 404 (Not Found)} if the adCategoryDTO is not found,
   * or with status {@code 500 (Internal Server Error)} if the adCategoryDTO couldn't be updated.
   * @throws URISyntaxException if the Location URI syntax is incorrect.
   */
  @PatchMapping(value = "/ad-categories/{id}", consumes = "application/merge-patch+json")
  public ResponseEntity<AdCategoryDTO> partialUpdateAdCategory(
    @PathVariable(value = "id", required = false) final Long id,
    @RequestBody AdCategoryDTO adCategoryDTO
  ) throws URISyntaxException {
    log.debug("REST request to partial update AdCategory partially : {}, {}", id, adCategoryDTO);
    if (adCategoryDTO.getId() == null) {
      throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
    }
    if (!Objects.equals(id, adCategoryDTO.getId())) {
      throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
    }

    if (!adCategoryRepository.existsById(id)) {
      throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
    }

    Optional<AdCategoryDTO> result = adCategoryService.partialUpdate(adCategoryDTO);

    return ResponseUtil.wrapOrNotFound(
      result,
      HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, adCategoryDTO.getId().toString())
    );
  }

  /**
   * {@code GET  /ad-categories} : get all the adCategories.
   *
   * @param pageable the pagination information.
   * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of adCategories in body.
   */
  @GetMapping("/ad-categories")
  public ResponseEntity<List<AdCategoryDTO>> getAllAdCategories(Pageable pageable) {
    log.debug("REST request to get a page of AdCategories");
    Page<AdCategoryDTO> page = adCategoryService.findAll(pageable);
    HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
    return ResponseEntity.ok().headers(headers).body(page.getContent());
  }

  /**
   * {@code GET  /ad-categories/:id} : get the "id" adCategory.
   *
   * @param id the id of the adCategoryDTO to retrieve.
   * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the adCategoryDTO, or with status {@code 404 (Not Found)}.
   */
  @GetMapping("/ad-categories/{id}")
  public ResponseEntity<AdCategoryDTO> getAdCategory(@PathVariable Long id) {
    log.debug("REST request to get AdCategory : {}", id);
    Optional<AdCategoryDTO> adCategoryDTO = adCategoryService.findOne(id);
    return ResponseUtil.wrapOrNotFound(adCategoryDTO);
  }

  /**
   * {@code DELETE  /ad-categories/:id} : delete the "id" adCategory.
   *
   * @param id the id of the adCategoryDTO to delete.
   * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
   */
  @DeleteMapping("/ad-categories/{id}")
  public ResponseEntity<Void> deleteAdCategory(@PathVariable Long id) {
    log.debug("REST request to delete AdCategory : {}", id);
    adCategoryService.delete(id);
    return ResponseEntity
      .noContent()
      .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
      .build();
  }
}
