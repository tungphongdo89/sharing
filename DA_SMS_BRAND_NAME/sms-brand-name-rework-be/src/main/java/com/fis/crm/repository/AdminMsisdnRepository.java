package com.fis.crm.repository;

import com.fis.crm.domain.AdminMsisdn;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the AdminMsisdn entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AdminMsisdnRepository extends JpaRepository<AdminMsisdn, Long> {}
