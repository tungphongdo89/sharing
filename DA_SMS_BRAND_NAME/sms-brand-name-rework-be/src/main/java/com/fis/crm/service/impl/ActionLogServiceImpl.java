package com.fis.crm.service.impl;

import com.fis.crm.domain.ActionLog;
import com.fis.crm.domain.ConfigSchedule;
import com.fis.crm.repository.ActionLogRepository;
import com.fis.crm.repository.ConfigScheduleRepository;
import com.fis.crm.service.ActionLogService;
import com.fis.crm.service.ConfigScheduleException;
import com.fis.crm.service.ConfigScheduleService;
import com.fis.crm.service.dto.ActionLogDTO;
import com.fis.crm.service.dto.ConfigScheduleDTO;
import com.fis.crm.service.mapper.ActionLogMapper;
import com.fis.crm.service.mapper.ConfigScheduleMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link ConfigSchedule}.
 */
@Service
@Transactional
public class ActionLogServiceImpl implements ActionLogService {

    private final Logger log = LoggerFactory.getLogger(ActionLogServiceImpl.class);

    private final ActionLogRepository actionLogRepository;
    private final ActionLogMapper actionLogMapper;

    public ActionLogServiceImpl(ActionLogRepository actionLogRepository, ActionLogMapper actionLogMapper) {
        this.actionLogRepository = actionLogRepository;
        this.actionLogMapper = actionLogMapper;
    }

    @Override
    public ActionLogDTO save(ActionLogDTO actionLogDTO) {
        log.debug("Request to save ConfigSchedule : {}", actionLogDTO);
        ActionLog actionLog = actionLogMapper.toEntity(actionLogDTO);
        actionLog = actionLogRepository.save(actionLog);
        return actionLogMapper.toDto(actionLog);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<ActionLogDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ConfigSchedules");
        return actionLogRepository.findAll(pageable)
            .map(actionLogMapper::toDto);
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<ActionLogDTO> findOne(Long id) {
        log.debug("Request to get ConfigSchedule : {}", id);
        return actionLogRepository.findById(id)
            .map(actionLogMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete ConfigSchedule : {}", id);
        actionLogRepository.deleteById(id);
    }
    public void saveActionLog(Long userId,String actionType,Long objectId,String objectName,String note,Instant issueDateTime){
        ActionLogDTO actionLogDTO = new ActionLogDTO();
        actionLogDTO.setUserId(userId);
        actionLogDTO.setActionType(actionType);
        actionLogDTO.setObjectId(objectId);
        actionLogDTO.setObjectName(objectName);
        actionLogDTO.setNote(note);
        actionLogDTO.setIssueDateTime(issueDateTime);
        save(actionLogDTO);
    }
}
