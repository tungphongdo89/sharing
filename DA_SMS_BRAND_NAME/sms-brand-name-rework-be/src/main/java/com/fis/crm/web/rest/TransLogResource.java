package com.fis.crm.web.rest;

import com.fis.crm.repository.TransLogRepository;
import com.fis.crm.service.dto.TransLogDTO;
import com.fis.crm.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.fis.crm.domain.TransLog}.
 */
@RestController
@RequestMapping("/api")
public class TransLogResource {

  private final Logger log = LoggerFactory.getLogger(TransLogResource.class);

  private static final String ENTITY_NAME = "transLog";

  @Value("${jhipster.clientApp.name}")
  private String applicationName;

  private final TransLogService transLogService;

  private final TransLogRepository transLogRepository;

  public TransLogResource(TransLogService transLogService, TransLogRepository transLogRepository) {
    this.transLogService = transLogService;
    this.transLogRepository = transLogRepository;
  }

  /**
   * {@code POST  /trans-logs} : Create a new transLog.
   *
   * @param transLogDTO the transLogDTO to create.
   * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new transLogDTO, or with status {@code 400 (Bad Request)} if the transLog has already an ID.
   * @throws URISyntaxException if the Location URI syntax is incorrect.
   */
  @PostMapping("/trans-logs")
  public ResponseEntity<TransLogDTO> createTransLog(@RequestBody TransLogDTO transLogDTO) throws URISyntaxException {
    log.debug("REST request to save TransLog : {}", transLogDTO);
    if (transLogDTO.getId() != null) {
      throw new BadRequestAlertException("A new transLog cannot already have an ID", ENTITY_NAME, "idexists");
    }
    TransLogDTO result = transLogService.save(transLogDTO);
    return ResponseEntity
      .created(new URI("/api/trans-logs/" + result.getId()))
      .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
      .body(result);
  }

  /**
   * {@code PUT  /trans-logs/:id} : Updates an existing transLog.
   *
   * @param id the id of the transLogDTO to save.
   * @param transLogDTO the transLogDTO to update.
   * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated transLogDTO,
   * or with status {@code 400 (Bad Request)} if the transLogDTO is not valid,
   * or with status {@code 500 (Internal Server Error)} if the transLogDTO couldn't be updated.
   * @throws URISyntaxException if the Location URI syntax is incorrect.
   */
  @PutMapping("/trans-logs/{id}")
  public ResponseEntity<TransLogDTO> updateTransLog(
    @PathVariable(value = "id", required = false) final Long id,
    @RequestBody TransLogDTO transLogDTO
  ) throws URISyntaxException {
    log.debug("REST request to update TransLog : {}, {}", id, transLogDTO);
    if (transLogDTO.getId() == null) {
      throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
    }
    if (!Objects.equals(id, transLogDTO.getId())) {
      throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
    }

    if (!transLogRepository.existsById(id)) {
      throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
    }

    TransLogDTO result = transLogService.save(transLogDTO);
    return ResponseEntity
      .ok()
      .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, transLogDTO.getId().toString()))
      .body(result);
  }

  /**
   * {@code PATCH  /trans-logs/:id} : Partial updates given fields of an existing transLog, field will ignore if it is null
   *
   * @param id the id of the transLogDTO to save.
   * @param transLogDTO the transLogDTO to update.
   * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated transLogDTO,
   * or with status {@code 400 (Bad Request)} if the transLogDTO is not valid,
   * or with status {@code 404 (Not Found)} if the transLogDTO is not found,
   * or with status {@code 500 (Internal Server Error)} if the transLogDTO couldn't be updated.
   * @throws URISyntaxException if the Location URI syntax is incorrect.
   */
  @PatchMapping(value = "/trans-logs/{id}", consumes = "application/merge-patch+json")
  public ResponseEntity<TransLogDTO> partialUpdateTransLog(
    @PathVariable(value = "id", required = false) final Long id,
    @RequestBody TransLogDTO transLogDTO
  ) throws URISyntaxException {
    log.debug("REST request to partial update TransLog partially : {}, {}", id, transLogDTO);
    if (transLogDTO.getId() == null) {
      throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
    }
    if (!Objects.equals(id, transLogDTO.getId())) {
      throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
    }

    if (!transLogRepository.existsById(id)) {
      throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
    }

    Optional<TransLogDTO> result = transLogService.partialUpdate(transLogDTO);

    return ResponseUtil.wrapOrNotFound(
      result,
      HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, transLogDTO.getId().toString())
    );
  }

  /**
   * {@code GET  /trans-logs} : get all the transLogs.
   *
   * @param pageable the pagination information.
   * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of transLogs in body.
   */
  @GetMapping("/trans-logs")
  public ResponseEntity<List<TransLogDTO>> getAllTransLogs(Pageable pageable) {
    log.debug("REST request to get a page of TransLogs");
    Page<TransLogDTO> page = transLogService.findAll(pageable);
    HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
    return ResponseEntity.ok().headers(headers).body(page.getContent());
  }

  /**
   * {@code GET  /trans-logs/:id} : get the "id" transLog.
   *
   * @param id the id of the transLogDTO to retrieve.
   * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the transLogDTO, or with status {@code 404 (Not Found)}.
   */
  @GetMapping("/trans-logs/{id}")
  public ResponseEntity<TransLogDTO> getTransLog(@PathVariable Long id) {
    log.debug("REST request to get TransLog : {}", id);
    Optional<TransLogDTO> transLogDTO = transLogService.findOne(id);
    return ResponseUtil.wrapOrNotFound(transLogDTO);
  }

  /**
   * {@code DELETE  /trans-logs/:id} : delete the "id" transLog.
   *
   * @param id the id of the transLogDTO to delete.
   * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
   */
  @DeleteMapping("/trans-logs/{id}")
  public ResponseEntity<Void> deleteTransLog(@PathVariable Long id) {
    log.debug("REST request to delete TransLog : {}", id);
    transLogService.delete(id);
    return ResponseEntity
      .noContent()
      .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
      .build();
  }
}
