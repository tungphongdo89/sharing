package com.fis.crm.web.rest;

import com.fis.crm.repository.ProvinceBccsRepository;
import com.fis.crm.service.dto.ProvinceBccsDTO;
import com.fis.crm.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.fis.crm.domain.ProvinceBccs}.
 */
@RestController
@RequestMapping("/api")
public class ProvinceBccsResource {

  private final Logger log = LoggerFactory.getLogger(ProvinceBccsResource.class);

  private static final String ENTITY_NAME = "provinceBccs";

  @Value("${jhipster.clientApp.name}")
  private String applicationName;

  private final ProvinceBccsService provinceBccsService;

  private final ProvinceBccsRepository provinceBccsRepository;

  public ProvinceBccsResource(ProvinceBccsService provinceBccsService, ProvinceBccsRepository provinceBccsRepository) {
    this.provinceBccsService = provinceBccsService;
    this.provinceBccsRepository = provinceBccsRepository;
  }

  /**
   * {@code POST  /province-bccs} : Create a new provinceBccs.
   *
   * @param provinceBccsDTO the provinceBccsDTO to create.
   * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new provinceBccsDTO, or with status {@code 400 (Bad Request)} if the provinceBccs has already an ID.
   * @throws URISyntaxException if the Location URI syntax is incorrect.
   */
  @PostMapping("/province-bccs")
  public ResponseEntity<ProvinceBccsDTO> createProvinceBccs(@RequestBody ProvinceBccsDTO provinceBccsDTO) throws URISyntaxException {
    log.debug("REST request to save ProvinceBccs : {}", provinceBccsDTO);
    if (provinceBccsDTO.getId() != null) {
      throw new BadRequestAlertException("A new provinceBccs cannot already have an ID", ENTITY_NAME, "idexists");
    }
    ProvinceBccsDTO result = provinceBccsService.save(provinceBccsDTO);
    return ResponseEntity
      .created(new URI("/api/province-bccs/" + result.getId()))
      .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
      .body(result);
  }

  /**
   * {@code PUT  /province-bccs/:id} : Updates an existing provinceBccs.
   *
   * @param id the id of the provinceBccsDTO to save.
   * @param provinceBccsDTO the provinceBccsDTO to update.
   * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated provinceBccsDTO,
   * or with status {@code 400 (Bad Request)} if the provinceBccsDTO is not valid,
   * or with status {@code 500 (Internal Server Error)} if the provinceBccsDTO couldn't be updated.
   * @throws URISyntaxException if the Location URI syntax is incorrect.
   */
  @PutMapping("/province-bccs/{id}")
  public ResponseEntity<ProvinceBccsDTO> updateProvinceBccs(
    @PathVariable(value = "id", required = false) final Long id,
    @RequestBody ProvinceBccsDTO provinceBccsDTO
  ) throws URISyntaxException {
    log.debug("REST request to update ProvinceBccs : {}, {}", id, provinceBccsDTO);
    if (provinceBccsDTO.getId() == null) {
      throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
    }
    if (!Objects.equals(id, provinceBccsDTO.getId())) {
      throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
    }

    if (!provinceBccsRepository.existsById(id)) {
      throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
    }

    ProvinceBccsDTO result = provinceBccsService.save(provinceBccsDTO);
    return ResponseEntity
      .ok()
      .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, provinceBccsDTO.getId().toString()))
      .body(result);
  }

  /**
   * {@code PATCH  /province-bccs/:id} : Partial updates given fields of an existing provinceBccs, field will ignore if it is null
   *
   * @param id the id of the provinceBccsDTO to save.
   * @param provinceBccsDTO the provinceBccsDTO to update.
   * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated provinceBccsDTO,
   * or with status {@code 400 (Bad Request)} if the provinceBccsDTO is not valid,
   * or with status {@code 404 (Not Found)} if the provinceBccsDTO is not found,
   * or with status {@code 500 (Internal Server Error)} if the provinceBccsDTO couldn't be updated.
   * @throws URISyntaxException if the Location URI syntax is incorrect.
   */
  @PatchMapping(value = "/province-bccs/{id}", consumes = "application/merge-patch+json")
  public ResponseEntity<ProvinceBccsDTO> partialUpdateProvinceBccs(
    @PathVariable(value = "id", required = false) final Long id,
    @RequestBody ProvinceBccsDTO provinceBccsDTO
  ) throws URISyntaxException {
    log.debug("REST request to partial update ProvinceBccs partially : {}, {}", id, provinceBccsDTO);
    if (provinceBccsDTO.getId() == null) {
      throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
    }
    if (!Objects.equals(id, provinceBccsDTO.getId())) {
      throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
    }

    if (!provinceBccsRepository.existsById(id)) {
      throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
    }

    Optional<ProvinceBccsDTO> result = provinceBccsService.partialUpdate(provinceBccsDTO);

    return ResponseUtil.wrapOrNotFound(
      result,
      HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, provinceBccsDTO.getId().toString())
    );
  }

  /**
   * {@code GET  /province-bccs} : get all the provinceBccs.
   *
   * @param pageable the pagination information.
   * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of provinceBccs in body.
   */
  @GetMapping("/province-bccs")
  public ResponseEntity<List<ProvinceBccsDTO>> getAllProvinceBccs(Pageable pageable) {
    log.debug("REST request to get a page of ProvinceBccs");
    Page<ProvinceBccsDTO> page = provinceBccsService.findAll(pageable);
    HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
    return ResponseEntity.ok().headers(headers).body(page.getContent());
  }

  /**
   * {@code GET  /province-bccs/:id} : get the "id" provinceBccs.
   *
   * @param id the id of the provinceBccsDTO to retrieve.
   * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the provinceBccsDTO, or with status {@code 404 (Not Found)}.
   */
  @GetMapping("/province-bccs/{id}")
  public ResponseEntity<ProvinceBccsDTO> getProvinceBccs(@PathVariable Long id) {
    log.debug("REST request to get ProvinceBccs : {}", id);
    Optional<ProvinceBccsDTO> provinceBccsDTO = provinceBccsService.findOne(id);
    return ResponseUtil.wrapOrNotFound(provinceBccsDTO);
  }

  /**
   * {@code DELETE  /province-bccs/:id} : delete the "id" provinceBccs.
   *
   * @param id the id of the provinceBccsDTO to delete.
   * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
   */
  @DeleteMapping("/province-bccs/{id}")
  public ResponseEntity<Void> deleteProvinceBccs(@PathVariable Long id) {
    log.debug("REST request to delete ProvinceBccs : {}", id);
    provinceBccsService.delete(id);
    return ResponseEntity
      .noContent()
      .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
      .build();
  }
}
