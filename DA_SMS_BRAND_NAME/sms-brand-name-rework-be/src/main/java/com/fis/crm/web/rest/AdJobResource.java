package com.fis.crm.web.rest;

import com.fis.crm.repository.AdJobRepository;
import com.fis.crm.service.dto.AdJobDTO;
import com.fis.crm.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.fis.crm.domain.AdJob}.
 */
@RestController
@RequestMapping("/api")
public class AdJobResource {

  private final Logger log = LoggerFactory.getLogger(AdJobResource.class);

  private static final String ENTITY_NAME = "adJob";

  @Value("${jhipster.clientApp.name}")
  private String applicationName;

  private final AdJobService adJobService;

  private final AdJobRepository adJobRepository;

  public AdJobResource(AdJobService adJobService, AdJobRepository adJobRepository) {
    this.adJobService = adJobService;
    this.adJobRepository = adJobRepository;
  }

  /**
   * {@code POST  /ad-jobs} : Create a new adJob.
   *
   * @param adJobDTO the adJobDTO to create.
   * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new adJobDTO, or with status {@code 400 (Bad Request)} if the adJob has already an ID.
   * @throws URISyntaxException if the Location URI syntax is incorrect.
   */
  @PostMapping("/ad-jobs")
  public ResponseEntity<AdJobDTO> createAdJob(@RequestBody AdJobDTO adJobDTO) throws URISyntaxException {
    log.debug("REST request to save AdJob : {}", adJobDTO);
    if (adJobDTO.getId() != null) {
      throw new BadRequestAlertException("A new adJob cannot already have an ID", ENTITY_NAME, "idexists");
    }
    AdJobDTO result = adJobService.save(adJobDTO);
    return ResponseEntity
      .created(new URI("/api/ad-jobs/" + result.getId()))
      .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
      .body(result);
  }

  /**
   * {@code PUT  /ad-jobs/:id} : Updates an existing adJob.
   *
   * @param id the id of the adJobDTO to save.
   * @param adJobDTO the adJobDTO to update.
   * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated adJobDTO,
   * or with status {@code 400 (Bad Request)} if the adJobDTO is not valid,
   * or with status {@code 500 (Internal Server Error)} if the adJobDTO couldn't be updated.
   * @throws URISyntaxException if the Location URI syntax is incorrect.
   */
  @PutMapping("/ad-jobs/{id}")
  public ResponseEntity<AdJobDTO> updateAdJob(@PathVariable(value = "id", required = false) final Long id, @RequestBody AdJobDTO adJobDTO)
    throws URISyntaxException {
    log.debug("REST request to update AdJob : {}, {}", id, adJobDTO);
    if (adJobDTO.getId() == null) {
      throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
    }
    if (!Objects.equals(id, adJobDTO.getId())) {
      throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
    }

    if (!adJobRepository.existsById(id)) {
      throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
    }

    AdJobDTO result = adJobService.save(adJobDTO);
    return ResponseEntity
      .ok()
      .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, adJobDTO.getId().toString()))
      .body(result);
  }

  /**
   * {@code PATCH  /ad-jobs/:id} : Partial updates given fields of an existing adJob, field will ignore if it is null
   *
   * @param id the id of the adJobDTO to save.
   * @param adJobDTO the adJobDTO to update.
   * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated adJobDTO,
   * or with status {@code 400 (Bad Request)} if the adJobDTO is not valid,
   * or with status {@code 404 (Not Found)} if the adJobDTO is not found,
   * or with status {@code 500 (Internal Server Error)} if the adJobDTO couldn't be updated.
   * @throws URISyntaxException if the Location URI syntax is incorrect.
   */
  @PatchMapping(value = "/ad-jobs/{id}", consumes = "application/merge-patch+json")
  public ResponseEntity<AdJobDTO> partialUpdateAdJob(
    @PathVariable(value = "id", required = false) final Long id,
    @RequestBody AdJobDTO adJobDTO
  ) throws URISyntaxException {
    log.debug("REST request to partial update AdJob partially : {}, {}", id, adJobDTO);
    if (adJobDTO.getId() == null) {
      throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
    }
    if (!Objects.equals(id, adJobDTO.getId())) {
      throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
    }

    if (!adJobRepository.existsById(id)) {
      throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
    }

    Optional<AdJobDTO> result = adJobService.partialUpdate(adJobDTO);

    return ResponseUtil.wrapOrNotFound(
      result,
      HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, adJobDTO.getId().toString())
    );
  }

  /**
   * {@code GET  /ad-jobs} : get all the adJobs.
   *
   * @param pageable the pagination information.
   * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of adJobs in body.
   */
  @GetMapping("/ad-jobs")
  public ResponseEntity<List<AdJobDTO>> getAllAdJobs(Pageable pageable) {
    log.debug("REST request to get a page of AdJobs");
    Page<AdJobDTO> page = adJobService.findAll(pageable);
    HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
    return ResponseEntity.ok().headers(headers).body(page.getContent());
  }

  /**
   * {@code GET  /ad-jobs/:id} : get the "id" adJob.
   *
   * @param id the id of the adJobDTO to retrieve.
   * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the adJobDTO, or with status {@code 404 (Not Found)}.
   */
  @GetMapping("/ad-jobs/{id}")
  public ResponseEntity<AdJobDTO> getAdJob(@PathVariable Long id) {
    log.debug("REST request to get AdJob : {}", id);
    Optional<AdJobDTO> adJobDTO = adJobService.findOne(id);
    return ResponseUtil.wrapOrNotFound(adJobDTO);
  }

  /**
   * {@code DELETE  /ad-jobs/:id} : delete the "id" adJob.
   *
   * @param id the id of the adJobDTO to delete.
   * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
   */
  @DeleteMapping("/ad-jobs/{id}")
  public ResponseEntity<Void> deleteAdJob(@PathVariable Long id) {
    log.debug("REST request to delete AdJob : {}", id);
    adJobService.delete(id);
    return ResponseEntity
      .noContent()
      .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
      .build();
  }
}
