package com.fis.crm.repository;

import com.fis.crm.domain.AdCategory;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the AdCategory entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AdCategoryRepository extends JpaRepository<AdCategory, Long> {}
