package com.fis.crm.service.dto;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

/**
 * A DTO for the {@link com.fis.crm.domain.AdRegFinal} entity.
 */
public class AdRegFinalDTO implements Serializable {

  private Long id;

  private String msisdn;

  private Instant insertTime;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getMsisdn() {
    return msisdn;
  }

  public void setMsisdn(String msisdn) {
    this.msisdn = msisdn;
  }

  public Instant getInsertTime() {
    return insertTime;
  }

  public void setInsertTime(Instant insertTime) {
    this.insertTime = insertTime;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof AdRegFinalDTO)) {
      return false;
    }

    AdRegFinalDTO adRegFinalDTO = (AdRegFinalDTO) o;
    if (this.id == null) {
      return false;
    }
    return Objects.equals(this.id, adRegFinalDTO.id);
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.id);
  }

  // prettier-ignore
    @Override
    public String toString() {
        return "AdRegFinalDTO{" +
            "id=" + getId() +
            ", msisdn='" + getMsisdn() + "'" +
            ", insertTime='" + getInsertTime() + "'" +
            "}";
    }
}
