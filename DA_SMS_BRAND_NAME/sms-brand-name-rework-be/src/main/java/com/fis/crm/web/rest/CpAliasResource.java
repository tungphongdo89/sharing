package com.fis.crm.web.rest;

import com.fis.crm.repository.CpAliasRepository;
import com.fis.crm.service.dto.CpAliasDTO;
import com.fis.crm.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.fis.crm.domain.CpAlias}.
 */
@RestController
@RequestMapping("/api")
public class CpAliasResource {

  private final Logger log = LoggerFactory.getLogger(CpAliasResource.class);

  private static final String ENTITY_NAME = "cpAlias";

  @Value("${jhipster.clientApp.name}")
  private String applicationName;

  private final CpAliasService cpAliasService;

  private final CpAliasRepository cpAliasRepository;

  public CpAliasResource(CpAliasService cpAliasService, CpAliasRepository cpAliasRepository) {
    this.cpAliasService = cpAliasService;
    this.cpAliasRepository = cpAliasRepository;
  }

  /**
   * {@code POST  /cp-aliases} : Create a new cpAlias.
   *
   * @param cpAliasDTO the cpAliasDTO to create.
   * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new cpAliasDTO, or with status {@code 400 (Bad Request)} if the cpAlias has already an ID.
   * @throws URISyntaxException if the Location URI syntax is incorrect.
   */
  @PostMapping("/cp-aliases")
  public ResponseEntity<CpAliasDTO> createCpAlias(@RequestBody CpAliasDTO cpAliasDTO) throws URISyntaxException {
    log.debug("REST request to save CpAlias : {}", cpAliasDTO);
    if (cpAliasDTO.getId() != null) {
      throw new BadRequestAlertException("A new cpAlias cannot already have an ID", ENTITY_NAME, "idexists");
    }
    CpAliasDTO result = cpAliasService.save(cpAliasDTO);
    return ResponseEntity
      .created(new URI("/api/cp-aliases/" + result.getId()))
      .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
      .body(result);
  }

  /**
   * {@code PUT  /cp-aliases/:id} : Updates an existing cpAlias.
   *
   * @param id the id of the cpAliasDTO to save.
   * @param cpAliasDTO the cpAliasDTO to update.
   * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated cpAliasDTO,
   * or with status {@code 400 (Bad Request)} if the cpAliasDTO is not valid,
   * or with status {@code 500 (Internal Server Error)} if the cpAliasDTO couldn't be updated.
   * @throws URISyntaxException if the Location URI syntax is incorrect.
   */
  @PutMapping("/cp-aliases/{id}")
  public ResponseEntity<CpAliasDTO> updateCpAlias(
    @PathVariable(value = "id", required = false) final Long id,
    @RequestBody CpAliasDTO cpAliasDTO
  ) throws URISyntaxException {
    log.debug("REST request to update CpAlias : {}, {}", id, cpAliasDTO);
    if (cpAliasDTO.getId() == null) {
      throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
    }
    if (!Objects.equals(id, cpAliasDTO.getId())) {
      throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
    }

    if (!cpAliasRepository.existsById(id)) {
      throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
    }

    CpAliasDTO result = cpAliasService.save(cpAliasDTO);
    return ResponseEntity
      .ok()
      .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, cpAliasDTO.getId().toString()))
      .body(result);
  }

  /**
   * {@code PATCH  /cp-aliases/:id} : Partial updates given fields of an existing cpAlias, field will ignore if it is null
   *
   * @param id the id of the cpAliasDTO to save.
   * @param cpAliasDTO the cpAliasDTO to update.
   * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated cpAliasDTO,
   * or with status {@code 400 (Bad Request)} if the cpAliasDTO is not valid,
   * or with status {@code 404 (Not Found)} if the cpAliasDTO is not found,
   * or with status {@code 500 (Internal Server Error)} if the cpAliasDTO couldn't be updated.
   * @throws URISyntaxException if the Location URI syntax is incorrect.
   */
  @PatchMapping(value = "/cp-aliases/{id}", consumes = "application/merge-patch+json")
  public ResponseEntity<CpAliasDTO> partialUpdateCpAlias(
    @PathVariable(value = "id", required = false) final Long id,
    @RequestBody CpAliasDTO cpAliasDTO
  ) throws URISyntaxException {
    log.debug("REST request to partial update CpAlias partially : {}, {}", id, cpAliasDTO);
    if (cpAliasDTO.getId() == null) {
      throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
    }
    if (!Objects.equals(id, cpAliasDTO.getId())) {
      throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
    }

    if (!cpAliasRepository.existsById(id)) {
      throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
    }

    Optional<CpAliasDTO> result = cpAliasService.partialUpdate(cpAliasDTO);

    return ResponseUtil.wrapOrNotFound(
      result,
      HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, cpAliasDTO.getId().toString())
    );
  }

  /**
   * {@code GET  /cp-aliases} : get all the cpAliases.
   *
   * @param pageable the pagination information.
   * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of cpAliases in body.
   */
  @GetMapping("/cp-aliases")
  public ResponseEntity<List<CpAliasDTO>> getAllCpAliases(Pageable pageable) {
    log.debug("REST request to get a page of CpAliases");
    Page<CpAliasDTO> page = cpAliasService.findAll(pageable);
    HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
    return ResponseEntity.ok().headers(headers).body(page.getContent());
  }

  /**
   * {@code GET  /cp-aliases/:id} : get the "id" cpAlias.
   *
   * @param id the id of the cpAliasDTO to retrieve.
   * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the cpAliasDTO, or with status {@code 404 (Not Found)}.
   */
  @GetMapping("/cp-aliases/{id}")
  public ResponseEntity<CpAliasDTO> getCpAlias(@PathVariable Long id) {
    log.debug("REST request to get CpAlias : {}", id);
    Optional<CpAliasDTO> cpAliasDTO = cpAliasService.findOne(id);
    return ResponseUtil.wrapOrNotFound(cpAliasDTO);
  }

  /**
   * {@code DELETE  /cp-aliases/:id} : delete the "id" cpAlias.
   *
   * @param id the id of the cpAliasDTO to delete.
   * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
   */
  @DeleteMapping("/cp-aliases/{id}")
  public ResponseEntity<Void> deleteCpAlias(@PathVariable Long id) {
    log.debug("REST request to delete CpAlias : {}", id);
    cpAliasService.delete(id);
    return ResponseEntity
      .noContent()
      .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
      .build();
  }
}
