package com.fis.crm.repository;

import com.fis.crm.domain.ProvinceBccs;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the ProvinceBccs entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ProvinceBccsRepository extends JpaRepository<ProvinceBccs, Long> {}
