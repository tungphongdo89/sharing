package com.fis.crm.domain;

import java.io.Serializable;
import java.time.Instant;
import javax.persistence.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Ticket.
 */
@Entity
@Table(name = "ticket")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Ticket implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
  @SequenceGenerator(name = "sequenceGenerator")
  private Long id;

  @Column(name = "ticket_id")
  private Long ticketId;

  @Column(name = "channel_type")
  private String channelType;

  @Column(name = "ticket_code")
  private String ticketCode;

  @Column(name = "customer_id")
  private Long customerId;

  @Column(name = "status")
  private String status;

  @Column(name = "fcr")
  private String fcr;

  @Column(name = "department_id")
  private Long departmentId;

  @Column(name = "create_user")
  private Long createUser;

  @Column(name = "create_datetime")
  private Instant createDatetime;

  @Column(name = "update_datetime")
  private Instant updateDatetime;

  // jhipster-needle-entity-add-field - JHipster will add fields here
  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Ticket id(Long id) {
    this.id = id;
    return this;
  }

  public Long getTicketId() {
    return this.ticketId;
  }

  public Ticket ticketId(Long ticketId) {
    this.ticketId = ticketId;
    return this;
  }

  public void setTicketId(Long ticketId) {
    this.ticketId = ticketId;
  }

  public String getChannelType() {
    return this.channelType;
  }

  public Ticket channelType(String channelType) {
    this.channelType = channelType;
    return this;
  }

  public void setChannelType(String channelType) {
    this.channelType = channelType;
  }

  public String getTicketCode() {
    return this.ticketCode;
  }

  public Ticket ticketCode(String ticketCode) {
    this.ticketCode = ticketCode;
    return this;
  }

  public void setTicketCode(String ticketCode) {
    this.ticketCode = ticketCode;
  }

  public Long getCustomerId() {
    return this.customerId;
  }

  public Ticket customerId(Long customerId) {
    this.customerId = customerId;
    return this;
  }

  public void setCustomerId(Long customerId) {
    this.customerId = customerId;
  }

  public String getStatus() {
    return this.status;
  }

  public Ticket status(String status) {
    this.status = status;
    return this;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public String getFcr() {
    return this.fcr;
  }

  public Ticket fcr(String fcr) {
    this.fcr = fcr;
    return this;
  }

  public void setFcr(String fcr) {
    this.fcr = fcr;
  }

  public Long getDepartmentId() {
    return this.departmentId;
  }

  public Ticket departmentId(Long departmentId) {
    this.departmentId = departmentId;
    return this;
  }

  public void setDepartmentId(Long departmentId) {
    this.departmentId = departmentId;
  }

  public Long getCreateUser() {
    return this.createUser;
  }

  public Ticket createUser(Long createUser) {
    this.createUser = createUser;
    return this;
  }

  public void setCreateUser(Long createUser) {
    this.createUser = createUser;
  }

  public Instant getCreateDatetime() {
    return this.createDatetime;
  }

  public Ticket createDatetime(Instant createDatetime) {
    this.createDatetime = createDatetime;
    return this;
  }

  public void setCreateDatetime(Instant createDatetime) {
    this.createDatetime = createDatetime;
  }

  public Instant getUpdateDatetime() {
    return this.updateDatetime;
  }

  public Ticket updateDatetime(Instant updateDatetime) {
    this.updateDatetime = updateDatetime;
    return this;
  }

  public void setUpdateDatetime(Instant updateDatetime) {
    this.updateDatetime = updateDatetime;
  }

  // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof Ticket)) {
      return false;
    }
    return id != null && id.equals(((Ticket) o).id);
  }

  @Override
  public int hashCode() {
    // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
    return getClass().hashCode();
  }

  // prettier-ignore
    @Override
    public String toString() {
        return "Ticket{" +
            "id=" + getId() +
            ", ticketId=" + getTicketId() +
            ", channelType='" + getChannelType() + "'" +
            ", ticketCode='" + getTicketCode() + "'" +
            ", customerId=" + getCustomerId() +
            ", status='" + getStatus() + "'" +
            ", fcr='" + getFcr() + "'" +
            ", departmentId=" + getDepartmentId() +
            ", createUser=" + getCreateUser() +
            ", createDatetime='" + getCreateDatetime() + "'" +
            ", updateDatetime='" + getUpdateDatetime() + "'" +
            "}";
    }
}
