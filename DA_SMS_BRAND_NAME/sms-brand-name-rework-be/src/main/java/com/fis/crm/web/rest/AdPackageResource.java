package com.fis.crm.web.rest;

import com.fis.crm.repository.AdPackageRepository;
import com.fis.crm.service.dto.AdPackageDTO;
import com.fis.crm.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.fis.crm.domain.AdPackage}.
 */
@RestController
@RequestMapping("/api")
public class AdPackageResource {

  private final Logger log = LoggerFactory.getLogger(AdPackageResource.class);

  private static final String ENTITY_NAME = "adPackage";

  @Value("${jhipster.clientApp.name}")
  private String applicationName;

  private final AdPackageService adPackageService;

  private final AdPackageRepository adPackageRepository;

  public AdPackageResource(AdPackageService adPackageService, AdPackageRepository adPackageRepository) {
    this.adPackageService = adPackageService;
    this.adPackageRepository = adPackageRepository;
  }

  /**
   * {@code POST  /ad-packages} : Create a new adPackage.
   *
   * @param adPackageDTO the adPackageDTO to create.
   * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new adPackageDTO, or with status {@code 400 (Bad Request)} if the adPackage has already an ID.
   * @throws URISyntaxException if the Location URI syntax is incorrect.
   */
  @PostMapping("/ad-packages")
  public ResponseEntity<AdPackageDTO> createAdPackage(@RequestBody AdPackageDTO adPackageDTO) throws URISyntaxException {
    log.debug("REST request to save AdPackage : {}", adPackageDTO);
    if (adPackageDTO.getId() != null) {
      throw new BadRequestAlertException("A new adPackage cannot already have an ID", ENTITY_NAME, "idexists");
    }
    AdPackageDTO result = adPackageService.save(adPackageDTO);
    return ResponseEntity
      .created(new URI("/api/ad-packages/" + result.getId()))
      .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
      .body(result);
  }

  /**
   * {@code PUT  /ad-packages/:id} : Updates an existing adPackage.
   *
   * @param id the id of the adPackageDTO to save.
   * @param adPackageDTO the adPackageDTO to update.
   * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated adPackageDTO,
   * or with status {@code 400 (Bad Request)} if the adPackageDTO is not valid,
   * or with status {@code 500 (Internal Server Error)} if the adPackageDTO couldn't be updated.
   * @throws URISyntaxException if the Location URI syntax is incorrect.
   */
  @PutMapping("/ad-packages/{id}")
  public ResponseEntity<AdPackageDTO> updateAdPackage(
    @PathVariable(value = "id", required = false) final Long id,
    @RequestBody AdPackageDTO adPackageDTO
  ) throws URISyntaxException {
    log.debug("REST request to update AdPackage : {}, {}", id, adPackageDTO);
    if (adPackageDTO.getId() == null) {
      throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
    }
    if (!Objects.equals(id, adPackageDTO.getId())) {
      throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
    }

    if (!adPackageRepository.existsById(id)) {
      throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
    }

    AdPackageDTO result = adPackageService.save(adPackageDTO);
    return ResponseEntity
      .ok()
      .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, adPackageDTO.getId().toString()))
      .body(result);
  }

  /**
   * {@code PATCH  /ad-packages/:id} : Partial updates given fields of an existing adPackage, field will ignore if it is null
   *
   * @param id the id of the adPackageDTO to save.
   * @param adPackageDTO the adPackageDTO to update.
   * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated adPackageDTO,
   * or with status {@code 400 (Bad Request)} if the adPackageDTO is not valid,
   * or with status {@code 404 (Not Found)} if the adPackageDTO is not found,
   * or with status {@code 500 (Internal Server Error)} if the adPackageDTO couldn't be updated.
   * @throws URISyntaxException if the Location URI syntax is incorrect.
   */
  @PatchMapping(value = "/ad-packages/{id}", consumes = "application/merge-patch+json")
  public ResponseEntity<AdPackageDTO> partialUpdateAdPackage(
    @PathVariable(value = "id", required = false) final Long id,
    @RequestBody AdPackageDTO adPackageDTO
  ) throws URISyntaxException {
    log.debug("REST request to partial update AdPackage partially : {}, {}", id, adPackageDTO);
    if (adPackageDTO.getId() == null) {
      throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
    }
    if (!Objects.equals(id, adPackageDTO.getId())) {
      throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
    }

    if (!adPackageRepository.existsById(id)) {
      throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
    }

    Optional<AdPackageDTO> result = adPackageService.partialUpdate(adPackageDTO);

    return ResponseUtil.wrapOrNotFound(
      result,
      HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, adPackageDTO.getId().toString())
    );
  }

  /**
   * {@code GET  /ad-packages} : get all the adPackages.
   *
   * @param pageable the pagination information.
   * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of adPackages in body.
   */
  @GetMapping("/ad-packages")
  public ResponseEntity<List<AdPackageDTO>> getAllAdPackages(Pageable pageable) {
    log.debug("REST request to get a page of AdPackages");
    Page<AdPackageDTO> page = adPackageService.findAll(pageable);
    HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
    return ResponseEntity.ok().headers(headers).body(page.getContent());
  }

  /**
   * {@code GET  /ad-packages/:id} : get the "id" adPackage.
   *
   * @param id the id of the adPackageDTO to retrieve.
   * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the adPackageDTO, or with status {@code 404 (Not Found)}.
   */
  @GetMapping("/ad-packages/{id}")
  public ResponseEntity<AdPackageDTO> getAdPackage(@PathVariable Long id) {
    log.debug("REST request to get AdPackage : {}", id);
    Optional<AdPackageDTO> adPackageDTO = adPackageService.findOne(id);
    return ResponseUtil.wrapOrNotFound(adPackageDTO);
  }

  /**
   * {@code DELETE  /ad-packages/:id} : delete the "id" adPackage.
   *
   * @param id the id of the adPackageDTO to delete.
   * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
   */
  @DeleteMapping("/ad-packages/{id}")
  public ResponseEntity<Void> deleteAdPackage(@PathVariable Long id) {
    log.debug("REST request to delete AdPackage : {}", id);
    adPackageService.delete(id);
    return ResponseEntity
      .noContent()
      .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
      .build();
  }
}
