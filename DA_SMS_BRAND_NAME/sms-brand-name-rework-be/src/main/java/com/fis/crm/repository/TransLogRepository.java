package com.fis.crm.repository;

import com.fis.crm.domain.TransLog;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the TransLog entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TransLogRepository extends JpaRepository<TransLog, Long> {}
