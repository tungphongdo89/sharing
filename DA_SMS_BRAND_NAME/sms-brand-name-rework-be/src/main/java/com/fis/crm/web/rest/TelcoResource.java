package com.fis.crm.web.rest;

import com.fis.crm.repository.TelcoRepository;
import com.fis.crm.service.dto.TelcoDTO;
import com.fis.crm.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.fis.crm.domain.Telco}.
 */
@RestController
@RequestMapping("/api")
public class TelcoResource {

  private final Logger log = LoggerFactory.getLogger(TelcoResource.class);

  private static final String ENTITY_NAME = "telco";

  @Value("${jhipster.clientApp.name}")
  private String applicationName;

  private final TelcoService telcoService;

  private final TelcoRepository telcoRepository;

  public TelcoResource(TelcoService telcoService, TelcoRepository telcoRepository) {
    this.telcoService = telcoService;
    this.telcoRepository = telcoRepository;
  }

  /**
   * {@code POST  /telcos} : Create a new telco.
   *
   * @param telcoDTO the telcoDTO to create.
   * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new telcoDTO, or with status {@code 400 (Bad Request)} if the telco has already an ID.
   * @throws URISyntaxException if the Location URI syntax is incorrect.
   */
  @PostMapping("/telcos")
  public ResponseEntity<TelcoDTO> createTelco(@RequestBody TelcoDTO telcoDTO) throws URISyntaxException {
    log.debug("REST request to save Telco : {}", telcoDTO);
    if (telcoDTO.getId() != null) {
      throw new BadRequestAlertException("A new telco cannot already have an ID", ENTITY_NAME, "idexists");
    }
    TelcoDTO result = telcoService.save(telcoDTO);
    return ResponseEntity
      .created(new URI("/api/telcos/" + result.getId()))
      .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
      .body(result);
  }

  /**
   * {@code PUT  /telcos/:id} : Updates an existing telco.
   *
   * @param id the id of the telcoDTO to save.
   * @param telcoDTO the telcoDTO to update.
   * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated telcoDTO,
   * or with status {@code 400 (Bad Request)} if the telcoDTO is not valid,
   * or with status {@code 500 (Internal Server Error)} if the telcoDTO couldn't be updated.
   * @throws URISyntaxException if the Location URI syntax is incorrect.
   */
  @PutMapping("/telcos/{id}")
  public ResponseEntity<TelcoDTO> updateTelco(@PathVariable(value = "id", required = false) final Long id, @RequestBody TelcoDTO telcoDTO)
    throws URISyntaxException {
    log.debug("REST request to update Telco : {}, {}", id, telcoDTO);
    if (telcoDTO.getId() == null) {
      throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
    }
    if (!Objects.equals(id, telcoDTO.getId())) {
      throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
    }

    if (!telcoRepository.existsById(id)) {
      throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
    }

    TelcoDTO result = telcoService.save(telcoDTO);
    return ResponseEntity
      .ok()
      .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, telcoDTO.getId().toString()))
      .body(result);
  }

  /**
   * {@code PATCH  /telcos/:id} : Partial updates given fields of an existing telco, field will ignore if it is null
   *
   * @param id the id of the telcoDTO to save.
   * @param telcoDTO the telcoDTO to update.
   * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated telcoDTO,
   * or with status {@code 400 (Bad Request)} if the telcoDTO is not valid,
   * or with status {@code 404 (Not Found)} if the telcoDTO is not found,
   * or with status {@code 500 (Internal Server Error)} if the telcoDTO couldn't be updated.
   * @throws URISyntaxException if the Location URI syntax is incorrect.
   */
  @PatchMapping(value = "/telcos/{id}", consumes = "application/merge-patch+json")
  public ResponseEntity<TelcoDTO> partialUpdateTelco(
    @PathVariable(value = "id", required = false) final Long id,
    @RequestBody TelcoDTO telcoDTO
  ) throws URISyntaxException {
    log.debug("REST request to partial update Telco partially : {}, {}", id, telcoDTO);
    if (telcoDTO.getId() == null) {
      throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
    }
    if (!Objects.equals(id, telcoDTO.getId())) {
      throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
    }

    if (!telcoRepository.existsById(id)) {
      throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
    }

    Optional<TelcoDTO> result = telcoService.partialUpdate(telcoDTO);

    return ResponseUtil.wrapOrNotFound(
      result,
      HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, telcoDTO.getId().toString())
    );
  }

  /**
   * {@code GET  /telcos} : get all the telcos.
   *
   * @param pageable the pagination information.
   * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of telcos in body.
   */
  @GetMapping("/telcos")
  public ResponseEntity<List<TelcoDTO>> getAllTelcos(Pageable pageable) {
    log.debug("REST request to get a page of Telcos");
    Page<TelcoDTO> page = telcoService.findAll(pageable);
    HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
    return ResponseEntity.ok().headers(headers).body(page.getContent());
  }

  /**
   * {@code GET  /telcos/:id} : get the "id" telco.
   *
   * @param id the id of the telcoDTO to retrieve.
   * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the telcoDTO, or with status {@code 404 (Not Found)}.
   */
  @GetMapping("/telcos/{id}")
  public ResponseEntity<TelcoDTO> getTelco(@PathVariable Long id) {
    log.debug("REST request to get Telco : {}", id);
    Optional<TelcoDTO> telcoDTO = telcoService.findOne(id);
    return ResponseUtil.wrapOrNotFound(telcoDTO);
  }

  /**
   * {@code DELETE  /telcos/:id} : delete the "id" telco.
   *
   * @param id the id of the telcoDTO to delete.
   * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
   */
  @DeleteMapping("/telcos/{id}")
  public ResponseEntity<Void> deleteTelco(@PathVariable Long id) {
    log.debug("REST request to delete Telco : {}", id);
    telcoService.delete(id);
    return ResponseEntity
      .noContent()
      .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
      .build();
  }
}
