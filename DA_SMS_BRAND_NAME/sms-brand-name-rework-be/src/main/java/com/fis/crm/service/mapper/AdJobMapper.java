package com.fis.crm.service.mapper;

import com.fis.crm.domain.*;
import com.fis.crm.service.dto.AdJobDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link AdJob} and its DTO {@link AdJobDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface AdJobMapper extends EntityMapper<AdJobDTO, AdJob> {}
