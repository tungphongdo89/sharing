package com.fis.crm.web.rest;

import com.fis.crm.repository.CpGroupSubRepository;
import com.fis.crm.service.dto.CpGroupSubDTO;
import com.fis.crm.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.fis.crm.domain.CpGroupSub}.
 */
@RestController
@RequestMapping("/api")
public class CpGroupSubResource {

  private final Logger log = LoggerFactory.getLogger(CpGroupSubResource.class);

  private static final String ENTITY_NAME = "cpGroupSub";

  @Value("${jhipster.clientApp.name}")
  private String applicationName;

  private final CpGroupSubService cpGroupSubService;

  private final CpGroupSubRepository cpGroupSubRepository;

  public CpGroupSubResource(CpGroupSubService cpGroupSubService, CpGroupSubRepository cpGroupSubRepository) {
    this.cpGroupSubService = cpGroupSubService;
    this.cpGroupSubRepository = cpGroupSubRepository;
  }

  /**
   * {@code POST  /cp-group-subs} : Create a new cpGroupSub.
   *
   * @param cpGroupSubDTO the cpGroupSubDTO to create.
   * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new cpGroupSubDTO, or with status {@code 400 (Bad Request)} if the cpGroupSub has already an ID.
   * @throws URISyntaxException if the Location URI syntax is incorrect.
   */
  @PostMapping("/cp-group-subs")
  public ResponseEntity<CpGroupSubDTO> createCpGroupSub(@RequestBody CpGroupSubDTO cpGroupSubDTO) throws URISyntaxException {
    log.debug("REST request to save CpGroupSub : {}", cpGroupSubDTO);
    if (cpGroupSubDTO.getId() != null) {
      throw new BadRequestAlertException("A new cpGroupSub cannot already have an ID", ENTITY_NAME, "idexists");
    }
    CpGroupSubDTO result = cpGroupSubService.save(cpGroupSubDTO);
    return ResponseEntity
      .created(new URI("/api/cp-group-subs/" + result.getId()))
      .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
      .body(result);
  }

  /**
   * {@code PUT  /cp-group-subs/:id} : Updates an existing cpGroupSub.
   *
   * @param id the id of the cpGroupSubDTO to save.
   * @param cpGroupSubDTO the cpGroupSubDTO to update.
   * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated cpGroupSubDTO,
   * or with status {@code 400 (Bad Request)} if the cpGroupSubDTO is not valid,
   * or with status {@code 500 (Internal Server Error)} if the cpGroupSubDTO couldn't be updated.
   * @throws URISyntaxException if the Location URI syntax is incorrect.
   */
  @PutMapping("/cp-group-subs/{id}")
  public ResponseEntity<CpGroupSubDTO> updateCpGroupSub(
    @PathVariable(value = "id", required = false) final Long id,
    @RequestBody CpGroupSubDTO cpGroupSubDTO
  ) throws URISyntaxException {
    log.debug("REST request to update CpGroupSub : {}, {}", id, cpGroupSubDTO);
    if (cpGroupSubDTO.getId() == null) {
      throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
    }
    if (!Objects.equals(id, cpGroupSubDTO.getId())) {
      throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
    }

    if (!cpGroupSubRepository.existsById(id)) {
      throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
    }

    CpGroupSubDTO result = cpGroupSubService.save(cpGroupSubDTO);
    return ResponseEntity
      .ok()
      .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, cpGroupSubDTO.getId().toString()))
      .body(result);
  }

  /**
   * {@code PATCH  /cp-group-subs/:id} : Partial updates given fields of an existing cpGroupSub, field will ignore if it is null
   *
   * @param id the id of the cpGroupSubDTO to save.
   * @param cpGroupSubDTO the cpGroupSubDTO to update.
   * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated cpGroupSubDTO,
   * or with status {@code 400 (Bad Request)} if the cpGroupSubDTO is not valid,
   * or with status {@code 404 (Not Found)} if the cpGroupSubDTO is not found,
   * or with status {@code 500 (Internal Server Error)} if the cpGroupSubDTO couldn't be updated.
   * @throws URISyntaxException if the Location URI syntax is incorrect.
   */
  @PatchMapping(value = "/cp-group-subs/{id}", consumes = "application/merge-patch+json")
  public ResponseEntity<CpGroupSubDTO> partialUpdateCpGroupSub(
    @PathVariable(value = "id", required = false) final Long id,
    @RequestBody CpGroupSubDTO cpGroupSubDTO
  ) throws URISyntaxException {
    log.debug("REST request to partial update CpGroupSub partially : {}, {}", id, cpGroupSubDTO);
    if (cpGroupSubDTO.getId() == null) {
      throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
    }
    if (!Objects.equals(id, cpGroupSubDTO.getId())) {
      throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
    }

    if (!cpGroupSubRepository.existsById(id)) {
      throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
    }

    Optional<CpGroupSubDTO> result = cpGroupSubService.partialUpdate(cpGroupSubDTO);

    return ResponseUtil.wrapOrNotFound(
      result,
      HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, cpGroupSubDTO.getId().toString())
    );
  }

  /**
   * {@code GET  /cp-group-subs} : get all the cpGroupSubs.
   *
   * @param pageable the pagination information.
   * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of cpGroupSubs in body.
   */
  @GetMapping("/cp-group-subs")
  public ResponseEntity<List<CpGroupSubDTO>> getAllCpGroupSubs(Pageable pageable) {
    log.debug("REST request to get a page of CpGroupSubs");
    Page<CpGroupSubDTO> page = cpGroupSubService.findAll(pageable);
    HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
    return ResponseEntity.ok().headers(headers).body(page.getContent());
  }

  /**
   * {@code GET  /cp-group-subs/:id} : get the "id" cpGroupSub.
   *
   * @param id the id of the cpGroupSubDTO to retrieve.
   * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the cpGroupSubDTO, or with status {@code 404 (Not Found)}.
   */
  @GetMapping("/cp-group-subs/{id}")
  public ResponseEntity<CpGroupSubDTO> getCpGroupSub(@PathVariable Long id) {
    log.debug("REST request to get CpGroupSub : {}", id);
    Optional<CpGroupSubDTO> cpGroupSubDTO = cpGroupSubService.findOne(id);
    return ResponseUtil.wrapOrNotFound(cpGroupSubDTO);
  }

  /**
   * {@code DELETE  /cp-group-subs/:id} : delete the "id" cpGroupSub.
   *
   * @param id the id of the cpGroupSubDTO to delete.
   * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
   */
  @DeleteMapping("/cp-group-subs/{id}")
  public ResponseEntity<Void> deleteCpGroupSub(@PathVariable Long id) {
    log.debug("REST request to delete CpGroupSub : {}", id);
    cpGroupSubService.delete(id);
    return ResponseEntity
      .noContent()
      .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
      .build();
  }
}
