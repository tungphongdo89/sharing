package com.fis.crm.service.dto;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

/**
 * A DTO for the {@link com.fis.crm.domain.TransLog} entity.
 */
public class TransLogDTO implements Serializable {

  private Long id;

  private Long transId;

  private Long cpId;

  private Instant transTime;

  private Long chanel;

  private Long amount;

  private String transNote;

  private String alias;

  private Long process;

  private Long balanceType;

  private Long balanceBefore;

  private Long balanceAfter;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Long getTransId() {
    return transId;
  }

  public void setTransId(Long transId) {
    this.transId = transId;
  }

  public Long getCpId() {
    return cpId;
  }

  public void setCpId(Long cpId) {
    this.cpId = cpId;
  }

  public Instant getTransTime() {
    return transTime;
  }

  public void setTransTime(Instant transTime) {
    this.transTime = transTime;
  }

  public Long getChanel() {
    return chanel;
  }

  public void setChanel(Long chanel) {
    this.chanel = chanel;
  }

  public Long getAmount() {
    return amount;
  }

  public void setAmount(Long amount) {
    this.amount = amount;
  }

  public String getTransNote() {
    return transNote;
  }

  public void setTransNote(String transNote) {
    this.transNote = transNote;
  }

  public String getAlias() {
    return alias;
  }

  public void setAlias(String alias) {
    this.alias = alias;
  }

  public Long getProcess() {
    return process;
  }

  public void setProcess(Long process) {
    this.process = process;
  }

  public Long getBalanceType() {
    return balanceType;
  }

  public void setBalanceType(Long balanceType) {
    this.balanceType = balanceType;
  }

  public Long getBalanceBefore() {
    return balanceBefore;
  }

  public void setBalanceBefore(Long balanceBefore) {
    this.balanceBefore = balanceBefore;
  }

  public Long getBalanceAfter() {
    return balanceAfter;
  }

  public void setBalanceAfter(Long balanceAfter) {
    this.balanceAfter = balanceAfter;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof TransLogDTO)) {
      return false;
    }

    TransLogDTO transLogDTO = (TransLogDTO) o;
    if (this.id == null) {
      return false;
    }
    return Objects.equals(this.id, transLogDTO.id);
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.id);
  }

  // prettier-ignore
    @Override
    public String toString() {
        return "TransLogDTO{" +
            "id=" + getId() +
            ", transId=" + getTransId() +
            ", cpId=" + getCpId() +
            ", transTime='" + getTransTime() + "'" +
            ", chanel=" + getChanel() +
            ", amount=" + getAmount() +
            ", transNote='" + getTransNote() + "'" +
            ", alias='" + getAlias() + "'" +
            ", process=" + getProcess() +
            ", balanceType=" + getBalanceType() +
            ", balanceBefore=" + getBalanceBefore() +
            ", balanceAfter=" + getBalanceAfter() +
            "}";
    }
}
