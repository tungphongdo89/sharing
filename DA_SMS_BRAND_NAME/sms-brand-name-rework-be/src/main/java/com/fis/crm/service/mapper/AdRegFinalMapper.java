package com.fis.crm.service.mapper;

import com.fis.crm.domain.*;
import com.fis.crm.service.dto.AdRegFinalDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link AdRegFinal} and its DTO {@link AdRegFinalDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface AdRegFinalMapper extends EntityMapper<AdRegFinalDTO, AdRegFinal> {}
