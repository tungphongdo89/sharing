package com.fis.crm.service.mapper;

import com.fis.crm.domain.*;
import com.fis.crm.service.dto.CpGroupDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link CpGroup} and its DTO {@link CpGroupDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface CpGroupMapper extends EntityMapper<CpGroupDTO, CpGroup> {}
