package com.fis.crm.domain;

import java.io.Serializable;
import javax.persistence.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Telco.
 */
@Entity
@Table(name = "telco")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Telco implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
  @SequenceGenerator(name = "sequenceGenerator")
  private Long id;

  @Column(name = "telco_id")
  private Long telcoId;

  @Column(name = "telco_name")
  private String telcoName;

  @Column(name = "telco_code")
  private String telcoCode;

  @Column(name = "telco_prefix")
  private String telcoPrefix;

  @Column(name = "is_viettel_group")
  private Long isViettelGroup;

  @Column(name = "prefix_detail")
  private String prefixDetail;

  @Column(name = "send_type")
  private Long sendType;

  @Column(name = "group_name")
  private String groupName;

  @Column(name = "can_free_sms")
  private Long canFreeSms;

  @Column(name = "is_vietnamese")
  private Long isVietnamese;

  // jhipster-needle-entity-add-field - JHipster will add fields here
  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Telco id(Long id) {
    this.id = id;
    return this;
  }

  public Long getTelcoId() {
    return this.telcoId;
  }

  public Telco telcoId(Long telcoId) {
    this.telcoId = telcoId;
    return this;
  }

  public void setTelcoId(Long telcoId) {
    this.telcoId = telcoId;
  }

  public String getTelcoName() {
    return this.telcoName;
  }

  public Telco telcoName(String telcoName) {
    this.telcoName = telcoName;
    return this;
  }

  public void setTelcoName(String telcoName) {
    this.telcoName = telcoName;
  }

  public String getTelcoCode() {
    return this.telcoCode;
  }

  public Telco telcoCode(String telcoCode) {
    this.telcoCode = telcoCode;
    return this;
  }

  public void setTelcoCode(String telcoCode) {
    this.telcoCode = telcoCode;
  }

  public String getTelcoPrefix() {
    return this.telcoPrefix;
  }

  public Telco telcoPrefix(String telcoPrefix) {
    this.telcoPrefix = telcoPrefix;
    return this;
  }

  public void setTelcoPrefix(String telcoPrefix) {
    this.telcoPrefix = telcoPrefix;
  }

  public Long getIsViettelGroup() {
    return this.isViettelGroup;
  }

  public Telco isViettelGroup(Long isViettelGroup) {
    this.isViettelGroup = isViettelGroup;
    return this;
  }

  public void setIsViettelGroup(Long isViettelGroup) {
    this.isViettelGroup = isViettelGroup;
  }

  public String getPrefixDetail() {
    return this.prefixDetail;
  }

  public Telco prefixDetail(String prefixDetail) {
    this.prefixDetail = prefixDetail;
    return this;
  }

  public void setPrefixDetail(String prefixDetail) {
    this.prefixDetail = prefixDetail;
  }

  public Long getSendType() {
    return this.sendType;
  }

  public Telco sendType(Long sendType) {
    this.sendType = sendType;
    return this;
  }

  public void setSendType(Long sendType) {
    this.sendType = sendType;
  }

  public String getGroupName() {
    return this.groupName;
  }

  public Telco groupName(String groupName) {
    this.groupName = groupName;
    return this;
  }

  public void setGroupName(String groupName) {
    this.groupName = groupName;
  }

  public Long getCanFreeSms() {
    return this.canFreeSms;
  }

  public Telco canFreeSms(Long canFreeSms) {
    this.canFreeSms = canFreeSms;
    return this;
  }

  public void setCanFreeSms(Long canFreeSms) {
    this.canFreeSms = canFreeSms;
  }

  public Long getIsVietnamese() {
    return this.isVietnamese;
  }

  public Telco isVietnamese(Long isVietnamese) {
    this.isVietnamese = isVietnamese;
    return this;
  }

  public void setIsVietnamese(Long isVietnamese) {
    this.isVietnamese = isVietnamese;
  }

  // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof Telco)) {
      return false;
    }
    return id != null && id.equals(((Telco) o).id);
  }

  @Override
  public int hashCode() {
    // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
    return getClass().hashCode();
  }

  // prettier-ignore
    @Override
    public String toString() {
        return "Telco{" +
            "id=" + getId() +
            ", telcoId=" + getTelcoId() +
            ", telcoName='" + getTelcoName() + "'" +
            ", telcoCode='" + getTelcoCode() + "'" +
            ", telcoPrefix='" + getTelcoPrefix() + "'" +
            ", isViettelGroup=" + getIsViettelGroup() +
            ", prefixDetail='" + getPrefixDetail() + "'" +
            ", sendType=" + getSendType() +
            ", groupName='" + getGroupName() + "'" +
            ", canFreeSms=" + getCanFreeSms() +
            ", isVietnamese=" + getIsVietnamese() +
            "}";
    }
}
