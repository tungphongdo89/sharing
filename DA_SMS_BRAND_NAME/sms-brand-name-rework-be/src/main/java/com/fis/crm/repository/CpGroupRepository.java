package com.fis.crm.repository;

import com.fis.crm.domain.CpGroup;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the CpGroup entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CpGroupRepository extends JpaRepository<CpGroup, Long> {}
