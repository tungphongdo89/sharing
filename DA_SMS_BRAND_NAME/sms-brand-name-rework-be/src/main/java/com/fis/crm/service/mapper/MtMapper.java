package com.fis.crm.service.mapper;

import com.fis.crm.domain.*;
import com.fis.crm.service.dto.MtDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Mt} and its DTO {@link MtDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface MtMapper extends EntityMapper<MtDTO, Mt> {}
