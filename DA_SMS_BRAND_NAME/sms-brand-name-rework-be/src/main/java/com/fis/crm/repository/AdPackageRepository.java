package com.fis.crm.repository;

import com.fis.crm.domain.AdPackage;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the AdPackage entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AdPackageRepository extends JpaRepository<AdPackage, Long> {}
