package com.fis.crm.repository;

import com.fis.crm.domain.TicketRequest;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the TicketRequest entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TicketRequestRepository extends JpaRepository<TicketRequest, Long> {}
