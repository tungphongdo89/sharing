package com.fis.crm.service.mapper;

import com.fis.crm.domain.*;
import com.fis.crm.service.dto.TelcoDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Telco} and its DTO {@link TelcoDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface TelcoMapper extends EntityMapper<TelcoDTO, Telco> {}
