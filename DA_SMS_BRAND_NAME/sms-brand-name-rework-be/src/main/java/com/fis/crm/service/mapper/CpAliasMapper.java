package com.fis.crm.service.mapper;

import com.fis.crm.domain.*;
import com.fis.crm.service.dto.CpAliasDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link CpAlias} and its DTO {@link CpAliasDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface CpAliasMapper extends EntityMapper<CpAliasDTO, CpAlias> {}
