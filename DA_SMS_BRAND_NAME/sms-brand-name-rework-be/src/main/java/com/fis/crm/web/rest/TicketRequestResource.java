package com.fis.crm.web.rest;

import com.fis.crm.repository.TicketRequestRepository;
import com.fis.crm.service.TicketRequestService;
import com.fis.crm.service.dto.TicketRequestDTO;
import com.fis.crm.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.fis.crm.domain.TicketRequest}.
 */
@RestController
@RequestMapping("/api")
public class TicketRequestResource {

  private final Logger log = LoggerFactory.getLogger(TicketRequestResource.class);

  private static final String ENTITY_NAME = "ticketRequest";

  @Value("${jhipster.clientApp.name}")
  private String applicationName;

  private final TicketRequestService ticketRequestService;

  private final TicketRequestRepository ticketRequestRepository;

  public TicketRequestResource(TicketRequestService ticketRequestService, TicketRequestRepository ticketRequestRepository) {
    this.ticketRequestService = ticketRequestService;
    this.ticketRequestRepository = ticketRequestRepository;
  }

  /**
   * {@code POST  /ticket-requests} : Create a new ticketRequest.
   *
   * @param ticketRequestDTO the ticketRequestDTO to create.
   * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new ticketRequestDTO, or with status {@code 400 (Bad Request)} if the ticketRequest has already an ID.
   * @throws URISyntaxException if the Location URI syntax is incorrect.
   */
  @PostMapping("/ticket-requests")
  public ResponseEntity<TicketRequestDTO> createTicketRequest(@RequestBody TicketRequestDTO ticketRequestDTO) throws URISyntaxException {
    log.debug("REST request to save TicketRequest : {}", ticketRequestDTO);
    if (ticketRequestDTO.getId() != null) {
      throw new BadRequestAlertException("A new ticketRequest cannot already have an ID", ENTITY_NAME, "idexists");
    }
    TicketRequestDTO result = ticketRequestService.save(ticketRequestDTO);
    return ResponseEntity
      .created(new URI("/api/ticket-requests/" + result.getId()))
      .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
      .body(result);
  }

  /**
   * {@code PUT  /ticket-requests/:id} : Updates an existing ticketRequest.
   *
   * @param id the id of the ticketRequestDTO to save.
   * @param ticketRequestDTO the ticketRequestDTO to update.
   * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated ticketRequestDTO,
   * or with status {@code 400 (Bad Request)} if the ticketRequestDTO is not valid,
   * or with status {@code 500 (Internal Server Error)} if the ticketRequestDTO couldn't be updated.
   * @throws URISyntaxException if the Location URI syntax is incorrect.
   */
  @PutMapping("/ticket-requests/{id}")
  public ResponseEntity<TicketRequestDTO> updateTicketRequest(
    @PathVariable(value = "id", required = false) final Long id,
    @RequestBody TicketRequestDTO ticketRequestDTO
  ) throws URISyntaxException {
    log.debug("REST request to update TicketRequest : {}, {}", id, ticketRequestDTO);
    if (ticketRequestDTO.getId() == null) {
      throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
    }
    if (!Objects.equals(id, ticketRequestDTO.getId())) {
      throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
    }

    if (!ticketRequestRepository.existsById(id)) {
      throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
    }

    TicketRequestDTO result = ticketRequestService.save(ticketRequestDTO);
    return ResponseEntity
      .ok()
      .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, ticketRequestDTO.getId().toString()))
      .body(result);
  }

  /**
   * {@code PATCH  /ticket-requests/:id} : Partial updates given fields of an existing ticketRequest, field will ignore if it is null
   *
   * @param id the id of the ticketRequestDTO to save.
   * @param ticketRequestDTO the ticketRequestDTO to update.
   * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated ticketRequestDTO,
   * or with status {@code 400 (Bad Request)} if the ticketRequestDTO is not valid,
   * or with status {@code 404 (Not Found)} if the ticketRequestDTO is not found,
   * or with status {@code 500 (Internal Server Error)} if the ticketRequestDTO couldn't be updated.
   * @throws URISyntaxException if the Location URI syntax is incorrect.
   */
  @PatchMapping(value = "/ticket-requests/{id}", consumes = "application/merge-patch+json")
  public ResponseEntity<TicketRequestDTO> partialUpdateTicketRequest(
    @PathVariable(value = "id", required = false) final Long id,
    @RequestBody TicketRequestDTO ticketRequestDTO
  ) throws URISyntaxException {
    log.debug("REST request to partial update TicketRequest partially : {}, {}", id, ticketRequestDTO);
    if (ticketRequestDTO.getId() == null) {
      throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
    }
    if (!Objects.equals(id, ticketRequestDTO.getId())) {
      throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
    }

    if (!ticketRequestRepository.existsById(id)) {
      throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
    }

    Optional<TicketRequestDTO> result = ticketRequestService.partialUpdate(ticketRequestDTO);

    return ResponseUtil.wrapOrNotFound(
      result,
      HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, ticketRequestDTO.getId().toString())
    );
  }

  /**
   * {@code GET  /ticket-requests} : get all the ticketRequests.
   *
   * @param pageable the pagination information.
   * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of ticketRequests in body.
   */
  @GetMapping("/ticket-requests")
  public ResponseEntity<List<TicketRequestDTO>> getAllTicketRequests(Pageable pageable) {
    log.debug("REST request to get a page of TicketRequests");
    Page<TicketRequestDTO> page = ticketRequestService.findAll(pageable);
    HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
    return ResponseEntity.ok().headers(headers).body(page.getContent());
  }

  /**
   * {@code GET  /ticket-requests/:id} : get the "id" ticketRequest.
   *
   * @param id the id of the ticketRequestDTO to retrieve.
   * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the ticketRequestDTO, or with status {@code 404 (Not Found)}.
   */
  @GetMapping("/ticket-requests/{id}")
  public ResponseEntity<TicketRequestDTO> getTicketRequest(@PathVariable Long id) {
    log.debug("REST request to get TicketRequest : {}", id);
    Optional<TicketRequestDTO> ticketRequestDTO = ticketRequestService.findOne(id);
    return ResponseUtil.wrapOrNotFound(ticketRequestDTO);
  }

  /**
   * {@code DELETE  /ticket-requests/:id} : delete the "id" ticketRequest.
   *
   * @param id the id of the ticketRequestDTO to delete.
   * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
   */
  @DeleteMapping("/ticket-requests/{id}")
  public ResponseEntity<Void> deleteTicketRequest(@PathVariable Long id) {
    log.debug("REST request to delete TicketRequest : {}", id);
    ticketRequestService.delete(id);
    return ResponseEntity
      .noContent()
      .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
      .build();
  }
}
