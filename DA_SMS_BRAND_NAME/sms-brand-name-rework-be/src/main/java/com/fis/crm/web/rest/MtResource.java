package com.fis.crm.web.rest;

import com.fis.crm.repository.MtRepository;
import com.fis.crm.service.dto.MtDTO;
import com.fis.crm.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.fis.crm.domain.Mt}.
 */
@RestController
@RequestMapping("/api")
public class MtResource {

  private final Logger log = LoggerFactory.getLogger(MtResource.class);

  private static final String ENTITY_NAME = "mt";

  @Value("${jhipster.clientApp.name}")
  private String applicationName;

  private final MtService mtService;

  private final MtRepository mtRepository;

  public MtResource(MtService mtService, MtRepository mtRepository) {
    this.mtService = mtService;
    this.mtRepository = mtRepository;
  }

  /**
   * {@code POST  /mts} : Create a new mt.
   *
   * @param mtDTO the mtDTO to create.
   * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new mtDTO, or with status {@code 400 (Bad Request)} if the mt has already an ID.
   * @throws URISyntaxException if the Location URI syntax is incorrect.
   */
  @PostMapping("/mts")
  public ResponseEntity<MtDTO> createMt(@RequestBody MtDTO mtDTO) throws URISyntaxException {
    log.debug("REST request to save Mt : {}", mtDTO);
    if (mtDTO.getId() != null) {
      throw new BadRequestAlertException("A new mt cannot already have an ID", ENTITY_NAME, "idexists");
    }
    MtDTO result = mtService.save(mtDTO);
    return ResponseEntity
      .created(new URI("/api/mts/" + result.getId()))
      .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
      .body(result);
  }

  /**
   * {@code PUT  /mts/:id} : Updates an existing mt.
   *
   * @param id the id of the mtDTO to save.
   * @param mtDTO the mtDTO to update.
   * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated mtDTO,
   * or with status {@code 400 (Bad Request)} if the mtDTO is not valid,
   * or with status {@code 500 (Internal Server Error)} if the mtDTO couldn't be updated.
   * @throws URISyntaxException if the Location URI syntax is incorrect.
   */
  @PutMapping("/mts/{id}")
  public ResponseEntity<MtDTO> updateMt(@PathVariable(value = "id", required = false) final Long id, @RequestBody MtDTO mtDTO)
    throws URISyntaxException {
    log.debug("REST request to update Mt : {}, {}", id, mtDTO);
    if (mtDTO.getId() == null) {
      throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
    }
    if (!Objects.equals(id, mtDTO.getId())) {
      throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
    }

    if (!mtRepository.existsById(id)) {
      throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
    }

    MtDTO result = mtService.save(mtDTO);
    return ResponseEntity
      .ok()
      .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, mtDTO.getId().toString()))
      .body(result);
  }

  /**
   * {@code PATCH  /mts/:id} : Partial updates given fields of an existing mt, field will ignore if it is null
   *
   * @param id the id of the mtDTO to save.
   * @param mtDTO the mtDTO to update.
   * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated mtDTO,
   * or with status {@code 400 (Bad Request)} if the mtDTO is not valid,
   * or with status {@code 404 (Not Found)} if the mtDTO is not found,
   * or with status {@code 500 (Internal Server Error)} if the mtDTO couldn't be updated.
   * @throws URISyntaxException if the Location URI syntax is incorrect.
   */
  @PatchMapping(value = "/mts/{id}", consumes = "application/merge-patch+json")
  public ResponseEntity<MtDTO> partialUpdateMt(@PathVariable(value = "id", required = false) final Long id, @RequestBody MtDTO mtDTO)
    throws URISyntaxException {
    log.debug("REST request to partial update Mt partially : {}, {}", id, mtDTO);
    if (mtDTO.getId() == null) {
      throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
    }
    if (!Objects.equals(id, mtDTO.getId())) {
      throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
    }

    if (!mtRepository.existsById(id)) {
      throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
    }

    Optional<MtDTO> result = mtService.partialUpdate(mtDTO);

    return ResponseUtil.wrapOrNotFound(
      result,
      HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, mtDTO.getId().toString())
    );
  }

  /**
   * {@code GET  /mts} : get all the mts.
   *
   * @param pageable the pagination information.
   * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of mts in body.
   */
  @GetMapping("/mts")
  public ResponseEntity<List<MtDTO>> getAllMts(Pageable pageable) {
    log.debug("REST request to get a page of Mts");
    Page<MtDTO> page = mtService.findAll(pageable);
    HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
    return ResponseEntity.ok().headers(headers).body(page.getContent());
  }

  /**
   * {@code GET  /mts/:id} : get the "id" mt.
   *
   * @param id the id of the mtDTO to retrieve.
   * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the mtDTO, or with status {@code 404 (Not Found)}.
   */
  @GetMapping("/mts/{id}")
  public ResponseEntity<MtDTO> getMt(@PathVariable Long id) {
    log.debug("REST request to get Mt : {}", id);
    Optional<MtDTO> mtDTO = mtService.findOne(id);
    return ResponseUtil.wrapOrNotFound(mtDTO);
  }

  /**
   * {@code DELETE  /mts/:id} : delete the "id" mt.
   *
   * @param id the id of the mtDTO to delete.
   * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
   */
  @DeleteMapping("/mts/{id}")
  public ResponseEntity<Void> deleteMt(@PathVariable Long id) {
    log.debug("REST request to delete Mt : {}", id);
    mtService.delete(id);
    return ResponseEntity
      .noContent()
      .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
      .build();
  }
}
