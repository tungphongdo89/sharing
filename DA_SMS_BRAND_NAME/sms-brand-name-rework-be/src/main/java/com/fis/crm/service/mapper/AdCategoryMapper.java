package com.fis.crm.service.mapper;

import com.fis.crm.domain.*;
import com.fis.crm.service.dto.AdCategoryDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link AdCategory} and its DTO {@link AdCategoryDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface AdCategoryMapper extends EntityMapper<AdCategoryDTO, AdCategory> {}
