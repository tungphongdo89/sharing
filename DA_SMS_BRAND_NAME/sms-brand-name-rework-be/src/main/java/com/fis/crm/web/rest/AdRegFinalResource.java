package com.fis.crm.web.rest;

import com.fis.crm.repository.AdRegFinalRepository;
import com.fis.crm.service.dto.AdRegFinalDTO;
import com.fis.crm.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.fis.crm.domain.AdRegFinal}.
 */
@RestController
@RequestMapping("/api")
public class AdRegFinalResource {

  private final Logger log = LoggerFactory.getLogger(AdRegFinalResource.class);

  private static final String ENTITY_NAME = "adRegFinal";

  @Value("${jhipster.clientApp.name}")
  private String applicationName;

  private final AdRegFinalService adRegFinalService;

  private final AdRegFinalRepository adRegFinalRepository;

  public AdRegFinalResource(AdRegFinalService adRegFinalService, AdRegFinalRepository adRegFinalRepository) {
    this.adRegFinalService = adRegFinalService;
    this.adRegFinalRepository = adRegFinalRepository;
  }

  /**
   * {@code POST  /ad-reg-finals} : Create a new adRegFinal.
   *
   * @param adRegFinalDTO the adRegFinalDTO to create.
   * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new adRegFinalDTO, or with status {@code 400 (Bad Request)} if the adRegFinal has already an ID.
   * @throws URISyntaxException if the Location URI syntax is incorrect.
   */
  @PostMapping("/ad-reg-finals")
  public ResponseEntity<AdRegFinalDTO> createAdRegFinal(@RequestBody AdRegFinalDTO adRegFinalDTO) throws URISyntaxException {
    log.debug("REST request to save AdRegFinal : {}", adRegFinalDTO);
    if (adRegFinalDTO.getId() != null) {
      throw new BadRequestAlertException("A new adRegFinal cannot already have an ID", ENTITY_NAME, "idexists");
    }
    AdRegFinalDTO result = adRegFinalService.save(adRegFinalDTO);
    return ResponseEntity
      .created(new URI("/api/ad-reg-finals/" + result.getId()))
      .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
      .body(result);
  }

  /**
   * {@code PUT  /ad-reg-finals/:id} : Updates an existing adRegFinal.
   *
   * @param id the id of the adRegFinalDTO to save.
   * @param adRegFinalDTO the adRegFinalDTO to update.
   * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated adRegFinalDTO,
   * or with status {@code 400 (Bad Request)} if the adRegFinalDTO is not valid,
   * or with status {@code 500 (Internal Server Error)} if the adRegFinalDTO couldn't be updated.
   * @throws URISyntaxException if the Location URI syntax is incorrect.
   */
  @PutMapping("/ad-reg-finals/{id}")
  public ResponseEntity<AdRegFinalDTO> updateAdRegFinal(
    @PathVariable(value = "id", required = false) final Long id,
    @RequestBody AdRegFinalDTO adRegFinalDTO
  ) throws URISyntaxException {
    log.debug("REST request to update AdRegFinal : {}, {}", id, adRegFinalDTO);
    if (adRegFinalDTO.getId() == null) {
      throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
    }
    if (!Objects.equals(id, adRegFinalDTO.getId())) {
      throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
    }

    if (!adRegFinalRepository.existsById(id)) {
      throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
    }

    AdRegFinalDTO result = adRegFinalService.save(adRegFinalDTO);
    return ResponseEntity
      .ok()
      .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, adRegFinalDTO.getId().toString()))
      .body(result);
  }

  /**
   * {@code PATCH  /ad-reg-finals/:id} : Partial updates given fields of an existing adRegFinal, field will ignore if it is null
   *
   * @param id the id of the adRegFinalDTO to save.
   * @param adRegFinalDTO the adRegFinalDTO to update.
   * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated adRegFinalDTO,
   * or with status {@code 400 (Bad Request)} if the adRegFinalDTO is not valid,
   * or with status {@code 404 (Not Found)} if the adRegFinalDTO is not found,
   * or with status {@code 500 (Internal Server Error)} if the adRegFinalDTO couldn't be updated.
   * @throws URISyntaxException if the Location URI syntax is incorrect.
   */
  @PatchMapping(value = "/ad-reg-finals/{id}", consumes = "application/merge-patch+json")
  public ResponseEntity<AdRegFinalDTO> partialUpdateAdRegFinal(
    @PathVariable(value = "id", required = false) final Long id,
    @RequestBody AdRegFinalDTO adRegFinalDTO
  ) throws URISyntaxException {
    log.debug("REST request to partial update AdRegFinal partially : {}, {}", id, adRegFinalDTO);
    if (adRegFinalDTO.getId() == null) {
      throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
    }
    if (!Objects.equals(id, adRegFinalDTO.getId())) {
      throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
    }

    if (!adRegFinalRepository.existsById(id)) {
      throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
    }

    Optional<AdRegFinalDTO> result = adRegFinalService.partialUpdate(adRegFinalDTO);

    return ResponseUtil.wrapOrNotFound(
      result,
      HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, adRegFinalDTO.getId().toString())
    );
  }

  /**
   * {@code GET  /ad-reg-finals} : get all the adRegFinals.
   *
   * @param pageable the pagination information.
   * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of adRegFinals in body.
   */
  @GetMapping("/ad-reg-finals")
  public ResponseEntity<List<AdRegFinalDTO>> getAllAdRegFinals(Pageable pageable) {
    log.debug("REST request to get a page of AdRegFinals");
    Page<AdRegFinalDTO> page = adRegFinalService.findAll(pageable);
    HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
    return ResponseEntity.ok().headers(headers).body(page.getContent());
  }

  /**
   * {@code GET  /ad-reg-finals/:id} : get the "id" adRegFinal.
   *
   * @param id the id of the adRegFinalDTO to retrieve.
   * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the adRegFinalDTO, or with status {@code 404 (Not Found)}.
   */
  @GetMapping("/ad-reg-finals/{id}")
  public ResponseEntity<AdRegFinalDTO> getAdRegFinal(@PathVariable Long id) {
    log.debug("REST request to get AdRegFinal : {}", id);
    Optional<AdRegFinalDTO> adRegFinalDTO = adRegFinalService.findOne(id);
    return ResponseUtil.wrapOrNotFound(adRegFinalDTO);
  }

  /**
   * {@code DELETE  /ad-reg-finals/:id} : delete the "id" adRegFinal.
   *
   * @param id the id of the adRegFinalDTO to delete.
   * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
   */
  @DeleteMapping("/ad-reg-finals/{id}")
  public ResponseEntity<Void> deleteAdRegFinal(@PathVariable Long id) {
    log.debug("REST request to delete AdRegFinal : {}", id);
    adRegFinalService.delete(id);
    return ResponseEntity
      .noContent()
      .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
      .build();
  }
}
