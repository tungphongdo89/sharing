package com.fis.crm.web.rest;

import com.fis.crm.repository.CpGroupRepository;
import com.fis.crm.service.dto.CpGroupDTO;
import com.fis.crm.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.fis.crm.domain.CpGroup}.
 */
@RestController
@RequestMapping("/api")
public class CpGroupResource {

  private final Logger log = LoggerFactory.getLogger(CpGroupResource.class);

  private static final String ENTITY_NAME = "cpGroup";

  @Value("${jhipster.clientApp.name}")
  private String applicationName;

  private final CpGroupService cpGroupService;

  private final CpGroupRepository cpGroupRepository;

  public CpGroupResource(CpGroupService cpGroupService, CpGroupRepository cpGroupRepository) {
    this.cpGroupService = cpGroupService;
    this.cpGroupRepository = cpGroupRepository;
  }

  /**
   * {@code POST  /cp-groups} : Create a new cpGroup.
   *
   * @param cpGroupDTO the cpGroupDTO to create.
   * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new cpGroupDTO, or with status {@code 400 (Bad Request)} if the cpGroup has already an ID.
   * @throws URISyntaxException if the Location URI syntax is incorrect.
   */
  @PostMapping("/cp-groups")
  public ResponseEntity<CpGroupDTO> createCpGroup(@RequestBody CpGroupDTO cpGroupDTO) throws URISyntaxException {
    log.debug("REST request to save CpGroup : {}", cpGroupDTO);
    if (cpGroupDTO.getId() != null) {
      throw new BadRequestAlertException("A new cpGroup cannot already have an ID", ENTITY_NAME, "idexists");
    }
    CpGroupDTO result = cpGroupService.save(cpGroupDTO);
    return ResponseEntity
      .created(new URI("/api/cp-groups/" + result.getId()))
      .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
      .body(result);
  }

  /**
   * {@code PUT  /cp-groups/:id} : Updates an existing cpGroup.
   *
   * @param id the id of the cpGroupDTO to save.
   * @param cpGroupDTO the cpGroupDTO to update.
   * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated cpGroupDTO,
   * or with status {@code 400 (Bad Request)} if the cpGroupDTO is not valid,
   * or with status {@code 500 (Internal Server Error)} if the cpGroupDTO couldn't be updated.
   * @throws URISyntaxException if the Location URI syntax is incorrect.
   */
  @PutMapping("/cp-groups/{id}")
  public ResponseEntity<CpGroupDTO> updateCpGroup(
    @PathVariable(value = "id", required = false) final Long id,
    @RequestBody CpGroupDTO cpGroupDTO
  ) throws URISyntaxException {
    log.debug("REST request to update CpGroup : {}, {}", id, cpGroupDTO);
    if (cpGroupDTO.getId() == null) {
      throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
    }
    if (!Objects.equals(id, cpGroupDTO.getId())) {
      throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
    }

    if (!cpGroupRepository.existsById(id)) {
      throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
    }

    CpGroupDTO result = cpGroupService.save(cpGroupDTO);
    return ResponseEntity
      .ok()
      .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, cpGroupDTO.getId().toString()))
      .body(result);
  }

  /**
   * {@code PATCH  /cp-groups/:id} : Partial updates given fields of an existing cpGroup, field will ignore if it is null
   *
   * @param id the id of the cpGroupDTO to save.
   * @param cpGroupDTO the cpGroupDTO to update.
   * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated cpGroupDTO,
   * or with status {@code 400 (Bad Request)} if the cpGroupDTO is not valid,
   * or with status {@code 404 (Not Found)} if the cpGroupDTO is not found,
   * or with status {@code 500 (Internal Server Error)} if the cpGroupDTO couldn't be updated.
   * @throws URISyntaxException if the Location URI syntax is incorrect.
   */
  @PatchMapping(value = "/cp-groups/{id}", consumes = "application/merge-patch+json")
  public ResponseEntity<CpGroupDTO> partialUpdateCpGroup(
    @PathVariable(value = "id", required = false) final Long id,
    @RequestBody CpGroupDTO cpGroupDTO
  ) throws URISyntaxException {
    log.debug("REST request to partial update CpGroup partially : {}, {}", id, cpGroupDTO);
    if (cpGroupDTO.getId() == null) {
      throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
    }
    if (!Objects.equals(id, cpGroupDTO.getId())) {
      throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
    }

    if (!cpGroupRepository.existsById(id)) {
      throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
    }

    Optional<CpGroupDTO> result = cpGroupService.partialUpdate(cpGroupDTO);

    return ResponseUtil.wrapOrNotFound(
      result,
      HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, cpGroupDTO.getId().toString())
    );
  }

  /**
   * {@code GET  /cp-groups} : get all the cpGroups.
   *
   * @param pageable the pagination information.
   * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of cpGroups in body.
   */
  @GetMapping("/cp-groups")
  public ResponseEntity<List<CpGroupDTO>> getAllCpGroups(Pageable pageable) {
    log.debug("REST request to get a page of CpGroups");
    Page<CpGroupDTO> page = cpGroupService.findAll(pageable);
    HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
    return ResponseEntity.ok().headers(headers).body(page.getContent());
  }

  /**
   * {@code GET  /cp-groups/:id} : get the "id" cpGroup.
   *
   * @param id the id of the cpGroupDTO to retrieve.
   * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the cpGroupDTO, or with status {@code 404 (Not Found)}.
   */
  @GetMapping("/cp-groups/{id}")
  public ResponseEntity<CpGroupDTO> getCpGroup(@PathVariable Long id) {
    log.debug("REST request to get CpGroup : {}", id);
    Optional<CpGroupDTO> cpGroupDTO = cpGroupService.findOne(id);
    return ResponseUtil.wrapOrNotFound(cpGroupDTO);
  }

  /**
   * {@code DELETE  /cp-groups/:id} : delete the "id" cpGroup.
   *
   * @param id the id of the cpGroupDTO to delete.
   * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
   */
  @DeleteMapping("/cp-groups/{id}")
  public ResponseEntity<Void> deleteCpGroup(@PathVariable Long id) {
    log.debug("REST request to delete CpGroup : {}", id);
    cpGroupService.delete(id);
    return ResponseEntity
      .noContent()
      .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
      .build();
  }
}
