package com.fis.crm.repository;

import com.fis.crm.domain.CpAlias;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the CpAlias entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CpAliasRepository extends JpaRepository<CpAlias, Long> {}
