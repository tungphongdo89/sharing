package com.fis.crm.service.mapper;

import com.fis.crm.domain.*;
import com.fis.crm.service.dto.AdPackageDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link AdPackage} and its DTO {@link AdPackageDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface AdPackageMapper extends EntityMapper<AdPackageDTO, AdPackage> {}
