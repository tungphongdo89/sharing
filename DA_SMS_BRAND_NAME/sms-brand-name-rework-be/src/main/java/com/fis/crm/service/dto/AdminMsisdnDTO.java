package com.fis.crm.service.dto;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

/**
 * A DTO for the {@link com.fis.crm.domain.AdminMsisdn} entity.
 */
public class AdminMsisdnDTO implements Serializable {

  private Long id;

  private String userName;

  private String msisdn;

  private Instant insertTime;

  private Integer status;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getUserName() {
    return userName;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }

  public String getMsisdn() {
    return msisdn;
  }

  public void setMsisdn(String msisdn) {
    this.msisdn = msisdn;
  }

  public Instant getInsertTime() {
    return insertTime;
  }

  public void setInsertTime(Instant insertTime) {
    this.insertTime = insertTime;
  }

  public Integer getStatus() {
    return status;
  }

  public void setStatus(Integer status) {
    this.status = status;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof AdminMsisdnDTO)) {
      return false;
    }

    AdminMsisdnDTO adminMsisdnDTO = (AdminMsisdnDTO) o;
    if (this.id == null) {
      return false;
    }
    return Objects.equals(this.id, adminMsisdnDTO.id);
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.id);
  }

  // prettier-ignore
    @Override
    public String toString() {
        return "AdminMsisdnDTO{" +
            "id=" + getId() +
            ", userName='" + getUserName() + "'" +
            ", msisdn='" + getMsisdn() + "'" +
            ", insertTime='" + getInsertTime() + "'" +
            ", status=" + getStatus() +
            "}";
    }
}
