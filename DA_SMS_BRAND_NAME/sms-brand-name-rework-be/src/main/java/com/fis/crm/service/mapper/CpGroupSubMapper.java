package com.fis.crm.service.mapper;

import com.fis.crm.domain.*;
import com.fis.crm.service.dto.CpGroupSubDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link CpGroupSub} and its DTO {@link CpGroupSubDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface CpGroupSubMapper extends EntityMapper<CpGroupSubDTO, CpGroupSub> {}
