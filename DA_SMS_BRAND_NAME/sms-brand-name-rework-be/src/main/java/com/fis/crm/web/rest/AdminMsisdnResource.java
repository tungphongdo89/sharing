package com.fis.crm.web.rest;

import com.fis.crm.repository.AdminMsisdnRepository;
import com.fis.crm.service.dto.AdminMsisdnDTO;
import com.fis.crm.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.fis.crm.domain.AdminMsisdn}.
 */
@RestController
@RequestMapping("/api")
public class AdminMsisdnResource {

  private final Logger log = LoggerFactory.getLogger(AdminMsisdnResource.class);

  private static final String ENTITY_NAME = "adminMsisdn";

  @Value("${jhipster.clientApp.name}")
  private String applicationName;

  private final AdminMsisdnService adminMsisdnService;

  private final AdminMsisdnRepository adminMsisdnRepository;

  public AdminMsisdnResource(AdminMsisdnService adminMsisdnService, AdminMsisdnRepository adminMsisdnRepository) {
    this.adminMsisdnService = adminMsisdnService;
    this.adminMsisdnRepository = adminMsisdnRepository;
  }

  /**
   * {@code POST  /admin-msisdns} : Create a new adminMsisdn.
   *
   * @param adminMsisdnDTO the adminMsisdnDTO to create.
   * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new adminMsisdnDTO, or with status {@code 400 (Bad Request)} if the adminMsisdn has already an ID.
   * @throws URISyntaxException if the Location URI syntax is incorrect.
   */
  @PostMapping("/admin-msisdns")
  public ResponseEntity<AdminMsisdnDTO> createAdminMsisdn(@RequestBody AdminMsisdnDTO adminMsisdnDTO) throws URISyntaxException {
    log.debug("REST request to save AdminMsisdn : {}", adminMsisdnDTO);
    if (adminMsisdnDTO.getId() != null) {
      throw new BadRequestAlertException("A new adminMsisdn cannot already have an ID", ENTITY_NAME, "idexists");
    }
    AdminMsisdnDTO result = adminMsisdnService.save(adminMsisdnDTO);
    return ResponseEntity
      .created(new URI("/api/admin-msisdns/" + result.getId()))
      .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
      .body(result);
  }

  /**
   * {@code PUT  /admin-msisdns/:id} : Updates an existing adminMsisdn.
   *
   * @param id the id of the adminMsisdnDTO to save.
   * @param adminMsisdnDTO the adminMsisdnDTO to update.
   * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated adminMsisdnDTO,
   * or with status {@code 400 (Bad Request)} if the adminMsisdnDTO is not valid,
   * or with status {@code 500 (Internal Server Error)} if the adminMsisdnDTO couldn't be updated.
   * @throws URISyntaxException if the Location URI syntax is incorrect.
   */
  @PutMapping("/admin-msisdns/{id}")
  public ResponseEntity<AdminMsisdnDTO> updateAdminMsisdn(
    @PathVariable(value = "id", required = false) final Long id,
    @RequestBody AdminMsisdnDTO adminMsisdnDTO
  ) throws URISyntaxException {
    log.debug("REST request to update AdminMsisdn : {}, {}", id, adminMsisdnDTO);
    if (adminMsisdnDTO.getId() == null) {
      throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
    }
    if (!Objects.equals(id, adminMsisdnDTO.getId())) {
      throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
    }

    if (!adminMsisdnRepository.existsById(id)) {
      throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
    }

    AdminMsisdnDTO result = adminMsisdnService.save(adminMsisdnDTO);
    return ResponseEntity
      .ok()
      .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, adminMsisdnDTO.getId().toString()))
      .body(result);
  }

  /**
   * {@code PATCH  /admin-msisdns/:id} : Partial updates given fields of an existing adminMsisdn, field will ignore if it is null
   *
   * @param id the id of the adminMsisdnDTO to save.
   * @param adminMsisdnDTO the adminMsisdnDTO to update.
   * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated adminMsisdnDTO,
   * or with status {@code 400 (Bad Request)} if the adminMsisdnDTO is not valid,
   * or with status {@code 404 (Not Found)} if the adminMsisdnDTO is not found,
   * or with status {@code 500 (Internal Server Error)} if the adminMsisdnDTO couldn't be updated.
   * @throws URISyntaxException if the Location URI syntax is incorrect.
   */
  @PatchMapping(value = "/admin-msisdns/{id}", consumes = "application/merge-patch+json")
  public ResponseEntity<AdminMsisdnDTO> partialUpdateAdminMsisdn(
    @PathVariable(value = "id", required = false) final Long id,
    @RequestBody AdminMsisdnDTO adminMsisdnDTO
  ) throws URISyntaxException {
    log.debug("REST request to partial update AdminMsisdn partially : {}, {}", id, adminMsisdnDTO);
    if (adminMsisdnDTO.getId() == null) {
      throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
    }
    if (!Objects.equals(id, adminMsisdnDTO.getId())) {
      throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
    }

    if (!adminMsisdnRepository.existsById(id)) {
      throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
    }

    Optional<AdminMsisdnDTO> result = adminMsisdnService.partialUpdate(adminMsisdnDTO);

    return ResponseUtil.wrapOrNotFound(
      result,
      HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, adminMsisdnDTO.getId().toString())
    );
  }

  /**
   * {@code GET  /admin-msisdns} : get all the adminMsisdns.
   *
   * @param pageable the pagination information.
   * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of adminMsisdns in body.
   */
  @GetMapping("/admin-msisdns")
  public ResponseEntity<List<AdminMsisdnDTO>> getAllAdminMsisdns(Pageable pageable) {
    log.debug("REST request to get a page of AdminMsisdns");
    Page<AdminMsisdnDTO> page = adminMsisdnService.findAll(pageable);
    HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
    return ResponseEntity.ok().headers(headers).body(page.getContent());
  }

  /**
   * {@code GET  /admin-msisdns/:id} : get the "id" adminMsisdn.
   *
   * @param id the id of the adminMsisdnDTO to retrieve.
   * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the adminMsisdnDTO, or with status {@code 404 (Not Found)}.
   */
  @GetMapping("/admin-msisdns/{id}")
  public ResponseEntity<AdminMsisdnDTO> getAdminMsisdn(@PathVariable Long id) {
    log.debug("REST request to get AdminMsisdn : {}", id);
    Optional<AdminMsisdnDTO> adminMsisdnDTO = adminMsisdnService.findOne(id);
    return ResponseUtil.wrapOrNotFound(adminMsisdnDTO);
  }

  /**
   * {@code DELETE  /admin-msisdns/:id} : delete the "id" adminMsisdn.
   *
   * @param id the id of the adminMsisdnDTO to delete.
   * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
   */
  @DeleteMapping("/admin-msisdns/{id}")
  public ResponseEntity<Void> deleteAdminMsisdn(@PathVariable Long id) {
    log.debug("REST request to delete AdminMsisdn : {}", id);
    adminMsisdnService.delete(id);
    return ResponseEntity
      .noContent()
      .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
      .build();
  }
}
