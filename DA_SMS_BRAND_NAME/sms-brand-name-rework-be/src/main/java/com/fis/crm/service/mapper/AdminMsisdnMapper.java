package com.fis.crm.service.mapper;

import com.fis.crm.domain.*;
import com.fis.crm.service.dto.AdminMsisdnDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link AdminMsisdn} and its DTO {@link AdminMsisdnDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface AdminMsisdnMapper extends EntityMapper<AdminMsisdnDTO, AdminMsisdn> {}
