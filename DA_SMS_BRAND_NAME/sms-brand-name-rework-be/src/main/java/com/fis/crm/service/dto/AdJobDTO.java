package com.fis.crm.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link com.fis.crm.domain.AdJob} entity.
 */
public class AdJobDTO implements Serializable {

  private Long id;

  private String jobName;

  private Integer status;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getJobName() {
    return jobName;
  }

  public void setJobName(String jobName) {
    this.jobName = jobName;
  }

  public Integer getStatus() {
    return status;
  }

  public void setStatus(Integer status) {
    this.status = status;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof AdJobDTO)) {
      return false;
    }

    AdJobDTO adJobDTO = (AdJobDTO) o;
    if (this.id == null) {
      return false;
    }
    return Objects.equals(this.id, adJobDTO.id);
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.id);
  }

  // prettier-ignore
    @Override
    public String toString() {
        return "AdJobDTO{" +
            "id=" + getId() +
            ", jobName='" + getJobName() + "'" +
            ", status=" + getStatus() +
            "}";
    }
}
