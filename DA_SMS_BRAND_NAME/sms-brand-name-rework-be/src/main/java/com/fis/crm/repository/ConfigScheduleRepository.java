package com.fis.crm.repository;

import com.fis.crm.domain.ConfigSchedule;

import com.fis.crm.domain.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the ConfigSchedule entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ConfigScheduleRepository extends JpaRepository<ConfigSchedule, Long> {
    @Query(value = "select a.id from config_schedule a where a.request_type = ?1 and a.bussiness_type = ?2", nativeQuery = true)
    List<ConfigSchedule> findOneByRequestTypeAndBussinessType(Integer requestType, Integer bussinessType);

    @Query(value = "SELECT\n" +
        "t1.BUSSINESSTYPE, t1.REQUESTTYPE, t1.PROCESSTIME, t1.CONFIRMTIME\n" +
        "FROM\n" +
        "(\n" +
        "SELECT\n" +
        "*\n" +
        "FROM\n" +
        "(\n" +
        "SELECT\n" +
        "cs.BUSSINESS_TYPE bussinessType,\n" +
        "cs.REQUEST_TYPE requestType,\n" +
        "(CASE\n" +
        "WHEN cs.REQUEST_TYPE = 1 THEN 'Thắc mắc'\n" +
        "WHEN cs.REQUEST_TYPE = 2 THEN 'Yêu cầu'\n" +
        "WHEN cs.REQUEST_TYPE = 3 THEN 'Khiếu nại'\n" +
        "END) AS requestTypeName,\n" +
        "(CASE\n" +
        "WHEN cs.BUSSINESS_TYPE = 1 THEN 'Xử lý'\n" +
        "WHEN cs.BUSSINESS_TYPE = 2 THEN 'Gia hạn'\n" +
        "WHEN cs.BUSSINESS_TYPE = 3 THEN 'Phân quyền'\n" +
        "END) AS bussinessTypeName,\n" +
        "cs.PROCESS_TIME processTime,\n" +
        "cs.CONFIRM_TIME confirmTime\n" +
        "FROM\n" +
        "CONFIG_SCHEDULE cs)) t1\n" +
        "WHERE\n" +
        "UPPER(t1.requestTypeName) LIKE CONCAT('%',:keySearch,'%') OR UPPER(bussinessTypeName) LIKE UPPER(:keySearch%)" , nativeQuery = true)
    Page<ConfigSchedule> findConfigSchedule(@Param("keySearch") String keySearch, Pageable pageable);
}
