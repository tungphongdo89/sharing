package com.fis.crm.service.dto;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

/**
 * A DTO for the {@link com.fis.crm.domain.Ticket} entity.
 */
public class TicketDTO implements Serializable {

  private Long id;

  private Long ticketId;

  private String channelType;

  private String ticketCode;

  private Long customerId;

  private String status;

  private String fcr;

  private Long departmentId;

  private Long createUser;

  private Instant createDatetime;

  private Instant updateDatetime;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Long getTicketId() {
    return ticketId;
  }

  public void setTicketId(Long ticketId) {
    this.ticketId = ticketId;
  }

  public String getChannelType() {
    return channelType;
  }

  public void setChannelType(String channelType) {
    this.channelType = channelType;
  }

  public String getTicketCode() {
    return ticketCode;
  }

  public void setTicketCode(String ticketCode) {
    this.ticketCode = ticketCode;
  }

  public Long getCustomerId() {
    return customerId;
  }

  public void setCustomerId(Long customerId) {
    this.customerId = customerId;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public String getFcr() {
    return fcr;
  }

  public void setFcr(String fcr) {
    this.fcr = fcr;
  }

  public Long getDepartmentId() {
    return departmentId;
  }

  public void setDepartmentId(Long departmentId) {
    this.departmentId = departmentId;
  }

  public Long getCreateUser() {
    return createUser;
  }

  public void setCreateUser(Long createUser) {
    this.createUser = createUser;
  }

  public Instant getCreateDatetime() {
    return createDatetime;
  }

  public void setCreateDatetime(Instant createDatetime) {
    this.createDatetime = createDatetime;
  }

  public Instant getUpdateDatetime() {
    return updateDatetime;
  }

  public void setUpdateDatetime(Instant updateDatetime) {
    this.updateDatetime = updateDatetime;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof TicketDTO)) {
      return false;
    }

    TicketDTO ticketDTO = (TicketDTO) o;
    if (this.id == null) {
      return false;
    }
    return Objects.equals(this.id, ticketDTO.id);
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.id);
  }

  // prettier-ignore
    @Override
    public String toString() {
        return "TicketDTO{" +
            "id=" + getId() +
            ", ticketId=" + getTicketId() +
            ", channelType='" + getChannelType() + "'" +
            ", ticketCode='" + getTicketCode() + "'" +
            ", customerId=" + getCustomerId() +
            ", status='" + getStatus() + "'" +
            ", fcr='" + getFcr() + "'" +
            ", departmentId=" + getDepartmentId() +
            ", createUser=" + getCreateUser() +
            ", createDatetime='" + getCreateDatetime() + "'" +
            ", updateDatetime='" + getUpdateDatetime() + "'" +
            "}";
    }
}
