package com.fis.crm.service.mapper;

import com.fis.crm.domain.*;
import com.fis.crm.service.dto.TransLogDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link TransLog} and its DTO {@link TransLogDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface TransLogMapper extends EntityMapper<TransLogDTO, TransLog> {}
