package com.fis.crm.domain;

import java.io.Serializable;
import java.time.Instant;
import javax.persistence.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A ProvinceBccs.
 */
@Entity
@Table(name = "province_bccs")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class ProvinceBccs implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
  @SequenceGenerator(name = "sequenceGenerator")
  private Long id;

  @Column(name = "province_bccs_id")
  private Long provinceBccsId;

  @Column(name = "province_bccs_name")
  private String provinceBccsName;

  @Column(name = "province_id")
  private Long provinceId;

  @Column(name = "bccs_staff_code")
  private String bccsStaffCode;

  @Column(name = "user_updated")
  private String userUpdated;

  @Column(name = "date_updated")
  private Instant dateUpdated;

  // jhipster-needle-entity-add-field - JHipster will add fields here
  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public ProvinceBccs id(Long id) {
    this.id = id;
    return this;
  }

  public Long getProvinceBccsId() {
    return this.provinceBccsId;
  }

  public ProvinceBccs provinceBccsId(Long provinceBccsId) {
    this.provinceBccsId = provinceBccsId;
    return this;
  }

  public void setProvinceBccsId(Long provinceBccsId) {
    this.provinceBccsId = provinceBccsId;
  }

  public String getProvinceBccsName() {
    return this.provinceBccsName;
  }

  public ProvinceBccs provinceBccsName(String provinceBccsName) {
    this.provinceBccsName = provinceBccsName;
    return this;
  }

  public void setProvinceBccsName(String provinceBccsName) {
    this.provinceBccsName = provinceBccsName;
  }

  public Long getProvinceId() {
    return this.provinceId;
  }

  public ProvinceBccs provinceId(Long provinceId) {
    this.provinceId = provinceId;
    return this;
  }

  public void setProvinceId(Long provinceId) {
    this.provinceId = provinceId;
  }

  public String getBccsStaffCode() {
    return this.bccsStaffCode;
  }

  public ProvinceBccs bccsStaffCode(String bccsStaffCode) {
    this.bccsStaffCode = bccsStaffCode;
    return this;
  }

  public void setBccsStaffCode(String bccsStaffCode) {
    this.bccsStaffCode = bccsStaffCode;
  }

  public String getUserUpdated() {
    return this.userUpdated;
  }

  public ProvinceBccs userUpdated(String userUpdated) {
    this.userUpdated = userUpdated;
    return this;
  }

  public void setUserUpdated(String userUpdated) {
    this.userUpdated = userUpdated;
  }

  public Instant getDateUpdated() {
    return this.dateUpdated;
  }

  public ProvinceBccs dateUpdated(Instant dateUpdated) {
    this.dateUpdated = dateUpdated;
    return this;
  }

  public void setDateUpdated(Instant dateUpdated) {
    this.dateUpdated = dateUpdated;
  }

  // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof ProvinceBccs)) {
      return false;
    }
    return id != null && id.equals(((ProvinceBccs) o).id);
  }

  @Override
  public int hashCode() {
    // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
    return getClass().hashCode();
  }

  // prettier-ignore
    @Override
    public String toString() {
        return "ProvinceBccs{" +
            "id=" + getId() +
            ", provinceBccsId=" + getProvinceBccsId() +
            ", provinceBccsName='" + getProvinceBccsName() + "'" +
            ", provinceId=" + getProvinceId() +
            ", bccsStaffCode='" + getBccsStaffCode() + "'" +
            ", userUpdated='" + getUserUpdated() + "'" +
            ", dateUpdated='" + getDateUpdated() + "'" +
            "}";
    }
}
