package com.fis.crm.service.mapper;

import com.fis.crm.domain.*;
import com.fis.crm.service.dto.ProvinceBccsDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link ProvinceBccs} and its DTO {@link ProvinceBccsDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ProvinceBccsMapper extends EntityMapper<ProvinceBccsDTO, ProvinceBccs> {}
