package com.fis.crm.repository;

import com.fis.crm.domain.Telco;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the Telco entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TelcoRepository extends JpaRepository<Telco, Long> {}
