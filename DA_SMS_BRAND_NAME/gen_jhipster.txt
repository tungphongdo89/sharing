entity Ticket {
	ticketId Long,
    channelType String,
    ticketCode String,
    customerId Long,
    status String,
    fcr String,
    departmentId Long,
    createUser Long,
    createDatetime Instant,
    updateDatetime Instant
}
dto Ticket with mapstruct
paginate Ticket with pagination
service Ticket with serviceClass
service Ticket with serviceImpl


{
  "name": "Ticket",
  "fields": [
    {
      "fieldName": "ticketId",
      "fieldType": "Long"
    },
    {
      "fieldName": "channelType",
      "fieldType": "String"
    },
    {
      "fieldName": "ticketCode",
      "fieldType": "String"
    },
    {
      "fieldName": "customerId",
      "fieldType": "Long"
    },
    {
      "fieldName": "status",
      "fieldType": "String"
    },
    {
      "fieldName": "fcr",
      "fieldType": "String"
    },
    {
      "fieldName": "departmentId",
      "fieldType": "Long"
    },
    {
      "fieldName": "createUser",
      "fieldType": "Long"
    },
    {
      "fieldName": "createDatetime",
      "fieldType": "Instant"
    },
    {
      "fieldName": "updateDatetime",
      "fieldType": "Instant"
    }
  ],
  "relationships": [],
  "changelogDate": "20210105072451",
  "entityTableName": "ticket",
  "dto": "mapstruct",
  "pagination": "pagination",
  "service": "serviceImpl",
  "jpaMetamodelFiltering": false,
  "fluentMethods": true,
  "readOnly": false,
  "embedded": false,
  "clientRootFolder": "",
  "applications": "*"
}






entity TicketRequest {
	ticketRequestId Long,
    ticketId String,
    ticketRequestCode String,
    requestType Long,
    bussinessType Long,
    departmentId Long,
    status String,
    deadline Instant,
    content String,
    confirmDate Instant,
    timeNotify Long,
    createUser Long,
    createDatetime Instant
}
dto TicketRequest with mapstruct
paginate TicketRequest with pagination
service TicketRequest with serviceClass
service TicketRequest with serviceImpl



{
  "name": "TicketRequest",
  "fields": [
    {
      "fieldName": "ticketRequestId",
      "fieldType": "Long"
    },
    {
      "fieldName": "ticketId",
      "fieldType": "String"
    },
    {
      "fieldName": "ticketRequestCode",
      "fieldType": "String"
    },
    {
      "fieldName": "requestType",
      "fieldType": "Long"
    },
    {
      "fieldName": "bussinessType",
      "fieldType": "Long"
    },
    {
      "fieldName": "departmentId",
      "fieldType": "Long"
    },
    {
      "fieldName": "status",
      "fieldType": "String"
    },
    {
      "fieldName": "deadline",
      "fieldType": "Instant"
    },
    {
      "fieldName": "content",
      "fieldType": "String"
    },
    {
      "fieldName": "confirmDate",
      "fieldType": "Instant"
    },
    {
      "fieldName": "timeNotify",
      "fieldType": "Long"
    },
    {
      "fieldName": "createUser",
      "fieldType": "Long"
    },
    {
      "fieldName": "createDatetime",
      "fieldType": "Instant"
    }
  ],
  "relationships": [],
  "changelogDate": "20210105072451",
  "entityTableName": "ticket_request",
  "dto": "mapstruct",
  "pagination": "pagination",
  "service": "serviceImpl",
  "jpaMetamodelFiltering": false,
  "fluentMethods": true,
  "readOnly": false,
  "embedded": false,
  "clientRootFolder": "",
  "applications": "*"
}



entity TicketRequestAttachment {
	ticketRequestAttachmentId Long,
    ticketRequestId Long,
    fileName String,
    fillNameEncrypt String,
    createDatetime Instant,
    createUser Long,
    status String
}
dto TicketRequestAttachment with mapstruct
paginate TicketRequestAttachment with pagination
service TicketRequestAttachment with serviceClass
service TicketRequestAttachment with serviceImpl


{
  "name": "TicketRequestAttachment",
  "fields": [
    {
      "fieldName": "ticketRequestAttachmentId",
      "fieldType": "Long"
    },
    {
      "fieldName": "ticketRequestId",
      "fieldType": "Long"
    },
    {
      "fieldName": "fileName",
      "fieldType": "String"
    },
    {
      "fieldName": "fillNameEncrypt",
      "fieldType": "String"
    },
    {
      "fieldName": "createDatetime",
      "fieldType": "Instant"
    },
    {
      "fieldName": "createUser",
      "fieldType": "Long"
    },
    {
      "fieldName": "status",
      "fieldType": "String"
    }
  ],
  "relationships": [],
  "changelogDate": "20210105072451",
  "entityTableName": "ticket_request_attachment",
  "dto": "mapstruct",
  "pagination": "pagination",
  "service": "serviceImpl",
  "jpaMetamodelFiltering": false,
  "fluentMethods": true,
  "readOnly": false,
  "embedded": false,
  "clientRootFolder": "",
  "applications": "*"
}
