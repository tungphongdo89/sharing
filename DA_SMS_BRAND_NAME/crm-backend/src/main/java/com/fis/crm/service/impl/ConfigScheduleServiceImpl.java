package com.fis.crm.service.impl;

import com.fis.crm.commons.DataUtil;
import com.fis.crm.config.Constants;
import com.fis.crm.domain.User;
import com.fis.crm.service.ConfigScheduleService;
import com.fis.crm.domain.ConfigSchedule;
import com.fis.crm.repository.ConfigScheduleRepository;
import com.fis.crm.service.UserService;
import com.fis.crm.service.dto.ConfigScheduleDTO;
import com.fis.crm.service.mapper.ConfigScheduleMapper;
import com.fis.crm.web.rest.errors.BadRequestAlertException;
import liquibase.util.StringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link ConfigSchedule}.
 */
@Service
@Transactional
public class ConfigScheduleServiceImpl implements ConfigScheduleService {

    private final Logger log = LoggerFactory.getLogger(ConfigScheduleServiceImpl.class);

    private final ConfigScheduleRepository configScheduleRepository;
    private final ConfigScheduleMapper configScheduleMapper;
    @Autowired
    private ActionLogServiceImpl actionLogServiceImpl;
    @Autowired
    private UserService userService;

    public ConfigScheduleServiceImpl(ConfigScheduleRepository configScheduleRepository, ConfigScheduleMapper configScheduleMapper) {
        this.configScheduleRepository = configScheduleRepository;
        this.configScheduleMapper = configScheduleMapper;
    }

    @Override
    public ConfigScheduleDTO save(ConfigScheduleDTO configScheduleDTO) {
        log.debug("Request to save ConfigSchedule : {}", configScheduleDTO);
        Optional<User> user = userService.getUserWithAuthorities();
        Instant now = Instant.now();
        if(configScheduleDTO.getId()==null)  //add new
        {
            configScheduleDTO.setCreateUser(user.get().getId());
            configScheduleDTO.setCreateDate(now);
            configScheduleDTO.setStatus("1");
        }
        else {  //update
            configScheduleDTO.setUpdateUser(user.get().getId());
            configScheduleDTO.setUpdateDate(now);
        }
        ConfigSchedule configSchedule = configScheduleMapper.toEntity(configScheduleDTO);
        //validate new
        List<ConfigSchedule> cfg = configScheduleRepository.findAll();
        ConfigScheduleDTO result = new ConfigScheduleDTO();
        if(configScheduleDTO.getId()==null){
            List<ConfigSchedule> objExisting = configScheduleRepository.findOneByRequestTypeAndBussinessType(configScheduleDTO.getRequestType(),configScheduleDTO.getBussinessType());
            if(!objExisting.isEmpty()) {
                result.setErrorCodeConfig("objectExist");
                return result;
            }
        }
        else if(configScheduleDTO.getId()!=null) {
            Optional<ConfigScheduleDTO> oldObj = configScheduleRepository.findById(configScheduleDTO.getId()).map(configScheduleMapper::toDto);
            if(!oldObj.isPresent()) {
                //throw new BadRequestAlertException("A configSchedule with request_type and bussiness_type is not existed yet", "CONFIG_SCHEDULE", "objNotExists");
                result.setErrorCodeConfig("objectNotExist");
                return result;
            }
            List<ConfigSchedule> objExisting = configScheduleRepository.findOneByRequestTypeAndBussinessType(configScheduleDTO.getRequestType(),configScheduleDTO.getBussinessType());
            if(!objExisting.isEmpty() && objExisting.get(0).getId()!= configScheduleDTO.getId()) {
                //throw new BadRequestAlertException("A configSchedule with request_type and bussiness_type already exist", "CONFIG_SCHEDULE", "objExists");
                result.setErrorCodeConfig("objectExist");
                return result;
            }
        }

        configSchedule = configScheduleRepository.save(configSchedule);
        if(configScheduleDTO.getId()==null){
            actionLogServiceImpl.saveActionLog(user.get().getId(), Constants.ACTION_LOG_TYPE.INSERT+"",configSchedule.getId(),"Config_schedule","", now);
        }
        else
            actionLogServiceImpl.saveActionLog(user.get().getId(), Constants.ACTION_LOG_TYPE.UPDATE+"",configSchedule.getId(),"Config_schedule","", now);
        return configScheduleMapper.toDto(configSchedule);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<ConfigScheduleDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ConfigSchedules");
        return configScheduleRepository.findAll(pageable)
            .map(configScheduleMapper::toDto);
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<ConfigScheduleDTO> findOne(Long id) {
        log.debug("Request to get ConfigSchedule : {}", id);
        return configScheduleRepository.findById(id)
            .map(configScheduleMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete ConfigSchedule : {}", id);
        configScheduleRepository.deleteById(id);
    }

    @Override
    public List<ConfigScheduleDTO> find(ConfigScheduleDTO obj) {
        List<Object[]> lst = new ArrayList<Object[]>();
        List<ConfigScheduleDTO> lstResult = new ArrayList<ConfigScheduleDTO>();
        try {
            StringBuilder sb = new StringBuilder(" " +
                " SELECT * FROM (SELECT cs.ID id,cs.BUSSINESS_TYPE bussinessType, cs.REQUEST_TYPE requestType, " +
                " cs.PROCESS_TIME processTime,cs.PROCESS_TIME_TYPE processTimeType, "+
                " cs.CONFIRM_TIME confirmTime,cs.CONFIRM_TIME_TYPE confirmTimeType, "+
                " (CASE WHEN cs.REQUEST_TYPE=1 THEN 'Thắc mắc' " +
                " WHEN cs.REQUEST_TYPE=2 THEN 'Yêu cầu' " +
                " WHEN cs.REQUEST_TYPE=3 THEN 'Khiếu nại' " +
                " END) AS requestTypeName, " +
                "(CASE WHEN cs.BUSSINESS_TYPE=1 THEN 'Xử lý' " +
                " WHEN cs.BUSSINESS_TYPE=2 THEN 'Gia hạn' " +
                " WHEN cs.BUSSINESS_TYPE=3 THEN 'Phân quyền' " +
                " END) AS bussinessTypeName from CONFIG_SCHEDULE cs) " +
                " WHERE 1=1 " );
                if(StringUtil.isNotEmpty(obj.getKeySearch())){
                sb.append("AND (processTime like (:keySearch) OR UPPER(requestTypeName) LIKE UPPER(:keySearch) " +
                " OR UPPER(bussinessTypeName) LIKE UPPER(:keySearch) OR confirmTime LIKE (:keySearch) ) ");
              }
            sb.append(" AND ( ROWNUM > :limit and ROWNUM <= :offset ) ");
            Query query =  em.createNativeQuery(sb.toString());
            query.setParameter("keySearch", "%"+obj.getKeySearch().trim()+"%");
            int limit = obj.getSize();
            int page = obj.getPage();
            query.setParameter("limit", (page*limit)-limit  );
            query.setParameter("offset", page*limit );


            if(StringUtil.isNotEmpty(obj.getKeySearch())) {
                query.setParameter("keySearch", "%" + obj.getKeySearch() + "%");
            }
            lst = query.getResultList();
            for (Object[] obj1 : lst){
                ConfigScheduleDTO cal = new ConfigScheduleDTO();
                cal.setId(DataUtil.safeToLong(obj1[0]));
                cal.setBussinessType(DataUtil.safeToInt(obj1[1]));
                cal.setRequestType(DataUtil.safeToInt(obj1[2]));
                cal.setProcessTime((float)DataUtil.safeToInt(obj1[3]));
                cal.setProcessTimeType(DataUtil.safeToString(obj1[4]));
                cal.setConfirmTime((float) DataUtil.safeToInt(obj1[5]));
                cal.setConfirmTimeType(DataUtil.safeToString(obj1[6]));
                lstResult.add(cal);
            }
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return lstResult;
    }

    @PersistenceContext
    EntityManager em;

}
