package com.fis.crm.repository;

import com.fis.crm.domain.ProccessTicket;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the TicketRequestAttachment entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ProcessTicketRepository extends JpaRepository<ProccessTicket, Long> {}
