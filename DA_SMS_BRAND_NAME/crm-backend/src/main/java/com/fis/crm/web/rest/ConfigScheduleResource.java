package com.fis.crm.web.rest;

import com.fis.crm.commons.ExcelReportUtils;
import com.fis.crm.commons.ExportUtils;
import com.fis.crm.commons.ExcelUtils;
import com.fis.crm.commons.FileUtils;
import com.fis.crm.repository.ConfigScheduleRepository;
import com.fis.crm.service.ConfigScheduleService;
import com.fis.crm.service.dto.ExcelColumn;
import com.fis.crm.service.dto.ExcelTitle;
import com.fis.crm.service.response.ConfigScheduleError;
import com.fis.crm.web.rest.errors.BadRequestAlertException;
import com.fis.crm.service.dto.ConfigScheduleDTO;

import io.swagger.annotations.ResponseHeader;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;
import com.fis.crm.repository.ConfigScheduleRepository;

import java.io.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.fis.crm.domain.ConfigSchedule}.
 */
@RestController
@RequestMapping("/api")
public class ConfigScheduleResource {

    private final Logger log = LoggerFactory.getLogger(ConfigScheduleResource.class);

    private static final String ENTITY_NAME = "configSchedule";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ConfigScheduleService configScheduleService;

    public ConfigScheduleResource(ConfigScheduleService configScheduleService) {
        this.configScheduleService = configScheduleService;
    }

    /**
     * {@code POST  /config-schedules} : Create a new configSchedule.
     *
     * @param configScheduleDTO the configScheduleDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new configScheduleDTO, or with status {@code 400 (Bad Request)} if the configSchedule has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/config-schedules")
    public ResponseEntity<Object> createConfigSchedule(@RequestBody ConfigScheduleDTO configScheduleDTO) throws URISyntaxException {
        log.debug("REST request to save ConfigSchedule : {}", configScheduleDTO);
        try{
            if (configScheduleDTO.getId() != null) {
                throw new BadRequestAlertException("A new configSchedule cannot already have an ID", ENTITY_NAME, "idexists");
            }
            ConfigScheduleDTO result = configScheduleService.save(configScheduleDTO);
            if( null!=result.getErrorCodeConfig() && !("").equals(result.getErrorCodeConfig()) ){
                if( ("objectExist").equals(result.getErrorCodeConfig())  ) {
                    String message = "Cấu hình thêm đã tồn tại" ;
                    ConfigScheduleError error = new ConfigScheduleError("400",message);
                    return ResponseEntity.ok().body(error);
                }
            }

            ConfigScheduleError success = new ConfigScheduleError("200","OK");
            return ResponseEntity.created(new URI("/api/config-schedules/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
                .body(success);
        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
        }
    }
    @PostMapping("/config-schedules/search")
    public ResponseEntity<List<ConfigScheduleDTO>> searchConfigSchedule(@RequestBody ConfigScheduleDTO configScheduleDTO) throws URISyntaxException {
        log.debug("REST request to searchConfigSchedule : {}", configScheduleDTO);
        List<ConfigScheduleDTO> page = new ArrayList<>();
        //if(configScheduleDTO.getKeySearch()!=null && !("").equals(configScheduleDTO.getKeySearch())){
         page = configScheduleService.find(configScheduleDTO);

        if (configScheduleDTO.getKeySearch() != null && !("").equals(configScheduleDTO.getKeySearch())) {
            page = configScheduleService.find(configScheduleDTO);
        }
        return new ResponseEntity<>(page, null, HttpStatus.OK);
    }
    /**
     * {@code PUT  /config-schedules} : Updates an existing configSchedule.
     *
     * @param configScheduleDTO the configScheduleDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated configScheduleDTO,
     * or with status {@code 400 (Bad Request)} if the configScheduleDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the configScheduleDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/config-schedules")
    public ResponseEntity<Object> updateConfigSchedule(@RequestBody ConfigScheduleDTO configScheduleDTO) throws URISyntaxException {
        log.debug("REST request to update ConfigSchedule : {}", configScheduleDTO);
        try{
            if (configScheduleDTO.getId() == null) {
                throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
            }
            ConfigScheduleDTO result = configScheduleService.save(configScheduleDTO);
            if( null!=result.getErrorCodeConfig() && !("").equals(result.getErrorCodeConfig()) ){
                String message = ("objectExist").equals(result.getErrorCodeConfig())  ? "Cấu hình cập nhật đã tồn tại loại cấu hình/loại nghiệp vụ" : "Cấu hình cập nhật chưa tồn tại";
                ConfigScheduleError error = new ConfigScheduleError("400",message);
                return ResponseEntity.ok().body(error);
            }

            ConfigScheduleError success = new ConfigScheduleError("200","OK");
            return ResponseEntity.created(new URI("/api/config-schedules/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
                .body(success);
        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
        }



//        return ResponseEntity.ok()
//            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, configScheduleDTO.getId().toString()))
//            .body(result);
    }

    /**
     * {@code GET  /config-schedules} : get all the configSchedules.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of configSchedules in body.
     */
    @GetMapping("/config-schedules")
    public ResponseEntity<List<ConfigScheduleDTO>> getAllConfigSchedules(Pageable pageable) {
        log.debug("REST request to get a page of ConfigSchedules");
        Page<ConfigScheduleDTO> page = configScheduleService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /config-schedules/:id} : get the "id" configSchedule.
     *
     * @param id the id of the configScheduleDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the configScheduleDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/config-schedules/{id}")
    public ResponseEntity<ConfigScheduleDTO> getConfigSchedule(@PathVariable Long id) {
        log.debug("REST request to get ConfigSchedule : {}", id);
        Optional<ConfigScheduleDTO> configScheduleDTO = configScheduleService.findOne(id);
        return ResponseUtil.wrapOrNotFound(configScheduleDTO);
    }

    /**
     * {@code DELETE  /config-schedules/:id} : delete the "id" configSchedule.
     *
     * @param id the id of the configScheduleDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/config-schedules/{id}")
    public ResponseEntity<Void> deleteConfigSchedule(@PathVariable Long id) {
        log.debug("REST request to delete ConfigSchedule : {}", id);
        configScheduleService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
    @PostMapping("/config-schedules/getSampleFile")
    public ResponseEntity<InputStreamResource> getSampleFile() {
        log.debug("REST request to down file sample ConfigSchedule : {}");
        List<ExcelColumn> listColumn = new ArrayList<>();
        HttpHeaders responseHeader = new HttpHeaders();
        ByteArrayInputStream file = null;
//        listColumn.add( new ExcelColumn( "Loại yêu cầu","Loại yêu cầu"  ,ExcelColumn.ALIGN_MENT.CENTER));
//        listColumn.add( new ExcelColumn( "Loại nghiệp vụ","Loại nghiệp vụ"  ,ExcelColumn.ALIGN_MENT.CENTER));
//        listColumn.add( new ExcelColumn( "Thời gian xử lý","Thời gian xử lý"  ,ExcelColumn.ALIGN_MENT.CENTER));
//        listColumn.add( new ExcelColumn( "Thời gian xác nhận","Thời gian xác nhận"  ,ExcelColumn.ALIGN_MENT.CENTER));
        List<String> listHeader = new ArrayList<>();
        listHeader.add( "Loại yêu cầu");
        listHeader.add( "Loại nghiệp vụ");
        listHeader.add( "Thời gian xử lý");
        listHeader.add( "Thời gian xác nhận");
        listHeader.add( "Loại thời gian xử lý");
        listHeader.add( "Loại thời gian xác nhận");
        List<Integer> listAlign = new ArrayList<Integer>();
        listAlign.add(0); listAlign.add(0); listAlign.add(0);listAlign.add(0);
        listAlign.add(0);listAlign.add(0);
        List<Integer> listSize = new ArrayList<>();
        listSize.add(10);listSize.add(10);listSize.add(10);listSize.add(10);
        listSize.add(10);listSize.add(10);

        ExcelTitle ex = new ExcelTitle("Danh sách nhập cấu hình","","");
        try {
            //file = new ExportUtils().onExport(listColumn,null,0,0,ex,false);
            file = new ExcelUtils().export(new ArrayList<>(),listHeader,listHeader,"Danh sách nhập cấu hình",listSize ,listAlign);

            InputStream inputStream = new BufferedInputStream(file);
            responseHeader.setContentType(MediaType.APPLICATION_OCTET_STREAM);
            // Thiết lập thông tin trả về
            //responseHeader.set("Content-disposition", "attachment; filename=" + file.getName());
            //responseHeader.setContentLength(data.length);
            InputStreamResource inputStreamResource = new InputStreamResource(inputStream);
            return new ResponseEntity<InputStreamResource>(inputStreamResource, responseHeader  , HttpStatus.OK);

        }
        catch (Exception e){
            log.error("",e.getMessage());
            return new ResponseEntity<>(null, responseHeader, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/config-schedules/uploadFile")
    public ResponseEntity<Object>  getSampleFile(@RequestParam("file") MultipartFile file) {
        log.debug("REST request to down file sample ConfigSchedule : {}");
        HttpHeaders responseHeader = new HttpHeaders();
        String name = file.getOriginalFilename();
        int validateFile = FileUtils.validateAttachFile(file,name);
        try {
            if(validateFile!=0) {
                String message =  "File sai định dạng";
                ConfigScheduleError error = new ConfigScheduleError("400",message);
                return ResponseEntity.ok().body(error);
            }
            else{
                List<ConfigScheduleDTO> tempStudentList = new ArrayList<ConfigScheduleDTO>();
                XSSFWorkbook workbook = new XSSFWorkbook(file.getInputStream());
                XSSFSheet worksheet = workbook.getSheetAt(0);
                ConfigScheduleDTO temp = new ConfigScheduleDTO();
                Iterator<Row> rows =   worksheet.rowIterator();
                int i=0;
                while (rows.hasNext()){
                    Row row = rows.next();
                    if(i>1){
                        ConfigScheduleDTO config = new ConfigScheduleDTO();
                        config.setRequestType((int)row.getCell(1).getNumericCellValue());
                        config.setBussinessType((int)(row.getCell(2).getNumericCellValue()) );
                        config.setProcessTime((float)row.getCell(3).getNumericCellValue());
                        config.setConfirmTime((float)row.getCell(4).getNumericCellValue());
                        config.setProcessTimeType(String.valueOf((int)row.getCell(5).getNumericCellValue()));
                        config.setConfirmTimeType(String.valueOf((int)row.getCell(6).getNumericCellValue()));
                        ConfigScheduleDTO result = configScheduleService.save(config);
                        if( null!=result.getErrorCodeConfig() && !("").equals(result.getErrorCodeConfig()) ){
                            if( ("objectExist").equals(result.getErrorCodeConfig())  ) {
                                String message = "Cấu hình thêm đã tồn tại" ;
                                ConfigScheduleError error = new ConfigScheduleError("400",message);
                                return ResponseEntity.ok().body(error);
                            }
                        }
                        ConfigScheduleError success = new ConfigScheduleError("200","OK");
                        return ResponseEntity.created(new URI("/api/config-schedules/" + result.getId()))
                            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
                            .body(success);
                    }
                    i++;
                }
                return ResponseEntity.status(HttpStatus.MULTI_STATUS).body(null);
            }
        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
        }
    }
}
