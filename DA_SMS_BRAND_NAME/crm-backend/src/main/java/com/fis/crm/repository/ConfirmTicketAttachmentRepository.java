package com.fis.crm.repository;

import com.fis.crm.domain.ConfirmTicketAttachment;
import com.fis.crm.domain.ProccessTicketAttachment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the ConfirmTicketAttachment entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ConfirmTicketAttachmentRepository extends JpaRepository<ConfirmTicketAttachment, Long> {}
