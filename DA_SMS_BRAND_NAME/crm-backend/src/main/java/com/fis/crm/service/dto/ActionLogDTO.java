package com.fis.crm.service.dto;

import java.io.Serializable;
import java.time.Instant;

/**
 * A DTO for the {@link com.fis.crm.domain.ConfigSchedule} entity.
 */
public class ActionLogDTO implements Serializable {
    
    private Long id;

    private Long userId;

    private String actionType;

    private Long objectId;

    private String objectName;

    private String note;

    private Instant issueDateTime;

    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getActionType() {
        return actionType;
    }

    public void setActionType(String actionType) {
        this.actionType = actionType;
    }

    public Long getObjectId() {
        return objectId;
    }

    public void setObjectId(Long objectId) {
        this.objectId = objectId;
    }

    public String getObjectName() {
        return objectName;
    }

    public void setObjectName(String objectName) {
        this.objectName = objectName;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Instant getIssueDateTime() {
        return issueDateTime;
    }

    public void setIssueDateTime(Instant issueDateTime) {
        this.issueDateTime = issueDateTime;
    }

    public ActionLogDTO(){

    }
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ActionLogDTO)) {
            return false;
        }

        return id != null && id.equals(((ActionLogDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 32;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ActionLogDTO{" +
            "id=" + getId() +
            ", userId=" + getUserId() +
            ", actionType=" + getActionType() +
            ", objectId=" + getObjectId() +
            ", objectName='" + getObjectName() + "'" +
            ", note='" + getNote() + "'" +
            ", issueDateTime='" + getIssueDateTime() + "'" +
            "}";
    }
}
