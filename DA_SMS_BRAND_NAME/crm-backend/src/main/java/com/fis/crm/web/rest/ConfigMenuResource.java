package com.fis.crm.web.rest;

import com.fis.crm.repository.ConfigMenuRepository;
import com.fis.crm.service.ConfigMenuService;
import com.fis.crm.service.dto.ConfigMenuDTO;
import com.fis.crm.service.dto.MenuItemDTO;
import com.fis.crm.web.rest.errors.BadRequestAlertException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * REST controller for managing {@link com.fis.crm.domain.ConfigMenu}.
 */
@RestController
@RequestMapping("/api")
public class ConfigMenuResource {

    private final Logger log = LoggerFactory.getLogger(ConfigMenuResource.class);

    private static final String ENTITY_NAME = "configMenu";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ConfigMenuService configMenuService;

    private final ConfigMenuRepository configMenuRepository;

    public ConfigMenuResource(ConfigMenuService configMenuService, ConfigMenuRepository configMenuRepository) {
        this.configMenuService = configMenuService;
        this.configMenuRepository = configMenuRepository;
    }

    /**
     * {@code POST  /config-menus} : Create a new configMenu.
     *
     * @param configMenuDTO the configMenuDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new configMenuDTO, or with status {@code 400 (Bad Request)} if the configMenu has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/config-menus")
    public ResponseEntity<ConfigMenuDTO> createConfigMenu(@RequestBody ConfigMenuDTO configMenuDTO) throws URISyntaxException {
        log.debug("REST request to save ConfigMenu : {}", configMenuDTO);
        if (configMenuDTO.getId() != null) {
            throw new BadRequestAlertException("A new configMenu cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ConfigMenuDTO result = configMenuService.save(configMenuDTO);
        return ResponseEntity
            .created(new URI("/api/config-menus/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /config-menus/:id} : Updates an existing configMenu.
     *
     * @param id            the id of the configMenuDTO to save.
     * @param configMenuDTO the configMenuDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated configMenuDTO,
     * or with status {@code 400 (Bad Request)} if the configMenuDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the configMenuDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/config-menus/{id}")
    public ResponseEntity<ConfigMenuDTO> updateConfigMenu(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody ConfigMenuDTO configMenuDTO
    ) throws URISyntaxException {
        log.debug("REST request to update ConfigMenu : {}, {}", id, configMenuDTO);
        if (configMenuDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, configMenuDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!configMenuRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        ConfigMenuDTO result = configMenuService.save(configMenuDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, configMenuDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /config-menus/:id} : Partial updates given fields of an existing configMenu, field will ignore if it is null
     *
     * @param id            the id of the configMenuDTO to save.
     * @param configMenuDTO the configMenuDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated configMenuDTO,
     * or with status {@code 400 (Bad Request)} if the configMenuDTO is not valid,
     * or with status {@code 404 (Not Found)} if the configMenuDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the configMenuDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/config-menus/{id}", consumes = "application/merge-patch+json")
    public ResponseEntity<ConfigMenuDTO> partialUpdateConfigMenu(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody ConfigMenuDTO configMenuDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update ConfigMenu partially : {}, {}", id, configMenuDTO);
        if (configMenuDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, configMenuDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!configMenuRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<ConfigMenuDTO> result = configMenuService.partialUpdate(configMenuDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, configMenuDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /config-menus} : get all the configMenus.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of configMenus in body.
     */
    @GetMapping("/config-menus")
    public ResponseEntity<List<ConfigMenuDTO>> getAllConfigMenus(Pageable pageable) {
        log.debug("REST request to get a page of ConfigMenus");
        Page<ConfigMenuDTO> page = configMenuService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /config-menus/:id} : get the "id" configMenu.
     *
     * @param id the id of the configMenuDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the configMenuDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/config-menus/{id}")
    public ResponseEntity<ConfigMenuDTO> getConfigMenu(@PathVariable Long id) {
        log.debug("REST request to get ConfigMenu : {}", id);
        Optional<ConfigMenuDTO> configMenuDTO = configMenuService.findOne(id);
        return ResponseUtil.wrapOrNotFound(configMenuDTO);
    }

    /**
     * {@code DELETE  /config-menus/:id} : delete the "id" configMenu.
     *
     * @param id the id of the configMenuDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/config-menus/{id}")
    public ResponseEntity<Void> deleteConfigMenu(@PathVariable Long id) {
        log.debug("REST request to delete ConfigMenu : {}", id);
        configMenuService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }

    @GetMapping("/config-menus/get-tree-by-login")
    public ResponseEntity<List<MenuItemDTO>> getMenuTreeLogin(String login) {
        log.debug("REST request to get a tree of Menus");
        List<MenuItemDTO> rs = configMenuService.getMenuTreeLogin(login);
        return ResponseEntity.ok().body(rs);
    }
}
