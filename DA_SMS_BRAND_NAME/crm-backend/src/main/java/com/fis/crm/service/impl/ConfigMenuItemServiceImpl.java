package com.fis.crm.service.impl;

import com.fis.crm.domain.ConfigMenuItem;
import com.fis.crm.repository.ConfigMenuItemRepository;
import com.fis.crm.service.ConfigMenuItemService;
import com.fis.crm.service.dto.ConfigMenuItemDTO;
import com.fis.crm.service.mapper.ConfigMenuItemMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link ConfigMenuItem}.
 */
@Service
@Transactional
public class ConfigMenuItemServiceImpl implements ConfigMenuItemService {

  private final Logger log = LoggerFactory.getLogger(ConfigMenuItemServiceImpl.class);

  private final ConfigMenuItemRepository configMenuItemRepository;

  private final ConfigMenuItemMapper configMenuItemMapper;

  public ConfigMenuItemServiceImpl(ConfigMenuItemRepository configMenuItemRepository, ConfigMenuItemMapper configMenuItemMapper) {
    this.configMenuItemRepository = configMenuItemRepository;
    this.configMenuItemMapper = configMenuItemMapper;
  }

  @Override
  public ConfigMenuItemDTO save(ConfigMenuItemDTO configMenuItemDTO) {
    log.debug("Request to save ConfigMenuItem : {}", configMenuItemDTO);
    ConfigMenuItem configMenuItem = configMenuItemMapper.toEntity(configMenuItemDTO);
    configMenuItem = configMenuItemRepository.save(configMenuItem);
    return configMenuItemMapper.toDto(configMenuItem);
  }

  @Override
  public Optional<ConfigMenuItemDTO> partialUpdate(ConfigMenuItemDTO configMenuItemDTO) {
    log.debug("Request to partially update ConfigMenuItem : {}", configMenuItemDTO);

    return configMenuItemRepository
      .findById(configMenuItemDTO.getId())
      .map(
        existingConfigMenuItem -> {
          configMenuItemMapper.partialUpdate(existingConfigMenuItem, configMenuItemDTO);
          return existingConfigMenuItem;
        }
      )
      .map(configMenuItemRepository::save)
      .map(configMenuItemMapper::toDto);
  }

  @Override
  @Transactional(readOnly = true)
  public Page<ConfigMenuItemDTO> findAll(Pageable pageable) {
    log.debug("Request to get all ConfigMenuItems");
    return configMenuItemRepository.findAll(pageable).map(configMenuItemMapper::toDto);
  }

  @Override
  @Transactional(readOnly = true)
  public Optional<ConfigMenuItemDTO> findOne(Long id) {
    log.debug("Request to get ConfigMenuItem : {}", id);
    return configMenuItemRepository.findById(id).map(configMenuItemMapper::toDto);
  }

  @Override
  public void delete(Long id) {
    log.debug("Request to delete ConfigMenuItem : {}", id);
    configMenuItemRepository.deleteById(id);
  }
}
