package com.fis.crm.service.impl;

import com.fis.crm.domain.TicketRequestAttachment;
import com.fis.crm.domain.ProccessTicket;

import com.fis.crm.repository.TicketRequestAttachmentRepository;
import com.fis.crm.repository.ProcessTicketRepository;

import com.fis.crm.service.TicketRequestAttachmentService;
import com.fis.crm.service.ProcessTicketService;

import com.fis.crm.service.dto.TicketRequestAttachmentDTO;
import com.fis.crm.service.dto.ProcessTicketDTO;

import com.fis.crm.service.mapper.TicketRequestAttachmentMapper;
import com.fis.crm.service.mapper.ProcessTicketMapper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link ProccessTicket}.
 */
@Service
@Transactional
public class ProcessTicketServiceImpl implements ProcessTicketService {

  private final Logger log = LoggerFactory.getLogger(ProcessTicketServiceImpl.class);

  private final ProcessTicketRepository processTicketRepository;

  private final ProcessTicketMapper processTicketMapper;

  public ProcessTicketServiceImpl(ProcessTicketRepository processTicketRepository,
                                  ProcessTicketMapper processTicketMapper) {
    this.processTicketRepository = processTicketRepository;
    this.processTicketMapper = processTicketMapper;
  }

  @Override
  public ProcessTicketDTO save(ProcessTicketDTO processTicketDTO) {
    log.debug("Request to save ProcessTicket : {}", processTicketDTO);
      ProccessTicket proccessTicket = processTicketMapper.toEntity(processTicketDTO);
      proccessTicket = processTicketRepository.save(proccessTicket);
    return processTicketMapper.toDto(proccessTicket);
  }

  @Override
  public Optional<ProcessTicketDTO> partialUpdate(ProcessTicketDTO processTicketDTO) {
    log.debug("Request to partially update ProcessTicket : {}", processTicketDTO);

//    return ticketRequestAttachmentRepository
//      .findById(ticketRequestAttachmentDTO.getTicketRequestAttachmentId())
//      .map(
//        existingTicketRequest -> {
//            ticketRequestAttachmentMapper.partialUpdate(existingTicketRequest, ticketRequestAttachmentDTO);
//          return existingTicketRequest;
//        }
//      )
//      .map(ticketRequestAttachmentRepository::save)
//      .map(ticketRequestAttachmentMapper::toDto);
      return null;

  }

  @Override
  @Transactional(readOnly = true)
  public Page<ProcessTicketDTO> findAll(Pageable pageable) {
    log.debug("Request to get all ProcessTickets");
    return processTicketRepository.findAll(pageable).map(processTicketMapper::toDto);
  }

  @Override
  @Transactional(readOnly = true)
  public Optional<ProcessTicketDTO> findOne(Long id) {
    log.debug("Request to get ProcessTicket : {}", id);
    return processTicketRepository.findById(id).map(processTicketMapper::toDto);
  }

  @Override
  public void delete(Long id) {
    log.debug("Request to delete ProcessTicket : {}", id);
      processTicketRepository.deleteById(id);
  }
}
