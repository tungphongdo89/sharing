package com.fis.crm.service.impl;

import com.fis.crm.commons.DataUtil;
import com.fis.crm.domain.Ticket;
import com.fis.crm.repository.TicketRepository;
import com.fis.crm.service.TicketService;
import com.fis.crm.service.dto.ConfigScheduleDTO;
import com.fis.crm.service.dto.TicketDTO;
import com.fis.crm.service.mapper.TicketMapper;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import liquibase.util.StringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 * Service Implementation for managing {@link Ticket}.
 */
@Service
@Transactional
public class TicketServiceImpl implements TicketService {

  private final Logger log = LoggerFactory.getLogger(TicketServiceImpl.class);

  private final TicketRepository ticketRepository;

  private final TicketMapper ticketMapper;

  public TicketServiceImpl(TicketRepository ticketRepository, TicketMapper ticketMapper) {
    this.ticketRepository = ticketRepository;
    this.ticketMapper = ticketMapper;
  }

  @Override
  public TicketDTO save(TicketDTO ticketDTO) {
    log.debug("Request to save Ticket : {}", ticketDTO);
    Ticket ticket = ticketMapper.toEntity(ticketDTO);
    ticket = ticketRepository.save(ticket);
    return ticketMapper.toDto(ticket);
  }

  @Override
  public Optional<TicketDTO> partialUpdate(TicketDTO ticketDTO) {
    log.debug("Request to partially update Ticket : {}", ticketDTO);

//    return ticketRepository
//      .findById(ticketDTO.getId())
//      .map(
//        existingTicket -> {
//          ticketMapper.partialUpdate(existingTicket, ticketDTO);
//          return existingTicket;
//        }
//      )
//      .map(ticketRepository::save)
//      .map(ticketMapper::toDto);
      return null;
  }

  @Override
  @Transactional(readOnly = true)
  public Page<TicketDTO> findAll(Pageable pageable) {
    log.debug("Request to get all Tickets");
    return ticketRepository.findAll(pageable).map(ticketMapper::toDto);
  }

  @Override
  @Transactional(readOnly = true)
  public Optional<TicketDTO> findOne(Long id) {
    log.debug("Request to get Ticket : {}", id);
    return ticketRepository.findById(id).map(ticketMapper::toDto);
  }

  @Override
  public void delete(Long id) {
    log.debug("Request to delete Ticket : {}", id);
    ticketRepository.deleteById(id);
  }

    @Override
    public List<TicketDTO> getListHistorySupports() {

        List<Object[]> lst = new ArrayList<Object[]>();
        List<TicketDTO> lstResult = new ArrayList<TicketDTO>();
        try {
            StringBuilder sb = new StringBuilder(
                " select ticket_id, ticket_code, status, chanel_type, create_datetime, jhi_user.first_name, jhi_user.last_name\n" +
                " from ticket join jhi_user on ticket.create_user = jhi_user.id " );

            Query query =  em.createNativeQuery(sb.toString());

            lst = query.getResultList();
            for (Object[] obj1 : lst){
                TicketDTO ticketDTO = new TicketDTO();
                ticketDTO.setTicketId(DataUtil.safeToLong(obj1[0]));
                ticketDTO.setTicketCode(DataUtil.safeToString(obj1[1]));
                ticketDTO.setStatus(DataUtil.safeToString(obj1[2]));
                ticketDTO.setChannelType(DataUtil.safeToString(obj1[3]));
                ticketDTO.setCreateDatetime(DataUtil.safeToInstant(obj1[4]));
                ticketDTO.setCustomerName(DataUtil.safeToString(obj1[5]) + DataUtil.safeToString(obj1[6]));

                lstResult.add(ticketDTO);
            }
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return lstResult;
    }

    @PersistenceContext
    EntityManager em;
}
