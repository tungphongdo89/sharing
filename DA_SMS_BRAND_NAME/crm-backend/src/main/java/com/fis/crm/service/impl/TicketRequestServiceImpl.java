package com.fis.crm.service.impl;

import com.fis.crm.domain.TicketRequest;
import com.fis.crm.repository.TicketRequestRepository;
import com.fis.crm.service.TicketRequestService;
import com.fis.crm.service.dto.TicketRequestDTO;
import com.fis.crm.service.mapper.TicketRequestMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link TicketRequest}.
 */
@Service
@Transactional
public class TicketRequestServiceImpl implements TicketRequestService {

  private final Logger log = LoggerFactory.getLogger(TicketRequestServiceImpl.class);

  private final TicketRequestRepository ticketRequestRepository;

  private final TicketRequestMapper ticketRequestMapper;

  public TicketRequestServiceImpl(TicketRequestRepository ticketRequestRepository, TicketRequestMapper ticketRequestMapper) {
    this.ticketRequestRepository = ticketRequestRepository;
    this.ticketRequestMapper = ticketRequestMapper;
  }

  @Override
  public TicketRequestDTO save(TicketRequestDTO ticketRequestDTO) {
    log.debug("Request to save TicketRequest : {}", ticketRequestDTO);
    TicketRequest ticketRequest = ticketRequestMapper.toEntity(ticketRequestDTO);
    ticketRequest = ticketRequestRepository.save(ticketRequest);
    return ticketRequestMapper.toDto(ticketRequest);
  }

  @Override
  public Optional<TicketRequestDTO> partialUpdate(TicketRequestDTO ticketRequestDTO) {
    log.debug("Request to partially update TicketRequest : {}", ticketRequestDTO);

//    return ticketRequestRepository
//      .findById(ticketRequestDTO.getId())
//      .map(
//        existingTicketRequest -> {
//          ticketRequestMapper.partialUpdate(existingTicketRequest, ticketRequestDTO);
//          return existingTicketRequest;
//        }
//      )
//      .map(ticketRequestRepository::save)
//      .map(ticketRequestMapper::toDto);
      return null;
  }

  @Override
  @Transactional(readOnly = true)
  public Page<TicketRequestDTO> findAll(Pageable pageable) {
    log.debug("Request to get all TicketRequests");
    return ticketRequestRepository.findAll(pageable).map(ticketRequestMapper::toDto);
  }

  @Override
  @Transactional(readOnly = true)
  public Optional<TicketRequestDTO> findOne(Long id) {
    log.debug("Request to get TicketRequest : {}", id);
    return ticketRequestRepository.findById(id).map(ticketRequestMapper::toDto);
  }

  @Override
  public void delete(Long id) {
    log.debug("Request to delete TicketRequest : {}", id);
    ticketRequestRepository.deleteById(id);
  }
}
