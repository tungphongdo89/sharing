package com.fis.crm.repository;

import com.fis.crm.domain.ProccessTicket;
import com.fis.crm.domain.ProccessTicketAttachment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the TicketRequestAttachment entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ProcessTicketAttachmentRepository extends JpaRepository<ProccessTicketAttachment, Long> {}
