package com.fis.crm.repository;

import com.fis.crm.domain.ConfigMenuItem;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the ConfigMenuItem entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ConfigMenuItemRepository extends JpaRepository<ConfigMenuItem, Long> {}
