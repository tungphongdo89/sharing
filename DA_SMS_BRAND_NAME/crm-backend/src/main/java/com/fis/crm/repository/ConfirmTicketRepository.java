package com.fis.crm.repository;

import com.fis.crm.domain.ProccessTicket;
import com.fis.crm.domain.ConfirmTicket;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the ConfirmTicket entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ConfirmTicketRepository extends JpaRepository<ConfirmTicket, Long> {}
