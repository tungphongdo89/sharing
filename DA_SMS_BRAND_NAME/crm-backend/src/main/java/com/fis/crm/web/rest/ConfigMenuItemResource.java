package com.fis.crm.web.rest;

import com.fis.crm.repository.ConfigMenuItemRepository;
import com.fis.crm.service.ConfigMenuItemService;
import com.fis.crm.service.dto.ConfigMenuItemDTO;
import com.fis.crm.web.rest.errors.BadRequestAlertException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * REST controller for managing {@link com.fis.crm.domain.ConfigMenuItem}.
 */
@RestController
@RequestMapping("/api")
public class ConfigMenuItemResource {

    private final Logger log = LoggerFactory.getLogger(ConfigMenuItemResource.class);

    private static final String ENTITY_NAME = "configMenuItem";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ConfigMenuItemService configMenuItemService;

    private final ConfigMenuItemRepository configMenuItemRepository;

    public ConfigMenuItemResource(ConfigMenuItemService configMenuItemService, ConfigMenuItemRepository configMenuItemRepository) {
        this.configMenuItemService = configMenuItemService;
        this.configMenuItemRepository = configMenuItemRepository;
    }

    /**
     * {@code POST  /config-menu-items} : Create a new configMenuItem.
     *
     * @param configMenuItemDTO the configMenuItemDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new configMenuItemDTO, or with status {@code 400 (Bad Request)} if the configMenuItem has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/config-menu-items")
    public ResponseEntity<ConfigMenuItemDTO> createConfigMenuItem(@RequestBody ConfigMenuItemDTO configMenuItemDTO)
        throws URISyntaxException {
        log.debug("REST request to save ConfigMenuItem : {}", configMenuItemDTO);
        if (configMenuItemDTO.getId() != null) {
            throw new BadRequestAlertException("A new configMenuItem cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ConfigMenuItemDTO result = configMenuItemService.save(configMenuItemDTO);
        return ResponseEntity
            .created(new URI("/api/config-menu-items/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /config-menu-items/:id} : Updates an existing configMenuItem.
     *
     * @param id                the id of the configMenuItemDTO to save.
     * @param configMenuItemDTO the configMenuItemDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated configMenuItemDTO,
     * or with status {@code 400 (Bad Request)} if the configMenuItemDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the configMenuItemDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/config-menu-items/{id}")
    public ResponseEntity<ConfigMenuItemDTO> updateConfigMenuItem(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody ConfigMenuItemDTO configMenuItemDTO
    ) throws URISyntaxException {
        log.debug("REST request to update ConfigMenuItem : {}, {}", id, configMenuItemDTO);
        if (configMenuItemDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, configMenuItemDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!configMenuItemRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        ConfigMenuItemDTO result = configMenuItemService.save(configMenuItemDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, configMenuItemDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /config-menu-items/:id} : Partial updates given fields of an existing configMenuItem, field will ignore if it is null
     *
     * @param id                the id of the configMenuItemDTO to save.
     * @param configMenuItemDTO the configMenuItemDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated configMenuItemDTO,
     * or with status {@code 400 (Bad Request)} if the configMenuItemDTO is not valid,
     * or with status {@code 404 (Not Found)} if the configMenuItemDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the configMenuItemDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/config-menu-items/{id}", consumes = "application/merge-patch+json")
    public ResponseEntity<ConfigMenuItemDTO> partialUpdateConfigMenuItem(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody ConfigMenuItemDTO configMenuItemDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update ConfigMenuItem partially : {}, {}", id, configMenuItemDTO);
        if (configMenuItemDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, configMenuItemDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!configMenuItemRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<ConfigMenuItemDTO> result = configMenuItemService.partialUpdate(configMenuItemDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, configMenuItemDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /config-menu-items} : get all the configMenuItems.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of configMenuItems in body.
     */
    @GetMapping("/config-menu-items")
    public ResponseEntity<List<ConfigMenuItemDTO>> getAllConfigMenuItems(Pageable pageable) {
        log.debug("REST request to get a page of ConfigMenuItems");
        Page<ConfigMenuItemDTO> page = configMenuItemService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /config-menu-items/:id} : get the "id" configMenuItem.
     *
     * @param id the id of the configMenuItemDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the configMenuItemDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/config-menu-items/{id}")
    public ResponseEntity<ConfigMenuItemDTO> getConfigMenuItem(@PathVariable Long id) {
        log.debug("REST request to get ConfigMenuItem : {}", id);
        Optional<ConfigMenuItemDTO> configMenuItemDTO = configMenuItemService.findOne(id);
        return ResponseUtil.wrapOrNotFound(configMenuItemDTO);
    }

    /**
     * {@code DELETE  /config-menu-items/:id} : delete the "id" configMenuItem.
     *
     * @param id the id of the configMenuItemDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/config-menu-items/{id}")
    public ResponseEntity<Void> deleteConfigMenuItem(@PathVariable Long id) {
        log.debug("REST request to delete ConfigMenuItem : {}", id);
        configMenuItemService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
