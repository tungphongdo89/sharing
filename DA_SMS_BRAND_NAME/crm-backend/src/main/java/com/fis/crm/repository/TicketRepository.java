package com.fis.crm.repository;

import com.fis.crm.domain.Ticket;
import com.fis.crm.service.dto.TicketDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the Ticket entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TicketRepository extends JpaRepository<Ticket, Long> {


}
