package com.fis.crm.repository;

import com.fis.crm.domain.ConfigMenu;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the ConfigMenu entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ConfigMenuRepository extends JpaRepository<ConfigMenu, Long>, ConfigMenuCustomRepository {
}
