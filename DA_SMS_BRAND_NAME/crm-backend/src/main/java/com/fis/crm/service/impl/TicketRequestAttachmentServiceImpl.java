package com.fis.crm.service.impl;

import com.fis.crm.domain.TicketRequest;
import com.fis.crm.domain.TicketRequestAttachment;
import com.fis.crm.repository.TicketRequestAttachmentRepository;
import com.fis.crm.repository.TicketRequestRepository;
import com.fis.crm.service.TicketRequestAttachmentService;
import com.fis.crm.service.TicketRequestService;
import com.fis.crm.service.dto.TicketRequestAttachmentDTO;
import com.fis.crm.service.dto.TicketRequestDTO;
import com.fis.crm.service.mapper.TicketRequestAttachmentMapper;
import com.fis.crm.service.mapper.TicketRequestMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link TicketRequestAttachment}.
 */
@Service
@Transactional
public class TicketRequestAttachmentServiceImpl implements TicketRequestAttachmentService {

  private final Logger log = LoggerFactory.getLogger(TicketRequestAttachmentServiceImpl.class);

  private final TicketRequestAttachmentRepository ticketRequestAttachmentRepository;

  private final TicketRequestAttachmentMapper ticketRequestAttachmentMapper;

  public TicketRequestAttachmentServiceImpl(TicketRequestAttachmentRepository ticketRequestAttachmentRepository,
                                            TicketRequestAttachmentMapper ticketRequestAttachmentMapper) {
    this.ticketRequestAttachmentRepository = ticketRequestAttachmentRepository;
    this.ticketRequestAttachmentMapper = ticketRequestAttachmentMapper;
  }

  @Override
  public TicketRequestAttachmentDTO save(TicketRequestAttachmentDTO ticketRequestAttachmentDTO) {
    log.debug("Request to save TicketRequest : {}", ticketRequestAttachmentDTO);
    TicketRequestAttachment ticketRequestAttachment = ticketRequestAttachmentMapper.toEntity(ticketRequestAttachmentDTO);
      ticketRequestAttachment = ticketRequestAttachmentRepository.save(ticketRequestAttachment);
    return ticketRequestAttachmentMapper.toDto(ticketRequestAttachment);
  }

  @Override
  public Optional<TicketRequestAttachmentDTO> partialUpdate(TicketRequestAttachmentDTO ticketRequestAttachmentDTO) {
    log.debug("Request to partially update TicketRequest : {}", ticketRequestAttachmentDTO);

//    return ticketRequestAttachmentRepository
//      .findById(ticketRequestAttachmentDTO.getTicketRequestAttachmentId())
//      .map(
//        existingTicketRequest -> {
//            ticketRequestAttachmentMapper.partialUpdate(existingTicketRequest, ticketRequestAttachmentDTO);
//          return existingTicketRequest;
//        }
//      )
//      .map(ticketRequestAttachmentRepository::save)
//      .map(ticketRequestAttachmentMapper::toDto);
      return null;

  }

  @Override
  @Transactional(readOnly = true)
  public Page<TicketRequestAttachmentDTO> findAll(Pageable pageable) {
    log.debug("Request to get all TicketRequestAttachments");
    return ticketRequestAttachmentRepository.findAll(pageable).map(ticketRequestAttachmentMapper::toDto);
  }

  @Override
  @Transactional(readOnly = true)
  public Optional<TicketRequestAttachmentDTO> findOne(Long id) {
    log.debug("Request to get TicketRequestAttachment : {}", id);
    return ticketRequestAttachmentRepository.findById(id).map(ticketRequestAttachmentMapper::toDto);
  }

  @Override
  public void delete(Long id) {
    log.debug("Request to delete TicketRequestAttachment : {}", id);
      ticketRequestAttachmentRepository.deleteById(id);
  }
}
