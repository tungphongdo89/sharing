package com.fis.crm.service.dto;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

/**
 * A DTO for the {@link com.fis.crm.domain.TicketRequest} entity.
 */
public class TicketRequestDTO implements Serializable {

  private Long id;

  private Long ticketRequestId;

  private String ticketId;

  private String ticketRequestCode;

  private Long requestType;

  private Long bussinessType;

  private Long departmentId;

  private String status;

  private Instant deadline;

  private String content;

  private Instant confirmDate;

  private Long timeNotify;

  private Long createUser;

  private Instant createDatetime;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Long getTicketRequestId() {
    return ticketRequestId;
  }

  public void setTicketRequestId(Long ticketRequestId) {
    this.ticketRequestId = ticketRequestId;
  }

  public String getTicketId() {
    return ticketId;
  }

  public void setTicketId(String ticketId) {
    this.ticketId = ticketId;
  }

  public String getTicketRequestCode() {
    return ticketRequestCode;
  }

  public void setTicketRequestCode(String ticketRequestCode) {
    this.ticketRequestCode = ticketRequestCode;
  }

  public Long getRequestType() {
    return requestType;
  }

  public void setRequestType(Long requestType) {
    this.requestType = requestType;
  }

  public Long getBussinessType() {
    return bussinessType;
  }

  public void setBussinessType(Long bussinessType) {
    this.bussinessType = bussinessType;
  }

  public Long getDepartmentId() {
    return departmentId;
  }

  public void setDepartmentId(Long departmentId) {
    this.departmentId = departmentId;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public Instant getDeadline() {
    return deadline;
  }

  public void setDeadline(Instant deadline) {
    this.deadline = deadline;
  }

  public String getContent() {
    return content;
  }

  public void setContent(String content) {
    this.content = content;
  }

  public Instant getConfirmDate() {
    return confirmDate;
  }

  public void setConfirmDate(Instant confirmDate) {
    this.confirmDate = confirmDate;
  }

  public Long getTimeNotify() {
    return timeNotify;
  }

  public void setTimeNotify(Long timeNotify) {
    this.timeNotify = timeNotify;
  }

  public Long getCreateUser() {
    return createUser;
  }

  public void setCreateUser(Long createUser) {
    this.createUser = createUser;
  }

  public Instant getCreateDatetime() {
    return createDatetime;
  }

  public void setCreateDatetime(Instant createDatetime) {
    this.createDatetime = createDatetime;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof TicketRequestDTO)) {
      return false;
    }

    TicketRequestDTO ticketRequestDTO = (TicketRequestDTO) o;
    if (this.id == null) {
      return false;
    }
    return Objects.equals(this.id, ticketRequestDTO.id);
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.id);
  }

  // prettier-ignore
    @Override
    public String toString() {
        return "TicketRequestDTO{" +
            "id=" + getId() +
            ", ticketRequestId=" + getTicketRequestId() +
            ", ticketId='" + getTicketId() + "'" +
            ", ticketRequestCode='" + getTicketRequestCode() + "'" +
            ", requestType=" + getRequestType() +
            ", bussinessType=" + getBussinessType() +
            ", departmentId=" + getDepartmentId() +
            ", status='" + getStatus() + "'" +
            ", deadline='" + getDeadline() + "'" +
            ", content='" + getContent() + "'" +
            ", confirmDate='" + getConfirmDate() + "'" +
            ", timeNotify=" + getTimeNotify() +
            ", createUser=" + getCreateUser() +
            ", createDatetime='" + getCreateDatetime() + "'" +
            "}";
    }
}
