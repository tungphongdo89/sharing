package com.fis.crm.service;

import com.fis.crm.service.dto.ProcessTicketDTO;
import com.fis.crm.service.dto.ConfirmTicketDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link com.fis.crm.domain.TicketRequest}.
 */
public interface ConfirmTicketService {
  /**
   * Save a ticketRequest.
   *
   * @param confirmTicketDTO the entity to save.
   * @return the persisted entity.
   */
  ConfirmTicketDTO save(ConfirmTicketDTO confirmTicketDTO);

  /**
   * Partially updates a ticketRequest.
   *
   * @param confirmTicketDTO the entity to update partially.
   * @return the persisted entity.
   */
  Optional<ConfirmTicketDTO> partialUpdate(ConfirmTicketDTO confirmTicketDTO);

  /**
   * Get all the ticketRequests.
   *
   * @param pageable the pagination information.
   * @return the list of entities.
   */
  Page<ConfirmTicketDTO> findAll(Pageable pageable);

  /**
   * Get the "id" ticketRequest.
   *
   * @param confirmTicketId the id of the entity.
   * @return the entity.
   */
  Optional<ConfirmTicketDTO> findOne(Long confirmTicketId);

  /**
   * Delete the "id" ticketRequest.
   *
   * @param confirmTicketId the id of the entity.
   */
  void delete(Long confirmTicketId);
}
