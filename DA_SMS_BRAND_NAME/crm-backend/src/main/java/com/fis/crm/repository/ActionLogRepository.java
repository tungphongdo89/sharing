package com.fis.crm.repository;

import com.fis.crm.domain.ActionLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the ConfigSchedule entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ActionLogRepository extends JpaRepository<ActionLog, Long> {
}
