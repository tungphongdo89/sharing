package com.fis.crm.service.impl;

import com.fis.crm.domain.ConfigMenu;
import com.fis.crm.repository.ConfigMenuRepository;
import com.fis.crm.service.ConfigMenuService;
import com.fis.crm.service.dto.ConfigMenuDTO;
import com.fis.crm.service.dto.MenuItemDTO;
import com.fis.crm.service.mapper.ConfigMenuMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link ConfigMenu}.
 */
@Service
@Transactional
public class ConfigMenuServiceImpl implements ConfigMenuService {

    private final Logger log = LoggerFactory.getLogger(ConfigMenuServiceImpl.class);

    private final ConfigMenuRepository configMenuRepository;

    private final ConfigMenuMapper configMenuMapper;

    public ConfigMenuServiceImpl(ConfigMenuRepository configMenuRepository, ConfigMenuMapper configMenuMapper) {
        this.configMenuRepository = configMenuRepository;
        this.configMenuMapper = configMenuMapper;
    }

    @Override
    public ConfigMenuDTO save(ConfigMenuDTO configMenuDTO) {
        log.debug("Request to save ConfigMenu : {}", configMenuDTO);
        ConfigMenu configMenu = configMenuMapper.toEntity(configMenuDTO);
        configMenu = configMenuRepository.save(configMenu);
        return configMenuMapper.toDto(configMenu);
    }

    @Override
    public Optional<ConfigMenuDTO> partialUpdate(ConfigMenuDTO configMenuDTO) {
        log.debug("Request to partially update ConfigMenu : {}", configMenuDTO);

        return configMenuRepository
            .findById(configMenuDTO.getId())
            .map(
                existingConfigMenu -> {
                    configMenuMapper.partialUpdate(existingConfigMenu, configMenuDTO);
                    return existingConfigMenu;
                }
            )
            .map(configMenuRepository::save)
            .map(configMenuMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<ConfigMenuDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ConfigMenus");
        return configMenuRepository.findAll(pageable).map(configMenuMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<ConfigMenuDTO> findOne(Long id) {
        log.debug("Request to get ConfigMenu : {}", id);
        return configMenuRepository.findById(id).map(configMenuMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete ConfigMenu : {}", id);
        configMenuRepository.deleteById(id);
    }

    @Override
    public List<MenuItemDTO> getMenuTreeLogin(String login) {
        return configMenuRepository.getMenuTreeLogin(login);
    }
}
