package com.fis.crm.web.rest;

import com.fis.crm.domain.User;
import com.fis.crm.service.UserService;
import com.fis.crm.service.dto.UserDTO;
import java.util.*;
import java.util.Collections;

import com.fis.crm.service.response.UpdateUserResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.PaginationUtil;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/api")
public class PublicUserResource {

    private static final List<String> ALLOWED_ORDERED_PROPERTIES = Collections.unmodifiableList(
        Arrays.asList("id", "login", "firstName", "lastName", "email", "activated", "langKey")
    );

    private final Logger log = LoggerFactory.getLogger(PublicUserResource.class);

    private final UserService userService;

    public PublicUserResource(UserService userService) {
        this.userService = userService;
    }

    /**
     * {@code GET /users} : get all users with only the public informations - calling this are allowed for anyone.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body all users.
     */
    @GetMapping("/users")
    public ResponseEntity<List<UserDTO>> getAllPublicUsers(Pageable pageable) {
        log.debug("REST request to get all public User names");
        if (!onlyContainsAllowedProperties(pageable)) {
            return ResponseEntity.badRequest().build();
        }

        final Page<UserDTO> page = userService.getAllPublicUsers(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    private boolean onlyContainsAllowedProperties(Pageable pageable) {
        return pageable.getSort().stream().map(Sort.Order::getProperty).allMatch(ALLOWED_ORDERED_PROPERTIES::contains);
    }

    @PutMapping(value = "/users", params = "userId")
    public ResponseEntity<UpdateUserResponse> editUser(@RequestBody UserDTO userDTO, HttpServletRequest httpServletRequest){
        log.debug("REST request to get all public User names");
        try {
            Long id = Long.parseLong(httpServletRequest.getParameter("userId"));
            userDTO.setId(id);
            boolean res = userService.editPublicUsers(userDTO, id);
            if(res == true){
                UpdateUserResponse updateUserResponse = new UpdateUserResponse();
                updateUserResponse.setMsgCode("200");
                updateUserResponse.setMessage("Success");
                return ResponseEntity.ok().body(updateUserResponse);
            }
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        } catch (Exception e) {
            log.error(e.getMessage(),e);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }
    }

    @GetMapping(value = "/users", params = "keyword")
    public ResponseEntity<List<UserDTO>> getUserByKeyword(HttpServletRequest request){
        log.debug("REST request to get User By keyword");
        try{
            String keyword = request.getParameter("keyword");
            List<UserDTO> userDTO = userService.searchUser(keyword);
            return ResponseEntity.ok().body(userDTO);
        }
        catch (Exception e){
            log.error(e.getMessage(),e);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }
    }


    /**
     * Gets a list of all roles.
     * @return a string list of all roles.
     */
    @GetMapping("/authorities")
    public List<String> getAuthorities() {
        return userService.getAuthorities();
    }
}
