package com.fis.crm.service;

import com.fis.crm.service.dto.ConfigMenuItemDTO;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link com.fis.crm.domain.ConfigMenuItem}.
 */
public interface ConfigMenuItemService {
  /**
   * Save a configMenuItem.
   *
   * @param configMenuItemDTO the entity to save.
   * @return the persisted entity.
   */
  ConfigMenuItemDTO save(ConfigMenuItemDTO configMenuItemDTO);

  /**
   * Partially updates a configMenuItem.
   *
   * @param configMenuItemDTO the entity to update partially.
   * @return the persisted entity.
   */
  Optional<ConfigMenuItemDTO> partialUpdate(ConfigMenuItemDTO configMenuItemDTO);

  /**
   * Get all the configMenuItems.
   *
   * @param pageable the pagination information.
   * @return the list of entities.
   */
  Page<ConfigMenuItemDTO> findAll(Pageable pageable);

  /**
   * Get the "id" configMenuItem.
   *
   * @param id the id of the entity.
   * @return the entity.
   */
  Optional<ConfigMenuItemDTO> findOne(Long id);

  /**
   * Delete the "id" configMenuItem.
   *
   * @param id the id of the entity.
   */
  void delete(Long id);
}
