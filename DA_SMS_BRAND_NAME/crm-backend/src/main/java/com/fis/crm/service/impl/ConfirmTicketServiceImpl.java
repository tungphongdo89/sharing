package com.fis.crm.service.impl;

import com.fis.crm.domain.ProccessTicket;
import com.fis.crm.domain.ConfirmTicket;

import com.fis.crm.repository.ProcessTicketRepository;
import com.fis.crm.repository.ConfirmTicketRepository;

import com.fis.crm.service.ProcessTicketService;
import com.fis.crm.service.ConfirmTicketService;

import com.fis.crm.service.dto.ProcessTicketDTO;
import com.fis.crm.service.dto.ConfirmTicketDTO;

import com.fis.crm.service.mapper.ProcessTicketMapper;
import com.fis.crm.service.mapper.ConfirmTicketMapper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link ConfirmTicket}.
 */
@Service
@Transactional
public class ConfirmTicketServiceImpl implements ConfirmTicketService {

  private final Logger log = LoggerFactory.getLogger(ConfirmTicketServiceImpl.class);

  private final ConfirmTicketRepository confirmTicketRepository;

  private final ConfirmTicketMapper confirmTicketMapper;

  public ConfirmTicketServiceImpl(ConfirmTicketRepository confirmTicketRepository,
                                  ConfirmTicketMapper confirmTicketMapper) {
    this.confirmTicketRepository = confirmTicketRepository;
    this.confirmTicketMapper = confirmTicketMapper;
  }

  @Override
  public ConfirmTicketDTO save(ConfirmTicketDTO confirmTicketDTO) {
    log.debug("Request to save ConfirmTicket : {}", confirmTicketDTO);
      ConfirmTicket confirmTicket = confirmTicketMapper.toEntity(confirmTicketDTO);
      confirmTicket = confirmTicketRepository.save(confirmTicket);
    return confirmTicketMapper.toDto(confirmTicket);
  }

  @Override
  public Optional<ConfirmTicketDTO> partialUpdate(ConfirmTicketDTO confirmTicketDTO) {
    log.debug("Request to partially update ConfirmTicket : {}", confirmTicketDTO);

//    return ticketRequestAttachmentRepository
//      .findById(ticketRequestAttachmentDTO.getTicketRequestAttachmentId())
//      .map(
//        existingTicketRequest -> {
//            ticketRequestAttachmentMapper.partialUpdate(existingTicketRequest, ticketRequestAttachmentDTO);
//          return existingTicketRequest;
//        }
//      )
//      .map(ticketRequestAttachmentRepository::save)
//      .map(ticketRequestAttachmentMapper::toDto);
      return null;

  }

  @Override
  @Transactional(readOnly = true)
  public Page<ConfirmTicketDTO> findAll(Pageable pageable) {
    log.debug("Request to get all ConfirmTickets");
    return confirmTicketRepository.findAll(pageable).map(confirmTicketMapper::toDto);
  }

  @Override
  @Transactional(readOnly = true)
  public Optional<ConfirmTicketDTO> findOne(Long id) {
    log.debug("Request to get ConfirmTicket : {}", id);
    return confirmTicketRepository.findById(id).map(confirmTicketMapper::toDto);
  }

  @Override
  public void delete(Long id) {
    log.debug("Request to delete ConfirmTicket : {}", id);
      confirmTicketRepository.deleteById(id);
  }
}
